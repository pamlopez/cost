-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 11-11-2021 a las 04:27:24
-- Versión del servidor: 5.7.33
-- Versión de PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto_prod_sept26`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `politica`
--

CREATE TABLE `politica` (
  `id_politica` bigint(20) UNSIGNED NOT NULL,
  `titulo` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion_html` longtext COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `politica`
--

INSERT INTO `politica` (`id_politica`, `titulo`, `descripcion_html`, `estado`) VALUES
(1, 'POLITICA DE PUBLICACIÓN', '<h1><strong>Politica de Publicaci&oacute;n</strong></h1>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n\r\n<p><cite>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce consectetur ornare est, a porttitor turpis semper a. Aenean scelerisque dolor arcu, nec vehicula orci maximus et. Duis augue leo, consequat elementum semper eu, suscipit eu eros. Duis rhoncus augue ac vulputate lobortis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas eu est malesuada elit convallis lacinia eu ut lacus. Nunc mollis ex eu dignissim sodales. Sed feugiat risus eu odio condimentum consectetur.</cite></p>\r\n\r\n<ul>\r\n	<li>Donec iaculis dictum quam. Cras at molestie purus. Morbi vitae felis vel lorem blandit tempor sit amet eu sapien. Sed vel iaculis purus. Aenean sed ex at massa rhoncus dapibus rutrum in augue. Morbi sit amet rhoncus nunc, quis finibus odio. Sed placerat augue luctus massa varius lacinia. Vestibulum id sollicitudin tellus. Aliquam a molestie odio. Sed posuere non enim in mollis. Duis fringilla dolor ac elementum fermentum. Integer laoreet ipsum at turpis aliquet condimentum. Phasellus non maximus enim, eu pretium neque. Morbi vulputate velit a tellus tincidunt cursus.</li>\r\n	<li>Pellentesque blandit ut sapien ut tempus. In porta ultrices mauris quis maximus. Phasellus et diam mauris. Curabitur dui libero, ornare eu eros a, euismod ultricies lectus. Integer luctus nulla lorem, vitae viverra metus eleifend non. In vehicula elementum enim. Ut sed turpis vel ante elementum maximus. Donec ut nulla feugiat, ultricies ex vitae, accumsan justo. Vivamus diam mi, imperdiet non hendrerit eu, mattis vitae ipsum. Nullam ullamcorper lectus ut lorem egestas tempus.</li>\r\n	<li>Pellentesque blandit ut sapien ut tempus. In porta ultrices mauris quis maximus. Phasellus et diam mauris. Curabitur dui libero, ornare eu eros a, euismod ultricies lectus. Integer luctus nulla lorem, vitae viverra metus eleifend non. In vehicula elementum enim. Ut sed turpis vel ante elementum maximus. Donec ut nulla feugiat, ultricies ex vitae, accumsan justo. Vivamus diam mi, imperdiet non hendrerit eu, mattis vitae ipsum. Nullam ullamcorper lectus ut lorem egestas tempus.</li>\r\n</ul>\r\n\r\n<ol>\r\n	<li>Vestibulum id viverra sapien. Fusce porttitor in elit ac congue. Aliquam efficitur magna nec nunc tristique, vel pulvinar ligula gravida.</li>\r\n	<li>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean id nulla facilisis, eleifend ipsum vel, scelerisque enim.</li>\r\n	<li>Nullam tristique ornare mi quis cursus. Vivamus sagittis ligula eu ex consectetur egestas. Aenean euismod mi finibus massa pharetra, vel pellentesque orci rutrum.</li>\r\n	<li>Maecenas eu justo ligula. Cras maximus eu urna sed mollis. Sed bibendum, mi vitae dapibus ornare, sem magna sodales enim, convallis bibendum diam nibh vitae mi.</li>\r\n	<li>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Etiam accumsan metus ligula, et finibus magna laoreet in.</li>\r\n	<li>Aliquam ut interdum orci, at tincidunt nunc. Integer at eros ornare, imperdiet quam vitae, faucibus elit.</li>\r\n</ol>\r\n\r\n<p><em>Aenean erat erat, placerat consectetur consequat vitae, molestie at magna. Nulla blandit, lectus eu volutpat hendrerit, libero ex vehicula turpis, eget venenatis neque ante eu odio. Duis venenatis rutrum nunc vulputate ullamcorper. Integer a dui lacus. Integer sagittis et ex eu facilisis. Nullam vel orci mauris. In lectus mi, placerat at enim a, maximus consectetur diam. Aenean nec est ut urna maximus tincidunt. Sed porta velit quis risus aliquet dapibus. Duis ornare quam nec leo pulvinar accumsan. Pellentesque pharetra ex ut urna iaculis rutrum.</em></p>\r\n', 1),
(2, 'PRESENTACION', '<h1><strong>PRESENTACI&Oacute;N DE COST</strong></h1>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n\r\n<p><cite>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce consectetur ornare est, a porttitor turpis semper a. Aenean scelerisque dolor arcu, nec vehicula orci maximus et. Duis augue leo, consequat elementum semper eu, suscipit eu eros. Duis rhoncus augue ac vulputate lobortis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas eu est malesuada elit convallis lacinia eu ut lacus. Nunc mollis ex eu dignissim sodales. Sed feugiat risus eu odio condimentum consectetur.</cite></p>\r\n\r\n<ul>\r\n	<li>Donec iaculis dictum quam. Cras at molestie purus. Morbi vitae felis vel lorem blandit tempor sit amet eu sapien. Sed vel iaculis purus. Aenean sed ex at massa rhoncus dapibus rutrum in augue. Morbi sit amet rhoncus nunc, quis finibus odio. Sed placerat augue luctus massa varius lacinia. Vestibulum id sollicitudin tellus. Aliquam a molestie odio. Sed posuere non enim in mollis. Duis fringilla dolor ac elementum fermentum. Integer laoreet ipsum at turpis aliquet condimentum. Phasellus non maximus enim, eu pretium neque. Morbi vulputate velit a tellus tincidunt cursus.</li>\r\n	<li>Pellentesque blandit ut sapien ut tempus. In porta ultrices mauris quis maximus. Phasellus et diam mauris. Curabitur dui libero, ornare eu eros a, euismod ultricies lectus. Integer luctus nulla lorem, vitae viverra metus eleifend non. In vehicula elementum enim. Ut sed turpis vel ante elementum maximus. Donec ut nulla feugiat, ultricies ex vitae, accumsan justo. Vivamus diam mi, imperdiet non hendrerit eu, mattis vitae ipsum. Nullam ullamcorper lectus ut lorem egestas tempus.</li>\r\n	<li>Pellentesque blandit ut sapien ut tempus. In porta ultrices mauris quis maximus. Phasellus et diam mauris. Curabitur dui libero, ornare eu eros a, euismod ultricies lectus. Integer luctus nulla lorem, vitae viverra metus eleifend non. In vehicula elementum enim. Ut sed turpis vel ante elementum maximus. Donec ut nulla feugiat, ultricies ex vitae, accumsan justo. Vivamus diam mi, imperdiet non hendrerit eu, mattis vitae ipsum. Nullam ullamcorper lectus ut lorem egestas tempus.</li>\r\n</ul>\r\n\r\n<ol>\r\n	<li>Vestibulum id viverra sapien. Fusce porttitor in elit ac congue. Aliquam efficitur magna nec nunc tristique, vel pulvinar ligula gravida.</li>\r\n	<li>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean id nulla facilisis, eleifend ipsum vel, scelerisque enim.</li>\r\n	<li>Nullam tristique ornare mi quis cursus. Vivamus sagittis ligula eu ex consectetur egestas. Aenean euismod mi finibus massa pharetra, vel pellentesque orci rutrum.</li>\r\n	<li>Maecenas eu justo ligula. Cras maximus eu urna sed mollis. Sed bibendum, mi vitae dapibus ornare, sem magna sodales enim, convallis bibendum diam nibh vitae mi.</li>\r\n	<li>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Etiam accumsan metus ligula, et finibus magna laoreet in.</li>\r\n	<li>Aliquam ut interdum orci, at tincidunt nunc. Integer at eros ornare, imperdiet quam vitae, faucibus elit.</li>\r\n</ol>\r\n\r\n<p><em>Aenean erat erat, placerat consectetur consequat vitae, molestie at magna. Nulla blandit, lectus eu volutpat hendrerit, libero ex vehicula turpis, eget venenatis neque ante eu odio. Duis venenatis rutrum nunc vulputate ullamcorper. Integer a dui lacus. Integer sagittis et ex eu facilisis. Nullam vel orci mauris. In lectus mi, placerat at enim a, maximus consectetur diam. Aenean nec est ut urna maximus tincidunt. Sed porta velit quis risus aliquet dapibus. Duis ornare quam nec leo pulvinar accumsan. Pellentesque pharetra ex ut urna iaculis rutrum.</em></p>\r\n', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `politica`
--
ALTER TABLE `politica`
  ADD PRIMARY KEY (`id_politica`),
  ADD UNIQUE KEY `id_politica` (`id_politica`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `politica`
--
ALTER TABLE `politica`
  MODIFY `id_politica` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
