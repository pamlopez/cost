--84
INSERT INTO `cat_cat` (`id_cat`, `descripcion`, `usuario_ingreso`, `ip_ingreso`, `fh_ingreso`, `usuario_ultima_mod`, `ip_ultima_mod`, `fh_ultima_mod`)
VALUES (NULL, 'roles_participante', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `cat_item` (`id_item`, `id_cat`, `descripcion`, `orden`, `otro`, `predeterminado`)
VALUES (NULL, '84', 'tenderer', '1', NULL, NULL), (NULL, '84', 'procuringEntity', '2', NULL, NULL),
       (NULL, '84', 'supplier', '', NULL, NULL), (NULL, '84', 'publicAuthority', '', NULL, NULL),
       (NULL, '84', 'buyer', '', NULL, NULL);

UPDATE `cat_item` SET `otro` = '1' WHERE `cat_item`.`id_item` = 8201;
UPDATE `cat_item` SET `otro` = '2' WHERE `cat_item`.`id_item` = 8202;
UPDATE `cat_item` SET `otro` = '3' WHERE `cat_item`.`id_item` = 8203;
UPDATE `cat_item` SET `otro` = '4' WHERE `cat_item`.`id_item` = 8204;
UPDATE `cat_item` SET `otro` = '5' WHERE `cat_item`.`id_item` = 8205

--85
INSERT INTO `cat_cat` (`id_cat`, `descripcion`, `usuario_ingreso`, `ip_ingreso`, `fh_ingreso`, `usuario_ultima_mod`, `ip_ultima_mod`, `fh_ultima_mod`)
VALUES (NULL, 'Estado OC4IDS ProjectStatus ', NULL, NULL, NULL, NULL, NULL, NULL);

INSERT INTO `cat_item` (`id_item`, `id_cat`, `descripcion`, `orden`, `otro`, `predeterminado`)
VALUES (NULL, '85', 'identification', NULL, 'Identificación', NULL), (NULL, '85', 'preparation', NULL, 'Preparación', NULL),
        (NULL, '85', 'implementation', NULL, 'Implementación', NULL), (NULL, '85', 'completion', NULL, 'Completando', NULL),
        (NULL, '85', 'completed', NULL, 'Completado', NULL), (NULL, '85', 'cancelled', NULL, 'Cancelado', NULL);



UPDATE `cat_item` SET `otro` = 'Necesita valoración' WHERE `cat_item`.`id_item` = 8164;
UPDATE `cat_item` SET `otro` = 'Análisis de valor por dinero' WHERE `cat_item`.`id_item` = 8165;

INSERT INTO `cat_item` (`id_item`, `id_cat`, `descripcion`, `orden`, `otro`, `predeterminado`)
VALUES
(NULL, '76', 'technicalSpecifications', NULL, 'Especificaciones técnicas', NULL),
(NULL, '76', 'serviceDescription', NULL, 'Descripciones de servicios', NULL),
(NULL, '76', 'estimatedDemand', NULL, 'Demanda estimada', NULL),
(NULL, '76', 'contractDraft', NULL, 'Borrador de contrato', NULL),
(NULL, '76', 'contractSigned', NULL, 'Contrato firmado', NULL),
 (NULL, '76', 'feasibilityStudy', NULL, 'Estudio de factibilidad', NULL),
 (NULL, '76', 'environmentalImpact', NULL, 'Environmental Impact', NULL),
 (NULL, '76', 'finalAudit', NULL, 'Auditoría final', NULL),
 (NULL, '76', 'tenderNotice', NULL, 'Aviso de licitación', NULL),
 (NULL, '76', 'evaluationCommittee', NULL, 'Detalles del comité de evaluación', NULL),
 (NULL, '76', 'requestForQualification', NULL, 'Solicitud de calificación', NULL),
 (NULL, '76', 'evaluationCriteria', NULL, 'Criterios de evaluación', NULL),
 (NULL, '76', 'minutes', NULL, 'Minutos', NULL),
  (NULL, '76', 'shortlistedFirms', NULL, 'Empresas preseleccionadas', NULL),
  (NULL, '76', 'negotiationParameters', NULL, 'Parámetros de negociación', NULL),
  (NULL, '76', 'evaluationReports', NULL, 'Reporte de evaluacion', NULL),
  (NULL, '76', 'contractGuarantees', NULL, 'Garantías', NULL),
  (NULL, '76', 'defaultEvents', NULL, 'Defaults', NULL),
   (NULL, '76', 'termination', NULL, 'Terminación', NULL),
    (NULL, '76', 'performanceReport', NULL, 'Reporte de desempeño', NULL),
    (NULL, '76', 'plannedProcurementNotice', NULL, 'Aviso de contratación planificada', NULL),
    (NULL, '76', 'awardNotice', NULL, 'Aviso de adjudicación', NULL),
    (NULL, '76', 'contractNotice', NULL, 'Aviso de contrato', NULL)


INSERT INTO `cat_item` (`id_item`, `id_cat`, `descripcion`, `orden`, `otro`, `predeterminado`)
VALUES (NULL, '76', 'eligibilityCriteria', NULL, 'Criterio de elegibilidad', NULL),
 (NULL, '76', 'clarifications', NULL, 'Aclaraciones a las preguntas de los licitadores', NULL),
 (NULL, '76', 'assetAndLiabilityAssessment', NULL, 'Evaluación de los activos y pasivos del gobierno', NULL),
 (NULL, '76', 'riskProvisions', NULL, 'Provisiones para la gestión de riesgos y pasivos', NULL),
 (NULL, '76', 'winningBid', NULL, 'Puja ganadora', NULL), (NULL, '76', 'complaints', NULL, 'Quejas y decisiones', NULL),
 (NULL, '76', 'contractAnnexe', NULL, 'Anexos al contrato', NULL), (NULL, '76', 'subContract', NULL, 'Subcontratos', NULL),
 (NULL, '76', 'projectPlan', NULL, 'Plan de proyecto', NULL),
 (NULL, '76', 'billOfQuantity', NULL, 'Lista de cantidad', NULL),
 (NULL, '76', 'bidders', NULL, 'Información sobre licitadores', NULL),
 (NULL, '76', 'conflictOfInterest', NULL, 'Conflicto de intereses', NULL),
 (NULL, '76', 'debarments', NULL, 'Inhabilitaciones', NULL),
 (NULL, '76', 'illustration', NULL, 'Illustrations', NULL),
  (NULL, '76', 'submissionDocuments', NULL, 'Bid submission documents', NULL),
  (NULL, '76', 'contractSummary', NULL, 'Contract summary', NULL),
  (NULL, '76', 'cancellationDetails', NULL, 'Detalles de cancelación', NULL),
  (NULL, '76', 'projectScope', NULL, 'Alcance del proyecto', NULL),
  (NULL, '76', 'landAndSettlementImpact', NULL, 'Impacto sobre la tierra y los asentamientos', NULL),
  (NULL, '76', 'projectEvaluation', NULL, 'Project evaluation', NULL),
   (NULL, '76', 'budgetApproval', NULL, 'Aprobación de presupuesto', NULL)




   INSERT INTO `cat_item` (`id_item`, `id_cat`, `descripcion`, `orden`, `otro`, `predeterminado`)
   VALUES (NULL, '86', 'pre-award', NULL, 'Pre-premio', NULL),
   (NULL, '86', 'active', NULL, 'Activo', NULL), (NULL, '86', 'closed', NULL, 'Cerrado', NULL);