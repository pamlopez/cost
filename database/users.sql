-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2018 a las 17:50:27
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `backup_prod_1027`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_caimu` int(11) DEFAULT NULL,
  `id_persona` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `id_caimu`, `id_persona`) VALUES
(5, 'selmy', 'selmyfulgan@gmail.com', '$2y$10$KFtHM4OlwH9JwgvX1h81vuq4LilM6C3E80YJxiWrtAg.CCms7FcQe', 'Yz60PgWqbFmlC6uKVBGLPHkxJMqXlAJ1k6MDCV8qYemlEnLlVd2K1QlrdQyb', '2018-02-24 10:06:40', '2018-11-04 00:05:19', 177, NULL),
(13, 'Frida Alejandra Quintanilla González', 'informaticacaimuguate@gmail.com', '$2y$10$K2rJucwBnYUXKzQ6L9gSoumyR9lg1QB9kl35w5wGwTI1d8qvn5qx2', 'WIuD5KDYRvJaK8WJogSlBknk3hnMNezDUPhNPePeBadi3x8jo3vJAJkW3aWy', '2018-08-04 03:02:29', '2018-09-05 04:58:57', 177, NULL),
(14, 'Lisbeth Yohanna Rosal Flores', 'recepcion.ggm@gmail.com', '$2y$10$1j1i.sN2YZy13tJmHWOmfu8wmvXJc.aU3cGm/1i7nOAXNEc6ZTF1i', '8Tl3oiYpg1DNzF9C9pgpUmQk5NyvIWj5ev6JkMC6pFM1fplShAckxtbfTuKa', '2018-08-04 04:43:16', '2018-09-07 00:09:30', 177, NULL),
(15, 'Alma Patricia Diaz Gonzalez', 'asistenciacaimuggm@gmail.com', '$2y$10$wHoIoCgmlnXtyLXlx3HPGOIwmHNx.7ivMafiAH4F.sYb4Rv2Z6o/K', '8Bpn2g6NRo7KL6KFJcgUmhocdLCHpaFyOvdWsdQTzUCexJJZDXw2BxO8ScIt', '2018-08-04 04:59:24', '2018-10-23 20:18:17', 177, NULL),
(16, 'Ingrid Ivonne Giron de Leon', 'ggmcaimuguatemala@gmail.com', '$2y$10$8hXtGEidYqQEMmz1w9LMOO3imU3CDWufyPRAlkUWH634HOb/SPxPC', 'EmNHtDFkbyPqyvrmSuLkZ7vlrDBakrwFPXxTYuNKpCoAJfmFeVahUQYMttFe', '2018-08-04 05:01:16', '2018-10-27 00:49:57', 177, NULL),
(17, 'Wendy Karina Tobar Taks', 'asesorialegalggm@gmail.com', '$2y$10$gX24i6Z2depproxnaB2moOtCPx5HFi8vyMyDrDR.Cfr1qjMcUE3HG', 'c12xhLyN67LCXMTSI3Ajw3bmsIo02gXxP4YNZeYhWtAUDbRg8bnwKKzI40st', '2018-08-08 05:19:26', '2018-10-13 03:50:08', 177, NULL),
(18, 'Dora Zarate', 'procuracioncaimuguate@gmail.com', '$2y$10$DD71lkde//h3hFQGL9DK6.I0enEb5A55J.1ZnwD4hqgCUIKivf02e', 'DGVadRzzGu6Uy6cYZLvVKgMwwQWWV1TpOpkMoFi70VjoxYobGBJqHdnfqzYw', '2018-08-11 03:49:47', '2018-10-11 05:02:13', 177, NULL),
(19, 'Norma Con', 'penalcaimu@gmail.com', '$2y$10$Tqm.WzQbEccyu/KYmIIT4erFS4/quW0ZPFww9CddCz8aTUC8YhMmm', 'F7BiSnuX2rqJKsHznVh6aaPJ1IoDQA1wrDOtabFoxL5rwXT0ffm7Qp9l7Hv8', '2018-08-11 03:54:03', '2018-08-17 20:49:33', 177, NULL),
(20, 'Marisa Batres', 'caimulegal@gmail.com', '$2y$10$jqB9wcvPDWLLpOPVfjTCB.7/I9ZaOXiNpwyn/IJDgsKe0RcSqdR.q', 'F2qgbgOE4hcwH316SrP1qJGwCeV3fwks7GMCoCnhfqgZq1cFMXgoLl9zDqLy', '2018-08-11 03:55:51', '2018-10-25 05:13:28', 177, NULL),
(21, 'Esperanza Batres', 'asistencialegalggm@gmail.com', '$2y$10$5mPB5yMh3PLhHZuXIhcrVuDtb6e3VepJfNnLXHUwqpm9JDAuk/ak2', 'S6OYK20LXzwqRU0AWAc58BakkVE3V6Uib7YPOaWdVOGylC3sOopSiL3xl04o', '2018-08-11 03:58:05', '2018-10-26 22:13:54', 177, NULL),
(22, 'Jessica Barrera', 'familialegalcaimu@gmail.com', '$2y$10$JklyIN4snJmg4jCIa8lq6Osv5rP64qgu3p4NapKa.aQFKmRDkQ3iu', 'yODqanf9xMP8GqoF2ngc2HKc84yYcqvZlbOFGG2jTasmBZzLc4Ywuxx877Fa', '2018-08-11 03:59:45', '2018-09-05 04:43:54', 177, NULL),
(24, 'Anayte Barrios Escobar', 'procuracioncaimu@gmail.com', '$2y$10$v4sXDbDLnTw0l14qWBOPH.smQS7cec.Cljj/UWlPL4ig6mf1Idvrm', 'zdeBkQmJv143Gg49McfpbZZxhvJJqqjQhErT4U6vg2ml6EbfUJWea6kIfBgg', '2018-08-13 21:14:58', '2018-10-26 03:54:16', 177, NULL),
(25, 'Emilsa Blanco Herrera', 'legalcaimuguate@gmail.com', '$2y$10$HhG5wluPnkJN3ieKGeEX1eBXhb12nY5e45od3iH4osoye2WMNEt0C', 'gOnj5XpFkmRY78XusgBKlUczRzXCqigQ4NwM7LB16pt3W4JGRW42Q0wHfYmE', '2018-08-14 22:51:01', '2018-10-27 03:17:54', 177, NULL),
(27, 'Mayra Dinora Gil Herrera', 'abogadaggm@gmail.com', '$2y$10$lO1gOduX8RRTKLX1K3PL0uGt9MlWA5j3wBwm.t7yT84Wb5sxC9JhO', 'VDbsRFCEU0EUe0Ia5JvL8ty9vynl0iA4zgASdRyNzW5zdvpBLy1MuXyr0bmI', '2018-09-04 21:06:29', '2018-10-16 20:40:23', 177, NULL),
(28, 'Paola Camo Ortega', 'psicopaolaggm@gmail.com', '$2y$10$I6yS3DpqDzsKPvaPJTA6POziuCuE5ZoOcJsC9ylu8HJ8UbZxTACfO', 'O2RTQGNJSiGiwMWXtrXKrCctRcNp5oobpkKxwPqTVOWEfgpwUkA152OABWFw', '2018-09-11 23:00:51', '2018-10-24 20:43:24', 177, NULL),
(29, 'Rosa Elvira Alvarado', 'coortrabajosocialggm@gmail.com', '$2y$10$jVACszeMmBjRJM/cwLmab.t6jXwIuZ0lwIXgJ0whoxB7Lc5hejVJ2', 'dvbF7jpwDIfR5KFNQJzeJxSx5iBkObdRtlSomAjMcZEPI3SWi2aDPN4zcspk', '2018-09-13 20:19:36', '2018-10-25 03:47:59', 177, NULL),
(30, 'Giovana Lemus', 'coordinacione@ggm.org.gt', '$2y$10$UEIsQwjI0dDkPj7GsXtM3eU7kV4v0ICJYP/eO94T1Pca93mXQiVJS', 'lOFr5utZJDdDs6hAsYYhEOOxHTlfL8h8iCcFnv7ClvzutcZ3OYZm07YTsN5Z', '2018-09-19 20:12:38', '2018-09-19 20:14:04', 177, NULL),
(31, 'admin', 'admin@gmail.com', '$2y$10$ZRGydvVIytQI.lj.a5v41uKYgzyPkbAcK2CZViQah/IjWslaI5yve', 'SFRbF37SQY1V7VTO5wLYrXEWk6CqrljUvN4MxogoEW3g9hv73wirpGiGTnCG', '2018-09-20 14:37:43', '2018-09-20 14:38:48', 177, NULL),
(33, 'psicologia', 'psicologia@gmail.com', '$2y$10$2s.lRCFwv9E26ydSpMMwleguse/uNI4qAurzAZO6g9S9jfIjhpjB6', '0W9ir7zc5QQ0SwnaUGTafjVqAikpDejIbIZ6aKuRr5eDdOygl2vKzDA2uwg5', '2018-10-23 03:56:35', '2018-10-23 04:07:59', 3, NULL),
(35, 'Heydi Lorena Coyote Ixén ', 'ggmheyditrabajosocial@gmail.com', '$2y$10$krTNyf2IQSOIbXv6j14lquvKcB0AJ2ghx91TX3FrRPIt3rOJWrFUO', '3esfa4zUYV2Z2JxBpTW96p1UrI0ynq94ZgqV551neKXfOhebhlt8xi6mkd68', '2018-10-23 21:54:29', '2018-10-27 03:51:03', 177, NULL),
(36, 'Lesly Yessenia De León y De León', 'leslytrabajosocial@gmail.com', '$2y$10$nGE7fOcdejxy7/zoHzdx7.x9URTjhCyhRrH4bYLSX14w/jkt4mRba', 'WjWaDZZ5Py81NRHAKxnVMKgiOa4uDzA44uH6MVoZpEcDnUHxBjJ9hpinhd48', '2018-10-23 22:01:58', '2018-10-25 04:39:42', 177, NULL),
(38, 'Rosa Maria Sicajau Garcia', 'admoncaimuguate@gmail.com', '$2y$10$ZpoNslAIcBrExCf6hgEX9ehqB3OjHUCjqoNqSanHyboMxv73E5BEG', '0PVgOSw8aWtcXv9mJZX8RFB6YLrLW2pNRZ8PRQe6QBajZ4UGy15DD3ozpYRi', '2018-10-24 03:30:46', '2018-10-27 03:41:06', 177, NULL),
(39, 'Noemí Solís Pérez de Matzar', 'psiconoeggm@gmail.com', '$2y$10$3ZESxVtm5WqLrKs.TLOHROX.opojx3z3GkqiEB/bM40/PSHZYR5TC', NULL, '2018-10-24 20:13:27', '2018-10-24 20:14:57', 177, NULL),
(40, 'Mildred Haydee Valdez Marroquín', 'psicomildredggm@gmail.com', '$2y$10$hMEUP1Bp/hD8CeLzsnACEOJpHhCNGoGvjfDR3hVwjKmEb8JyacN42', NULL, '2018-10-24 20:16:06', '2018-10-24 20:16:27', 177, NULL),
(41, 'Aida Saravia', 'ggmprogramaapoyoyseguridad@gmail.com', '$2y$10$qZSgTy6joU3s4EZj9h/OOe4Mg0PumTA1bsII2M/3JVQ./CRks8WFm', NULL, '2018-10-25 06:24:28', '2018-10-25 06:28:48', 177, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
