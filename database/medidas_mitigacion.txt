                            <div class="info-box">
                                <span class="info-box-icon bg-green-gradient"><i class="fa fa-leaf"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text"><b>Medidas de mitigación ambiental</b></span>
                                    <span class="info-box-text ">
                                            <div class="row">
                                                 <div class="col-md-4">
                                                  1.  Acuerdos de Financiamiento
                                                </div>
                                                 <div class="col-md-2">
                                                     @if(Auth::check())
                                                         @php($af=\App\Models\cost_acuerdo_financiamiento::acuerdo_f($costProyecto->id,4))
                                                         @if(count($af)==0)
                                                             <a href="{!! route('costAcuerdoFinanciamientos.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>4]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             {!! Form::open(['route' => ['costAcuerdoFinanciamientos.destroy', $af[0]], 'method' => 'delete']) !!}
                                                             <a href="{!! route('costAcuerdoFinanciamientos.show', [$af[0]]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                             <a href="{!! route('costAcuerdoFinanciamientos.edit', [$af[0]]) !!}"
                                                                class='btn btn-warning btn-xs'><i
                                                                         class="glyphicon glyphicon-edit"></i></a>
                                                             {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                             {!! Form::close() !!}
                                                         @endif

                                                     @endif
                                                 </div>
                                                  <div class="col-md-4">
                                                  4.  Actas
                                                </div>
                                                 <div class="col-md-2">
                                                       @if(Auth::check())
                                                         @php($a=\App\Models\cost_acta::acta_t($costProyecto->id,4))

                                                         @if(count($a)==0)
                                                             <a href="{!! route('costActas.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>4]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             <a href="{!! route('costActas.index', ['id_proyecto'=>$costProyecto->id,'tipo'=>4]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                         @endif
                                                     @endif
                                                 </div>
                                            </div>
                                             <div class="row">
                                                 <div class="col-md-4">
                                                  2. Información del Contrato
                                                </div>
                                                 <div class="col-md-2">
                                                     @if(Auth::check())
                                                         @php($ic=\App\Models\cost_info_contrato::info_c($costProyecto->id,4))
                                                         @if(count($ic)==0)
                                                             <a href="{!! route('costInfoContratos.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>4]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             {!! Form::open(['route' => ['costInfoContratos.destroy', $ic[0]], 'method' => 'delete']) !!}
                                                             <a href="{!! route('costInfoContratos.show', [$ic[0]]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                             <a href="{!! route('costInfoContratos.edit', [$ic[0]]) !!}"
                                                                class='btn btn-warning btn-xs'><i
                                                                         class="glyphicon glyphicon-edit"></i></a>
                                                             {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                             {!! Form::close() !!}
                                                         @endif
                                                     @endif
                                                 </div>
                                                <div class="col-md-4">
                                                  5.  Fianzas
                                                </div>
                                                 <div class="col-md-2">
                                                    @if(Auth::check())
                                                         @php($f= \App\Models\cost_fianza::fianza_p($costProyecto->id,4))
                                                         @if(count($f)==0)
                                                             <a href="{!! route('costFianzas.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>4]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             <a href="{!! route('costFianzas.index', ['id_proyecto'=>$costProyecto->id,'tipo'=>4]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                         @endif
                                                     @endif
                                                 </div>

                                            </div>
                                             <div class="row">
                                                <div class="col-md-4">
                                                  3.  Pagos Efectuados
                                                </div>
                                                 <div class="col-md-2">
                                                       @if(Auth::check())
                                                         @php($pe=\App\Models\cost_pago_efectuado::pago_e($costProyecto->id,4))

                                                         @if(count($pe)==0)
                                                             <a href="{!! route('costPagoEfectuados.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>4]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$costProyecto->id,'tipo'=>4]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                         @endif
                                                     @endif
                                                 </div>
                                                 <div class="col-md-4">
                                                  6.  Ampliación del Plazo de Ejecución
                                                </div>
                                                 <div class="col-md-2">
                                                      @if(Auth::check())
                                                         @php($amp=\App\Models\cost_ampliacion::ampliacion_p($costProyecto->id,4))
                                                         @if(count($amp)==0)
                                                             <a href="{!! route('costAmpliacions.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>4]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             <a href="{!! route('costAmpliacions.index', ['id_proyecto'=>$costProyecto->id,'tipo'=>4]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                         @endif
                                                     @endif
                                                 </div>
                                            </div>
                                    </span>
                                </div>
                            </div>
                            <div class="info-box">
                                <span class="info-box-icon bg-navy-gradient"><i class="fa fa-exclamation-triangle"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text"><b>Medidas de mitigación de riesgo</b></span>
                                    <span class="info-box-text ">
                                            <div class="row">
                                                 <div class="col-md-4">
                                                  1.  Acuerdos de Financiamiento
                                                </div>
                                                 <div class="col-md-2">
                                                     @if(Auth::check())
                                                         @php($af=\App\Models\cost_acuerdo_financiamiento::acuerdo_f($costProyecto->id,5))
                                                         @if(count($af)==0)
                                                             <a href="{!! route('costAcuerdoFinanciamientos.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>5]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             {!! Form::open(['route' => ['costAcuerdoFinanciamientos.destroy', $af[0]], 'method' => 'delete']) !!}
                                                             <a href="{!! route('costAcuerdoFinanciamientos.show', [$af[0]]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                             <a href="{!! route('costAcuerdoFinanciamientos.edit', [$af[0]]) !!}"
                                                                class='btn btn-warning btn-xs'><i
                                                                         class="glyphicon glyphicon-edit"></i></a>
                                                             {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                             {!! Form::close() !!}
                                                         @endif

                                                     @endif
                                                 </div>
                                                  <div class="col-md-4">
                                                  4.  Actas
                                                </div>
                                                 <div class="col-md-2">
                                                       @if(Auth::check())
                                                         @php($a=\App\Models\cost_acta::acta_t($costProyecto->id,5))

                                                         @if(count($a)==0)
                                                             <a href="{!! route('costActas.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>5]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             <a href="{!! route('costActas.index', ['id_proyecto'=>$costProyecto->id,'tipo'=>5]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                         @endif
                                                     @endif
                                                 </div>
                                            </div>
                                             <div class="row">
                                                 <div class="col-md-4">
                                                  2. Información del Contrato
                                                </div>
                                                 <div class="col-md-2">
                                                     @if(Auth::check())
                                                         @php($ic=\App\Models\cost_info_contrato::info_c($costProyecto->id,5))
                                                         @if(count($ic)==0)
                                                             <a href="{!! route('costInfoContratos.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>5]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             {!! Form::open(['route' => ['costInfoContratos.destroy', $ic[0]], 'method' => 'delete']) !!}
                                                             <a href="{!! route('costInfoContratos.show', [$ic[0]]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                             <a href="{!! route('costInfoContratos.edit', [$ic[0]]) !!}"
                                                                class='btn btn-warning btn-xs'><i
                                                                         class="glyphicon glyphicon-edit"></i></a>
                                                             {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                             {!! Form::close() !!}
                                                         @endif
                                                     @endif
                                                 </div>
                                                <div class="col-md-4">
                                                  5.  Fianzas
                                                </div>
                                                 <div class="col-md-2">
                                                    @if(Auth::check())
                                                         @php($f= \App\Models\cost_fianza::fianza_p($costProyecto->id,5))
                                                         @if(count($f)==0)
                                                             <a href="{!! route('costFianzas.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>5]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             <a href="{!! route('costFianzas.index', ['id_proyecto'=>$costProyecto->id,'tipo'=>5]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                         @endif
                                                     @endif
                                                 </div>

                                            </div>
                                             <div class="row">
                                                <div class="col-md-4">
                                                  3.  Pagos Efectuados
                                                </div>
                                                 <div class="col-md-2">
                                                       @if(Auth::check())
                                                         @php($pe=\App\Models\cost_pago_efectuado::pago_e($costProyecto->id,5))

                                                         @if(count($pe)==0)
                                                             <a href="{!! route('costPagoEfectuados.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>5]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$costProyecto->id,'tipo'=>5]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                         @endif
                                                     @endif
                                                 </div>
                                                 <div class="col-md-4">
                                                  6.  Ampliación del Plazo de Ejecución
                                                </div>
                                                 <div class="col-md-2">
                                                      @if(Auth::check())
                                                         @php($amp=\App\Models\cost_ampliacion::ampliacion_p($costProyecto->id,5))
                                                         @if(count($amp)==0)
                                                             <a href="{!! route('costAmpliacions.create', ['id_proyecto'=>$costProyecto->id,'tipo'=>5]) !!}"
                                                                class='btn btn-success btn-xs'><i
                                                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                                                         @else
                                                             <a href="{!! route('costAmpliacions.index', ['id_proyecto'=>$costProyecto->id,'tipo'=>5]) !!}"
                                                                class='btn btn-info btn-xs'><i
                                                                         class="glyphicon glyphicon-eye-open"></i></a>
                                                         @endif
                                                     @endif
                                                 </div>
                                            </div>
                                    </span>
                                </div>
                            </div>