-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2018 a las 18:14:06
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `backup_prod_1027`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_asignacion`
--

CREATE TABLE `users_asignacion` (
  `id_users_asignacion` int(11) NOT NULL,
  `id_users` int(11) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `modifica_informacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users_asignacion`
--

INSERT INTO `users_asignacion` (`id_users_asignacion`, `id_users`, `id_rol`, `modifica_informacion`) VALUES
(1, 5, 1, NULL),
(2, 5, 2, NULL),
(3, 5, 3, NULL),
(4, 5, 4, NULL),
(5, 5, 5, NULL),
(6, 5, 6, NULL),
(7, 5, 7, NULL),
(8, 5, 8, NULL),
(9, 5, 9, NULL),
(10, 12, 1, NULL),
(11, 12, 2, NULL),
(12, 12, 3, NULL),
(13, 12, 4, NULL),
(14, 12, 5, NULL),
(15, 12, 6, NULL),
(16, 12, 7, NULL),
(17, 12, 8, NULL),
(18, 12, 9, NULL),
(19, 13, 1, NULL),
(20, 13, 2, NULL),
(21, 13, 3, NULL),
(22, 13, 4, NULL),
(23, 13, 5, NULL),
(24, 13, 6, NULL),
(25, 13, 7, NULL),
(26, 13, 8, NULL),
(27, 13, 9, NULL),
(28, 14, 8, NULL),
(29, 15, 8, NULL),
(30, 16, 7, NULL),
(31, 17, 2, NULL),
(32, 17, 3, NULL),
(33, 17, 7, NULL),
(34, 17, 9, NULL),
(35, 17, 1, NULL),
(36, 18, 2, NULL),
(37, 18, 3, NULL),
(38, 18, 9, NULL),
(39, 19, 2, NULL),
(40, 19, 3, NULL),
(41, 19, 9, NULL),
(42, 20, 2, NULL),
(43, 20, 3, NULL),
(44, 20, 9, NULL),
(45, 21, 9, NULL),
(46, 22, 2, NULL),
(47, 22, 3, NULL),
(48, 22, 9, NULL),
(49, 23, 2, NULL),
(50, 23, 3, NULL),
(51, 23, 9, NULL),
(52, 24, 2, NULL),
(53, 24, 9, NULL),
(54, 21, 2, NULL),
(55, 21, 3, NULL),
(57, 21, 7, NULL),
(58, 25, 2, NULL),
(59, 25, 3, NULL),
(60, 25, 9, NULL),
(61, 21, 7, NULL),
(62, 14, 2, NULL),
(63, 25, 7, NULL),
(64, 16, 1, NULL),
(65, 16, 2, NULL),
(66, 16, 3, NULL),
(67, 16, 4, NULL),
(68, 16, 5, NULL),
(69, 16, 6, NULL),
(70, 16, 8, NULL),
(71, 16, 9, NULL),
(73, 31, 1, NULL),
(74, 31, 2, NULL),
(75, 31, 3, NULL),
(76, 31, 4, NULL),
(77, 31, 5, NULL),
(78, 31, 6, NULL),
(79, 31, 7, NULL),
(80, 31, 8, NULL),
(81, 27, 2, NULL),
(82, 27, 3, NULL),
(83, 27, 9, NULL),
(84, 24, 2, NULL),
(85, 24, 3, NULL),
(86, 24, 7, NULL),
(87, 24, 9, NULL),
(88, 18, 2, NULL),
(89, 18, 3, NULL),
(90, 18, 7, NULL),
(91, 18, 9, NULL),
(92, 27, 2, NULL),
(93, 27, 3, NULL),
(94, 27, 7, NULL),
(95, 27, 9, NULL),
(96, 32, 1, NULL),
(97, 32, 2, NULL),
(98, 32, 3, NULL),
(99, 32, 4, NULL),
(100, 32, 5, NULL),
(101, 32, 6, NULL),
(102, 32, 7, NULL),
(103, 32, 8, NULL),
(104, 32, 9, NULL),
(106, 33, 4, NULL),
(107, 29, 5, NULL),
(109, 34, 7, NULL),
(110, 34, 8, NULL),
(115, 36, 5, NULL),
(116, 38, 7, NULL),
(117, 38, 8, NULL),
(121, 40, 6, NULL),
(122, 40, 4, NULL),
(123, 40, 7, NULL),
(124, 28, 6, NULL),
(125, 28, 4, NULL),
(126, 28, 7, NULL),
(127, 35, 6, NULL),
(128, 35, 2, NULL),
(129, 35, 4, NULL),
(130, 35, 5, NULL),
(149, 41, 1, NULL),
(150, 41, 6, NULL),
(151, 41, 3, NULL),
(152, 41, 2, NULL),
(153, 41, 4, NULL),
(154, 41, 5, NULL),
(155, 41, 9, NULL),
(156, 41, 7, NULL),
(157, 41, 8, NULL),
(162, 39, 6, NULL),
(163, 39, 4, NULL),
(164, 39, 7, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users_asignacion`
--
ALTER TABLE `users_asignacion`
  ADD PRIMARY KEY (`id_users_asignacion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users_asignacion`
--
ALTER TABLE `users_asignacion`
  MODIFY `id_users_asignacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
