ALTER TABLE `cost_proyecto_dis_plan` ADD `resultado_principal_impacto` TEXT NULL AFTER `aprobacion_viabilidad`;
ALTER TABLE `cost_proyecto_dis_plan` ADD `observaciones_marn` TEXT NULL AFTER `resultado_principal_impacto`, ADD `links_marn` TEXT NULL AFTER `observaciones_marn`;

ALTER TABLE `cost_proyecto_dis_plan` ADD `observaciones_agrip` TEXT NULL AFTER `links_marn`, ADD `links_agrip` TEXT NULL AFTER `observaciones_agrip`;

ALTER TABLE `cost_proyecto_dis_plan` ADD `observaciones_suelo` TEXT NULL AFTER `links_agrip`, ADD `links_suelo` TEXT NULL AFTER `observaciones_suelo`;

ALTER TABLE `cost_proyecto_dis_plan` ADD `observaciones_topografia` TEXT NULL AFTER `links_suelo`, ADD `links_topografia` TEXT NULL AFTER `observaciones_topografia`;

#segundos alters

ALTER TABLE `cost_proyecto_dis_plan` ADD `observaciones_geologico` TEXT NULL AFTER `observaciones_topografia`, ADD `links_geologico` TEXT NULL AFTER `observaciones_geologico`;

ALTER TABLE `cost_proyecto_dis_plan` ADD `estudio_hidrogeologico` INT NULL AFTER `links_topografia`, ADD `fecha_hidrogeologico` DATE NULL AFTER `estudio_hidrogeologico`, ADD `hecho_por_hidrogeologico_` VARCHAR(250) NULL AFTER `fecha_hidrogeologico`, ADD `observaciones_higrogeoogico` TEXT NULL AFTER `hecho_por_hidrogeologico_`, ADD `links_higrogeoogico` TEXT NULL AFTER `observaciones_higrogeoogico`;
#estos alters soon por me equivoue en un nombre =)
ALTER TABLE `cost_proyecto_dis_plan` CHANGE `hecho_por_hidrogeologico_` `hecho_por_hidrogeologico` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL;
ALTER TABLE `cost_proyecto_dis_plan` CHANGE `observaciones_higrogeoogico` `observaciones_hidrogeologico` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL;
ALTER TABLE `cost_proyecto_dis_plan` CHANGE `links_higrogeoogico` `links_hidrogeologico` TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL;

ALTER TABLE `cost_proyecto_dis_plan` ADD `observaciones_estructural` TEXT NULL AFTER `links_hidrogeologico`, ADD `links_estructural` TEXT NULL AFTER `observaciones_estructural`;
ALTER TABLE `cost_proyecto_dis_plan` ADD `memoria_calculo_f` DATE NULL AFTER `links_estructural`, ADD `memoria_calculo_hecho_por` TEXT NULL AFTER `memoria_calculo_f`, ADD `observaciones_memoria_calculo` TEXT NULL AFTER `memoria_calculo_hecho_por`, ADD `liks_memoria_calculo` TEXT NULL AFTER `observaciones_memoria_calculo`;
ALTER TABLE `cost_proyecto_dis_plan` ADD `plano_completo_f` DATE NULL AFTER `liks_memoria_calculo`;
ALTER TABLE `cost_proyecto_dis_plan` ADD `observaciones_plano_completo` TEXT NULL AFTER `plano_completo_f`, ADD `liks_plano_completo` TEXT NULL AFTER `observaciones_plano_completo`;
ALTER TABLE `cost_proyecto_dis_plan` ADD `espe_general_f` DATE NULL AFTER `liks_memoria_calculo`, ADD `observaciones_espe_general` TEXT NULL AFTER `espe_general_f`, ADD `links_espe_general` TEXT NULL AFTER `observaciones_espe_general`;

ALTER TABLE `cost_proyecto_dis_plan` ADD `espe_tecnica_f` TEXT NULL AFTER `liks_plano_completo`, ADD `links_espe_tecnica` TEXT NULL AFTER `espe_tecnica_f`, ADD `observaciones_espe_tecnica` TEXT NULL AFTER `links_espe_tecnica`;
ALTER TABLE `cost_proyecto_dis_plan` ADD `observaciones_marn_dispo_especial` TEXT NULL AFTER `observaciones_espe_tecnica`, ADD `links_dispo_especial` TEXT NULL AFTER `observaciones_marn_dispo_especial`;
ALTER TABLE `cost_proyecto_dis_plan` ADD `otro_f` DATE NULL AFTER `links_dispo_especial`, ADD `links_otro` TEXT NULL AFTER `otro_f`, ADD `observaciones_otro` TEXT NULL AFTER `links_otro`;


--  QUERYS PARA ELIMINAR LO QUE NO SE USARÁ
	DROP TABLE agenda
	DROP TABLE control_asistencia
	DROP TABLE proceso_detalle_demanda_penal
	DROP TABLE proceso_detalle_demanda_familia
	DROP TABLE proceso
	DROP TABLE asesoria_proceso
	DROP TABLE asesoria_llamada
	DROP TABLE atencion_inicial_tipo_violencia
	DROP TABLE atencion_inicial_referencia_caimu
	DROP TABLE atencion_inicial
	DROP TABLE asesoria
	TRUNCATE atencion_inicial 
	DROP TABLE atencion_inicial

-- FIN


ALTER TABLE `cost_proyecto_dis_plan` ADD `dispo_especial_f` DATE NOT NULL AFTER `observaciones_espe_tecnica`;