-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2018 a las 18:14:32
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `backup_prod_1027`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_rol`
--

CREATE TABLE `users_rol` (
  `id_users_rol` int(11) NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `otro` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users_rol`
--

INSERT INTO `users_rol` (`id_users_rol`, `descripcion`, `otro`) VALUES
(1, 'Administrador', NULL),
(2, 'Área legal', NULL),
(3, 'Área familia', NULL),
(4, 'Área psicológica', NULL),
(5, 'Área trabajo social', NULL),
(6, 'Albergue', NULL),
(7, 'Atención inicial', NULL),
(8, 'Control asistencia', NULL),
(9, 'Asesoria', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `users_rol`
--
ALTER TABLE `users_rol`
  ADD PRIMARY KEY (`id_users_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users_rol`
--
ALTER TABLE `users_rol`
  MODIFY `id_users_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
