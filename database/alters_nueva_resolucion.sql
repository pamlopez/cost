ALTER TABLE `cost_proyecto` ADD `analisis_alternativas` TEXT NOT NULL AFTER `operador_portal`, 
ADD `descripcion` TEXT NOT NULL AFTER `analisis_alternativas`, 
ADD `presupuesto_proyecto` INT NOT NULL AFTER `descripcion`,
ADD `nog` INT NOT NULL AFTER `presupuesto_proyecto` ,
ADD `nog_f` DATE NOT NULL AFTER `nog`,
ADD `avance_fisico` FLOAT NOT NULL AFTER `nog_f`,
ADD `avance_financiero` FLOAT NOT NULL AFTER `avance_fisico`,
ADD `estado_actual_proyecto` INT NOT NULL AFTER `avance_financiero` 
ADD `estado_actual_proyecto_d` INT NOT NULL AFTER `estado_actual_proyecto`;

ALTER TABLE `cost_proyecto_dis_plan` ADD `terminos_referencia` VARCHAR(500) NOT NULL AFTER `dispo_especial_f`;
ALTER TABLE `cost_proyecto_dis_plan` ADD `links_plano_completo` TEXT NOT NULL AFTER `terminos_referencia`;
ALTER TABLE `cost_proyecto_dis_plan` CHANGE `espe_tecnica_f` `espe_tecnica_f` DATE NULL DEFAULT NULL;


ALTER TABLE `cost_proyecto_dis_plan` ADD `inst_ea` INT NOT NULL AFTER `links_plano_completo`, ADD `inst_ea_resoponsable` VARCHAR(250) NOT NULL AFTER `inst_ea`, ADD `inst_ea_d` DATE NOT NULL AFTER `inst_ea_resoponsable`, ADD `links_inst_ea` TEXT NOT NULL AFTER `inst_ea_d`;

ALTER TABLE `cost_proyecto_dis_plan` ADD `links_presupuesto` TEXT NOT NULL AFTER `links_inst_ea`;

ALTER TABLE `cost_proyecto_dis_plan` ADD `resp_diseno` TEXT NOT NULL AFTER `links_presupuesto`, ADD `a_estudio_facti` TEXT NOT NULL AFTER `resp_diseno`, ADD `a_entidades` TEXT NOT NULL AFTER `a_estudio_facti`, ADD `a_derecho_paso` TEXT NOT NULL AFTER `a_entidades`;

ALTER TABLE `cost_proyecto` ADD `f_r_proactiva` DATE NULL AFTER `estado_actual_proyecto_d`, ADD `f_r_reactiva` DATE NULL AFTER `f_r_proactiva`;