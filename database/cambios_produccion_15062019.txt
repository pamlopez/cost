
ALTER TABLE `participante` ADD `localidad` VARCHAR(250) NULL AFTER `telefono`, ADD `region` VARCHAR(250) NULL AFTER `localidad`, ADD `nombre_pais` VARCHAR(250) NULL AFTER `region`, ADD `punto_contacto` VARCHAR(250) NULL AFTER `nombre_pais`, ADD `numero_fax` VARCHAR(100) NULL AFTER `punto_contacto`;

CREATE TABLE `prod_sept26`.`articulo` ( `id_articulo` SERIAL NOT NULL AUTO_INCREMENT , `id_proyecto` INT NOT NULL , `id_fase` INT NOT NULL , `descripcion` VARCHAR(250) NULL , `cantidad` FLOAT NULL , `observacion` TEXT NULL , PRIMARY KEY (`id_articulo`)) ENGINE = InnoDB;


CREATE TABLE `prod_sept26`.`cost_licitacion` ( `id_licitacion` SERIAL NOT NULL AUTO_INCREMENT , `id_proyecto` INT NOT NULL , `titulo` VARCHAR(250) NULL , `descripcion` TEXT NULL , `estado_licitacion` VARCHAR(250) NULL , `metodo_licitacion` VARCHAR(250) NULL , `metodo_licitacion_justificacion` TEXT NULL , `periodo` VARCHAR(250) NULL , `fh_incio` DATETIME NULL , `fh_fin` DATETIME NULL , PRIMARY KEY (`id_licitacion`)) ENGINE = InnoDB;

CREATE TABLE `prod_sept26`.`documento` ( `id_documento` SERIAL NOT NULL AUTO_INCREMENT , `id_proyecto` INT NOT NULL , `id_fase` INT NOT NULL , `tipo` INT NOT NULL , `url` VARCHAR(500) NULL , `documento` VARCHAR(250) NULL , `titulo` VARCHAR(500) NULL , PRIMARY KEY (`id_documento`)) ENGINE = InnoDB;


ALTER TABLE `documento` ADD `fh_publicacion` DATETIME NULL AFTER `titulo`, ADD `fh_modificacion` DATETIME NULL AFTER `fh_publicacion`, ADD `formato` VARCHAR(250) NULL AFTER `fh_modificacion`, ADD `lenguaje` VARCHAR(250) NULL AFTER `formato`;

ALTER TABLE `cost_proyecto` ADD `estado_oc` INT NULL AFTER `estado_actual_proyecto_d`;
ALTER TABLE `cost_proyecto` ADD `estado_oc` INT NULL AFTER `estado_actual_proyecto_d`;
ALTER TABLE `cost_proyecto` ADD `estado_oc_d` VARCHAR(250) NULL AFTER `estado_oc`;


ALTER TABLE `cost_proyecto` ADD `sector_oc` VARCHAR(250) NULL AFTER `sub_sector`, ADD `subsector_oc` VARCHAR(250) NULL AFTER `sector_oc`, ADD `tipo_oc` VARCHAR(250) NULL AFTER `subsector_oc`;


ALTER TABLE `cost_proyecto` ADD `f_finalizacion` DATE NULL AFTER `f_aprobacion`;

ALTER TABLE `cost_proyecto` ADD `vida_activo` int NULL AFTER `f_r_reactiva`;


ALTER TABLE `cost_proyecto` ADD `localidad` VARCHAR(250) NOT NULL AFTER `localizacion_depto`, ADD `codigo_postal` VARCHAR(100) NOT NULL AFTER `localidad`, ADD `pais` VARCHAR(100) NOT NULL AFTER `codigo_postal`;

ALTER TABLE `cost_proyecto_dis_plan` ADD `presupuesto_moneda` VARCHAR(50) NULL AFTER `presupuesto_proyecto`;

ALTER TABLE `participante` ADD `schema` INT NULL AFTER `id_participante`, ADD `id_schema` INT NULL AFTER `schema`;

ALTER TABLE `participante` ADD `url` VARCHAR(1000) NOT NULL AFTER `numero_fax`;

ALTER TABLE `documento` ADD `pagina_inicio` INT NULL AFTER `lenguaje`, ADD `pagina_fin` INT NULL AFTER `pagina_inicio`, ADD `detalle_acceso` TEXT NULL AFTER `pagina_fin`, ADD `autor` VARCHAR(250) NULL AFTER `detalle_acceso`;

ALTER TABLE `cost_liquidacion_monto` ADD `moneda` VARCHAR(50) NULL AFTER `no_fianza`;


ALTER TABLE `cost_liquidacion` ADD `f_finalizacion` DATE NULL AFTER `informe_evaluacion`, ADD `f_finalizacion_detalle` TEXT NULL AFTER `f_finalizacion`, ADD `monto_final` DOUBLE NULL AFTER `f_finalizacion_detalle`, ADD `moneda` VARCHAR(250) NULL AFTER `monto_final`, ADD `monto_final_detalle` TEXT NULL AFTER `moneda`, ADD `alcance_final` TEXT NULL AFTER `monto_final_detalle`, ADD `alcance_final_detalle` TEXT NULL AFTER `alcance_final`;





ALTER TABLE `cost_info_contrato` ADD `estado_contrato_oc` INT NULL AFTER `estado_contrato`;
ALTER TABLE `cost_info_contrato` ADD `estado_contrato_oc_d` VARCHAR(250) NULL AFTER `estado_contrato_oc`;

ALTER TABLE `cost_licitacion` ADD `id_fase` INT NULL AFTER `id_proyecto`;
ALTER TABLE `cost_info_contrato` ADD `moneda` VARCHAR(50) NULL AFTER `precio_contrato`;


ALTER TABLE `cost_info_contrato` ADD `precio_contrato_final` DOUBLE NULL AFTER `moneda`, ADD `moneda_final` VARCHAR(50) NULL AFTER `precio_contrato_final`;

besthost_prod_sept26
CREATE TABLE `besthost_prod_sept26`.`ocds_entrega` ( `id_ocds_entrega` SERIAL NOT NULL , `id_entrega` VARCHAR(250) NULL , `tag` VARCHAR(250) NULL , `fecha_entrega` INT NULL , `id_proyecto` INT NULL , `id_fase` INT NULL , PRIMARY KEY (`id_ocds_entrega`)) ENGINE = InnoDB


ALTER TABLE `cost_liquidacion_monto` ADD `fecha_cambio` DATE NULL AFTER `moneda`;
ALTER TABLE `cost_liquidacion_tiempo` ADD `fecha_cambio` DATE NULL;
ALTER TABLE `cost_liquidacion_alcance` ADD `fecha_cambio` DATE NULL ;

CREATE VIEW 
proyecto_view AS 
SELECT 
id_proyecto as id, 
fh_ultima_actualizacion as updated
nombre_proyecto as title,
descripcion as description,
estado_oc_d as status,
resumen as purpose,
sector_oc as sector,
tipo_oc as type
FROM cost_proyecto


CREATE VIEW 
proyecto_view_tiempo AS 
SELECT 
id_proyecto as id, 
f_aprobacion as startDate
f_finalizacion as endDate,
DATEDIFF(f_finalizacion,f_aprobacion) as  durationInDays
FROM cost_proyecto

CREATE VIEW 
proyecto_view_direccion AS 
SELECT 
id_proyecto as id, 
localizacion as description,
'point' as type,
concat(latitud,',',longitud) as  coordinates,
localizacion as streetAddress,
localidad as locality,
concat(localizacion_muni_d, ' ', localizacion_depto_d) as region,
codigo_postal as postalCode,
'Guatemala' as countryName
FROM cost_proyecto


CREATE VIEW 
presupuesto_view  AS 
SELECT 
id_proyecto as id,
presupuesto_proyecto as amount,
'GTQ' as  currency,
presupuesto_aprobacion_f as approvalDate
FROM `cost_proyecto_dis_plan` WHERE 1


CREATE VIEW
participante_view AS
SELECT
nombre_comun as name,
p.id_participante as id_participante,
p.id_participante as id,
direccion as StreetAddress,
localidad as locality,
region,
codigo_postal as postalCode,
nombre_pais as countryName
FROM `participante` as p

CREATE VIEW participante_schema_contact_view AS select `besthost_prod_sept26`.`participante`.`id_participante` AS `id_participante`,`besthost_prod_sept26`.`participante`.`schema` AS `scheme`,`besthost_prod_sept26`.`participante`.`id_schema` AS `id`,`besthost_prod_sept26`.`participante`.`nombre_legal` AS `legalName`,`besthost_prod_sept26`.`participante`.`uri` AS `uri`,`besthost_prod_sept26`.`participante`.`nombre_contacto` AS `name`,`besthost_prod_sept26`.`participante`.`email` AS `email`,`besthost_prod_sept26`.`participante`.`telefono` AS `telephone`,`besthost_prod_sept26`.`participante`.`numero_fax` AS `faxNumber`,`besthost_prod_sept26`.`participante`.`url` AS `url` from participante

ALTER TABLE `cost_oferente` ADD `rol` INT NULL DEFAULT '1' AFTER `oferta_presentada`, ADD INDEX `rol_idx` (`rol`);
CREATE VIEW
documento_view AS
SELECT 
id_proyecto,
id_fase,
id_documento as id,
tipo as documentType,
titulo as title,
documento as description,
url,
fh_publicacion as datePublished,
fh_modificacion as dateModified,
formato as format,
lenguaje as language,
pagina_inicio as pageStart,
pagina_fin as pageEnd,
detalle_acceso as accessDetails,
autor as author

FROM documento

CREATE VIEW
liquidacion_view AS
SELECT id_proyecto, monto_final as amount , moneda as currency, monto_final_detalle as finalValueDetails, alcance_final as  finalScope, alcance_final_detalle  as finalScopeDetails FROM `cost_liquidacion` 


ALTER TABLE `cost_info_contrato` ADD `metodo_procuracion` VARCHAR(150) NULL AFTER `oferentes`, ADD `detalle_procuracion` TEXT NULL AFTER `metodo_procuracion`, ADD `costo_estimado` DOUBLE NULL AFTER `detalle_procuracion`, ADD `costo_estimado_moneda` VARCHAR(100) NULL AFTER `costo_estimado`;


ALTER TABLE `cost_liquidacion_tiempo` ADD `antigua_fecha_finaliza` DATE NULL AFTER `nueva_fecha_finaliza`;
ALTER TABLE `cost_liquidacion_tiempo` ADD `id_fase` INT NULL DEFAULT '3' AFTER `id_proyecto`;
ALTER TABLE `cost_liquidacion_alcance` ADD `id_fase` INT NULL DEFAULT '3' AFTER `id_proyecto`;
ALTER TABLE `cost_liquidacion_tiempo` ADD `antigua_fecha_inicio` DATE NULL AFTER `antigua_fecha_finaliza`, ADD `nueva_fecha_inicio` DATE NULL AFTER `antigua_fecha_inicio`;

ALTER TABLE `cost_liquidacion_tiempo` ADD `cantidad_tiempo_modifica_inicio` INT NULL DEFAULT '0' AFTER `nueva_fecha_inicio`;

ALTER TABLE `cost_liquidacion_monto` ADD `id_fase` INT NULL DEFAULT '3' AFTER `id_proyecto`;

ALTER TABLE `cost_liquidacion_monto` ADD `monto_antiguo` DOUBLE NULL AFTER `fecha_cambio`, ADD `moneda_antigua` VARCHAR(50) NULL AFTER `monto_antiguo`;


YA---------------------------------------->











