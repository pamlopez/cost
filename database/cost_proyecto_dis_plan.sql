-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-03-2019 a las 19:24:10
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prod_sept26`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cost_proyecto_dis_plan`
--

CREATE TABLE `cost_proyecto_dis_plan` (
  `id_proyecto_dis_plan` bigint(20) UNSIGNED NOT NULL,
  `id_proyecto` int(11) NOT NULL,
  `dis_anuncio_invitacion` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_listado_oferentes` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `acta_adjudica` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_responsable` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_precio_contratado` double NOT NULL DEFAULT '0',
  `dis_plazo_contrato` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_tipo_contrato` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_afianzadora` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_fianza` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_f_inicio` date DEFAULT NULL,
  `dis_f_entrega` date DEFAULT NULL,
  `dis_responsable_ea` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_f_aprobacion_contrato_d` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_f_aprobacion_contrato` date DEFAULT NULL,
  `dis_anticipo` float NOT NULL DEFAULT '0',
  `dis_numero_pagos` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_fechas` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_mod_plazo` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_responsable_mod_plazo` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `dis_f_nueva_finaliza` date DEFAULT NULL,
  `dis_monto_modificado` float NOT NULL DEFAULT '0',
  `dis_monto_modificado_d` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_responsable_mod_monto` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_porcentaje_cambio` double NOT NULL DEFAULT '0',
  `dis_acta_recepcion` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_f_recepcion` date DEFAULT NULL,
  `dis_f_recepcion_d` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_f_finiquito` date DEFAULT NULL,
  `dis_fecha_finiquito_det` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_fianza_detalle` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dis_det_fianza` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `instrumento_eia` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `elaborado_por_eia` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `f_eia` date DEFAULT NULL,
  `resolucion_marn` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `f_resolucion_marn` date DEFAULT NULL,
  `medida_mitigacion_presupuesto` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `agrip` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `agrip_f` date DEFAULT NULL,
  `agrip_elaborado_por` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `agrip_medida_mitigacion` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `responsable_estudio_tecnico` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estudio_suelo` int(11) NOT NULL DEFAULT '0',
  `estudio_suelo_f` date DEFAULT NULL,
  `estudio_suelo_por` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estudio_topografia` int(11) DEFAULT '0',
  `estudio_topografia_f` date DEFAULT NULL,
  `estudio_topografia_por` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estudio_geologico` int(11) NOT NULL DEFAULT '0',
  `estudio_geologico_f` date DEFAULT NULL,
  `estudio_geologico_por` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `diseno_estructural` int(11) NOT NULL DEFAULT '0',
  `diseno_estructural_f` date DEFAULT NULL,
  `diseno_estructural_por` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `memoria_calculo` int(11) NOT NULL DEFAULT '0',
  `memoria_calculo_responsable` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `plano_completo` int(11) DEFAULT '0',
  `plano_completo_responsable` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `espe_tecnica` int(11) NOT NULL DEFAULT '0',
  `espe_tecnica_responsable` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `espe_general` int(11) NOT NULL DEFAULT '0',
  `espe_general_responsable` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `dispo_especial` int(11) NOT NULL DEFAULT '0',
  `dispo_especial_responsable` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `otro_estudio` int(11) DEFAULT '0',
  `otro_estudio_responsable` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `presupuesto_proyecto` float NOT NULL DEFAULT '0',
  `costo_estimado_proyecto` float DEFAULT '0',
  `presupuesto_aprobacion_f` date DEFAULT NULL,
  `estado_actual_proyecto_d` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado_actual_proyecto` int(11) NOT NULL DEFAULT '0',
  `avance_fisico` float NOT NULL DEFAULT '0',
  `avance_financiero` float NOT NULL DEFAULT '0',
  `modalidad_contratacion` int(11) NOT NULL DEFAULT '0',
  `modalidad_contratacion_d` text COLLATE utf8_spanish_ci,
  `fuente_financiamiento_dis` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `monto_asignado_dis` float NOT NULL DEFAULT '0',
  `nog` int(11) NOT NULL DEFAULT '0',
  `nog_f` date DEFAULT NULL,
  `fuente_f_sup` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `monto_sup` float DEFAULT '0',
  `fuente_f_ejecucion` text COLLATE utf8_spanish_ci,
  `monto_ejecucion` float DEFAULT '0',
  `fuente_f_ambiental` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `monto_ambiental` double NOT NULL DEFAULT '0',
  `fuente_riesgo` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `monto_riesgo` float NOT NULL DEFAULT '0',
  `estudio_viabilidad` varchar(500) COLLATE utf8_spanish_ci DEFAULT NULL,
  `aprobacion_viabilidad` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `resultado_principal_impacto` text COLLATE utf8_spanish_ci,
  `observaciones_marn` text COLLATE utf8_spanish_ci,
  `links_marn` text COLLATE utf8_spanish_ci,
  `observaciones_agrip` text COLLATE utf8_spanish_ci,
  `links_agrip` text COLLATE utf8_spanish_ci,
  `observaciones_suelo` text COLLATE utf8_spanish_ci,
  `links_suelo` text COLLATE utf8_spanish_ci,
  `observaciones_topografia` text COLLATE utf8_spanish_ci,
  `observaciones_geologico` text COLLATE utf8_spanish_ci,
  `links_geologico` text COLLATE utf8_spanish_ci,
  `links_topografia` text COLLATE utf8_spanish_ci,
  `estudio_hidrogeologico` int(11) DEFAULT NULL,
  `fecha_hidrogeologico` date DEFAULT NULL,
  `hecho_por_hidrogeologico` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `observaciones_hidrogeologico` text COLLATE utf8_spanish_ci,
  `links_hidrogeologico` text COLLATE utf8_spanish_ci,
  `observaciones_estructural` text COLLATE utf8_spanish_ci,
  `links_estructural` text COLLATE utf8_spanish_ci,
  `memoria_calculo_f` date DEFAULT NULL,
  `memoria_calculo_hecho_por` text COLLATE utf8_spanish_ci,
  `observaciones_memoria_calculo` text COLLATE utf8_spanish_ci,
  `liks_memoria_calculo` text COLLATE utf8_spanish_ci,
  `espe_general_f` date DEFAULT NULL,
  `observaciones_espe_general` text COLLATE utf8_spanish_ci,
  `links_espe_general` text COLLATE utf8_spanish_ci,
  `plano_completo_f` date DEFAULT NULL,
  `observaciones_plano_completo` text COLLATE utf8_spanish_ci,
  `liks_plano_completo` text COLLATE utf8_spanish_ci,
  `espe_tecnica_f` date DEFAULT NULL,
  `links_espe_tecnica` text COLLATE utf8_spanish_ci,
  `observaciones_espe_tecnica` text COLLATE utf8_spanish_ci,
  `dispo_especial_f` date NOT NULL,
  `terminos_referencia` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `links_plano_completo` text COLLATE utf8_spanish_ci NOT NULL,
  `inst_ea` int(11) NOT NULL,
  `inst_ea_resoponsable` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `inst_ea_d` date NOT NULL,
  `links_inst_ea` text COLLATE utf8_spanish_ci NOT NULL,
  `links_presupuesto` text COLLATE utf8_spanish_ci NOT NULL,
  `resp_diseno` text COLLATE utf8_spanish_ci NOT NULL,
  `a_estudio_facti` text COLLATE utf8_spanish_ci NOT NULL,
  `a_entidades` text COLLATE utf8_spanish_ci NOT NULL,
  `a_derecho_paso` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cost_proyecto_dis_plan`
--

INSERT INTO `cost_proyecto_dis_plan` (`id_proyecto_dis_plan`, `id_proyecto`, `dis_anuncio_invitacion`, `dis_listado_oferentes`, `acta_adjudica`, `dis_responsable`, `dis_precio_contratado`, `dis_plazo_contrato`, `dis_tipo_contrato`, `dis_afianzadora`, `dis_fianza`, `dis_f_inicio`, `dis_f_entrega`, `dis_responsable_ea`, `dis_f_aprobacion_contrato_d`, `dis_f_aprobacion_contrato`, `dis_anticipo`, `dis_numero_pagos`, `dis_fechas`, `dis_mod_plazo`, `dis_responsable_mod_plazo`, `dis_f_nueva_finaliza`, `dis_monto_modificado`, `dis_monto_modificado_d`, `dis_responsable_mod_monto`, `dis_porcentaje_cambio`, `dis_acta_recepcion`, `dis_f_recepcion`, `dis_f_recepcion_d`, `dis_f_finiquito`, `dis_fecha_finiquito_det`, `dis_fianza_detalle`, `dis_det_fianza`, `instrumento_eia`, `elaborado_por_eia`, `f_eia`, `resolucion_marn`, `f_resolucion_marn`, `medida_mitigacion_presupuesto`, `agrip`, `agrip_f`, `agrip_elaborado_por`, `agrip_medida_mitigacion`, `responsable_estudio_tecnico`, `estudio_suelo`, `estudio_suelo_f`, `estudio_suelo_por`, `estudio_topografia`, `estudio_topografia_f`, `estudio_topografia_por`, `estudio_geologico`, `estudio_geologico_f`, `estudio_geologico_por`, `diseno_estructural`, `diseno_estructural_f`, `diseno_estructural_por`, `memoria_calculo`, `memoria_calculo_responsable`, `plano_completo`, `plano_completo_responsable`, `espe_tecnica`, `espe_tecnica_responsable`, `espe_general`, `espe_general_responsable`, `dispo_especial`, `dispo_especial_responsable`, `otro_estudio`, `otro_estudio_responsable`, `presupuesto_proyecto`, `costo_estimado_proyecto`, `presupuesto_aprobacion_f`, `estado_actual_proyecto_d`, `estado_actual_proyecto`, `avance_fisico`, `avance_financiero`, `modalidad_contratacion`, `modalidad_contratacion_d`, `fuente_financiamiento_dis`, `monto_asignado_dis`, `nog`, `nog_f`, `fuente_f_sup`, `monto_sup`, `fuente_f_ejecucion`, `monto_ejecucion`, `fuente_f_ambiental`, `monto_ambiental`, `fuente_riesgo`, `monto_riesgo`, `estudio_viabilidad`, `aprobacion_viabilidad`, `resultado_principal_impacto`, `observaciones_marn`, `links_marn`, `observaciones_agrip`, `links_agrip`, `observaciones_suelo`, `links_suelo`, `observaciones_topografia`, `observaciones_geologico`, `links_geologico`, `links_topografia`, `estudio_hidrogeologico`, `fecha_hidrogeologico`, `hecho_por_hidrogeologico`, `observaciones_hidrogeologico`, `links_hidrogeologico`, `observaciones_estructural`, `links_estructural`, `memoria_calculo_f`, `memoria_calculo_hecho_por`, `observaciones_memoria_calculo`, `liks_memoria_calculo`, `espe_general_f`, `observaciones_espe_general`, `links_espe_general`, `plano_completo_f`, `observaciones_plano_completo`, `liks_plano_completo`, `espe_tecnica_f`, `links_espe_tecnica`, `observaciones_espe_tecnica`, `dispo_especial_f`, `terminos_referencia`, `links_plano_completo`, `inst_ea`, `inst_ea_resoponsable`, `inst_ea_d`, `links_inst_ea`, `links_presupuesto`, `resp_diseno`, `a_estudio_facti`, `a_entidades`, `a_derecho_paso`) VALUES
(1, 1, '', '', '', '', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 1, '', 1, '', 0, '', 0, '', 0, 0, '0000-00-00', 'Liquidado', 0, 100, 0, 0, 'Licitación pública', '', 0, 5970482, '2017-02-13', '', 0, '2017-1113-0013-205-00-13-00-000-004-000-000-017-0001-173-0101-29-0101-5-0000-0000-0000, 2017-1113-0013-205-00-13-00-000-004-000-000-017-0001-173-0101-31-0000-0-0000-0000-0000,2017-1113-0013-205-00-13-00-000-004-000-000-017-0001-173-0101-32-0000-0-0000-0000-0000.', 0, '', 0, '', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(2, 2, '', '', '', 'Arquitecta Ingrid Elena García, Alde las Brisas y  Arquitecta Ely Alejandra Jump, Aldea La Unión.', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', '', '0000-00-00', 'Ampliación del Instituto Diversificado Nacional, Aldea Las Brisas Petacalapa  RESOLUCIÓN AEI-4891-2014 Y Ampliacion de la Escuela Oficial Rural Mixta Aldea La Unión RESOLUCIÓN EIA-4891-2014', '2015-01-07', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 1, '', 1, '', 0, '', 0, '', 0, '', 0, 0, '0000-00-00', 'En ejecución', 0, 0, 0, 0, 'Licitación pública', '', 0, 6463738, '2017-06-23', '', 0, 'Convenios y Tratados Internacionales (Art. 1 LCE) CDP No. 30475485 (9/octubre/2017)', 0, '', 0, '', 0, 'si', 'DICTAMEN TECNICO 04-2015-DIPLAN FECHA 23 DE FEBRERO DE 2015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(3, 3, '', '', '', 'Ing. Civil Ahmid Eduardo Daccarett Daccarett Col. 1754 (Activo)', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', 'DGGA-GA-R-001/DE FECHA 3-03-2017', 'Dirección de Gestión Ambiental y Recursos Naturales -MAR-', '2017-03-03', 'SI', '2015-01-07', '', 'SI', '0000-00-00', 'UNIDAD PARA EL DESARROLLO DE VIVIENDA POPULAR -UDEVIPO Y MAR', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 1, 'Ing. Civil Ahmid Eduardo Daccarett Daccarett Col. 1754 (Activo)', 1, 'UNIDAD PARA EL DESARROLLO DE VIVIENDA POPULAR -UDEVIPO', 1, 'UNIDAD PARA EL DESARROLLO DE VIVIENDA POPULAR -UDEVIPO', 1, 'UNIDAD PARA EL DESARROLLO DE VIVIENDA POPULAR -UDEVIPO', 0, '', 0, 0, '2017-02-01', 'En ejecución', 0, 0, 0, 0, 'Licitación pública UDEVIPO LIC-001-2017', '', 0, 6710204, '2017-09-14', '', 0, 'FONDOS PÚBLICOS  (Partida presupuestaria No. 2017-1113-0013-214-00-20-00-001-000-001-332-0115-11)', 0, '', 0, '', 0, 'Resolución de Evaluación Técnica del Proyecto. SEGEPLAN', 'Resolución de Evaluación Técnica del Proyecto. SEGEPLAN - 25/08/2017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(4, 4, '', '', '', 'Dirección General de Caminos -DGC-', 0, '', '', '', '', '0000-00-00', '0000-00-00', 'Ministerio de Comunicaciones Infraestructura y Vivienda', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', 'SI', 'Ing. Haniel Isaac Girón de León', '2016-05-01', 'SI', '2017-04-11', 'SI', '', '0000-00-00', '', 'SI', 'Dirección General de Caminos', 1, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 1, 'Dirección General de Caminos', 1, 'Dirección General de Caminos', 1, 'Dirección General de Caminos', 0, '', 0, 0, '2017-04-01', 'Finalizado', 0, 0, 0, 0, 'Licitación pública Nacional No. DGC-015-2016-C', '', 0, 5670217, '2016-12-28', '', 0, 'CDP 29435622 ', 0, 'Fondos de gobierno', 7874000, '', 0, 'Dictamen Técnico favorable ', ' fecha 18 de noviembre de 2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(5, 5, '', '', '', '', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, 0, '0000-00-00', 'En ejecución', 0, 0, 0, 0, '', '', 0, 687170, '2008-11-14', '', 0, '', 0, '', 0, '', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(6, 6, '', '', '', '', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, 0, '0000-00-00', 'Recindido', 0, 0, 0, 0, 'DIRECTA POR EMERGENCIA TT AGATHA', '', 0, 1518275, '2011-03-23', '', 0, '', 0, '', 0, '', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(7, 7, '', '', '', '', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, 0, '0000-00-00', 'En ejecución', 0, 0, 0, 0, '', '', 0, 247723, '2006-04-25', '', 0, '', 0, '', 0, '', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(8, 8, 'El mismo proceso de ejecución', 'El mismo proceso de ejecución', 'El mismo proceso de ejecución', 'SOLEL BONEH FTN, SOCIEDAD ANONIMA', 2023719.11, '6 meses', 'Incluido dentro del contrato del ejecutor', '', '', '0000-00-00', '0000-00-00', 'DIRECCIÓN GENERAL DE CAMINOS', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', '', '0000-00-00', 'si', '2011-12-13', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 1, 'DIRECCIÓN GENERAL DE CAMINOS', 1, 'DIRECCIÓN GENERAL DE CAMINOS', 1, 'DIRECCIÓN GENERAL DE CAMINOS', 1, 'DIRECCIÓN GENERAL DE CAMINOS', 0, 0, '2014-01-17', 'En ejecución', 0, 0, 0, 0, 'Licitación pública Internacional DGC-01-2006-FTN', 'Banco Centroamericano de Integración Económica -BCIE-', 0, 257346, '2006-05-20', '', 0, 'Banco Centroamericano de Integración Económica -BCIE-PRESTAMO 1994 Y Presupuesto General de Ingresos de la Nación. PARTIDA P 2009-13-202-11-01-001-000-002-181-3000-52-0401-0059*', 0, '', 0, '', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(9, 9, '', '', '', '', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '28/06/2017', '', '0000-00-00', 'No. 030-2017/DIGARN/OBT/laf.', '2017-09-19', '', '', '0000-00-00', '', '', 'Ing. Edwin Raúl Barrios Ambrosy', 1, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 1, '', 1, '', 1, '', 0, '', 0, 0, '0000-00-00', 'En ejecución', 0, 0, 0, 0, 'Licitación Pública', '', 0, 6463959, '2017-06-14', '', 0, '11-01-001-000-003-331-2199-41-1204-0069 Q.2,200,222.00                                       11-01-001-000-003-331-2199-52-0401-0067 Q.6,901,041.00', 0, '', 0, '', 0, 'publicado el 15-03-2018', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(10, 10, '', '', '', '', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '42914', '', '0000-00-00', '016-2017/DIGARN/OBT/laf', '2017-09-19', '', '', '0000-00-00', '', '', 'Ing. Edwin Raúl Barrios Ambrosy', 1, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 1, '', 0, '', 1, '', 0, '', 0, 0, '0000-00-00', 'En ejecución', 0, 0, 0, 0, 'Licitación Pública', '', 0, 6463983, '2017-06-28', '', 0, '11-01-001-000-003-331-2199-41-1204-0069 Q.2,200,222.00                                       11-01-001-000-003-331-2199-52-0401-0067 Q.6,901,041.00', 0, '', 0, '', 0, 'publicado el 15-03-2018', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(11, 11, '', '', '', '', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 1, '', 0, '', 0, '', 0, '', 0, 0, '0000-00-00', 'Finalizado', 0, 100, 0, 0, '', '', 0, 6176658, '2017-05-04', '', 0, 'Licitacitación Pública', 0, '', 0, '', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(12, 12, 'SI', 'SI', 'SI', '', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 1, 'Lic Juan Luis Grajeda Micheo Subdirector Técnico MAGA-PRODENORTE', 1, 'Lic Juan Luis Grajeda Micheo Subdirector Técnico MAGA-PRODENORTE', 1, 'Lic Juan Luis Grajeda Micheo Subdirector Técnico MAGA-PRODENORTE', 0, '', 0, 0, '0000-00-00', 'Liquidado', 0, 0, 0, 0, 'Contratación Directa', '', 0, 6239854, '2017-04-27', '', 0, 'Donación Financiada por Fondo Internacional de Desarrollo Agricola.', 0, '', 0, '', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(13, 13, '', '', 'SI', 'SI', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', 'DGGA-GA-R-001', 'MINISTERIO DE AMBIENTE Y RECURSOS NATURALES', '0000-00-00', 'Subida el 23 de diciembre de 2016', '2016-12-23', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 1, '', 1, '', 1, 'DIRECCION GENERAL DE LA POLICIA NACIONAL CIVIL -PNC-', 1, 'DIRECCION GENERAL DE LA POLICIA NACIONAL CIVIL -PNC-', 1, 'DIRECCION GENERAL DE LA POLICIA NACIONAL CIVIL -PNC-', 0, '', 0, 0, '2017-05-23', 'Finalizado', 0, 100, 0, 0, 'Licitación pública', '', 0, 5783828, '2016-12-23', '1905/OC-GU-Programa de Apoyo al Sector Justicia Penal.', 0, '11-00-001-000-001-332-1905-52-0402-0104(CDP No. 29470149) Préstamo 1905/OC-GU', 0, '', 0, '', 0, 'SI', 'SI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(14, 14, '', '', '', 'Arquitecto Manuel Eduardo Chin colegiado No. 2535', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', '', '0000-00-00', 'No. 000939-2017/DIGARN/OBT/rjop', '2015-01-07', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 1, 'Arquitecto Manuel Eduardo Chin colegiado No. 2535', 1, '', 1, '', 1, '', 0, '', 0, 0, '2017-07-01', 'En ejecución', 0, 0, 0, 0, 'Licitación pública', '', 0, 6413706, '2017-07-13', 'Renglón presupuestario 188', 0, 'CDP No. 30425307 PARTIDA P 11-00-001-000-001-32-0101-41-1204-0069', 0, '', 0, '', 0, 'Evaluación Técnica del proyecto FICHA SNIP ', 'de fecha 13/07/2017  ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(15, 15, '', '', '', 'Arq. José Darwin Pérez Estrada', 0, '', '', '', '', '0000-00-00', '0000-00-00', 'DEPARTAMENTO DE DESARROLLO DE LA DIRECCIÓN DE INFRAESTRUCTURA FISICA ARQ. JOSÉ DARWIN PÉREZ ESTRADA. JEFE DEL DEPARTAMENTO.', 'NOTIFICACIÓN', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', 'EXPEDIENTE No. EAI-186', 'MINISTERIO DE AMBIENTE Y RECURSOS NATURALES', '2013-04-04', '', '0000-00-00', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 1, ' ARQ. JOSÉ DARWIN PÉREZ ESTRADA. JEFE DEL DEPARTAMENTO.', 1, ' ARQ. JOSÉ DARWIN PÉREZ ESTRADA. JEFE DEL DEPARTAMENTO.', 1, 'DEPARTAMENTO DE DESARROLLO DE LA DIRECCIÓN DE INFRAESTRUCTURA ', 1, 'DEPARTAMENTO DE DESARROLLO DE LA DIRECCIÓN DE INFRAESTRUCTURA ', 1, 'DEPARTAMENTO DE DESARROLLO DE LA DIRECCIÓN DE INFRAESTRUCTURA ', 0, '', 0, 0, '0000-00-00', 'Finalizado', 0, 0, 0, 0, 'LICITACIÓN PUBLICA No. DGDR-DC-L-12-2017', '', 0, 6134106, '2017-04-20', 'PARTIDA PRESUPUESTARIA No. 2017-111300015-104-00-0101-0003-03-13-00-000-001-000-01-0014', 0, 'RENGLON 332-PARTIDA PRESUPUESTARIA No. 2017-111300015-104-00-13-00-001-000-001-332-1610-22', 0, '', 0, '', 0, 'SI', 'SI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(16, 16, '', '', '', 'formato 1-4', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', 'Ingeniero Mario Rene Chew Cruz', '0000-00-00', 'EIA-0143-2015', '2015-05-13', 'Incluidas en la resolución ambiental', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 1, '0000-00-00', 'Empresa PRISMATOP', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 1, 'Sin especificar', 1, 'Sin especificar', 1, 'Sin especificar', 0, '', 0, 0, '0000-00-00', 'En ejecución', 0, 100, 83, 0, 'LICITACIÓN PUBLICA', '11-00-001-000-001-332-1001-31-0000-0000', 59, 4808770, '2016-05-23', '', 0, '11-00-001-000-001-332-1001-31-0000-0000', 0, '', 0, '', 0, 'Estudio de Viabilidad del proyecto', 'Aprobación del estudio de factibilidad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(17, 17, '', '', '', 'Ing. Roberto Enrique\nGalindo Calderón', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '', '0000-00-00', '', '', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 1, 'Ing. Roberto Enrique\nGalindo Calderón', 1, 'UNOPS/GTM-045-2016 ', 1, 'UNOPS/GTM-045-2016 ', 0, '', 0, '', 0, 0, '0000-00-00', 'Liquidado', 0, 0, 0, 0, 'Licitación Pública Internacional', '', 0, 4657101, '2016-03-14', '', 0, '', 0, '', 0, '', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(18, 18, '', '', '', '', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', 'Expediente 2015-EAI-0179', 'Elmer Leonidas Guerra Calderon', '0000-00-00', '18-2015/NGRC/baog', '2015-03-11', '', '', '0000-00-00', '', '', 'Concejo Municipal', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 1, 'subidos el 25-04-2018, sin responsable, ni firmas ni sellos', 0, '', 0, '', 0, '', 0, '', 0, 0, '0000-00-00', 'Finalizado', 0, 100, 100, 0, 'Cotización ', '', 0, 4575482, '2016-02-24', '', 0, 'oficio CDP DAFIM 11-03-2018, Oficio CDF DAFIM 19-04-2016', 0, '', 0, '', 0, 'publicado parcialmente el 01-03-2016', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(19, 19, '', '', '', 'Luis Oswaldo Donis Duran', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', 'EAI-2043-2016', 'SICAN,RUANO,,HELAMAN,ABINADI', '0000-00-00', '075-2016/DCN/DDJALAPA/BAOG/memc', '2016-04-28', '', '', '0000-00-00', '', '', 'Efren Antonio Juarez Morales, Arquitecto Col 2703, Supervisor de Obras', 1, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 0, '', 1, '', 0, '', 0, '', 0, '', 0, 0, '0000-00-00', 'Finalizado', 0, 100, 0, 0, 'Licitación Pública', '', 0, 5336066, '2016-08-31', '', 0, '13-020-332-21-0101-0001 y 13-020-332-22-0101-0001', 0, '', 0, '', 0, '', 'Acta del CM 027-2016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(20, 20, '', '', '', '', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', 'Evaluación Ambiental Inicial, sin fecha', 'Municipalidad de Mataquescuintla, Jalapa', '0000-00-00', 'Expediente 2016-E41-68, Resolución  15B-2016/DCN/DDJALAPA/BAOG/memc', '2016-11-30', 'compromisos ambientales  numeral VI, al XIV', '', '0000-00-00', '', '', 'Luis Oswaldo Donis Durán ', 1, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 1, 'Sin firma ni sellos, publicados el  21/07/2017', 1, 'Ing. David Alejandro Quejel Vasquez, Col 5997', 0, '', 0, '', 0, '', 0, 0, '0000-00-00', 'En ejecución', 0, 0, 0, 0, 'Licitacion publica', '', 0, 6584675, '2017-07-21', '', 0, '', 0, '', 0, '', 0, 'Luis Oswaldo Donis Durán ', 'concejo municipal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(21, 21, '', '', '', 'Mileidy Natali Santizo Zepeda', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', 'EAI-4872-2016', '', '2016-09-30', '05039-2016/DIGARN/DCA/OJCH/ammr', '2016-10-14', '', '', '0000-00-00', '', '', 'Ing. Carlos Francisco Patzan con', 1, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 1, 'Mileidy Natali Santizo Zepeda', 1, '', 1, '', 0, '', 0, '', 0, 0, '0000-00-00', 'Finalizado', 0, 100, 0, 0, '', '', 0, 5660319, '2016-11-16', '', 0, '', 0, '', 0, '', 0, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', ''),
(22, 22, '', '', '', 'Carlos Francisco Patzán Con', 0, '', '', '', '', '0000-00-00', '0000-00-00', '', '', '0000-00-00', 0, '', '', '', '', '0000-00-00', 0, '', '', 0, '', '0000-00-00', '', '0000-00-00', '', '', '', 'EAI-0041-2017', '', '2017-01-10', '00261-2017/DIGARN/DCA/OBT/rdor', '2017-02-03', '', '', '0000-00-00', '', '', 'Ing. Edga Rodolfo Moroy Chuquiej', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '0000-00-00', '', 0, '', 1, 'Carlos Francisco Patzán Con', 1, '', 1, '', 1, '', 0, '', 0, 0, '0000-00-00', 'En ejecución', 0, 0, 0, 0, 'Cotizacion', '', 0, 6764584, '2017-08-28', '', 0, '', 0, '', 0, '', 0, 'SI', 'SI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', '', '', 0, '', '0000-00-00', '', '', '', '', '', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cost_proyecto_dis_plan`
--
ALTER TABLE `cost_proyecto_dis_plan`
  ADD PRIMARY KEY (`id_proyecto_dis_plan`),
  ADD UNIQUE KEY `id_proyecto_dis_plan` (`id_proyecto_dis_plan`),
  ADD KEY `id_proyecto_idx` (`id_proyecto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cost_proyecto_dis_plan`
--
ALTER TABLE `cost_proyecto_dis_plan`
  MODIFY `id_proyecto_dis_plan` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
