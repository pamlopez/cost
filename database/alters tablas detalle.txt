ALTER TABLE `cost_acuerdo_financiamiento` ADD `id_fase` INT NOT NULL DEFAULT '0' AFTER `id_proyecto`;
ALTER TABLE `cost_acta` ADD `id_fase` INT NOT NULL DEFAULT '0' AFTER `id_proyecto`;
ALTER TABLE `cost_ampliacion` CHANGE `tipo` `id_fase` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `cost_ampliacion` CHANGE `tipo_ampliacion` `tipo` INT(11) NOT NULL;
ALTER TABLE `cost_fianza` CHANGE `tipo` `id_fase` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `cost_fianza` CHANGE `tipo_fianza` `tipo` INT(11) NOT NULL;
ALTER TABLE `cost_info_contrato` ADD `id_fase` INT NOT NULL DEFAULT '0' AFTER `id_proyecto`;

ALTER TABLE `cost_acuerdo_financiamiento` ADD `monto_asignado` DOUBLE NOT NULL AFTER `partida_presupuestaria`;
ALTER TABLE `cost_acuerdo_financiamiento` ADD `fuente_financiamiento` TEXT NOT NULL AFTER `tipo`;
ALTER TABLE `cost_acuerdo_financiamiento` CHANGE `monto_asignado` `monto_total_asignado` DOUBLE NOT NULL;


ALTER TABLE `cost_info_contrato` ADD `nombre_responsable_seleccionado` VARCHAR(500) NOT NULL AFTER `oferentes`;

ALTER TABLE `cost_info_contrato` ADD `modalidad_contratacion` INT NULL DEFAULT '0' AFTER `conclusion`, ADD `modalidad_contratacion_d` VARCHAR(250) NULL AFTER `modalidad_contratacion`, ADD `bases_concurso` TEXT NULL AFTER `modalidad_contratacion_d`, ADD `bases_concurso_link` TEXT NULL AFTER `bases_concurso`, ADD `fecha_publicacion` DATE NULL AFTER `bases_concurso_link`, ADD `fecha_ultima_mod_gc` DATE NULL AFTER `fecha_publicacion`, ADD `total_inconformidades` INT NULL AFTER `fecha_ultima_mod_gc`, ADD `total_inconformidades_r` INT NULL AFTER `total_inconformidades`, ADD `alcance` TEXT NULL AFTER `total_inconformidades_r`, ADD `programa` TEXT NULL AFTER `alcance`, ADD `nombres_suscriben` INT NOT NULL AFTER `programa`;

ALTER TABLE `cost_info_contrato` CHANGE `nombres_suscriben` `nombres_suscriben` TEXT NULL;

ALTER TABLE `cost_info_contrato` ADD `fecha_autoriza_bitacora` DATE NULL AFTER `nombres_suscriben`, ADD `quien_autoriza_bitacora` VARCHAR(250) NULL AFTER `fecha_autoriza_bitacora`, ADD `clausula_sobrecosto` TEXT NULL AFTER `quien_autoriza_bitacora`, ADD `fecha_inicio_ejecucion` DATE NULL AFTER `clausula_sobrecosto`;



CREATE TABLE `prod_sept26`.`cost_liquidacion` ( `id_liquidacion` SERIAL NOT NULL AUTO_INCREMENT , `id_proyecto` INT NOT NULL , `informe_avances` TEXT NULL , `planos_finales` TEXT NULL , `reporte_evaluacion` TEXT NULL , `informe_terminacion` TEXT NULL , `informe_evaluacion` TEXT NULL , PRIMARY KEY (`id_liquidacion`)) ENGINE = InnoDB;

CREATE TABLE `prod_sept26`.`cost_liquidacion_monto` ( `id_liquidacion_monto` SERIAL NOT NULL AUTO_INCREMENT , `id_liquidacion` INT NOT NULL , `documento_cambio` TEXT NULL , `suma_mod` DOUBLE NULL , `razon_cambio` TEXT NULL , `monto_actualizado` DOUBLE NULL , `no_fianza` VARCHAR(500) NULL , PRIMARY KEY (`id_liquidacion_monto`)) ENGINE = InnoDB;


CREATE TABLE `prod_sept26`.`cost_liquidacion_tiempo` ( `id_liquidacion_tiempo` SERIAL NOT NULL AUTO_INCREMENT , `id_liquidacion` INT NOT NULL , `no_modificaciones_plazo` INT NULL , `cantidad_tiempo_modificado` VARCHAR(250) NULL , `nueva_fecha_finaliza` DATE NULL , `razones_cambio_plazo` TEXT NULL , `no_fianza` VARCHAR(500) NULL , PRIMARY KEY (`id_liquidacion_tiempo`)) ENGINE = InnoDB;