<?php
if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

setlocale(LC_TIME, 'es_GT.UTF-8','es_GT.ISO-8859-1','es_GT','es_ES','es_ES','spanish');
//Carbon
use Carbon\Carbon;
Carbon::setLocale('es');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();
Route::get('/', function () {
    return redirect('costProyectos');
});

//Muestra de chivos de controles listos para ser usados
Route::get('chivos', function () {
    return view('chivos.demo');
});

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index');
    Route::resource('catCats', 'cat_catController');
    Route::resource('catItems', 'cat_itemController');
    Route::get('json/catalogo','cat_itemController@json');
    Route::get('itemdeshabilitado/{id}','cat_itemController@itemdeshabilitado');

    //cost loqueado

    //Controles geograficos
    Route::get('json/geo','geoController@mostrar_hijos');
    Route::resource('administracion','AdministracionController');
    Route::resource('sedes', 'sedeController');
    Route::post('edit_user/{id}','AdministracionController@edit_user');
    Route::get('reiniciar_password/{id}','AdministracionController@reiniciar_password');
    Route::get('reiniciar_password_perfil/{id}','AdministracionController@reiniciar_password_perfil');
    Route::get('password_perfil_change/{id}','AdministracionController@password_perfil_change');
    Route::post('password_change','AdministracionController@password_change');
    Route::get('password_deshabilitar/{id}','AdministracionController@password_deshabilitar');
    Route::get('password_habilitar/{id}','AdministracionController@password_habilitar');


    //upload de fotografias del proyecto
    Route::get('image_upload','ImageController@imageUpload');
    Route::post('image_upload_save','ImageController@imageUploadPost');
    Route::post('eliminar_foto/{id}','ImageController@eliminar_foto');

    //publicador
    Route::get('publicar_proyecto/{id}','cost_proyectoController@publicar_proyecto');
    Route::get('no_publicar_proyecto/{id}','cost_proyectoController@no_publicar_proyecto');

    //seguimiento
    Route::get('marcar_seguimiento/{id}','cost_proyectoController@marcar_seguimiento');
    Route::get('no_marcar_seguimiento/{id}','cost_proyectoController@no_marcar_seguimiento');

    Route::resource('documentos', 'documentoController');
    Route::resource('empleos', 'empleoController');
    Route::get('identificacionProyecto/{id}', 'cost_proyectoController@detalle_plano_ident_proyecto');
    Route::resource('costProyectoDisPlans', 'cost_proyecto_dis_planController');
    Route::get('costProyectoDisPlans_plano', 'cost_proyecto_dis_planController@vista_plana');
    Route::resource('costProyectos', 'cost_proyectoController');
    Route::resource('costProyectoDisPlans', 'cost_proyecto_dis_planController');
    Route::resource('costProyectoPlanificas', 'cost_proyecto_planificaController');
    Route::resource('costProyectoSupervisions', 'cost_proyecto_supervisionController');
    Route::resource('costMetodologia','cost_metodologiaController');
    Route::resource('costProyectoDisPlans', 'cost_proyecto_dis_planController');
    Route::resource('costAmpliacions', 'cost_ampliacionController');
    Route::resource('costActas', 'cost_actaController');
    Route::resource('costAcuerdoFinanciamientos', 'cost_acuerdo_financiamientoController');
    Route::resource('costFianzas', 'cost_fianzaController');
    Route::resource('costInfoContratos', 'cost_info_contratoController');
    Route::resource('costPagoEfectuados', 'cost_pago_efectuadoController');
    Route::resource('descargaResponsables', 'descarga_responsableController');
    Route::resource('costLiquidacions', 'cost_liquidacionController');
    Route::resource('costLiquidacionAlcances', 'cost_liquidacion_alcanceController');
    Route::resource('costLiquidacionMontos', 'cost_liquidacion_montoController');
    Route::resource('costEstimacions', 'cost_estimacionController');
    Route::resource('costLiquidacionTiempos', 'cost_liquidacion_tiempoController');
    Route::resource('participantes', 'participanteController');
    Route::resource('costOferentes', 'cost_oferenteController');
    Route::resource('costOperadors', 'cost_operadorController');
    Route::resource('politicas', 'politicaController');
});
//rutas publicas
Route::resource('politicas', 'politicaController', [ 'only' => [ 'show'] ]);
Route::get("indicadores","indicadoresController@indicadores");
Route::resource('documentos', 'documentoController', [ 'only' => [ 'show','index' ] ]);
Route::resource('costProyectoDisPlans', 'cost_proyecto_dis_planController', [ 'only' => [ 'show' ,'index'] ]);
Route::resource('costProyectos', 'cost_proyectoController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costProyectoDisPlans', 'cost_proyecto_dis_planController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costProyectoPlanificas', 'cost_proyecto_planificaController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costProyectoSupervisions', 'cost_proyecto_supervisionController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costMetodologia', 'cost_metodologiaController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costProyectoDisPlans', 'cost_proyecto_dis_planController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costAmpliacions', 'cost_ampliacionController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costActas', 'cost_actaController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costAcuerdoFinanciamientos', 'cost_acuerdo_financiamientoController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costFianzas', 'cost_fianzaController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costInfoContratos', 'cost_info_contratoController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costPagoEfectuados', 'cost_pago_efectuadoController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('descargaResponsables', 'descarga_responsableController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costContactenos', 'cost_contactenosController');
Route::resource('costLiquidacions', 'cost_liquidacionController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costLiquidacionAlcances', 'cost_liquidacion_alcanceController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costLiquidacionMontos', 'cost_liquidacion_montoController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costEstimacions', 'cost_estimacionController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costLiquidacionTiempos', 'cost_liquidacion_tiempoController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('participantes', 'participanteController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costOferentes', 'cost_oferenteController', [ 'only' => [ 'show', 'index' ] ]);
Route::resource('costOperadors', 'cost_operadorController', [ 'only' => [ 'show', 'index' ] ]);
Route::get('identificacionProyecto/{id}', 'cost_proyectoController@detalle_plano_ident_proyecto');
Route::get('download_oc4ids', 'cost_proyectoController@download_oc4ids');
Route::get('busqueda_especifica', 'cost_proyectoController@busqueda_especifica');
Route::post('busqueda_especifica', 'cost_proyectoController@busqueda_especifica');

//Pruebas
//Envio de correos
Route::get('/test/correo', 'emailController@test');

//convert json to csv
Route::get('convert_json_csv/{id}','cost_proyectoController@convert_json_csv');
Route::get('convert_json_excel/{id}','cost_proyectoController@convert_json_excel');
Route::get('costProyectos/proyecto/{id}','cost_proyectoController@ocds');
Route::post('costProyectos/proyectos_unificados','cost_proyectoController@ocds_unificado');

//Controles geograficos
Route::post('json/geo','geoController@mostrar_hijos');
Route::post('json/geo_todo','geoController@mostrar_hijos_con_todo');

