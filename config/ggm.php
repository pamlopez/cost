<?php

return [

    /*
    |--------------------------------------------------------------------------
    | RAngo de edades CAIMU
    |--------------------------------------------------------------------------
    |
    | Arreglo con los valores con que inicia c/rango.  Ordenados de mayor a menor

    */

    'rango_caimu' => [
        48 => 61,
        47 => 56,
        46 => 51,
        45 => 46,
        44 => 41,
        43 => 36,
        42 => 31,
        41 => 26,
        40 => 21,
        39 => 16,
        38 => 11,
        37 => 6,
        49 => 0,
    ],
     //Default para el rango
    'rango_caimu_default' => 49,

    'rango_ine' => [
        68 => 77,
        67 => 74,
        66 => 70,
        65 => 66,
        64 => 62,
        63 => 58,
        62 => 54,
        61 => 50,
        60 => 46,
        59 => 42,
        58 => 38,
        57 => 34,
        56 => 30,
        55 => 26,
        54 => 22,
        53 => 18,
        52 => 14,
        51 => 10,
        50 => 5,
        69 => 0,
    ],
    //Default para el rango
    'rango_ine_default' => 69,



    'rango_mingob' => [
        73 => 61,
        72 => 31,
        71 => 14,
        70 => 0,

    ],
    //Default para el rango
    'rango_mingob_default' => 70,



];
