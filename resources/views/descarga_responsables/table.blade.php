<table class="table table-responsive" id="descargaResponsables-table">
    <thead>
        <tr>
            <th>Nombre Completo</th>
        <th>No Identificacion Personal</th>
        <th>Descripcion</th>
        <th>Fh Descarga</th>
        <th>Id Proyecto</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($descargaResponsables as $descargaResponsable)
        <tr>
            <td>{!! $descargaResponsable->nombre_completo !!}</td>
            <td>{!! $descargaResponsable->no_identificacion_personal !!}</td>
            <td>{!! $descargaResponsable->descripcion !!}</td>
            <td>{!! $descargaResponsable->fh_descarga !!}</td>
            <td>{!! $descargaResponsable->id_proyecto !!}</td>
            <td>
                {!! Form::open(['route' => ['descargaResponsables.destroy', $descargaResponsable->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('descargaResponsables.show', [$descargaResponsable->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('descargaResponsables.edit', [$descargaResponsable->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>