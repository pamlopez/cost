<!-- Nombre Completo Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nombre_completo', '* Nombre completo:') !!}
    {!! Form::text('nombre_completo', null, ['class' => 'form-control','required' => 'required']) !!}
</div>

<!-- No Identificacion Personal Field -->
<div class="form-group col-sm-12">
    {!! Form::label('no_identificacion_personal', '* No Identificación personal:', ['class'=>'required']) !!}
    {!! Form::text('no_identificacion_personal', null, ['class' => 'form-control required', 'required' => 'required']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', '* Descripción:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Fh Descarga Field -->
<div class="form-group col-sm-6">
    {!! Form::hidden('fh_descarga', \Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control']) !!}
</div>

@if(isset($bnd_gnrl) && $bnd_gnrl == 2)
    <!-- Id Proyecto Field -->
    <div class="form-group col-sm-6">
        {!! Form::hidden('id_proyecto', $costProyecto->id, ['class' => 'form-control']) !!}
    </div>
@else
    {!! Form::hidden('id_proyecto',0, ['class' => 'form-control']) !!}
    {!! Form::hidden('general',1, ['class' => 'form-control']) !!}
@endif

