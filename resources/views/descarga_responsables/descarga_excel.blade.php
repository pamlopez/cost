@extends('layouts.app')
@include('reportes._style')
@push('styles')
    <link rel="stylesheet" href="{{ url("/css/reportes/bootstrap-datetimepicker/bootstrap-datetimepicker.css") }}">
    <link rel="stylesheet" href="{{ url("/css/reportes/datatables/fixedColumns.dataTables.min.css") }}">
    <link rel="stylesheet" href="{{ url("/css/reportes/datatables/buttons.dataTables.css") }}">
@endpush
@push('javascript')
    <script src="{{ url('/js/reportes/data_tables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/buttons.flash.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/jszip.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/pdfmake.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/vfs_fonts.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/buttons.html5.min.js') }}"></script>
    <script src="{{ url('/js/reportes/data_tables/buttons.print.min.js') }}"></script>

    <script type="text/javascript">
        var base64_data_table = '{!! $json_data_table !!}';
        var json_data_table = JSON.parse(atob(base64_data_table));
        console.log(json_data_table);
    </script>
@endpush
@section('content')
    <div class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <a href="{{ URL::previous() }}" class="btn btn-primary  no-print btn-sm"><i class="fa fa-reply" aria-hidden="true"></i>Ir atrás</a>
                    <b>  <i class="fa fa-download" aria-hidden="true"></i> Decarga del proyecto </b>
                </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>

            <div class="box-body table-responsive">
                <div class="table-responsive col-md-12">
                    <table  id="tabla_proyecto" class="table table-bordered">
                        <thead>
                            <tr>
                                @foreach($array_resultado[0] as $key => $val)
                                    <th>{{$key}}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @if($tipo_descarga == 1)
                                @foreach(collect ($array_resultado) as $key => $val)
                                    <tr>
                                        @foreach($val as $k =>$v)
                                            <td>{{$v}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @else
                                @foreach($array_resultado[0] as $key => $val)
                                    <td>{{$val}}</td>
                                @endforeach
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabla_proyecto').DataTable(json_data_table);
        } );
    </script>
@endpush