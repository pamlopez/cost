<!-- Id Descarga Responsable Field -->
<div class="form-group">
    {!! Form::label('id_descarga_responsable', 'Id Descarga Responsable:') !!}
    <p>{!! $descargaResponsable->id_descarga_responsable !!}</p>
</div>

<!-- Nombre Completo Field -->
<div class="form-group">
    {!! Form::label('nombre_completo', 'Nombre Completo:') !!}
    <p>{!! $descargaResponsable->nombre_completo !!}</p>
</div>

<!-- No Identificacion Personal Field -->
<div class="form-group">
    {!! Form::label('no_identificacion_personal', 'No Identificacion Personal:') !!}
    <p>{!! $descargaResponsable->no_identificacion_personal !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $descargaResponsable->descripcion !!}</p>
</div>

<!-- Fh Descarga Field -->
<div class="form-group">
    {!! Form::label('fh_descarga', 'Fh Descarga:') !!}
    <p>{!! $descargaResponsable->fh_descarga !!}</p>
</div>

<!-- Id Proyecto Field -->
<div class="form-group">
    {!! Form::label('id_proyecto', 'Id Proyecto:') !!}
    <p>{!! $descargaResponsable->id_proyecto !!}</p>
</div>

