{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costLiquidacionAlcance->id_proyecto, ['class' => 'form-control']) !!}

<!-- No Modificaciones Alcance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_modificaciones_alcance', 'No. de modificaciones al alcance:') !!}
    {!! Form::number('no_modificaciones_alcance', null, ['class' => 'form-control']) !!}
</div>

<!-- F Aprobacion Field -->
<div class="form-group col-sm-6">
        {!! Form::label('fecha_cambio', 'Fecha cambio (OC4IDS):') !!}
        @if($edit = 0)
                {!! Form::text('fecha_cambio', null, ['class' => 'form-control datepicker ','data-value'=>\Carbon\Carbon::now()]) !!}
        @else
                {!! Form::text('fecha_cambio', null, ['class' => 'form-control datepicker ','data-value'=>$costLiquidacionAlcance->fecha_cambio]) !!}
        @endif
</div>


<!-- Razon Cambio Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('razon_cambio', 'Razon  de Cambio:') !!}
    {!! Form::textarea('razon_cambio', null, ['class' => 'form-control']) !!}
</div>

<!-- Alcance Real Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('alcance_real', 'Alcance real:') !!}
    {!! Form::textarea('alcance_real', null, ['class' => 'form-control']) !!}
</div>

<!-- Programa Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('programa', 'Programa de trabajo actualizado:') !!}
    {!! Form::textarea('programa', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza', 'Fianza actualizada al nuevo alcance:') !!}
    {!! Form::text('no_fianza', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    @if(isset($id_proyecto))
        <a href="{!! route('costLiquidacionAlcances.show', [$id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costLiquidacionAlcances.show', [$costLiquidacionAlcance->id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif
</div>
