@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Nueva ingreso de Alcance
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'costLiquidacionAlcances.store']) !!}

                        @include('cost_liquidacion_alcances.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
