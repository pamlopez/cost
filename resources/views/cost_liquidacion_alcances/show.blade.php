@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Alcance
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_liquidacion_alcances.show_fields')
                </div>
                <a href="{!! route('costLiquidacionAlcances.index',['id_proyecto'=>$costLiquidacionAlcance->id_proyecto]) !!}" class="btn btn-default">Atrás</a>
            </div>
        </div>
    </div>
@endsection
