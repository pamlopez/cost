<table class="table table-responsive" id="costLiquidacionAlcances-table">
    <thead>
    <tr>
        <th>#</th>
        <th>No. de modificaciones al alcance</th>
        <th>Fecha de cambio</th>
        <th>Razones de cambio</th>
        <th>Alcance real</th>
        <th>Programa</th>
        <th>No. Fianza</th>
        @if(!isset($a))
        <th colspan="3">Action</th>
            @endif
    </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($costLiquidacionAlcances as $costLiquidacionAlcance)
        @php($i++)
        <tr>
            <td>{!! $i !!}</td>
            <td>{!! $costLiquidacionAlcance->no_modificaciones_alcance !!}</td>
            <td>{!! $costLiquidacionAlcance->FmtFechaCambio !!}</td>
            <td>{!! $costLiquidacionAlcance->razon_cambio !!}</td>
            <td>{!! $costLiquidacionAlcance->alcance_real !!}</td>
            <td>{!! $costLiquidacionAlcance->programa !!}</td>
            <td>{!! $costLiquidacionAlcance->no_fianza !!}</td>
            @if(!isset($a))
            <td>
                {!! Form::open(['route' => ['costLiquidacionAlcances.destroy', $costLiquidacionAlcance->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costLiquidacionAlcances.show', [$costLiquidacionAlcance->id]) !!}"
                       class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                        <a href="{!! route('costLiquidacionAlcances.edit', [$costLiquidacionAlcance->id]) !!}"
                           class='btn btn-warning btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
                @endif
        </tr>
    @endforeach
    </tbody>
</table>