<!-- No Modificaciones Alcance Field -->
<div class="form-group col-md-6">
    {!! Form::label('no_modificaciones_alcance', 'No. de modificaciones al alcance:') !!}
    <p>{!! $costLiquidacionAlcance->no_modificaciones_alcance !!}</p>
</div>

<!-- Razon Cambio Field -->
<div class="form-group col-md-12">
    {!! Form::label('fecha_cambio', 'Fecha del cambio:') !!}
    <p>{!! $costLiquidacionAlcance->FmtFechaCambio !!}</p>
</div>

<!-- Razon Cambio Field -->
<div class="form-group col-md-12">
    {!! Form::label('razon_cambio', 'Razon del cambio:') !!}
    <p>{!! $costLiquidacionAlcance->razon_cambio !!}</p>
</div>

<!-- Alcance Real Field -->
<div class="form-group col-md-12">
    {!! Form::label('alcance_real', 'Alcance real:') !!}
    <p>{!! $costLiquidacionAlcance->alcance_real !!}</p>
</div>

<!-- Programa Field -->
<div class="form-group col-md-12">
    {!! Form::label('programa', 'Programa de trabajo actualizado:') !!}
    <p>{!! $costLiquidacionAlcance->programa !!}</p>
</div>

<!-- No Fianza Field -->
<div class="form-group col-md-6">
    {!! Form::label('no_fianza', 'No. Fianza:') !!}
    <p>{!! $costLiquidacionAlcance->no_fianza !!}</p>
</div>

