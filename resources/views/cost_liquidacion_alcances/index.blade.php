@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Alcances</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
               href="{!! route('costLiquidacionAlcances.create', compact('id_proyecto')) !!}">Nuevo Alcance</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('cost_liquidacion_alcances.table')
            </div>
        </div>
        <div class="text-left">
            <a href="{!! route('costProyectos.show', [$id_proyecto,'a'=>1]) !!}"
               class="btn btn-default">Atrás</a>
        </div>
    </div>
@endsection

