@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cost Operador
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costOperador, ['route' => ['costOperadors.update', $costOperador->id], 'method' => 'patch']) !!}

                        @include('cost_operadors.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection