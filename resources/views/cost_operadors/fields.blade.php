{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costOperador->id_proyecto, ['class' => 'form-control']) !!}


<!-- Nombre Operador Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_operador', 'Nombre del operador:') !!}
    {!! Form::text('nombre_operador', null, ['class' => 'form-control']) !!}
</div>

<!-- Usuario Operador Field -->
<div class="form-group col-sm-6">
    {!! Form::label('usuario_operador', 'Usuario del operador:') !!}
    {!! Form::text('usuario_operador', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Operador Field -->
<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_operador', 'Fecha en que aparece por primera vez como operador del portal:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_operador', null, ['class' => 'form-control datepicker','data-value'=>$costOperador->fecha_operador]) !!}
    @else
        {!! Form::text('fecha_operador', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}

    @if(isset($id_proyecto))
        <a href="{!! route('costOperadors.index', ['id_proyecto'=>$id_proyecto]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costOperadors.index', ['id_proyecto'=>$costOperador->id_proyecto]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif

</div>
