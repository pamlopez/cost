

<!-- Id Proyecto Field -->
<div class="form-group">
    {!! Form::label('id_proyecto', 'Proyecto no:') !!}
    <p>{!! $costOperador->id_proyecto !!}-2018Gt</p>
</div>

<!-- Nombre Operador Field -->
<div class="form-group">
    {!! Form::label('nombre_operador', 'Nombre Operador:') !!}
    <p>{!! $costOperador->nombre_operador !!}</p>
</div>

<!-- Usuario Operador Field -->
<div class="form-group">
    {!! Form::label('usuario_operador', 'Usuario Operador:') !!}
    <p>{!! $costOperador->usuario_operador !!}</p>
</div>

<!-- Fecha Operador Field -->
<div class="form-group">
    {!! Form::label('fecha_operador', 'Fecha Operador:') !!}
    <p>{!! $costOperador->fecha_operador !!}</p>
</div>

