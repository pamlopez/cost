<table class="table table-responsive" id="costOperadors-table">
    <thead>
        <tr>
            <th>#</th>
        <th>Nombre del operador</th>
        <th>Usuario del operador</th>
        <th>Fecha en que aparece por primera vez como operador del portal</th>
            <th colspan="3">Acción</th>
        </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($costOperadors as $costOperador)
        @php($i++)
        <tr>
            <td>{!! $i !!}</td>
            <td>{!! $costOperador->nombre_operador !!}</td>
            <td>{!! $costOperador->usuario_operador !!}</td>
            <td>{!! $costOperador->fecha_operador !!}</td>
            <td>
                {!! Form::open(['route' => ['costOperadors.destroy', $costOperador->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costOperadors.show', [$costOperador->id]) !!}" class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('costOperadors.edit', [$costOperador->id]) !!}" class='btn btn-warning btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>