@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Operadores del portal para proyecto No. {{$id_proyecto}}-2018Gt</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right"  href="{!! route('costOperadors.create', ['id_proyecto'=>$id_proyecto]) !!}"
               style="margin-top: -10px;margin-bottom: 5px" ><i
                        class="fa fa-file-text" aria-hidden="true"></i> Nuevo Operador</a>

        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('cost_operadors.table')
            </div>
        </div>
        <div class="text-left">
            <a href="{!! route('costProyectos.show', [$id_proyecto,'a'=>1]) !!}"
               class="btn btn-default">Atrás</a>
        </div>
    </div>
@endsection

