@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Actas</h1>
        @if(Auth::check())
            <h1 class="pull-right">
                <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px"
                   href="{!! route('costActas.create', compact('id_fase','id_proyecto')) !!}">Nueva Acta</a>
            </h1>
        @endif
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="table table-responsive">
                    @include('cost_actas.table')
                </div>
            </div>
        </div>
        <div class="text-left">
            <a href="{!! route('costProyectos.show', [$id_proyecto,'a'=>1]) !!}"
               class="btn btn-default">Atrás</a>
        </div>
    </div>
@endsection

