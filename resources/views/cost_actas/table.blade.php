<table class="table table-responsive" id="costActas-table">
    <thead>
    <tr>
        <th>#</th>
        <th>Tipo Acta</th>
        <th>No Acta</th>
        <th>Fecha Acta</th>
        <th>Link</th>
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($costActas as $costActa)
        @php($i++)
        <tr>
            <td>{!! $i !!}</td>
            @if($costActa->tipo>0)
                <td>{!! $costActa->fmt_tipo !!}</td>
            @else
                <td>{!! $costActa->tipo_acta !!}</td>
            @endif
            <td>{!! $costActa->no_acta !!}</td>
            <td>{!! $costActa->fecha_acta !!}</td>
            <td>{!! $costActa->link !!}</td>
            <td>
                {!! Form::open(['route' => ['costActas.destroy', $costActa->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costActas.show', [$costActa->id]) !!}" class='btn btn-info btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                        <a href="{!! route('costActas.edit', [$costActa->id]) !!}" class='btn btn-warning btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>