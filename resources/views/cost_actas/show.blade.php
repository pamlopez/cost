@extends('layouts.app')
@include('flash::message')
@section('content')
    <section class="content-header">
        <h1>
           Actas
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_actas.show_fields')

                </div>
                <a href="{!! route('costActas.index', ['id_proyecto'=>$costActa->id_proyecto,'id_fase'=>$costActa->id_fase]) !!}"
                   class="btn btn-default">Atrás</a>
            </div>
        </div>
    </div>
@endsection
