@if($costActa->tipo>0)
    <!-- Tipo Field -->
    <div class="form-group col-md-6">
        {!! Form::label('tipo', 'Tipo:') !!}
        <p>{!! $costActa->fmt_tipo !!}</p>
    </div>
@else
    <!-- Tipo Acta D Field -->
    <div class="form-group col-md-6">
        {!! Form::label('tipo_acta_d', 'Tipo:') !!}
        <p>{!! $costActa->tipo_acta_d !!}</p>
    </div>
@endif


<!-- No Acta Field -->
<div class="form-group col-md-6">
    {!! Form::label('no_acta', 'No Acta:') !!}
    <p>{!! $costActa->no_acta !!}</p>
</div>

<!-- Fecha Acta Field -->
<div class="form-group col-md-6">
    {!! Form::label('fecha_acta', 'Fecha Acta:') !!}
    <p>{!! $costActa->fecha_acta !!}</p>
</div>

<!-- Link Field -->
<div class="form-group col-md-6">
    {!! Form::label('link', 'Link:') !!}
    <p>{!! $costActa->link !!}</p>
</div>

