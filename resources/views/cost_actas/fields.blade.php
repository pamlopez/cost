{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costActa->id_proyecto, ['class' => 'form-control']) !!}
{!! Form::hidden('id_fase',isset($id_fase)?$id_fase:$costActa->id_fase, ['class' => 'form-control']) !!}
<!-- Tipo Field -->

@include('chivos.catalogo', ['chivo_control' => 'tipo'
                           ,'chivo_id_cat'=>68
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Tipo de acta:'])
<!-- No Acta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_acta', 'No. Acta:') !!}
    {!! Form::text('no_acta', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Acta Field -->
<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_acta', 'Fecha del Acta:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_acta', null, ['class' => 'form-control datepicker','data-value'=>$costActa->fecha_acta]) !!}
    @else
        {!! Form::text('fecha_acta', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>

<!-- Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link', 'Link:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    @if(isset($id_fase))
        <a href="{!! route('costActas.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costActas.index', ['id_proyecto'=>$costActa->id_proyecto,'id_fase'=>$costActa->id_fase]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif

</div>
