<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"> <a href="{{ URL::previous() }}" class="btn btn-info btn-circle no-print">
                <i class="fa fa-reply" aria-hidden="true"></i></a><b> Personalizar la informaci&oacute;n visualizada</b></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        {{ Form::open(array('url' =>"#",'method' => 'get')) }}
        <div class="row">
            <div class="col-md-2 ">
                <div class="form-group">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Desde: </label>
                    {!! Form::text('fecha_del', null, ['class' => 'form-control datepicker','data-value'=>$filtros->fecha_del]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Hasta: </label>
                    {!! Form::text('fecha_al', null, ['class' => 'form-control datepicker','data-value'=>$filtros->fecha_al]) !!}
                </div>
            </div>
            <div class="col-md-6">
            @include('chivos.geo2', ['chivo_control' => 'id_muni_domicilio',
                                     'chivo_default'=>0,
                                     "chivo_depto"=>0,
                                     'label'=>'domicilio'])
            </div>
            <div class="form-group col-sm-2">
                {!! Form::label('zona_domicilio', 'Zona domicilio:') !!}
                {!! Form::select('zona_domicilio', \App\Models\atencion_inicial::listado_items_oitem('Seleccionar',5),null,['class' => 'form-control js-example-basic-multiple']) !!}
            </div>
            @include('chivos.catalogo', ['chivo_control' => 'visita_atencion_inicial'
                                                   ,'chivo_id_cat'=>56
                                                   ,'chivo_default'=>null
                                                   ,'chivo_tamanio'=>'col-sm-2'
                                                   ,'chivo_texto'=>'Visita A.T:'])

            <!-- Id Otros Visita Atencion Inicial Field -->
            @include('chivos.catalogo', ['chivo_control' => 'id_otros_visita_atencion_inicial'
                                                        ,'chivo_id_cat'=>57
                                                        , 'chivo_default'=>null
                                                        ,'chivo_tamanio'=>'col-sm-2'
                                                        ,'chivo_texto'=>'Otros visita A.T:'])
            <div class="form-group col-sm-2">
                {!! Form::label('id_atendio_visita_atencion_inicial', 'Atendió A.T:') !!}
                {!! Form::select('id_atendio_visita_atencion_inicial', \App\Models\empleada::listado_items('Seleccionar'),null,['class' => 'form-control']) !!}
            </div>
            @include('chivos.catalogo', ['chivo_control' => 'id_visita_area_legal'
                                                                    ,'chivo_id_cat'=>58
                                                                    , 'chivo_default'=>null
                                                                    ,'chivo_tamanio'=>'col-sm-2'
                                                                    ,'chivo_texto'=>'Visita legal:'])

            <div class="form-group col-sm-2">
                {!! Form::label('id_atendio_visita_area_legal', 'Atendió legal:') !!}
                {!! Form::select('id_atendio_visita_area_legal', \App\Models\empleada::listado_items('Seleccionar'),null,['class' => 'form-control']) !!}
            </div>
            @include('chivos.catalogo', ['chivo_control' => 'id_visita_area_psicologica'
                                                    ,'chivo_id_cat'=>59
                                                    , 'chivo_default'=>null
                                                    ,'chivo_tamanio'=>'col-sm-2'
                                                    ,'chivo_texto'=>'Visita psicología:'])

            <!-- Id Atendio Visita Area Psicologica Field -->
            <div class="form-group col-sm-2">
                {!! Form::label('id_atendio_visita_area_psicologica', 'Atendió psicología:') !!}
                {!! Form::select('id_atendio_visita_area_psicologica', \App\Models\empleada::listado_items('Seleccionar'),null,['class' => 'form-control']) !!}
            </div>
            <!-- Id Visita Area Trabajo Social Field -->
            @include('chivos.catalogo', ['chivo_control' => 'id_visita_area_trabajo_social'
                                                    ,'chivo_id_cat'=>60
                                                    , 'chivo_default'=>null
                                                    ,'chivo_tamanio'=>'col-sm-2'
                                                    ,'chivo_texto'=>'Visita T.S:'])

            <!-- Id Atendio Visita Area Trabajo Social Field -->
            <div class="form-group col-sm-2">
                {!! Form::label('id_atendio_visita_area_trabajo_social', 'Atendió T.S:') !!}
                {!! Form::select('id_atendio_visita_area_trabajo_social', \App\Models\empleada::listado_items('Seleccionar'),null,['class' => 'form-control']) !!}

            </div>

            <div class="col-md-2">
                <div class="form-group">
                    {!! Form::label('', '') !!}
                    <button type="submit" class="btn bg-purple form-control ">  <i class="fa fa-filter" aria-hidden="true"></i> Filtrar</button>
                </div>

            </div>

            <div class="col-md-2">
                <div class="form-group">
                    {!! Form::label('', '') !!}
                    <a type="button" class="btn bg-navy form-control" href="{{ $filtros->url }}"><i class="fa fa-refresh" aria-hidden="true"></i> Reiniciar</a>
                </div>

            </div>
        </div>
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>



@push("head")
    {{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
    <head><meta name="csrf-token" content="{!! csrf_token() !!}"></head>
@endpush



@push('javascript')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $('.datepicker').pickadate({
            selectMonths: true // Creates a dropdown to control month
            , selectYears: 15 // Creates a dropdown of 15 years to control year
            //The format to show on the `input` element
            , format: 'dd-mmmm-yyyy'   //Como se muestra al usuario
            , formatSubmit: 'yyyy-mm-dd',  //IMPORTANTE: para el submit
            //The title label to use for the month nav buttons
            labelMonthNext: 'Mes siguiente',
            labelMonthPrev: 'Mes anterior',
            //The title label to use for the dropdown selectors
            labelMonthSelect: 'Elegir mes',
            labelYearSelect: 'Elegir año',
            //Months and weekdays
            monthsFull: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre' ],
            monthsShort: [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
            weekdaysFull: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
            weekdaysShort: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
            //Materialize modified
            weekdaysLetter: [ 'D', 'L', 'M', 'M', 'J', 'V', 'S' ],
            //Today and clear
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Cerrar'
            //,editable: true
        });


        $('#id_atendio_visita_atencion_inicial').select2({
            placeholder: 'Select an option'
        });
        $('#id_atendio_visita_area_legal').select2({
            placeholder: 'Select an option'
        });
        $('#id_atendio_visita_area_psicologica').select2({
            placeholder: 'Select an option'
        });
        $('#id_atendio_visita_area_trabajo_social').select2({
            placeholder: 'Select an option'
        });
    </script>
@endpush