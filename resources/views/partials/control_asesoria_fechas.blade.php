
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">
            <a href="{{ URL::previous() }}" class="btn btn-info no-print">
                <i class="fa fa-reply" aria-hidden="true"></i> Ir atr&aacute;s
            </a>
                <b> Personalizar los datos visualizados</b>
        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>

    </div>

    <div class="box-body">
        {{ Form::open(array('url' =>"#",'method' => 'get')) }}
        <div class="row">
            <div class="col-md-2 ">
                <div class="form-group">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Desde: </label>
                    {!! Form::text('fh_inicial', null, ['class' => 'form-control datepicker','data-value'=>$filtros->fecha_del]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Hasta: </label>
                    {!! Form::text('fh_fin', null, ['class' => 'form-control datepicker','data-value'=>$filtros->fecha_al]) !!}
                </div>
            </div>
            <!-- Id Estado Papeleria Field -->
            @include('chivos.catalogo', ['chivo_control' => 'id_estado_papeleria'
                                         ,'chivo_id_cat'=>40
                                         , 'chivo_default'=>null
                                         , 'chivo_tamanio'=>'col-md-2'
                                         ,'chivo_texto'=>'Estado papeleria:'])
        <!-- Id Estado Papeleria Field -->
            @include('chivos.catalogo', ['chivo_control' => 'id_avance'
                                         ,'chivo_id_cat'=>43
                                         , 'chivo_default'=>null
                                         , 'chivo_tamanio'=>'col-md-2'
                                         ,'chivo_texto'=>'Avance:'])

            <div class="col-md-2">
                <div class="form-group">
                    {!! Form::label('', '') !!}
                    <button type="submit" class="btn bg-purple form-control ">  <i class="fa fa-filter" aria-hidden="true"></i> Aplicar</button>
                </div>

            </div>
            <div class="col-md-2">
                <div class="form-group">
                    {!! Form::label('', '') !!}
                    <a type="button" class="btn bg-navy form-control" href="{{ $filtros->url }}"><i class="fa fa-refresh" aria-hidden="true"></i>  Reiniciar</a>
                </div>

            </div>
        </div>
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>



@push("head")
    {{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
    <head><meta name="csrf-token" content="{!! csrf_token() !!}"></head>
@endpush



@push('javascript')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $('.datepicker').pickadate({
            selectMonths: true // Creates a dropdown to control month
            , selectYears: 15 // Creates a dropdown of 15 years to control year
            //The format to show on the `input` element
            , format: 'dd-mmmm-yyyy'   //Como se muestra al usuario
            , formatSubmit: 'yyyy-mm-dd',  //IMPORTANTE: para el submit
            //The title label to use for the month nav buttons
            labelMonthNext: 'Mes siguiente',
            labelMonthPrev: 'Mes anterior',
            //The title label to use for the dropdown selectors
            labelMonthSelect: 'Elegir mes',
            labelYearSelect: 'Elegir año',
            //Months and weekdays
            monthsFull: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre' ],
            monthsShort: [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
            weekdaysFull: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
            weekdaysShort: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
            //Materialize modified
            weekdaysLetter: [ 'D', 'L', 'M', 'M', 'J', 'V', 'S' ],
            //Today and clear
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Cerrar'
            //,editable: true
        });
    </script>
@endpush