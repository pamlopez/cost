<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title"> Personalizar la información visualizada</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        {{ Form::open(array('url' =>"empleadas",'method' => 'get')) }}
        <div class="row">
            <div class="col-md-8 ">
                <div class="form-group">
                    <label><i class="fa fa-female" aria-hidden="true"></i> Buscar coincidencias de nombres: </label>
                    {!! Form::text('n1', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    {!! Form::label('', '') !!}
                    <button type="submit" class="btn bg-purple form-control ">  <i class="fa fa-filter" aria-hidden="true"></i> Aplicar</button>
                </div>

            </div>
            <div class="col-md-2">
                <div class="form-group">
                    {!! Form::label('', '') !!}
                    <a type="button" class="btn bg-navy form-control" href="{{url('empleadas') }}"><i class="fa fa-refresh" aria-hidden="true"></i>  Reiniciar</a>
                </div>

            </div>
        </div>
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>



@push("head")
    {{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
    <head><meta name="csrf-token" content="{!! csrf_token() !!}"></head>
@endpush
