@php($tipo_proceso = $tipo_demanda == 1?30:31)
<div class="box box-default {{isset($collapse)?'':'collapsed-box'}}">
    <div class="box-header with-border">
        <h3 class="box-title">
            <b>Personalizar la información visualizada</b>
        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa {{isset($collapse)?'fa-minus':' fa-plus'}}"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        {{ Form::open(array('url' =>"#",'method' => 'get')) }}
        <div class="row">
            <div class="col-md-3 ">
                <div class="form-group">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Desde: </label>
                    {!! Form::text('fh_inicial', null, ['class' => 'form-control datepicker','data-value'=>$filtros->fecha_del]) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Hasta: </label>
                    {!! Form::text('fh_fin', null, ['class' => 'form-control datepicker','data-value'=>$filtros->fecha_al]) !!}
                </div>
            </div>
            @if(isset($demanda))
                {!! Form::hidden('demanda', $demanda) !!}
            @endif

            @include('chivos.catalogo', ['chivo_control' => 'id_tipo_proceso'
                     ,'chivo_id_cat'=>$tipo_proceso
                     , 'chivo_default'=>null
                     , 'chivo_tamanio'=>'col-sm-3'
                     ,'chivo_texto'=>'Tipo de proceso'])


            <!-- Juzgado Field -->
            @include('chivos.catalogo', ['chivo_control' => 'id_juzgado'
                                          ,'chivo_id_cat'=>37
                                          , 'chivo_default'=>null
                                          , 'chivo_tamanio'=>'col-sm-3'
                                          ,'chivo_texto'=>'Juzgado:'])
        </div>
        <div class="row">
            <!-- Motivo Field -->
            @php($cat_motivo = $tipo_demanda == 1?36:38)
            @include('chivos.catalogo', ['chivo_control' => 'id_motivo'
                                  ,'chivo_id_cat'=>$cat_motivo
                                  , 'chivo_default'=>null
                                   , 'chivo_tamanio'=>'col-sm-3'
                                  ,'chivo_texto'=>'Motivo:'])

            <!-- Id Estado Field -->
            <div class="form-group col-sm-3">
                {!! Form::label('id_estado', 'Estado:') !!}
                {!! Form::select('id_estado',\App\Models\cat_item::listado_items(29,'Seleccionar'),null,['class' => 'form-control']) !!}
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    {!! Form::label('', '') !!}
                    <button type="submit" class="btn bg-purple form-control ">  <i class="fa fa-filter" aria-hidden="true"></i> Filtrar</button>
                </div>

            </div>
            <div class="col-md-2">
                <div class="form-group">
                    {!! Form::label('', '') !!}
                    @if($tipo_demanda == 1)
                        <a type="button" class="btn bg-navy form-control" href="{{route('procesos.index',['demanda'=>'legal'])}}"><i class="fa fa-refresh" aria-hidden="true"></i>  Reiniciar</a>
                    @else
                        <a type="button" class="btn bg-navy form-control" href="{{route('procesos.index',['demanda'=>'familia'])}}"><i class="fa fa-refresh" aria-hidden="true"></i>  Reiniciar</a>
                    @endif
                </div>

            </div>
        </div>

        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>



@push("head")
    {{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
    <head><meta name="csrf-token" content="{!! csrf_token() !!}"></head>
@endpush



@push('javascript')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $('.datepicker').pickadate({
            selectMonths: true // Creates a dropdown to control month
            , selectYears: 15 // Creates a dropdown of 15 years to control year
            //The format to show on the `input` element
            , format: 'dd-mmmm-yyyy'   //Como se muestra al usuario
            , formatSubmit: 'yyyy-mm-dd',  //IMPORTANTE: para el submit
            //The title label to use for the month nav buttons
            labelMonthNext: 'Mes siguiente',
            labelMonthPrev: 'Mes anterior',
            //The title label to use for the dropdown selectors
            labelMonthSelect: 'Elegir mes',
            labelYearSelect: 'Elegir año',
            //Months and weekdays
            monthsFull: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre' ],
            monthsShort: [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
            weekdaysFull: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
            weekdaysShort: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
            //Materialize modified
            weekdaysLetter: [ 'D', 'L', 'M', 'M', 'J', 'V', 'S' ],
            //Today and clear
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Cerrar'
            //,editable: true
        });
    </script>
    <script>
        $('#id_estado').select2({
            placeholder: 'Select an option'
        });
    </script>
@endpush