<div class="box box-default">
    <div class="box-header with-border">
        <h3 align="center"  style="margin-top:1%; margin-bottom: -1%;">
            <b><i class="fa fa-home" aria-hidden="true"></i> Agenda albergue</b>
        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        {{ Form::open(array('url' =>"#",'method' => 'get')) }}
        <div class="row">
            <div class="col-md-2 ">
                <div class="form-group">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Desde: </label>
                    {!! Form::text('fh_inicial', null, ['class' => 'form-control datepicker','data-value'=>$filtros->fecha_del]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Hasta: </label>
                    {!! Form::text('fh_fin', null, ['class' => 'form-control datepicker','data-value'=>$filtros->fecha_al]) !!}
                </div>
            </div>
            <div class="form-group col-sm-2">
                {!! Form::label('id_abogada', 'Trabajadora social:') !!}
                {!! Form::select('id_abogada', \App\Models\empleada::listado_items('Seleccionar'),null,['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-2">
                {!! Form::label('id_accion', 'Acción:') !!}
                {!! Form::select('id_accion',  \App\Models\cat_item::listado_items(55,'Seleccionar'),null,['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-2">
                {!! Form::label('vehiculo', 'Vehículo:') !!}
                {!! Form::select('vehiculo', array('0' => 'Seleccionar','SI' => 'Si', 'NO' => 'No'), null,['class' => 'form-control']) !!}
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    {!! Form::label('', '') !!}
                    <button type="submit" class="btn bg-purple form-control ">  <i class="fa fa-filter" aria-hidden="true"></i> Filtrar</button>
                </div>

            </div>
            <div class="col-md-2">
                <div class="form-group">
                    {!! Form::label('', '') !!}
                    <a type="button" class="btn bg-navy form-control" href="{{ $filtros->url }}"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                </div>

            </div>
        </div>
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>



@push("head")
    {{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
    <head><meta name="csrf-token" content="{!! csrf_token() !!}"></head>
@endpush

@push('javascript')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $('#id_abogada').select2({
            placeholder: 'Select an option'
        });
        $('#id_accion').select2({
            placeholder: 'Select an option'
        });
        $('#id_tipo_proceso').select2({
            placeholder: 'Select an option'
        });
        $('#id_quien_agenda').select2({
            placeholder: 'Select an option'
        });
    </script>
@endpush