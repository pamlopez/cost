@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cost Contactenos
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costContactenos, ['route' => ['costContactenos.update', $costContactenos->id], 'method' => 'patch']) !!}

                        @include('cost_contactenos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection