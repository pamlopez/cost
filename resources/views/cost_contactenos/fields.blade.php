<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', '* Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control','required'=>'required']) !!}
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono', 'Teléfono:') !!}
    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion', '*Descripción:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control','required'=>'required']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12" align="center">
    {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
</div>
