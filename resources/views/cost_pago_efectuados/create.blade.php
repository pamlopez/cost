@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Nuevo Pago Efectuado
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'costPagoEfectuados.store']) !!}

                        @include('cost_pago_efectuados.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
