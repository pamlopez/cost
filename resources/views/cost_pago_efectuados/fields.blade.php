{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costPagoEfectuado->id_proyecto, ['class' => 'form-control']) !!}
{!! Form::hidden('id_fase',isset($id_fase)?$id_fase:$costPagoEfectuado->id_fase, ['class' => 'form-control']) !!}



@include('chivos.catalogo', ['chivo_control' => 'tipo'
                           ,'chivo_id_cat'=>70
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Tipo de pago efectuado:'])

<!-- No Pago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_pago', 'No Pago:') !!}
    {!! Form::number('no_pago', null, ['class' => 'form-control']) !!}
</div>



<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_pago', 'Fecha del Pago:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_pago', null, ['class' => 'form-control datepicker','data-value'=>$costPagoEfectuado->fecha_pago]) !!}
    @else
        {!! Form::text('fecha_pago', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>

<!-- Monto Pagado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_pagado', 'Monto Pagado:') !!}
    {!! Form::text('monto_pagado', null, ['class' => 'form-control']) !!}
</div>
<div class="col-md-12">
    <hr>
</div>

<!-- Tiene Ampliacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tiene_ampliacion', 'Tiene Ampliacion:') !!}
    {!! Form::select('tiene_ampliacion', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costPagoEfectuado->tiene_ampliacion)?$costPagoEfectuado->tiene_ampliacion:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
</div>


<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_aprobacion_ampliacion', 'Fecha de aprobación de la ampliacion:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_aprobacion_ampliacion', null, ['class' => 'form-control datepicker','data-value'=>$costPagoEfectuado->fecha_aprobacion_ampliacion]) !!}
    @else
        {!! Form::text('fecha_aprobacion_ampliacion', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>

<!-- Monto Ampliacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_ampliacion', 'Monto de la ampliación:') !!}
    {!! Form::text('monto_ampliacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Ampliacion Aprobada Por Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ampliacion_aprobada_por', 'Ampliación aprobada por:') !!}
    {!! Form::text('ampliacion_aprobada_por', null, ['class' => 'form-control']) !!}
</div>

<!-- Acta Ampliacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acta_ampliacion', 'Acta de Ampliación:') !!}
    {!! Form::text('acta_ampliacion', null, ['class' => 'form-control']) !!}
</div>


<div class="col-md-12">
    <hr>
</div>
<!-- Observaciones Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}

    @if(isset($id_fase))
        <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$costPagoEfectuado->id_proyecto,'id_fase'=>$costPagoEfectuado->id_fase]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif

</div>
