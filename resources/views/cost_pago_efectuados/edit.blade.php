@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Edita Pago Efectuado
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($costPagoEfectuado, ['route' => ['costPagoEfectuados.update', $costPagoEfectuado->id], 'method' => 'patch']) !!}
                    @include('cost_pago_efectuados.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection