@extends('layouts.app')
@include('flash::message')
@section('content')
    <section class="content-header">
        <h1>
            Pago Efectuado
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_pago_efectuados.show_fields')
                </div>
                <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$costPagoEfectuado->id_proyecto,'id_fase'=>$costPagoEfectuado->id_fase]) !!}"
                   class="btn btn-default">Atrás</a>
            </div>
        </div>
    </div>
@endsection
