
@if($costPagoEfectuado->tipo>0)
    <!-- Tipo Field -->
    <div class="form-group col-md-6">
        {!! Form::label('fmt_tipo', 'Tipo:') !!}
        <p>{!! $costPagoEfectuado->fmt_tipo !!}</p>
    </div>
@else
    <div class="form-group col-md-6">
        {!! Form::label('tipo', 'Tipo:') !!}
        <p>{!! $costPagoEfectuado->tipo !!}</p>
    </div>
@endif
<!-- No Pago Field -->
<div class="form-group col-md-6">
    {!! Form::label('no_pago', 'No Pago:') !!}
    <p>{!! $costPagoEfectuado->no_pago !!}</p>
</div>

<!-- Fecha Pago Field -->
<div class="form-group col-md-6">
    {!! Form::label('fecha_pago', 'Fecha Pago:') !!}
    <p>{!! $costPagoEfectuado->fecha_pago !!}</p>
</div>

<!-- Monto Pagado Field -->
<div class="form-group col-md-6">
    {!! Form::label('monto_pagado', 'Monto Pagado:') !!}
    <p>{!! $costPagoEfectuado->monto_pagado !!}</p>
</div>

<div class="form-group col-md-12">
    <hr>
</div>
<!-- Tiene Ampliacion Field -->
<div class="form-group col-md-6">
    {!! Form::label('fmt_tiene_ampliacion', 'Tiene Ampliacion:') !!}
    <p>{!! $costPagoEfectuado->fmt_tiene_ampliacion !!}</p>
</div>

<!-- Fecha Aprobacion Ampliacion Field -->
<div class="form-group col-md-6">
    {!! Form::label('fecha_aprobacion_ampliacion', 'Fecha Aprobacion Ampliacion:') !!}
    <p>{!! $costPagoEfectuado->fecha_aprobacion_ampliacion !!}</p>
</div>

<!-- Monto Ampliacion Field -->
<div class="form-group col-md-6">
    {!! Form::label('monto_ampliacion', 'Monto Ampliacion:') !!}
    <p>{!! $costPagoEfectuado->monto_ampliacion !!}</p>
</div>

<!-- Ampliacion Aprobada Por Field -->
<div class="form-group col-md-6">
    {!! Form::label('ampliacion_aprobada_por', 'Ampliacion Aprobada Por:') !!}
    <p>{!! $costPagoEfectuado->ampliacion_aprobada_por !!}</p>
</div>

<!-- Acta Ampliacion Field -->
<div class="form-group col-md-6">
    {!! Form::label('acta_ampliacion', 'Acta Ampliacion:') !!}
    <p>{!! $costPagoEfectuado->acta_ampliacion !!}</p>
</div>


<div class="form-group col-md-12">
    <hr>
</div>

<!-- Monto Acumulado Field -->
<div class="form-group col-md-6">
    {!! Form::label('monto_acumulado', 'Monto Acumulado:') !!}
    <p>{!! $costPagoEfectuado->monto_acumulado !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group col-md-6">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $costPagoEfectuado->observaciones !!}</p>
</div>

