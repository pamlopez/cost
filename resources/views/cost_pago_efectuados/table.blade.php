<table class="table table-responsive" id="costPagoEfectuados-table">
    <thead>
    <tr>
        <th>#</th>
        <th>Tipo</th>
        <th>No Pago</th>
        <th>Fecha Pago</th>
        <th>Monto Pagado</th>
        <th>Tiene Ampliacion</th>
        <th>Fecha Aprobacion Ampliacion</th>
        <th>Monto Ampliacion</th>

        <th colspan="3">Acción</th>
    </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($costPagoEfectuados as $costPagoEfectuado)        @php($i++)
    <tr>
        <td>{!! $i !!}</td>

            <td>{!! $costPagoEfectuado->fmt_tipo !!}</td>
            <td>{!! $costPagoEfectuado->no_pago !!}</td>
            <td>{!! $costPagoEfectuado->fecha_pago !!}</td>
            <td>{!! $costPagoEfectuado->monto_pagado !!}</td>
            <td>{!! $costPagoEfectuado->fmt_tiene_ampliacion !!}</td>
            <td>{!! $costPagoEfectuado->fecha_aprobacion_ampliacion !!}</td>
            <td>{!! $costPagoEfectuado->monto_ampliacion !!}</td>

            <td>
                {!! Form::open(['route' => ['costPagoEfectuados.destroy', $costPagoEfectuado->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costPagoEfectuados.show', [$costPagoEfectuado->id]) !!}"
                       class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                        <a href="{!! route('costPagoEfectuados.edit', [$costPagoEfectuado->id]) !!}"
                           class='btn btn-warning btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>