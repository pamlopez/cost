{{-- Control tipo dropdouwn de un catalogo --}}
<?php
        /**
         * Default: Si en la base de datos hay un campo "predeterminado", se utiliza para identificar el preselecciondo
         * Si la tabla cat_item on tiene este campo,  cometar este código para evitar el error que produciría
         */
        //Si no tiene default, obtener el de la BD si lo hubiera
        if($chivo_default==null) {
            $tmp_defa=0;
            if(!empty($tmp_defa)) {
                $chivo_default=$tmp_defa->id_participante;
            }
        }
        $chivo_tamanio = isset($chivo_tamanio)?$chivo_tamanio:'col-sm-6';
        $chivo_label_color = isset($chivo_label_color)?$chivo_label_color:'#000000';

?>

<!-- Catalogo  -->
<div class="form-group {{$chivo_tamanio}} {{ $errors->has($chivo_control) ? 'has-error' :'' }}">
    <label style="color: {{$chivo_label_color}}">{{$chivo_texto}}</label>
    {!! Form::select($chivo_control, \App\Models\participante::listado_items('Seleccionar'), $chivo_default,['class' => 'form-control','id'=>$chivo_control,'name'=>$chivo_control]) !!}
    {!! $errors->first($chivo_control,'<span class="help-block" style="color:red;">:message</span>') !!}
</div>


@push('javascript')
    <script>
        var control ='#<?=($chivo_control);?>';
        $(control).select2({
            placeholder: 'Select an option'
        });
    </script>
@endpush