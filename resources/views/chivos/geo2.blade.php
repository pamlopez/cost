{{-- Select dependiente para pedir departamento/municipio --}}


<?php
        //Esta primera parte me sirve para el despliegue inicial
        if($chivo_default>0) {
            $tmp_info=\App\Models\geo::find($chivo_default);
            if(!empty($tmp_info)) {
                $tmp_id_depto=$tmp_info->id_padre;
            }
            else {
                $tmp_id_depto=null;
            }
        }
        elseif($chivo_depto>0) {
            $tmp_id_depto=$chivo_depto;
        }
        else {
            $tmp_id_depto=null;
        }
        //dd($filtros);
    // dd(\App\Models\geo::listar_hijos($tmp_id_depto));
    $depto_nombre = "Departamento ";
    $depto_nombre .= isset($label)?$label:null;

    $muni_nombre = "Municipio ";
    $muni_nombre .= isset($label)?$label:null;

?>
{{-- Desplegar los controles --}}

<!-- Departamento -->
@php($chivo_tamanio = isset($chivo_tamanio)?$chivo_tamanio:'col-sm-6')
<div class="form-group {{$chivo_tamanio}}">
    {!! Form::label($chivo_control."_depto", $depto_nombre) !!}
    {!! Form::select($chivo_control."_depto", \App\Models\geo::listar_hijos(null,"(Mostrar todos)"), $tmp_id_depto,['class' => 'form-control']) !!}
</div>

<!-- Municipio -->
<div class="form-group {{$chivo_tamanio}}">
    {!! Form::label($chivo_control, $muni_nombre) !!}
    {!! Form::select($chivo_control, \App\Models\geo::listar_hijos($tmp_id_depto,"(Mostrar todos)"), $chivo_default,['class' => 'form-control','id'=>$chivo_control,'name'=>$chivo_control]) !!}
</div>


{{-- Javascript para hacerlos dependientes --}}
@push('javascript')
    <script>

        $("#{{ $chivo_control }}").depdrop({
            url: '{{ url('json/geo_todo') }}',
            depends: ['{{ $chivo_control }}_depto'],
            initialize: {{ $tmp_id_depto==null ? "true" : "false" }},
            loadingText:'Cargando datos...',
            placeholder:false,
            emptyMsg:'Sin datos :-('
        });
    </script>
    <script>
    /*    var control_depto ='#<?=($chivo_control);?>'+"_depto";
        $(control_depto).select2({
            placeholder: 'Select an option'
        });
        var control_muni ='#<?=($chivo_control);?>';
        $(control_muni).select2({
            placeholder: 'Select an option'
        });*/
    </script>
@endpush