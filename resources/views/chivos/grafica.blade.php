<div class="row">
    <div class="col-xs-12">
        <div id="grafica">
            cargando...
        </div>
    </div>
</div>

<?php


//Generar datos
$datos=array();
$datos[0]['name']='A';
$datos[0]['data']=array(10,20,30);
$datos[1]['name']='B';
$datos[1]['data']=array(30,20,10);
$datos[2]['name']='C';
$datos[2]['data']=array(10,10,10);
$datos[3]['name']='D';
$datos[3]['data']=array(30,30,30);

// Configurar grafica
$grafico=array();
$grafico['chart']['type']='column';
$grafico['title']['text']="Demostracion";
$grafico['subtitle']['text']="Highcharts";
$grafico['xAxis']['categories']=array("Primero","Segundo","Tercero");
$grafico['xAxis']['title']['text']="Eje X";
$grafico['yAxis']['title']['text']="Eje Y";
$grafico['plotOptions']['bar']['datalabels']['enabled']=true;
$grafico['credits']['enabled']=false;

//Asignacion de datos
$grafico['series']=$datos;
$json_grafico=json_encode($grafico);

?>

@push('javascript')
<script>
    $(function () {
        $('#grafica').highcharts(
            {!! $json_grafico !!}
        )}
    );

</script>
@endpush