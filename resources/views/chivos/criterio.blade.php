{{-- Control tipo dropdouwn para criterio fijo --}}
@php($chivo_tamanio = isset($chivo_tamanio)?$chivo_tamanio:'col-sm-6')
@php($chivo_label_color = isset($chivo_label_color)?$chivo_label_color:'#000000')

{{-- Criterio fijo --}}
<div class="form-group {{$chivo_tamanio}} {{ $errors->has($chivo_control) ? 'has-error' :'' }}">
    <label style="color: {{$chivo_label_color}}">{{$chivo_texto}}</label>
    {!! Form::select($chivo_control,\App\Models\criterio_fijo::listado_items($chivo_id_grupo,'Seleccionar'),$chivo_default,['class' => 'form-control','id'=>$chivo_control,'name'=>$chivo_control]) !!}
</div>

@push('javascript')
    <script>
        var control ='#<?=($chivo_control);?>';
        $(control).select2({
            placeholder: 'Select an option'
        });
    </script>
@endpush