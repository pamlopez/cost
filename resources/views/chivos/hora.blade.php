<!-- time Picker -->
<div class="bootstrap-timepicker">
    <div class="form-group">
        {!! Form::label($chivo_control, $chivo_texto) !!}

        <div class="input-group">
            {!! Form::text($chivo_control, null, ['class' => 'form-control timepicker', 'data-two-digits-hour'=>'true']) !!}
            <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
            </div>
        </div>
    </div>
</div>

<?php
        if($chivo_control=='' || $chivo_control==null) {
            $js_default="false";
        }
        else {
            $js_default=$chivo_control;
        }
?>


{{-- Campo tipo fecha--}}
@push('javascript')
<script>
    $(".timepicker").pickatime({
        format: 'HH:i',
        formatSubmit: 'HH:i',
        defaultTime: "{{ $js_default }}"
        , interval: 15
        //,editable: true

    });
</script>
@endpush
