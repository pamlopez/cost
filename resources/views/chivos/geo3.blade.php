{{-- Select dependiente para pedir departamento/municipio/lugar poblado --}}


<?php
        //Esta primera parte me sirve para el despliegue inicial
        if($chivo_default>0) {
            $tmp_info=\App\Models\gestion\geo::find($chivo_default);
            if(!empty($tmp_info)) {
                $tmp_id_muni=$tmp_info->id_padre;
                $tmp_info=\App\Models\gestion\geo::find($tmp_id_muni);
                if(!empty($tmp_info)) {
                    $tmp_id_depto=$tmp_info->id_padre;
                }
                else {
                    $temp_id_depto=null;
                }
            }
            else {
                $tmp_id_depto=null;
                $tmp_id_muni=null;
            }
        }
        else {
            $tmp_id_depto=null;
            $tmp_id_muni=null;
        }

        //echo "<br>Default: $chivo_default. $tmp_id_depto/$tmp_id_muni";
?>
{{-- Desplegar los controles --}}

<!-- Departamento -->
<div class="form-group col-sm-4">
    {!! Form::label($chivo_control."_depto", "Departamento") !!}
    {!! Form::select($chivo_control."_depto", \App\Models\gestion\geo::listar_hijos(null), $tmp_id_depto,['class' => 'form-control']) !!}
</div>
<!-- Municipio -->
<div class="form-group col-sm-4">

    {!! Form::label($chivo_control."_muni", "Municipio") !!}
    {!! Form::select($chivo_control."_muni", \App\Models\gestion\geo::listar_hijos($tmp_id_depto), $tmp_id_muni,['class' => 'form-control']) !!}
</div>
<!-- Lugar Poblado -->
<div class="form-group col-sm-4">
    {!! Form::label($chivo_control, "Lugar poblado") !!}
    {!! Form::select($chivo_control, \App\Models\gestion\geo::listar_hijos($tmp_id_muni), $chivo_default,['class' => 'form-control']) !!}
</div>



{{--
 Si tienen valor predeterminado, se cargan con dichos valores
 De lo contrario, initialize=true para que cargue los valores iniciales
--}}

@push('javascript')
<script>
    // Controles dependientes
    //Municipio
    $("#{{ $chivo_control }}_muni").depdrop({
        url: '{{ url('json/geo') }}',
        depends: ['{{ $chivo_control }}_depto'],
        initialize: {{ $tmp_id_depto==null ? "true" : "false" }},
        loadingText:'Cargando datos...',
        placeholder:false,
        emptyMsg:'Sin datos :-('
    });
    // Lugar poblado
    $("#{{ $chivo_control }}").depdrop({
        url: '{{ url('json/geo') }}',
        depends: ['{{ $chivo_control }}_muni'],
        initialize: {{ $tmp_id_muni==null ? "true" : "false" }},
        loadingText:'Cargando datos...',
        placeholder:false,
        emptyMsg:'Sin datos :-('
    });

</script>
@endpush







