
<!-- Documento Cambio Field -->
<div class="form-group col-md-6">
    {!! Form::label('documento_cambio', 'Documento de cambio:') !!}
    <p>{!! $costLiquidacionMonto->documento_cambio !!}</p>
</div>

<!-- Suma Mod Field -->
<div class="form-group col-md-6">
    {!! Form::label('suma_mod', 'Suma de las modificaciones al monto:') !!}
    <p>{!! $costLiquidacionMonto->suma_mod !!}</p>
</div>

<!-- Razon Cambio Field -->
<div class="form-group col-md-12">
    {!! Form::label('razon_cambio', 'Razones de cambios en el precio o monto del contrato:') !!}
    <p>{!! $costLiquidacionMonto->razon_cambio !!}</p>
</div>

<!-- Monto Actualizado Field -->
<div class="form-group col-md-2">
    {!! Form::label('monto_actualizado', 'Moneda actualizada del contrato (OC4IDS):') !!}
    <p>{!! $costLiquidacionMonto->FmtMonedaActual !!}</p>
</div>

<!-- Monto Actualizado Field -->
<div class="form-group col-md-4">
    {!! Form::label('monto_actualizado', 'Monto actualizado del contrato (OC4IDS):') !!}
    <p>{!! $costLiquidacionMonto->monto_actualizado !!}</p>
</div>


<!-- Monto Actualizado Field -->
<div class="form-group col-md-2">
    {!! Form::label('monto_actualizado', 'Moneda antigua del contrato (OC4IDS):') !!}
    <p>{!! $costLiquidacionMonto->FmtMonedaAntigua !!}</p>
</div>

<!-- Monto Actualizado Field -->
<div class="form-group col-md-4">
    {!! Form::label('monto_actualizado', 'Monto antigua del contrato (OC4IDS):') !!}
    <p>{!! $costLiquidacionMonto->monto_antiguo !!}</p>
</div>

<div class="clearfix"></div>
<!-- No Fianza Field -->
<div class="form-group col-md-6">
    {!! Form::label('no_fianza', 'No. Fianza actualizada al nuevo monto:') !!}
    <p>{!! $costLiquidacionMonto->no_fianza !!}</p>
</div>

