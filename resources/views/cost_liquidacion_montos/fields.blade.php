{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costLiquidacionMonto->id_proyecto, ['class' => 'form-control']) !!}

<!-- Documento Cambio Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('documento_cambio', 'Documento de cambio:') !!}
    {!! Form::textarea('documento_cambio', null, ['class' => 'form-control']) !!}
</div>

<!-- Suma Mod Field -->
<div class="form-group col-sm-6">
    {!! Form::label('suma_mod', 'Suma de las modificaciones al monto:') !!}
    {!! Form::text('suma_mod', null, ['class' => 'form-control']) !!}
</div>

<!-- Razon Cambio Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('razon_cambio', 'Razones de cambios en el precio o monto del contrato:') !!}
    {!! Form::textarea('razon_cambio', null, ['class' => 'form-control']) !!}
</div>

@include('chivos.catalogo', ['chivo_control' => 'moneda'
                          ,'chivo_id_cat'=>83
                          , 'chivo_default'=>null
                          , 'chivo_tamanio'=>'col-md-2'
                          ,'chivo_texto'=>'Moneda actualizada (OC4IDS):'])
<!-- Monto Actualizado Field -->
<div class="form-group col-sm-4">
    {!! Form::label('monto_actualizado', 'Monto actualizado del contrato (OC4IDS):') !!}
    {!! Form::text('monto_actualizado', null, ['class' => 'form-control']) !!}
</div>

@include('chivos.catalogo', ['chivo_control' => 'moneda_antigua'
                          ,'chivo_id_cat'=>83
                          , 'chivo_default'=>null
                          , 'chivo_tamanio'=>'col-md-2'
                          ,'chivo_texto'=>'Moneda antigua (OC4IDS):'])
<!-- No Fianza Field -->
<div class="form-group col-sm-4">
    {!! Form::label('monto_antiguo', 'Antiguo monto (OC4IDS):') !!}
        {!! Form::text('monto_antiguo', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza', 'No. Fianza actualizada al nuevo monto:') !!}
    {!! Form::text('no_fianza', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    @if(isset($id_proyecto))
        <a href="{!! route('costLiquidacionMontos.show', [$id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costLiquidacionMontos.show', [$costLiquidacionMonto->id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif
</div>
