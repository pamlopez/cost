@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Nuevo monto de la liquidación
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'costLiquidacionMontos.store']) !!}

                        @include('cost_liquidacion_montos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
