<table class="table table-responsive" id="costLiquidacionMontos-table">
    <thead>
    <tr>
        <th width="5%">#</th>
        <th width="20%">Documento de Cambio</th>
        <th width="30%">Razon de Cambio</th>
        <th width="10%">Moneda Actualizada (OC4IDS)</th>
        <th width="15%" >Monto Actualizado (OC4IDS)</th>
        <th width="10%"> Moneda Antigua (OC4IDS)</th>
        <th width="15%" >Monto Antigua (OC4IDS)</th>
        <th width="20%">No. Fianza</th>
        @if(!isset($a))
        <th width="10%" colspan="3">Action</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($costLiquidacionMontos as $costLiquidacionMonto)
        @php($i++)
        @php($money = number_format($costLiquidacionMonto->monto_actualizado, 2, '.', ',') )
        @php($money_antiguo = number_format($costLiquidacionMonto->monto_antiguo , 2, '.', ',') )
         <tr>
            <td>{!! $i !!}</td>
            <td><div class="table-responsive"> {!!$costLiquidacionMonto->documento_cambio !!}</div></td>
            <td>{!! $costLiquidacionMonto->razon_cambio !!}</td>
            <td>{!! $costLiquidacionMonto->FmtMonedaActual !!}</td>
            <td>{!! $money !!}</td>
             <td>{!! $costLiquidacionMonto->FmtMonedaAntigua !!}</td>
             <td>{!! $money_antiguo !!}</td>
            <td>{!! $costLiquidacionMonto->no_fianza !!}</td>
             @if(!isset($a))
            <td>
                {!! Form::open(['route' => ['costLiquidacionMontos.destroy', $costLiquidacionMonto->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costLiquidacionMontos.show', [$costLiquidacionMonto->id]) !!}"
                       class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                        <a href="{!! route('costLiquidacionMontos.edit', [$costLiquidacionMonto->id]) !!}"
                           class='btn btn-warning btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
             @endif
        </tr>
    @endforeach
    </tbody>
</table>