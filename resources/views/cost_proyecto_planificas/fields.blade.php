<!-- Id Proyecto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_proyecto', 'Id Proyecto:') !!}
    {!! Form::number('id_proyecto', null, ['class' => 'form-control']) !!}
</div>

<!-- Bases Concurso Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bases_concurso', 'Bases Concurso:') !!}
    {!! Form::date('bases_concurso', null, ['class' => 'form-control']) !!}
</div>

<!-- Bases F Publicacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bases_f_publicacion', 'Bases F Publicacion:') !!}
    {!! Form::date('bases_f_publicacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Bases F Ultima Mod Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bases_f_ultima_mod', 'Bases F Ultima Mod:') !!}
    {!! Form::date('bases_f_ultima_mod', null, ['class' => 'form-control']) !!}
</div>

<!-- No Inconformidades Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_inconformidades', 'No Inconformidades:') !!}
    {!! Form::number('no_inconformidades', null, ['class' => 'form-control']) !!}
</div>

<!-- No Inconformidades R Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_inconformidades_r', 'No Inconformidades R:') !!}
    {!! Form::number('no_inconformidades_r', null, ['class' => 'form-control']) !!}
</div>

<!-- Oferente Field -->
<div class="form-group col-sm-6">
    {!! Form::label('oferente', 'Oferente:') !!}
    {!! Form::text('oferente', null, ['class' => 'form-control']) !!}
</div>

<!-- Acta Adjudicacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acta_adjudicacion', 'Acta Adjudicacion:') !!}
    {!! Form::text('acta_adjudicacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Alcance Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alcance_contrato', 'Alcance Contrato:') !!}
    {!! Form::text('alcance_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Programa Ejecucion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('programa_ejecucion', 'Programa Ejecucion:') !!}
    {!! Form::number('programa_ejecucion', null, ['class' => 'form-control']) !!}
</div>

<!-- Estado Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estado_contrato', 'Estado Contrato:') !!}
    {!! Form::number('estado_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo No Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_no_contrato', 'Tipo No Contrato:') !!}
    {!! Form::text('tipo_no_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Empresa Constructora Field -->
<div class="form-group col-sm-6">
    {!! Form::label('empresa_constructora', 'Empresa Constructora:') !!}
    {!! Form::text('empresa_constructora', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_contrato', 'Monto Contrato:') !!}
    {!! Form::number('monto_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- F Aprobacion Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('f_aprobacion_contrato', 'F Aprobacion Contrato:') !!}
    {!! Form::date('f_aprobacion_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Firmante Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_firmante', 'Nombre Firmante:') !!}
    {!! Form::text('nombre_firmante', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombramiento F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombramiento_f', 'Nombramiento F:') !!}
    {!! Form::date('nombramiento_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Plazo Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('plazo_contrato', 'Plazo Contrato:') !!}
    {!! Form::text('plazo_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- F Inicio Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('f_inicio_contrato', 'F Inicio Contrato:') !!}
    {!! Form::date('f_inicio_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- F Autoriza Bitacora Field -->
<div class="form-group col-sm-6">
    {!! Form::label('f_autoriza_bitacora', 'F Autoriza Bitacora:') !!}
    {!! Form::date('f_autoriza_bitacora', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Anticipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_anticipo', 'Monto Anticipo:') !!}
    {!! Form::number('monto_anticipo', null, ['class' => 'form-control']) !!}
</div>

<!-- No Estima Pagada Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_estima_pagada', 'No Estima Pagada:') !!}
    {!! Form::number('no_estima_pagada', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Estima Pagada Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_estima_pagada', 'Monto Estima Pagada:') !!}
    {!! Form::number('monto_estima_pagada', null, ['class' => 'form-control']) !!}
</div>

<!-- No Pagos Voac Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_pagos_voac', 'No Pagos Voac:') !!}
    {!! Form::number('no_pagos_voac', null, ['class' => 'form-control']) !!}
</div>

<!-- Pagos Voac Aprobado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pagos_voac_aprobado', 'Pagos Voac Aprobado:') !!}
    {!! Form::text('pagos_voac_aprobado', null, ['class' => 'form-control']) !!}
</div>

<!-- Acta Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acta_inicio', 'Acta Inicio:') !!}
    {!! Form::text('acta_inicio', null, ['class' => 'form-control']) !!}
</div>

<!-- Acta Inicio F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acta_inicio_f', 'Acta Inicio F:') !!}
    {!! Form::date('acta_inicio_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Acta Recepcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acta_recepcion', 'Acta Recepcion:') !!}
    {!! Form::text('acta_recepcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Acta Recepcion F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acta_recepcion_f', 'Acta Recepcion F:') !!}
    {!! Form::date('acta_recepcion_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Acta Liquidacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acta_liquidacion', 'Acta Liquidacion:') !!}
    {!! Form::text('acta_liquidacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Acta Aprobacion Liquidacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acta_aprobacion_liquidacion', 'Acta Aprobacion Liquidacion:') !!}
    {!! Form::text('acta_aprobacion_liquidacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Finiquito Fechas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('finiquito_fechas', 'Finiquito Fechas:') !!}
    {!! Form::text('finiquito_fechas', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Sos Obra Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza_sos_obra', 'No Fianza Sos Obra:') !!}
    {!! Form::text('no_fianza_sos_obra', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Sos Obra F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza_sos_obra_f', 'No Fianza Sos Obra F:') !!}
    {!! Form::date('no_fianza_sos_obra_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Fianza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_fianza', 'Monto Fianza:') !!}
    {!! Form::number('monto_fianza', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Cumple Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza_cumple_contrato', 'No Fianza Cumple Contrato:') !!}
    {!! Form::text('no_fianza_cumple_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Cumple Contrato F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza_cumple_contrato_f', 'No Fianza Cumple Contrato F:') !!}
    {!! Form::text('no_fianza_cumple_contrato_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Fianza Cumple Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_fianza_cumple', 'Monto Fianza Cumple:') !!}
    {!! Form::number('monto_fianza_cumple', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Anticipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza_anticipo', 'No Fianza Anticipo:') !!}
    {!! Form::text('no_fianza_anticipo', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Anticipo F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza_anticipo_f', 'No Fianza Anticipo F:') !!}
    {!! Form::date('no_fianza_anticipo_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Fianza Anticipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_fianza_anticipo', 'Monto Fianza Anticipo:') !!}
    {!! Form::number('monto_fianza_anticipo', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Conser Obra Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza_conser_obra', 'No Fianza Conser Obra:') !!}
    {!! Form::text('no_fianza_conser_obra', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Conser Obra F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza_conser_obra_f', 'No Fianza Conser Obra F:') !!}
    {!! Form::date('no_fianza_conser_obra_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Fianza Conser Obra Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_fianza_conser_obra', 'Monto Fianza Conser Obra:') !!}
    {!! Form::number('monto_fianza_conser_obra', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Saldo Deudor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza_saldo_deudor', 'No Fianza Saldo Deudor:') !!}
    {!! Form::text('no_fianza_saldo_deudor', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Saldo Deudor F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza_saldo_deudor_f', 'No Fianza Saldo Deudor F:') !!}
    {!! Form::date('no_fianza_saldo_deudor_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Fianza Saldo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_fianza_saldo', 'Monto Fianza Saldo:') !!}
    {!! Form::number('monto_fianza_saldo', null, ['class' => 'form-control']) !!}
</div>

<!-- Entidad Afianzadora Field -->
<div class="form-group col-sm-6">
    {!! Form::label('entidad_afianzadora', 'Entidad Afianzadora:') !!}
    {!! Form::text('entidad_afianzadora', null, ['class' => 'form-control']) !!}
</div>

<!-- Clase Fianza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('clase_fianza', 'Clase Fianza:') !!}
    {!! Form::text('clase_fianza', null, ['class' => 'form-control']) !!}
</div>

<!-- Doc Cambios Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('doc_cambios_contrato', 'Doc Cambios Contrato:') !!}
    {!! Form::text('doc_cambios_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Suma Mod Monto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('suma_mod_monto', 'Suma Mod Monto:') !!}
    {!! Form::number('suma_mod_monto', null, ['class' => 'form-control']) !!}
</div>

<!-- Razon Cambio Monto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('razon_cambio_monto', 'Razon Cambio Monto:') !!}
    {!! Form::text('razon_cambio_monto', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Actualizado Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_actualizado_contrato', 'Monto Actualizado Contrato:') !!}
    {!! Form::number('monto_actualizado_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Pagos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_pagos', 'Total Pagos:') !!}
    {!! Form::text('total_pagos', null, ['class' => 'form-control']) !!}
</div>

<!-- No Mod Plazo Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_mod_plazo_contrato', 'No Mod Plazo Contrato:') !!}
    {!! Form::number('no_mod_plazo_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Cantidad Tiempo Mod Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cantidad_tiempo_mod', 'Cantidad Tiempo Mod:') !!}
    {!! Form::number('cantidad_tiempo_mod', null, ['class' => 'form-control']) !!}
</div>

<!-- Nueva F Finaliza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nueva_f_finaliza', 'Nueva F Finaliza:') !!}
    {!! Form::date('nueva_f_finaliza', null, ['class' => 'form-control']) !!}
</div>

<!-- Razon Cambio Plazo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('razon_cambio_plazo', 'Razon Cambio Plazo:') !!}
    {!! Form::text('razon_cambio_plazo', null, ['class' => 'form-control']) !!}
</div>

<!-- No Mod Alcance Obra Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_mod_alcance_obra', 'No Mod Alcance Obra:') !!}
    {!! Form::number('no_mod_alcance_obra', null, ['class' => 'form-control']) !!}
</div>

<!-- Razon Cambio Alcance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('razon_cambio_alcance', 'Razon Cambio Alcance:') !!}
    {!! Form::text('razon_cambio_alcance', null, ['class' => 'form-control']) !!}
</div>

<!-- Alcance Real Obra Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alcance_real_obra', 'Alcance Real Obra:') !!}
    {!! Form::text('alcance_real_obra', null, ['class' => 'form-control']) !!}
</div>

<!-- Programa Trabajo Actualizado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('programa_trabajo_actualizado', 'Programa Trabajo Actualizado:') !!}
    {!! Form::text('programa_trabajo_actualizado', null, ['class' => 'form-control']) !!}
</div>

<!-- Informe Avance Pago Field -->
<div class="form-group col-sm-6">
    {!! Form::label('informe_avance_pago', 'Informe Avance Pago:') !!}
    {!! Form::text('informe_avance_pago', null, ['class' => 'form-control']) !!}
</div>

<!-- Planos Completos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('planos_completos', 'Planos Completos:') !!}
    {!! Form::number('planos_completos', null, ['class' => 'form-control']) !!}
</div>

<!-- Reporte Eva Au Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reporte_eva_au', 'Reporte Eva Au:') !!}
    {!! Form::text('reporte_eva_au', null, ['class' => 'form-control']) !!}
</div>

<!-- Informe Termina Proyecto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('informe_termina_proyecto', 'Informe Termina Proyecto:') !!}
    {!! Form::text('informe_termina_proyecto', null, ['class' => 'form-control']) !!}
</div>

<!-- Informe Evalua Pro Field -->
<div class="form-group col-sm-6">
    {!! Form::label('informe_evalua_pro', 'Informe Evalua Pro:') !!}
    {!! Form::text('informe_evalua_pro', null, ['class' => 'form-control']) !!}
</div>

<!-- Estado Actual Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estado_actual', 'Estado Actual:') !!}
    {!! Form::number('estado_actual', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('costProyectoPlanificas.index') !!}" class="btn btn-default">Cancel</a>
</div>
