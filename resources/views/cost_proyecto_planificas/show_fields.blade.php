<!-- Id Cost Proyecto Planifica Field -->
<div class="form-group">
    {!! Form::label('id_cost_proyecto_planifica', 'Id Cost Proyecto Planifica:') !!}
    <p>{!! $costProyectoPlanifica->id_cost_proyecto_planifica !!}</p>
</div>

<!-- Id Proyecto Field -->
<div class="form-group">
    {!! Form::label('id_proyecto', 'Id Proyecto:') !!}
    <p>{!! $costProyectoPlanifica->id_proyecto !!}</p>
</div>

<!-- Bases Concurso Field -->
<div class="form-group">
    {!! Form::label('bases_concurso', 'Bases Concurso:') !!}
    <p>{!! $costProyectoPlanifica->bases_concurso !!}</p>
</div>

<!-- Bases F Publicacion Field -->
<div class="form-group">
    {!! Form::label('bases_f_publicacion', 'Bases F Publicacion:') !!}
    <p>{!! $costProyectoPlanifica->bases_f_publicacion !!}</p>
</div>

<!-- Bases F Ultima Mod Field -->
<div class="form-group">
    {!! Form::label('bases_f_ultima_mod', 'Bases F Ultima Mod:') !!}
    <p>{!! $costProyectoPlanifica->bases_f_ultima_mod !!}</p>
</div>

<!-- No Inconformidades Field -->
<div class="form-group">
    {!! Form::label('no_inconformidades', 'No Inconformidades:') !!}
    <p>{!! $costProyectoPlanifica->no_inconformidades !!}</p>
</div>

<!-- No Inconformidades R Field -->
<div class="form-group">
    {!! Form::label('no_inconformidades_r', 'No Inconformidades R:') !!}
    <p>{!! $costProyectoPlanifica->no_inconformidades_r !!}</p>
</div>

<!-- Oferente Field -->
<div class="form-group">
    {!! Form::label('oferente', 'Oferente:') !!}
    <p>{!! $costProyectoPlanifica->oferente !!}</p>
</div>

<!-- Acta Adjudicacion Field -->
<div class="form-group">
    {!! Form::label('acta_adjudicacion', 'Acta Adjudicacion:') !!}
    <p>{!! $costProyectoPlanifica->acta_adjudicacion !!}</p>
</div>

<!-- Alcance Contrato Field -->
<div class="form-group">
    {!! Form::label('alcance_contrato', 'Alcance Contrato:') !!}
    <p>{!! $costProyectoPlanifica->alcance_contrato !!}</p>
</div>

<!-- Programa Ejecucion Field -->
<div class="form-group">
    {!! Form::label('programa_ejecucion', 'Programa Ejecucion:') !!}
    <p>{!! $costProyectoPlanifica->programa_ejecucion !!}</p>
</div>

<!-- Estado Contrato Field -->
<div class="form-group">
    {!! Form::label('estado_contrato', 'Estado Contrato:') !!}
    <p>{!! $costProyectoPlanifica->estado_contrato !!}</p>
</div>

<!-- Tipo No Contrato Field -->
<div class="form-group">
    {!! Form::label('tipo_no_contrato', 'Tipo No Contrato:') !!}
    <p>{!! $costProyectoPlanifica->tipo_no_contrato !!}</p>
</div>

<!-- Empresa Constructora Field -->
<div class="form-group">
    {!! Form::label('empresa_constructora', 'Empresa Constructora:') !!}
    <p>{!! $costProyectoPlanifica->empresa_constructora !!}</p>
</div>

<!-- Monto Contrato Field -->
<div class="form-group">
    {!! Form::label('monto_contrato', 'Monto Contrato:') !!}
    <p>{!! $costProyectoPlanifica->monto_contrato !!}</p>
</div>

<!-- F Aprobacion Contrato Field -->
<div class="form-group">
    {!! Form::label('f_aprobacion_contrato', 'F Aprobacion Contrato:') !!}
    <p>{!! $costProyectoPlanifica->f_aprobacion_contrato !!}</p>
</div>

<!-- Nombre Firmante Field -->
<div class="form-group">
    {!! Form::label('nombre_firmante', 'Nombre Firmante:') !!}
    <p>{!! $costProyectoPlanifica->nombre_firmante !!}</p>
</div>

<!-- Nombramiento F Field -->
<div class="form-group">
    {!! Form::label('nombramiento_f', 'Nombramiento F:') !!}
    <p>{!! $costProyectoPlanifica->nombramiento_f !!}</p>
</div>

<!-- Plazo Contrato Field -->
<div class="form-group">
    {!! Form::label('plazo_contrato', 'Plazo Contrato:') !!}
    <p>{!! $costProyectoPlanifica->plazo_contrato !!}</p>
</div>

<!-- F Inicio Contrato Field -->
<div class="form-group">
    {!! Form::label('f_inicio_contrato', 'F Inicio Contrato:') !!}
    <p>{!! $costProyectoPlanifica->f_inicio_contrato !!}</p>
</div>

<!-- F Autoriza Bitacora Field -->
<div class="form-group">
    {!! Form::label('f_autoriza_bitacora', 'F Autoriza Bitacora:') !!}
    <p>{!! $costProyectoPlanifica->f_autoriza_bitacora !!}</p>
</div>

<!-- Monto Anticipo Field -->
<div class="form-group">
    {!! Form::label('monto_anticipo', 'Monto Anticipo:') !!}
    <p>{!! $costProyectoPlanifica->monto_anticipo !!}</p>
</div>

<!-- No Estima Pagada Field -->
<div class="form-group">
    {!! Form::label('no_estima_pagada', 'No Estima Pagada:') !!}
    <p>{!! $costProyectoPlanifica->no_estima_pagada !!}</p>
</div>

<!-- Monto Estima Pagada Field -->
<div class="form-group">
    {!! Form::label('monto_estima_pagada', 'Monto Estima Pagada:') !!}
    <p>{!! $costProyectoPlanifica->monto_estima_pagada !!}</p>
</div>

<!-- No Pagos Voac Field -->
<div class="form-group">
    {!! Form::label('no_pagos_voac', 'No Pagos Voac:') !!}
    <p>{!! $costProyectoPlanifica->no_pagos_voac !!}</p>
</div>

<!-- Pagos Voac Aprobado Field -->
<div class="form-group">
    {!! Form::label('pagos_voac_aprobado', 'Pagos Voac Aprobado:') !!}
    <p>{!! $costProyectoPlanifica->pagos_voac_aprobado !!}</p>
</div>

<!-- Acta Inicio Field -->
<div class="form-group">
    {!! Form::label('acta_inicio', 'Acta Inicio:') !!}
    <p>{!! $costProyectoPlanifica->acta_inicio !!}</p>
</div>

<!-- Acta Inicio F Field -->
<div class="form-group">
    {!! Form::label('acta_inicio_f', 'Acta Inicio F:') !!}
    <p>{!! $costProyectoPlanifica->acta_inicio_f !!}</p>
</div>

<!-- Acta Recepcion Field -->
<div class="form-group">
    {!! Form::label('acta_recepcion', 'Acta Recepcion:') !!}
    <p>{!! $costProyectoPlanifica->acta_recepcion !!}</p>
</div>

<!-- Acta Recepcion F Field -->
<div class="form-group">
    {!! Form::label('acta_recepcion_f', 'Acta Recepcion F:') !!}
    <p>{!! $costProyectoPlanifica->acta_recepcion_f !!}</p>
</div>

<!-- Acta Liquidacion Field -->
<div class="form-group">
    {!! Form::label('acta_liquidacion', 'Acta Liquidacion:') !!}
    <p>{!! $costProyectoPlanifica->acta_liquidacion !!}</p>
</div>

<!-- Acta Aprobacion Liquidacion Field -->
<div class="form-group">
    {!! Form::label('acta_aprobacion_liquidacion', 'Acta Aprobacion Liquidacion:') !!}
    <p>{!! $costProyectoPlanifica->acta_aprobacion_liquidacion !!}</p>
</div>

<!-- Finiquito Fechas Field -->
<div class="form-group">
    {!! Form::label('finiquito_fechas', 'Finiquito Fechas:') !!}
    <p>{!! $costProyectoPlanifica->finiquito_fechas !!}</p>
</div>

<!-- No Fianza Sos Obra Field -->
<div class="form-group">
    {!! Form::label('no_fianza_sos_obra', 'No Fianza Sos Obra:') !!}
    <p>{!! $costProyectoPlanifica->no_fianza_sos_obra !!}</p>
</div>

<!-- No Fianza Sos Obra F Field -->
<div class="form-group">
    {!! Form::label('no_fianza_sos_obra_f', 'No Fianza Sos Obra F:') !!}
    <p>{!! $costProyectoPlanifica->no_fianza_sos_obra_f !!}</p>
</div>

<!-- Monto Fianza Field -->
<div class="form-group">
    {!! Form::label('monto_fianza', 'Monto Fianza:') !!}
    <p>{!! $costProyectoPlanifica->monto_fianza !!}</p>
</div>

<!-- No Fianza Cumple Contrato Field -->
<div class="form-group">
    {!! Form::label('no_fianza_cumple_contrato', 'No Fianza Cumple Contrato:') !!}
    <p>{!! $costProyectoPlanifica->no_fianza_cumple_contrato !!}</p>
</div>

<!-- No Fianza Cumple Contrato F Field -->
<div class="form-group">
    {!! Form::label('no_fianza_cumple_contrato_f', 'No Fianza Cumple Contrato F:') !!}
    <p>{!! $costProyectoPlanifica->no_fianza_cumple_contrato_f !!}</p>
</div>

<!-- Monto Fianza Cumple Field -->
<div class="form-group">
    {!! Form::label('monto_fianza_cumple', 'Monto Fianza Cumple:') !!}
    <p>{!! $costProyectoPlanifica->monto_fianza_cumple !!}</p>
</div>

<!-- No Fianza Anticipo Field -->
<div class="form-group">
    {!! Form::label('no_fianza_anticipo', 'No Fianza Anticipo:') !!}
    <p>{!! $costProyectoPlanifica->no_fianza_anticipo !!}</p>
</div>

<!-- No Fianza Anticipo F Field -->
<div class="form-group">
    {!! Form::label('no_fianza_anticipo_f', 'No Fianza Anticipo F:') !!}
    <p>{!! $costProyectoPlanifica->no_fianza_anticipo_f !!}</p>
</div>

<!-- Monto Fianza Anticipo Field -->
<div class="form-group">
    {!! Form::label('monto_fianza_anticipo', 'Monto Fianza Anticipo:') !!}
    <p>{!! $costProyectoPlanifica->monto_fianza_anticipo !!}</p>
</div>

<!-- No Fianza Conser Obra Field -->
<div class="form-group">
    {!! Form::label('no_fianza_conser_obra', 'No Fianza Conser Obra:') !!}
    <p>{!! $costProyectoPlanifica->no_fianza_conser_obra !!}</p>
</div>

<!-- No Fianza Conser Obra F Field -->
<div class="form-group">
    {!! Form::label('no_fianza_conser_obra_f', 'No Fianza Conser Obra F:') !!}
    <p>{!! $costProyectoPlanifica->no_fianza_conser_obra_f !!}</p>
</div>

<!-- Monto Fianza Conser Obra Field -->
<div class="form-group">
    {!! Form::label('monto_fianza_conser_obra', 'Monto Fianza Conser Obra:') !!}
    <p>{!! $costProyectoPlanifica->monto_fianza_conser_obra !!}</p>
</div>

<!-- No Fianza Saldo Deudor Field -->
<div class="form-group">
    {!! Form::label('no_fianza_saldo_deudor', 'No Fianza Saldo Deudor:') !!}
    <p>{!! $costProyectoPlanifica->no_fianza_saldo_deudor !!}</p>
</div>

<!-- No Fianza Saldo Deudor F Field -->
<div class="form-group">
    {!! Form::label('no_fianza_saldo_deudor_f', 'No Fianza Saldo Deudor F:') !!}
    <p>{!! $costProyectoPlanifica->no_fianza_saldo_deudor_f !!}</p>
</div>

<!-- Monto Fianza Saldo Field -->
<div class="form-group">
    {!! Form::label('monto_fianza_saldo', 'Monto Fianza Saldo:') !!}
    <p>{!! $costProyectoPlanifica->monto_fianza_saldo !!}</p>
</div>

<!-- Entidad Afianzadora Field -->
<div class="form-group">
    {!! Form::label('entidad_afianzadora', 'Entidad Afianzadora:') !!}
    <p>{!! $costProyectoPlanifica->entidad_afianzadora !!}</p>
</div>

<!-- Clase Fianza Field -->
<div class="form-group">
    {!! Form::label('clase_fianza', 'Clase Fianza:') !!}
    <p>{!! $costProyectoPlanifica->clase_fianza !!}</p>
</div>

<!-- Doc Cambios Contrato Field -->
<div class="form-group">
    {!! Form::label('doc_cambios_contrato', 'Doc Cambios Contrato:') !!}
    <p>{!! $costProyectoPlanifica->doc_cambios_contrato !!}</p>
</div>

<!-- Suma Mod Monto Field -->
<div class="form-group">
    {!! Form::label('suma_mod_monto', 'Suma Mod Monto:') !!}
    <p>{!! $costProyectoPlanifica->suma_mod_monto !!}</p>
</div>

<!-- Razon Cambio Monto Field -->
<div class="form-group">
    {!! Form::label('razon_cambio_monto', 'Razon Cambio Monto:') !!}
    <p>{!! $costProyectoPlanifica->razon_cambio_monto !!}</p>
</div>

<!-- Monto Actualizado Contrato Field -->
<div class="form-group">
    {!! Form::label('monto_actualizado_contrato', 'Monto Actualizado Contrato:') !!}
    <p>{!! $costProyectoPlanifica->monto_actualizado_contrato !!}</p>
</div>

<!-- Total Pagos Field -->
<div class="form-group">
    {!! Form::label('total_pagos', 'Total Pagos:') !!}
    <p>{!! $costProyectoPlanifica->total_pagos !!}</p>
</div>

<!-- No Mod Plazo Contrato Field -->
<div class="form-group">
    {!! Form::label('no_mod_plazo_contrato', 'No Mod Plazo Contrato:') !!}
    <p>{!! $costProyectoPlanifica->no_mod_plazo_contrato !!}</p>
</div>

<!-- Cantidad Tiempo Mod Field -->
<div class="form-group">
    {!! Form::label('cantidad_tiempo_mod', 'Cantidad Tiempo Mod:') !!}
    <p>{!! $costProyectoPlanifica->cantidad_tiempo_mod !!}</p>
</div>

<!-- Nueva F Finaliza Field -->
<div class="form-group">
    {!! Form::label('nueva_f_finaliza', 'Nueva F Finaliza:') !!}
    <p>{!! $costProyectoPlanifica->nueva_f_finaliza !!}</p>
</div>

<!-- Razon Cambio Plazo Field -->
<div class="form-group">
    {!! Form::label('razon_cambio_plazo', 'Razon Cambio Plazo:') !!}
    <p>{!! $costProyectoPlanifica->razon_cambio_plazo !!}</p>
</div>

<!-- No Mod Alcance Obra Field -->
<div class="form-group">
    {!! Form::label('no_mod_alcance_obra', 'No Mod Alcance Obra:') !!}
    <p>{!! $costProyectoPlanifica->no_mod_alcance_obra !!}</p>
</div>

<!-- Razon Cambio Alcance Field -->
<div class="form-group">
    {!! Form::label('razon_cambio_alcance', 'Razon Cambio Alcance:') !!}
    <p>{!! $costProyectoPlanifica->razon_cambio_alcance !!}</p>
</div>

<!-- Alcance Real Obra Field -->
<div class="form-group">
    {!! Form::label('alcance_real_obra', 'Alcance Real Obra:') !!}
    <p>{!! $costProyectoPlanifica->alcance_real_obra !!}</p>
</div>

<!-- Programa Trabajo Actualizado Field -->
<div class="form-group">
    {!! Form::label('programa_trabajo_actualizado', 'Programa Trabajo Actualizado:') !!}
    <p>{!! $costProyectoPlanifica->programa_trabajo_actualizado !!}</p>
</div>

<!-- Informe Avance Pago Field -->
<div class="form-group">
    {!! Form::label('informe_avance_pago', 'Informe Avance Pago:') !!}
    <p>{!! $costProyectoPlanifica->informe_avance_pago !!}</p>
</div>

<!-- Planos Completos Field -->
<div class="form-group">
    {!! Form::label('planos_completos', 'Planos Completos:') !!}
    <p>{!! $costProyectoPlanifica->planos_completos !!}</p>
</div>

<!-- Reporte Eva Au Field -->
<div class="form-group">
    {!! Form::label('reporte_eva_au', 'Reporte Eva Au:') !!}
    <p>{!! $costProyectoPlanifica->reporte_eva_au !!}</p>
</div>

<!-- Informe Termina Proyecto Field -->
<div class="form-group">
    {!! Form::label('informe_termina_proyecto', 'Informe Termina Proyecto:') !!}
    <p>{!! $costProyectoPlanifica->informe_termina_proyecto !!}</p>
</div>

<!-- Informe Evalua Pro Field -->
<div class="form-group">
    {!! Form::label('informe_evalua_pro', 'Informe Evalua Pro:') !!}
    <p>{!! $costProyectoPlanifica->informe_evalua_pro !!}</p>
</div>

<!-- Estado Actual Field -->
<div class="form-group">
    {!! Form::label('estado_actual', 'Estado Actual:') !!}
    <p>{!! $costProyectoPlanifica->estado_actual !!}</p>
</div>

