@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cost Proyecto Planifica
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costProyectoPlanifica, ['route' => ['costProyectoPlanificas.update', $costProyectoPlanifica->id], 'method' => 'patch']) !!}

                        @include('cost_proyecto_planificas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection