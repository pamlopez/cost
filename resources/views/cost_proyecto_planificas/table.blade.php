<table class="table table-responsive" id="costProyectoPlanificas-table">
    <thead>
        <tr>
            <th>Id Proyecto</th>
        <th>Bases Concurso</th>
        <th>Bases F Publicacion</th>
        <th>Bases F Ultima Mod</th>
        <th>No Inconformidades</th>
        <th>No Inconformidades R</th>
        <th>Oferente</th>
        <th>Acta Adjudicacion</th>
        <th>Alcance Contrato</th>
        <th>Programa Ejecucion</th>
        <th>Estado Contrato</th>
        <th>Tipo No Contrato</th>
        <th>Empresa Constructora</th>
        <th>Monto Contrato</th>
        <th>F Aprobacion Contrato</th>
        <th>Nombre Firmante</th>
        <th>Nombramiento F</th>
        <th>Plazo Contrato</th>
        <th>F Inicio Contrato</th>
        <th>F Autoriza Bitacora</th>
        <th>Monto Anticipo</th>
        <th>No Estima Pagada</th>
        <th>Monto Estima Pagada</th>
        <th>No Pagos Voac</th>
        <th>Pagos Voac Aprobado</th>
        <th>Acta Inicio</th>
        <th>Acta Inicio F</th>
        <th>Acta Recepcion</th>
        <th>Acta Recepcion F</th>
        <th>Acta Liquidacion</th>
        <th>Acta Aprobacion Liquidacion</th>
        <th>Finiquito Fechas</th>
        <th>No Fianza Sos Obra</th>
        <th>No Fianza Sos Obra F</th>
        <th>Monto Fianza</th>
        <th>No Fianza Cumple Contrato</th>
        <th>No Fianza Cumple Contrato F</th>
        <th>Monto Fianza Cumple</th>
        <th>No Fianza Anticipo</th>
        <th>No Fianza Anticipo F</th>
        <th>Monto Fianza Anticipo</th>
        <th>No Fianza Conser Obra</th>
        <th>No Fianza Conser Obra F</th>
        <th>Monto Fianza Conser Obra</th>
        <th>No Fianza Saldo Deudor</th>
        <th>No Fianza Saldo Deudor F</th>
        <th>Monto Fianza Saldo</th>
        <th>Entidad Afianzadora</th>
        <th>Clase Fianza</th>
        <th>Doc Cambios Contrato</th>
        <th>Suma Mod Monto</th>
        <th>Razon Cambio Monto</th>
        <th>Monto Actualizado Contrato</th>
        <th>Total Pagos</th>
        <th>No Mod Plazo Contrato</th>
        <th>Cantidad Tiempo Mod</th>
        <th>Nueva F Finaliza</th>
        <th>Razon Cambio Plazo</th>
        <th>No Mod Alcance Obra</th>
        <th>Razon Cambio Alcance</th>
        <th>Alcance Real Obra</th>
        <th>Programa Trabajo Actualizado</th>
        <th>Informe Avance Pago</th>
        <th>Planos Completos</th>
        <th>Reporte Eva Au</th>
        <th>Informe Termina Proyecto</th>
        <th>Informe Evalua Pro</th>
        <th>Estado Actual</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($costProyectoPlanificas as $costProyectoPlanifica)
        <tr>
            <td>{!! $costProyectoPlanifica->id_proyecto !!}</td>
            <td>{!! $costProyectoPlanifica->bases_concurso !!}</td>
            <td>{!! $costProyectoPlanifica->bases_f_publicacion !!}</td>
            <td>{!! $costProyectoPlanifica->bases_f_ultima_mod !!}</td>
            <td>{!! $costProyectoPlanifica->no_inconformidades !!}</td>
            <td>{!! $costProyectoPlanifica->no_inconformidades_r !!}</td>
            <td>{!! $costProyectoPlanifica->oferente !!}</td>
            <td>{!! $costProyectoPlanifica->acta_adjudicacion !!}</td>
            <td>{!! $costProyectoPlanifica->alcance_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->programa_ejecucion !!}</td>
            <td>{!! $costProyectoPlanifica->estado_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->tipo_no_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->empresa_constructora !!}</td>
            <td>{!! $costProyectoPlanifica->monto_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->f_aprobacion_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->nombre_firmante !!}</td>
            <td>{!! $costProyectoPlanifica->nombramiento_f !!}</td>
            <td>{!! $costProyectoPlanifica->plazo_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->f_inicio_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->f_autoriza_bitacora !!}</td>
            <td>{!! $costProyectoPlanifica->monto_anticipo !!}</td>
            <td>{!! $costProyectoPlanifica->no_estima_pagada !!}</td>
            <td>{!! $costProyectoPlanifica->monto_estima_pagada !!}</td>
            <td>{!! $costProyectoPlanifica->no_pagos_voac !!}</td>
            <td>{!! $costProyectoPlanifica->pagos_voac_aprobado !!}</td>
            <td>{!! $costProyectoPlanifica->acta_inicio !!}</td>
            <td>{!! $costProyectoPlanifica->acta_inicio_f !!}</td>
            <td>{!! $costProyectoPlanifica->acta_recepcion !!}</td>
            <td>{!! $costProyectoPlanifica->acta_recepcion_f !!}</td>
            <td>{!! $costProyectoPlanifica->acta_liquidacion !!}</td>
            <td>{!! $costProyectoPlanifica->acta_aprobacion_liquidacion !!}</td>
            <td>{!! $costProyectoPlanifica->finiquito_fechas !!}</td>
            <td>{!! $costProyectoPlanifica->no_fianza_sos_obra !!}</td>
            <td>{!! $costProyectoPlanifica->no_fianza_sos_obra_f !!}</td>
            <td>{!! $costProyectoPlanifica->monto_fianza !!}</td>
            <td>{!! $costProyectoPlanifica->no_fianza_cumple_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->no_fianza_cumple_contrato_f !!}</td>
            <td>{!! $costProyectoPlanifica->monto_fianza_cumple !!}</td>
            <td>{!! $costProyectoPlanifica->no_fianza_anticipo !!}</td>
            <td>{!! $costProyectoPlanifica->no_fianza_anticipo_f !!}</td>
            <td>{!! $costProyectoPlanifica->monto_fianza_anticipo !!}</td>
            <td>{!! $costProyectoPlanifica->no_fianza_conser_obra !!}</td>
            <td>{!! $costProyectoPlanifica->no_fianza_conser_obra_f !!}</td>
            <td>{!! $costProyectoPlanifica->monto_fianza_conser_obra !!}</td>
            <td>{!! $costProyectoPlanifica->no_fianza_saldo_deudor !!}</td>
            <td>{!! $costProyectoPlanifica->no_fianza_saldo_deudor_f !!}</td>
            <td>{!! $costProyectoPlanifica->monto_fianza_saldo !!}</td>
            <td>{!! $costProyectoPlanifica->entidad_afianzadora !!}</td>
            <td>{!! $costProyectoPlanifica->clase_fianza !!}</td>
            <td>{!! $costProyectoPlanifica->doc_cambios_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->suma_mod_monto !!}</td>
            <td>{!! $costProyectoPlanifica->razon_cambio_monto !!}</td>
            <td>{!! $costProyectoPlanifica->monto_actualizado_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->total_pagos !!}</td>
            <td>{!! $costProyectoPlanifica->no_mod_plazo_contrato !!}</td>
            <td>{!! $costProyectoPlanifica->cantidad_tiempo_mod !!}</td>
            <td>{!! $costProyectoPlanifica->nueva_f_finaliza !!}</td>
            <td>{!! $costProyectoPlanifica->razon_cambio_plazo !!}</td>
            <td>{!! $costProyectoPlanifica->no_mod_alcance_obra !!}</td>
            <td>{!! $costProyectoPlanifica->razon_cambio_alcance !!}</td>
            <td>{!! $costProyectoPlanifica->alcance_real_obra !!}</td>
            <td>{!! $costProyectoPlanifica->programa_trabajo_actualizado !!}</td>
            <td>{!! $costProyectoPlanifica->informe_avance_pago !!}</td>
            <td>{!! $costProyectoPlanifica->planos_completos !!}</td>
            <td>{!! $costProyectoPlanifica->reporte_eva_au !!}</td>
            <td>{!! $costProyectoPlanifica->informe_termina_proyecto !!}</td>
            <td>{!! $costProyectoPlanifica->informe_evalua_pro !!}</td>
            <td>{!! $costProyectoPlanifica->estado_actual !!}</td>
            <td>
                {!! Form::open(['route' => ['costProyectoPlanificas.destroy', $costProyectoPlanifica->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costProyectoPlanificas.show', [$costProyectoPlanifica->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('costProyectoPlanificas.edit', [$costProyectoPlanifica->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>