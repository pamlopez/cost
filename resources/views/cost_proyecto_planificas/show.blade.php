@extends('layouts.app')
@include('flash::message')
@section('content')
    <section class="content-header">
        <h1>
            Cost Proyecto Planifica
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_proyecto_planificas.show_fields')
                    <a href="{!! route('costProyectoPlanificas.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
