@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Documentos
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('documentos.show_fields')
                    <a href="{!! route('documentos.index', ['id_proyecto'=>$documento->id_proyecto,'id_fase'=>$documento->id_fase]) !!}" class="btn btn-default">Atrás</a>
                </div>
            </div>
        </div>
    </div>
@endsection
