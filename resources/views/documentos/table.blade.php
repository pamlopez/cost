<div class="table-responsive">
<table class="table table-responsive" id="documentos-table">
    <thead>
        <tr>
            <th width="5%">#</th>
            <th width="15%">Autor</th>
            <th width="20%">Titulo</th>
            <th width="40%">Descripción</th>
            <th width="10%">Url</th>
            <th width="15%" colspan="3">Acción</th>
        </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($documentos as $documento)
        @php($i++)
        <tr>
            <td>{!! $i !!}</td>
            <td>{!! $documento->autor !!}</td>
            <td>{!! $documento->titulo !!}</td>
            <td>{!! $documento->documento !!}</td>
            <td><a href="{!! $documento->url  !!}" target="_blank"><b>Ver referencia</b></a></td>

            <td>
                {!! Form::open(['route' => ['documentos.destroy', $documento->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('documentos.show', [$documento->id]) !!}" class='btn btn-info btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                        <a href="{!! route('documentos.edit', [$documento->id]) !!}" class='btn btn-warning btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro de borrar este documento?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>