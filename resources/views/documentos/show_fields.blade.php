

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('tipo', 'Tipo de documento:') !!}
    <p>{!! $documento->fmt_tipo !!}</p>
</div>

<!-- Titulo Field -->
<div class="form-group">
    {!! Form::label('titulo', 'Título del documento:') !!}
    <p>{!! $documento->titulo !!}</p>
</div>

<!-- Documento Field -->
<div class="form-group">
    {!! Form::label('documento', 'Descripción del documento:') !!}
    <p>{!! $documento->documento !!}</p>
</div>


<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', 'Url del documento:') !!}
    <p>{!! $documento->url !!}</p>
</div>


<!-- Fh Publicacion Field -->
<div class="form-group">
    {!! Form::label('fh_publicacion', 'Fecha de última publicación:') !!}
    <p>{!! $documento->fh_publicacion !!}</p>
</div>

<!-- Fh Modificacion Field -->
<div class="form-group">
    {!! Form::label('fh_modificacion', 'Fecha de última modificación:') !!}
    <p>{!! $documento->fh_modificacion !!}</p>
</div>

<!-- Formato Field -->
<div class="form-group">
    {!! Form::label('formato', 'Formato:') !!}
    <p>{!! $documento->fmt_formato !!}</p>
</div>

<!-- Lenguaje Field -->
<div class="form-group">
    {!! Form::label('lenguaje', 'Lenguaje:') !!}
    <p>{!! $documento->fmt_lenguaje !!}</p>
</div>

<!-- Pagina Inicio Field -->
<div class="form-group">
    {!! Form::label('pagina_inicio', 'Pagina de inicio:') !!}
    <p>{!! $documento->pagina_inicio !!}</p>
</div>

<!-- Pagina Fin Field -->
<div class="form-group">
    {!! Form::label('pagina_fin', 'Pagina de fin:') !!}
    <p>{!! $documento->pagina_fin !!}</p>
</div>

<!-- Detalle Acceso Field -->
<div class="form-group">
    {!! Form::label('detalle_acceso', 'Detalle del acceso:') !!}
    <p>{!! $documento->detalle_acceso !!}</p>
</div>

<!-- Autor Field -->
<div class="form-group">
    {!! Form::label('autor', 'Autor:') !!}
    <p>{!! $documento->autor !!}</p>
</div>

