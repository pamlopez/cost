{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$documento->id_proyecto, ['class' => 'form-control']) !!}
{!! Form::hidden('id_fase',isset($id_fase)?$id_fase:$documento->id_fase, ['class' => 'form-control']) !!}

<div class="form-group col-sm-6">
    {!! Form::label('tipo', 'Tipo de Documento (OC4IDS):') !!}
    {!! Form::select('tipo', \App\Models\cat_item::listar_catalogo_traduccion(76),$documento->tipo,['class' => 'form-control','style'=>'width:100%','id'=>'tipo']) !!}
</div>


<!-- Titulo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('titulo', 'Título del documento (OC4IDS):') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<!-- Documento Field -->
<div class="form-group col-sm-12">
    {!! Form::label('documento', 'Descripción del ocumento (OC4IDS):') !!}
    {!! Form::text('documento', null, ['class' => 'form-control']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-12">
    {!! Form::label('url', 'Url del documento (OC4IDS):') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fh_publicacion', 'Fecha de la publicación (OC4IDS):') !!}
    @if($bnd_edit)
        {!! Form::text('fh_publicacion', null, ['class' => 'form-control datepicker','data-value'=>$documento->fh_publicacion]) !!}
    @else
        {!! Form::text('fh_publicacion', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>

<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fh_modificacion', 'Fecha de la última modificación (OC4IDS):') !!}
    @if($bnd_edit)
        {!! Form::text('fh_modificacion', null, ['class' => 'form-control datepicker','data-value'=>$documento->fh_modificacion]) !!}
    @else
        {!! Form::text('fh_modificacion', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>


@include('chivos.catalogo', ['chivo_control' => 'formato'
                           ,'chivo_id_cat'=>77
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Tipo de formato (OC4IDS):'])

@include('chivos.catalogo', ['chivo_control' => 'lenguaje'
                           ,'chivo_id_cat'=>78
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Lenguaje en el que está el documento (OC4IDS):'])



<!-- Pagina Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pagina_inicio', 'Pagina de inicio del documento (OC4IDS):') !!}
    {!! Form::number('pagina_inicio', null, ['class' => 'form-control']) !!}
</div>

<!-- Pagina Fin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pagina_fin', 'Pagina de fin del documento (OC4IDS):') !!}
    {!! Form::number('pagina_fin', null, ['class' => 'form-control']) !!}
</div>

<!-- Detalle Acceso Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('detalle_acceso', 'Detalle de acceso al documento:') !!}
    {!! Form::textarea('detalle_acceso', null, ['class' => 'form-control']) !!}
</div>

<!-- Autor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('autor', 'Autor:') !!}
    {!! Form::text('autor', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    @if(isset($id_fase))
        <a href="{!! route('documentos.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('documentos.index', ['id_proyecto'=>$documento->id_proyecto,'id_fase'=>$documento->id_fase]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif
</div>

