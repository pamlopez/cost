
<!-- Id Participante Field -->
<div class="form-group">
    {!! Form::label('id_participante', 'Participante:') !!}
    <p>{!! $costOferente->fmt_participante !!}</p>
</div>

<!-- Monto Ofertado Field -->
<div class="form-group">
    {!! Form::label('monto_ofertado', 'Monto Ofertado:') !!}
    <p>{!! $costOferente->monto_ofertado !!}</p>
</div>

<!-- Oferta Presentada Field -->
<div class="form-group">
    {!! Form::label('oferta_presentada', 'Oferta Presentada:') !!}
    <p>{!! $costOferente->oferta_presentada !!}</p>
</div>

