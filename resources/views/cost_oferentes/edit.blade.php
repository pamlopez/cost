@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Editar al Oferente
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costOferente, ['route' => ['costOferentes.update', $costOferente->id], 'method' => 'patch']) !!}

                        @include('cost_oferentes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection