<table class="table table-responsive" id="costOferentes-table">
    <thead>
    <tr>
        <th>#</th>
        <th>Participante</th>
        <th>Monto Ofertado</th>
        <th>Oferta Presentada</th>
        @if(!isset($a))
        <th colspan="3">Acción</th>
            @endif
    </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($costOferentes as $costOferente)
        @php($i++)
        @php($money = number_format($costOferente->monto_ofertado , 2, '.', ',') )
        <tr>
            <td>{!! $i !!}</td>
            <td>{!! $costOferente->fmt_participante !!}</td>
            <td>Q. {!! $money !!}</td>
            <td>{!! $costOferente->oferta_presentada !!}</td>
            @if(!isset($a))
            <td>
                {!! Form::open(['route' => ['costOferentes.destroy', $costOferente->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costOferentes.show', [$costOferente->id]) !!}" class='btn btn-info btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                        <a href="{!! route('costOferentes.edit', [$costOferente->id]) !!}"
                           class='btn btn-warning btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>