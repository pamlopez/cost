{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costOferente->id_proyecto, ['class' => 'form-control']) !!}
{!! Form::hidden('id_fase',isset($id_fase)?$id_fase:$costOferente->id_fase, ['class' => 'form-control']) !!}

@include('chivos.participante', ['chivo_control' => 'id_participante'
                           , 'chivo_default'=>$costOferente->id_participante
                           ,'chivo_texto'=>'Participante:'])

<!-- Monto Ofertado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_ofertado', 'Monto Ofertado:') !!}
    {!! Form::text('monto_ofertado', null, ['class' => 'form-control']) !!}
</div>

<!-- Oferta Presentada Field -->
<div class="form-group col-sm-6">
    {!! Form::label('oferta_presentada', 'Oferta Presentada:') !!}
    {!! Form::text('oferta_presentada', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}

    @if(isset($id_proyecto))
        <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$id_proyecto]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$costOferente->id_proyecto]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif

</div>