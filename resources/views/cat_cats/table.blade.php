<table class="table table-responsive" id="catCats-table">
    <thead>
        <th>#</th>
        <th>Descripcion</th>
        <th>Acciones</th>
    </thead>
    <tbody>
    @php($cnt =1)
    @foreach($catCats as $catCat)
        <tr>
            <td>{!! $cnt++ !!}</td>
            <td>{!! $catCat->descripcion !!}</td>
            <td>
                {!! Form::open(['route' => ['catCats.destroy', $catCat->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('catCats.edit', [$catCat->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿ Está seguro de eliminar este registro ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@push('javascript')
    <script>
        $('#catCats-table').dataTable();
    </script>
@endpush