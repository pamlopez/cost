@include('cost_proyectos.estudios')
<div class="col-md-12">
    <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> MARN</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Resolucion Marn Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('resolucion_marn', 'Resolucion Marn:') !!}
                    <p>{!! $costProyectoDisPlan->resolucion_marn !!}</p>
                </div>
                <!-- F Resolucion Marn Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('f_resolucion_marn', 'De fecha:') !!}
                    @if($costProyectoDisPlan->f_resolucion_marn != null)
                        <p>{!! \Carbon\Carbon::parse($costProyectoDisPlan->f_resolucion_marn)->format('d/m/Y') !!}</p>
                    @else
                        <p> </p>
                    @endif
                </div>
                <!-- Medida Mitigacion Presupuesto Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('medida_mitigacion_presupuesto', 'Medidas de mitigación incluidas en presupuesto:') !!}
                    <p>{!! $costProyectoDisPlan->medida_mitigacion_presupuesto!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_marn', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_marn!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_marn', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_marn!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-primary collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> AGRIP</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Estudio Suelo Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('agrip', 'AGRIP') !!}
                    <p>{!! $costProyectoDisPlan->FMTAgrip!!}</p>
                </div>
                <!-- Agrip F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('agrip_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->agrip_f != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->agrip_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif

                </div>
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('agrip_elaborado_por', 'AGRIP elaborado por:') !!}
                    <p>{!! $costProyectoDisPlan->agrip_elaborado_por!!}</p>
                </div>

                <!-- Agrip Medida Mitigacion Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('agrip_medida_mitigacion', 'Medidas de mitigación incluidas en presupuesto:') !!}
                    <p>{!! $costProyectoDisPlan->agrip_medida_mitigacion!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_agrip', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_agrip!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_agrip', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_agrip!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="modal fade in" id="modal-default" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Estudio de Suelos</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-cafe">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Suelo</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <!-- Estudio Suelo Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('estudio_suelo', 'Estudios de suelos:') !!}
                                        <p>{!! $costProyectoDisPlan->FmtestudioSuelo!!}</p>
                                    </div>

                                    <!-- Estudio Suelo F Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('estudio_suelo_f', 'De fecha:') !!}
                                        @if($costProyectoDisPlan->estudio_suelo_f != null)
                                            <p>{!! \Carbon\Carbon::parse($costProyectoDisPlan->estudio_suelo_f)->format('d/m/Y')!!}</p>
                                        @else
                                            <p> </p>
                                        @endif

                                    </div>

                                    <!-- Estudio Suelo Por Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('estudio_suelo_por', 'Hecho por:') !!}
                                        <p>{!! $costProyectoDisPlan->estudio_suelo_por!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('observaciones_suelo', 'Observaciones:') !!}
                                        <p>{!! $costProyectoDisPlan->observaciones_suelo!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('links_suelo', 'Links:') !!}
                                        <p>{!! $costProyectoDisPlan->links_suelo!!}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="col-md-12">
    <div class="box box-cafe collapsed-box ">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Suelo</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Estudio Suelo Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_suelo', 'Estudios de suelos:') !!}
                    <p>{!! $costProyectoDisPlan->FmtestudioSuelo!!}</p>
                </div>

                <!-- Estudio Suelo F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_suelo_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->estudio_suelo_f != null)
                        <p>{!! \Carbon\Carbon::parse($costProyectoDisPlan->estudio_suelo_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif

                </div>

                <!-- Estudio Suelo Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_suelo_por', 'Hecho por:') !!}
                    <p>{!! $costProyectoDisPlan->estudio_suelo_por!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_suelo', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_suelo!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_suelo', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_suelo!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="modal fade in" id="modal-default-topo" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Topografía</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Topografía</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">

                                    <!-- Estudio Topografia Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('estudio_topografia', 'Estudio Topografia:') !!}
                                        <p>{!! $costProyectoDisPlan->estudio_topografia!!}</p>
                                    </div>

                                    <!-- Estudio Topografia F Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('estudio_topografia_f', 'De fecha:') !!}
                                        @if($costProyectoDisPlan->estudio_topografia_f != null)
                                            <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->estudio_topografia_f)->format('d/m/Y')!!}</p>
                                        @else
                                            <p> </p>
                                        @endif

                                    </div>

                                    <!-- Estudio Topografia Por Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('estudio_topografia_por', 'Hecho por:') !!}
                                        <p>{!! $costProyectoDisPlan->estudio_topografia_por!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('observaciones_topografia', 'Observaciones:') !!}
                                        <p>{!! $costProyectoDisPlan->observaciones_topografia!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('links_topografia', 'Links:') !!}
                                        <p>{!! $costProyectoDisPlan->links_topografia!!}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="col-md-12">
    <div class="box box-info collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Topografía</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">

                <!-- Estudio Topografia Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_topografia', 'Estudio Topografia:') !!}
                    <p>{!! $costProyectoDisPlan->estudio_topografia!!}</p>
                </div>

                <!-- Estudio Topografia F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_topografia_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->estudio_topografia_f != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->estudio_topografia_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif

                </div>

                <!-- Estudio Topografia Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_topografia_por', 'Hecho por:') !!}
                    <p>{!! $costProyectoDisPlan->estudio_topografia_por!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_topografia', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_topografia!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_topografia', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_topografia!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="modal fade in" id="modal-default-geo" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Geológico</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>  Geológico</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <!-- Estudio Geologico Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('estudio_geologico', 'Estudio geológico:') !!}
                                        <p>{!! $costProyectoDisPlan->FmtestudioGeologico!!}</p>
                                    </div>

                                    <!-- Estudio Geologico F Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('estudio_geologico_f', 'De fecha:') !!}
                                        @if($costProyectoDisPlan->estudio_geologico_f != null)
                                            <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->estudio_geologico_f)->format('d/m/Y')!!}</p>
                                        @else
                                            <p> </p>
                                        @endif

                                    </div>

                                    <!-- Estudio Geologico Por Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('estudio_geologico_por', 'Hecho por:') !!}
                                        <p>{!! $costProyectoDisPlan->estudio_geologico_por!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('observaciones_geologico', 'Observaciones:') !!}
                                        <p>{!! $costProyectoDisPlan->observaciones_geologico!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('links_geologico', 'Links:') !!}
                                        <p>{!! $costProyectoDisPlan->links_geologico!!}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="col-md-12">
    <div class="box box-success collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>  Geológico</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Estudio Geologico Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_geologico', 'Estudio geológico:') !!}
                    <p>{!! $costProyectoDisPlan->FmtestudioGeologico!!}</p>
                </div>

                <!-- Estudio Geologico F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_geologico_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->estudio_geologico_f != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->estudio_geologico_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif

                </div>

                <!-- Estudio Geologico Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_geologico_por', 'Hecho por:') !!}
                    <p>{!! $costProyectoDisPlan->estudio_geologico_por!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_geologico', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_geologico!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_geologico', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_geologico!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>


<div class="col-md-12">
    <div class="box box-blue collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>  Hidrogeológico</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Estudio Geologico Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_hidrogeologico', 'Estudio hidrogeologico:') !!}
                    <p>{!! $costProyectoDisPlan->FmtEstudioHidrogeologico!!}</p>
                </div>

                <!-- Estudio Geologico F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('fecha_hidrogeologico', 'De fecha:') !!}
                    @if($costProyectoDisPlan->fecha_hidrogeologico != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->fecha_hidrogeologico)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif
                </div>

                <!-- Estudio Geologico Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('hecho_por_hidrogeologico', 'Hecho por:') !!}
                    <p>{!! $costProyectoDisPlan->hecho_por_hidrogeologico!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_hidrogeologico', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_hidrogeologico!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_hidrogeologico', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_hidrogeologico!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="modal fade in" id="modal-default-estructura" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Estructuras</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-maroon">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>  Estructuras</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">

                                    <!-- Diseno Estructural Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('diseno_estructural', 'Diseno estructural:') !!}
                                        <p>{!! $costProyectoDisPlan->FmtDisenoEstructural!!}</p>
                                    </div>

                                    <!-- Diseno Estructural F Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('diseno_estructural_f', 'De fecha:') !!}
                                        @if($costProyectoDisPlan->diseno_estructural_f != null)
                                            <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->diseno_estructural_f)->format('d/m/Y')!!}</p>
                                        @else
                                            <p> </p>
                                        @endif
                                    </div>

                                    <!-- Diseno Estructural Por Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('diseno_estructural_por', 'Hecho  Por:') !!}
                                        <p>{!! $costProyectoDisPlan->diseno_estructural_por!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('observaciones_estructural', 'Observaciones:') !!}
                                        <p>{!! $costProyectoDisPlan->observaciones_estructural!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('links_estructural', 'Links:') !!}
                                        <p>{!! $costProyectoDisPlan->links_estructural!!}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="col-md-12">
    <div class="box box-maroon collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>  Estructuras</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">

                <!-- Diseno Estructural Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('diseno_estructural', 'Diseno estructural:') !!}
                    <p>{!! $costProyectoDisPlan->FmtDisenoEstructural!!}</p>
                </div>

                <!-- Diseno Estructural F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('diseno_estructural_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->diseno_estructural_f != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->diseno_estructural_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif

                </div>

                <!-- Diseno Estructural Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('diseno_estructural_por', 'Hecho  Por:') !!}
                    <p>{!! $costProyectoDisPlan->diseno_estructural_por!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_estructural', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_estructural!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_estructural', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_estructural!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="modal fade in" id="modal-default-memo" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Memorias de cálculo</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>  Memorias de cálculo</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <!-- Memoria Calculo Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('memoria_calculo', 'Memoria cálculo:') !!}
                                        <p>{!! $costProyectoDisPlan->FmtmemoriaCalculo!!}</p>
                                    </div>
                                    <!-- Diseno Estructural F Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('memoria_calculo_f', 'De fecha:') !!}
                                        @if($costProyectoDisPlan->memoria_calculo_f != null)
                                            <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->memoria_calculo_f)->format('d/m/Y')!!}</p>
                                        @else
                                            <p> </p>
                                        @endif

                                    </div>
                                    <!-- Diseno Estructural Por Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('memoria_calculo_hecho_por', 'Diseño estructural por:') !!}
                                        <p>{!! $costProyectoDisPlan->memoria_calculo_hecho_por!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('observaciones_memoria_calculo', 'Observaciones:') !!}
                                        <p>{!! $costProyectoDisPlan->observaciones_memoria_calculo!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('liks_memoria_calculo', 'Links:') !!}
                                        <p>{!! $costProyectoDisPlan->liks_memoria_calculo!!}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="col-md-12">
    <div class="box box-warning collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>  Memorias de cálculo</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Memoria Calculo Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('memoria_calculo', 'Memoria cálculo:') !!}
                    <p>{!! $costProyectoDisPlan->FmtmemoriaCalculo!!}</p>
                </div>
                <!-- Diseno Estructural F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('memoria_calculo_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->memoria_calculo_f != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->memoria_calculo_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif
                </div>
                <!-- Diseno Estructural Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('memoria_calculo_hecho_por', 'Diseño estructural por:') !!}
                    <p>{!! $costProyectoDisPlan->memoria_calculo_hecho_por!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_memoria_calculo', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_memoria_calculo!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('liks_memoria_calculo', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->liks_memoria_calculo!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>

<div class="modal fade in" id="modal-default-plano" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Planos completos</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>  Planos Completos</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">

                                    <!-- Plano Completo Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('plano_completo', 'Plano Completo:') !!}
                                        <p>{!! $costProyectoDisPlan->FmtplanoCompleto!!}</p>
                                    </div>

                                    <!-- Diseno Estructural F Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('plano_completo_f', 'De fecha:') !!}
                                        @if($costProyectoDisPlan->plano_completo_f != null)
                                            <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->plano_completo_f)->format('d/m/Y')!!}</p>
                                        @else
                                            <p> </p>
                                        @endif

                                    </div>

                                    <!-- Plano Completo Responsable Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('plano_completo_responsable', 'Hecho por:') !!}
                                        <p>{!! $costProyectoDisPlan->plano_completo_responsable!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('observaciones_plano_completo', 'Observaciones:') !!}
                                        <p>{!! $costProyectoDisPlan->observaciones_plano_completo!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('liks_plano_completo', 'Links:') !!}
                                        <p>{!! $costProyectoDisPlan->liks_plano_completo!!}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="col-md-12">
    <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>  Planos Completos</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">

                <!-- Plano Completo Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('plano_completo', 'Plano Completo:') !!}
                    <p>{!! $costProyectoDisPlan->FmtplanoCompleto!!}</p>
                </div>

                <!-- Diseno Estructural F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('plano_completo_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->plano_completo_f != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->plano_completo_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif

                </div>

                <!-- Plano Completo Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('plano_completo_responsable', 'Hecho por:') !!}
                    <p>{!! $costProyectoDisPlan->plano_completo_responsable!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_plano_completo', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_plano_completo!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('liks_plano_completo', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->liks_plano_completo!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="modal fade in" id="modal-default-general" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Especificaciones Generales</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Especificaciones Generales</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <!-- Espe General Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('espe_general', 'Especificación General:') !!}
                                        <p>{!! $costProyectoDisPlan->FmtEspeGeneral!!}</p>
                                    </div>
                                    <!-- Espe General Responsable Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('espe_general_responsable', 'Hecho por:') !!}
                                        <p>{!! $costProyectoDisPlan->espe_general_responsable!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('observaciones_espe_general', 'Observaciones:') !!}
                                        <p>{!! $costProyectoDisPlan->observaciones_espe_general!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('liks_espe_general', 'Links:') !!}
                                        <p>{!! $costProyectoDisPlan->links_espe_general!!}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="col-sm-12">
    <div class="box box-info collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Especificaciones Generales</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Espe General Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('espe_general', 'Especificación General:') !!}
                    <p>{!! $costProyectoDisPlan->FmtEspeGeneral!!}</p>
                </div>
                <!-- Espe General Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('espe_general_responsable', 'Hecho por:') !!}
                    <p>{!! $costProyectoDisPlan->espe_general_responsable!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_espe_general', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_espe_general!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('liks_espe_general', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_espe_general!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="modal fade in" id="modal-default-espe" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"> Especificaciones Técnicas</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Especificaciones Técnicas</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <!-- Espe Tecnica Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('espe_tecnica', 'Especificación técnica:') !!}
                                        <p>{!! $costProyectoDisPlan->FmtEspeTecnica!!}</p>
                                    </div>
                                    <!-- Diseno Estructural F Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('espe_general_f', 'De fecha:') !!}
                                        @if($costProyectoDisPlan->espe_general_f != null)
                                            <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->espe_general_f)->format('d/m/Y')!!}</p>
                                        @else
                                            <p> </p>
                                        @endif

                                    </div>
                                    <!-- Espe Tecnica Responsable Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('espe_tecnica_responsable', 'Hecho por:') !!}
                                        <p>{!! $costProyectoDisPlan->espe_tecnica_responsable!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('observaciones_espe_tecnica', 'Observaciones:') !!}
                                        <p>{!! $costProyectoDisPlan->observaciones_espe_tecnica!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('liks_espe_tecnica', 'Links:') !!}
                                        <p>{!! $costProyectoDisPlan->links_espe_tecnica!!}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="col-sm-12">
    <div class="box box-success collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Especificaciones Técnicas</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Espe Tecnica Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('espe_tecnica', 'Especificación técnica:') !!}
                    <p>{!! $costProyectoDisPlan->FmtEspeTecnica!!}</p>
                </div>
                <!-- Diseno Estructural F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('espe_general_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->espe_general_f != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->espe_general_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif
                </div>
                <!-- Espe Tecnica Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('espe_tecnica_responsable', 'Hecho por:') !!}
                    <p>{!! $costProyectoDisPlan->espe_tecnica_responsable!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_espe_tecnica', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_espe_tecnica!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('liks_espe_tecnica', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_espe_tecnica!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="modal fade in" id="modal-default-dispo" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"> Disposiciones Especiales</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Disposiciones Especiales</h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <!-- Dispo Especial Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('dispo_especial', 'Disposicion especial:') !!}
                                        <p>{!! $costProyectoDisPlan->FmtDispoEspecial!!}</p>
                                    </div>

                                    <!-- Dispo Especial Responsable Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('dispo_especial_f', 'De fecha:') !!}
                                        @if($costProyectoDisPlan->dispo_especial_f != null)
                                            <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->dispo_especial_f)->format('d/m/Y')!!}</p>
                                        @else
                                            <p> </p>
                                        @endif

                                    </div>
                                    <!-- Dispo Especial Responsable Field -->
                                    <div class="form-group col-sm-4">
                                        {!! Form::label('dispo_especial_responsable', 'Hecho por:') !!}
                                        <p>{!! $costProyectoDisPlan->dispo_especial_responsable!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('observaciones_espe_tecnica', 'Observaciones:') !!}
                                        <p>{!! $costProyectoDisPlan->obeservaciones_dispo_espe!!}</p>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        {!! Form::label('links_dispo_especial', 'Links:') !!}
                                        <p>{!! $costProyectoDisPlan->links_dispo_especial!!}</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="col-sm-12">
    <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Disposiciones Especiales</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Dispo Especial Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('dispo_especial', 'Disposicion especial:') !!}
                    <p>{!! $costProyectoDisPlan->FmtDispoEspecial!!}</p>
                </div>

                <!-- Dispo Especial Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('dispo_especial_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->dispo_especial_f != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->dispo_especial_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif

                </div>
                <!-- Dispo Especial Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('dispo_especial_responsable', 'Hecho por:') !!}
                    <p>{!! $costProyectoDisPlan->dispo_especial_responsable!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_espe_tecnica', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->obeservaciones_dispo_espe!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_dispo_especial', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_dispo_especial!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Otros</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('otro_estudio', 'otro') !!}
                    <p>{!! $costProyectoDisPlan->FmtOtroEstudio!!}</p>
                </div>
                <!-- Agrip F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('otro_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->otro_f != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->otro_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif

                </div>
                <!-- Agrip Medida Mitigacion Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('otro_estudio_responsable', 'Hecho por:') !!}
                    <p>{!! $costProyectoDisPlan->otro_estudio_responsable!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_otro', 'Observaciones:') !!}
                    <p>{!! $costProyectoDisPlan->observaciones_otro!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_otro', 'Links:') !!}
                    <p>{!! $costProyectoDisPlan->links_otro!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>

<div class="col-sm-12">
    <div class="box box-success collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Evaluaci&oacute;n ambiental</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-sm-4">
                    {!! Form::label('inst_ea', 'Instrumento de evaluación ambiental:') !!}
                    <p>{!! $costProyectoDisPlan->FmtInstEa!!}</p>
                </div>
                <!-- F Resolucion Marn Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('inst_ea_d', 'De fecha:') !!}
                    @if($costProyectoDisPlan->inst_ea_d != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->inst_ea_d)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif
                </div>
                <!-- Espe General Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('inst_ea_resoponsable', 'Elaborado por') !!}
                    <p>{!! $costProyectoDisPlan->inst_ea_resoponsable!!}</p>
                </div>
                <!-- Espe General Responsable Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('links_inst_ea', 'Instrumento de evaluación ambiental links:') !!}
                    <p>{!! $costProyectoDisPlan->links_inst_ea!!}</p>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-sm-12">
    <div class="box box-warning collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Presupuesto</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-sm-4">
                    {!! Form::label('presupuesto_proyecto', 'Presupuesto:') !!}
                    <p>{!! $costProyectoDisPlan->FmtPresupuestoProyecto!!}</p>
                </div>
                <div class="form-group col-sm-4">
                    {!! Form::label('presupuesto_aprobacion_f', 'De fecha:') !!}
                    @if($costProyectoDisPlan->presupuesto_aprobacion_f != null)
                        <p>{!!  \Carbon\Carbon::parse($costProyectoDisPlan->presupuesto_aprobacion_f)->format('d/m/Y')!!}</p>
                    @else
                        <p> </p>
                    @endif

                </div>
                <!-- Espe General Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('links_presupuesto', 'Links presupuesto:') !!}
                    <p>{!! $costProyectoDisPlan->links_presupuesto!!}</p>
                </div>
                <!-- Espe General Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('costo_estimado_proyecto ', 'Costo estimado del proyecto:') !!}
                    <p>{!! $costProyectoDisPlan->costo_estimado_proyecto!!}</p>
                </div>
                <div class="form-group col-sm-4">
                    {!! Form::label('presupuesto_moneda', 'Tipo de moneda:') !!}
                    <p>{!! $costProyectoDisPlan->FmtPresupuestoMoneda!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-info collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Datos generales de la preparación del proyecto</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('', 'Anuncio o invitación a cotizar diseño:') !!}
                    <p>{!! $costProyectoDisPlan->dis_anuncio_invitacion!!}</p>
                </div>
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('dis_terminos_referencia', 'Términos de Referencia para el Diseño o Estudio de Factibilidad:') !!}
                    <p>{!! $costProyectoDisPlan->dis_terminos_referencia!!}</p>
                </div>
            </div>
            <div class="row">
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('a_estudio_facti', 'Aprobación del estudio de factibilidad:') !!}
                    <p>{!! $costProyectoDisPlan->a_estudio_facti!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('', 'Aprobación del estudio de factibilidad (Entidades):') !!}
                    <p>{!! $costProyectoDisPlan->a_entidades!!}</p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('a_derecho_paso', 'Aprobación del estudio de factibilidad (Derecho paso):') !!}
                    <p>{!! $costProyectoDisPlan->a_derecho_paso!!}</p>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
