{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costProyectoDisPlan->id_proyecto, ['class' => 'form-control']) !!}

<div class="form-group col-sm-12">
    {!! Form::label('cost_proyecto_dis_plan', 'Resultado principal o impacto del proyecto:') !!}
    {!! Form::textarea('cost_proyecto_dis_plan', null, ['class' => 'form-control', 'rows' => 3, 'cols' => 40]) !!}
</div>
<div class="col-md-12">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> MARN</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Resolucion Marn Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('resolucion_marn', 'Resolucion Marn:') !!}
                    {!! Form::text('resolucion_marn', null, ['class' => 'form-control']) !!}
                </div>

                <!-- F Resolucion Marn Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('f_resolucion_marn', 'De fecha:') !!}
                    @if($bnd_edit == true)
                        {!! Form::text('f_resolucion_marn', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->f_resolucion_marn]) !!}
                    @else
                        {!! Form::text('f_resolucion_marn', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}

                    @endif
                </div>
                <!-- Medida Mitigacion Presupuesto Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('medida_mitigacion_presupuesto', 'Medidas de mitigación incluidas en presupuesto:') !!}
                    {!! Form::text('medida_mitigacion_presupuesto', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_marn', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_marn', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_marn', 'Links:') !!}
                    {!! Form::text('links_marn', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> AGRIP</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Estudio Suelo Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('agrip', 'AGRIP') !!}
                    {!! Form::select('agrip', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->agrip)?$costProyectoDisPlan->agrip:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('agrip_elaborado_por', 'AGRIP elaborado por:') !!}
                    {!! Form::text('agrip_elaborado_por', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Agrip F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('agrip_f', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('agrip_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        @if($costProyectoDisPlan->agrip_f != '-0001-11-30 00:00:00.000000')

                            {!! Form::text('agrip_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->agrip_f]) !!}
                        @else

                            {!! Form::text('agrip_f', null, ['class' => 'form-control datepicker','data-value'=>'']) !!}
                        @endif

                    @endif
                </div>
                <!-- Agrip Medida Mitigacion Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('agrip_medida_mitigacion', 'Medidas de mitigación incluidas en presupuesto:') !!}
                    {!! Form::text('agrip_medida_mitigacion', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_agrip', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_agrip', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_agrip', 'Links:') !!}
                    {!! Form::text('links_agrip', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-cafe">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Suelo</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Estudio Suelo Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_suelo', 'Estudios de suelos:') !!}
                    {!! Form::select('estudio_suelo', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->estudio_suelo)?$costProyectoDisPlan->estudio_suelo:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>

                <!-- Estudio Suelo F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_suelo_f', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('estudio_suelo_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('estudio_suelo_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->estudio_suelo_f]) !!}
                    @endif
                </div>

                <!-- Estudio Suelo Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_suelo_por', 'Hecho por:') !!}
                    {!! Form::text('estudio_suelo_por', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_suelo', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_suelo', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_suelo', 'Links:') !!}
                    {!! Form::text('links_suelo', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>

<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Topografía</h3>
        </div>
        <div class="box-body">
            <div class="row">

                <!-- Estudio Topografia Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_topografia', 'Estudio Topografia:') !!}
                    {!! Form::select('estudio_topografia', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'),isset($costProyectoDisPlan->estudio_topografia)?$costProyectoDisPlan->estudio_topografia:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>

                <!-- Estudio Topografia F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_topografia_f', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('estudio_topografia_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('estudio_topografia_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->estudio_topografia_f]) !!}
                    @endif
                </div>

                <!-- Estudio Topografia Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_topografia_por', 'Hecho por:') !!}
                    {!! Form::text('estudio_topografia_por', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_topografia', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_topografia', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_topografia', 'Links:') !!}
                    {!! Form::text('links_topografia', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Geológico</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Estudio Geologico Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_geologico', 'Estudio geológico:') !!}
                    {!! Form::select('estudio_geologico', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->estudio_geologico)?$costProyectoDisPlan->estudio_geologico:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>

                <!-- Estudio Geologico F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_geologico_f', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('estudio_geologico_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('estudio_geologico_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->estudio_geologico_f]) !!}
                    @endif
                </div>

                <!-- Estudio Geologico Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_geologico_por', 'Hecho por:') !!}
                    {!! Form::text('estudio_geologico_por', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_geologico', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_geologico', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_geologico', 'Links:') !!}
                    {!! Form::text('links_geologico', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>


<div class="col-md-12">
    <div class="box box-blue">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Hidrogeológico</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Estudio Geologico Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('estudio_hidrogeologico', 'Estudio hidrogeologico:') !!}
                    {!! Form::select('estudio_hidrogeologico', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->estudio_hidrogeologico)?$costProyectoDisPlan->estudio_hidrogeologico:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>

                <!-- Estudio Geologico F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('fecha_hidrogeologico', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('fecha_hidrogeologico', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('fecha_hidrogeologico', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->fecha_hidrogeologico]) !!}
                    @endif
                </div>

                <!-- Estudio Geologico Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('hecho_por_hidrogeologico', 'Hecho por:') !!}
                    {!! Form::text('hecho_por_hidrogeologico', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_hidrogeologico', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_hidrogeologico', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_hidrogeologico', 'Links:') !!}
                    {!! Form::text('links_hidrogeologico', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-maroon">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Estructuras</h3>
        </div>
        <div class="box-body">
            <div class="row">

                <!-- Diseno Estructural Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('diseno_estructural', 'Diseno estructural:') !!}
                    {!! Form::select('diseno_estructural', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->diseno_estructural)?$costProyectoDisPlan->diseno_estructural:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>

                <!-- Diseno Estructural F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('diseno_estructural_f', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('diseno_estructural_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('diseno_estructural_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->diseno_estructural_f]) !!}
                    @endif

                </div>

                <!-- Diseno Estructural Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('diseno_estructural_por', 'Hecho Por:') !!}
                    {!! Form::text('diseno_estructural_por', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_estructural', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_estructural', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_estructural', 'Links:') !!}
                    {!! Form::text('links_estructural', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>

<div class="col-md-12">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Memorias de cálculo</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Memoria Calculo Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('memoria_calculo', 'Memoria cálculo:') !!}
                    {!! Form::select('memoria_calculo', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->memoria_calculo)?$costProyectoDisPlan->memoria_calculo:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>
                <!-- Diseno Estructural F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('memoria_calculo_f', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('memoria_calculo_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('memoria_calculo_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->memoria_calculo_f]) !!}
                    @endif
                </div>

                <!-- Diseno Estructural Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('memoria_calculo_hecho_por', 'Hecho por:') !!}
                    {!! Form::text('memoria_calculo_hecho_por', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_memoria_calculo', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_memoria_calculo', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('liks_memoria_calculo', 'Links:') !!}
                    {!! Form::text('liks_memoria_calculo', null, ['class' => 'form-control']) !!}
                </div>


            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Planos Completos</h3>
        </div>
        <div class="box-body">
            <div class="row">

                <!-- Plano Completo Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('plano_completo', 'Plano Completo:') !!}
                    {!! Form::select('plano_completo', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->plano_completo)?$costProyectoDisPlan->plano_completo:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>

                <!-- Diseno Estructural F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('plano_completo_f', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('plano_completo_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('plano_completo_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->plano_completo_f]) !!}
                    @endif
                </div>

                <!-- Plano Completo Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('plano_completo_responsable', 'Hecho por:') !!}
                    {!! Form::text('plano_completo_responsable', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_plano_completo', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_plano_completo', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('liks_plano_completo', 'Links:') !!}
                    {!! Form::text('liks_plano_completo', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-sm-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Especificaciones Generales
            </h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Espe General Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('espe_general', 'Especificación General:') !!}
                    {!! Form::select('espe_general', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->espe_general)?$costProyectoDisPlan->espe_general:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>
                <!-- Espe General Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('espe_general_responsable', 'Hecho por:') !!}
                    {!! Form::text('espe_general_responsable', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_espe_general', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_espe_general', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_espe_general', 'Links:') !!}
                    {!! Form::text('links_espe_general', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-sm-12">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Especificaciones Técnicas</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Espe Tecnica Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('espe_tecnica', 'Especificación técnica:') !!}
                    {!! Form::select('espe_tecnica', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->espe_tecnica)?$costProyectoDisPlan->espe_tecnica:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>
                <!-- Diseno Estructural F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('espe_general_f', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('espe_general_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('espe_general_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->espe_general_f]) !!}
                    @endif
                </div>
                <!-- Espe Tecnica Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('espe_tecnica_responsable', 'Hecho por:') !!}
                    {!! Form::text('espe_tecnica_responsable', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_espe_tecnica', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_espe_tecnica', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_espe_tecnica', 'Links:') !!}
                    {!! Form::text('links_espe_tecnica', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-sm-12">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Disposiciones Especiales</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Dispo Especial Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('dispo_especial', 'Disposicion especial:') !!}
                    {!! Form::select('dispo_especial', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->dispo_especial)?$costProyectoDisPlan->dispo_especial:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>

                <!-- Dispo Especial Responsable Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('dispo_especial_f', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('dispo_especial_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('dispo_especial_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->dispo_especial_f]) !!}
                    @endif
                </div>
                <!-- Dispo Especial Responsable Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('dispo_especial_responsable', 'Hecho por:') !!}
                    {!! Form::text('dispo_especial_responsable', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('obeservaciones_dispo_espe', 'Observaciones:') !!}
                    {!! Form::textarea('obeservaciones_dispo_espe', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_dispo_especial', 'Links:') !!}
                    {!! Form::text('links_dispo_especial', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">Otros</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('otro_estudio', 'Otro') !!}
                    {!! Form::select('otro_estudio', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->otro_estudio)?$costProyectoDisPlan->otro_estudio:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>
                <!-- Agrip F Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('otro_f', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('otro_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('otro_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->otro_f]) !!}
                    @endif
                </div>
                <!-- Agrip Medida Mitigacion Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('otro_estudio_responsable', 'Hecho por:') !!}
                    {!! Form::text('otro_estudio_responsable', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('observaciones_otro', 'Observaciones:') !!}
                    {!! Form::textarea('observaciones_otro', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('links_otro', 'Links:') !!}
                    {!! Form::text('links_otro', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('dis_anuncio_invitacion', 'Anuncio o invitación a cotizar diseño:') !!}
                    {!! Form::text('dis_anuncio_invitacion', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('dis_terminos_referencia', 'Términos de Referencia para el Diseño o Estudio de Factibilidad:') !!}
                    {!! Form::text('dis_terminos_referencia', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-sm-12">
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Evaluaci&oacute;n ambiental
            </h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-sm-4">
                    {!! Form::label('inst_ea', 'Instrumento de evaluación ambiental:') !!}
                    {!! Form::select('inst_ea', array('0' => 'Seleccionar','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->inst_ea)?$costProyectoDisPlan->inst_ea:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>
                <!-- Espe General Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('links_inst_ea', 'Instrumento de evaluación ambiental links:') !!}
                    {!! Form::text('links_inst_ea', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Espe General Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('inst_ea_resoponsable', 'Elaborado por') !!}
                    {!! Form::text('inst_ea_resoponsable', null, ['class' => 'form-control']) !!}
                </div>
                <!-- F Resolucion Marn Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('inst_ea_d', 'De fecha:') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('inst_ea_d', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('inst_ea_d', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->inst_ea_d]) !!}
                    @endif
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-sm-12">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i>Presupuesto</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="form-group col-sm-4">
                    {!! Form::label('presupuesto_proyecto', 'Presupuesto (OC4IDS):') !!}
                    {!! Form::select('presupuesto_proyecto', array('0' => 'Seleccionaeeeer','1' => 'Si', '2' => 'No'), isset($costProyectoDisPlan->presupuesto_proyecto)?$costProyectoDisPlan->presupuesto_proyecto:null,['class' => 'form-control', 'style'=>'width: 100%;']) !!}
                </div>
                <div class="form-group col-sm-4">
                    {!! Form::label('presupuesto_aprobacion_f', 'De fecha (OC4IDS):') !!}
                    @if($bnd_edit == false)
                        {!! Form::text('presupuesto_aprobacion_f', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('presupuesto_aprobacion_f', null, ['class' => 'form-control datepicker','data-value'=>$costProyectoDisPlan->presupuesto_aprobacion_f]) !!}
                    @endif
                </div>
                <!-- Espe General Responsable Field -->
                <div class="form-group col-sm-4">
                    {!! Form::label('links_presupuesto', 'Links presupuesto:') !!}
                    {!! Form::text('links_presupuesto', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Espe General Responsable Field -->
                @include('chivos.catalogo', ['chivo_control' => 'presupuesto_moneda'
                         ,'chivo_id_cat'=>83
                         , 'chivo_default'=>null
                         ,'chivo_tamanio'=>'col-md-4'
                         ,'chivo_texto'=>'Tipo Moneda (OC4IDS):'])

                <div class="form-group col-sm-4">
                    {!! Form::label('costo_estimado_proyecto', 'Costo estimado del proyecto (OC4IDS):') !!}
                    {!! Form::text('costo_estimado_proyecto', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-body">
            <div class="row">
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('resp_diseno', 'Anuncio o invitación a cotizar diseño:') !!}
                    {!! Form::textarea('dis_anuncio_invitacion', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <!-- Agrip Elaborado Por Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('a_estudio_facti', 'Aprobación del estudio de factibilidad:') !!}
                    {!! Form::textarea('a_estudio_facti', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('a_entidades', 'Aprobación del estudio de factibilidad (Entidades):') !!}
                    {!! Form::textarea('a_entidades', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('a_derecho_paso', 'Aprobación del estudio de factibilidad (Derecho paso):') !!}
                    {!! Form::textarea('a_derecho_paso', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12" align="center">
    {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
    @if($bnd_edit == false)
        <a href="{!! route('costProyectos.show', [$costProyecto->id_proyecto,'a'=>1]) !!}"
           class="btn btn-danger">Cancelar</a>
    @else
        <a href="{!! route('costProyectos.show', [$costProyectoDisPlan->id_proyecto,'a'=>1]) !!}"
           class="btn btn-danger">Cancelar</a>
    @endif
</div>
