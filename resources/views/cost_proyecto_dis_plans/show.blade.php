@extends('layouts.app')
@include('flash::message')
@section('content')
    <section class="content-header">
        <a href="{!! route('costProyectos.show', [$costProyectoDisPlan->id_proyecto,'a'=>1]) !!}" class="btn btn-info">Volver panel principal</a>
        @if(Auth::check())
        <a href="{!! route('costProyectoDisPlans.edit', $id) !!}"
           class='btn btn-warning'><i class="glyphicon glyphicon-edit"></i> Editar registros</a>
        @endif
        <h2>
            2. Preparación del Proyecto
        </h2>
    </section>

    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_proyecto_dis_plans.show_fields')
                    <a href="{!! route('costProyectos.show', [$costProyectoDisPlan->id_proyecto,'a'=>1]) !!}" class="btn btn-info">Volver panel principal</a>
                </div>
            </div>
        </div>
    </div>
@endsection
