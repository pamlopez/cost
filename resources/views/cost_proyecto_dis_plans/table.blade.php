<table class="table table-responsive" id="costProyectoDisPlans-table">
    <thead>
        <tr>
            <th>Id Proyecto</th>
        <th>Dis Anuncio Invitacion</th>
        <th>Acta Adjudica</th>
        <th>Dis Responsable</th>
        <th>Dis Precio Contratado</th>
        <th>Dis Plazo Contrato</th>
        <th>Dis Tipo Contrato</th>
        <th>Dis Afianzadora</th>
        <th>Dis Fianza</th>
        <th>Dis F Inicio</th>
        <th>Dis F Entrega</th>
        <th>Dis Responsable Ea</th>
        <th>Dis F Aprobacion Contrato D</th>
        <th>Dis F Aprobacion Contrato</th>
        <th>Dis Anticipo</th>
        <th>Dis Numero Pagos</th>
        <th>Dis Fechas</th>
        <th>Dis Mod Plazo</th>
        <th>Dis Responsable Mod Plazo</th>
        <th>Dis F Nueva Finaliza</th>
        <th>Dis Monto Modificado</th>
        <th>Dis Monto Modificado D</th>
        <th>Dis Responsable Mod Monto</th>
        <th>Dis Porcentaje Cambio</th>
        <th>Dis Acta Recepcion</th>
        <th>Dis F Recepcion</th>
        <th>Dis F Recepcion D</th>
        <th>Dis F Finiquito</th>
        <th>Dis Fecha Finiquito Det</th>
        <th>Dis Fianza Detalle</th>
        <th>Dis Det Fianza</th>
        <th>Instrumento Eia</th>
        <th>Elaborado Por Eia</th>
        <th>F Eia</th>
        <th>Resolucion Marn</th>
        <th>F Resolucion Marn</th>
        <th>Medida Mitigacion Presupuesto</th>
        <th>Agrip</th>
        <th>Agrip F</th>
        <th>Agrip Elaborado Por</th>
        <th>Agrip Medida Mitigacion</th>
        <th>Responsable Estudio Tecnico</th>
        <th>Estudio Suelo</th>
        <th>Estudio Suelo F</th>
        <th>Estudio Suelo Por</th>
        <th>Estudio Topografia</th>
        <th>Estudio Topografia F</th>
        <th>Estudio Topografia Por</th>
        <th>Estudio Geologico</th>
        <th>Estudio Geologico F</th>
        <th>Estudio Geologico Por</th>
        <th>Diseno Estructural</th>
        <th>Diseno Estructural F</th>
        <th>Diseno Estructural Por</th>
        <th>Memoria Calculo</th>
        <th>Memoria Calculo Responsable</th>
        <th>Plano Completo</th>
        <th>Plano Completo Responsable</th>
        <th>Espe Tecnica</th>
        <th>Espe Tecnica Responsable</th>
        <th>Espe General</th>
        <th>Espe General Responsable</th>
        <th>Dispo Especial</th>
        <th>Dispo Especial Responsable</th>
        <th>Otro Estudio</th>
        <th>Otro Estudio Responsable</th>
        <th>Presupuesto Proyecto</th>
        <th>Costo Estimado Proyecto</th>
        <th>Presupuesto Aprobacion F</th>
        <th>Estado Actual Proyecto D</th>
        <th>Estado Actual Proyecto</th>
        <th>Avance Fisico</th>
        <th>Avance Financiero</th>
        <th>Modalidad Contratacion</th>
        <th>Fuente Financiamiento Dis</th>
        <th>Monto Asignado Dis</th>
        <th>Nog</th>
        <th>Nog F</th>
        <th>Fuente F Sup</th>
        <th>Monto Sup</th>
        <th>Fuente F Ejecucion</th>
        <th>Monto Ejecucion</th>
        <th>Fuente F Ambiental</th>
        <th>Monto Ambiental</th>
        <th>Fuente Riesgo</th>
        <th>Monto Riesgo</th>
        <th>Estudio Viabilidad</th>
        <th>Aprobacion Viabilidad</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($costProyectoDisPlans as $costProyectoDisPlan)
        <tr>
            <td>{!! $costProyectoDisPlan->id_proyecto !!}</td>
            <td>{!! $costProyectoDisPlan->dis_anuncio_invitacion !!}</td>
            <td>{!! $costProyectoDisPlan->acta_adjudica !!}</td>
            <td>{!! $costProyectoDisPlan->dis_responsable !!}</td>
            <td>{!! $costProyectoDisPlan->dis_precio_contratado !!}</td>
            <td>{!! $costProyectoDisPlan->dis_plazo_contrato !!}</td>
            <td>{!! $costProyectoDisPlan->dis_tipo_contrato !!}</td>
            <td>{!! $costProyectoDisPlan->dis_afianzadora !!}</td>
            <td>{!! $costProyectoDisPlan->dis_fianza !!}</td>
            <td>{!! $costProyectoDisPlan->dis_f_inicio !!}</td>
            <td>{!! $costProyectoDisPlan->dis_f_entrega !!}</td>
            <td>{!! $costProyectoDisPlan->dis_responsable_ea !!}</td>
            <td>{!! $costProyectoDisPlan->dis_f_aprobacion_contrato_d !!}</td>
            <td>{!! $costProyectoDisPlan->dis_f_aprobacion_contrato !!}</td>
            <td>{!! $costProyectoDisPlan->dis_anticipo !!}</td>
            <td>{!! $costProyectoDisPlan->dis_numero_pagos !!}</td>
            <td>{!! $costProyectoDisPlan->dis_fechas !!}</td>
            <td>{!! $costProyectoDisPlan->dis_mod_plazo !!}</td>
            <td>{!! $costProyectoDisPlan->dis_responsable_mod_plazo !!}</td>
            <td>{!! $costProyectoDisPlan->dis_f_nueva_finaliza !!}</td>
            <td>{!! $costProyectoDisPlan->dis_monto_modificado !!}</td>
            <td>{!! $costProyectoDisPlan->dis_monto_modificado_d !!}</td>
            <td>{!! $costProyectoDisPlan->dis_responsable_mod_monto !!}</td>
            <td>{!! $costProyectoDisPlan->dis_porcentaje_cambio !!}</td>
            <td>{!! $costProyectoDisPlan->dis_acta_recepcion !!}</td>
            <td>{!! $costProyectoDisPlan->dis_f_recepcion !!}</td>
            <td>{!! $costProyectoDisPlan->dis_f_recepcion_d !!}</td>
            <td>{!! $costProyectoDisPlan->dis_f_finiquito !!}</td>
            <td>{!! $costProyectoDisPlan->dis_fecha_finiquito_det !!}</td>
            <td>{!! $costProyectoDisPlan->dis_fianza_detalle !!}</td>
            <td>{!! $costProyectoDisPlan->dis_det_fianza !!}</td>
            <td>{!! $costProyectoDisPlan->instrumento_eia !!}</td>
            <td>{!! $costProyectoDisPlan->elaborado_por_eia !!}</td>
            <td>{!! $costProyectoDisPlan->f_eia !!}</td>
            <td>{!! $costProyectoDisPlan->resolucion_marn !!}</td>
            <td>{!! $costProyectoDisPlan->f_resolucion_marn !!}</td>
            <td>{!! $costProyectoDisPlan->medida_mitigacion_presupuesto !!}</td>
            <td>{!! $costProyectoDisPlan->agrip !!}</td>
            <td>{!! $costProyectoDisPlan->agrip_f !!}</td>
            <td>{!! $costProyectoDisPlan->agrip_elaborado_por !!}</td>
            <td>{!! $costProyectoDisPlan->agrip_medida_mitigacion !!}</td>
            <td>{!! $costProyectoDisPlan->responsable_estudio_tecnico !!}</td>
            <td>{!! $costProyectoDisPlan->estudio_suelo !!}</td>
            <td>{!! $costProyectoDisPlan->estudio_suelo_f !!}</td>
            <td>{!! $costProyectoDisPlan->estudio_suelo_por !!}</td>
            <td>{!! $costProyectoDisPlan->estudio_topografia !!}</td>
            <td>{!! $costProyectoDisPlan->estudio_topografia_f !!}</td>
            <td>{!! $costProyectoDisPlan->estudio_topografia_por !!}</td>
            <td>{!! $costProyectoDisPlan->estudio_geologico !!}</td>
            <td>{!! $costProyectoDisPlan->estudio_geologico_f !!}</td>
            <td>{!! $costProyectoDisPlan->estudio_geologico_por !!}</td>
            <td>{!! $costProyectoDisPlan->diseno_estructural !!}</td>
            <td>{!! $costProyectoDisPlan->diseno_estructural_f !!}</td>
            <td>{!! $costProyectoDisPlan->diseno_estructural_por !!}</td>
            <td>{!! $costProyectoDisPlan->memoria_calculo !!}</td>
            <td>{!! $costProyectoDisPlan->memoria_calculo_responsable !!}</td>
            <td>{!! $costProyectoDisPlan->plano_completo !!}</td>
            <td>{!! $costProyectoDisPlan->plano_completo_responsable !!}</td>
            <td>{!! $costProyectoDisPlan->espe_tecnica !!}</td>
            <td>{!! $costProyectoDisPlan->espe_tecnica_responsable !!}</td>
            <td>{!! $costProyectoDisPlan->espe_general !!}</td>
            <td>{!! $costProyectoDisPlan->espe_general_responsable !!}</td>
            <td>{!! $costProyectoDisPlan->dispo_especial !!}</td>
            <td>{!! $costProyectoDisPlan->dispo_especial_responsable !!}</td>
            <td>{!! $costProyectoDisPlan->otro_estudio !!}</td>
            <td>{!! $costProyectoDisPlan->otro_estudio_responsable !!}</td>
            <td>{!! $costProyectoDisPlan->presupuesto_proyecto !!}</td>
            <td>{!! $costProyectoDisPlan->costo_estimado_proyecto !!}</td>
            <td>{!! $costProyectoDisPlan->presupuesto_aprobacion_f !!}</td>
            <td>{!! $costProyectoDisPlan->estado_actual_proyecto_d !!}</td>
            <td>{!! $costProyectoDisPlan->estado_actual_proyecto !!}</td>
            <td>{!! $costProyectoDisPlan->avance_fisico !!}</td>
            <td>{!! $costProyectoDisPlan->avance_financiero !!}</td>
            <td>{!! $costProyectoDisPlan->modalidad_contratacion !!}</td>
            <td>{!! $costProyectoDisPlan->fuente_financiamiento_dis !!}</td>
            <td>{!! $costProyectoDisPlan->monto_asignado_dis !!}</td>
            <td>{!! $costProyectoDisPlan->nog !!}</td>
            <td>{!! $costProyectoDisPlan->nog_f !!}</td>
            <td>{!! $costProyectoDisPlan->fuente_f_sup !!}</td>
            <td>{!! $costProyectoDisPlan->monto_sup !!}</td>
            <td>{!! $costProyectoDisPlan->fuente_f_ejecucion !!}</td>
            <td>{!! $costProyectoDisPlan->monto_ejecucion !!}</td>
            <td>{!! $costProyectoDisPlan->fuente_f_ambiental !!}</td>
            <td>{!! $costProyectoDisPlan->monto_ambiental !!}</td>
            <td>{!! $costProyectoDisPlan->fuente_riesgo !!}</td>
            <td>{!! $costProyectoDisPlan->monto_riesgo !!}</td>
            <td>{!! $costProyectoDisPlan->estudio_viabilidad !!}</td>
            <td>{!! $costProyectoDisPlan->aprobacion_viabilidad !!}</td>
            <td>
                {!! Form::open(['route' => ['costProyectoDisPlans.destroy', $costProyectoDisPlan->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costProyectoDisPlans.show', [$costProyectoDisPlan->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('costProyectoDisPlans.edit', [$costProyectoDisPlan->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>