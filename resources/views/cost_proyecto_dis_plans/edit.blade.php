@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>2. Preparaci&oacute;n del proyecto</h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costProyectoDisPlan, ['route' => ['costProyectoDisPlans.update', $costProyectoDisPlan->id], 'method' => 'patch']) !!}

                        @include('cost_proyecto_dis_plans.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection