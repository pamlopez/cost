@extends('layouts.app')

@section('content')
    <div class="content"  style="margin-top: -2%">
        @include('adminlte-templates::common.errors')
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><b>2.  Preparación del Proyecto</b></h3>
                <h3 align="center">
                    <b>Nombre del proyecto: {{$costProyecto->nombre_proyecto}}</b>
                </h3>
            </div>

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'costProyectoDisPlans.store']) !!}

                        @include('cost_proyecto_dis_plans.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
