@php($cont = 1)
@include('reportes._style')

<div class="scroll-div-tablas" id="TablaContenedor">
    <table class=" table-bordered table-striped " id="Topicos_Datatable">
    <thead style="background-color: #8e24aa; color: #FFFFFF" width="100%"  id="header-fixed">
        <th># </th>
        <th>Nombre y apellido </th>
        <th>Mes </th>
        <th>Fecha </th>
        <th>Departamento </th>
        <th>Municipio </th>
        <th>DPI </th>
        <th>M </th>
        <th>F </th>
        <th>0 a 13 años </th>
        <th>14 a 30 años </th>
        <th>31 a 60 años </th>
        <th>Mayores de 60 años </th>
        <th>Maya </th>
        <th>Xinca </th>
        <th>Garífuna </th>
        <th>Ladino </th>
        <th>Otro </th>
        <th>De donde nos visita </th>
        <th>Tipología </th>
    </thead>
    <tbody>
        @foreach($atencionInicials as $at)
            @if(!empty($at->colonia))
                @php($visita ='Zona '.$at->rel_id_persona->FmtZonaDomicilio.', '.$at->colonia)
            @elseif(!empty($at->extranjero))
                @php($visita =$at->extranjero)
            @else
                @php($visita = $at->FmtIdMuniDomicilio.', '.$at->FmtIdDeptoDomicilio )
            @endif
          <tr>
              <td>{{$cont++}}</td>
              <td width="25%" >{!! $at->FmtIdPersona !!}</td>
              <td>{{$at->fecha->format('m')}}</td>
              <td>{{$at->fecha->format('d/m/Y')}}</td>
              <td>{{$at->FmtIdDeptoDomicilio}}</td>
              <td>{{$at->FmtIdMuniDomicilio}}</td>
              <td>{{$at->no_identificacion}}</td>
              <td align="center">{{$at->genero==2?'X':''}}</td>
              <td align="center">{{$at->genero==1?'X':''}}</td>
              <td align="center">{{$at->id_rango_mingob==70?'X':''}}</td>
              <td align="center">{{$at->id_rango_mingob==71?'X':''}}</td>
              <td align="center">{{$at->id_rango_mingob==72?'X':''}}</td>
              <td align="center">{{$at->id_rango_mingob==73?'X':''}}</td>
              <td align="center">{{$at->id_etnia==32?'X':''}}</td>
              <td align="center">{{$at->id_etnia==33?'X':''}}</td>
              <td align="center">{{$at->id_etnia==34?'X':''}}</td>
              <td align="center">{{$at->id_etnia==35 || $at->id_etnia==107?'X':''}}</td>
              <td align="center">{{$at->id_etnia==36?'X':''}}</td>
              <td align="center">{{$visita}}</td>
              @php($tipologia = \App\Models\atencion_inicial_tipo_violencia::where('id_atencion_inicial',$at->id_atencion_inicial)->get())
              <td>
                  @foreach($tipologia as $tp)
                    {{($tp->rel_id_atencion_inicial_tipo_violencia->descripcion != null)?$tp->rel_id_atencion_inicial_tipo_violencia->descripcion:''}},
                  @endforeach
              </td>
          </tr>
        @endforeach
    </tbody>
</table>
</div>