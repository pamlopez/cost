@php($titulo_reporte = "Acciones por abogadas")
@php($subtitulo = "Detalle de Acciones por abogadas")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> {{ $subtitulo }}</b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>

    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_agenda_responsable" class="table table-hover table-bordered ">
                        <thead>
                        <tr class="tbl-encabezados">
                            <th colspan="{{ $consulta_agenda_abogada->groupBy("nombres")->count()+2 }}">
                                <h4 class="text-center">
                                    <b>{{ $subtitulo }}</b>
                                </h4>
                            </th>
                        </tr>
                        <tr class="tbl-encabezados">
                            <th>Tipo de acción</th>
                            @foreach( $consulta_agenda_abogada->groupBy("nombres") as $kaa => $vaa)
                                <th>{{ $kaa }}</th>
                            @endforeach
                            <th>Cantidad</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @php($total_iniciado=null)
                        @php($total_piniciar=null)
                        @foreach($consulta_agenda_abogada->groupBy("descripcion") as $kam =>$vam)
                            <tr>
                                <td>{{ $data_x[]=$kam }}</td>
                                @php($subtotal=[])
                                @foreach( $consulta_agenda_abogada->groupBy("nombres") as $kaa => $vaa)
                                    @php($consulta = $vaa->where("descripcion",$kam)->where("nombres",$kaa))
                                    <td>{{ $subtotal[] = $consulta->sum("cantidad") }}</td>
                                @endforeach
                                <td>{{ $data_y[]=array_sum($subtotal)  }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            @foreach( $consulta_agenda_abogada->groupBy("nombres") as $kaa => $vaa)
                                @php($consulta = $vaa->where("nombres",$kaa))
                                <td>{{ $consulta->sum("cantidad") }}</td>
                            @endforeach
                            <td>{{ array_sum($data_y)}}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_por_agenda_responsable" class="chart">
                        Area g_por_agenda_responsable
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_p_mes = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Cantidad"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_por_agenda_responsable').highcharts(
                    {!! $js_data_chart_p_mes !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_agenda_responsable').DataTable(json_data_table);
        } );
    </script>
@endpush
