@extends('layouts.app')
@section('content')
    <style>

        h3{
            font-family: 'Josefin Sans', sans-serif;
        }

        .box{
            padding:60px 0px;
        }

        .box-part{
            background:#FFF;
            border-radius:10px;
            padding:60px 10px;
            margin:30px 0px;
        }

        .box-part:hover{
            background:#4183D7;
        }

        .box-part:hover .fa ,
        .box-part:hover .title ,
        .box-part:hover .text ,
        .box-part:hover a{
            color:#FFF;
            -webkit-transition: all 1s ease-out;
            -moz-transition: all 1s ease-out;
            -o-transition: all 1s ease-out;
            transition: all 1s ease-out;
        }

        .text{
            margin:20px 0px;
        }

        .fao{
            color:#4183D7;
        }
    </style>
    <div class="">
        <div class="container" style="margin-top: -3%">
            <h3 align="center" style="color: #605CA8;"><b> <i class="fa fa-line-chart" aria-hidden="true"></i> Informe estadístico de acciones área legal </b></h3>
            <div class="row">
                @foreach($data as $kd=>$d)
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                        <div class="box-part text-center">
                            {!!  $d["icon"] !!}

                            <div class="title">
                                <h3>{!!  1+$kd.". ".$d["title"] !!}</h3>
                            </div>

                            <div class="text">
                            </div>

                            <a href="{!! url($d["url"]) !!}">Ver an&aacute;lisis estad&iacute;stico</a>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection