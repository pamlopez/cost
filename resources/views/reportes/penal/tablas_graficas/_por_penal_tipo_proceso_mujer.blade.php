@php($titulo_reporte = "Procesos penales por mujer en el año $array_rango_anio[1]")
@php($subtitulo = "Detalle de Procesos penales por mujer")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> 3.4 {{ $subtitulo  }} </b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>
    <div class="box-body">
        <div class="box">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_familia_tipo_proceso_mujer" class="table table-hover table-bordered ">
                        <thead>

                            <tr class="tbl-encabezados">
                                <th>Nombres</th>
                                @foreach($desc_ripo_proceso_per as $kepi => $vepi)
                                    <th>{{ $vepi }}</th>
                                @endforeach
                                <th>Sub-total</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($consulta_tipo_proceso_persona->sortBy("nombre")->groupBy("id_per") as $kap =>$vap)
                            <tr>
                                <td>{{ $data_x[]=$vap->first()->nombre }}</td>
                                @php($sub_total= [])
                                @foreach($desc_ripo_proceso_per as $kepf => $vepf)
                                    @php($sub_consulta = $consulta_tipo_proceso_persona->where("id_per",$kap)->where("id_tipo_proceso",$vepf))
                                    <td>{{ $sub_total[]= $vap->where("id_tipo_proceso",$kepf)->sum("cantidad")  }}</td>
                                @endforeach
                                <td>{{ $data_y[]=array_sum($sub_total) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            @php($sub_t_total= null)
                            @foreach($desc_ripo_proceso_per as $kepf => $vepf)
                                @php($sub_consulta = $consulta_tipo_proceso_persona->where("id_tipo_proceso",$kepf))
                                <td>{{ $sub_consulta->sum("cantidad" )  }}</td>
                            @endforeach
                            <td>{{ array_sum($data_y) }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.box -->
    </div>
</div>
@push('javascript')
    <script type="text/javascript">
        var base64_data_table = '{!! $json_data_table !!}';
        var json_data_table_special = JSON.parse(atob(base64_data_table));
        json_data_table_special.searching = true;
        json_data_table_special.paging = true;
        json_data_table_special.ordering = true;
        json_data_table_special.info = true;
        json_data_table_special.scrollY=true;
        json_data_table_special.scrollX=true;
        json_data_table_special.scrollCollapse= true;
        json_data_table_special.buttons.push({"extend":"colvis","className":"btn bg-black",columnText: function ( dt, idx, title ) {
                return (idx+1)+': '+title;
            }});
        json_data_table_special.fixedColumns=   {
            leftColumns: 1
        };

        json_data_table_special.language.buttons.colvis = '<i class="fa fa-eye"></i> Ver Columnas'
        $(document).ready(function() {
            $('#tabla_familia_tipo_proceso_mujer').DataTable(json_data_table_special);
        } );
    </script>
@endpush
