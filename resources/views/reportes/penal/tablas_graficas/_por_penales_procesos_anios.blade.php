@php($titulo_reporte = "Procesos penales por año de inicio del proceso")
@php($subtitulo = "Detalle de Procesos penales por año de inicio del proceso")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> 3.1 Procesos penales por año de inicio del proceso </b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>
    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_penal_proceso_anio" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="{{ $estado_penal_anio_desc->count()+2 }}">
                                    <h4 class="text-center">
                                        <b>{{ $subtitulo }}</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Año</th>
                                @foreach($estado_penal_anio_desc as $kepi => $vepi)
                                    <th>{{ $vepi }}</th>
                                @endforeach
                                <th>Sub-total</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($rango_anios as $kap =>$vap)
                            <tr>
                                <td>{{ $data_x[]=$vap }}</td>
                                @php($sub_total= [])
                                @foreach($estado_penal_anio_id as $kepf => $vepf)
                                    @php($sub_consulta = $consulta_proceso->where("id_estado",$vepf)->where("anio",$vap))
                                    <td>{{ $sub_total[]= $sub_consulta->sum("cantidad")  }}</td>
                                @endforeach
                                <td>{{ $data_y[]=array_sum($sub_total) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            @php($sub_t_total= null)
                            @foreach($estado_penal_anio_id as $kepf => $vepf)
                                <td>{{ $consulta_proceso->where("id_estado",$vepf)->sum("cantidad")  }}</td>
                            @endforeach
                            <td>{{ $consulta_proceso->sum("cantidad" )  }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_penal_proceso_anio" class="chart">
                        Area g_penal_proceso_anio
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_proceso_anio = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Cantidad"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_penal_proceso_anio').highcharts(
                    {!! $js_data_chart_proceso_anio !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_penal_proceso_anio').DataTable(json_data_table);
        } );
    </script>
@endpush
