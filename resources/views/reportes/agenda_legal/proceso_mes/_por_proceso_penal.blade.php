@php($titulo_reporte = "Tipos de procesos por mes penales")
@php($subtitulo = "Detalle de Tipos de procesos penales")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> {{ $subtitulo }}</b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>

    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_agenda_proceso_penales" class="table table-hover table-bordered ">
                        <thead>
                        <tr class="tbl-encabezados">
                            <th colspan="{{ 2+$proceso_mes->where("id_tipo_demanda",1)->count() }}">
                                <h4 class="text-center">
                                    <b>{{ $subtitulo }}</b>
                                </h4>
                            </th>
                        </tr>
                        <tr class="tbl-encabezados">
                            <th>Tipo de proceso</th>
                            @foreach($proceso_mes->where("id_tipo_demanda",1)->groupBy("fecha") as $kf => $vf)
                                <th>{{ $kf }}</th>
                            @endforeach
                            <th>Sub-Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @php($total_iniciado=null)
                        @php($total_piniciar=null)
                        @foreach($proceso_mes->where("id_tipo_demanda",1)->sortBy("descripcion")->groupBy("descripcion") as $kam =>$vam)
                            <tr>
                                <td>{{ $data_x[]=$kam }}</td>
                                @php($sub_total=[])
                                @foreach($proceso_mes->where("id_tipo_demanda",1)->groupBy("fecha") as $kf => $vf)
                                    @php($consulta = $proceso_mes->where("id_tipo_demanda",1)->where("descripcion",$kam)->where("fecha",$kf))
                                    <td>{{ $sub_total[]=$consulta->sum("cantidad") }}</td>
                                @endforeach
                                <td>{{ $data_y[]=array_sum($sub_total)  }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            @foreach($proceso_mes->where("id_tipo_demanda",1)->groupBy("fecha") as $kf => $vf)
                                @php($consulta = $proceso_mes->where("id_tipo_demanda",1)->where("fecha",$kf))
                                <td>{{ $consulta->sum("cantidad") }}</td>
                            @endforeach
                            <td>{{ array_sum($data_y) }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_por_agenda_proceso_penales" class="chart">
                        Area g_por_agenda_proceso_penales
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_p_mes = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Cantidad"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_por_agenda_proceso_penales').highcharts(
                    {!! $js_data_chart_p_mes !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_agenda_proceso_penales').DataTable(json_data_table);
        } );
    </script>
@endpush
