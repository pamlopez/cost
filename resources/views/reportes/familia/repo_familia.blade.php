@extends('layouts.app')
@include('reportes._style')
@push('styles')
    <link rel="stylesheet" href="{{ url("/css/reportes/bootstrap-datetimepicker/bootstrap-datetimepicker.css") }}">
    <link rel="stylesheet" href="{{ url("/css/reportes/datatables/fixedColumns.dataTables.min.css") }}">
    <link rel="stylesheet" href="{{ url("/css/reportes/datatables/buttons.dataTables.css") }}">
@endpush
@section('content')
    @push('javascript')
        <script src="{{ url('/js/reportes/data_tables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url('/js/reportes/data_tables/dataTables.buttons.min.js') }}"></script>
        <script src="{{ url('/js/reportes/data_tables/buttons.flash.min.js') }}"></script>
        <script src="{{ url('/js/reportes/data_tables/jszip.min.js') }}"></script>
        <script src="{{ url('/js/reportes/data_tables/pdfmake.min.js') }}"></script>
        <script src="{{ url('/js/reportes/data_tables/vfs_fonts.js') }}"></script>
        <script src="{{ url('/js/reportes/data_tables/dataTables.fixedColumns.min.js') }}"></script>
        <script src="{{ url('/js/reportes/data_tables/buttons.html5.min.js') }}"></script>
        <script src="{{ url('/js/reportes/data_tables/buttons.print.min.js') }}"></script>
        <script src="{{ url('/js/reportes/data_tables/buttons.colVis.min.js') }}"></script>
        <script type="text/javascript">
            var base64_data_table = '{!! $json_data_table !!}';
            var json_data_table = JSON.parse(atob(base64_data_table));
        </script>
    @endpush
    <div class="content" style="margin-top: -3%;">
        <div class="clearfix"></div>
        @include('partials.control_fechas_anios')

        <div class="clearfix"></div>
        @include('reportes.familia.tablas_graficas._por_familia_inicio')
        <div class="clearfix"></div>
        @include('reportes.familia.tablas_graficas._por_familia_tipo_proceso')
        <div class="clearfix"></div>
        @include('reportes.familia.tablas_graficas._por_familia_juzgado')
        <div class="clearfix"></div>
        @include('reportes.familia.tablas_graficas._por_familia_proceso_tramite')
        <div class="clearfix"></div>
        @include('reportes.familia.tablas_graficas._por_familia_tipo_proceso_mujer')
    </div>
@endsection
