@php($titulo_reporte = "Tramites en juzgados año $array_rango_anio[1]")
@php($subtitulo = "Detalle Tramites en juzgados")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> 2.3 Juzgados</b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>
    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_familia_juzgado" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="2">
                                    <h4 class="text-center">
                                        <b>{{ $subtitulo }}</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Juzgado</th>
                                <th>Sub-total Estatus</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($consulta_proceso_juzgado as $kap =>$vap)
                            <tr>
                                <td>{{ $data_x[]=isset($tipo_proceso_fam_juzgadodes[$vap->id_juzgado]) ? $tipo_proceso_fam_juzgadodes[$vap->id_juzgado] : '--' }}</td>
                                <td>{{ $data_y[]=$vap->cantidad }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            <td>{{ array_sum($data_y) }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_por_juzgado" class="chart">
                        Area g_por_juzgado
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_juzgado = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Cantidad"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_por_juzgado').highcharts(
                    {!! $js_data_chart_juzgado !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_familia_juzgado').DataTable(json_data_table);
        } );
    </script>
@endpush
