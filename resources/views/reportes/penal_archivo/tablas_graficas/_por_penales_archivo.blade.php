@php($titulo_reporte = "Motivo de archivo del año $array_rango_anio[1] (PENAL)")
@php($subtitulo = "Detalle de Motivo de archivo")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> 4.1 Motivo de archivo </b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>
    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_penal_archivo_motivo" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="2">
                                    <h4 class="text-center">
                                        <b>{{ $subtitulo }}</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Motivo</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach($consulta_motivo_archivo->sortBy("juzgado") as $kap =>$vap)
                            <tr>
                                <td>{{ $data_x[]=$vap->motivo }}</td>
                                <td>{{ $data_y[]=$vap->cantidad }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            <td>{{ $consulta_motivo_archivo->sum("cantidad" )  }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_penal_motivo_archivo" class="chart">
                        Area g_penal_motivo_archivo
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_motivo_archivo = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Cantidad"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_penal_motivo_archivo').highcharts(
                    {!! $js_data_chart_motivo_archivo !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_penal_archivo_motivo').DataTable(json_data_table);
        } );
    </script>
@endpush
