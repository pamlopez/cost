@extends('layouts.app')
@section('content')
    <div class="content" style="margin-top: -3%">
        <div class="clearfix"></div>
        @include('partials.control_fechas')
        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <a class=" btn btn-success  pull-right"  onclick="tableToExcel('Topicos_Datatable','Reporte MINGOB ')" >
                    <i class="fa fa-file-excel-o" aria-hidden="true" ></i> Exportar reporte a excel
                </a>
                <h4 class="pull-left"><b> <i class="fa fa-file-excel-o" aria-hidden="true"></i> Reporte Ministerio de Gobernaci&oacute;n</b></h4>
                <br>
                <h3 align="center" style="color: #4a148c;">
                    <b>&nbsp;&nbsp;&nbsp; Resultados del
                   ( {{\Carbon\Carbon::createFromFormat("Y-m-d",$filtros->fecha_del)->format("d-m-Y")}} ) al
                   ( {{\Carbon\Carbon::createFromFormat("Y-m-d",$filtros->fecha_al)->format("d-m-Y")}} )
                    </b>
                </h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    @include('reportes._tabla_mingob')
                </div>
            </div>
        </div>
    </div>
@endsection
@push('javascript')
   @include('reportes._export_excel')
@endpush