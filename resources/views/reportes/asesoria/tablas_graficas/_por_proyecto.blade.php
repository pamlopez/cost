@php($titulo_reporte = "Proyectos de demanda según año  $array_rango_anio[1]")
@php($subtitulo = "Detalle de proyectos de demanda")
@php($data_pie["data"] = [])
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b> 1.6.2 Tipos de demandas por iniciar</b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>

    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_proyecto" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="{{ count($rango_anios)+2 }}">
                                    <h4 class="text-center">
                                        <b>{{ $subtitulo }}</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Procesos por Iniciar</th>
                                <th>Cantidad Proyectos Dem</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @foreach($proyectos->groupBy("id_item") as $kp =>$vp)
                            <tr>
                                @php($consulta = $vp->where("id_item",$kp))
                                <td>{{ $name=isset($consulta->first()->descripcion) ? $consulta->first()->descripcion : '--' }}</td>
                                <td>{{ $y=$consulta->sum("cantidad")  }}</td>
                                @php($total[]=$y)
                                @php($data_x[]=$name)
                                @php($data_y[]=$y)
                                @php($data_pie["data"][] = ["name"=>$name,"y"=>$y])
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            <td>{{ array_sum($total) }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_por_proyecto_pie" class="chart" style="height: 500px">
                        Area g_por_proyecto_pie
                    </div>
                    <br>
                    <div id="g_por_proyecto_barra" class="chart" style="height: 500px">
                        Area g_por_proyecto_barra
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_proyecto_pie =\App\Models\asesoria::g_pie($data_pie,$titulo_reporte,$subtitulo,$subtitulo))
@php($js_data_chart_proyecto_barra = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Cantidad"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_por_proyecto_pie').highcharts(
                    {!! $js_data_chart_proyecto_pie !!}
            )}
        );
        $(function () {
            $('#g_por_proyecto_barra').highcharts(
                    {!! $js_data_chart_proyecto_barra !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_proyecto').DataTable(json_data_table);
        } );
    </script>
@endpush
