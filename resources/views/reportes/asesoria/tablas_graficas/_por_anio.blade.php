@php($titulo_reporte = "Resultados del año  $array_rango_anio[0] al $array_rango_anio[1]")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b>1.1 Citas según H.V. del año {{ $array_rango_anio[0] }} al {{ $array_rango_anio[1] }}</b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>

    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_anio" class="table table-hover table-bordered ">
                        <thead>
                        <tr class="tbl-encabezados">
                            <th colspan="{{ count($rango_anios)+2 }}">
                                <h4 class="text-center">
                                    <b>Año de H.V.</b>
                                </h4>
                            </th>
                        </tr>
                        <tr class="tbl-encabezados">
                            <th>Mes</th>
                            @foreach($rango_anios as $kra =>$vra)
                                @php($consulta = $consulta_asesoria->where("anio",$vra))
                                @php($data_x[] = $vra)
                                @php($data_y[] = $consulta->count() > 0 ? $consulta->sum("atendidos") : 0 )
                                <th>{{ $vra }}</th>
                            @endforeach
                            <th>Sub-total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @foreach($array_meses as $kam =>$vam)
                            <tr>
                                <td>{{ $mes[-1+$vam] }}</td>
                                @php($sub_total=null)
                                @foreach($rango_anios as $kaa => $vaa)
                                    @php($consulta = $consulta_asesoria->where("anio",$vaa)->where("mes",$vam)->first())
                                    <td>{{ $consulta=$consulta ? $consulta->atendidos : 0  }}</td>
                                    @php($sub_total[]=(int)$consulta)
                                @endforeach
                                <td>{{ $total[]=array_sum($sub_total) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            @foreach($rango_anios as $kaa2 => $vaa2)
                                @php($consulta2 = $consulta_asesoria->where("anio",$vaa2))
                                <td>{{ $consulta2->sum("atendidos") }}</td>
                            @endforeach
                            <td>{{ array_sum($total) }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_asesoria_anio" class="chart">
                        Area g_anio
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_anio = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Atendidas"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_asesoria_anio').highcharts(
                    {!! $js_data_chart_anio !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_anio').DataTable(json_data_table);
        } );
    </script>
@endpush
