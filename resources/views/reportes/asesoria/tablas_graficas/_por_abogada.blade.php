@php($titulo_reporte = $consulta_asesoria_abogada->sum("atendidos")." Citas atendidas por abogadas del año  $array_rango_anio[1]")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b>1.3 Citas por abogada </b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>

    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_abogada" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="{{ count($rango_anios)+2 }}">
                                    <h4 class="text-center">
                                        <b>Listado de Abogadas que atendieron citas en el año {{ $array_rango_anio[1] }}</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Abogada</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @foreach($consulta_asesoria_abogada as $kcaa =>$vcaa)
                            <tr>
                                <td>{{ $vcaa->abogada }}</td>
                                <td>{{ $vcaa->atendidos }}</td>
                                @php($data_x[] = $vcaa->abogada)
                                @php($data_y[] = (int)$vcaa->atendidos)
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            <td>{{ array_sum($data_y) }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_asesoria_abogada" class="chart">
                        Area g_abogada
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_abogada = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Atendidas"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_asesoria_abogada').highcharts(
                    {!! $js_data_chart_abogada !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_abogada').DataTable(json_data_table);
        } );
    </script>
@endpush
