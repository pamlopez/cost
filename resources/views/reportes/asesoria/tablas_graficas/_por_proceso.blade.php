@php($titulo_reporte = "Procesos iniciados y por iniciar año  $array_rango_anio[1]")
@php($subtitulo = "Detalle de Procesos iniciados y por iniciar")
@php($data_pie["data"] = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left">
            <b>1.4 Cantidad de proecesos (procesos iniciados y por iniciar) </b>
        </h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>

    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_proceso" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="{{ count($rango_anios)+2 }}">
                                    <h4 class="text-center">
                                        <b>{{ $subtitulo }}</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Mes</th>
                                @foreach($criterio_fijo_proceso as $kcf => $vcf)
                                    <th>Cantidad de {{ $vcf }}</th>
                                @endforeach
                                <th>Sub-Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @php($total_iniciado=null)
                        @php($total_piniciar=null)
                        @foreach($atencion_inicial->groupBy("mes") as $kai =>$vai)
                            <tr>
                                <td>{{ $mes[-1+$kai] }}</td>
                                @foreach($criterio_fijo_proceso as $kcf => $vcf)
                                    <td>{{ $total_iniciado[$kcf][]=$vai->where("tipo_proceso",$kcf)->sum("cantidad")  }}</td>
                                @endforeach
                                <td>{{ $total[]=$vai->sum("cantidad")  }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            @foreach($criterio_fijo_proceso as $kcf => $vcf)
                                @php($total_c = array_sum($total_iniciado[$kcf]))
                                @php($data_pie["data"][] = ["name"=>$vcf, "y"=>$total_c])
                                <td>{{  $total_c }}</td>
                            @endforeach
                            <td>{{ array_sum($total) }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_por_proceso" class="chart">
                        Area g_proceso
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
@php($js_data_chart_proceso =\App\Models\asesoria::g_pie($data_pie,$titulo_reporte,$subtitulo,$subtitulo))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_por_proceso').highcharts(
                    {!! $js_data_chart_proceso !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_proceso').DataTable(json_data_table);
        } );
    </script>
@endpush
