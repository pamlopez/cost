@php($titulo_reporte = "Resultados del año  $array_rango_anio[1]")
@php($data_x = [])
@php($data_y = [])
<div class="box box-primary">
    <div class="box-header">
        <h4 class="pull-left"><b>1.2 Citas por mes del año {{ $array_rango_anio[1] }}</b></h4>
        <br>
        <h3 align="center" style="color: #4a148c;">
            <b>&nbsp;&nbsp;&nbsp; {{ $titulo_reporte }}
            </b>
        </h3>
    </div>

    <div class="box-body">
        <div class="box">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="box-body table-responsive no-padding">
                    <br>
                    <table id="tabla_mes" class="table table-hover table-bordered ">
                        <thead>
                            <tr class="tbl-encabezados">
                                <th colspan="{{ count($rango_anios)+2 }}">
                                    <h4 class="text-center">
                                        <b>Año {{ $array_rango_anio[1] }} de H.V.</b>
                                    </h4>
                                </th>
                            </tr>
                            <tr class="tbl-encabezados">
                                <th>Mes</th>
                                <th>{{ $array_rango_anio[1] }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php($total=null)
                        @foreach($array_meses as $kam =>$vam)
                            <tr>
                                <td>{{ $mes[-1+$vam] }}</td>
                                @php($sub_total=null)
                                @php($consulta = $consulta_asesoria->where("anio",$array_rango_anio[1])->where("mes",$vam)->first())
                                @php($data_x[] = $mes[-1+$vam])
                                <td>{{ $consulta=$consulta ? $consulta->atendidos : 0  }}</td>
                                @php($data_y[] = $consulta)
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr class="tbl-encabezados">
                            <td>TOTAL</td>
                            @php($consulta2 = $consulta_asesoria->where("anio",$array_rango_anio[1]))
                            <td>{{ $consulta2->sum("atendidos") }}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <br>
                <br>
                <div class="box-body table-responsive no-padding">
                    <div id="g_asesoria_mes" class="chart">
                        Area g_mes
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

@php($js_data_chart_mes = \App\Models\asesoria::g_columna($data_x,$data_y,$titulo_reporte,"Atendidas"))
@push('javascript')
    <script type="text/javascript">
        $(function () {
            $('#g_asesoria_mes').highcharts(
                    {!! $js_data_chart_mes !!}
            )}
        );
        $(document).ready(function() {
            $('#tabla_mes').DataTable(json_data_table);
        } );
    </script>
@endpush
