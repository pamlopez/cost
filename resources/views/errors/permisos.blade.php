
@extends("entorno")

@section("contenido")

   <div class="row">
      <div class="col-md-10 col-md-offset-1">
          <h2 class="page-header">Acceso denegado <small><span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span> privilegios insuficientes </small></h2>
         <div class="alert alert-danger" role="alert">

            <h3><span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> Atención</h3>
            @if(isset($mensaje))
               <p>{{ $mensaje }}</p>
            @endif
         </div>
      </div>
   </div>
@endsection