@if (Session::has("mensaje"))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
        <strong>Informaci&oacute;n</strong><br> {{ Session::get('mensaje') }}.
    </div>
@endif