
@extends("entorno")

@section("contenido")

   <div class="row">
      <div class="col-md-10 col-md-offset-1">
          <h3 class="page-header">Ha ocurrido un error :(</h3>
         <div class="alert alert-danger" role="alert">

            <span class="glyphicon glyphicon-alert" aria-hidden="true"></span>
            @if(isset($mensaje))
               {{ $mensaje }}
            @endif
         </div>
      </div>
   </div>
@endsection