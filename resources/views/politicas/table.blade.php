<table class="table table-responsive" id="politicas-table">
    <thead>
        <tr>
            <th>Titulo</th>
        <th>Descripcion Html</th>
        <th>Estado</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($politicas as $politica)
        <tr>
            <td>{!! $politica->titulo !!}</td>
            <td>{!! $politica->descripcion_html !!}</td>
            <td>{!! $politica->estado !!}</td>
            <td>
                {!! Form::open(['route' => ['politicas.destroy', $politica->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('politicas.show', [$politica->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('politicas.edit', [$politica->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>