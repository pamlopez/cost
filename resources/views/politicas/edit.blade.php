@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Politica
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($politica, ['route' => ['politicas.update', $politica->id], 'method' => 'patch']) !!}

                        @include('politicas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection