<!-- Titulo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('titulo', 'Titulo:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<!-- Descripcion Html Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descripcion_html', 'Politica de Publicación:') !!}
    {!! Form::textarea('descripcion_html', null, ['class' => 'form-control','id' => 'descripcion_html']) !!}
</div>

<script>
    CKEDITOR.replace( 'descripcion_html' );
</script>

<!-- Estado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estado', 'Estado:') !!}
    {!! Form::number('estado', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('politicas.index') !!}" class="btn btn-default">Cancel</a>
</div>
