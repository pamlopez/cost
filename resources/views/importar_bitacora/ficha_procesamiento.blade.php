<div class="box">
    <div class="box-header">
        Procesamiento del archivo
    </div>
    <div class="box-body">
        <h4 class="card-title">
            @if($item->id_estado==1)
                Proceso de importación
            @elseif($item->id_estado==2)
                Interpretación de los datos
            @elseif($item->id_estado==3)
                Resultados de la importación
            @elseif($item->id_estado==99)
                <span class="text-danger">
                                Archivo anulado
                            </span>

            @endif
        </h4>
        @if(  $item->id_estado== 1)
            <p>Debe iniciar el procesamiento del archivo cargado utilizando la opción "Importar datos".</p>
        @elseif(  $item->id_estado== 2)
                <table class="table table-bordered table-sm">
                    <tr>
                        <td>Total de filas encontradas</td>
                        <td class="text-right">
                                    <span id="filas_total">
                                        @if($item->id_estado==1)
                                            Pendiente de importar
                                        @else
                                            {{ $item->conteos->filas_total }}
                                        @endif

                                    </span>
                        </td>
                    </tr>
                    <tr>
                        <td>Filas correctas</td>
                        <td class="text-right"> <span id="filas_exito">{{ $item->conteos->filas_exito }}</span></td>
                    </tr>
                    <tr>
                        <td>Filas con problemas</td>
                        <td class="text-right"> <span id="filas_fallo">{{ $item->conteos->filas_fallo }}</span></td>
                    </tr>
                    <tr>
                        <td>Filas pendientes de procesar</td>
                        <td class="text-right"> <span id="filas_pendiente">{{ $item->conteos->filas_pendiente }}</span></td>
                    </tr>
                    <tr>
                        <td>Bitácora de importación</td>
                        <td class="text-right">
                            <button type="btn btn-info" onclick="$('#div_log_bd').toggle()">Mostrar/Ocultar</button>
                        </td>
                    </tr>

                </table>
        @elseif($item->id_estado == 3)
            <table class="table table-bordered table-sm">

                @foreach($item->conteos_finales as $var=>$val)
                    <tr>
                        <td>{{ $var }}</td>
                        <td class="text-right">{{ number_format($val,0) }}</td>
                    </tr>
                @endforeach

                <tr>
                    <td>Bitácora de errores</td>
                    <td class="text-right">
                        <button class="btn btn-info" onclick="$('#div_log_bd').toggle()">Mostrar/Ocultar</button>
                    </td>
                </tr>

            </table>
            <a class="btn btn-danger" onclick="return confirm('Esta seguro? Se borraran todos los casos y victimas importadas')" href="{{ action("Importar\importar_bitacoraController@anular",$item->id_importar_bitacora) }}">Anular este archivo</a>

                <a class="btn btn-success " href="{{ action("Importar\importar_bitacoraController@index") }}">Regresar </a>


        @endif
        @if($item->id_estado==99)
            <a class="btn btn-success " href="{{ action("Importar\importar_bitacoraController@index") }}">Regresar </a>
            <button class="btn btn-info" onclick="$('#div_log_bd').toggle()">Mostrar/Ocultar bitácora</button>
        @endif

        @if($item->id_estado==2)
            <div class="progress">
                <div class="progress-bar bg-success" id="barra_avance" role="progressbar" style="width: {{ $item->conteos->avance }}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><span id="barra_porcentaje">{{ $item->conteos->avance }}%</span></div>
            </div>
        @endif

    </div>
</div>