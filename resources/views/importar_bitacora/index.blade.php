@extends("layouts.app")

@section('content_header')
    <h1 class="page-header">Listado de importaciones</h1>
@endsection


@section("content")
<div class="box box-info">
    <div class="box-header with-border">
        <a href="{{ action("Importar\importar_bitacoraController@create") }}" class="btn btn-primary">Cargar nuevo archivo</a>
    </div>


    <table class="table table-bordered table-condensed table-sm table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Fecha</th>
            <th>Tipo</th>
            <th>Archivo</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach($listado as $id=>$registro)
            <tr>
                <td> {{ $id+1 }}</td>
                <td> {{ $registro->fecha_hora->format("d-m-Y H:i") }}</td>
                <td> {{ $registro->fmt_id_tipo }}</td>
                <td> {!! $registro->link_archivo !!}</td>
                <td> {{ $registro->fmt_id_estado }}</td>
                <td> <a href=" {{ action("Importar\importar_bitacoraController@show",$registro->id_importar_bitacora) }}" class="btn btn-info">Detalles</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@endsection