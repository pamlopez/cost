@extends("layouts.app")

@section('content_header')
    <h1 class="page-header">Información de la importación</h1>
@endsection



@section("contenido")

    <div class="row">
        <div class="col-sm-6">
           @include("importar_bitacora.ficha")
        </div>
        <div class="col-sm-6">
            @include("importar_bitacora.ficha_procesamiento")
        </div>
    </div>
    <br>

    {{-- DIVS ocultos --}}

    {{-- Botón de continuar que sale al finalizar el proceso de importacion --}}

    <div class="row" id="div_continuar" style="display: none">
        <div class="col-sm-6 offset-md-3 text-center " >
            <div class="box box-success ">
                <div class="box-header ">
                    Confirmación
                </div>
                <div class="box-body" >
                    <h4 class="card-title text-success">Importación completada</h4>
                    <p class="card-text">Favor de revisar la bitácora de importación.  Si todo se encuentra bien, presione el siguiente botón.</p>
                    <p class="text-danger" id="log_error" style="display: none">Advertencia: se reportan filas con errores</p>
                    <button id="btn_exito" onclick="location.reload()" class="btn btn-success">Continuar con el proceso</button>
                    <a onclick="return confirm('Esta seguro? Se borraran todos los casos y victimas importadas')"  class="btn btn-danger" href="{{ action("Importar\importar_bitacoraController@anular",$item->id_importar_bitacora) }}">Anular este archivo</a>
                </div>
            </div>
        </div>
    </div>

    {{-- Botón de continuar que sale al finalizar el proceso de procesamiento --}}
    <div class="row"  id="div_log_bd" style="display: none">

        <div class="col-sm-10 col-sm-offset-1 ">
            <div class="box box-warning ">
                <div class="box-header ">
                    Bitácora general
                </div>
                <div class="box-body">
                    <div  id="log_importa" >
                        <h3 class="text-danger">Errores de Interpretación de los datos</h3>
                        {!! $item->log_procesa  !!}
                    </div>
                    <div   id="log_procesa" >

                        {!! $item->log_importa !!}
                    </div>


                </div>

            </div>


        </div>
    </div>

    {{-- div de WAIT con el relojito que sale  al iniciar cualquier proceso --}}
    <div class="row" id="div_wait" style="display: none">

        <div class="col-sm-6 offset-md-3 text-center" >
            <div class="box box-info ">
                <div class="box-header ">
                    Procesando <span id="time">0:0:0</span>
                </div>
                <div class="box-body" >
                    <h4 class="card-title text-info">Procesando archivo</h4>
                    <img src=" {{ url("wait.gif") }}" border="0">

                    <p class="card-text">Según el tamaño del archivo, este proceso puede tomar hasta 5 minutos.</p>
                </div>
            </div>
        </div>
    </div>

    {{-- Bitácora del procesamiento, incluye la del error --}}
    <div class="row" id="div_bitacora" style="display: none">
        <div class="col-md-10 offset-md-1 " >
            <h3 class="page-header">
                Bitácora
            </h3>
            <div id="div_log_error" class=" ">


            </div>
            <div id="div_bitacora_detalle">

            </div>
        </div>
    </div>







@endsection

@push("javascript")
    <script>
        var avanzar=0;
        function importar() {

            if(avanzar==0) {  //Para no reiniciar en cada recarga
                iniciar();
            }


            $("#div_wait").show();
            $("#div_bitacora").show();
            $("#btn_procesar").hide();

            $.ajax({
                method: "GET",
                dataType: "json",
                url: "{{ action('Importar\importar_bitacoraController@procesar',$item->id_importar_bitacora) }}"

            })
                .done(function( respuesta ) {

                        //console.log(respuesta);
                    actualiza_log_procesa(respuesta);

                    if(respuesta.filas_pendiente > 0) {
                        //console.log("otra carga");
                        importar();
                    }
                    else {
                        $("#time").text($("#time").text() + ' Finalizado. ');
                        //console.log("fin");
                        $("#div_wait").hide();
                        $("#div_continuar").show();
                    }
                });
        }

        function actualiza_log_procesa(datos) {
            //console.log('antes de actualizar');
            $("#div_bitacora_detalle").append(datos.log);
            $("#div_log_error").append(datos.log_error);
            $("#filas_total").text(datos.filas_total);
            $("#filas_exito").text(datos.filas_exito);
            $("#filas_fallo").text(datos.filas_fallo);
            $("#filas_pendiente").text(datos.filas_pendiente);
            if(datos.filas_fallo>0) {
                $('#log_error').show();
                $('#div_log_error').show();
            }
            //Ocultar botón de continuar
            if(!datos.resultado) {
                $('#log_error').show();
                $('#btn_exito').hide();
            }
            if(datos.filas_total > 0) {
                var avance= (datos.filas_total - datos.filas_pendiente) /datos.filas_total;
                avance=avance*100;
                $("#barra_avance").attr('aria-valuenow', avance+'%' ).css('width',avance+'%');
                $("#barra_porcentaje").text(parseInt(avance)+'%');
            }


        }




    </script>
@endpush

@push("javascript")

    @include("importar_bitacora.js_reloj")

@endpush