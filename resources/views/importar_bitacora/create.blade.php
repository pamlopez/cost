@extends("layouts.app")
@section('content_header')
    <h1 class="page-header">Nueva importación</h1>
@endsection

@section("contenido")



<div class="row">
    <div class="col-sm-6">
        <div class="box box-info ">
            <div class="box-header with-border">
                Adjuntar archivo
            </div>
            <div class="box-body">
                {!! Form::open(['action'=> ['Importar\importar_bitacoraController@store'],'files'=>'true', "id"=>"demo-upload"]) !!}


                <div class="form-group">
                    {!! Form::label('id_tipo', 'Tipo de archivo a cargar') !!}
                    {!! Form::select('id_tipo', \App\Importar\importar_bitacora::a_tipos() ,null,["class"=>"form-control"])!!}
                </div>
                <div class="input-group ">
                    <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Escoger archivo&hellip; <input type="file" required="required" name='file' style="display: none;" >
                            </span>
                    </label>
                    <input type="text" class="form-control" readonly>
                </div>

                <div class="text-center">
                    <br>
                    <button type="submit" class="btn btn-success" id="submit"><span class="glyphicon glyphicon-import"  aria-hidden="true"></span> Subir archivo al servidor</button>
                </div>
                {!! Form::close()  !!}
            </div>
        </div>

    </div>
</div>


@endsection




    @push('javascript')
        <script>
            $(function() {

                // We can attach the `fileselect` event to all file inputs on the page
                $(document).on('change', ':file', function() {
                    var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                });

                // We can watch for our custom `fileselect` event like this
                $(document).ready( function() {
                    $(':file').on('fileselect', function(event, numFiles, label) {

                        var input = $(this).parents('.input-group').find(':text'),
                            log = numFiles > 1 ? numFiles + ' files selected' : label;

                        if( input.length ) {
                            input.val(log);
                        } else {
                            if( log ) alert(log);
                        }

                    });
                });

            });
        </script>
    @endpush





