<div class="box">
    <div class="box-header">
        Información del archivo
    </div>
    <div class="box-body">
        <h4 class="card-title">Datos generales</h4>
        <ul>
            <li>Tipo de datos: <b>{{ $item->fmt_id_tipo }} </b></li>
            <li>Situación actual: {{ $item->fmt_id_estado }} </li>
            <li>Fecha de carga:  {{ $item->fmt_fecha_hora }}</li>
            <li>Archivo: {!! $item->link_archivo  !!} </li>
        </ul>
        @if($item->id_estado == 1)
            <span class="pull-right">
                            <button id="btn_procesar" onclick="importar({{$item->id_tipo}})"  class="btn btn-primary ">
                                Importar datos
                            </button>
                        </span>
        @elseif($item->id_estado == 2)
            <button id="btn_procesar" onclick="importar({{$item->id_tipo}})"  class="btn btn-primary ">
                Procesar los datos
            </button>
        @endif


    </div>
</div>