<table class="table table-responsive" id="costInfoContratos-table">
    <thead>
        <tr>
            <th>Id Proyecto</th>
        <th>Tipo</th>
        <th>Estado Contrado</th>
        <th>Plazo Contrato</th>
        <th>Tipo Numero Contrato</th>
        <th>Fecha Inicio</th>
        <th>Empresa Constructora</th>
        <th>Objeto Contrato</th>
        <th>No Precalificado</th>
        <th>Fecha Actualizacion</th>
        <th>Precio Contrato</th>
        <th>Fecha Contrato</th>
        <th>Aprobacion Contrato</th>
        <th>Fecha Aprobacion Contrato</th>
        <th>Nombre Firma</th>
        <th>Cargo Firma</th>
        <th>Nombramiento</th>
        <th>Fecha Nombramiento</th>
        <th>Conclusion</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($costInfoContratos as $costInfoContrato)
        <tr>
            <td>{!! $costInfoContrato->id_proyecto !!}</td>
            <td>{!! $costInfoContrato->tipo !!}</td>
            <td>{!! $costInfoContrato->estado_contrado !!}</td>
            <td>{!! $costInfoContrato->plazo_contrato !!}</td>
            <td>{!! $costInfoContrato->tipo_numero_contrato !!}</td>
            <td>{!! $costInfoContrato->fecha_inicio !!}</td>
            <td>{!! $costInfoContrato->empresa_constructora !!}</td>
            <td>{!! $costInfoContrato->objeto_contrato !!}</td>
            <td>{!! $costInfoContrato->no_precalificado !!}</td>
            <td>{!! $costInfoContrato->fecha_actualizacion !!}</td>
            <td>{!! $costInfoContrato->precio_contrato !!}</td>
            <td>{!! $costInfoContrato->fecha_contrato !!}</td>
            <td>{!! $costInfoContrato->aprobacion_contrato !!}</td>
            <td>{!! $costInfoContrato->fecha_aprobacion_contrato !!}</td>
            <td>{!! $costInfoContrato->nombre_firma !!}</td>
            <td>{!! $costInfoContrato->cargo_firma !!}</td>
            <td>{!! $costInfoContrato->nombramiento !!}</td>
            <td>{!! $costInfoContrato->fecha_nombramiento !!}</td>
            <td>{!! $costInfoContrato->conclusion !!}</td>
            <td>
                {!! Form::open(['route' => ['costInfoContratos.destroy', $costInfoContrato->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costInfoContratos.show', [$costInfoContrato->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('costInfoContratos.edit', [$costInfoContrato->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>