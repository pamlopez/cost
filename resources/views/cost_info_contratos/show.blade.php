@extends('layouts.app')
@section('content')
    @include('flash::message')
    <section class="content-header">
        <h1>
            Información del Contrato
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_info_contratos.show_fields')
                </div>
                <div class="text-left">
                    <a href="{!! route('costProyectos.show', [$costInfoContrato->id_proyecto,'a'=>1]) !!}"
                       class="btn btn-default">Atrás</a>
                </div>
            </div>
        </div>
    </div>
@endsection
