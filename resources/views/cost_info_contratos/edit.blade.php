@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cost Info Contrato
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costInfoContrato, ['route' => ['costInfoContratos.update', $costInfoContrato->id], 'method' => 'patch']) !!}

                        @include('cost_info_contratos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection