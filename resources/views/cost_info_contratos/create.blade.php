@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
           Nueva Información del Contrato
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'costInfoContratos.store']) !!}

                        @include('cost_info_contratos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
