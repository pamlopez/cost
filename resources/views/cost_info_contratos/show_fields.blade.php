<div class="row">
    <div class="form-group col-sm-6 ">
        {!! Form::label('modalidad_contratacion', 'Modalidad de Contratación:') !!}
        <p>{!! $costInfoContrato->fmt_modalidad_contratacion !!}</p>
    </div>


    <!-- Oferentes Field -->
    <div class="form-group col-sm-6 ">
        {!! Form::label('bases_concurso_link', 'Link de las bases del concurso:') !!}
        <p>{!! $costInfoContrato->bases_concurso_link !!}</p>
    </div>

    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('bases_concurso', 'Bases del concurso:') !!}
        <p>{!! $costInfoContrato->bases_concurso !!}</p>
    </div>

    <!-- Fecha Aprobacion Field -->
    <div class="form-group col-sm-6">
        <i class="fa fa-calendar"></i>
        {!! Form::label('fecha_publicacion', 'Fecha de la publicación del contrato:') !!}
        <p>{!! $costInfoContrato->fmt_fecha_publicacion !!}</p>

    </div>

    <!-- Fecha Aprobacion Field -->
    <div class="form-group col-sm-6">
        <i class="fa fa-calendar"></i>
        {!! Form::label('fecha_ultima_mod_gc', 'Fecha de la  última publicación en Guatecompras:') !!}
        <p>{!! $costInfoContrato->fmt_fecha_ultima_mod_gc !!}</p>
    </div>
</div>


<div class="form-group col-md-12">
    <hr>
</div>
<div class="row">
    <!-- Oferentes Field -->
    <div class="form-group col-sm-6 ">
        {!! Form::label('total_inconformidades', 'No. de Inconformidades presentadas:') !!}
        <p>{!! $costInfoContrato->total_inconformidades !!}</p>
    </div>

    <!-- Oferentes Field -->
    <div class="form-group col-sm-6 ">
        {!! Form::label('total_inconformidades_r', 'Inconformidades resueltas y comunicadas a todos los oferentes:') !!}
        <p>{!! $costInfoContrato->total_inconformidades_r !!}</p>
    </div>
</div>

<div class="form-group col-md-12">
    <hr>
</div>
<div class="row">
    <div class="form-group col-sm-4 ">
        {!! Form::label('alcance', 'Alcance del contrato:') !!}
        <p>{!! $costInfoContrato->alcance !!}</p>

    </div>
    <div class="form-group col-sm-4 ">
        {!! Form::label('objeto_contrato', 'Objeto del contrato:') !!}
        <p>{!! $costInfoContrato->objeto_contrato !!}</p>

    </div>
    <div class="form-group col-sm-4">
        {!! Form::label('programa', 'Programa de trabajo de ejecución:') !!}
        <p>{!! $costInfoContrato->programa !!}</p>
    </div>
</div>

<div class="form-group col-md-12">
    <hr>
</div>
<div class="row">
    <!-- Oferentes Field -->
    <div class="form-group col-sm-3 ">
        {!! Form::label('nombre_responsable_seleccionado', 'Nombre del responsable de diseño seleccionado:') !!}
        <p>{!! $costInfoContrato->nombre_responsable_seleccionado !!}</p>
    </div>

    <!-- Precio Contrato Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('precio_contrato', 'Monto del contrato:') !!}
        <p>{!! $costInfoContrato->precio_contrato !!}</p>
    </div>

    <!-- Plazo Contrato Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('plazo_contrato', 'Plazo del contrato:') !!}
        <p>{!! $costInfoContrato->plazo_contrato !!}</p>
    </div>

    <!-- Tipo Numero Contrato Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('tipo_numero_contrato', 'Tipo y número de contrato:') !!}
        <p>{!! $costInfoContrato->tipo_numero_contrato !!}</p>
    </div>
</div>
<div class="row">

    <!-- Fecha Aprobacion Field -->
    <div class="form-group col-sm-3">
        <i class="fa fa-calendar"></i>
        {!! Form::label('fecha_inicio', 'Fecha de inicio del contrato') !!}
        <p>{!! $costInfoContrato->fmt_fecha_inicio !!}</p>
    </div>

    <!-- Nombre Firma Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('nombre_firma', 'Responsable del contrato del  estudio de factibilidad enla unidad ejecutora:') !!}
        <p>{!! $costInfoContrato->nombre_firma !!}</p>
    </div>

    <!-- Cargo Firma Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('cargo_firma', 'Cargo del responsable contrato del estudio de factibilidad enla unidad ejecutora:') !!}
        <p>{!! $costInfoContrato->cargo_firma !!}</p>
    </div>
    <!-- Fecha Aprobacion Field -->
    <div class="form-group col-sm-3">
        <i class="fa fa-calendar"></i>
        {!! Form::label('fecha_aprobacion', 'Fecha aprobación del contrato:') !!}
        <p>{!! $costInfoContrato->fmt_fecha_aprobacion_contrato !!}</p>
    </div>
    <div class="form-group col-sm-12">
        {!! Form::label('nombres_suscriben', 'Nombres y cargos de quienes suscriben el contrato:') !!}
        <p>{!! $costInfoContrato->nombres_suscriben !!}</p>
    </div>
</div>

<div class="form-group col-sm-12">
    <hr>
</div>

<div class="row">
    <!-- Empresa Constructora Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('empresa_constructora', 'Empresa constructora seleccionada:') !!}
        <p>{!! $costInfoContrato->empresa_constructora !!}</p>
    </div>

    <!-- Aprobacion Contrato Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('aprobacion_contrato', 'No. de aprobación del contrato:') !!}
        <p>{!! $costInfoContrato->aprobacion_contrato !!}</p>
    </div>
    <!-- Nombramiento Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('nombramiento', 'Nombramiento:') !!}
        <p>{!! $costInfoContrato->nombramiento !!}</p>
    </div>

    <!-- Fecha Aprobacion Field -->
    <div class="form-group col-sm-3">
        <i class="fa fa-calendar"></i>
        {!! Form::label('fecha_nombramiento', 'Fecha del nombramiento:') !!}
        <p>{!! $costInfoContrato->fmt_fecha_nombramiento !!}</p>
    </div>
</div>
<div class="row">
    <!-- Fecha Contrato Field -->

    <div class="form-group col-sm-3">
        <i class="fa fa-calendar"></i>
        {!! Form::label('fecha_contrato', 'Fecha del contrato') !!}
        <p>{!! $costInfoContrato->fmt_fecha_contrato !!}</p>

    </div>

    <!-- No Precalificado Field -->
    <div class="form-group col-sm-3">
        {!! Form::label('no_precalificado', 'No. de precalificado:') !!}
        <p>{!! $costInfoContrato->no_precalificado !!}</p>
    </div>


    <div class="form-group col-sm-3">
        {!! Form::label('clausula_sobrecosto', 'Cláusula de sobrecostos:') !!}
        <p>{!! $costInfoContrato->clausula_sobrecosto !!}</p>
    </div>
    <div class="form-group col-sm-3">
        <i class="fa fa-calendar"></i>
        {!! Form::label('fecha_inicio_ejecucion', 'Fecha de inicio de la ejecución del contrato') !!}
        <p>{!! $costInfoContrato->fmt_fecha_inicio_ejecucion !!}</p>

    </div>
</div>
<!-- No Precalificado Field -->

<div class="row">

    <div class="form-group col-sm-3">
        {!! Form::label('estado_contrato', 'Estado del contrado:') !!}
        <p>{!! $costInfoContrato->fmt_estado_contrato !!}</p>
    </div>
    <!-- Objeto Contrato Field -->

    <div class="form-group col-sm-3">
        <i class="fa fa-calendar"></i>
        {!! Form::label('fecha_autoriza_bitacora', 'Fecha de autorización de bitácora y libro') !!}
        <p>{!! $costInfoContrato->fmt_fecha_autoriza_bitacora !!}</p>

    </div>


    <div class="form-group col-sm-3">
        {!! Form::label('quien_autoriza_bitacora', 'Bitacora aprobada por:') !!}
        <p>{!! $costInfoContrato->quien_autoriza_bitacora !!}</p>
    </div>
</div>
<div class="row">
    <!-- Conclusion Field -->
    <div class="form-group col-sm-12 col-lg-6">
        {!! Form::label('conclusion', 'Conclusion:') !!}
        <p>{!! $costInfoContrato->conclusion !!}</p>

    </div>
</div>