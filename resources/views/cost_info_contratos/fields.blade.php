{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costInfoContrato->id_proyecto, ['class' => 'form-control']) !!}
{!! Form::hidden('tipo',isset($tipo)?$tipo:$costInfoContrato->tipo, ['class' => 'form-control']) !!}
{!! Form::hidden('id_fase',isset($id_fase)?$id_fase:$costInfoContrato->id_fase, ['class' => 'form-control']) !!}
@php($id_fase_formulario = isset($id_fase)?$id_fase:$costInfoContrato->id_fase)

@include('chivos.catalogo', ['chivo_control' => 'modalidad_contratacion'
                           ,'chivo_id_cat'=>72
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Modalidad de Contratación:'])


<!-- Oferentes Field -->
<div class="form-group col-sm-6 ">
    {!! Form::label('bases_concurso_link', 'Link de las bases del concurso:') !!}
    {!! Form::text('bases_concurso_link', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('bases_concurso', 'Bases del concurso:') !!}
    {!! Form::textarea('bases_concurso', null, ['class' => 'form-control','rows' => 3]) !!}
</div>

<!-- Fecha Aprobacion Field -->
<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_publicacion', 'Fecha de la publicación del contrato:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_publicacion', null, ['class' => 'form-control datepicker','data-value'=>$costInfoContrato->fecha_publicacion]) !!}
    @else
        {!! Form::text('fecha_publicacion', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>

<!-- Fecha Aprobacion Field -->
<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_ultima_mod_gc', 'Fecha de la  última publicación en Guatecompras:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_ultima_mod_gc', null, ['class' => 'form-control datepicker','data-value'=>$costInfoContrato->fecha_ultima_mod_gc]) !!}
    @else
        {!! Form::text('fecha_ultima_mod_gc', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>
<div class="form-group col-md-12">
    <hr>
</div>
<!-- Oferentes Field -->
<div class="form-group col-sm-6 ">
    {!! Form::label('total_inconformidades', 'No. de Inconformidades presentadas:') !!}
    {!! Form::number('total_inconformidades', null, ['class' => 'form-control']) !!}
</div>

<!-- Oferentes Field -->
<div class="form-group col-sm-6 ">
    {!! Form::label('total_inconformidades_r', 'Inconformidades resueltas y comunicadas a todos los oferentes:') !!}
    {!! Form::number('total_inconformidades_r', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-md-12">
    <hr>
</div>

<div class="form-group col-md-12">
    <hr>
</div>


<div class="form-group col-sm-6 ">
    {!! Form::label('alcance', 'Alcance u objeto del contrato de ejecución:') !!}
    {!! Form::text('alcance', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6 ">
    {!! Form::label('programa', 'Programa de trabajo de ejecución:') !!}
    {!! Form::text('programa', null, ['class' => 'form-control']) !!}
</div>


<!-- Oferentes Field -->
<div class="form-group col-sm-6 ">
    @if($id_fase == 6 )
        {!! Form::label('nombre_responsable_seleccionado', 'Nombre del responsable de supervisión seleccionado:') !!}
    @else
        {!! Form::label('nombre_responsable_seleccionado', 'Nombre del responsable de diseño seleccionado:') !!}
    @endif
    {!! Form::text('nombre_responsable_seleccionado', null, ['class' => 'form-control']) !!}
</div>

<!-- Empresa Constructora Field -->
<div class="form-group col-sm-6">
    {!! Form::label('empresa_constructora', 'Empresa constructora seleccionada:') !!}
    {!! Form::text('empresa_constructora', null, ['class' => 'form-control']) !!}
</div>

<!-- Plazo Contrato Field -->
<div class="form-group col-sm-4">
    {!! Form::label('plazo_contrato', 'Plazo del contrato:') !!}
    {!! Form::text('plazo_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Precio Contrato Field -->
<div class="form-group col-sm-4">
    {!! Form::label('precio_contrato', 'Monto del contrato:') !!}
    {!! Form::text('precio_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Numero Contrato Field -->
<div class="form-group col-sm-4">
    {!! Form::label('tipo_numero_contrato', 'Tipo y número de contrato:') !!}
    {!! Form::text('tipo_numero_contrato', null, ['class' => 'form-control']) !!}
</div>


<!-- Objeto Contrato Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('nombres_suscriben', 'Nombres y cargos de quienes suscriben el contrato:') !!}
    {!! Form::textarea('nombres_suscriben', null, ['class' => 'form-control','rows' => 3]) !!}
</div>
<!-- Objeto Contrato Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('objeto_contrato', 'Objeto del contrato:') !!}
    {!! Form::textarea('objeto_contrato', null, ['class' => 'form-control','rows' => 3]) !!}
</div>


<!-- Fecha Aprobacion Field -->
<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_aprobacion', 'Fecha aprobación del contrato:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_aprobacion_contrato', null, ['class' => 'form-control datepicker','data-value'=>$costInfoContrato->fecha_aprobacion_contrato]) !!}
    @else
        {!! Form::text('fecha_aprobacion_contrato', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>

<!-- Aprobacion Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aprobacion_contrato', 'No. de aprobación del contrato:') !!}
    {!! Form::text('aprobacion_contrato', null, ['class' => 'form-control']) !!}
</div>
<!-- Nombramiento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombramiento', 'Nombramiento:') !!}
    {!! Form::text('nombramiento', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Aprobacion Field -->
<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_nombramiento', 'Fecha del nombramiento:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_nombramiento', null, ['class' => 'form-control datepicker','data-value'=>$costInfoContrato->fecha_nombramiento]) !!}
    @else
        {!! Form::text('fecha_nombramiento', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>
<!-- Nombre Firma Field -->
<div class="form-group col-sm-6">
    @if($id_fase == 6 )
        {!! Form::label('nombre_firma', 'Responsable del contrato en la EA:') !!}
    @else
        {!! Form::label('nombre_firma', 'Responsable del contrato del diseño y estudio de factibilidad en la unidad ejecutora :') !!}
    @endif
    {!! Form::text('nombre_firma', null, ['class' => 'form-control']) !!}
</div>

<!-- Cargo Firma Field -->
<div class="form-group col-sm-6">
    @if($id_fase == 6 )
        {!! Form::label('cargo_firma', 'Cargo del responsable del contrato en la EA:') !!}
    @else
        {!! Form::label('cargo_firma', 'Cargo del responsable del contrato del diseño y estudio de factibilidad en la unidad ejecutora:') !!}
    @endif
    {!! Form::text('cargo_firma', null, ['class' => 'form-control']) !!}
</div>

<!-- Fecha Aprobacion Field -->
<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_inicio', 'Fecha de inicio del contrato') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_inicio', null, ['class' => 'form-control datepicker','data-value'=>$costInfoContrato->fecha_inicio]) !!}
    @else
        {!! Form::text('fecha_inicio', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>
<!-- Fecha Contrato Field -->

<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_contrato', 'Fecha del contrato') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_contrato', null, ['class' => 'form-control datepicker','data-value'=>$costInfoContrato->fecha_contrato]) !!}
    @else
        {!! Form::text('fecha_contrato', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>
<!-- No Precalificado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('clausula_sobrecosto', 'Cláusula de sobrecostos:') !!}
    {!! Form::text('clausula_sobrecosto', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_inicio_ejecucion', 'Fecha de inicio de la ejecución del contrato') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_inicio_ejecucion', null, ['class' => 'form-control datepicker','data-value'=>$costInfoContrato->fecha_inicio_ejecucion]) !!}
    @else
        {!! Form::text('fecha_inicio_ejecucion', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>


@include('chivos.catalogo', ['chivo_control' => 'estado_contrato'
                           ,'chivo_id_cat'=>67
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Estado del contrado:'])


<div class="form-group col-sm-6">
    {!! Form::label('estado_contrato_oc', 'Estado actual de proyecto (OC4IDS):') !!}
    {!! Form::select('estado_contrato_oc', \App\Models\cat_item::listar_catalogo_traduccion(86),$costInfoContrato->estado_oc,['class' => 'form-control','style'=>'width:100%','id'=>'estado_contrato_oc']) !!}
</div>

<!-- No Precalificado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_precalificado', 'No. de precalificado:') !!}
    {!! Form::number('no_precalificado', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_autoriza_bitacora', 'Fecha de autorización de bitácora y libro de actas de obra:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_autoriza_bitacora', null, ['class' => 'form-control datepicker','data-value'=>$costInfoContrato->fecha_autoriza_bitacora]) !!}
    @else
        {!! Form::text('fecha_autoriza_bitacora', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>

<div class="form-group col-sm-6">
    {!! Form::label('quien_autoriza_bitacora', 'Bitacora aprobada por:') !!}
    {!! Form::text('quien_autoriza_bitacora', null, ['class' => 'form-control']) !!}
</div>

@if($id_fase_formulario == 3)
    <div class="form-group col-sm-6">
        {!! Form::label('metodo_procuracion', 'Método de procuración (OC4IDS):') !!}
        {!! Form::text('metodo_procuracion', null, ['class' => 'form-control']) !!}
    </div>
    @include('chivos.catalogo', ['chivo_control' => 'costo_estimado_moneda'
                       ,'chivo_id_cat'=>83
                       , 'chivo_default'=>null
                       ,'chivo_tamanio'=>'col-md-3'
                       ,'chivo_texto'=>'Tipo Moneda (OC4IDS):'])

    <div class="form-group col-sm-3">
        {!! Form::label('costo_estimado', 'Costo estimado del proyecto (OC4IDS):') !!}
        {!! Form::text('costo_estimado', null, ['class' => 'form-control']) !!}
    </div>

    @include('chivos.catalogo', ['chivo_control' => 'moneda_final'
                       ,'chivo_id_cat'=>83
                       , 'chivo_default'=>null
                       ,'chivo_tamanio'=>'col-md-3'
                       ,'chivo_texto'=>'Tipo Moneda (OC4IDS):'])

    <div class="form-group col-sm-3 col-lg-3">
        {!! Form::label('precio_contrato_final', 'Precio contrato final (OC4IDS):') !!}
        {!! Form::text('precio_contrato_final', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-sm-4">
        <i class="fa fa-calendar"></i>
        {!! Form::label('fecha_inicio_ejecucion', 'Fecha de inicio ejecución (OC4IDS):') !!}
        @if($bnd_edit)
            {!! Form::text('fecha_inicio_ejecucion', null, ['class' => 'form-control datepicker','data-value'=>$costInfoContrato->fecha_inicio_ejecucion]) !!}
        @else
            {!! Form::text('fecha_inicio_ejecucion', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
        @endif
    </div>
    <div class="form-group col-sm-4">
        <i class="fa fa-calendar"></i>
        {!! Form::label('fecha_fin_ejecucion', 'Fecha de fin ejecución (OC4IDS):') !!}
        @if($bnd_edit)
            {!! Form::text('fecha_fin_ejecucion', null, ['class' => 'form-control datepicker','data-value'=>$costInfoContrato->fecha_fin_ejecucion]) !!}
        @else
            {!! Form::text('fecha_fin_ejecucion', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
        @endif
    </div>


@endif
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('conclusion', 'Conclusion:') !!}
    {!! Form::textarea('conclusion', null, ['class' => 'form-control','rows' => 3]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    @if(isset($id_proyecto))

        <a href="{!! route('costProyectos.show', [$id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costProyectos.show', [$costInfoContrato->id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif
</div>

@push('javascript')
    <script>
        $('#estado_contrato_oc').select2({
            placeholder: 'Seleccione'
        });
    </script>
@endpush
