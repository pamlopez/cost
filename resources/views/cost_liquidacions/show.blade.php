@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cost Liquidacion
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_liquidacions.show_fields')
                </div>
                <div class="text-left">
                    <a href="{!! route('costProyectos.show', [$costLiquidacion->id_proyecto,'a'=>1]) !!}"
                       class="btn btn-default">Atrás</a>
                </div>
            </div>
        </div>
    </div>
@endsection
