
<!-- Informe Avances Field -->
<div class="form-group col-sm-12">
    {!! Form::label('informe_avances', 'Informes de avances del proyecto:') !!}
    <p>{!! $costLiquidacion->informe_avances !!}</p>
</div>

<!-- Planos Finales Field -->
<div class="form-group col-sm-12">
    {!! Form::label('planos_finales', 'Planos finales completos con respaldo de cambios:') !!}
    <p>{!! $costLiquidacion->planos_finales !!}</p>
</div>

<!-- Reporte Evaluacion Field -->
<div class="form-group col-sm-12">
    {!! Form::label('reporte_evaluacion', 'Reporte de evaluaciones y auditorías realizadas al proyecto:') !!}
    <p>{!! $costLiquidacion->reporte_evaluacion !!}</p>
</div>

<!-- Informe Terminacion Field -->
<div class="form-group col-sm-12">
    {!! Form::label('informe_terminacion', 'Informe de terminación del proyecto:') !!}
    <p>{!! $costLiquidacion->informe_terminacion !!}</p>
</div>

<!-- Informe Evaluacion Field -->
<div class="form-group col-sm-12">
    {!! Form::label('informe_evaluacion', 'Informe de evaluación del proyecto:') !!}
    <p>{!! $costLiquidacion->informe_evaluacion !!}</p>
</div>

