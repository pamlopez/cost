{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costLiquidacion->id_proyecto, ['class' => 'form-control']) !!}

<!-- Informe Avances Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('informe_avances', 'Informes de avances del proyecto:') !!}
    {!! Form::textarea('informe_avances', null, ['class' => 'form-control']) !!}
</div>

<!-- Planos Finales Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('planos_finales', 'Planos finales completos con respaldo de cambios:') !!}
    {!! Form::textarea('planos_finales', null, ['class' => 'form-control']) !!}
</div>

<!-- Reporte Evaluacion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('reporte_evaluacion', 'Reporte de evaluaciones y auditorías realizadas al proyecto:') !!}
    {!! Form::textarea('reporte_evaluacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Informe Terminacion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('informe_terminacion', 'Informe de terminación del proyecto:') !!}
    {!! Form::textarea('informe_terminacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Informe Evaluacion Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('informe_evaluacion', 'Informe de evaluación del proyecto:') !!}
    {!! Form::textarea('informe_evaluacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    @if(isset($id_proyecto))

        <a href="{!! route('costProyectos.show', [$id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costProyectos.show', [$costLiquidacion->id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif

</div>
