<table class="table table-responsive" id="costLiquidacions-table">
    <thead>
        <tr>
            <th>Id Proyecto</th>
        <th>Informe Avances</th>
        <th>Planos Finales</th>
        <th>Reporte Evaluacion</th>
        <th>Informe Terminacion</th>
        <th>Informe Evaluacion</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($costLiquidacions as $costLiquidacion)
        <tr>
            <td>{!! $costLiquidacion->id_proyecto !!}</td>
            <td>{!! $costLiquidacion->informe_avances !!}</td>
            <td>{!! $costLiquidacion->planos_finales !!}</td>
            <td>{!! $costLiquidacion->reporte_evaluacion !!}</td>
            <td>{!! $costLiquidacion->informe_terminacion !!}</td>
            <td>{!! $costLiquidacion->informe_evaluacion !!}</td>
            <td>
                {!! Form::open(['route' => ['costLiquidacions.destroy', $costLiquidacion->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costLiquidacions.show', [$costLiquidacion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('costLiquidacions.edit', [$costLiquidacion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>