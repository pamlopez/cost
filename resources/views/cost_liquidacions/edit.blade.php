@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
           Editar información de liquidación
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costLiquidacion, ['route' => ['costLiquidacions.update', $costLiquidacion->id], 'method' => 'patch']) !!}

                        @include('cost_liquidacions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection