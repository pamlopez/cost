<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>COST Guatemala - Datos Abiertos</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">
    <link rel="stylesheet" href="{{ url('css/pace-theme-center-circle.css') }}">
    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.css') }}">
    <link rel="stylesheet" href=" {{ url('css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/select2.min.css') }}">
    {{-- date picker --}}
    <link rel="stylesheet" href="{{ url('js/themes/default.css') }}">
    <link rel="stylesheet" href="{{ url('js/themes/default.date.css') }}">
    <link rel="stylesheet" href="{{ url('js/themes/default.time.css') }}">
    {{-- Ionicons --}}
    <link rel="stylesheet" href="{{ url('css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ url('css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/buttons.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/personalizado.css') }}">
    <link rel="stylesheet" href="{{ url('css/caimu.css') }}">
    <link rel="stylesheet" href="{{ url('css/ol.css') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
    <script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
    @yield('css')
    @stack('styles')
    @stack('head')
    <style>
        .card {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 300px;
            min-height: 400px;
            background: #fff;
            box-shadow: 0 20px 50px rgba(0, 0, 0, .1);
            border-radius: 10px;
            transition: 0.5s;
        }

        .card:hover {
            box-shadow: 0 30px 70px rgba(0, 0, 0, .2);
        }

        .card .box {
            position: absolute;
            top: 50%;
            left: 0;
            transform: translateY(-50%);
            text-align: center;
            padding: 20px;
            box-sizing: border-box;
            width: 100%;
        }

        .card .box .img {
            width: 120px;
            height: 120px;
            margin: 0 auto;
            border-radius: 50%;
            overflow: hidden;
        }

        .card .box .img img {
            width: 100%;
            height: 100%;
        }

        .card .box h2 {
            font-size: 20px;
            color: #262626;
            margin: 20px auto;
        }

        .card .box h2 span {
            font-size: 14px;
            background: #e91e63;
            color: #fff;
            display: inline-block;
            padding: 4px 10px;
            border-radius: 15px;
        }

        .card .box p {
            color: #262626;
        }

        .card .box span {
            display: inline-flex;
        }

        .card .box ul {
            margin: 0;
            padding: 0;
        }

        .card .box ul li {
            list-style: none;
            float: left;
        }

        .card .box ul li a {
            display: block;
            color: #aaa;
            margin: 0 10px;
            font-size: 20px;
            transition: 0.5s;
            text-align: center;
        }

        .card .box ul li:hover a {
            color: #e91e63;
            transform: rotateY(360deg);
        }

        .btn_login {
            background-color: #CF1F46;
            color: #FFFFFF;
        }
    </style>
</head>

@if(Auth::check() ==true)
    @php($collapse = '')
@else
    @php($collapse = 'sidebar-collapse')
@endif
<body class="skin-black-light layout-top-nav">

<div class="container-fluid div">
    <div class="row" style=" height: 50px">
        <div class="col-md-12">

        </div>
    </div>
    <div class="row no-print">
        <div class="col-md-3 col-md-offset-1">
            <a href="http://costguatemala.org/" class="logo" target="_blank">
                <img src="{{ asset('images/cost.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-2 col-md-offset-5 text-right">
            <a href="http://hivos.org.gt/" class="logo" target="_blank">
                <img src="{{ asset('images/hivos_logo.gif') }}" alt="" width="128px" height="74px">
            </a>
        </div>
    </div>
    <div class="row print-only">
        <div class="col-sm-12">

            <img src="{{ asset('images/cost.jpg') }}" alt="" width="150px" height="74px">


            <img src="{{ asset('images/hivos_logo.gif') }}" alt="" width="128px" height="74px" align="right">

        </div>

    </div>
    <div class="row" style="height: 40px">
        <div class="col-md-3 col-md-offset-1 text-center">
            <a href="https://www.facebook.com/CostGuatemala/" class="logo no-print" target="_blank">
                <img src="{{ asset('images/logo_face.png') }}" alt="" width="32px" height="32px">
            </a>
            <a href="https://twitter.com/CostGuate" class="logo no-print" target="_blank">
                <img src="{{ asset('images/logo_twitter.png') }}" alt="" width="32px" height="32px">
            </a>

        </div>
        <div class="col-md-2 col-md-offset-4 text-right">
        </div>
    </div>
</div>

<div class="wrapper">
    <!-- Main Header -->
    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="{!! url('costProyectos') !!}" class="navbar-brand"><b>CosT</b>Guatemala</a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        @include('layouts.menu')
                        <form action="{{ action("cost_proyectoController@index") }}" method="get"
                              class="navbar-form navbar-left" data-toggle="tooltip"
                              title="Búsqueda por nombre proyecto, SNIP, NOG, localizacion, sector, sub sector, entidad aquisicion"
                              data-placement="right">
                            <div class="input-group">
                                <input type="text" name="q" class="form-control"
                                       placeholder="B&uacute;squeda r&aacute;pida"/>
                                <span class="input-group-btn">
                                     <button type='submit' name='search' id='search-btn' class="btn btn-primary"><i
                                                 class="fa fa-search"></i>
                                     </button>
                                </span>
                            </div>
                        </form>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li><a href="{!! url('costContactenos/create') !!}"><i class="fa fa-comments" aria-hidden="true"
                                                                               style="color: #01579b"></i> <span>Contáctenos</span></a>
                    </ul>


                    <ul class="nav navbar-nav navbar-right navbar-custom-menu">
                        @if(Auth::check())
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-user-circle" aria-hidden="true"></i> {!! Auth::user()->name !!}
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- The user image in the menu -->
                                    <li class="user-header card" style="color: #FFFFFF">

                                        <div class="card">
                                            <div class="box">
                                                <h2>
                                                    <br><br><br>
                                                    <img src="{{ asset('images/cost.jpg') }}" alt="" width="60%"
                                                         height="60%">
                                                    <br><span>{!! Auth::user()->name !!}</span>
                                                </h2>
                                                <p>
                                                    Acceso al sistema
                                                    desde {!! Auth::user()->created_at->format('M. Y') !!}
                                                </p>

                                                <div class="pull-left">
                                                    <a href="{!! url('reiniciar_password_perfil', [Auth::user()->id]) !!}"
                                                       class="btn  btn-flat btn_login">
                                                        <i class="fa fa-id-badge" aria-hidden="true"></i>
                                                        &nbsp; Perfil
                                                    </a>
                                                </div>
                                                <div class="pull-right">
                                                    <a href="{!! url('/logout') !!}" class="btn btn_login btn-flat"
                                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                        <i class="fa fa-unlock-alt" aria-hidden="true"></i> Cerrar sesi&oacute;n
                                                    </a>
                                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                                          style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <p style="font-size: 10pt;  color: #FFFFFF">
                                         <span>   {!! Auth::user()->name !!}<br>
                                            <span></span></span>
                                        </p>


                                    </li>
                                </ul>
                            </li>
                        @else
                            <ul class="nav navbar-nav">
                                <!-- Authentication Links -->
                                <li>
                                    <a href="{!! url('/login') !!}" style="color: #01579b">
                                        <i class="fa fa-unlock" aria-hidden="true"></i>
                                        INGRESO
                                        &nbsp;</a>
                                </li>
                            </ul>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="background-color: white">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('content_header')
        </section>

        <!-- Main content -->
        <section class="content">
            <div class='container'>
                @include("errors.errors")
            </div>
            @yield('content')
            @yield("contenido")
        </section>
    </div>
    <!-- /.content-wrapper -->
</div>


<!-- Main Footer -->
<footer class="main-footer" style="max-height: 100px;text-align: center">
    <strong>COST © 2018 <a href="http://costguatemala.org">CoST Guatemala</a>.</strong> Versión inicial.
</footer>


{{-- JAVASCRIPT y lo que lleva al final --}}

<script src="{{ url('js/jquery.min.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/select2.min.js') }}"></script>
<script src="{{ url('js/app.min.js') }}"></script>

<script src="{{ url('js/icheck.min.js') }}"></script>


{{-- Exportar a excel por JavaScript--}}
{{--
<script src="{{ url('js/tableExport.js') }}"  type="text/javascript"></script>
<script src="{{ url('js/jquery.base64.js') }}"  type="text/javascript"></script>
--}}
<script src="{{ url('js/jquery.table2excel.min.js') }}" type="text/javascript"></script>
<script src="{{ url('js/table2csv.min.js') }}" type="text/javascript"></script>


{{-- Dependent Drop Down --}}
<script src="{{ url('js/dependent-dropdown.js') }}" type="text/javascript"></script>
<!-- OpenLayers JS-->
<script src="{{ url('js/ol.js') }}" type="text/javascript"></script>

{{-- High Charts --}}
<script src="{{ url('highcharts/highcharts.js') }}" type="text/javascript"></script>
<script src="{{ url('highcharts/modules/exporting.js') }}" type="text/javascript"></script>
<script src="{{ url('highcharts/modules/offline-exporting.js') }}" type="text/javascript"></script>

<script src="{{ url('highcharts/themes/grid.js') }}" type="text/javascript"></script>

{{-- http://w3lessons.info/2015/07/13/export-html-table-to-excel-csv-json-pdf-png-using-jquery/ --}}
<script src="{{ url('js/tableExport.js') }}" type="text/javascript"></script>
<script src="{{ url('js/jquery.base64.js') }}" type="text/javascript"></script>


{{-- Autocomplete --}}
<script src="{{ url('js/jquery.tokeninput.js') }}" type="text/javascript"></script>

{{-- Date Picker --}}
<script src="{{ url('js/picker.js') }}" type="text/javascript"></script>
<script src="{{ url('js/picker.date.js') }}"></script>
{{-- Time Picker --}}
<script src="{{ url('js/picker.time.js') }}" type="text/javascript"></script>

<script src="{{ url('js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('js/dataTables.buttons.min.js') }}" type="text/javascript"></script>
<script src="{{ url('js/buttons.bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ url('js/buttons.colVis.min.js') }}" type="text/javascript"></script>


<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>



<script>
    // Internationalization
    Highcharts.setOptions({
        lang: {
            drillUpText: '◁ Regresar a {series.name}',
            printChart: 'Imprimir',
            downloadPNG: 'Descargar PNG',
            downloadJPEG: 'Descargar JPEG',
            downloadPDF: 'Descargar PDF',
            downloadSVG: 'Descargar SVG',
            contextButtonTitle: 'Menu contextual'
        }
    });
</script>
<script>
    $('.datepicker').pickadate({
        selectMonths: true // Creates a dropdown to control month
        , selectYears: 15 // Creates a dropdown of 15 years to control year
        //The format to show on the `input` element
        , format: 'dd-mmmm-yyyy'   //Como se muestra al usuario
        , formatSubmit: 'yyyy-mm-dd',  //IMPORTANTE: para el submit
        //The title label to use for the month nav buttons
        labelMonthNext: 'Mes siguiente',
        labelMonthPrev: 'Mes anterior',
        //The title label to use for the dropdown selectors
        labelMonthSelect: 'Elegir mes',
        labelYearSelect: 'Elegir año',
        //Months and weekdays
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        //Materialize modified
        weekdaysLetter: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        //Today and clear
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'Cerrar'
        //,editable: true
    });
</script>

{{-- Analytics
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-11738647-2', 'auto');
    ga('send', 'pageview');

</script>
--}}
@yield('scripts')
@stack('javascript')

</body>
</html>