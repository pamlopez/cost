<li class="dropdown">

    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-home" aria-hidden="true" style="color: #bf360c"></i>
        Acerca de<span class="caret"></span></a>

    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="{!! url('politicas/2') !!}"> <i class="fa fa-info" aria-hidden="true" style="color: #1976d2"></i>
                Acerca de
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="{!! url('costMetodologia') !!}"> <i class="fa fa-fw fa-building" aria-hidden="true" style="color: #1976d2"></i>
                Metodología
            </a>
        </li>
    </ul>

</li>
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-search-plus" aria-hidden="true" style="color: #bf360c"></i> Búsqueda de Proyectos <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="{!! url('costProyectos?b=1') !!}"> <i class="fa fa-search" aria-hidden="true" style="color: #1976d2"></i>
                Búsqueda general
            </a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="{!! url('busqueda_especifica') !!}"><i class="fa fa-search-plus" aria-hidden="true" style="color: #827717"></i>Búsqueda específica</a>
        </li>
    </ul>
</li>
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-bookmark" aria-hidden="true" style="color: #bf360c"></i> OC4IDS<span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a href="{!! url('politicas/1') !!}"><i class="fa fa-copyright" aria-hidden="true" style="color: #827717"></i>Politica de publicación</a>
        </li>
        <li class="divider"></li>
        <li>
            <a href="{!! url('download_oc4ids') !!}"> <i class="fa fa-cloud-download" aria-hidden="true" style="color: #1976d2"></i>
                Descarga Masiva
            </a>
        </li>
    </ul>
</li>
<li class="dropdown">
    <a href="{!! url('indicadores') !!}"><i class="fa fa-fw fa-bar-chart" aria-hidden="true" style="color: #e65100"></i>
        <span>Dashboard</span></a>
</li>





@if(Auth::check())
    @if(\App\User::tiene_rol(1)==true)
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-list-alt" aria-hidden="true" style="color: #1633c8"></i> Administración <span
                        class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{!! url('participantes') !!}"> <i class="fa fa-users" aria-hidden="true"
                                                               style="color: #c81823"></i> Gestión de
                        Participantes</a></li>
                <li class="divider"></li>

                <li><a href="{!! url('catCats') !!}"><i class="fa fa-cube" aria-hidden="true"></i> Gestión de Catalogos</a>
                </li>
                <li><a href="{!! url('catItems') !!}"><i class="fa fa-cubes" aria-hidden="true"
                                                         style="color: #827717"></i>
                        <span>Gesti&oacute;n de items de cat&aacute;logos</span></a></li>
                <li class="divider"></li>
                <li><a href="{!! url('politicas/1/edit') !!}"><i class="fa fa-info" aria-hidden="true"
                                                         style="color: #827717"></i>
                        <span>Gesti&oacute;n de Politica de Publicación</span></a></li>
                <li class="divider"></li>
                <li><a href="{!! url('politicas/2/edit') !!}"><i class="fa fa-copyright" aria-hidden="true"
                                                          style="color: #827717"></i>
                        <span>Gesti&oacute;n de Acerca de</span></a></li>

            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i
                        class="fa fa-address-card" aria-hidden="true"
                        style="color: #134d7a"></i> Usuarios <span
                        class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ url('/administracion') }}"><i class="fa fa-user-plus" aria-hidden="true"
                                                              style="color: #039be5"></i>
                        <span>Administración usuario</span></a>
                </li>
                <li class="divider"></li>
            </ul>
        </li>
    @endif
@endif
@push('javascript')

    <script>
        jQuery(document).ready(function ($) {
            try {
                var window_loc = window.location.href.split('?')[0], target = $('.sidebar-menu li a'),
                    exprecion = /\d+/;
                var url_valida = exprecion.test(window_loc) && window_loc.match(exprecion).length > 0 ? window_loc.replace(/[0-9]/g, '').slice(0, -1) : window_loc
                url_valida = url_valida.replace('//edi', '')
                url_valida = url_valida.replace('/create', '')
                target.each(function (v, k) {
                    var element = $(this), url_href = element.attr("href");
                    if (url_href == url_valida) {
                        element.closest("li").addClass('active');
                    }
                })
            } catch (e) {
                console.log(e.message)
            }
        });

    </script>
@endpush