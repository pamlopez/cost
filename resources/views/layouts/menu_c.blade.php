
<li class="header">ANÁLISIS ESTADÍSTICO</li>
<li><a href="{!! url('dash/ai') !!}"><i class="fa fa-fw fa-female" aria-hidden="true"></i> <span>Análisis Atención Inicial</span></a></li>
<li><a href="{!! url('dash/legal') !!}"><i class="fa fa-fw fa-gavel" aria-hidden="true"></i> <span>Análisis Asesoría Legal</span></a></li>
<li><a href="{!! url('dash/caimus') !!}"><i class="fa fa-fw fa-building" aria-hidden="true"></i> <span>Análisis Asistencia CAIMUS</span></a></li>

<li class="header">GESTIÓN DE INFORMACIÓN</li>

<li><a href="{!! url('atencionInicials') !!}"><i class="fa fa-id-card-o" aria-hidden="true"></i> <span>Atencion inicial</span></a></li>
<li><a href="{!! url('menuLegal') !!}"><i class="fa fa-balance-scale" aria-hidden="true"></i> <span>Área legal</span></a></li>
<li><a href="{!! url('agendas') !!}"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Agenda legal</span></a></li>
<li><a href="{{  url('agendaPsicologia')}}"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Agenda psicología</span></a></li>
<li><a href="{!! url('agendaTrabajoSocial') !!}"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Agenda trabajo social</span></a></li>
<li><a href="{!! url('agendaAlbergue') !!}"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Agenda albergue</span></a></li>


<li class="header">CONTROL DE ASISTENCIA</li>
<li><a href="{!! url('controlAsistencias') !!}"><i class="fa fa-list-alt" aria-hidden="true"></i> <span>Control de asistencia</span></a></li>


<li class="header">REPORTES</li>
<li><a href="{!! url('reportes/repoMingob') !!}">  <i class="fa fa-file-excel-o" aria-hidden="true"></i><span>Reporte MINGOB</span></a></li>
<li><a href="{!! url('reportes/analisis') !!}">  <span class="glyphicon glyphicon-book" aria-hidden="true"></span><span>Informe estadístico de <br> acciones área Legal</span></a></li>
<li><a href="{!! url('reportes/indexAgendaLegal') !!}">  <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span><span>Informe estadístico de acciones  <br> Agenda Legal</span></a></li>


@if(\App\User::tiene_rol(1)==true)
    <li class="header">Personalización</li>
    <!--li><a href="{!! url('catCats') !!}"><i class="fa fa-cube" aria-hidden="true"></i> Catalogos</a></li-->
    <li><a href="{!! url('catItems') !!}"><i class="fa fa-cubes" aria-hidden="true"></i> <span>Gesti&oacute;n de cat&aacute;logos</span></a></li>
    <li><a href="{!! url('empleadas') !!}"><i class="fa fa-id-badge" aria-hidden="true"></i> <span>Personal CAIMU</span></a></li>
    <li class="header">Usuarios</li>
    <li><a href="{{ url('/administracion') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> <span>Registrar usuario</span></a></li>
@endif

@push('javascript')

    <script>
        jQuery(document).ready(function($){
            try {
                var window_loc = window.location.href.split('?')[0], target = $('.sidebar-menu li a'),exprecion = /\d+/;
                var url_valida = exprecion.test(window_loc) && window_loc.match(exprecion).length > 0 ? window_loc.replace(/[0-9]/g, '').slice(0,-1): window_loc
                url_valida = url_valida.replace('//edi', '')
                url_valida = url_valida.replace('/create', '')
                target.each(function (v,k) {
                    var element = $(this),url_href=element.attr("href");
                    if (url_href==url_valida){
                        element.closest( "li" ).addClass('active');
                    }
                })
            }catch (e) {
                console.log(e.message)
            }
        });

    </script>

@endpush