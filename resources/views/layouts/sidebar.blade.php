@if(Auth::check())
    <aside class="main-sidebar" id="sidebar-wrapper">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            {{--
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ url("images/ggm.jpg") }}" class="img-responsive"
                         alt="User Image"/>
                </div>
                <div class="pull-left info">
                    @if (Auth::guest())
                    <p>CAIMU</p>
                    @else
                        <p>{{ Auth::user()->name}}</p>
                    @endif
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            --}}

            <!-- search form (Optional) -->
            <form action="{{ action("atencion_inicialController@index") }}" method="get" class="sidebar-form" data-toggle="tooltip" title="Búsqueda por nombre de sobreviviente, H.V, DPI, agresor"  data-placement="right">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="B&uacute;squeda r&aacute;pida"/>
                    <span class="input-group-btn">
                        <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
            <!-- Sidebar Menu -->

            <ul class="sidebar-menu">
                @include('layouts.menu')
            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-theme-demo-options-tab" data-toggle="tab"><i class="fa fa-search"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div id="control-sidebar-theme-demo-options-tab" class="tab-pane active">
                {{ Form::open(array('action' => 'atencion_inicialController@index','method' => 'post')) }}
                <!-- Id Caimu Field -->
                    <div class="form-group">
                        {!! Form::label('n1', 'Primer Nombre:') !!}
                        {!! Form::text('n1', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Id Persona Field -->
                    <div class="form-group">
                        {!! Form::label('p1', 'Primer Apellido:') !!}
                        {!! Form::text('p1', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Fecha Field -->
                    <div class="form-group">
                        {!! Form::label('fh_inicial', 'Fecha inicial:') !!}
                        {!! Form::text('fh_inicial', null, ['class' => 'form-control datepicker']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('fh_fin', 'Fecha final:') !!}
                        {!! Form::text('fh_fin', null, ['class' => 'form-control datepicker']) !!}
                    </div>

                    <button type="submit" class="btn bg-purple"><i class="fa fa-filter"></i> Filtrar</button>

                {{ Form::close() }}
            </div>

            <!-- /.tab-pane -->
        </div>
    </aside>
@else
    <aside class="main-sidebar" id="sidebar-wrapper">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li class="header">Estadísticas</li>
                <li><a href="{!! url('dash/ai') !!}"><i class="fa fa-fw fa-female" aria-hidden="true"></i> <span>Atención Inicial</span></a></li>
                <li><a href="{!! url('dash/legal') !!}"><i class="fa fa-fw fa-gavel" aria-hidden="true"></i> <span>Asesoría Legal</span></a></li>
                <li><a href="{!! url('dash/caimus') !!}"><i class="fa fa-fw fa-building" aria-hidden="true"></i> <span>Asistencia CAIMUS</span></a></li>
                <li class="header">Usuarios registrados</li>
                <li><a href="{!! url('/login') !!}"><i class="fa fa-unlock" aria-hidden="true"></i> <span>Ingreso</span></a></li>

            </ul>
        </section>
    </aside>
@endif




@push('javascript')
    <script>
        $('.datepicker').pickadate({
            selectMonths: true // Creates a dropdown to control month
            , selectYears: 15 // Creates a dropdown of 15 years to control year
            //The format to show on the `input` element
            , format: 'dd-mmmm-yyyy'   //Como se muestra al usuario
            , formatSubmit: 'yyyy-mm-dd',  //IMPORTANTE: para el submit
            //The title label to use for the month nav buttons
            labelMonthNext: 'Mes siguiente',
            labelMonthPrev: 'Mes anterior',
            //The title label to use for the dropdown selectors
            labelMonthSelect: 'Elegir mes',
            labelYearSelect: 'Elegir año',
            //Months and weekdays
            monthsFull: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Dicembre' ],
            monthsShort: [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
            weekdaysFull: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
            weekdaysShort: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
            //Materialize modified
            weekdaysLetter: [ 'D', 'L', 'M', 'M', 'J', 'V', 'S' ],
            //Today and clear
            today: 'Hoy',
            clear: 'Limpiar',
            close: 'Cerrar'
            //,editable: true
        });
    </script>
@endpush