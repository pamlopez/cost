<table class="table table-responsive" id="costEstimacions-table">
    <thead>
    <tr>
        <th>#</th>
        <th>Tipo de estimación</th>
        <th>Numero de pagos</th>
        <th>Monto total pagado</th>
        <th colspan="3">Acción</th>
    </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($costEstimacions as $costEstimacion)
        @php($i++)
        <tr>
            <td>{!! $i !!}</td>
            <td>{!! $costEstimacion->fmt_tipo_estimacion !!}</td>
            <td>{!! $costEstimacion->numero_pagos !!}</td>
            <td>{!! $costEstimacion->monto_total_pagos !!}</td>
            <td>
                {!! Form::open(['route' => ['costEstimacions.destroy', $costEstimacion->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costEstimacions.show', [$costEstimacion->id]) !!}"
                       class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                        <a href="{!! route('costEstimacions.edit', [$costEstimacion->id]) !!}"
                           class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>