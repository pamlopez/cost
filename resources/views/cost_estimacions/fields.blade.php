{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costEstimacion->id_proyecto, ['class' => 'form-control']) !!}
{!! Form::hidden('tipo_estimacion',isset($tipo_estimacion)?$tipo_estimacion:$costEstimacion->tipo_estimacion, ['class' => 'form-control']) !!}


<!-- Numero Pagos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numero_pagos', 'Numero Pagos:') !!}
    {!! Form::number('numero_pagos', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Total Pagos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto_total_pagos', 'Monto Total Pagos:') !!}
    {!! Form::text('monto_total_pagos', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}

    @if(isset($id_proyecto))
        <a href="{!! route('costEstimacions.index', ['id_proyecto'=>$id_proyecto,'tipo_estimacion'=>$tipo_estimacion]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costEstimacions.index', ['id_proyecto'=>$costEstimacion->id_proyecto,'tipo_estimacion'=>$costEstimacion->tipo_estimacion]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif

</div>
