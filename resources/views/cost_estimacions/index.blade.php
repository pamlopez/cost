@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            @if($tipo_estimacion == 1)
                {{"Estimaciones Aprobadas"}}
            @else
                {{"Estimaciones Pagadas"}}
            @endif
        </h1>
        @if(Auth::check())
            <h1 class="pull-right">
                <a class="btn btn-primary pull-right"
                   href="{!! route('costEstimacions.create', ['id_proyecto'=>$id_proyecto,'tipo_estimacion'=>$tipo_estimacion]) !!}"
                   style="margin-top: -10px;margin-bottom: 5px">Nueva Estimación</a>
            </h1>
        @endif
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('cost_estimacions.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

