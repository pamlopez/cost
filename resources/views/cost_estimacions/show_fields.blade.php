
<!-- Id Proyecto Field -->
<div class="form-group">
    {!! Form::label('id_proyecto', 'No. de proyecto:') !!}
    <p>{!! $costEstimacion->id_proyecto !!}-2018Gt</p>
</div>

<!-- Tipo Estimacion Field -->
<div class="form-group">
    {!! Form::label('tipo_estimacion', 'Tipo estimacion:') !!}
    <p>{!! $costEstimacion->fmt_tipo_estimacion !!}</p>
</div>

<!-- Numero Pagos Field -->
<div class="form-group">
    {!! Form::label('numero_pagos', 'Numero de pagos:') !!}
    <p>{!! $costEstimacion->numero_pagos !!}</p>
</div>

<!-- Monto Total Pagos Field -->
<div class="form-group">
    {!! Form::label('monto_total_pagos', 'Monto  total Pagado:') !!}
    <p>{!! $costEstimacion->monto_total_pagos !!}</p>
</div>

