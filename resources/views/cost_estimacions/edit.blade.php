@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Editar {{$costEstimacion->fmt_tipo_estimacion}}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costEstimacion, ['route' => ['costEstimacions.update', $costEstimacion->id], 'method' => 'patch']) !!}

                        @include('cost_estimacions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection