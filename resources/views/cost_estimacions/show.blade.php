@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cost Estimacion
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_estimacions.show_fields')
                    <a href="{!! route('costEstimacions.index', ['id_proyecto'=>$costEstimacion->id_proyecto,'tipo_estimacion'=>$costEstimacion->tipo_estimacion]) !!}"
                       class="btn btn-default">Atrás</a>

                </div>
            </div>
        </div>
    </div>
@endsection
