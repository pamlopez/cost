<!-- Nombre Comun Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_comun', 'Nombre:') !!}
    {!! Form::text('nombre_comun', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Legal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_legal', 'Nombre legal:') !!}
    {!! Form::text('nombre_legal', null, ['class' => 'form-control']) !!}
</div>

<!-- Uri Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uri', 'Uri:') !!}
    {!! Form::text('uri', null, ['class' => 'form-control']) !!}
</div>

<!-- Direccion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('direccion', 'Dirección:') !!}
    {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
</div>

<!-- Direccion Referencia Field -->
<div class="form-group col-sm-6">
    {!! Form::label('direccion_referencia', 'Dirección de referencia:') !!}
    {!! Form::text('direccion_referencia', null, ['class' => 'form-control']) !!}
</div>

<!-- Codigo Postal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('codigo_postal', 'Código Postal:') !!}
    {!! Form::number('codigo_postal', null, ['class' => 'form-control']) !!}
</div>

<!-- Nombre Contacto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre_contacto', 'Nombre del contacto:') !!}
    {!! Form::text('nombre_contacto', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

@include('chivos.catalogo', ['chivo_control' => 'schema'
                           ,'chivo_id_cat'=>79
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Categoría según Finanzas (OC4IDS):'])

<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono', 'Teléfono:') !!}
    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('numero_fax', 'Número de Fax:') !!}
    {!! Form::text('numero_fax', null, ['class' => 'form-control']) !!}
</div>


<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('localidad', 'Localidad:') !!}
    {!! Form::text('localidad', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('region', 'Region:') !!}
    {!! Form::text('region', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('nombre_pais', 'Nombre del País:') !!}
    {!! Form::text('nombre_pais', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('url', 'Url del Participante:') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('participantes.index') !!}" class="btn btn-default">Cancelar</a>
</div>
