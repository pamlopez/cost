<table class="table table-responsive" id="participantes-table">
    <thead>
    <tr>
        <th>#</th>
        <th>Nombre Común</th>
        <th>Nombre Legal</th>
        <th>Uri</th>
        <th>Dirección</th>
        <th>Referencia</th>
        <th>Código Postal</th>
        <th>Nombre del contacto</th>
        <th>Email</th>
        <th>Teléfono</th>
        <th colspan="3">Acción</th>
    </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($participantes as $participante)
        @php($i++)
        <tr>
            <td>{!! $i !!}</td>
            <td>{!! $participante->nombre_comun !!}</td>
            <td>{!! $participante->nombre_legal !!}</td>
            <td>{!! $participante->uri !!}</td>
            <td>{!! $participante->direccion !!}</td>
            <td>{!! $participante->direccion_referencia !!}</td>
            <td>{!! $participante->codigo_postal !!}</td>
            <td>{!! $participante->nombre_contacto !!}</td>
            <td>{!! $participante->email !!}</td>
            <td>{!! $participante->telefono !!}</td>
            <td>
                {!! Form::open(['route' => ['participantes.destroy', $participante->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('participantes.show', [$participante->id]) !!}" class='btn btn-info btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('participantes.edit', [$participante->id]) !!}" class='btn btn-warning btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>