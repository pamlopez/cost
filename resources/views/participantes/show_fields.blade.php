<!-- Id Participante Field -->
<div class="form-group">
    {!! Form::label('id_participante', 'Id Participante:') !!}
    <p>{!! $participante->id_participante !!}</p>
</div>

<!-- Nombre Comun Field -->
<div class="form-group">
    {!! Form::label('nombre_comun', 'Nombre:') !!}
    <p>{!! $participante->nombre_comun !!}</p>
</div>

<!-- Nombre Legal Field -->
<div class="form-group">
    {!! Form::label('nombre_legal', 'Nombre legal:') !!}
    <p>{!! $participante->nombre_legal !!}</p>
</div>

<!-- Uri Field -->
<div class="form-group">
    {!! Form::label('uri', 'Uri:') !!}
    <p>{!! $participante->uri !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{!! $participante->direccion !!}</p>
</div>

<!-- Direccion Referencia Field -->
<div class="form-group">
    {!! Form::label('direccion_referencia', 'Referencia:') !!}
    <p>{!! $participante->direccion_referencia !!}</p>
</div>

<!-- Codigo Postal Field -->
<div class="form-group">
    {!! Form::label('codigo_postal', 'Codigo postal:') !!}
    <p>{!! $participante->codigo_postal !!}</p>
</div>

<!-- Nombre Contacto Field -->
<div class="form-group">
    {!! Form::label('nombre_contacto', 'Nombre del contacto:') !!}
    <p>{!! $participante->nombre_contacto !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $participante->email !!}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', 'Teléfono:') !!}
    <p>{!! $participante->telefono !!}</p>
</div>

<!-- Codigo Postal Field -->
<div class="form-group">
    {!! Form::label('numero_fax', 'Número de Fax:') !!}
    <p>{!! $participante->numero_fax !!}</p>
</div>

<!-- Codigo Postal Field -->
<div class="form-group">
    {!! Form::label('localidad', 'Localidad:') !!}
    <p>{!! $participante->localidad !!}</p>
</div>

<!-- Nombre Contacto Field -->
<div class="form-group">
    {!! Form::label('region', 'Region:') !!}
    <p>{!! $participante->region !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('nombre_pais', 'Nombre del País:') !!}
    <p>{!! $participante->nombre_pais !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('url', 'Url:') !!}
    <p>{!! $participante->url !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('schema', 'Categoría según Finanzas (OC4IDS):') !!}
    <p>{!! $participante->fmt_schema !!}</p>
</div>

<div class="form-group">
    {!! Form::label('id_schema', 'ID según Finanzas (OC4IDS):') !!}
    <p>{!! $participante->fmt_id_schema !!}</p>
</div>

