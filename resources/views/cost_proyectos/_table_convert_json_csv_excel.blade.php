<div class="table-responsive">
    <table id="csv" class="table table-striped table-condensed">
        <thead>
        <tr>
            @if(isset($jsonDecoded->uri ))
                <th>uri</th>  @endif
            @if(isset($jsonDecoded->publishedDate ))
                <th>publishedDate</th> @endif
            @if(isset($jsonDecoded->publisher->name  ))
                <th>publisher</th> @endif
            @if(isset($jsonDecoded->license  ))
                <th>license</th> @endif
            @if(isset($jsonDecoded->projects[0]) && isset($jsonDecoded->projects[0]->id ))
                <th>id</th> @endif
            @if(isset($jsonDecoded->projects[0]) && isset($jsonDecoded->projects[0]->updated ))
                <th>updated</th> @endif
            @if(isset($jsonDecoded->projects[0]) && isset($jsonDecoded->projects[0]->title ))
                <th>title</th> @endif
            @if(isset($jsonDecoded->projects[0]) && isset($jsonDecoded->projects[0]->description ))
                <th>description</th> @endif
            @if(isset($jsonDecoded->projects[0]) && isset($jsonDecoded->projects[0]->status ))
                <th>status</th> @endif

            @if(isset($jsonDecoded->projects[0]->sector))
                @foreach($jsonDecoded->projects[0]->sector as $id => $v)
                    <td> sector</td>
                @endforeach
            @endif

            @if(isset($jsonDecoded->projects[0]->type))
                <th>type</th>
            @endif

            @if(isset($jsonDecoded->projects[0]->period))
                <th>period / startDate</th>
                <th>period / endDate</th>
                <th>period / durationInDays</th>
            @endif

            @if(isset($jsonDecoded->projects[0]->locations))
                <th>locations / id</th>
                <th>locations / description</th>
                <th>locations / geometry / type</th>
                <th>locations / geometry / coordinates / X</th>
                <th>locations / geometry / coordinates / Y</th>
                <th>locations / address / streetAddress</th>
                <th>locations / address / region</th>
                <th>locations / address / countryName</th>
            @endif

            @if(isset($jsonDecoded->projects[0]->parties))
                @foreach($jsonDecoded->projects[0]->parties as $a => $b )
                    <th>parties / id</th>
                    <th>parties / name</th>
                    @if(isset($b->roles))
                        @foreach($b->roles as $aa =>$bb )
                            <th>parties / roles</th>
                        @endforeach
                    @endif
                @endforeach
            @endif
            @if(isset($jsonDecoded->projects[0]->documents))
                @foreach($jsonDecoded->projects[0]->documents as $aaa =>$bbb )
                    <th>documents / id</th>
                    <th>documents / documentType</th>
                    <th>documents / title</th>
                    <th>documents / description</th>
                    <th>documents / url</th>
                    <th>documents / datePublished</th>
                    <th>documents / format</th>
                    <th>documents / language</th>
                    <th>documents / pageStart</th>
                    <th>documents / pageEnd</th>
                    <th>documents / accessDetails</th>
                    <th>documents / author</th>
                @endforeach
            @endif
            @if(isset($jsonDecoded->projects[0]->contractingProcesses))
                @foreach($jsonDecoded->projects[0]->contractingProcesses as $x =>$y )
                    <th>contractingProcesses / id</th>
                    <th>contractingProcesses / summary / nature</th>
                    <th>contractingProcesses / summary / title</th>
                    <th>contractingProcesses / summary / description</th>
                    <th>contractingProcesses / summary / status</th>
                    <th>contractingProcesses / summary / tender / costEstimate / amount</th>
                    <th>contractingProcesses / summary / tender / costEstimate / currency</th>
                    @if(isset($y->summary->suppliers))
                        @foreach($y->summary->suppliers as $u => $v)
                            <th>contractingProcesses / summary / id</th>
                            <th>contractingProcesses / summary / name</th>
                        @endforeach
                    @endif
                    <td>contractingProcesses / summary / contractValue / amount</td>
                    <td>contractingProcesses / summary / contractValue / currency</td>
                    <td>contractingProcesses / summary / contractPeriod / startDate</td>
                    <td>contractingProcesses / summary / contractPeriod / endDate</td>
                    <td>contractingProcesses / summary / contractPeriod / durationInDays</td>
                    <td>contractingProcesses / summary / finalValue / amount</td>
                    <td>contractingProcesses / summary / finalValue / currency</td>
                    <td>contractingProcesses / summary / documents / amount</td>
                    <td>contractingProcesses / summary / documents / currency</td>
                    @if(isset($y->summary->documents ))
                        @foreach($y->summary->documents as $u => $bbb)
                            @if($bbb != 'amount' || $bbb != 'currency')
                                <th>contractingProcesses / summary / documents / id</th>
                                <th>contractingProcesses / summary / documents / documentType</th>
                                <th>contractingProcesses / summary / documents / title</th>
                                <th>contractingProcesses / summary / documents / description</th>
                                <th>contractingProcesses / summary / documents / url</th>
                                <th>contractingProcesses / summary / documents / datePublished</th>
                                <th>contractingProcesses / summary / documents / format</th>
                                <th>contractingProcesses / summary / documents / language</th>
                                <th>contractingProcesses / summary / documents / pageStart</th>
                                <th>contractingProcesses / summary / documents / pageEnd</th>
                                <th>contractingProcesses / summary / documents / accessDetails</th>
                                <th>contractingProcesses / summary / documents / author</th>
                            @endif
                        @endforeach
                    @endif
                    @if(isset($y->summary->modifications))
                        @foreach($y->summary->modifications as $u => $z)
                            <td>contractingProcesses / summary /modifications/ id</td>
                            <td>contractingProcesses / summary /modifications/ description</td>
                            <td>contractingProcesses / summary /modifications/ rationale</td>
                            <td>contractingProcesses / summary /modifications/ type</td>
                            @if(isset($v->oldContractValue))
                                <td>contractingProcesses / summary /modifications/ oldContractValue / amount
                                </td>
                                <td>contractingProcesses / summary /modifications/ oldContractValue / currency
                                </td>
                            @endif
                            @if(isset($v->newContractValue))
                                <td>contractingProcesses / summary /modifications/ newContractValue / amount
                                </td>
                                <td>contractingProcesses / summary /modifications/ newContractValue / currency
                                </td>
                            @endif
                            @if(isset($v->newContractPeriod))
                                <td>contractingProcesses / summary /modifications/ newContractPeriod / endDate
                                </td>
                                <td>contractingProcesses / summary /modifications/ newContractPeriod /
                                    durationInDays
                                </td>
                            @endif
                        @endforeach
                    @endif


                @endforeach
            @endif
        </tr>
        </thead>
        <tbody>
        <tr>
            @if(isset($jsonDecoded->uri ))
                <td>{!! $jsonDecoded->uri !!}</td>  @endif
            @if(isset($jsonDecoded->publishedDate ))
                <td>{!! $jsonDecoded->publishedDate !!}</td> @endif
            @if(isset($jsonDecoded->publisher->name  ))
                <td>{!! $jsonDecoded->publisher->name !!}</td> @endif
            @if(isset($jsonDecoded->license  ))
                <td>{!! $jsonDecoded->license !!}</td> @endif
            @if(isset($jsonDecoded->projects[0]) && isset($jsonDecoded->projects[0]->id ))
                <td>{!! $jsonDecoded->projects[0]->id !!}</td> @endif
            @if(isset($jsonDecoded->projects[0]) && isset($jsonDecoded->projects[0]->updated ))
                <td>{!! $jsonDecoded->projects[0]->updated !!}</td> @endif
            @if(isset($jsonDecoded->projects[0]) && isset($jsonDecoded->projects[0]->title ))
                <td>{!! $jsonDecoded->projects[0]->title !!}</td> @endif
            @if(isset($jsonDecoded->projects[0]) && isset($jsonDecoded->projects[0]->description ))
                <td>{!! $jsonDecoded->projects[0]->description !!}</td> @endif
            @if(isset($jsonDecoded->projects[0]) && isset($jsonDecoded->projects[0]->status ))
                <td>{!! $jsonDecoded->projects[0]->status !!}</td> @endif

            @if(isset($jsonDecoded->projects[0]->sector))
                @foreach($jsonDecoded->projects[0]->sector as $id => $v)
                    <td>  {!! $v!!} </td>
                @endforeach
            @endif

            @if(isset($jsonDecoded->projects[0]->type))
                <td>{!! $jsonDecoded->projects[0]->type !!}</td>
            @endif
            @if(isset($jsonDecoded->projects[0]->period))
                <td>
                    @if(isset($jsonDecoded->projects[0]->period->startDate))
                        {!! $jsonDecoded->projects[0]->period->startDate !!}
                    @endif
                </td>
                <td>
                    @if(isset($jsonDecoded->projects[0]->period->endDate))
                        {!! $jsonDecoded->projects[0]->period->endDate !!}
                    @endif
                </td>
                <td>
                    @if(isset($jsonDecoded->projects[0]->period->durationInDays))
                        {!! $jsonDecoded->projects[0]->period->durationInDays !!}
                    @endif
                </td>
            @endif

            @if(isset($jsonDecoded->projects[0]->locations))
                @php($locations = $jsonDecoded->projects[0]->locations[0])
                <td>
                    @if(isset($locations->id))
                        {!! $locations->id!!}
                    @endif
                </td>
                <td>
                    @if(isset($locations->description))
                        {!! $locations->description!!}
                    @endif
                </td>
                <td>
                    @if(isset($locations->geometry->type))
                        {!! $locations->geometry->type!!}
                    @endif
                </td>
                <td>
                    @if(isset($locations->geometry->coordinates[0]))
                        {!! $locations->geometry->coordinates[0]!!}
                    @endif
                </td>
                <td>
                    @if(isset($locations->geometry->coordinates[1]))
                        {!! $locations->geometry->coordinates[1]!!}
                    @endif
                </td>
                <td>
                    @if(isset($locations->address->streetAddress))
                        {!! $locations->address->streetAddress!!}
                    @endif
                </td>
                <td>
                    @if(isset($locations->address->region))
                        {!! $locations->address->region!!}
                    @endif
                </td>
                <td>
                    @if(isset($locations->address->countryName))
                        {!! $locations->address->countryName!!}
                    @endif
                </td>
            @endif
            @if(isset($jsonDecoded->projects[0]->parties))
                @foreach($jsonDecoded->projects[0]->parties as $a => $b )
                    <td>
                        @if(isset($b->id))
                            {!! $b->id!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($b->name))
                            {!! $b->name!!}
                        @endif
                    </td>

                    @if(isset($b->roles))
                        @foreach($b->roles as $aa =>$bb )
                            <td>{!! $bb !!}</td>
                        @endforeach
                    @endif
                @endforeach
            @endif
            @if(isset($jsonDecoded->projects[0]->documents))
                @foreach($jsonDecoded->projects[0]->documents as $aaa =>$bbb )
                    <td>
                        @if(isset($bbb->id))
                            {!! $bbb->id!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->documentType))
                            {!! $bbb->documentType!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->title))
                            {!! $bbb->title!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->description))
                            {!! $bbb->description!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->url))
                            {!! $bbb->url!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->datePublished))
                            {!! $bbb->datePublished!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->format))
                            {!! $bbb->format!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->language))
                            {!! $bbb->language!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->pageStart))
                            {!! $bbb->pageStart!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->pageEnd))
                            {!! $bbb->pageEnd!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->accessDetails))
                            {!! $bbb->accessDetails!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($bbb->author))
                            {!! $bbb->author!!}
                        @endif
                    </td>
                @endforeach
            @endif
            @if(isset($jsonDecoded->projects[0]->contractingProcesses))
                @foreach($jsonDecoded->projects[0]->contractingProcesses as $x =>$y )
                    <td>
                        @if(isset($y->id))
                            {!! $y->id!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->nature[0]))
                            {!! $y->summary->nature[0]!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->title))
                            {!! $y->summary->title!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->description))
                            {!! $y->summary->description!!}
                            @endif
                    </td>
                    <td>
                        @if(isset($y->summary->status))
                            {!! $y->summary->status!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->tender->costEstimate->amount))
                            {!! $y->summary->tender->costEstimate->amount!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->tender->costEstimate->currency))
                            {!! $y->summary->tender->costEstimate->currency!!}
                        @endif
                    </td>

                    @if(isset($y->summary->suppliers))
                        @foreach($y->summary->suppliers as $u => $v)
                            <td>
                                @if(isset($v->id))
                                    {!! $v->id!!}
                                @endif
                            </td>
                            <td>
                                @if(isset($v->name))
                                    {!! $v->name!!}
                                @endif
                            </td>
                        @endforeach
                    @endif

                    <td>
                        @if(isset($y->summary->contractValue->amount))
                            {!! $y->summary->contractValue->amount!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->contractValue->currency))
                            {!! $y->summary->contractValue->currency!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->contractPeriod->startDate))
                            {!! $y->summary->contractPeriod->startDate!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->contractPeriod->endDate))
                            {!! $y->summary->contractPeriod->endDate!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->contractPeriod->durationInDays))
                            {!! $y->summary->contractPeriod->durationInDays!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->finalValue->amount))
                            {!! $y->summary->finalValue->amount!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->finalValue->currency))
                            {!! $y->summary->finalValue->currency!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->documents->currency))
                            {!! $y->summary->documents->currency!!}
                        @endif
                    </td>
                    <td>
                        @if(isset($y->summary->documents->amount))
                            {!! $y->summary->documents->amount!!}
                        @endif
                    </td>

                    @if(isset($y->summary->documents ))
                        @foreach($y->summary->documents as $u => $bbb)
                            @if($bbb != 'amount' || $bbb != 'currency')
                                <td>
                                    @if(isset( $bbb->id))
                                        {!! $bbb->id!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->documentType))
                                        {!! $bbb->documentType!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->title))
                                        {!! $bbb->title!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->description))
                                        {!! $bbb->description!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->url))
                                        {!! $bbb->url!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->datePublished))
                                        {!! $bbb->datePublished!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->format))
                                        {!! $bbb->format!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->language))
                                        {!! $bbb->language!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->pageStart))
                                        {!! $bbb->pageStart!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->pageEnd))
                                        {!! $bbb->pageEnd!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->accessDetails))
                                        {!! $bbb->accessDetails!!}
                                    @endif
                                </td>
                                <td>
                                    @if(isset( $bbb->author))
                                    {!! $bbb->author!!}
                                    @endif
                                </td>
                            @endif
                        @endforeach
                    @endif

                    @if(isset($y->summary->modifications))
                        @foreach($y->summary->modifications as $u => $z)
                            <td>{!! $z->id!!}  </td>
                            <td>{!! $z->description!!}  </td>
                            <td>{!! $z->rationale!!}  </td>
                            <td>{!! $z->type!!}  </td>
                            @if(isset($v->oldContractValue))
                                <td>{!! $z->oldContractValue->amount!!}  </td>
                                <td>{!! $z->oldContractValue->currency!!}  </td>
                            @endif
                            @if(isset($v->newContractValue))
                                <td>{!! $z->newContractValue->amount!!}  </td>
                                <td>{!! $z->newContractValue->currency!!}  </td>
                            @endif
                            @if(isset($v->newContractPeriod))
                                <td>{!! $z->newContractPeriod->endDate!!}  </td>
                                <td>{!! $z->newContractPeriod->durationInDays!!}  </td>
                            @endif
                        @endforeach
                    @endif

                @endforeach
            @endif
        </tr>
        </tbody>
    </table>
</div>
