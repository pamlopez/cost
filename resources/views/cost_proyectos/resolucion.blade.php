
<div class="alert alert-danger alert-dismissible">
    <h4><i class="icon fa fa-warning"></i> Advertencia!</h4>
    Esta página se encuentra en construcción. Los datos públicados pueden variar en base a su fuente.
</div>


<div class="box box-danger">
    <div class="box-header" style="background-color: rgba(200,24,35,0.2)">
        <b><h3 class="box-title"><i class="fa fa-pencil" aria-hidden="true"></i> Información General del Proyecto
            </h3></b>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body " style="background-color: #f7f7f7;">
        <br>
        <div class="info-box">
            <span class="info-box-icon bg-blue-gradient"><i class="fa fa-file-text"></i></span>
            <div class="info-box-content">
        <span class="info-box-number">
            <div class="col-md-3 ">
              a. Identificación del Proyecto
            </div><!-- a. Identificación del Proyecto -->
             <div class="col-md-2">
               <a href="{!! url('identificacionProyecto', [$costProyecto->id]) !!}"
                  class='btn btn-info btn-xs'><i
                           class="glyphicon glyphicon-eye-open"></i></a>
                 @if(Auth::check() && $you_have_permission == true)
                     <a href="{!! route('costProyectos.edit', [$costProyecto->id]) !!}"
                        class='btn btn-warning btn-xs'><i
                                 class="glyphicon glyphicon-edit"></i></a>
                 @endif
             </div>
        </span>
                <span class="info-box-text">
            <div class="row">
                 <div class="col-md-4">
                     <h5>1.  Operadores del Portal</h5>
                </div>
                 <div class="col-md-2">
                     @php($af=\App\Models\cost_operador::operador_c($costProyecto->id))
                     @if(count($af)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costOperadors.create', ['id_proyecto'=>$costProyecto->id]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>

                         @endif
                     @else
                         <a href="{!! route('costOperadors.index', ['id_proyecto'=>$costProyecto->id]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif
                 </div>

            </div>

        </span>
                <span class="info-box-text">
            <div class="row">
                 <div class="col-md-4">
                     <h5>2.  Galería de fotos del proyecto</h5>
                </div>
                 <div class="col-md-2">
                         @if(Auth::check() && $you_have_permission == true)
                         @if(isset($fotos) && count($fotos)>0)
                             <a href="{!! url('image_upload?a='.$costProyecto->id) !!}"
                                class='btn btn-info btn-xs'><i
                                         class="fa fa-eye" aria-hidden="true"></i></a>
                         @else
                             <a href="{!! url('image_upload?a='.$costProyecto->id) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @endif

                     @else
                         <a href="#" class='btn btn-default btn-xs' disabled><i
                                     class="fa fa-eye-slash" aria-hidden="true"></i></a>

                     @endif

                 </div>

            </div>

        </span>
            </div>
        </div>
        <br>

    </div>
    <!-- /.box-body -->

</div>
<br>
<div class="box box-danger">
    <div class="box-header" style="background-color: rgba(200,24,35,0.2)">
        <b><h3 class="box-title"><i class="fa fa-pencil" aria-hidden="true"></i> Proceso de Diseño
            </h3></b>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body " style="background-color: #f7f7f7;">
        <br>
        <div class="info-box">
            <span class="info-box-icon bg-yellow-gradient"><i class="fa fa-pencil-square-o"></i></span>
            <div class="info-box-content">
        <span class="info-box-number">
            <div class="col-xs-3">
                b.  Preparación del Proyecto
            </div><!-- b.  Preparación del Proyecto -->
            <div class="col-xs-2">
                @php($af=\App\Models\cost_proyecto_dis_plan::dis_plan_t($costProyecto->id))
                @if(count($af)==0)
                    @if(Auth::check() && $you_have_permission == true)
                        <a href="{!! route('costProyectoDisPlans.create', ['cost'=>$costProyecto->id]) !!}"
                           class='btn btn-success btn-xs'><i class="fa fa-file-text"
                                                             aria-hidden="true"></i></a>
                    @else
                        <a href="#" class='btn btn-default btn-xs' disabled><i
                                    class="fa fa-eye-slash" aria-hidden="true"></i></a>
                    @endif
                @else
                    {!! Form::open(['route' => ['costProyectoDisPlans.destroy', $af[0]], 'method' => 'delete']) !!}
                    <a href="{!! route('costProyectoDisPlans.show', [$af[0]]) !!}"
                       class='btn btn-info btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                        <a href="{!! route('costProyectoDisPlans.edit', [$af[0]]) !!}"
                           class='btn btn-warning btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                    {!! Form::close() !!}
                @endif
            </div>
        </span>
                <span class="info-box-text">
            <div class="row">
                 <div class="col-md-4">
                     <h5>1.  Fuente de Financiamiento para el estudio <br> de factibilidad,
                         planificación y diseño <br>(CDP y CDF)</h5>
                </div>
                 <div class="col-md-2">
                     @php($af=\App\Models\cost_acuerdo_financiamiento::acuerdo_f($costProyecto->id,2))
                     @if(count($af)==0)
                         @if(Auth::check() && $you_have_permission == true )
                             <a href="{!! route('costAcuerdoFinanciamientos.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2,'tipo'=>1]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>

                         @endif
                     @else
                         {!! Form::open(['route' => ['costAcuerdoFinanciamientos.destroy', $af[0]], 'method' => 'delete']) !!}
                         <a href="{!! route('costAcuerdoFinanciamientos.show', [$af[0]]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costAcuerdoFinanciamientos.edit', [$af[0]]) !!}"
                                class='btn btn-warning btn-xs'><i
                                         class="glyphicon glyphicon-edit"></i></a>
                             {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Esta seguro(a) de borrar este registro?')"]) !!}
                         @endif
                         {!! Form::close() !!}
                     @endif
                 </div>
                <div class="col-md-4">
                    <h5> 5.  Documentos </h5>
                </div>
                 <div class="col-md-2">
                    @php($f= \App\Models\documento::documento_t($costProyecto->id,2))
                     @if(count($f)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('documentos.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('documentos.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>
            </div>
            <div class="row">
                 <div class="col-md-4">
                     <h5>  2. Información del Contrato de diseño </h5>
                </div>
                 <div class="col-md-2">

                         @php($ic=\App\Models\cost_info_contrato::info_c($costProyecto->id,2))
                     @if(count($ic)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costInfoContratos.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2,'tipo'=>1]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         {!! Form::open(['route' => ['costInfoContratos.destroy', $ic[0]], 'method' => 'delete']) !!}
                         <a href="{!! route('costInfoContratos.show', [$ic[0]]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                         @if(Auth::check())
                             <a href="{!! route('costInfoContratos.edit', [$ic[0]]) !!}"
                                class='btn btn-warning btn-xs'><i
                                         class="glyphicon glyphicon-edit"></i></a>
                             {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                         @endif
                         {!! Form::close() !!}
                     @endif

                 </div>
                  <div class="col-md-4">
                      <h5> 6.  Pagos Efectuados</h5>
                </div>
                 <div class="col-md-2">

                         @php($pe=\App\Models\cost_pago_efectuado::pago_e($costProyecto->id,2))

                     @if(count($pe)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costPagoEfectuados.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif
                 </div>

            </div>
            <div class="row">
                 <div class="col-md-4">
                     <h5> 3.  Actas </h5>
                </div>
                 <div class="col-md-2">
                    @php($a=\App\Models\cost_acta::acta_t($costProyecto->id,2))
                     @if(count($a)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costActas.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('costActas.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif
                 </div>
                 <div class="col-md-4">
                     <h5> 7.  Ampliación del Plazo de Ejecución </h5>
                </div>
                 <div class="col-md-2">

                         @php($amp=\App\Models\cost_ampliacion::ampliacion_p($costProyecto->id,2))
                     @if(count($amp)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costAmpliacions.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('costAmpliacions.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>
            </div>
            <div class="row">
                  <div class="col-md-4">
                    <h5> 4.  Fianzas </h5>
                </div>
                 <div class="col-md-2">
                    @php($f= \App\Models\cost_fianza::fianza_p($costProyecto->id,2))
                     @if(count($f)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costFianzas.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('costFianzas.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>2]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>
                 <div class="col-md-4">
                     <h5> 8.  Oferentes del diseño </h5>
                </div>
                 <div class="col-md-2">
                    @php($a=\App\Models\cost_oferente::oferente_p($costProyecto->id,2))
                     @if(count($a)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costOferentes.create', ['id_proyecto'=>$costProyecto->id, 'id_fase'=>2]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('costOferentes.index', ['id_proyecto'=>$costProyecto->id, 'id_fase'=>2]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif
                 </div>
            </div>
        </span>
            </div>
        </div>
        <br>

    </div>
    <!-- /.box-body -->

</div>
<br>
<div class="box box-danger">
    <div class="box-header" style="background-color: rgba(200,24,35,0.2)">
        <b><h3 class="box-title"><i class="fa fa-cogs" aria-hidden="true"></i> Proceso de Ejecución
            </h3></b>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body " style="background-color: #f7f7f7;">
        <br>
        <div class="info-box">
            <span class="info-box-icon bg-teal-gradient"><i class="fa fa-cubes"></i></span>
            <div class="info-box-content">
        <span class="info-box-number">
            <div class="col-md-3">
                c. Información del Contrato
            </div> <!--  c. Información del Contrato -->
            <div class="col-md-2">

                    @php($ic=\App\Models\cost_info_contrato::info_c($costProyecto->id,3))
                @if(count($ic)==0)
                    @if(Auth::check() && $you_have_permission == true)
                        <a href="{!! route('costInfoContratos.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>3,'tipo'=>2]) !!}"
                           class='btn btn-success btn-xs'><i
                                    class="fa fa-file-text" aria-hidden="true"></i></a>
                    @else
                        <a href="#" class='btn btn-default btn-xs' disabled><i
                                    class="fa fa-eye-slash" aria-hidden="true"></i></a>
                    @endif
                @else
                    {!! Form::open(['route' => ['costInfoContratos.destroy', $ic[0]], 'method' => 'delete']) !!}
                    <a href="{!! route('costInfoContratos.show', [$ic[0]]) !!}"
                       class='btn btn-info btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check() && $you_have_permission == true)
                        <a href="{!! route('costInfoContratos.edit', [$ic[0]]) !!}"
                           class='btn btn-warning btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                    {!! Form::close() !!}

                @endif

                     </div>
        </span>
                <span class="info-box-text ">
            <div class="row">
                 <div class="col-md-4">
                     <h5>  1.  Acuerdos de Financiamiento </h5>
                </div>
                 <div class="col-md-2">

                         @php($af=\App\Models\cost_acuerdo_financiamiento::acuerdo_f($costProyecto->id,3))
                     @if(count($af)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costAcuerdoFinanciamientos.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>3,'tipo'=>2]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         {!! Form::open(['route' => ['costAcuerdoFinanciamientos.destroy', $af[0]], 'method' => 'delete']) !!}
                         <a href="{!! route('costAcuerdoFinanciamientos.show', [$af[0]]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costAcuerdoFinanciamientos.edit', [$af[0]]) !!}"
                                class='btn btn-warning btn-xs'><i
                                         class="glyphicon glyphicon-edit"></i></a>
                             {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                         @endif
                         {!! Form::close() !!}
                     @endif

                 </div>
                 <div class="col-md-4">
                     <h5>  4.  Pagos Efectuados </h5>
                </div>
                 <div class="col-md-2">

                         @php($pe=\App\Models\cost_pago_efectuado::pago_e($costProyecto->id,3))

                     @if(count($pe)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costPagoEfectuados.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>3]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif

                     @else
                         <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>3]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>

            </div>
             <div class="row">
                <div class="col-md-4">
                    <h5>  2.  Actas </h5>
                </div>
                 <div class="col-md-2">

                         @php($a=\App\Models\cost_acta::acta_t($costProyecto->id,3))

                     @if(count($a)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costActas.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>3]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('costActas.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>3]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>

                <div class="col-md-4">
                    <h5>  5.  Ampliación del Plazo de Ejecución </h5>
                </div>
                 <div class="col-md-2">

                         @php($amp=\App\Models\cost_ampliacion::ampliacion_p($costProyecto->id,3))
                     @if(count($amp)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costAmpliacions.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>3]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('costAmpliacions.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>3]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>
            </div>
             <div class="row">
                 <div class="col-md-4">
                     <h5> 3.  Fianzas </h5>
                </div>
                 <div class="col-md-2">

                         @php($f= \App\Models\cost_fianza::fianza_p($costProyecto->id,3))
                     @if(count($f)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costFianzas.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>3]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('costFianzas.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>3]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>
                 <div class="col-md-4">
                     <h5> 6.  Oferentes de la ejecución </h5>
                </div>
                 <div class="col-md-2">
                    @php($a=\App\Models\cost_oferente::oferente_p($costProyecto->id,3))
                     @if(count($a)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costOferentes.create', ['id_proyecto'=>$costProyecto->id, 'id_fase'=>3]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('costOferentes.index', ['id_proyecto'=>$costProyecto->id, 'id_fase'=>3]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif
                 </div>
            </div>
        </span>
            </div>
        </div>
        <div class="info-box">
            <span class="info-box-icon bg-green-gradient"><i class="fa fa-cogs"></i></span>
            <div class="info-box-content">
        <span class="info-box-number">
            <div class="col-md-3">
                      d. Información de la Ejecución o Implementación

            </div> <!--   d. Información de la Ejecución o Implementación -->
            <div class="col-md-2">

         </div>
        </span>
                <span class="info-box-text ">
             <div class="row">
                <div class="col-md-4">
                    <h5> 1.  Actas </h5>
                </div>
                 <div class="col-md-2">

                         @php($a=\App\Models\cost_acta::acta_t($costProyecto->id,4))

                     @if(count($a)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costActas.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>4]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif

                     @else
                         <a href="{!! route('costActas.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>4]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>
                 <div class="col-md-4">
                    <h5> 4. Estimaciones aprobadas </h5>
                </div>
                 <div class="col-md-2">

                     @php($a=\App\Models\cost_estimacion::estimacion_p($costProyecto->id,1))

                     @if(count($a)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costEstimacions.create', ['id_proyecto'=>$costProyecto->id,'tipo_estimacion'=>1]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif

                     @else
                         <a href="{!! route('costEstimacions.index', ['id_proyecto'=>$costProyecto->id,'tipo_estimacion'=>1]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>
             </div>
             <div class="row">
                <div class="col-md-4">
                    <h5> 2.  Fianzas </h5>
                </div>
                 <div class="col-md-2">

                         @php($f= \App\Models\cost_fianza::fianza_p($costProyecto->id,4))
                     @if(count($f)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costFianzas.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>4]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('costFianzas.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>4]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>
                  <div class="col-md-4">
                    <h5> 5. Estimaciones pagadas </h5>
                </div>
                 <div class="col-md-2">

                     @php($a=\App\Models\cost_estimacion::estimacion_p($costProyecto->id,2))

                     @if(count($a)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costEstimacions.create', ['id_proyecto'=>$costProyecto->id,'tipo_estimacion'=>2]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif

                     @else
                         <a href="{!! route('costEstimacions.index', ['id_proyecto'=>$costProyecto->id,'tipo_estimacion'=>2]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                     <h5>  3.  Pagos Efectuados </h5>
                </div>
                 <div class="col-md-2">

                         @php($pe=\App\Models\cost_pago_efectuado::pago_e($costProyecto->id,4))

                     @if(count($pe)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('costPagoEfectuados.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>4]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif

                     @else
                         <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>4]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>
                 <div class="col-md-4">
                    <h5> 6.  Documentos </h5>
                </div>
                 <div class="col-md-2">
                    @php($f= \App\Models\documento::documento_t($costProyecto->id,4))
                     @if(count($f)==0)
                         @if(Auth::check() && $you_have_permission == true)
                             <a href="{!! route('documentos.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>4]) !!}"
                                class='btn btn-success btn-xs'><i
                                         class="fa fa-file-text" aria-hidden="true"></i></a>
                         @else
                             <a href="#" class='btn btn-default btn-xs' disabled><i
                                         class="fa fa-eye-slash" aria-hidden="true"></i></a>
                         @endif
                     @else
                         <a href="{!! route('documentos.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>4]) !!}"
                            class='btn btn-info btn-xs'><i
                                     class="glyphicon glyphicon-eye-open"></i></a>
                     @endif

                 </div>

            </div>
        </span>
            </div>
        </div>
        <div class="info-box">
            <span class="info-box-icon bg-maroon-gradient"><i class="fa fa-check-square"></i></span>
            <div class="info-box-content">
        <span class="info-box-number">
            <div class="col-md-3">
                      e. Liquidación del Proyecto
            </div><!--    e. Liquidación del Proyecto -->
            <div class="col-md-2">
             @php($ic=\App\Models\cost_liquidacion::info_c($costProyecto->id,5))
                @if(count($ic)==0)
                    @if(Auth::check() && $you_have_permission == true)
                        <a href="{!! route('costLiquidacions.create', ['id_proyecto'=>$costProyecto->id]) !!}"
                           class='btn btn-success btn-xs'><i
                                    class="fa fa-file-text" aria-hidden="true"></i></a>
                    @else
                        <a href="#" class='btn btn-default btn-xs' disabled><i
                                    class="fa fa-eye-slash" aria-hidden="true"></i></a>
                    @endif
                @else
                    {!! Form::open(['route' => ['costLiquidacions.destroy', $ic[0]], 'method' => 'delete']) !!}
                    <a href="{!! route('costLiquidacions.show', [$ic[0]]) !!}"
                       class='btn btn-info btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                        <a href="{!! route('costLiquidacions.edit', [$ic[0]]) !!}"
                           class='btn btn-warning btn-xs'><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                    {!! Form::close() !!}
                @endif

            </div>
        </span>
                <span class="info-box-text ">
                <div class="row">
                    <div class="col-md-4">
                        <h5> 1.  Actas </h5>
                    </div>
                    <div class="col-md-2">

                    @php($a=\App\Models\cost_acta::acta_t($costProyecto->id,5))

                        @if(count($a)==0)
                            @if(Auth::check() && $you_have_permission == true)
                                <a href="{!! route('costActas.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>5]) !!}"
                                   class='btn btn-success btn-xs'><i
                                            class="fa fa-file-text" aria-hidden="true"></i></a>
                            @else
                                <a href="#" class='btn btn-default btn-xs' disabled><i
                                            class="fa fa-eye-slash" aria-hidden="true"></i></a>
                            @endif

                        @else
                            <a href="{!! route('costActas.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>5]) !!}"
                               class='btn btn-info btn-xs'><i
                                        class="glyphicon glyphicon-eye-open"></i></a>
                        @endif

                    </div>
                    <div class="col-md-4">
                        <h5>  4.  Modificaciones al plazo de la obra </h5>
                    </div>
                    <div class="col-md-2">
                        @php($f= \App\Models\cost_liquidacion_tiempo::tiempo($costProyecto->id))
                        @if(count($f)==0)
                            @if(Auth::check() && $you_have_permission == true)
                                <a href="{!! route('costLiquidacionTiempos.create', ['id_proyecto'=>$costProyecto->id]) !!}"
                                   class='btn btn-success btn-xs'><i
                                            class="fa fa-file-text" aria-hidden="true"></i></a>
                            @else
                                <a href="#" class='btn btn-default btn-xs' disabled><i
                                            class="fa fa-eye-slash" aria-hidden="true"></i></a>
                            @endif
                        @else
                            <a href="{!! route('costLiquidacionTiempos.index', ['id_proyecto'=>$costProyecto->id]) !!}"
                               class='btn btn-info btn-xs'><i
                                        class="glyphicon glyphicon-eye-open"></i></a>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                         <h5> 2.  Fianzas </h5>
                    </div>
                    <div class="col-md-2">
                    @php($f= \App\Models\cost_fianza::fianza_p($costProyecto->id,5))
                        @if(count($f)==0)
                            @if(Auth::check() && $you_have_permission == true)
                                <a href="{!! route('costFianzas.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>5]) !!}"
                                   class='btn btn-success btn-xs'><i
                                            class="fa fa-file-text" aria-hidden="true"></i></a>
                            @else
                                <a href="#" class='btn btn-default btn-xs' disabled><i
                                            class="fa fa-eye-slash" aria-hidden="true"></i></a>
                            @endif
                        @else
                            <a href="{!! route('costFianzas.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>5]) !!}"
                               class='btn btn-info btn-xs'><i
                                        class="glyphicon glyphicon-eye-open"></i></a>
                        @endif
                    </div>

                    <div class="col-md-4">
                     <h5> 5.  Modificaciones al alcance de la obra</h5>
                    </div>
                    <div class="col-md-2">
                    @php($amp=\App\Models\cost_liquidacion_alcance::alcance($costProyecto->id))
                        @if(count($amp)==0)
                            @if(Auth::check() && $you_have_permission == true)
                                <a href="{!! route('costLiquidacionAlcances.create', ['id_proyecto'=>$costProyecto->id]) !!}"
                                   class='btn btn-success btn-xs'><i
                                            class="fa fa-file-text" aria-hidden="true"></i></a>
                            @else
                                <a href="#" class='btn btn-default btn-xs' disabled><i
                                            class="fa fa-eye-slash" aria-hidden="true"></i></a>
                            @endif
                        @else
                            <a href="{!! route('costLiquidacionAlcances.index', ['id_proyecto'=>$costProyecto->id]) !!}"
                               class='btn btn-info btn-xs'><i
                                        class="glyphicon glyphicon-eye-open"></i></a>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                         <h5> 3.  Modificaciones al monto </h5>
                    </div>
                     <div class="col-md-2">
                             @php($amp=\App\Models\cost_liquidacion_monto::monto($costProyecto->id))
                         @if(count($amp)==0)
                             @if(Auth::check() && $you_have_permission == true)
                                 <a href="{!! route('costLiquidacionMontos.create', ['id_proyecto'=>$costProyecto->id]) !!}"
                                    class='btn btn-success btn-xs'><i
                                             class="fa fa-file-text" aria-hidden="true"></i></a>
                             @else
                                 <a href="#" class='btn btn-default btn-xs' disabled><i
                                             class="fa fa-eye-slash" aria-hidden="true"></i></a>
                             @endif
                         @else
                             <a href="{!! route('costLiquidacionMontos.index', ['id_proyecto'=>$costProyecto->id]) !!}"
                                class='btn btn-info btn-xs'><i
                                         class="glyphicon glyphicon-eye-open"></i></a>
                         @endif
                     </div>
                </div>
        </span>
            </div>
        </div>
        <br>
    </div>
    <!-- /.box-body -->

</div>
<br>
<div class="box box-danger">
    <div class="box-header" style="background-color: rgba(200,24,35,0.2)">
        <b><h3 class="box-title"><i class="fa fa-binoculars" aria-hidden="true"></i> Proceso de Supervisión
            </h3></b>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body " style="background-color: #f7f7f7;">
        <br>
        <div class="info-box">
            <span class="info-box-icon bg-purple-gradient"><i class="fa fa-binoculars"></i></span>
            <div class="info-box-content">
        <span class="info-box-number">
             <div class="col-md-3">
                    f. Información del Contrato de la supervisión
             </div> <!--    f. Información del Contrato de la supervisión -->
             <div class="col-md-2">

                         @php($ic=\App\Models\cost_info_contrato::info_c($costProyecto->id,6))
                 @if(count($ic)==0)
                     @if(Auth::check() && $you_have_permission == true)
                         <a href="{!! route('costInfoContratos.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6, 'tipo'=>3]) !!}"
                            class='btn btn-success btn-xs'><i
                                     class="fa fa-file-text" aria-hidden="true"></i></a>
                     @else
                         <a href="#" class='btn btn-default btn-xs' disabled><i
                                     class="fa fa-eye-slash" aria-hidden="true"></i></a>
                     @endif
                 @else
                     {!! Form::open(['route' => ['costInfoContratos.destroy', $ic[0]], 'method' => 'delete']) !!}
                     <a href="{!! route('costInfoContratos.show', [$ic[0]]) !!}"
                        class='btn btn-info btn-xs'><i
                                 class="glyphicon glyphicon-eye-open"></i></a>
                     @if(Auth::check())
                         <a href="{!! route('costInfoContratos.edit', [$ic[0]]) !!}"
                            class='btn btn-warning btn-xs'><i
                                     class="glyphicon glyphicon-edit"></i></a>
                         {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                     @endif
                     {!! Form::close() !!}
                 @endif
                     </div>
        </span>
                <span class="info-box-text ">
                        <div class="row">
                             <div class="col-md-4">
                                 <h5>1.  Acuerdos de Financiamiento </h5>
                            </div>
                             <div class="col-md-2">

                                     @php($af=\App\Models\cost_acuerdo_financiamiento::acuerdo_f($costProyecto->id,6))
                                 @if(count($af)==0)
                                     @if(Auth::check() && $you_have_permission == true)
                                         <a href="{!! route('costAcuerdoFinanciamientos.create', ['id_proyecto'=>$costProyecto->id, 'id_fase'=>6, 'tipo'=>3]) !!}"
                                            class='btn btn-success btn-xs'><i
                                                     class="fa fa-file-text" aria-hidden="true"></i></a>
                                     @else
                                         <a href="#" class='btn btn-default btn-xs' disabled><i
                                                     class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                     @endif
                                 @else
                                     {!! Form::open(['route' => ['costAcuerdoFinanciamientos.destroy', $af[0]], 'method' => 'delete']) !!}
                                     <a href="{!! route('costAcuerdoFinanciamientos.show', [$af[0]]) !!}"
                                        class='btn btn-info btn-xs'><i
                                                 class="glyphicon glyphicon-eye-open"></i></a>
                                     @if(Auth::check() && $you_have_permission == true)
                                         <a href="{!! route('costAcuerdoFinanciamientos.edit', [$af[0]]) !!}"
                                            class='btn btn-warning btn-xs'><i
                                                     class="glyphicon glyphicon-edit"></i></a>
                                         {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                     @endif
                                     {!! Form::close() !!}


                                 @endif
                             </div>
                             <div class="col-md-4">
                                 <h5>   4.  Pagos Efectuados </h5>
                            </div>
                             <div class="col-md-2">

                                     @php($pe=\App\Models\cost_pago_efectuado::pago_e($costProyecto->id,6))

                                 @if(count($pe)==0)
                                     @if(Auth::check()  && $you_have_permission == true)
                                         <a href="{!! route('costPagoEfectuados.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6]) !!}"
                                            class='btn btn-success btn-xs'><i
                                                     class="fa fa-file-text" aria-hidden="true"></i></a>
                                     @else
                                         <a href="#" class='btn btn-default btn-xs' disabled><i
                                                     class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                     @endif
                                 @else
                                     <a href="{!! route('costPagoEfectuados.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6]) !!}"
                                        class='btn btn-info btn-xs'><i
                                                 class="glyphicon glyphicon-eye-open"></i></a>
                                 @endif

                             </div>
                        </div>
                         <div class="row">
                        <div class="col-md-4">
                            <h5>   2.  Actas </h5>
                            </div>
                             <div class="col-md-2">

                                     @php($a=\App\Models\cost_acta::acta_t($costProyecto->id,6))

                                 @if(count($a)==0)
                                     @if(Auth::check()  && $you_have_permission == true)
                                         <a href="{!! route('costActas.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6]) !!}"
                                            class='btn btn-success btn-xs'><i
                                                     class="fa fa-file-text" aria-hidden="true"></i></a>
                                     @else
                                         <a href="#" class='btn btn-default btn-xs' disabled><i
                                                     class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                     @endif
                                 @else
                                     <a href="{!! route('costActas.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6]) !!}"
                                        class='btn btn-info btn-xs'><i
                                                 class="glyphicon glyphicon-eye-open"></i></a>
                                 @endif

                             </div>
                            <div class="col-md-4">
                                <h5>  5.  Ampliación del Plazo de Ejecución </h5>
                            </div>
                             <div class="col-md-2">

                                     @php($amp=\App\Models\cost_ampliacion::ampliacion_p($costProyecto->id,6))
                                 @if(count($amp)==0)
                                     @if(Auth::check()  && $you_have_permission == true)
                                         <a href="{!! route('costAmpliacions.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6]) !!}"
                                            class='btn btn-success btn-xs'><i
                                                     class="fa fa-file-text" aria-hidden="true"></i></a>
                                     @else
                                         <a href="#" class='btn btn-default btn-xs' disabled><i
                                                     class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                     @endif
                                 @else
                                     <a href="{!! route('costAmpliacions.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6]) !!}"
                                        class='btn btn-info btn-xs'><i
                                                 class="glyphicon glyphicon-eye-open"></i></a>
                                 @endif

                             </div>

                        </div>
                         <div class="row">
                              <div class="col-md-4">
                                  <h5>  3.  Fianzas </h5>
                            </div>
                             <div class="col-md-2">

                                     @php($f= \App\Models\cost_fianza::fianza_p($costProyecto->id,6))
                                 @if(count($f)==0)
                                     @if(Auth::check()  && $you_have_permission == true)
                                         <a href="{!! route('costFianzas.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6]) !!}"
                                            class='btn btn-success btn-xs'><i
                                                     class="fa fa-file-text" aria-hidden="true"></i></a>
                                     @else
                                         <a href="#" class='btn btn-default btn-xs' disabled><i
                                                     class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                     @endif
                                 @else
                                     <a href="{!! route('costFianzas.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6]) !!}"
                                        class='btn btn-info btn-xs'><i
                                                 class="glyphicon glyphicon-eye-open"></i></a>
                                 @endif

                             </div>
                             <div class="col-md-4">
                                 <h5> 6.  Oferentes de la supervisión</h5>
                            </div>
                             <div class="col-md-2">
                                @php($a=\App\Models\cost_oferente::oferente_p($costProyecto->id,6))
                                 @if(count($a)==0)
                                     @if(Auth::check()  && $you_have_permission == true)
                                         <a href="{!! route('costOferentes.create', ['id_proyecto'=>$costProyecto->id, 'id_fase'=>6]) !!}"
                                            class='btn btn-success btn-xs'><i
                                                     class="fa fa-file-text" aria-hidden="true"></i></a>
                                     @else
                                         <a href="#" class='btn btn-default btn-xs' disabled><i
                                                     class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                     @endif
                                 @else
                                     <a href="{!! route('costOferentes.index', ['id_proyecto'=>$costProyecto->id, 'id_fase'=>6]) !!}"
                                        class='btn btn-info btn-xs'><i
                                                 class="glyphicon glyphicon-eye-open"></i></a>
                                 @endif
                             </div>
                        </div>
                        <div class="row">
                             <div class="col-md-4">
                                <h5>  </h5>
                            </div>
                             <div class="col-md-2">
                             </div>
                             <div class="col-md-4">
                                <h5> 7.  Documentos </h5>
                            </div>
                             <div class="col-md-2">
                                @php($f= \App\Models\documento::documento_t($costProyecto->id,6))
                                 @if(count($f)==0)
                                     @if(Auth::check()  && $you_have_permission == true)
                                         <a href="{!! route('documentos.create', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6]) !!}"
                                            class='btn btn-success btn-xs'><i
                                                     class="fa fa-file-text" aria-hidden="true"></i></a>
                                     @else
                                         <a href="#" class='btn btn-default btn-xs' disabled><i
                                                     class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                     @endif
                                 @else
                                     <a href="{!! route('documentos.index', ['id_proyecto'=>$costProyecto->id,'id_fase'=>6]) !!}"
                                        class='btn btn-info btn-xs'><i
                                                 class="glyphicon glyphicon-eye-open"></i></a>
                                 @endif

                             </div>
                        </div>
                     </span>
            </div>
        </div>
        <br>
    </div>

</div>





