<div class="col-sm-12">
    <div class="box box-info">
        <div class="box-body">
            <div class="row">
                <div class="form-group col-sm-6">
                    {!! Form::label('entidad_adquisicion', 'Entidad de adquisición (OC4IDS):') !!}
                    {!! Form::select('entidad_adquisicion', \App\Models\cost_proyecto::listado_entidad_adquisicion(),$costProyecto->id_entidad_adquisicion,
                    ['class' => 'form-control id_entidad_participante','name'=>'id_entidad_adquisicion']) !!}
                </div>
                <div class="form-group col-sm-6">
                        {!! Form::label('rol', 'Rol Entidad Adquisición (OC4IDS):') !!}
                        {!! Form::select('rol', \App\Models\cost_proyecto::listado_roles_participantes(),$seleccionados_roles,
                        ['class' => 'form-control js-example-basic-multiple','name'=>'roles[]','multiple'=>'multiple','required']) !!}
                </div>

                <!-- Entidad Adquisicion Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('entidad_admin_contrato', 'Entidad administradora del contrato:') !!}
                    {!! Form::text('entidad_admin_contrato', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Sector Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('sector', 'Sector:') !!}
                    {!! Form::text('sector', null, ['class' => 'form-control']) !!}
                </div>

                <!-- Sub Sector Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('sub_sector', 'Sub sector:') !!}
                    {!! Form::text('sub_sector', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group col-sm-6">
                    {!! Form::label('estado_oc', 'Estado actual de proyecto (OC4IDS):') !!}
                    {!! Form::select('estado_oc', \App\Models\cat_item::estado_ejecusion(85,$costProyecto->id_proyecto),$costProyecto->estado_oc,['class' => 'form-control','style'=>'width:100%','id'=>'estado_oc']) !!}
                </div>

                @include('chivos.catalogo', ['chivo_control' => 'sector_oc'
                           ,'chivo_id_cat'=>81
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Sector (OC4IDS):'])

                @include('chivos.catalogo', ['chivo_control' => 'tipo_oc'
                                          ,'chivo_id_cat'=>82
                                          , 'chivo_default'=>null
                                          ,'chivo_texto'=>'Tipo de proyecto (OC4IDS):'])

                <div class="form-group col-sm-6">
                    {!! Form::label('programa_multi_d', 'Programa multianual:') !!}
                    {!! Form::text('programa_multi_d', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('plan_adqui_d', 'Plan anual de adquisiciones:') !!}
                    {!! Form::text('plan_adqui_d', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('contacto_entidad', 'Detalles de contacto en entidad de adquisición y rol:') !!}
                    {!! Form::text('contacto_entidad', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('nombre_proyecto', 'Nombre proyecto:') !!}
                    {!! Form::text('nombre_proyecto', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Localizacion Depto D Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('detalle_responsable', 'Datos de contacto con entidad de adquisiciones:') !!}
                    {!! Form::text('detalle_responsable', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-body">
            <div class="row">
                <div class="form-group col-sm-6">
                    {!! Form::label('f_r_proactiva', 'Fecha de revisión proactiva:') !!}
                    @if($edit = 0)
                        {!! Form::text('f_r_proactiva', null, ['class' => 'form-control datepicker ','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        @if($costProyecto->f_r_proactiva != '0000-00-00')
                            {!! Form::text('f_r_proactiva', null, ['class' => 'form-control datepicker ','data-value'=>$costProyecto->f_r_proactiva]) !!}
                        @else
                            {!! Form::text('f_r_proactiva', null, ['class' => 'form-control datepicker ','data-value'=>'']) !!}
                        @endif
                    @endif

                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('f_r_reactiva', 'Fecha de revisión reactiva:') !!}
                    @if($edit = 0)
                        {!! Form::text('f_r_reactiva', null, ['class' => 'form-control datepicker ','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        @if($costProyecto->f_r_reactiva != '0000-00-00')
                            {!! Form::text('f_r_reactiva', null, ['class' => 'form-control datepicker ','data-value'=>$costProyecto->f_r_reactiva]) !!}
                        @else
                            {!! Form::text('f_r_reactiva', null, ['class' => 'form-control datepicker ','data-value'=>'']) !!}
                        @endif
                    @endif

                </div>

                <div class="form-group col-sm-6">
                    {!! Form::label('f_finalizacion', 'Fecha de finalización (OC4IDS):') !!}
                    @if($edit = 0)
                        {!! Form::text('f_finalizacion', null, ['class' => 'form-control datepicker ','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        @if($costProyecto->f_finalizacion != '0000-00-00')
                            {!! Form::text('f_finalizacion', null, ['class' => 'form-control datepicker ','data-value'=>$costProyecto->f_finalizacion]) !!}
                        @else
                            {!! Form::text('f_finalizacion', null, ['class' => 'form-control datepicker ','data-value'=>'']) !!}
                        @endif
                    @endif

                </div>
            </div>
        </div>
    </div>

</div>

<div class="col-sm-12">
    <div class="box box-warning">
        <div class="box-body">
            <div class="row">
                <!-- Localizacion Field -->
                <div class="form-group col-sm-12">
                    {!! Form::label('localizacion', 'Localización (OC4IDS):') !!}
                    {!! Form::text('localizacion', null, ['class' => 'form-control']) !!}
                </div>

                @include('chivos.geo2', ['chivo_control' => 'localizacion_muni', 'chivo_default'=>$costProyecto->localizacion_muni>0?$costProyecto->localizacion_muni:32,
                          "chivo_depto"=>$costProyecto->localizacion_depto>0?$costProyecto->localizacion_depto:1,
                          'label'=>'localizacion'])



                <div class="col-md-6" style="color: #0d6aad">Departamento (OC4IDS): {{$costProyecto->localizacion_depto_d}}</div>
                <div class="col-md-6" style="color: #0d6aad">Municipio (OC4IDS): {{$costProyecto->localizacion_muni_d}}</div>
                <div class="form-group col-sm-6">
                    {!! Form::label('pais', 'País (OC4IDS):') !!}
                    {!! Form::text('pais', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('coordenadas', 'Coordenadas (OC4IDS):') !!}
                    {!! Form::text('coordenadas', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('codigo_postal', 'Código Postal (OC4IDS):') !!}
                    {!! Form::text('codigo_postal', null, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="box box-info">
        <div class="box-body">
            <div class="row">
                <!-- Snip Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('snip', 'SNIP:') !!}
                    {!! Form::number('snip', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('snip_detalle', 'SNIP detalle:') !!}
                    {!! Form::text('snip_detalle', null, ['class' => 'form-control']) !!}
                </div>
                <!-- Proposito Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('proposito', 'Propósito:') !!}
                    {!! Form::textarea('proposito', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('analisis_alternativas', 'Análisis alternativas:') !!}
                    {!! Form::textarea('analisis_alternativas', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('descripcion', 'Descripción (OC4IDS):') !!}
                    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>
                <div class="form-group col-sm-6 col-lg-6">
                    {!! Form::label('beneficiario_total', 'Número de Beneficiarios:') !!}
                    {!! Form::number('beneficiario_total', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('presupuesto_proyecto_multi_d', 'Presupuesto de proyecto multianual:') !!}
                    {!! Form::text('presupuesto_proyecto_multi_d', null, ['class' => 'form-control']) !!}
                </div>
                <!-- F Aprobacion Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('f_aprobacion', 'Fecha aprobación (OC4IDS):') !!}
                    @if($edit = 0)
                        {!! Form::text('f_aprobacion', null, ['class' => 'form-control datepicker ','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        {!! Form::text('f_aprobacion', null, ['class' => 'form-control datepicker ','data-value'=>$costProyecto->f_aprobacion]) !!}
                    @endif

                </div>


                <!-- Localizacion Depto D Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('nog', 'NOG (OC4IDS):') !!}
                    {!! Form::text('nog', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('nog_f', 'Fecha NOG:') !!}
                    @if($edit = 0)
                        {!! Form::text('nog_f', null, ['class' => 'form-control datepicker ','data-value'=>\Carbon\Carbon::now()]) !!}
                    @else
                        @if($costProyecto->nog_f != '0000-00-00')
                            {!! Form::text('nog_f',$costProyecto->nog_f, ['class' => 'form-control datepicker ','data-value'=>$costProyecto->nog_f]) !!}
                        @else
                            {!! Form::text('nog_f',$costProyecto->nog_f, ['class' => 'form-control datepicker ','data-value'=>$costProyecto->nog_f]) !!}
                        @endif
                    @endif

                </div>
                <!-- Localizacion Depto D Field -->
                <div class="form-group col-sm-6">
                    {!! Form::label('avance_fisico', 'Avance Físico a la fecha:') !!}
                    {!! Form::text('avance_fisico', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group col-sm-6">
                    {!! Form::label('avance_financiero', 'Avance Financiero a la fecha:') !!}
                    {!! Form::text('avance_financiero', null, ['class' => 'form-control']) !!}
                </div>

                @include('chivos.catalogo', ['chivo_control' => 'estado_actual_proyecto'
                           ,'chivo_id_cat'=>73
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Estado actual de proyecto:'])


                <!-- Resumen Field -->
                <div class="form-group col-sm-12 col-lg-12">
                    {!! Form::label('resumen', 'Resumen (OC4IDS):') !!}
                    {!! Form::textarea('resumen', null, ['class' => 'form-control', 'rows' => 2, 'cols' => 40]) !!}
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12" align="center">
    {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
    <a href="{!! route('costProyectos.show', [$costProyecto->id_proyecto,'a'=>1]) !!}"
       class="btn btn-danger">Cancelar</a>
</div>
@push("head")
    {{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
    <head><meta name="csrf-token" content="{!! csrf_token() !!}"></head>
@endpush
@push('javascript')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $('.js-example-basic-multiple').select2({
            placeholder: 'Selecciona los roles'
        });
        $('.id_entidad_participante').select2({
            placeholder: 'Selecciona los roles'
        });
        $('#estado_oc').select2({
            placeholder: 'Select an option'
        });
    </script>
@endpush