@if($json_final != [])
    <div class="row">
        <div class="panel-body">
            <div class="alert alert-danger alert-dismissible">
                <h4><i class="icon fa fa-warning"></i> Advertencia!</h4>
                Esta página se encuentra en construcción. Los datos públicados pueden variar en base a su fuente.
            </div>
            <div class="btn-group ">
                <button type="button" class="btn btn-default">OC4IDS</button>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                        aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        {{ Form::open(array('url' =>"costProyectos/proyectos_unificados",'method' => 'post','id'=>'json','target'=>'_blank')) }}
                        <input type="hidden" value="{{$json_final}}" name="json_final">
                        <button type="submit">Release 1</button>
                        {!! Form::close() !!}
                    </li>
                </ul>
            </div>
            <div class="table table-responsive">
                <pre>  {{$json_final}} </pre>
            </div>
        </div>
    </div>
@endif