
<div class="box box-solid ">
    <div class="box-header with-border">
        <h3 class="box-title">
            <b style="color: #e53935;">
                <i class="fa fa-cubes" aria-hidden="true"></i> Información por fase del Proyecto
            </b>
        </h3>
    </div>
    <div class="box-body">
        <div class="row">
            @include('chivos.criterio', ['chivo_control' => 'fase_proyecto'
                                        ,'chivo_id_grupo'=>8
                                         ,'chivo_tamanio'=>'col-xs-3'
                                         ,'chivo_default'=>null
                                         ,'chivo_label_color'=>$filtros->fase_proyecto_color
                                         ,'chivo_texto'=>'Fase proyecto'])


            @include('chivos.participante', ['chivo_control' => 'id_participante'
                                       , 'chivo_default'=>$filtros->id_participante
                                       ,'chivo_tamanio'=>'col-xs-3'
                                       ,'chivo_label_color'=>$filtros->id_participante_color
                                       ,'chivo_texto'=>'parties / name:'])


            @include('chivos.catalogo', ['chivo_control' => 'tipo_acta'
                                       ,'chivo_id_cat'=>68
                                       , 'chivo_default'=>null
                                       ,'chivo_tamanio'=>'col-xs-3'
                                       ,'chivo_label_color'=>$filtros->tipo_acta_color
                                       ,'chivo_texto'=>'Tipo de acta:'])

            @include('chivos.catalogo', ['chivo_control' => 'tipo_fianza'
                                       ,'chivo_id_cat'=>69
                                       , 'chivo_default'=>null
                                       ,'chivo_tamanio'=>'col-xs-3'
                                        ,'chivo_label_color'=>$filtros->tipo_fianza_color
                                       ,'chivo_texto'=>'Tipo de fianza:'])

            @include('chivos.catalogo', ['chivo_control' => 'tipo_pago_efectuado'
                                       ,'chivo_id_cat'=>70
                                       , 'chivo_default'=>null
                                       ,'chivo_tamanio'=>'col-xs-3'
                                        ,'chivo_label_color'=>$filtros->tipo_pago_efectuado_color
                                       ,'chivo_texto'=>'Tipo de pago efectuado:'])

            @include('chivos.catalogo', ['chivo_control' => 'tipo_ampliacion'
                                       ,'chivo_id_cat'=>71
                                       , 'chivo_default'=>null
                                        ,'chivo_tamanio'=>'col-xs-3'
                                        ,'chivo_label_color'=>$filtros->tipo_ampliacion_color
                                       ,'chivo_texto'=>'Tipo de ampliación plazo de ejecución:'])


            @include('cost_proyectos.busqueda_proyectos_oc4ids._filters_documents_busqueda_proyectos_oc4ids')

        </div>
    </div>
</div>