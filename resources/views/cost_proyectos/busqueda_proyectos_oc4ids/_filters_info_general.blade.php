<div class="box box-solid ">
    <div class="box-header with-border">
        <h3 class="box-title">
            <b style="color: #e53935;">
                <i class="fa fa-indent" aria-hidden="true"></i> Información General del Proyecto
            </b>
        </h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="form-group col-xs-6">
                <label style="color: {{$filtros->title_color}}">Título</label>
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-3">
                <label style="color: {{$filtros->status_color}}">Estado</label>
                {!! Form::select('status',\App\Models\cat_item::estado_ejecusion(85,null),null,['class' => 'form-control','style'=>'width:100%','id'=>'status']) !!}
            </div>
            @include('chivos.catalogo', ['chivo_control' => 'sector'
                                                     ,'chivo_id_cat'=>81
                                                     ,'chivo_default'=>$filtros->sector
                                                     ,'chivo_tamanio'=>'col-xs-3'
                                                     ,'chivo_label_color'=>$filtros->sector_color
                                                     ,'chivo_texto'=>'Sector:'])
            <br/><br/><br/><br/>
            @include('chivos.catalogo', ['chivo_control' => 'type'
                                          ,'chivo_id_cat'=>82
                                          ,'chivo_tamanio'=>'col-xs-3'
                                          ,'chivo_default'=>$filtros->type
                                          ,'chivo_label_color'=>$filtros->type_color
                                          ,'chivo_texto'=>'Tipo de proyecto:'])

            @include('chivos.geo2', ['chivo_control' => 'localizacion_muni',
                                    'chivo_default'=>'',
                                    "chivo_depto"=>''
                                    ,'chivo_tamanio'=>'col-xs-3',
                                    'label'=>'localizacion'])

            <div class="form-group col-xs-3">
                <label style="color: {{$filtros->localizacion_color}}">Dirección</label>
                {!! Form::text('localizacion', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-xs-3">
                <label style="color: {{$filtros->startDate_color}}"> <i class="fa fa-calendar"></i> Día de inicio:</label>
                {!! Form::text('startDate', null, ['class' => 'form-control datepicker ','data-value'=>$filtros->startDate]) !!}
            </div>
            <div class="form-group col-xs-3">
                <label style="color: {{$filtros->endDate_color}}"><i class="fa fa-calendar"></i> Día de fin:</label>
                {!! Form::text('endDate', null, ['class' => 'form-control datepicker ','data-value'=>$filtros->endDate]) !!}
            </div>
        </div>
    </div>
    <!-- /.box-body -->
</div>


@push('javascript')
    <script>
        $('#status').select2({
            placeholder: 'Seleccionar'
        });
    </script>
@endpush