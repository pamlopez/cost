@php($id_fase = isset($id_fase)?$id_fase:1)
@php($name_input_documentType= 'documentType')
@php($name_input_documentTitle= 'documentTitle')
@php($name_input_datePublished= 'datePublished')
@php($name_input_dateModified= 'dateModified'.'_'.$id_fase)

@php($documentType_color= 'documentType_color')
@php($documentTitle_color= 'documentTitle_color')
@php($datePublished_color= 'datePublished_color')
@php($dateModified_color= 'dateModified_color')

<div class="col-md-12">
    <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Documents</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                @include('chivos.tipo_documento_traduccion', ['chivo_control' => 'documentType'
                                       , 'chivo_default'=>$filtros->documentType
                                       ,'chivo_tamanio'=>'col-xs-3'
                                       ,'chivo_label_color'=>$filtros->documentType_color
                                       ,'chivo_texto'=>'Tipo documento:'])

                <div class="form-group col-xs-3">
                    <label style="color: {{$filtros->documentTitle_color}}">Título:</label>
                    {!! Form::text($name_input_documentTitle,null,['class' => 'form-control']) !!}
                </div>
            </div>
            <!-- /.box-body -->
            </div>
    </div>
</div>