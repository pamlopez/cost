<div class="box box-solid ">
    <div class="box-header with-border">
        <h3 class="box-title">
            <b style="color: #e53935;">
                <i class="fa fa-object-group" aria-hidden="true"></i> Componentes del proyecto para su despliegue
            </b>
        </h3>
    </div>
    <div class="box-body">
        <div class="row">
            @include('chivos.criterio', ['chivo_control' => 'fase_proyecto_despliegue'
                                               ,'chivo_id_grupo'=>8
                                                ,'chivo_tamanio'=>'col-xs-3'
                                                ,'chivo_default'=>null
                                                ,'chivo_label_color'=>$filtros->fase_proyecto_despliegue_color
                                                ,'chivo_texto'=>'Fase proyecto despliegue'])

            @include('chivos.criterio', ['chivo_control' => 'componente_proyecto_despliegue'
                                              ,'chivo_id_grupo'=>9
                                               ,'chivo_tamanio'=>'col-xs-3'
                                               ,'chivo_default'=>null
                                               ,'chivo_label_color'=>$filtros->componente_proyecto_despliegue_color
                                               ,'chivo_texto'=>'Componentes proyecto despliegue'])
        </div>
    </div>
</div>