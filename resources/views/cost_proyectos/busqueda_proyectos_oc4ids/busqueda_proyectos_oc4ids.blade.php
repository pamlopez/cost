@extends('layouts.app')
@section('content')
    <div>
        @if(isset($tipo_busqueda) && $tipo_busqueda == 2)
            {{ Form::open(array('url' =>"busqueda_especifica",'method' => 'get','id'=>'formulario')) }}
        @elseif(isset($tipo_busqueda) && $tipo_busqueda == 3)
            {{ Form::open(array('url' =>"download_oc4ids",'method' => 'get','id'=>'formulario')) }}
        @endif

        <h3 class="" style="text-align:center;">
            <b>
                <i class="fa {{$icono}}" aria-hidden="true" style="color:{{$icono_color}}"></i>
                {{$titulo_busqueda}}
            </b>
        </h3>

        @include('cost_proyectos.busqueda_proyectos_oc4ids._filters_info_general')
        @include('cost_proyectos.busqueda_proyectos_oc4ids._filters_fase_proyecto')

        @if(isset($tipo_busqueda) && $tipo_busqueda == 3)
            @include('cost_proyectos.busqueda_proyectos_oc4ids._filters_componentes_proyecto')
        @endif


        <div class="box-footer">
            <div class="form-group col-sm-12" align="center">
                <button class="btn btn-primary" id="submit">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    Buscar
                </button> @if(isset($tipo_busqueda) && $tipo_busqueda == 2)
                    <a href="{!! url('busqueda_especifica') !!}"
                       class="btn btn-default"><i class="fa fa-refresh" aria-hidden="true"></i> Reiniciar
                    </a>
                @elseif(isset($tipo_busqueda) && $tipo_busqueda == 3)
                    <a href="{!! url('download_oc4ids') !!}"
                       class="btn btn-default"><i class="fa fa-refresh" aria-hidden="true"></i> Reiniciar
                    </a>
                @endif
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    @if(isset($tipo_busqueda) && $tipo_busqueda == 2)
        @include('cost_proyectos.busqueda_proyectos_oc4ids._tabla_busqueda_especifica',compact('costProyectos','you_have_permission','you_have_permission_publicador'))
    @elseif(isset($tipo_busqueda) && $tipo_busqueda == 3)
        @include('cost_proyectos.busqueda_proyectos_oc4ids.resultado_json_oc4ids',compact('union_json_proyects'))
    @endif

@endsection

@push("head")
    {{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
    <head>
        <meta name="csrf-token" content="{!! csrf_token() !!}">
    </head>
@endpush
@push('javascript')
    @if (Auth::guest())
        <script src="{{ url('js/dependent-dropdown.js') }}" type="text/javascript"></script>
    @endif

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $('#status').select2({
            placeholder: 'Seleccionar'
        });


        /* var form = document.getElementById("formulario");
         var fase_proyecto = document.getElementById("fase_proyecto").value;
         console.log(fase_proyecto);
         document.getElementById("submit").addEventListener("click", function () {
             if (fase_proyecto == 0){
                 alert('Debe seleccionar una fase del proyecto');
             }else if(fase_proyecto >0){
                 alert(fase_proyecto);
             }

            // form.submit();

         });*/
    </script>


@endpush