<div class="box-body">
    <div class="table-responsive">
        @if($costProyectos != [])
            @include('cost_proyectos.table',compact('costProyectos','you_have_permission','you_have_permission_publicador'))
        @endif

    </div>
</div>
<div class="box-footer">
    @if($costProyectos != [])
        {!! $costProyectos->appends(request()->except('page'))->links() !!}
    @endif
</div>