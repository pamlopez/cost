<br/> <br/>
<div class="col-md-8 col-sm-offset-2">
    <h3>
        <b>
            <i class="fa fa-picture-o" aria-hidden="true"></i>
            Listado de fotografías del proyecto
            <span><b style="color: #605CA8"> Mostrando {{ $contenido->total() }} registros</b></span>
        </b>
    </h3>
    <br/>
    <table class="table table-bordered table-condensed table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Link</th>
            <th>Imagen</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        @foreach($contenido as $id => $val)
            <tr>
                <td>{{  (($contenido->currentPage() -1) * $contenido->perPage())+1+$id }}</td>
                <td>{{$val->link}}</td>
                <td><img src="{{ asset($val->link) }}" alt="" width="auto" height="20%"></td>
                <td>
                    {!! Form::open(['url' => ['eliminar_foto', $val->id_proyecto_foto], 'method' => 'post']) !!}
                    {!! Form::hidden('id_proyecto',$val->id_proyecto, ['class' => 'form-control']) !!}
                    {!! Form::button('<i class="glyphicon glyphicon-trash" data-toggle="tooltip" title="¿Esta seguro que desea eliminar este registro?"  data-placement="top"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Esta seguro que desea eliminar este registro?')"]) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="box-footer">
        {!! $contenido->appends(request()->except('page'))->links() !!}
    </div>
</div>

