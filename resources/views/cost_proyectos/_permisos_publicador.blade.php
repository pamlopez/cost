@if($you_have_permission_publicador == true)
    @if($costProyecto->estado == 0)
        @if($btn_large_name == true)
            <a href="{!! url('publicar_proyecto', [$costProyecto->id]) !!}"
               class='btn btn-danger'
               data-toggle="tooltip" data-placement="top" title="PUBLICAR proyecto"
               onclick="return confirm('Esta seguro que desea PUBLICAR a este proyecto?')">
                <i class="fa fa-arrow-up" aria-hidden="true"></i> Publicar proyecto
            </a>
        @else
            <a href="{!! url('publicar_proyecto', [$costProyecto->id]) !!}"
               class='btn btn-danger btn-xs'
               data-toggle="tooltip" data-placement="top" title="PUBLICAR proyecto"
               onclick="return confirm('Esta seguro que desea PUBLICAR a este proyecto?')">
                <i class="fa fa-arrow-up" aria-hidden="true"></i>
            </a>
        @endif
    @else
        @if($btn_large_name == true)
            <a href="{!! url('no_publicar_proyecto', [$costProyecto->id]) !!}"
               class='btn btn-primary'
               data-toggle="tooltip" data-placement="top" title="NO PUBLICAR proyecto"
               onclick="return confirm('Esta seguro que desea NO PUBLICAR a este proyecto?')">
                <i class="fa fa-arrow-down" aria-hidden="true"></i> No publicar proyecto
            </a>
        @else
            <a href="{!! url('no_publicar_proyecto', [$costProyecto->id]) !!}"
               class='btn btn-primary btn-xs'
               data-toggle="tooltip" data-placement="top" title="NO PUBLICAR proyecto"
               onclick="return confirm('Esta seguro que desea NO PUBLICAR a este proyecto?')">
                <i class="fa fa-arrow-down" aria-hidden="true"></i>
            </a>
        @endif
    @endif()
@endif()