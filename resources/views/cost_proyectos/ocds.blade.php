<div class="alert alert-danger alert-dismissible">
    <h4><i class="icon fa fa-warning"></i> Advertencia!</h4>
    Esta página se encuentra en construcción. Los datos públicados pueden variar en base a su fuente.
</div>
<div class="btn-group open">
    <button type="button" class="btn btn-default">OC4IDS</button>
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{asset('costProyectos/proyecto/'.$costProyecto->id_proyecto)}}">Release 1</a></li>

    </ul>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default" >Encrypt Key</button>

@include('cost_proyectos._convert_json_csv',compact('costProyecto'))
@include('cost_proyectos._convert_json_excel',compact('costProyecto'))
</div>
<div class="table table-responsive">
    <pre>  {{$json_format}} </pre>
</div>

<div class="modal fade in" id="modal-default" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Llave para corroborar tamaño de JSON</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Función Hash MD5 </h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-4">

                                        <p>{!! md5($json_format) !!}</p>
                                    </div>


                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
