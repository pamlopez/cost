@extends('layouts.app')
@section('content')
    @php($btn_large_name = true)
    @if($costProyecto->foto==0)
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example" data-slide-to="1"></li>
                <li data-target="#carousel-example" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner">
                <div class="item active">
                    <a href="#">
                        <img src="{{asset('images/proyectos/'.$costProyecto->id_proyecto.'/foto1.jpg') }}" alt="">
                    </a>
                    <div class="carousel-caption">
                        <h3></h3>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <a href="#"> <img src="{{asset('images/proyectos/'.$costProyecto->id_proyecto.'/foto2.jpg') }} "
                                      alt=""></a>
                    <div class="carousel-caption">
                        <h3></h3>
                        <p></p>
                    </div>
                </div>
                <div class="item">
                    <a href="#"><img src="{{asset('images/proyectos/'.$costProyecto->id_proyecto.'/foto3.jpg') }} "
                                     alt=""></a>
                    <div class="carousel-caption">
                        <h3></h3>
                        <p></p>
                    </div>
                </div>
            </div>

            <a class="left carousel-control" href="#carousel-example" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    @endif

    <div class="content">
        <div class="box box-danger">
            <div class="box-body">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <h1>
                                <b>{!! $costProyecto->nombre_proyecto !!}</b>
                            </h1>
                            <br/>
                            <h3>
                                <b>NOG: {!! $costProyecto->nog !!}</b>
                                <br/>
                                <b>No.Proyecto: {{$costProyecto->no_cost}} Gt</b>
                            </h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-6">
                            @if($costProyecto->pdf==0)
                                <h2>
                                    <a href="{{asset('files/resumen'.$costProyecto->id_proyecto.'.pdf')}}"
                                       target="_blank"> <i
                                                class="fa fa-file-pdf-o no-print" aria-hidden="true"></i> Resumen
                                        PDF</a>
                                    @if($costProyecto->id_proyecto == 38)
                                        <a href="{{asset('files/resumen'.$costProyecto->id_proyecto.'a.pdf')}}"
                                           target="_blank"> <i
                                                    class="fa fa-file-pdf-o no-print" aria-hidden="true"></i> Informe
                                            PDF</a>
                                        <a href="{{asset('files/resumen'.$costProyecto->id_proyecto.'b.pdf')}}"
                                           target="_blank"> <i
                                                    class="fa fa-file-pdf-o no-print" aria-hidden="true"></i> Analisis
                                            PDF</a>
                                        <a href="{{asset('files/resumen'.$costProyecto->id_proyecto.'c.pdf')}}"
                                           target="_blank"> <i
                                                    class="fa fa-file-pdf-o no-print" aria-hidden="true"></i> Segundo
                                            Informe PDF</a>
                                    @endif
                                    @if($costProyecto->seguimiento == 1)
                                        <a href="{{asset('files/resumen'.$costProyecto->id_proyecto.'s.pdf')}}"
                                           target="_blank"> <i
                                                    class="fa fa-file-pdf-o no-print" aria-hidden="true"></i> Seguimiento
                                            PDF</a>
                                    @endif

                                </h2>
                                <button onclick="window.print()" type="button"
                                        class="btn btn-warning pull-left no-print">
                                    <i class="fa fa-print"></i> Imprimir
                                </button>
                                @include('cost_proyectos._seguimiento',compact('cost_proyecto'))
                                @include('cost_proyectos._permisos_publicador',compact('btn_large_name'))
                            @endif
                        </div>
                        <div class="col-sm-3">
                            <div class="info-box bg-blue-gradient">
                                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Fecha de revisión proactiva:</span>
                                    <span class="info-box-number">{!! \Carbon\Carbon::parse($costProyecto->f_r_proactiva)->format('d/m/Y')!!}</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 100%"></div>
                                    </div>
                                    <span class="progress-description"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="info-box bg-red-gradient">
                                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Fecha de revisión reactiva:</span>
                                    <span class="info-box-number">{!! \Carbon\Carbon::parse($costProyecto->f_r_reactiva)->format('d/m/Y')!!}</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 100%"></div>
                                    </div>
                                    <span class="progress-description"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="exTab" class="container30000">
                    <ul class="nav nav-tabs no-print">
                        @if(isset($a) and $a==1)
                            <li>
                                <a href="#11" data-toggle="tab">Datos clave del proyecto</a>
                            </li>
                            <li class="active"><a href="#12" data-toggle="tab">Fases del Proyecto</a>
                            </li>
                            <li><a href="f" data-toggle="tab">OC4IDS</a>
                            </li>
                        @else
                            <li class="active">
                                <a href="#11" data-toggle="tab">Datos clave del proyecto</a>
                            </li>
                            <li>
                                <a href="#12" data-toggle="tab">Fases del Proyecto</a>
                            </li>
                            <li>
                                <a href="#13" data-toggle="tab">OC4IDS</a>
                            </li>
                        @endif

                    </ul>

                    @if(isset($a) and $a==1)
                        <div class="tab-content">
                            <div class="tab-pane" id="11">
                                <div class="box-body">
                                    @include('cost_proyectos.show_fields')
                                </div>
                            </div>
                            <div class="tab-pane active" id="12">
                                <div class="box-body">
                                    @include('cost_proyectos.resolucion')
                                </div>
                            </div>
                            <div class="tab-pane" id="13">
                                <div class="box-body">
                                    @include('cost_proyectos.ocds')
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="tab-content">
                            <div class="tab-pane active" id="11">
                                <div class="box-body">
                                    @include('cost_proyectos.show_fields')
                                </div>
                            </div>
                            <div class="tab-pane" id="12">
                                <div class="box-body">
                                    @include('cost_proyectos.resolucion')
                                </div>
                            </div>
                            <div class="tab-pane" id="13">
                                <div class="box-body">
                                    @include('cost_proyectos.ocds')
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="box-footer no-print">
                    <br>
                    @include('partials.btn_previous')
                </div>
            </div>
        </div>
    </div>
@endsection
