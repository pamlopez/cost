@php($btn_large_name = false)

<table class="table table-hover table-bordered  table-responsive table-condensed" id="costProyectos-table">
    <thead>
    <tr style="background-color: #eeeeee; color:#424242;">
        <th>#</th>
        <th>No.CoST</th>
        <th style="text-align: center">Fecha ingreso</th>
        <th style="text-align: center">Entidad Adquisición</th>
        <th style="text-align: center">Nombre del Proyecto</th>
        <th style="text-align: center">Localización</th>
        <th style="text-align: center">NOG</th>
        <th style="text-align: center">SNIP</th>
        <th style="text-align: center">Seguimiento</th>

        @if($you_have_permission == true ||  $you_have_permission_publicador == true)
            <th colspan="3" width="10%" style="text-align: center">Acciones</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @php($i=0)

    @if(count($costProyectos)>0)
        @foreach($costProyectos as $id => $costProyecto)
            <tr>
                <td @include('cost_proyectos._ruta_onclick')>{{  (($costProyectos->currentPage() -1) * $costProyectos->perPage())+1+$id }}</td>
                <td @include('cost_proyectos._ruta_onclick')><b>{{$costProyecto->no_cost}}</b></td>
                <td @include('cost_proyectos._ruta_onclick')>{!!\Carbon\Carbon::parse( $costProyecto->f_ingreso)->format('d/m/Y') !!}</td>
                <td @include('cost_proyectos._ruta_onclick')>{!! $costProyecto->FmtIdEntidadAdquisicion !!}</td>
                <td @include('cost_proyectos._ruta_onclick')><b>{!! $costProyecto->nombre_proyecto !!}</b></td>
                <td @include('cost_proyectos._ruta_onclick')>{!! $costProyecto->localizacion !!}</td>
                <td @include('cost_proyectos._ruta_onclick')><b>{!! $costProyecto->nog !!}</b></td>
                <td @include('cost_proyectos._ruta_onclick')>{!! $costProyecto->snip !!}</td>
                <td @include('cost_proyectos._ruta_onclick')>{!! $costProyecto->seguimiento ? 'Si' : 'No' !!}</td>
                @if(Auth::check() && $you_have_permission == true || $you_have_permission_publicador == true )
                    <td width="11%">
                        <div class='btn-group'>
                            @if($you_have_permission == true)
                                <a href="{!! route('costProyectos.show', [$costProyecto->id]) !!}"
                                   class='btn btn-info btn-xs'
                                   data-toggle="tooltip" title="Ver proyecto" data-placement="top"><i
                                            class="glyphicon glyphicon-eye-open"></i></a>
                                @if(Auth::check() )
                                    <a href="{!! route('costProyectos.edit', [$costProyecto->id]) !!}"
                                       class='btn btn-warning btn-xs' data-toggle="tooltip" title="Editar información"
                                       data-placement="top"><i
                                                class="glyphicon glyphicon-edit"></i></a>

                                    {!! Form::button('<i class="glyphicon glyphicon-menu-down" data-toggle="tooltip" title="Ver detalle"  data-placement="top"></i>', ['type' => 'button', 'class' => 'btn btn-success btn-xs', 'data-toggle' => "collapse" , 'data-target'=>"#collapseme".$i ]) !!}
                                @endif
                            @endif()
                           @include('cost_proyectos._permisos_publicador',compact('btn_large_name'))
                           @include('cost_proyectos._seguimiento',compact('btn_large_name'))
                        </div>
                    </td>
                @endif()
            </tr>
            @if(Auth::check())

                <tr class="collapse out" id="collapseme{{$i}}">
                    <td colspan="12">
                        @include('cost_proyectos.resolucion',compact('you_have_permission'))
                    </td>
                </tr>
            @endif
            <div class="modal modal-info fade" id="modal-primary{{$costProyecto->id}}" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            @if(Auth::check())
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title" style="color:#FFFFFF"><i class="fa fa-download"
                                                                                 aria-hidden="true"></i>
                                    Descarga de proyecto
                                    <br/>
                                    {{$costProyecto->nombre_proyecto}}
                                </h4>
                            @endif
                        </div>
                        {!! Form::open(['route' => 'descargaResponsables.store']) !!}
                        <div class="modal-body">
                            <div class="row">
                                @php($bnd_gnrl = 2)
                                @include('descarga_responsables.fields')
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn  pull-left btn-danger" data-dismiss="modal"><i
                                        class="fa fa-close" aria-hidden="true"></i> Cancelar
                            </button>
                            <button type="submit" class="btn btn-success"><i class="fa fa-download"
                                                                             aria-hidden="true"></i> Descargar proyecto
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        @endforeach
    @else
        <tr class="warning">
            <td style="text-align: center" colspan="9"><b> No se encontraron resultados </b></td>
        </tr>
    @endif
    </tbody>
</table>

