
<div class="box box-default  @if(isset($request->b)) @else collapsed-box @endif" >
    <div class="box-header with-border" style="background-color:#00acc1; color: #FFFFFF" data-toggle="tooltip" title="Presione el botón + para ver los filtros de búsqueda específicos" data-placement="bottom">
        <h3 class="box-title" data-toggle="tooltip" title="Presione el botón + para ver los filtros de búsqueda específicos" data-placement="top">
            <b style="color: #FFFFFF">Personalizar la información visualizada</b>
        </h3>
        <div class="box-tools pull-right" >
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus" style="color: #FFFFFF"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        {{ Form::open(array('url' =>"#",'method' => 'get')) }}
        <div class="row">
            <div class="col-md-2 ">
                <div class="form-group">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Desde: </label>
                    {!! Form::text('fh_inicial', null, ['class' => 'form-control datepicker','data-value'=>$filtros->fecha_del]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label><i class="fa fa-calendar" aria-hidden="true"></i> Hasta: </label>
                    {!! Form::text('fh_fin', null, ['class' => 'form-control datepicker','data-value'=>$filtros->fecha_al]) !!}
                </div>
            </div>
            <div class="form-group col-sm-2">
                {!! Form::label('nombre_proyecto', 'Nombre proyecto:') !!}
                {!! Form::text('nombre_proyecto', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-2">
                {!! Form::label('snip', 'SNIP:') !!}
                {!! Form::number('snip', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-2">
                {!! Form::label('nog', 'NOG:') !!}
                {!! Form::number('nog', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-2">
                {!! Form::label('localizacion', 'Localización:') !!}
                {!! Form::text('localizacion', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-2">
                {!! Form::label('sector', 'Sector:') !!}
                {!! Form::text('sector', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-2">
                {!! Form::label('sub_sector', 'Sub Sector:') !!}
                {!! Form::text('sub_sector', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-2">
                {!! Form::label('entidad_adquisicion', 'Entidad adquisición:') !!}
                {!! Form::text('entidad_adquisicion', null, ['class' => 'form-control']) !!}
            </div>
            <!-- Id Estado Civil Field -->
            <div class="col-md-4" style="margin-top: 2%; margin-bottom: 2%;">
                {!! Form::label('', '') !!}
                {!! Form::label('', 'Fecha última actualización:') !!}
                <input type="radio" class="flat" name="fh_ultima_actualizacion"  value="1"> &nbsp;&nbsp;&nbsp;

                {!! Form::label('', 'Fecha aprobación:') !!}
                <input type="radio" value="2" class="flat" name="f_aprobacion">
            </div>
            <div style="margin-top:0.3%">
                <div class="col-md-1">
                    <div class="form-group">
                        {!! Form::label('', '') !!}
                        <button type="submit" class="btn btn-danger form-control ">  <i class="fa fa-filter" aria-hidden="true"></i> Filtrar</button>
                    </div>

                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        {!! Form::label('', '') !!}
                        <a type="button" class="btn bg-navy form-control" href="{{ $filtros->url }}"><i class="fa fa-refresh" aria-hidden="true"></i> </a>
                    </div>

                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <!-- /.box-body -->
</div>