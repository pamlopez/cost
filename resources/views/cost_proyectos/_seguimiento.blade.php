@if($you_have_permission_publicador == true || $you_have_permission == true)
    @if($costProyecto->seguimiento == 0)
        @if($btn_large_name == true)
            <a href="{!! url('marcar_seguimiento', [$costProyecto->id]) !!}"
               class='btn bg-navy'
               data-toggle="tooltip" data-placement="top" title="Marcar como seguimiento"
               onclick="return confirm('Esta seguro que desea marcar como seguimiento este proyecto?')">
                <i class="fa fa fa-circle-o-notch" aria-hidden="true"></i> Marcar como seguimiento
            </a>
        @else
            <a href="{!! url('marcar_seguimiento', [$costProyecto->id]) !!}"
               class='btn bg-navy btn-xs'
               data-toggle="tooltip" data-placement="top" title="Marcar como seguimiento"
               onclick="return confirm('Esta seguro que desea marcar como seguimiento este proyecto?')">
                <i class="fa fa fa-circle-o-notch" aria-hidden="true"></i>
            </a>
        @endif
    @else
        @if($btn_large_name == true)
            <a href="{!! url('no_marcar_seguimiento', [$costProyecto->id]) !!}"
               class='btn btn-success'
               data-toggle="tooltip" data-placement="top" title="NO Marcar como seguimiento"
               onclick="return confirm('Esta seguro que desea marcar como NO seguimiento este proyecto?')">
                <i class="fa fa-circle-o" aria-hidden="true"></i> No  Marcar como seguimiento
            </a>
        @else
            <a href="{!! url('no_marcar_seguimiento', [$costProyecto->id]) !!}"
               class='btn btn-success btn-xs'
               data-toggle="tooltip" data-placement="top" title="NO Marcar como seguimiento"
               onclick="return confirm('Esta seguro que desea marcar como NO seguimiento este proyecto?')">
                <i class="fa fa-circle-o" aria-hidden="true"></i>
            </a>
        @endif
    @endif()
@endif()