@extends('layouts.app')
@section('content')
    @include('cost_proyectos._table_convert_json_csv_excel')
@endsection

@push('javascript')
    <script>
        // Listener al boton
      $("#csv").table2csv({
            name: "datos",
            filename: "{{ $costProyecto->no_cost }}_fecha_descarga_" + new Date().toLocaleString("en-GB", {timeZone: "America/Guatemala"}).replace(/[\-\:\.]/g, "").substring(0, 10) + ".csv",
            fileext: ".csv",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });

        $(document).ready(function () {
            setTimeout(function () {
                window.location.href = '{{ URL::previous() }}';
            }, 1000);
        });
    </script>

@endpush