<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Different Height</h3>
    </div>
    <div class="box-body">

        <!-- Entidad Adquisicion Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('entidad_adquisicion', 'Entidad Adquisicion:') !!}
            {!! Form::text('entidad_adquisicion', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Entidad Admin Contrato Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('entidad_admin_contrato', 'Entidad Admin Contrato:') !!}
            {!! Form::text('entidad_admin_contrato', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Sector Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('sector', 'Sector:') !!}
            {!! Form::text('sector', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Sub Sector Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('sub_sector', 'Sub Sector:') !!}
            {!! Form::text('sub_sector', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Programa Multi D Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('programa_multi_d', 'Programa Multi D:') !!}
            {!! Form::text('programa_multi_d', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Programa Multi Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('programa_multi', 'Programa Multi:') !!}
            {!! Form::number('programa_multi', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Presupuesto Proyecto Multi D Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('presupuesto_proyecto_multi_d', 'Presupuesto Proyecto Multi D:') !!}
            {!! Form::text('presupuesto_proyecto_multi_d', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Presupuesto Proyecto Multi Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('presupuesto_proyecto_multi', 'Presupuesto Proyecto Multi:') !!}
            {!! Form::number('presupuesto_proyecto_multi', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Contacto Entidad Field -->
        <div class="form-group col-sm-12 col-lg-12">
            {!! Form::label('contacto_entidad', 'Contacto Entidad:') !!}
            {!! Form::textarea('contacto_entidad', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Nombre Proyecto Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('nombre_proyecto', 'Nombre Proyecto:') !!}
            {!! Form::text('nombre_proyecto', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Detalle Responsable Field -->
        <div class="form-group col-sm-12 col-lg-12">
            {!! Form::label('detalle_responsable', 'Detalle Responsable:') !!}
            {!! Form::textarea('detalle_responsable', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Fh Ultima Actualizacion Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('fh_ultima_actualizacion', 'Fh Ultima Actualizacion:') !!}
            {!! Form::date('fh_ultima_actualizacion', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Localizacion Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('localizacion', 'Localizacion:') !!}
            {!! Form::text('localizacion', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Localizacion Muni D Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('localizacion_muni_d', 'Localizacion Muni D:') !!}
            {!! Form::text('localizacion_muni_d', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Localizacion Muni Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('localizacion_muni', 'Localizacion Muni:') !!}
            {!! Form::number('localizacion_muni', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Localizacion Depto D Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('localizacion_depto_d', 'Localizacion Depto D:') !!}
            {!! Form::text('localizacion_depto_d', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Localizacion Depto Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('localizacion_depto', 'Localizacion Depto:') !!}
            {!! Form::number('localizacion_depto', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Coordenadas Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('coordenadas', 'Coordenadas:') !!}
            {!! Form::text('coordenadas', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Coordenadas Cln Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('coordenadas_cln', 'Coordenadas Cln:') !!}
            {!! Form::text('coordenadas_cln', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Snip Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('snip', 'Snip:') !!}
            {!! Form::number('snip', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Snip Detalle Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('snip_detalle', 'Snip Detalle:') !!}
            {!! Form::text('snip_detalle', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Proposito Field -->
        <div class="form-group col-sm-12 col-lg-12">
            {!! Form::label('proposito', 'Proposito:') !!}
            {!! Form::textarea('proposito', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Resumen Field -->
        <div class="form-group col-sm-12 col-lg-12">
            {!! Form::label('resumen', 'Resumen:') !!}
            {!! Form::textarea('resumen', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Beneficiario D Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('beneficiario_d', 'Beneficiario D:') !!}
            {!! Form::text('beneficiario_d', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Beneficiario Total Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('beneficiario_total', 'Beneficiario Total:') !!}
            {!! Form::number('beneficiario_total', null, ['class' => 'form-control']) !!}
        </div>

        <!-- F Aprobacion Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('f_aprobacion', 'F Aprobacion:') !!}
            {!! Form::date('f_aprobacion', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Submit Field -->
        <div class="form-group col-sm-12">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{!! route('costProyectos.index') !!}" class="btn btn-default">Cancel</a>
        </div>

    </div>
</div>