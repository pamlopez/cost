
<div class="container-fluid">
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <i class="fa fa-info" aria-hidden="true"></i>
                    <h3 class="box-title">Descripción del proyecto</h3>
                </div>
                <div class="box-body">
                    <p>{!! $costProyecto->descripcion !!}</p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <i class="fa fa-info" aria-hidden="true"></i>
                    <h3 class="box-title">Propósito</h3>
                </div>
                <div class="box-body">
                    <p>{!! $costProyecto->proposito !!}</p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

    </div>
    <br>
    <div class="row">
        <div class="col-md-4" align="center">
            <div class="info-box">
                <span class="info-box-icon bg-red-gradient">
                    @if($costProyecto->id_entidad_adquisicion==1)
                        <i class="fa fa-university"></i>

                    @elseif($costProyecto->id_entidad_adquisicion==2)
                        <i class="fa fa-building"></i>
                    @else
                        <i class="fa fa-university"></i><br/>
                    @endif
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Entidad a cargo</span>
                    <span class="info-box-number"><small>{!! $costProyecto->FmtIdEntidadAdquisicion !!}<br>
                            @if(strlen($costProyecto->sector)>0)
                                {!! $costProyecto->sector !!}<br>
                                @if(strlen($costProyecto->sub_sector)>3)
                                    {!! $costProyecto->sub_sector !!}<br>
                                @endif
                            @endif</small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-4" align="center">
            <div class="info-box">
                <span class="info-box-icon bg-red-gradient"><i class="fa fa-bookmark" aria-hidden="true"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Estado actual del proyecto</span>
                    <span class="info-box-number"><small>{!! $costProyecto->fmt_estado_actual_proyecto !!}</small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <div class="col-md-4" align="center">
            <div class="info-box">
                <span class="info-box-icon bg-red-gradient"><i class="ion ion-ios-gear-outline"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Modalidad contratación</span>
                    @if(strlen($modalidad_contrato )>0)
                        <span class="info-box-number"><small>{!! $modalidad_contrato !!}</small></span>
                    @else
                        <span class="info-box-number"><small>Sin datos</small></span>
                    @endif
                </div>
                <!-- /.info-box-content -->
            </div>

        </div>

    </div>
    <br>
    <div class="box box-solid bg-light-blue-gradient">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse"
                        data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
            <!-- /. tools -->

            <i class="fa fa-map-marker"></i>

            <h3 class="box-title" style="color:floralwhite">
                @if(strlen($costProyecto->localizacion)>0)
                Ubicación: {!! $costProyecto->localizacion !!}, {!!$costProyecto->localizacion_depto_d !!}
                , {!!$costProyecto->localizacion_muni_d !!}
                    @else
                No se especifico la ubicación
                    @endif
            </h3>
        </div>
        <div class="box-body">
            <div id="map" style="min-width: 50px; height: 250px; margin: 0 auto"></div>
        </div>
        <!-- /.box-body-->

    </div>
    <br>

    <div class="row">
        <div class="col-md-4">
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-black"
                     style="background: url({{asset('images/estudio.jpg') }}) center center;">
                    <h3 class="widget-user-username " style="color:black; text-align: center"></h3>
                    <h5 class="widget-user-desc"></h5>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" src="{{asset('images/usuario.png') }}" alt="User Avatar">
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-12 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$responsable_estudio}}</h5>
                                <span class="description-text">Responsable del estudio</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-black"
                     style="background: url({{asset('images/diseno.png') }}) center center;">
                    <h3 class="widget-user-username " style="color:black; text-align: center"></h3>
                    <h5 class="widget-user-desc"></h5>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" src="{{asset('images/usuario.png') }}" alt="User Avatar">
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-12 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$responsable_diseno}}</h5>
                                <span class="description-text">Responsable del diseño</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-black"
                     style="background: url({{asset('images/supervision.jpg') }}) center center;">
                    <h3 class="widget-user-username " style="color:black; text-align: center"></h3>
                    <h5 class="widget-user-desc"></h5>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" src="{{asset('images/usuario.png') }}" alt="User Avatar">
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-12 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$responsable_supervision}}</h5>
                                <span class="description-text">Responsable de la supervisión</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="  col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h2 style="color:white">{{$empresa_contratada}}</h2>
                    Empresa Contratada
                </div>
                <div class="icon">
                    <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                </div>
                @if(isset($mc))
                    <a href="{!! route('costInfoContratos.show', [$mc->id]) !!}" class="small-box-footer">
                        Más info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                @else
                    <a href="#" class="small-box-footer">
                        Más info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                @endif
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h2 style="color:white">
                        @php($money = $monto_contratado > 0 ? number_format($monto_contratado, 2, '.', ',') : '' )

                        Q. {{ $money  }}</h2>
                    Monto contratado
                </div>
                <div class="icon">
                    <i class="fa fa-money" aria-hidden="true"></i>
                </div>
                @if(isset($mc))
                    <a href="{!! route('costInfoContratos.show', [$mc->id]) !!}" class="small-box-footer">
                        Más info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                @else
                    <a href="#" class="small-box-footer">
                        Más info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                @endif
            </div>
        </div>
        <div class=" col-lg-4 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h2 style="color:white">{{$fecha_inicio}}</h2>
                    Fecha de Inicio de Contrato
                </div>
                <div class="icon">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </div>
                @if(isset($mc))
                    <a href="{!! route('costInfoContratos.show', [$mc->id]) !!}" class="small-box-footer">
                        Más info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                @else
                    <a href="#" class="small-box-footer">
                        Más info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                @endif
            </div>
        </div>
    </div>
    <br>
    <div class="box box-danger">
        <div class="box-header">
            <b><h3 class="box-title"><i class="fa fa-users" aria-hidden="true"></i> Listado de Oferentes</h3></b>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            @include('cost_oferentes.table')
        </div>
        <!-- /.box-body -->
    </div>
    <br>
    <div class="box box-danger">
        <div class="box-header">
            <b><h3 class="box-title"><i class="fa fa-money" aria-hidden="true"></i> Modificaciones al monto</h3></b>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            @if(isset($costLiquidacionMontos))
                @include('cost_liquidacion_montos.table')
            @else
                "Información no disponible"
            @endif
        </div>
        <!-- /.box-body -->
    </div>
    <br>
    <div class="box box-danger">
        <div class="box-header">
            <b><h3 class="box-title"><i class="fa fa-hourglass-half" aria-hidden="true"></i> Modificaciones al plazo
                </h3></b>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            @if(isset($costLiquidacionTiempos))
                @include('cost_liquidacion_tiempos.table')
            @else
                "Información no disponible"
            @endif

        </div>
        <!-- /.box-body -->
    </div>
    <br>
    <div class="box box-danger">
        <div class="box-header">
            <b><h3 class="box-title"><i class="fa fa-paper-plane" aria-hidden="true"></i> Modificaciones al alcance</h3>
            </b>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            @if(isset($costLiquidacionAlcances))
                @include('cost_liquidacion_alcances.table')
            @else
                "Información no disponible"
            @endif

        </div>
        <!-- /.box-body -->
    </div>
    <br>
    <div class="box box-danger">
        <div class="box-header">
            <b><h3 class="box-title"><i class="fa fa-paper-plane" aria-hidden="true"></i> Documentos consultados</h3>
            </b>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <div class="table-responsive">
                @if(isset($documentos))
                    @include('documentos.table')
                @else
                    "Información no disponible"
                @endif
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <br>
    {{--
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <i class="fa fa-info" aria-hidden="true"></i>
                    <h3 class="box-title">Divulgación Proactiva según IDS CoST SI</h3>
                </div>
                <div class="box-body">
                    <div id="container2" style="min-width: 50px; height: 250px; max-width: 550px; margin: 0 auto"></div>
                    <br>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <div class="col-md-6">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Detalle de Divulgación Proactiva según IDS CoST SI</h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Dato</th>
                                <th>Pendiente</th>
                                <th>Completo</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i=0)
                            @foreach($cost_inter as $key=>$value)
                                @php($i++)
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>{{$key}}</td>
                                    @if($value==1)
                                        <td></td>
                                        <td><span class="label label-success">Completo</span></td>
                                    @else
                                        <td><span class="label label-danger">Pendiente</span></td>
                                        <td></td>
                                    @endif
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    --}}
{{--
<div class="row">

    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <i class="fa fa-info" aria-hidden="true"></i>
                <h3 class="box-title">Divulgación Proactiva según Resolución 1-2014</h3>
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="col-xs-12">

                            <div class="table-responsive no-padding">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Dato</th>
                                        <th>Pendiente</th>
                                        <th>Completo</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @php($i=0)
                                    @foreach($res_inter2 as $resultado_inter)
                                        @php($i++)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>
                                                <a href="{!! route($resultado_inter['link_view'], [$resultado_inter['link_id']]) !!}">
                                                    {{$resultado_inter['nombre']}}
                                                </a>
                                            </td>

                                            @if($resultado_inter['valor']==1)
                                                <td></td>
                                                <td><span class="label label-success">Completo</span></td>
                                            @else
                                                <td><span class="label label-danger">Pendiente</span></td>
                                                <td></td>
                                            @endif
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->

                        <!-- /.box -->
                    </div>
                </div>
                <div class="col-md-6">


                        <div class="table-responsive no-padding">
                            <div id="container1" style="min-width: 50px; height: 250px; max-width: 550px; margin: 0 auto"></div>
                            <br>
                        </div>



                </div>



            </div>
        </div>


    </div>


</div>
 --}}


@if(count($fotos)>0)
    <div class="">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title"><b><i class="fa fa-camera-retro" aria-hidden="true"></i> Fotografías del
                        proyecto</b>
                </h3>
            </div>

            <div class="box-body">
                <div class="col-md-6 col-sm-offset-3">

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @foreach($fotos as $key => $value)
                                @php($active = $key == 0 ? 'active':'')
                                <li data-target="#carousel-example-generic" data-slide-to="{{$key}}"
                                    class="{{$active}}"></li>
                            @endforeach
                        </ol>

                        <div class="carousel-inner">
                            @foreach($fotos as $key => $value)
                                @php($active = $key == 0 ? 'active':'')
                                <div class="item {{$active}}">
                                    <img src="{{url($value->link)}}" width="auto" height="100px" alt="First slide">
                                    <!--div class="carousel-caption">
                                    </div-->
                                </div>
                            @endforeach
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="fa fa-angle-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="fa fa-angle-right"></span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
@endif
</div>
@push('javascript')
<script>
    jQuery(document).ready(function ($) {

        $('#myCarousel').carousel({
            interval: 5000
        });

        //Handles the carousel thumbnails
        $('[id^=carousel-selector-]').click(function () {
            var id_selector = $(this).attr("id");
            try {
                var id = /-(\d+)$/.exec(id_selector)[ 1 ];
                console.log(id_selector, id);
                jQuery('#myCarousel').carousel(parseInt(id));
            } catch (e) {
                console.log('Regex failed!', e);
            }
        });
        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid.bs.carousel', function (e) {
            var id = $('.item.active').data('slide-number');
            $('#carousel-text').html($('#slide-content-' + id).html());
        });
    });
</script>

<script>
    var map = new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            }),
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([{!! $costProyecto->longitud !!}, {!! $costProyecto->latitud !!}]),
            zoom: 15
        }),
        interactions: ol.interaction.defaults({ mouseWheelZoom: false }),

    });

    // Geometries
    var point = new ol.geom.Point(
        ol.proj.transform([{!! $costProyecto->longitud !!}, {!! $costProyecto->latitud !!}], 'EPSG:4326', 'EPSG:3857')
    );

    // Features
    var pointFeature = new ol.Feature(point);

    // Source
    var vectorSource = new ol.source.Vector({
        projection: 'EPSG:4326'
    });

    function flickrStyle(feature) {
        var style = new ol.style.Style({
            image: new ol.style.Icon({
                anchor: [ 0.5, 0.5 ],
                scale: 0.10,
                src: '{{asset('images/marker.png') }}'
            })
        });
        return [ style ];
    }

    vectorSource.addFeatures([ pointFeature ]);

    // Vector layer
    var vectorLayer = new ol.layer.Vector({
        source: vectorSource,
        style: flickrStyle
    });
    // Add Vector layer to map
    map.addLayer(vectorLayer);

    Highcharts.setOptions({
        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {
                    cx: 0.5,
                    cy: 0.3,
                    r: 0.7
                },
                stops: [
                    [ 0, color ],
                    [ 1, Highcharts.Color(color).brighten(-0.3).get('rgb') ] // darken
                ]
            };
        })
    });
    Highcharts.chart('container1', {
        chart: {
            borderWidth: 0,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'bar'
        },
        title: {
            text: 'Según Resolución 1-2014'
        },
        xAxis: {
            categories: [ '' ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Indices'
            },

        },
        tooltip: {
            pointFormat: '{series.name}:{point.y}<br/> .',
            positioner: function (labelWidth, labelHeight, point) {
                var tooltipX = point.plotX - 125;
                var tooltipY = point.plotY - 30;
                return {
                    x: tooltipX,
                    y: tooltipY
                };
            }
        },
        legend: {
            reversed: false
        },

        plotOptions: {
            series: {

                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [
            {
                name: 'Indicadores de la Resolución 1-2014',
                data: [
                    {{$de_cuantos_res}}
                ]
            }, {
                name: 'Datos publicados',
                data: [
                    {{$cuantos_res}}
                ]
            } ]
    });
    Highcharts.chart('container2', {
        chart: {
            borderWidth: 0,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'bar'
        },
        title: {
            text: 'Según IDS CoST SI'
        },
        xAxis: {
            categories: [ '' ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Indices'
            },

        },
        tooltip: {
            pointFormat: '{series.name}:{point.y}<br/> .',
            positioner: function (labelWidth, labelHeight, point) {
                var tooltipX = point.plotX - 125;
                var tooltipY = point.plotY - 30;
                return {
                    x: tooltipX,
                    y: tooltipY
                };
            }
        },
        legend: {
            reversed: false
        },

        plotOptions: {
            series: {

                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [
            {
                name: 'Indicadores del Estandar CoST',
                data: [
                    {{$de_cuantos_cost}}
                ]
            }, {
                name: 'Datos publicados',
                data: [
                    {{$cuantos_cost}}
                ]
            } ]
    });


</script>
@endpush