@extends('layouts.app')
@section('content')
    <section class="content-header" style="margin-top: -2%">
        @include('cost_proyectos.filtros')
    </section>
    <div class="content" style="margin-top: -1%">
        <div class="clearfix"></div>
        @include('flash::message')
        <div class="clearfix"></div>
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-header">
                    <b style="color:#c62828">
                        <i class="fa fa-cubes" aria-hidden="true"></i>
                        Total de proyectos {{ $costProyectos->total() }}
                    </b>
                    @if(Auth::check() && $you_have_permission == true)
                        <a class="btn btn-primary pull-right" href="{!! route('costProyectos.create') !!}">
                            <i class="fa fa-file-text" aria-hidden="true"></i> Crear proyecto</a>
                    @endif
                </h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    @include('cost_proyectos.table',compact('you_have_permission','you_have_permission_publicador'))
                </div>

            </div>
            <div class="box-footer">
                {!! $costProyectos->appends(request()->except('page'))->links() !!}
            </div>
        </div>
        <div class="modal modal-info fade" id="modal-primary" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" style="color:#FFFFFF"> <i class="fa fa-download" aria-hidden="true"></i>
                            Descarga de proyecto
                            <br/>
                            @if(isset($bnd_gnrl) && $bnd_gnrl == 2) {{$costProyecto->nombre_proyecto}}@endif
                        </h4>
                    </div>
                    {!! Form::open(['route' => 'descargaResponsables.store']) !!}
                    <div class="modal-body">
                        <div class="row">
                            @php($bnd_gnrl = 1)
                            @include('descarga_responsables.fields')
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn  pull-left btn-danger" data-dismiss="modal"><i class="fa fa-close" aria-hidden="true"></i> Cancelar</button>
                        <button type="submit" class="btn btn-success"> <i class="fa fa-download" aria-hidden="true"></i> Descargar proyecto</button>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
@endsection
@push('javascript')
@endpush