@extends('layouts.app')
@section('content')
    <div class="content">
        <div class="no-print">
            @include('partials.btn_previous')
        </div>
        @include('adminlte-templates::common.errors')
        <div class="row">
            <h3 align="center"><b><i class="fa fa-file-text" aria-hidden="true"></i>  Nuevo proyecto</b></h3>
            {!! Form::open(['route' => 'costProyectos.store']) !!}
                @include('cost_proyectos.fields')
            {!! Form::close() !!}
        </div>
    </div>
@endsection
