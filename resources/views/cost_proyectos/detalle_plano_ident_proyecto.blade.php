@extends('layouts.app')
@section('content')
    <div style="margin-top: -1%">
        <a href="{!! route('costProyectos.show', [$costProyecto->id_proyecto,'a'=>1]) !!}" class="btn btn-info">Volver
            panel principal</a>
        @if(Auth::check())
            <a href="{!! route('costProyectos.edit', [$costProyecto->id]) !!}"
               class='btn btn-warning'><i
                        class="glyphicon glyphicon-edit"></i>Editar registros</a>
        @endif
        @if(Auth::check())
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
            OCDS
        </button>

        <div class="modal fade" id="modal-default" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h3 class="modal-title">ocds-87sd38-{{$costProyecto->no_cost}}Gt</h3>
                    </div>
                    <div class="modal-body">
                        <div class="table table-responsive">
                            <pre>  <code>{{$json_format}}</code> </pre>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        @endif
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><b>1. Identificación del Proyecto</b></h3>
                <h3 align="center">
                    <b>Nombre del proyecto: {{$costProyecto->nombre_proyecto}}</b>
                </h3>
            </div>
            <div class="box-body">

                <div class="col-sm-12">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="row">
                                <!-- Entidad Adquisicion Field -->
                                <div class="form-group col-sm-12">
                                    {!! Form::label('entidad_adquisicion', 'Entidad de adquisición:') !!}
                                    <p>{!! $costProyecto->entidad_adquisicion !!}</p>
                                </div>
                                <!-- Entidad Adquisicion Field -->
                                <div class="form-group col-sm-12">
                                    {!! Form::label('entidad_admin_contrato', 'Entidad administradora del contrato:') !!}
                                    <p>{!! $costProyecto->entidad_admin_contrato!!}</p>
                                </div>
                                <!-- Sector Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('sector', 'Sector:') !!}
                                    <p>{!! $costProyecto->sector!!}</p>
                                </div>

                                <!-- Sub Sector Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('sub_sector', 'Sub sector:') !!}
                                    <p>{!! $costProyecto->sub_sector!!}</p>
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('estado_oc', 'Estado del Proyecto (OC4IDS):') !!}
                                    <p>{!! $costProyecto->fmt_estado_oc!!}</p>
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('sector', 'Sector (OC4IDS):') !!}
                                    <p>{!! $costProyecto->fmt_sector_oc!!}</p>
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('tipo_oc', 'Tipo (OC4IDS):') !!}
                                    <p>{!! $costProyecto->fmt_tipo_oc!!}</p>
                                </div>



                                <div class="form-group col-sm-6">
                                    {!! Form::label('programa_multi_d', 'Programa multianual:') !!}
                                    <p>{!! $costProyecto->programa_multi_d!!}</p>
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('plan_adqui_d', 'Plan anual de adquisiciones:') !!}
                                    <p>{!! $costProyecto->plan_adqui_d!!}</p>
                                </div>
                                <div class="form-group col-sm-12">
                                    {!! Form::label('contacto_entidad', 'Detalles de contacto en entidad de adquisición y rol:') !!}
                                    <p>{!! $costProyecto->contacto_entidad!!}</p>
                                </div>
                                <div class="form-group col-sm-12">
                                    {!! Form::label('nombre_proyecto', 'Nombre proyecto:') !!}
                                    <p>{!! $costProyecto->nombre_proyecto!!}</p>
                                </div>
                                <!-- Localizacion Depto D Field -->
                                <div class="form-group col-sm-12">
                                    {!! Form::label('detalle_responsable', 'Datos de contacto con entidad de adquisiciones:') !!}
                                    <p>{!! $costProyecto->detalle_responsable!!}</p>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="box box-warning">
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-sm-2">
                                    {!! Form::label('depto', 'Departamento:') !!}
                                    <p>{!! $costProyecto->FmtIdDeptoDomicilio!!}</p>
                                </div>
                                <div class="form-group col-sm-2">
                                    {!! Form::label('muni', 'Municipio:') !!}
                                    <p>{!! $costProyecto->FmtIdMuniDomicilio!!}</p>
                                </div>
                                <div class="form-group col-sm-4">
                                    {!! Form::label('localizacion', 'Localización:') !!}
                                    <p>{!! $costProyecto->localizacion!!}</p>
                                </div>
                                <div class="form-group col-sm-4">
                                    {!! Form::label('pais', 'País:') !!}
                                    <p>{!! $costProyecto->pais!!}</p>
                                </div>
                                <div class="form-group col-sm-4">
                                    {!! Form::label('codigo_postal', 'Código Postal:') !!}
                                    <p>{!! $costProyecto->codigo_postal!!}</p>
                                </div>
                                <div class="form-group col-sm-4">
                                    {!! Form::label('coordenadas', 'Coordenadas:') !!}
                                    <p>{!! $costProyecto->coordenadas!!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="box box-warning">
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    {!! Form::label('f_r_proactiva', 'Fecha de revisión proactiva:') !!}
                                    <p>{!! \Carbon\Carbon::parse($costProyecto->f_r_proactiva)->format('d/m/Y')!!}</p>
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('f_r_reactiva', 'Fecha de revisión reactiva:') !!}
                                    <p>{!! \Carbon\Carbon::parse($costProyecto->f_r_reactiva)->format('d/m/Y')!!}</p>
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('f_finalizacion', 'Fecha de finalización (OC4IDS):') !!}
                                    <p>{!! \Carbon\Carbon::parse($costProyecto->f_finalizacion)->format('d/m/Y')!!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="row">
                                <!-- Snip Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('snip', 'SNIP:') !!}
                                    <p>{!! $costProyecto->snip!!}</p>
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('snip_detalle', 'SNIP detalle:') !!}
                                    <p>{!! $costProyecto->snip_detalle!!}</p>
                                </div>
                                <!-- Proposito Field -->
                                <div class="form-group col-sm-12 col-lg-12">
                                    {!! Form::label('proposito', 'Propósito:') !!}
                                    <p>{!! $costProyecto->proposito!!}</p>
                                </div>
                                <div class="form-group col-sm-12 col-lg-12">
                                    {!! Form::label('analisis_alternativas', 'Análisis alternativas:') !!}
                                    <p>{!! $costProyecto->analisis_alternativas!!}</p>
                                </div>
                                <div class="form-group col-sm-12 col-lg-12">
                                    {!! Form::label('descripcion', 'Descripción:') !!}
                                    <p>{!! $costProyecto->descripcion!!}</p>
                                </div>
                                <div class="form-group col-sm-6 col-lg-6">
                                    {!! Form::label('beneficiario_total', 'Número de Beneficiarios:') !!}
                                    <p>{!! $costProyecto->beneficiario_total!!}</p>
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('presupuesto_proyecto_multi_d', 'Presupuesto de proyecto multianual:') !!}
                                    <p>{!! $costProyecto->presupuesto_proyecto_multi_d!!}</p>
                                </div>
                                <!-- F Aprobacion Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('f_aprobacion', 'Fecha aprobación:') !!}
                                    <p>{!! \Carbon\Carbon::parse($costProyecto->f_aprobacion)->format('d/m/Y')!!}</p>
                                </div>
                                <!-- Localizacion Depto D Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('nog', 'NOG:') !!}
                                    <p>{!! $costProyecto->nog!!}</p>
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('nog_f', 'Fecha NOG:') !!}
                                    <p>{!! \Carbon\Carbon::parse($costProyecto->nog_f)->format('d/m/Y')!!}</p>
                                </div>
                                <!-- Localizacion Depto D Field -->
                                <div class="form-group col-sm-6">
                                    {!! Form::label('avance_fisico', 'Avance Físico a la fecha:') !!}
                                    <p>{!! $costProyecto->avance_fisico!!} % </p>
                                </div>

                                <div class="form-group col-sm-6">
                                    {!! Form::label('avance_financiero', 'Avance Financiero a la fecha:') !!}
                                    <p>{!! $costProyecto->avance_financiero!!} % </p>
                                </div>
                                <div class="form-group col-sm-6">
                                    {!! Form::label('estado_actual_proyecto_d', 'Estado actual de proyecto:') !!}
                                    <p>{!! $costProyecto->fmt_estado_actual_proyecto!!}</p>
                                </div>


                                <!-- Resumen Field -->
                                <div class="form-group col-sm-12 col-lg-12">
                                    {!! Form::label('resumen', 'Resumen:') !!}
                                    <p>{!! $costProyecto->resumen!!}</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <a href="{!! route('costProyectos.show', [$costProyecto->id_proyecto,'a'=>1]) !!}" class="btn btn-info">Volver
                    panel principal</a>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

@endsection