<a href="{!! url('convert_json_csv', [$costProyecto->id]) !!}"
   class='btn btn-success'
   data-toggle="tooltip" data-placement="top" title="Proyecto en CSV"
>
    <i class="fa fa-cloud-download" aria-hidden="true"></i> Descargar CSV
</a>

