<!-- Id Empleo Field -->
<div class="form-group">
    {!! Form::label('id_empleo', 'Id Empleo:') !!}
    <p>{!! $empleo->id_empleo !!}</p>
</div>

<!-- Puesto Field -->
<div class="form-group">
    {!! Form::label('puesto', 'Puesto:') !!}
    <p>{!! $empleo->puesto !!}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{!! $empleo->link !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $empleo->descripcion !!}</p>
</div>

<!-- Otros Field -->
<div class="form-group">
    {!! Form::label('otros', 'Otros:') !!}
    <p>{!! $empleo->otros !!}</p>
</div>

