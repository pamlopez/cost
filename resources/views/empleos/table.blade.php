<table class="table table-responsive" id="empleos-table">
    <thead>
        <tr>
            <th>Puesto</th>
        <th>Link</th>
        <th>Descripcion</th>
        <th>Otros</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($empleos as $empleo)
        <tr>
            <td>{!! $empleo->puesto !!}</td>
            <td>{!! $empleo->link !!}</td>
            <td>{!! $empleo->descripcion !!}</td>
            <td>{!! $empleo->otros !!}</td>
            <td>
                {!! Form::open(['route' => ['empleos.destroy', $empleo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('empleos.show', [$empleo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('empleos.edit', [$empleo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>