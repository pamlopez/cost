@if($costAmpliacion->tipo>0)
    <!-- Tipo Field -->
    <div class="form-group col-md-6">
        {!! Form::label('tipo', 'Tipo de ampliación:') !!}
        <p>{!! $costAmpliacion->tipo !!}</p>
    </div>
@else
    <div class="form-group col-md-6">
        {!! Form::label('tipo', 'Tipo de ampliación:') !!}
        <p>{!! $costAmpliacion->tipo !!}</p>
    </div>
@endif


<!-- No Ampliacion Field -->
<div class="form-group col-md-6">
    {!! Form::label('no_ampliacion', 'No Ampliacion:') !!}
    <p>{!! $costAmpliacion->no_ampliacion !!}</p>
</div>

<!-- Fecha Ampliacion Field -->
<div class="form-group col-md-6">
    {!! Form::label('fecha_ampliacion', 'Fecha Ampliacion:') !!}
    <p>{!! $costAmpliacion->fecha_ampliacion !!}</p>
</div>

<!-- No Registro Ampliacion Field -->
<div class="form-group col-md-6">
    {!! Form::label('no_registro_ampliacion', 'No Registro Ampliacion:') !!}
    <p>{!! $costAmpliacion->no_registro_ampliacion !!}</p>
</div>

<!-- Avance Financiero Field -->
<div class="form-group col-md-6">
    {!! Form::label('avance_financiero', 'Avance Financiero:') !!}
    <p>{!! $costAmpliacion->avance_financiero !!}</p>
</div>

<!-- Avance Obra Field -->
<div class="form-group col-md-6">
    {!! Form::label('avance_obra', 'Avance Obra:') !!}
    <p>{!! $costAmpliacion->avance_obra !!}</p>
</div>

<!-- Link Field -->
<div class="form-group col-md-6">
    {!! Form::label('link', 'Link:') !!}
    <p>{!! $costAmpliacion->link !!}</p>
</div>

<!-- Observaciones Field -->
<div class="form-group col-md-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $costAmpliacion->observaciones !!}</p>
</div>

