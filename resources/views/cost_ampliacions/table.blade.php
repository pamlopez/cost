<table class="table table-responsive" id="costAmpliacions-table">
    <thead>
    <tr>
        <th>Id Ampliacion</th>
        <th>Tipo Ampliacion</th>
        <th>No Ampliacion</th>
        <th>Fecha Ampliacion</th>
        <th>No Registro Ampliacion</th>

        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($costAmpliacions as $costAmpliacion)
        @php($i++)
        <tr>
            <td>{!! $i !!}</td>
            <td>{!! $costAmpliacion->tipo !!}</td>
            <td>{!! $costAmpliacion->no_ampliacion !!}</td>
            <td>{!! $costAmpliacion->fecha_ampliacion !!}</td>
            <td>{!! $costAmpliacion->no_registro_ampliacion !!}</td>

            <td>
                {!! Form::open(['route' => ['costAmpliacions.destroy', $costAmpliacion->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costAmpliacions.show', [$costAmpliacion->id]) !!}"
                       class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                        <a href="{!! route('costAmpliacions.edit', [$costAmpliacion->id]) !!}"
                           class='btn btn-warning btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>