@extends('layouts.app')
@include('flash::message')
@section('content')
    <section class="content-header">
        <h1>
           Ampliación
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_ampliacions.show_fields')
                </div>
                <a href="{!! route('costAmpliacions.index', ['id_proyecto'=>$costAmpliacion->id_proyecto,'id_fase'=>$costAmpliacion->id_fase]) !!}"
                   class="btn btn-default">Atrás</a>
            </div>
        </div>
    </div>
@endsection
