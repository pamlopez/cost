{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costAmpliacion->id_proyecto, ['class' => 'form-control']) !!}
{!! Form::hidden('id_fase',isset($id_fase)?$id_fase:$costAmpliacion->id_fase, ['class' => 'form-control']) !!}



@include('chivos.catalogo', ['chivo_control' => 'tipo'
                           ,'chivo_id_cat'=>71
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Tipo de ampliación:'])

<!-- No Ampliacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_ampliacion', 'No Ampliacion:') !!}
    {!! Form::number('no_ampliacion', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_ampliacion', 'Fecha de la ampliación:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_ampliacion', null, ['class' => 'form-control datepicker','data-value'=>$costAmpliacion->fecha_ampliacion]) !!}
    @else
        {!! Form::text('fecha_ampliacion', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>

<!-- No Registro Ampliacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_registro_ampliacion', 'No. de Registro Ampliación:') !!}
    {!! Form::text('no_registro_ampliacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Avance Financiero Field -->
<div class="form-group col-sm-6">
    {!! Form::label('avance_financiero', 'Avance financiero:') !!}
    {!! Form::number('avance_financiero', null, ['class' => 'form-control']) !!}
</div>

<!-- Avance Obra Field -->
<div class="form-group col-sm-6">
    {!! Form::label('avance_obra', 'Avance de la obra:') !!}
    {!! Form::number('avance_obra', null, ['class' => 'form-control']) !!}
</div>

<!-- Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link', 'Link:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<!-- Observaciones Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}

    @if(isset($id_fase))
        <a href="{!! route('costAmpliacions.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costAmpliacions.index', ['id_proyecto'=>$costAmpliacion->id_proyecto,'id_fase'=>$costAmpliacion->id_fase]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif

</div>
