<!-- Id Cost Proyecto Supervision Field -->
<div class="form-group">
    {!! Form::label('id_cost_proyecto_supervision', 'Id Cost Proyecto Supervision:') !!}
    <p>{!! $costProyectoSupervision->id_cost_proyecto_supervision !!}</p>
</div>

<!-- Id Proyecto Field -->
<div class="form-group">
    {!! Form::label('id_proyecto', 'Id Proyecto:') !!}
    <p>{!! $costProyectoSupervision->id_proyecto !!}</p>
</div>

<!-- Sup Invitacion Field -->
<div class="form-group">
    {!! Form::label('sup_invitacion', 'Sup Invitacion:') !!}
    <p>{!! $costProyectoSupervision->sup_invitacion !!}</p>
</div>

<!-- Sup Listado Ofe Field -->
<div class="form-group">
    {!! Form::label('sup_listado_ofe', 'Sup Listado Ofe:') !!}
    <p>{!! $costProyectoSupervision->sup_listado_ofe !!}</p>
</div>

<!-- Sup Acta Adj Field -->
<div class="form-group">
    {!! Form::label('sup_acta_adj', 'Sup Acta Adj:') !!}
    <p>{!! $costProyectoSupervision->sup_acta_adj !!}</p>
</div>

<!-- Sup Responsable Field -->
<div class="form-group">
    {!! Form::label('sup_responsable', 'Sup Responsable:') !!}
    <p>{!! $costProyectoSupervision->sup_responsable !!}</p>
</div>

<!-- Sup Monto Contratado Field -->
<div class="form-group">
    {!! Form::label('sup_monto_contratado', 'Sup Monto Contratado:') !!}
    <p>{!! $costProyectoSupervision->sup_monto_contratado !!}</p>
</div>

<!-- Sup Plazo Contrato Field -->
<div class="form-group">
    {!! Form::label('sup_plazo_contrato', 'Sup Plazo Contrato:') !!}
    <p>{!! $costProyectoSupervision->sup_plazo_contrato !!}</p>
</div>

<!-- Sup Tipo Contrato Field -->
<div class="form-group">
    {!! Form::label('sup_tipo_contrato', 'Sup Tipo Contrato:') !!}
    <p>{!! $costProyectoSupervision->sup_tipo_contrato !!}</p>
</div>

<!-- Sup Contrato Alcance Field -->
<div class="form-group">
    {!! Form::label('sup_contrato_alcance', 'Sup Contrato Alcance:') !!}
    <p>{!! $costProyectoSupervision->sup_contrato_alcance !!}</p>
</div>

<!-- Sup Programa Trabajo Field -->
<div class="form-group">
    {!! Form::label('sup_programa_trabajo', 'Sup Programa Trabajo:') !!}
    <p>{!! $costProyectoSupervision->sup_programa_trabajo !!}</p>
</div>

<!-- Sup F Inicio Field -->
<div class="form-group">
    {!! Form::label('sup_f_inicio', 'Sup F Inicio:') !!}
    <p>{!! $costProyectoSupervision->sup_f_inicio !!}</p>
</div>

<!-- Sup F Entrega Field -->
<div class="form-group">
    {!! Form::label('sup_f_entrega', 'Sup F Entrega:') !!}
    <p>{!! $costProyectoSupervision->sup_f_entrega !!}</p>
</div>

<!-- Sup Responsable Ea Field -->
<div class="form-group">
    {!! Form::label('sup_responsable_ea', 'Sup Responsable Ea:') !!}
    <p>{!! $costProyectoSupervision->sup_responsable_ea !!}</p>
</div>

<!-- Sup F Aprobacion Contrato Field -->
<div class="form-group">
    {!! Form::label('sup_f_aprobacion_contrato', 'Sup F Aprobacion Contrato:') !!}
    <p>{!! $costProyectoSupervision->sup_f_aprobacion_contrato !!}</p>
</div>

<!-- Sup Anticipo Field -->
<div class="form-group">
    {!! Form::label('sup_anticipo', 'Sup Anticipo:') !!}
    <p>{!! $costProyectoSupervision->sup_anticipo !!}</p>
</div>

<!-- Sup No Pagos Field -->
<div class="form-group">
    {!! Form::label('sup_no_pagos', 'Sup No Pagos:') !!}
    <p>{!! $costProyectoSupervision->sup_no_pagos !!}</p>
</div>

<!-- Sup Fechas Field -->
<div class="form-group">
    {!! Form::label('sup_fechas', 'Sup Fechas:') !!}
    <p>{!! $costProyectoSupervision->sup_fechas !!}</p>
</div>

<!-- Sup Mod Plazo Field -->
<div class="form-group">
    {!! Form::label('sup_mod_plazo', 'Sup Mod Plazo:') !!}
    <p>{!! $costProyectoSupervision->sup_mod_plazo !!}</p>
</div>

<!-- Resp Mod Monto Field -->
<div class="form-group">
    {!! Form::label('resp_mod_monto', 'Resp Mod Monto:') !!}
    <p>{!! $costProyectoSupervision->resp_mod_monto !!}</p>
</div>

<!-- Nueva F Finaliza Field -->
<div class="form-group">
    {!! Form::label('nueva_f_finaliza', 'Nueva F Finaliza:') !!}
    <p>{!! $costProyectoSupervision->nueva_f_finaliza !!}</p>
</div>

<!-- Sup Mod Monto Field -->
<div class="form-group">
    {!! Form::label('sup_mod_monto', 'Sup Mod Monto:') !!}
    <p>{!! $costProyectoSupervision->sup_mod_monto !!}</p>
</div>

<!-- Sup Resp Mod Monto Field -->
<div class="form-group">
    {!! Form::label('sup_resp_mod_monto', 'Sup Resp Mod Monto:') !!}
    <p>{!! $costProyectoSupervision->sup_resp_mod_monto !!}</p>
</div>

<!-- Sup Porcentaje Field -->
<div class="form-group">
    {!! Form::label('sup_porcentaje', 'Sup Porcentaje:') !!}
    <p>{!! $costProyectoSupervision->sup_porcentaje !!}</p>
</div>

<!-- Sup Acta Recepcion Field -->
<div class="form-group">
    {!! Form::label('sup_acta_recepcion', 'Sup Acta Recepcion:') !!}
    <p>{!! $costProyectoSupervision->sup_acta_recepcion !!}</p>
</div>

<!-- Sup Acta Recepcion F Field -->
<div class="form-group">
    {!! Form::label('sup_acta_recepcion_f', 'Sup Acta Recepcion F:') !!}
    <p>{!! $costProyectoSupervision->sup_acta_recepcion_f !!}</p>
</div>

<!-- Sup Finiquito F Field -->
<div class="form-group">
    {!! Form::label('sup_finiquito_f', 'Sup Finiquito F:') !!}
    <p>{!! $costProyectoSupervision->sup_finiquito_f !!}</p>
</div>

<!-- Fianza Sos Oferta Field -->
<div class="form-group">
    {!! Form::label('fianza_sos_oferta', 'Fianza Sos Oferta:') !!}
    <p>{!! $costProyectoSupervision->fianza_sos_oferta !!}</p>
</div>

<!-- Fianza Cumplimiento Field -->
<div class="form-group">
    {!! Form::label('fianza_cumplimiento', 'Fianza Cumplimiento:') !!}
    <p>{!! $costProyectoSupervision->fianza_cumplimiento !!}</p>
</div>

<!-- Sup Afianzadora Field -->
<div class="form-group">
    {!! Form::label('sup_afianzadora', 'Sup Afianzadora:') !!}
    <p>{!! $costProyectoSupervision->sup_afianzadora !!}</p>
</div>

