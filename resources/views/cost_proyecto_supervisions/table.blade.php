<table class="table table-responsive" id="costProyectoSupervisions-table">
    <thead>
        <tr>
            <th>Id Proyecto</th>
        <th>Sup Invitacion</th>
        <th>Sup Listado Ofe</th>
        <th>Sup Acta Adj</th>
        <th>Sup Responsable</th>
        <th>Sup Monto Contratado</th>
        <th>Sup Plazo Contrato</th>
        <th>Sup Tipo Contrato</th>
        <th>Sup Contrato Alcance</th>
        <th>Sup Programa Trabajo</th>
        <th>Sup F Inicio</th>
        <th>Sup F Entrega</th>
        <th>Sup Responsable Ea</th>
        <th>Sup F Aprobacion Contrato</th>
        <th>Sup Anticipo</th>
        <th>Sup No Pagos</th>
        <th>Sup Fechas</th>
        <th>Sup Mod Plazo</th>
        <th>Resp Mod Monto</th>
        <th>Nueva F Finaliza</th>
        <th>Sup Mod Monto</th>
        <th>Sup Resp Mod Monto</th>
        <th>Sup Porcentaje</th>
        <th>Sup Acta Recepcion</th>
        <th>Sup Acta Recepcion F</th>
        <th>Sup Finiquito F</th>
        <th>Fianza Sos Oferta</th>
        <th>Fianza Cumplimiento</th>
        <th>Sup Afianzadora</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($costProyectoSupervisions as $costProyectoSupervision)
        <tr>
            <td>{!! $costProyectoSupervision->id_proyecto !!}</td>
            <td>{!! $costProyectoSupervision->sup_invitacion !!}</td>
            <td>{!! $costProyectoSupervision->sup_listado_ofe !!}</td>
            <td>{!! $costProyectoSupervision->sup_acta_adj !!}</td>
            <td>{!! $costProyectoSupervision->sup_responsable !!}</td>
            <td>{!! $costProyectoSupervision->sup_monto_contratado !!}</td>
            <td>{!! $costProyectoSupervision->sup_plazo_contrato !!}</td>
            <td>{!! $costProyectoSupervision->sup_tipo_contrato !!}</td>
            <td>{!! $costProyectoSupervision->sup_contrato_alcance !!}</td>
            <td>{!! $costProyectoSupervision->sup_programa_trabajo !!}</td>
            <td>{!! $costProyectoSupervision->sup_f_inicio !!}</td>
            <td>{!! $costProyectoSupervision->sup_f_entrega !!}</td>
            <td>{!! $costProyectoSupervision->sup_responsable_ea !!}</td>
            <td>{!! $costProyectoSupervision->sup_f_aprobacion_contrato !!}</td>
            <td>{!! $costProyectoSupervision->sup_anticipo !!}</td>
            <td>{!! $costProyectoSupervision->sup_no_pagos !!}</td>
            <td>{!! $costProyectoSupervision->sup_fechas !!}</td>
            <td>{!! $costProyectoSupervision->sup_mod_plazo !!}</td>
            <td>{!! $costProyectoSupervision->resp_mod_monto !!}</td>
            <td>{!! $costProyectoSupervision->nueva_f_finaliza !!}</td>
            <td>{!! $costProyectoSupervision->sup_mod_monto !!}</td>
            <td>{!! $costProyectoSupervision->sup_resp_mod_monto !!}</td>
            <td>{!! $costProyectoSupervision->sup_porcentaje !!}</td>
            <td>{!! $costProyectoSupervision->sup_acta_recepcion !!}</td>
            <td>{!! $costProyectoSupervision->sup_acta_recepcion_f !!}</td>
            <td>{!! $costProyectoSupervision->sup_finiquito_f !!}</td>
            <td>{!! $costProyectoSupervision->fianza_sos_oferta !!}</td>
            <td>{!! $costProyectoSupervision->fianza_cumplimiento !!}</td>
            <td>{!! $costProyectoSupervision->sup_afianzadora !!}</td>
            <td>
                {!! Form::open(['route' => ['costProyectoSupervisions.destroy', $costProyectoSupervision->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costProyectoSupervisions.show', [$costProyectoSupervision->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('costProyectoSupervisions.edit', [$costProyectoSupervision->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>