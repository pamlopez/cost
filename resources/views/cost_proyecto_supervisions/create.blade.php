@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Cost Proyecto Supervision
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'costProyectoSupervisions.store']) !!}

                        @include('cost_proyecto_supervisions.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
