<!-- Id Proyecto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_proyecto', 'Id Proyecto:') !!}
    {!! Form::number('id_proyecto', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Invitacion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_invitacion', 'Sup Invitacion:') !!}
    {!! Form::text('sup_invitacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Listado Ofe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_listado_ofe', 'Sup Listado Ofe:') !!}
    {!! Form::text('sup_listado_ofe', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Acta Adj Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_acta_adj', 'Sup Acta Adj:') !!}
    {!! Form::text('sup_acta_adj', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Responsable Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_responsable', 'Sup Responsable:') !!}
    {!! Form::text('sup_responsable', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Monto Contratado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_monto_contratado', 'Sup Monto Contratado:') !!}
    {!! Form::number('sup_monto_contratado', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Plazo Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_plazo_contrato', 'Sup Plazo Contrato:') !!}
    {!! Form::text('sup_plazo_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Tipo Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_tipo_contrato', 'Sup Tipo Contrato:') !!}
    {!! Form::text('sup_tipo_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Contrato Alcance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_contrato_alcance', 'Sup Contrato Alcance:') !!}
    {!! Form::text('sup_contrato_alcance', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Programa Trabajo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_programa_trabajo', 'Sup Programa Trabajo:') !!}
    {!! Form::number('sup_programa_trabajo', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup F Inicio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_f_inicio', 'Sup F Inicio:') !!}
    {!! Form::date('sup_f_inicio', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup F Entrega Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_f_entrega', 'Sup F Entrega:') !!}
    {!! Form::date('sup_f_entrega', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Responsable Ea Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_responsable_ea', 'Sup Responsable Ea:') !!}
    {!! Form::text('sup_responsable_ea', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup F Aprobacion Contrato Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_f_aprobacion_contrato', 'Sup F Aprobacion Contrato:') !!}
    {!! Form::date('sup_f_aprobacion_contrato', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Anticipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_anticipo', 'Sup Anticipo:') !!}
    {!! Form::number('sup_anticipo', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup No Pagos Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_no_pagos', 'Sup No Pagos:') !!}
    {!! Form::number('sup_no_pagos', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Fechas Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_fechas', 'Sup Fechas:') !!}
    {!! Form::text('sup_fechas', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Mod Plazo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_mod_plazo', 'Sup Mod Plazo:') !!}
    {!! Form::text('sup_mod_plazo', null, ['class' => 'form-control']) !!}
</div>

<!-- Resp Mod Monto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('resp_mod_monto', 'Resp Mod Monto:') !!}
    {!! Form::text('resp_mod_monto', null, ['class' => 'form-control']) !!}
</div>

<!-- Nueva F Finaliza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nueva_f_finaliza', 'Nueva F Finaliza:') !!}
    {!! Form::date('nueva_f_finaliza', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Mod Monto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_mod_monto', 'Sup Mod Monto:') !!}
    {!! Form::number('sup_mod_monto', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Resp Mod Monto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_resp_mod_monto', 'Sup Resp Mod Monto:') !!}
    {!! Form::text('sup_resp_mod_monto', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Porcentaje Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_porcentaje', 'Sup Porcentaje:') !!}
    {!! Form::number('sup_porcentaje', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Acta Recepcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_acta_recepcion', 'Sup Acta Recepcion:') !!}
    {!! Form::text('sup_acta_recepcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Acta Recepcion F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_acta_recepcion_f', 'Sup Acta Recepcion F:') !!}
    {!! Form::date('sup_acta_recepcion_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Finiquito F Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_finiquito_f', 'Sup Finiquito F:') !!}
    {!! Form::date('sup_finiquito_f', null, ['class' => 'form-control']) !!}
</div>

<!-- Fianza Sos Oferta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fianza_sos_oferta', 'Fianza Sos Oferta:') !!}
    {!! Form::text('fianza_sos_oferta', null, ['class' => 'form-control']) !!}
</div>

<!-- Fianza Cumplimiento Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('fianza_cumplimiento', 'Fianza Cumplimiento:') !!}
    {!! Form::textarea('fianza_cumplimiento', null, ['class' => 'form-control']) !!}
</div>

<!-- Sup Afianzadora Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sup_afianzadora', 'Sup Afianzadora:') !!}
    {!! Form::text('sup_afianzadora', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('costProyectoSupervisions.index') !!}" class="btn btn-default">Cancel</a>
</div>
