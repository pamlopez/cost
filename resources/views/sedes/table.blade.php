<table class="table table-responsive" id="sedes-table">
    <thead>
        <tr>
            <th>Descripcion</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($sedes as $sede)
        <tr>
            <td>{!! $sede->descripcion !!}</td>
            <td>
                {!! Form::open(['route' => ['sedes.destroy', $sede->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('sedes.show', [$sede->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('sedes.edit', [$sede->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>