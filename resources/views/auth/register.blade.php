@extends('layouts.app')
@section('content')

    <div class="box box-maroon">
        <div class="box-header with-border">
            <div class="register-logo">
                <b><i class="fa fa-user-plus" aria-hidden="true"></i> Registrar </b>nuevo usuario
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-6 col-md-offset-3">
                <div class="register-box-body" style="border-radius: 0%">
                    <p class="login-box-msg"><i class="fa fa-plus-circle" aria-hidden="true"></i><b> Ingrese los datos del usuario</b></p>

                    <form method="post" action="{{ url('/register') }}">

                        {!! csrf_field() !!}

                        <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nombre completo">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>

                            @if ($errors->has('name'))
                                <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                            @endif
                        </div>

                        <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo electrónico">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                            @if ($errors->has('email'))
                                <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                            @endif
                        </div>

                        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" name="password" placeholder="Contraseña">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                            @if ($errors->has('password'))
                                <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                            @endif
                        </div>

                        <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirmar contraseña">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                            @endif
                        </div>
                        <br><br>
                        <div class="col-xs-4 col-md-offset-2">
                            <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-floppy-o" aria-hidden="true"></i> Registrar</button>
                        </div>
                        <div class="col-xs-4">
                            <a href="{{ url('dash/ai') }}" type="submit" class="btn btn-danger btn-block btn-flat"><i class="fa fa-close" aria-hidden="true"></i> Cancelar</a>
                        </div>
                    </form>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
<!-- /.register-box -->

@endsection
