@extends('layouts.app')
@section('content')
    <div class="row">
        <a href="{!!url('administracion')!!}" class="btn btn-info col-sm-offset-1"> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Volver</a>
    </div>

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-user" aria-hidden="true"></i> Asignaci&oacute;n de accesos al sistema</h3>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <tr>
                        <th width="20%">Nombre del usuario:</th>
                        <th>{{$user->name}}</th>
                        <th width="15%">Correo electr&oacute;nico:</th>
                        <td>{{$user->email}}</td>
                    </tr>
                    <tr>
                        <th>Fecha de creaci&oacute;n:</th>
                        <td colspan="3">{{\Carbon\Carbon::parse($user->created_at)->format('d/m/Y')}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-cogs" aria-hidden="true"></i> Elegir los accesos al sistema para este usuario</h3>
            </div>
            {!! Form::open(['url' => 'administracion']) !!}
            <div class="panel-body">
                @include('auth.administracion._form')
            </div>
            <div class="panel-footer">
                <div class="form-group row">
                    <div align="center">
                        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('administracion.index') !!}" class="btn btn-default">Cancelar</a>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>


@endsection