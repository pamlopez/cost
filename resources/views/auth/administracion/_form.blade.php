{!! Form::hidden('id_sede',isset($user->id_sede)?$user->id_sede:1,['class' => 'form-control']) !!}
{!! Form::hidden('id_users', $user->id, ['class' => 'form-control']) !!}

<div class="form-group col-sm-12">
    <div class="form-group col-sm-10 col-md-offset-1">
        {!! Form::select('id_rol', \App\Models\users_rol::listado_items(),$seleccionados_roles,['class' => 'form-control js-example-basic-multiple','name'=>'roles[]','multiple'=>'multiple','required']) !!}
    </div>
</div>

@push("head")
    {{-- Para que los AJAX no den problemas, esto tiene que ir al principio.  Debe ir acompañado de javascript que se coloca al final  --}}
    <head>
        <meta name="csrf-token" content="{!! csrf_token() !!}">
    </head>
@endpush
@push('javascript')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script>
        $('.js-example-basic-multiple').select2({
            placeholder: 'Selecciona los roles'
        });
    </script>

@endpush