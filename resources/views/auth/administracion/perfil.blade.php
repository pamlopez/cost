@extends('layouts.app')
@section('content')
    @include('adminlte-templates::common.errors')
    <div class="row">
        <a href="{!!url('costProyectos')!!}" class="btn btn-info" style="margin-left: 10%"> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Volver al panel principal</a>
    </div>
    <p style="font-size: 20pt" align="center">
        <b>Perfil del usuario</b>
    </p>

    <div class="row">
        <div class="col-md-6 col-sm-offset-3">
            <div class="box">
                <div class="box-body">
                    <table class="table table-striped table-striped">
                        <tr>
                            <th>Nombre registrado:</th>
                            <td>{{$user->name}}</td>
                        </tr>
                        <tr>
                            <th>Correo electrónico:</th>
                            <td>{{$user->email}}</td>
                        </tr>
                        <tr>
                            <th>Fecha de creación de tu usuario:</th>
                            <td>{{$user->created_at}}</td>
                        </tr>
                        <tr>
                            <th>Fecha de la última actualización de tu usuario:</th>
                            <td>{{$user->updated_at}}</td>
                        </tr>
                    </table>

                    <p style="font-size: 20pt" align="center">
                        <b>Roles actualmente asignados</b>
                    </p>

                    <table class="table table-striped table-striped">
                        <tr>
                            <th>Roles o accesos en el sistema:</th>
                            <td>
                                <ul>
                                    @foreach($roles_asignados as $k => $v)
                                        <li>{{$v->FmtIdRol}}</li>
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="box-footer" align="center">
                    <a  href="{!! url('password_perfil_change', [Auth::user()->id]) !!}"
                       class="btn  btn-flat btn_login">
                        <i class="fa fa-refresh" aria-hidden="true"></i>
                        &nbsp; Cambiar contraseña
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
