@extends('layouts.app')
@section('content')
    @include('adminlte-templates::common.errors')
    @include('auth.administracion.filtros_busqueda')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h4 align="center">
                <a style="margin-top: -1%;" type="button " class="btn bg-primary pull-right" href="{{url('/register')}}"><i class="fa fa-plus-circle" aria-hidden="true"></i>  Agregar usuario </a>
            </h4>
        </div>
        <div class="box-body">
            <div class="table-reponsive">
                <table class="table table-bordered table-striped">
                    <thead>
                    <th>#</th>
                    <th>Nombre de usuario</th>
                    <th>Correo</th>
                    <th>Rol / acceso a la informaci&oacute;n</th>
                    <th>Fecha de creaci&oacute;n</th>
                    <th>Última actualización</th>
                    <th>Estado</th>
                    <th>Acciones</th>
                    </thead>
                    <tbody>
                    @foreach($usuarios as $id => $value)
                        @php($rol=$value->rel_rol)
                        <tr>
                            <td>{{  (($usuarios->currentPage() -1) * $usuarios->perPage())+1+$id }}</td>
                            <td><b>{{$value->name}}</b></td>
                            <td>{{$value->email}}</td>
                            <td>
                                @foreach($rol as $i => $r)
                                    <ul>
                                        <li>{{$r->FmtIdRol}}</li>
                                    </ul>
                                @endforeach
                            </td>
                            <td>{{\Carbon\Carbon::parse($value->created_at)->format('d/m/Y')}}</td>
                            <td>{{\Carbon\Carbon::parse($value->updated_at)->format('d/m/Y')}}</td>
                            <td>
                                @if($value->estado == 1)
                                    <b style="color:#3c8dbc">Deshabilitado</b>
                                @else
                                    <b style="color:#00A65A ">Habilitado</b>
                                @endif
                            </td>
                            <td>
                                {!! Form::open(['route' => ['administracion.destroy', $value->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('administracion.edit', [$value->id]) !!}" class='btn btn-warning btn-sm' data-toggle="tooltip" data-placement="top" title="Editar datos del usuario">
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    @if($value->estado == 1)
                                        <a href="{!! url('password_habilitar', [$value->id]) !!}" class='btn btn-primary btn-sm' data-toggle="tooltip" data-placement="top" title="DESACTIVAR el usuario" onclick="return confirm('Esta seguro que desea DESACTIVAR a este usuario?')">
                                            <i class="fa fa-ban" aria-hidden="true"></i>
                                        </a>
                                    @else
                                        <a href="{!! url('password_deshabilitar', [$value->id]) !!}" class='btn bg-navy btn-sm' data-toggle="tooltip" data-placement="top" title="HABILITAR el usuario" onclick="return confirm('Esta seguro que desea HABILITAR a este usuario?')">
                                            <i class="fa fa-ban" aria-hidden="true"></i>
                                        </a>
                                    @endif
                                    <a href="{!! url('reiniciar_password', [$value->id]) !!}" class='btn btn-success btn-sm' data-toggle="tooltip" data-placement="top" title="Reiniciar contraseña">
                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                    </a>
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-footer">
            {!! $usuarios->appends(request()->except('page'))->links() !!}
        </div>
        <!-- /.box-body -->
    </div>
@endsection
