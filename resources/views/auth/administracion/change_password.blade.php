@extends('layouts.app')
@section('content')
    <div class="row">
        <a href="{!!url('costProyectos')!!}" class="btn btn-info" style="margin-left: 10%"> <i
                    class="fa fa-long-arrow-left" aria-hidden="true"></i> Volver al panel principal</a>
    </div>
    <p style="font-size: 20pt" align="center">
        <b>
            <i class="fa fa-refresh" aria-hidden="true"></i>
            &nbsp; Cambiar contraseña
        </b><br/>
        <small>*Debe escribir un m&iacute;nimo de 6 carácteres</small>
    </p>

    <div class="row">
        <div class="col-md-4 col-sm-offset-4">
            {!! Form::open(['url' => ['password_change'], 'method' => 'post']) !!}
            <input type="hidden" class="form-control" name="id" value="{{$id}}">

            @if($error ==true)
                <p align="center">
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <strong>{{$error_titulo}}</strong>
                        <br/> {{$error_descripcion}}
                    </div>
                </p>
            @endif

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <label><b>Nueva contraseña:</b></label>
                <input type="password" class="form-control" placeholder="Contraseña" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>@endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <label><b>Confirme la nueva contraseña:</b></label>
                <input type="password" class="form-control" placeholder="Contraseña" name="password_dos">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>@endif
            </div>
            <div class="col-sm-offset-4">
                <button type="submit" class="btn  btn-success"><i class="fa fa-save" aria-hidden="true"></i>Guardar
                </button>
                <a href="{!! url('password_perfil_change', [$id]) !!}" type="submit" class="btn  btn-danger"><i
                            class="fa fa-close" aria-hidden="true"></i>Cancelar</a>
            </div>
            {{ Form::close() }}
        </div>

    </div>
@endsection
