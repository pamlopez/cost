@extends('layouts.app')
@include('flash::message')
@section('content')
    <section class="content-header">
        <h1>
            Fianza
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_fianzas.show_fields')
                </div>
                <a href="{!! route('costFianzas.index', ['id_proyecto'=>$costFianza->id_proyecto,'id_fase'=>$costFianza->id_fase]) !!}"
                   class="btn btn-default">Atrás</a>
            </div>
        </div>
    </div>
@endsection
