@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
           Edita Fianza
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costFianza, ['route' => ['costFianzas.update', $costFianza->id], 'method' => 'patch']) !!}

                        @include('cost_fianzas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection