<table class="table table-responsive" id="costFianzas-table">
    <thead>
    <tr>
        <th>#</th>
        <th>Tipo</th>
        <th>No Fianza</th>
        <th>Monto</th>
        <th>Fecha de la Fianza</th>
        <th>Link</th>
        <th colspan="3">Acción</th>
    </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($costFianzas as $costFianza)
        @php($i++)
        <tr>
            <td>{!! $i !!}</td>
            @if($costFianza->tipo>0)
                <td>{!! $costFianza->fmt_tipo !!}</td>
            @else
                <td>{!! $costFianza->tipo_fianza !!}</td>
            @endif
            <td>{!! $costFianza->no_fianza !!}</td>
            <td>{!! $costFianza->monto !!}</td>
            <td>{!! $costFianza->fecha_fianza !!}</td>
            <td>{!! $costFianza->link !!}</td>
            <td>
                {!! Form::open(['route' => ['costFianzas.destroy', $costFianza->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costFianzas.show', [$costFianza->id]) !!}" class='btn btn-info btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                    @if(Auth::check())
                    <a href="{!! route('costFianzas.edit', [$costFianza->id]) !!}" class='btn btn-warning btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    @endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>