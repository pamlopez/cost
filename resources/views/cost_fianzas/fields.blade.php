{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costFianza->id_proyecto, ['class' => 'form-control']) !!}
{!! Form::hidden('id_fase',isset($id_fase)?$id_fase:$costFianza->id_fase, ['class' => 'form-control']) !!}


@include('chivos.catalogo', ['chivo_control' => 'tipo'
                           ,'chivo_id_cat'=>69
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Tipo de fianza:'])

<!-- No Fianza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza', 'No Fianza:') !!}
    {!! Form::text('no_fianza', null, ['class' => 'form-control']) !!}
</div>

<!-- Monto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('monto', 'Monto:') !!}
    {!! Form::text('monto', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_fianza', 'Fecha de la fianza:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_fianza', null, ['class' => 'form-control datepicker','data-value'=>$costFianza->fecha_fianza]) !!}
    @else
        {!! Form::text('fecha_fianza', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>

<!-- Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link', 'Link:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('clase_fianza', 'Clase de la Fianza:') !!}
    {!! Form::text('clase_fianza', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('plazo_fianza', 'Plazo de la fianza:') !!}
    {!! Form::text('plazo_fianza', null, ['class' => 'form-control']) !!}
</div>

@include('chivos.catalogo', ['chivo_control' => 'entidad_afianzadora'
                           ,'chivo_id_cat'=>75
                           , 'chivo_default'=>null
                           ,'chivo_texto'=>'Entidad Afianzadora:'])

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    @if(isset($id_fase))
        <a href="{!! route('costFianzas.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costFianzas.index', ['id_proyecto'=>$costFianza->id_proyecto,'id_fase'=>$costFianza->id_fase]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif
</div>
