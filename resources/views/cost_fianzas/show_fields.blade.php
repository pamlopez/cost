@if($costFianza->tipo>0)
    <!-- Tipo Field -->
    <div class="form-group col-md-6">
        {!! Form::label('fmt_tipo', 'Tipo de fianza:') !!}
        <p>{!! $costFianza->fmt_tipo !!}</p>
    </div>
@else

@endif

<!-- No Fianza Field -->
<div class="form-group col-md-6">
    {!! Form::label('no_fianza', 'No Fianza:') !!}
    <p>{!! $costFianza->no_fianza !!}</p>
</div>

<!-- Monto Field -->
<div class="form-group col-md-6">
    {!! Form::label('monto', 'Monto:') !!}
    <p>{!! $costFianza->monto !!}</p>
</div>

<!-- Fecha Fianza Field -->
<div class="form-group col-md-6">
    {!! Form::label('fecha_fianza', 'Fecha Fianza:') !!}
    <p>{!! $costFianza->fecha_fianza !!}</p>
</div>

<!-- Link Field -->
<div class="form-group col-md-6">
    {!! Form::label('link', 'Link:') !!}
    <p>{!! $costFianza->link !!}</p>
</div>

<div class="form-group col-md-6">
    {!! Form::label('clase_fianza', 'Clase de Fianza:') !!}
    <p>{!! $costFianza->clase_fianza !!}</p>
</div>


<div class="form-group col-md-6">
    {!! Form::label('plazo_fianza', 'Plazo de Fianza:') !!}
    <p>{!! $costFianza->clase_fianza !!}</p>
</div>

<div class="form-group col-md-6">
    {!! Form::label('fmt_entidad_afianzadora', 'Entidad afianzadora:') !!}
    <p>{!! $costFianza->fmt_entidad_afianzadora !!}</p>
</div>