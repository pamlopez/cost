<table class="table table-responsive" id="costAcuerdoFinanciamientos-table">
    <thead>
        <tr>
            <th>Id Proyecto</th>
        <th>Tipo</th>
        <th>Partida Presupuestaria</th>
        <th>Constitucional</th>
        <th>Iva Paz</th>
        <th>Circulacion Vehiculos</th>
        <th>Petroleo</th>
        <th>Consejo Desarrollo</th>
        <th>Fondos Propios</th>
        <th>Otros Descripcion</th>
        <th>Otros</th>
        <th>Donacion</th>
        <th>Prestamo</th>
        <th>Fecha Aprobacion</th>
        <th>Aporte Beneficiario</th>
        <th>Transferencia</th>
        <th>Observaciones</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($costAcuerdoFinanciamientos as $costAcuerdoFinanciamiento)
        <tr>
            <td>{!! $costAcuerdoFinanciamiento->id_proyecto !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->tipo !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->partida_presupuestaria !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->constitucional !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->iva_paz !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->circulacion_vehiculos !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->petroleo !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->consejo_desarrollo !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->fondos_propios !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->otros_descripcion !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->otros !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->donacion !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->prestamo !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->fecha_aprobacion !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->aporte_beneficiario !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->transferencia !!}</td>
            <td>{!! $costAcuerdoFinanciamiento->observaciones !!}</td>
            <td>
                {!! Form::open(['route' => ['costAcuerdoFinanciamientos.destroy', $costAcuerdoFinanciamiento->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costAcuerdoFinanciamientos.show', [$costAcuerdoFinanciamiento->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('costAcuerdoFinanciamientos.edit', [$costAcuerdoFinanciamiento->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>