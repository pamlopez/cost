@extends('layouts.app')
@include('flash::message')
@section('content')
    <section class="content-header">
        <h1>
            Acuerdos de Financiamiento
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('cost_acuerdo_financiamientos.show_fields')
                </div>
                <div class="text-left">
                    <a href="{!! route('costProyectos.show', [$costAcuerdoFinanciamiento->id_proyecto,'a'=>1]) !!}"
                       class="btn btn-default">Atrás</a>
                </div>

            </div>
        </div>
    </div>
@endsection
