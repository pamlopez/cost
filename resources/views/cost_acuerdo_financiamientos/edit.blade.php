@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Acuerdos de Financiamiento
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costAcuerdoFinanciamiento, ['route' => ['costAcuerdoFinanciamientos.update', $costAcuerdoFinanciamiento->id], 'method' => 'patch']) !!}

                        @include('cost_acuerdo_financiamientos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection