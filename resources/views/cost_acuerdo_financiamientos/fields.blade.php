{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costAcuerdoFinanciamiento->id_proyecto, ['class' => 'form-control']) !!}
{!! Form::hidden('tipo',isset($tipo)?$tipo:$costAcuerdoFinanciamiento->tipo, ['class' => 'form-control']) !!}
{!! Form::hidden('id_fase',isset($id_fase)?$id_fase:$costAcuerdoFinanciamiento->id_fase, ['class' => 'form-control']) !!}

<!-- Constitucional Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('constitucional', '10% Constitucional(Monto):') !!}
    {!! Form::text('constitucional', null, ['class' => 'form-control']) !!}
</div>

<!-- Iva Paz Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('iva_paz', 'IVA-PAZ(Monto):') !!}
    {!! Form::text('iva_paz', null, ['class' => 'form-control']) !!}
</div>

<!-- Circulacion Vehiculos Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('circulacion_vehiculos', 'Circulación Vehículos (Monto):') !!}
    {!! Form::text('circulacion_vehiculos', null, ['class' => 'form-control']) !!}
</div>

<!-- Petroleo Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('petroleo', 'Petróleo (Monto):') !!}
    {!! Form::text('petroleo', null, ['class' => 'form-control']) !!}
</div>

<!-- Consejo Desarrollo Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('consejo_desarrollo', 'Consejo de Desarrollo (Monto):') !!}
    {!! Form::text('consejo_desarrollo', null, ['class' => 'form-control']) !!}
</div>

<!-- Fondos Propios Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('fondos_propios', 'Fondos Propios (Monto):') !!}
    {!! Form::text('fondos_propios', null, ['class' => 'form-control']) !!}
</div>


<!-- Donacion Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('donacion', 'Donación (Monto):') !!}
    {!! Form::text('donacion', null, ['class' => 'form-control']) !!}
</div>

<!-- Prestamo Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('prestamo', 'Prestamo (Monto):') !!}
    {!! Form::text('prestamo', null, ['class' => 'form-control']) !!}
</div>

<!-- Aporte Beneficiario Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('aporte_beneficiario', 'Aporte de beneficiario (Monto):') !!}
    {!! Form::text('aporte_beneficiario', null, ['class' => 'form-control']) !!}
</div>

<!-- Transferencia Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('transferencia', 'Transferencia (Monto):') !!}
    {!! Form::text('transferencia', null, ['class' => 'form-control']) !!}
</div>


<!-- Otros Descripcion Field -->
<div class="form-group col-sm-3">
    {!! Form::label('otros_descripcion', 'Otros descripción:') !!}
    {!! Form::text('otros_descripcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Otros Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-money" aria-hidden="true"></i>
    {!! Form::label('otros', 'Otros (Monto):') !!}
    {!! Form::text('otros',null, ['class' => 'form-control']) !!}
</div>
<!-- Fecha Aprobacion Field -->
<div class="form-group col-sm-3">
    <i class="fa fa-calendar"></i>
    {!! Form::label('fecha_aprobacion', 'Fecha aprobación:') !!}
    @if($bnd_edit)
        {!! Form::text('fecha_aprobacion', null, ['class' => 'form-control datepicker','data-value'=>$costAcuerdoFinanciamiento->fecha_aprobacion]) !!}
    @else
        {!! Form::text('fecha_aprobacion', null, ['class' => 'form-control datepicker','data-value'=>\Carbon\Carbon::now()]) !!}
    @endif
</div>


<!-- Partida Presupuestaria Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('partida_presupuestaria', 'Partida presupuestaria:') !!}
    {!! Form::textarea('partida_presupuestaria', null, ['class' => 'form-control']) !!}
</div>
<!-- Observaciones Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    {!! Form::textarea('observaciones', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">

    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    @if(isset($id_fase))

        <a href="{!! route('costProyectos.show', [$id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costProyectos.show', [$costAcuerdoFinanciamiento->id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif

</div>
