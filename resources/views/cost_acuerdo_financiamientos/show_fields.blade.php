<!-- Fecha Aprobacion Field -->
<div class="form-group col-sm-2">
    {!! Form::label('fecha_aprobacion', 'Fecha Aprobacion:') !!}
    <p>{!! $costAcuerdoFinanciamiento->fecha_aprobacion !!}</p>
</div>


<!-- Partida Presupuestaria Field -->
<div class="form-group col-sm-5">
    {!! Form::label('partida_presupuestaria', 'Partida Presupuestaria:') !!}
    <p>{!! $costAcuerdoFinanciamiento->partida_presupuestaria !!}</p>
</div>
<!-- Observaciones Field -->
<div class="form-group col-sm-5">
    {!! Form::label('observaciones', 'Observaciones:') !!}
    <p>{!! $costAcuerdoFinanciamiento->observaciones !!}</p>
</div>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <table class="table table-striped">
            <tr>
                <th>Fuente de Financiamiento</th>
                <th>Monto</th>
            </tr>
            <tr>
                <td>10% Constitucional</td>
                <td>{!! $costAcuerdoFinanciamiento->constitucional !!}</td>
            </tr>
            <tr>
                <td>Iva Paz</td>
                <td>{!! $costAcuerdoFinanciamiento->iva_paz !!}</td>
            </tr>
            <tr>
                <td>Circulación de vehículos</td>
                <td>{!! $costAcuerdoFinanciamiento->circulacion_vehiculos !!}</td>
            </tr>
            <tr>
                <td>Petróleo</td>
                <td>{!! $costAcuerdoFinanciamiento->petroleo !!}</td>
            </tr>
            <tr>
                <td>Consejo de desarrollo</td>
                <td>{!! $costAcuerdoFinanciamiento->consejo_desarrollo !!}</td>
            </tr>
            <tr>
                <td>Fondos propios</td>
                <td>{!! $costAcuerdoFinanciamiento->fondos_propios !!}</td>
            </tr>
            <tr>
                <td>Donación</td>
                <td>{!! $costAcuerdoFinanciamiento->donacion !!}</td>
            </tr>
            <tr>
                <td>Prestamo</td>
                <td>{!! $costAcuerdoFinanciamiento->prestamo !!}</td>
            </tr>
            <tr>
                <td>{!! $costAcuerdoFinanciamiento->otros_descripcion !!}</td>
                <td>{!! $costAcuerdoFinanciamiento->otros !!}</td>
            </tr>
            <tr>
                <td>Aporte de beneficiario</td>
                <td>{!! $costAcuerdoFinanciamiento->aporte_beneficiario !!}</td>
            </tr>
            <tr>
                <td>Transferencia</td>
                <td>{!! $costAcuerdoFinanciamiento->transferencia !!}</td>
            </tr>
            <tr>
                <th>Total del monto asignado</th>
                <th>{!! $costAcuerdoFinanciamiento->monto_total_asignado !!}</th>
            </tr>
        </table>
    </div>
</div>





