<!-- No Modificaciones Plazo Field -->
<div class="form-group col-md-6">
    {!! Form::label('no_modificaciones_plazo', 'No. de modificaciones al plazo:') !!}
    <p>{!! $costLiquidacionTiempo->no_modificaciones_plazo !!}</p>
</div>

<!-- Cantidad Tiempo Modificado Field -->
<div class="form-group col-md-6">
    {!! Form::label('cantidad_tiempo_modificado', 'Cantidad de tiempo modificado:') !!}
    <p>{!! $costLiquidacionTiempo->cantidad_tiempo_modificado !!}</p>
</div>

<!-- Nueva Fecha Finaliza Field -->
<div class="form-group col-md-6">
    {!! Form::label('nueva_fecha_finaliza', 'Nueva Fecha Finaliza:') !!}
    <p>{!! $costLiquidacionTiempo->nueva_fecha_finaliza !!}</p>
</div>

<!-- Razones Cambio Plazo Field -->
<div class="form-group col-md-12">
    {!! Form::label('razones_cambio_plazo', 'Razones de cambio del plazo:') !!}
    <p>{!! $costLiquidacionTiempo->razones_cambio_plazo !!}</p>
</div>

<!-- No Fianza Field -->
<div class="form-group">
    {!! Form::label('no_fianza', 'No Fianza:') !!}
    <p>{!! $costLiquidacionTiempo->no_fianza !!}</p>
</div>

