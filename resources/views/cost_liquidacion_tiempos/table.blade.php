
<table class="table table-responsive" id="costLiquidacionTiempos-table">
    <thead>
    <tr>
        <th>#</th>
        <th>No. de modificaciones al plazo</th>
        <th>Cantidad de tiempo modificado</th>
        <th>Nueva fecha finaliza</th>
        <th>Razones de cambio de plazo</th>
        <th>No. Fianza</th>
        @if(!isset($a))
            <th colspan="3">Acción</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @php($i=0)
    @foreach($costLiquidacionTiempos as $costLiquidacionTiempo)
        @php($i++)

        <tr>
            <td>{!! $i !!}</td>
            <td>{!! $costLiquidacionTiempo->no_modificaciones_plazo !!}</td>
            <td>{!! $costLiquidacionTiempo->cantidad_tiempo_modificado !!}</td>
            <td>{!! $costLiquidacionTiempo->fmt_nueva_fecha_finaliza !!}</td>
            <td>{!! $costLiquidacionTiempo->razones_cambio_plazo !!}</td>
            <td>{!! $costLiquidacionTiempo->no_fianza !!}
            @if(!isset($a))
                <td>
                    {!! Form::open(['route' => ['costLiquidacionTiempos.destroy', $costLiquidacionTiempo->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('costLiquidacionTiempos.show', [$costLiquidacionTiempo->id]) !!}"
                           class='btn btn-info btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        @if(Auth::check())
                            <a href="{!! route('costLiquidacionTiempos.edit', [$costLiquidacionTiempo->id]) !!}"
                               class='btn btn-warning btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        @endif
                    </div>
                    {!! Form::close() !!}
                </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>