{!! Form::hidden('id_proyecto',isset($id_proyecto)?$id_proyecto:$costLiquidacionTiempo->id_proyecto, ['class' => 'form-control']) !!}


<!-- No Modificaciones Plazo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_modificaciones_plazo', 'No. de modificaciones al plazo:') !!}
    {!! Form::number('no_modificaciones_plazo', null, ['class' => 'form-control']) !!}
</div>

<!-- Cantidad Tiempo Modificado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cantidad_tiempo_modificado', 'Cantidad de tiempo modificado:') !!}
    {!! Form::text('cantidad_tiempo_modificado', null, ['class' => 'form-control']) !!}
</div>

<!-- Nueva Fecha Finaliza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nueva_fecha_finaliza', 'Nueva fecha de finalización:') !!}
    {!! Form::date('nueva_fecha_finaliza', null, ['class' => 'form-control']) !!}
</div>

<!-- Razones Cambio Plazo Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('razones_cambio_plazo', 'Razones de cambios en el plazo del contrato:') !!}
    {!! Form::textarea('razones_cambio_plazo', null, ['class' => 'form-control']) !!}
</div>

<!-- No Fianza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_fianza', 'No. de fianza actualizada al nuevo plazo:') !!}
    {!! Form::text('no_fianza', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    @if(isset($id_proyecto))
        <a href="{!! route('costLiquidacionTiempos.show', [$id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @else
        <a href="{!! route('costLiquidacionTiempos.show', [$costLiquidacionTiempo->id_proyecto,'a'=>1]) !!}"
           class="btn btn-default">Cancelar</a>
    @endif
</div>
