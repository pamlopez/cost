@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Nuevo ingreso de Ampliación de Plazo
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'costLiquidacionTiempos.store']) !!}

                        @include('cost_liquidacion_tiempos.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
