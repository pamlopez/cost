@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Editar el Tiempo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($costLiquidacionTiempo, ['route' => ['costLiquidacionTiempos.update', $costLiquidacionTiempo->id], 'method' => 'patch']) !!}

                        @include('cost_liquidacion_tiempos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection