<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_liquidacion_montoRequest;
use App\Http\Requests\Updatecost_liquidacion_montoRequest;
use App\Repositories\cost_liquidacion_montoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_liquidacion_montoController extends AppBaseController {

    /** @var  cost_liquidacion_montoRepository */
    private $costLiquidacionMontoRepository;

    public function __construct(cost_liquidacion_montoRepository $costLiquidacionMontoRepo)
    {
        $this->costLiquidacionMontoRepository = $costLiquidacionMontoRepo;
    }

    /**
     * Display a listing of the cost_liquidacion_monto.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $id_proyecto = $request->id_proyecto;
        $this->costLiquidacionMontoRepository->pushCriteria(new RequestCriteria($request));
        $costLiquidacionMontos = $this->costLiquidacionMontoRepository->findWhere([ 'id_proyecto' => $id_proyecto ])->all();

        return view('cost_liquidacion_montos.index', compact('id_proyecto'))
            ->with('costLiquidacionMontos', $costLiquidacionMontos);
    }

    /**
     * Show the form for creating a new cost_liquidacion_monto.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        return view('cost_liquidacion_montos.create', compact('bnd_edit', 'id_proyecto'));
    }

    /**
     * Store a newly created cost_liquidacion_monto in storage.
     *
     * @param Createcost_liquidacion_montoRequest $request
     *
     * @return Response
     */
    public function store(Createcost_liquidacion_montoRequest $request)
    {
        $input = $request->all();
        $costLiquidacionMonto = $this->costLiquidacionMontoRepository->create($input);
        Flash::success('El monto a liquidar se creo satisfactoriamente');
        return view('cost_liquidacion_montos.show')->with('costLiquidacionMonto', $costLiquidacionMonto);
    }
    /**
     * Display the specified cost_liquidacion_monto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costLiquidacionMonto = $this->costLiquidacionMontoRepository->findWithoutFail($id);

        if (empty($costLiquidacionMonto)) {
            Flash::error('Cost Liquidacion Monto not found');

            return redirect(route('costLiquidacionMontos.index'));
        }

        return view('cost_liquidacion_montos.show')->with('costLiquidacionMonto', $costLiquidacionMonto);
    }

    /**
     * Show the form for editing the specified cost_liquidacion_monto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costLiquidacionMonto = $this->costLiquidacionMontoRepository->findWithoutFail($id);
        $bnd_edit = true;
        if (empty($costLiquidacionMonto)) {
            Flash::error('Cost Liquidacion Monto not found');

            return redirect(route('costLiquidacionMontos.index'));
        }

        return view('cost_liquidacion_montos.edit', compact('bnd_edit'))->with('costLiquidacionMonto', $costLiquidacionMonto);
    }

    /**
     * Update the specified cost_liquidacion_monto in storage.
     *
     * @param  int $id
     * @param Updatecost_liquidacion_montoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_liquidacion_montoRequest $request)
    {
        $costLiquidacionMonto = $this->costLiquidacionMontoRepository->findWithoutFail($id);

        if (empty($costLiquidacionMonto)) {
            Flash::error('Cost Liquidacion Monto not found');

            return redirect(route('costLiquidacionMontos.index'));
        }

        $costLiquidacionMonto = $this->costLiquidacionMontoRepository->update($request->all(), $id);

        Flash::success('La información del monto de liquidación se actualizó satisfactoriamente');
        return view('cost_liquidacion_montos.show')->with('costLiquidacionMonto', $costLiquidacionMonto);
    }

    /**
     * Remove the specified cost_liquidacion_monto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costLiquidacionMonto = $this->costLiquidacionMontoRepository->findWithoutFail($id);

        if (empty($costLiquidacionMonto)) {
            Flash::error('Cost Liquidacion Monto not found');

            return redirect(route('costLiquidacionMontos.index'));
        }
        $id_proyecto = $costLiquidacionMonto->id_proyecto;
        $this->costLiquidacionMontoRepository->delete($id);

        Flash::success('El monto de liquidación ha sido borrado exitosamente');

        return redirect(route('costLiquidacionMontos.index', [ 'id_proyecto' => $id_proyecto ]));
    }
}
