<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_proyecto_supervisionRequest;
use App\Http\Requests\Updatecost_proyecto_supervisionRequest;
use App\Repositories\cost_proyecto_supervisionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_proyecto_supervisionController extends AppBaseController
{
    /** @var  cost_proyecto_supervisionRepository */
    private $costProyectoSupervisionRepository;

    public function __construct(cost_proyecto_supervisionRepository $costProyectoSupervisionRepo)
    {
        $this->costProyectoSupervisionRepository = $costProyectoSupervisionRepo;
    }

    /**
     * Display a listing of the cost_proyecto_supervision.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->costProyectoSupervisionRepository->pushCriteria(new RequestCriteria($request));
        $costProyectoSupervisions = $this->costProyectoSupervisionRepository->all();

        return view('cost_proyecto_supervisions.index')
            ->with('costProyectoSupervisions', $costProyectoSupervisions);
    }

    /**
     * Show the form for creating a new cost_proyecto_supervision.
     *
     * @return Response
     */
    public function create()
    {
        return view('cost_proyecto_supervisions.create');
    }

    /**
     * Store a newly created cost_proyecto_supervision in storage.
     *
     * @param Createcost_proyecto_supervisionRequest $request
     *
     * @return Response
     */
    public function store(Createcost_proyecto_supervisionRequest $request)
    {
        $input = $request->all();

        $costProyectoSupervision = $this->costProyectoSupervisionRepository->create($input);

        Flash::success('Cost Proyecto Supervision saved successfully.');

        return redirect(route('costProyectoSupervisions.index'));
    }

    /**
     * Display the specified cost_proyecto_supervision.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costProyectoSupervision = $this->costProyectoSupervisionRepository->findWithoutFail($id);

        if (empty($costProyectoSupervision)) {
            Flash::error('Cost Proyecto Supervision not found');

            return redirect(route('costProyectoSupervisions.index'));
        }

        return view('cost_proyecto_supervisions.show')->with('costProyectoSupervision', $costProyectoSupervision);
    }

    /**
     * Show the form for editing the specified cost_proyecto_supervision.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costProyectoSupervision = $this->costProyectoSupervisionRepository->findWithoutFail($id);

        if (empty($costProyectoSupervision)) {
            Flash::error('Cost Proyecto Supervision not found');

            return redirect(route('costProyectoSupervisions.index'));
        }

        return view('cost_proyecto_supervisions.edit')->with('costProyectoSupervision', $costProyectoSupervision);
    }

    /**
     * Update the specified cost_proyecto_supervision in storage.
     *
     * @param  int              $id
     * @param Updatecost_proyecto_supervisionRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_proyecto_supervisionRequest $request)
    {
        $costProyectoSupervision = $this->costProyectoSupervisionRepository->findWithoutFail($id);

        if (empty($costProyectoSupervision)) {
            Flash::error('Cost Proyecto Supervision not found');

            return redirect(route('costProyectoSupervisions.index'));
        }

        $costProyectoSupervision = $this->costProyectoSupervisionRepository->update($request->all(), $id);

        Flash::success('Cost Proyecto Supervision updated successfully.');

        return redirect(route('costProyectoSupervisions.index'));
    }

    /**
     * Remove the specified cost_proyecto_supervision from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costProyectoSupervision = $this->costProyectoSupervisionRepository->findWithoutFail($id);

        if (empty($costProyectoSupervision)) {
            Flash::error('Cost Proyecto Supervision not found');

            return redirect(route('costProyectoSupervisions.index'));
        }

        $this->costProyectoSupervisionRepository->delete($id);

        Flash::success('Cost Proyecto Supervision deleted successfully.');

        return redirect(route('costProyectoSupervisions.index'));
    }
}
