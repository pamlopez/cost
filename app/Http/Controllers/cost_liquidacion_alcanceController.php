<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_liquidacion_alcanceRequest;
use App\Http\Requests\Updatecost_liquidacion_alcanceRequest;
use App\Repositories\cost_liquidacion_alcanceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_liquidacion_alcanceController extends AppBaseController {

    /** @var  cost_liquidacion_alcanceRepository */
    private $costLiquidacionAlcanceRepository;

    public function __construct(cost_liquidacion_alcanceRepository $costLiquidacionAlcanceRepo)
    {
        $this->costLiquidacionAlcanceRepository = $costLiquidacionAlcanceRepo;
    }

    /**
     * Display a listing of the cost_liquidacion_alcance.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $id_proyecto = $request->id_proyecto;
        $this->costLiquidacionAlcanceRepository->pushCriteria(new RequestCriteria($request));
        $costLiquidacionAlcances = $this->costLiquidacionAlcanceRepository->findWhere([ 'id_proyecto' => $id_proyecto ])->all();

        return view('cost_liquidacion_alcances.index', compact('id_proyecto'))
            ->with('costLiquidacionAlcances', $costLiquidacionAlcances);
    }

    /**
     * Show the form for creating a new cost_liquidacion_alcance.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        return view('cost_liquidacion_alcances.create', compact('bnd_edit', 'id_proyecto'));
    }

    /**
     * Store a newly created cost_liquidacion_alcance in storage.
     *
     * @param Createcost_liquidacion_alcanceRequest $request
     *
     * @return Response
     */
    public function store(Createcost_liquidacion_alcanceRequest $request)
    {
        $input = $request->all();
        $input[ 'fecha_cambio' ] = $request->fecha_cambio_submit;
        $costLiquidacionAlcance = $this->costLiquidacionAlcanceRepository->create($input);
        Flash::success('El tiempo a liquidar se creo satisfactoriamente');
        return view('cost_liquidacion_alcances.show')->with('costLiquidacionAlcance', $costLiquidacionAlcance);
    }

    /**
     * Display the specified cost_liquidacion_alcance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costLiquidacionAlcance = $this->costLiquidacionAlcanceRepository->findWithoutFail($id);

        if (empty($costLiquidacionAlcance)) {
            Flash::error('Cost Liquidacion Alcance not found');

            return redirect(route('costLiquidacionAlcances.index'));
        }
        return view('cost_liquidacion_alcances.show')->with('costLiquidacionAlcance', $costLiquidacionAlcance);
    }

    /**
     * Show the form for editing the specified cost_liquidacion_alcance.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costLiquidacionAlcance = $this->costLiquidacionAlcanceRepository->findWithoutFail($id);
        $bnd_edit = true;
        if (empty($costLiquidacionAlcance)) {
            Flash::error('Cost Liquidacion Alcance not found');

            return redirect(route('costLiquidacionAlcances.index'));
        }

        return view('cost_liquidacion_alcances.edit', compact('bnd_edit'))->with('costLiquidacionAlcance', $costLiquidacionAlcance);
    }

    /**
     * Update the specified cost_liquidacion_alcance in storage.
     *
     * @param  int $id
     * @param Updatecost_liquidacion_alcanceRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_liquidacion_alcanceRequest $request)
    {

        $costLiquidacionAlcance = $this->costLiquidacionAlcanceRepository->findWithoutFail($id);
        if (empty($costLiquidacionAlcance)) {
            Flash::error('Cost Liquidacion Alcance not found');
            return redirect(route('costLiquidacionAlcances.index'));
        }

        $costLiquidacionAlcance->id_proyecto = $request->id_proyecto;
        $costLiquidacionAlcance->no_modificaciones_alcance = $request->no_modificaciones_alcance;
        $costLiquidacionAlcance->fecha_cambio = $request->fecha_cambio_submit;
        $costLiquidacionAlcance->razon_cambio = $request->razon_cambio;
        $costLiquidacionAlcance->alcance_real = $request->alcance_real;
        $costLiquidacionAlcance->programa = $request->programa;
        $costLiquidacionAlcance->no_fianza = $request->no_fianza;
        $costLiquidacionAlcance->save();

        Flash::success('La información del alcance se actualizó exitosamente.');
        return view('cost_liquidacion_alcances.show')->with('costLiquidacionAlcance', $costLiquidacionAlcance);
    }

    /**
     * Remove the specified cost_liquidacion_alcance from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costLiquidacionAlcance = $this->costLiquidacionAlcanceRepository->findWithoutFail($id);

        if (empty($costLiquidacionAlcance)) {
            Flash::error('Cost Liquidacion Alcance not found');

            return redirect(route('costLiquidacionAlcances.index'));
        }
        $id_proyecto = $costLiquidacionAlcance->id_proyecto;
        $this->costLiquidacionAlcanceRepository->delete($id);

        Flash::success(' La liquidacion del alcance se borró exitosamente.');

        return redirect(route('costLiquidacionAlcances.index', [ 'id_proyecto' => $id_proyecto ]));
    }
}
