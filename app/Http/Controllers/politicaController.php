<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatepoliticaRequest;
use App\Http\Requests\UpdatepoliticaRequest;
use App\Repositories\politicaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class politicaController extends AppBaseController
{
    /** @var  politicaRepository */
    private $politicaRepository;

    public function __construct(politicaRepository $politicaRepo)
    {
        $this->politicaRepository = $politicaRepo;
    }

    /**
     * Display a listing of the politica.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->politicaRepository->pushCriteria(new RequestCriteria($request));
        $politicas = $this->politicaRepository->all();

        return view('politicas.index')
            ->with('politicas', $politicas);
    }

    /**
     * Show the form for creating a new politica.
     *
     * @return Response
     */
    public function create()
    {
        return view('politicas.create');
    }

    /**
     * Store a newly created politica in storage.
     *
     * @param CreatepoliticaRequest $request
     *
     * @return Response
     */
    public function store(CreatepoliticaRequest $request)
    {
        $input = $request->all();

        $politica = $this->politicaRepository->create($input);

        Flash::success('Politica saved successfully.');

        return redirect(route('politicas.index'));
    }

    /**
     * Display the specified politica.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $politica = $this->politicaRepository->findWithoutFail($id);

        if (empty($politica)) {
            Flash::error('Politica not found');

            return redirect(route('politicas.index'));
        }

        return view('politicas.show')->with('politica', $politica);
    }

    /**
     * Show the form for editing the specified politica.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $politica = $this->politicaRepository->findWithoutFail($id);

        if (empty($politica)) {
            Flash::error('Politica not found');

            return redirect(route('politicas.index'));
        }

        return view('politicas.edit')->with('politica', $politica);
    }

    /**
     * Update the specified politica in storage.
     *
     * @param  int              $id
     * @param UpdatepoliticaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatepoliticaRequest $request)
    {
        $politica = $this->politicaRepository->findWithoutFail($id);

        if (empty($politica)) {
            Flash::error('Politica not found');

            return redirect(route('politicas.index'));
        }

        $politica = $this->politicaRepository->update($request->all(), $id);

        Flash::success('Politica updated successfully.');
        return view('politicas.show')->with('politica', $politica);
        return redirect(route('politicas.index'));
    }

    /**
     * Remove the specified politica from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $politica = $this->politicaRepository->findWithoutFail($id);

        if (empty($politica)) {
            Flash::error('Politica not found');

            return redirect(route('politicas.index'));
        }

        $this->politicaRepository->delete($id);

        Flash::success('Politica deleted successfully.');

        return redirect(route('politicas.index'));
    }
}
