<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_acuerdo_financiamientoRequest;
use App\Http\Requests\Updatecost_acuerdo_financiamientoRequest;
use App\Models\cost_acuerdo_financiamiento;
use App\Repositories\cost_acuerdo_financiamientoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Carbon\Carbon;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_acuerdo_financiamientoController extends AppBaseController {

    /** @var  cost_acuerdo_financiamientoRepository */
    private $costAcuerdoFinanciamientoRepository;

    public function __construct(cost_acuerdo_financiamientoRepository $costAcuerdoFinanciamientoRepo)
    {
        $this->costAcuerdoFinanciamientoRepository = $costAcuerdoFinanciamientoRepo;
    }

    /**
     * Display a listing of the cost_acuerdo_financiamiento.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->costAcuerdoFinanciamientoRepository->pushCriteria(new RequestCriteria($request));
        $costAcuerdoFinanciamientos = $this->costAcuerdoFinanciamientoRepository->all();

        return view('cost_acuerdo_financiamientos.index')
            ->with('costAcuerdoFinanciamientos', $costAcuerdoFinanciamientos);
    }

    /**
     * Show the form for creating a new cost_acuerdo_financiamiento.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        $tipo = $request->tipo;
        return view('cost_acuerdo_financiamientos.create',compact('bnd_edit','id_proyecto','id_fase','tipo'));
    }

    /**
     * Store a newly created cost_acuerdo_financiamiento in storage.
     *
     * @param Createcost_acuerdo_financiamientoRequest $request
     *
     * @return Response
     */
    public function store(Createcost_acuerdo_financiamientoRequest $request)
    {
        //dd($request);
        $cost_info_contrato = new cost_acuerdo_financiamiento();
        $cost_info_contrato->id_acuerdo_financiamiento = isset($request->id_acuerdo_financiamiento)?$request->id_acuerdo_financiamiento:$cost_info_contrato->id_acuerdo_financiamiento;
        $cost_info_contrato->id_proyecto = isset($request->id_proyecto)?$request->id_proyecto:$cost_info_contrato->id_proyecto;
        $cost_info_contrato->id_fase = isset($request->id_fase)?$request->id_fase:$cost_info_contrato->id_fase;
        $cost_info_contrato->tipo = isset($request->tipo)?$request->tipo:$cost_info_contrato->tipo;
        $cost_info_contrato->constitucional = isset($request->constitucional)?$request->constitucional:$cost_info_contrato->constitucional;
        $cost_info_contrato->iva_paz = isset($request->iva_paz)?$request->iva_paz:$cost_info_contrato->iva_paz;
        $cost_info_contrato->circulacion_vehiculos = isset($request->circulacion_vehiculos)?$request->circulacion_vehiculos:$cost_info_contrato->circulacion_vehiculos;
        $cost_info_contrato->petroleo = isset($request->petroleo)?$request->petroleo:$cost_info_contrato->petroleo;
        $cost_info_contrato->consejo_desarrollo = isset($request->consejo_desarrollo)?$request->consejo_desarrollo:$cost_info_contrato->consejo_desarrollo;
        $cost_info_contrato->fondos_propios = isset($request->fondos_propios)?$request->fondos_propios:$cost_info_contrato->fondos_propios;
        $cost_info_contrato->prestamo = isset($request->prestamo)?$request->prestamo:$cost_info_contrato->prestamo;
        $cost_info_contrato->aporte_beneficiario = isset($request->aporte_beneficiario)?$request->aporte_beneficiario:$cost_info_contrato->aporte_beneficiario;
        $cost_info_contrato->donacion = isset($request->donacion)?$request->donacion:$cost_info_contrato->donacion;
        $cost_info_contrato->transferencia = isset($request->transferencia)?$request->transferencia:$cost_info_contrato->transferencia;
        $cost_info_contrato->otros_descripcion = isset($request->otros_descripcion)?$request->otros_descripcion:$cost_info_contrato->otros_descripcion;
        $cost_info_contrato->otros = isset($request->otros)?$request->otros:$cost_info_contrato->otros;
        $cost_info_contrato->monto_total_asignado = isset($request->monto_total_asignado)?$request->monto_total_asignado:$cost_info_contrato->monto_total_asignado;
        $cost_info_contrato->fecha_aprobacion = isset($request->fecha_aprobacion_submit)?$request->fecha_aprobacion_submit:$cost_info_contrato->fecha_aprobacion;
        $cost_info_contrato->partida_presupuestaria = isset($request->partida_presupuestaria)?$request->partida_presupuestaria:$cost_info_contrato->partida_presupuestaria;
        $cost_info_contrato->observaciones = isset($request->observaciones)?$request->observaciones:$cost_info_contrato->observaciones;
        //dd($cost_info_contrato);
        $cost_info_contrato->save();
       // $costAcuerdoFinanciamiento = $this->costAcuerdoFinanciamientoRepository->create($input);

        Flash::success('Se crearon los acuerdos de financiamiento exitosamente');
        return redirect(route('costAcuerdoFinanciamientos.show', [$cost_info_contrato->id]));
    }

    /**
     * Display the specified cost_acuerdo_financiamiento.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costAcuerdoFinanciamiento = $this->costAcuerdoFinanciamientoRepository->findWithoutFail($id);

        if (empty($costAcuerdoFinanciamiento)) {
            Flash::error('Cost Acuerdo Financiamiento not found');

            return redirect(route('costAcuerdoFinanciamientos.index'));
        }

        return view('cost_acuerdo_financiamientos.show')->with('costAcuerdoFinanciamiento', $costAcuerdoFinanciamiento);
    }

    /**
     * Show the form for editing the specified cost_acuerdo_financiamiento.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
       $costAcuerdoFinanciamiento = $this->costAcuerdoFinanciamientoRepository->findWithoutFail($id);
       $bnd_edit = true;
      if (empty($costAcuerdoFinanciamiento)) {
            Flash::error('Cost Acuerdo Financiamiento not found');
            return redirect(route('costAcuerdoFinanciamientos.index'));
        }


        return view('cost_acuerdo_financiamientos.edit', compact('bnd_edit'))->with('costAcuerdoFinanciamiento', $costAcuerdoFinanciamiento);
    }

    /**
     * Update the specified cost_acuerdo_financiamiento in storage.
     *
     * @param  int $id
     * @param Updatecost_acuerdo_financiamientoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_acuerdo_financiamientoRequest $request)
    {
        $cost_info_contrato = $this->costAcuerdoFinanciamientoRepository->findWithoutFail($id);
        $cost_info_contrato->id_acuerdo_financiamiento = isset($request->id_acuerdo_financiamiento)?$request->id_acuerdo_financiamiento:$cost_info_contrato->id_acuerdo_financiamiento;
        $cost_info_contrato->id_proyecto = isset($request->id_proyecto)?$request->id_proyecto:$cost_info_contrato->id_proyecto;
        $cost_info_contrato->id_fase = isset($request->id_fase)?$request->id_fase:$cost_info_contrato->id_fase;
        $cost_info_contrato->tipo = isset($request->tipo)?$request->tipo:$cost_info_contrato->tipo;
        $cost_info_contrato->constitucional = isset($request->constitucional)?$request->constitucional:$cost_info_contrato->constitucional;
        $cost_info_contrato->iva_paz = isset($request->iva_paz)?$request->iva_paz:$cost_info_contrato->iva_paz;
        $cost_info_contrato->circulacion_vehiculos = isset($request->circulacion_vehiculos)?$request->circulacion_vehiculos:$cost_info_contrato->circulacion_vehiculos;
        $cost_info_contrato->petroleo = isset($request->petroleo)?$request->petroleo:$cost_info_contrato->petroleo;
        $cost_info_contrato->consejo_desarrollo = isset($request->consejo_desarrollo)?$request->consejo_desarrollo:$cost_info_contrato->consejo_desarrollo;
        $cost_info_contrato->fondos_propios = isset($request->fondos_propios)?$request->fondos_propios:$cost_info_contrato->fondos_propios;
        $cost_info_contrato->prestamo = isset($request->prestamo)?$request->prestamo:$cost_info_contrato->prestamo;
        $cost_info_contrato->aporte_beneficiario = isset($request->aporte_beneficiario)?$request->aporte_beneficiario:$cost_info_contrato->aporte_beneficiario;
        $cost_info_contrato->donacion = isset($request->donacion)?$request->donacion:$cost_info_contrato->donacion;
        $cost_info_contrato->transferencia = isset($request->transferencia)?$request->transferencia:$cost_info_contrato->transferencia;
        $cost_info_contrato->otros_descripcion = isset($request->otros_descripcion)?$request->otros_descripcion:$cost_info_contrato->otros_descripcion;
        $cost_info_contrato->otros = isset($request->otros)?$request->otros:$cost_info_contrato->otros;
        $cost_info_contrato->monto_total_asignado = isset($request->monto_total_asignado)?$request->monto_total_asignado:$cost_info_contrato->monto_total_asignado;
        $cost_info_contrato->fecha_aprobacion = isset($request->fecha_aprobacion_submit)?$request->fecha_aprobacion_submit:$cost_info_contrato->fecha_aprobacion;
        $cost_info_contrato->partida_presupuestaria = isset($request->partida_presupuestaria)?$request->partida_presupuestaria:$cost_info_contrato->partida_presupuestaria;
        $cost_info_contrato->observaciones = isset($request->observaciones)?$request->observaciones:$cost_info_contrato->observaciones;
        $cost_info_contrato->save();

        if (empty($cost_info_contrato)) {
            Flash::error('Cost Acuerdo Financiamiento not found');
            return redirect(route('costAcuerdoFinanciamientos.index'));
        }

        Flash::success('Cost Acuerdo Financiamiento updated successfully.');
        return redirect(route('costAcuerdoFinanciamientos.show', [$cost_info_contrato->id]));

    }

    /**
     * Remove the specified cost_acuerdo_financiamiento from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costAcuerdoFinanciamiento = $this->costAcuerdoFinanciamientoRepository->findWithoutFail($id);

        if (empty($costAcuerdoFinanciamiento)) {
            Flash::error('Cost Acuerdo Financiamiento not found');

            return redirect(route('costAcuerdoFinanciamientos.index'));
        }

        $this->costAcuerdoFinanciamientoRepository->delete($id);

        Flash::success(' Acuerdo Financiamiento eliminado exitosamente.');

        return redirect(route('costProyectos.show', [$id,'a'=>1]));
    }
}
