<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_operadorRequest;
use App\Http\Requests\Updatecost_operadorRequest;
use App\Repositories\cost_operadorRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_operadorController extends AppBaseController {

    /** @var  cost_operadorRepository */
    private $costOperadorRepository;

    public function __construct(cost_operadorRepository $costOperadorRepo)
    {
        $this->costOperadorRepository = $costOperadorRepo;
    }

    /**
     * Display a listing of the cost_operador.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $id_proyecto = $request->id_proyecto;
        $this->costOperadorRepository->pushCriteria(new RequestCriteria($request));
        $costOperadors = $this->costOperadorRepository->findWhere([ 'id_proyecto' => $request->id_proyecto ])->all();

        return view('cost_operadors.index', compact('id_proyecto'))
            ->with('costOperadors', $costOperadors);
    }

    /**
     * Show the form for creating a new cost_operador.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        return view('cost_operadors.create', compact('bnd_edit', 'id_proyecto'));
    }

    /**
     * Store a newly created cost_operador in storage.
     *
     * @param Createcost_operadorRequest $request
     *
     * @return Response
     */
    public function store(Createcost_operadorRequest $request)
    {

        $input = $request->all();
        $input[ 'fecha_operador' ] = $request->fecha_operador_submit;
        unset($input[ 'fecha_operador_submit' ]);
        $costOperador = $this->costOperadorRepository->create($input);
        Flash::success('El Operador se creó exitosamente');
        return redirect(route('costOperadors.show', [ $costOperador->id ]));
    }

    /**
     * Display the specified cost_operador.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costOperador = $this->costOperadorRepository->findWithoutFail($id);

        if (empty($costOperador)) {
            Flash::error('Cost Operador not found');

            return redirect(route('costOperadors.index'));
        }

        return view('cost_operadors.show')->with('costOperador', $costOperador);
    }

    /**
     * Show the form for editing the specified cost_operador.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costOperador = $this->costOperadorRepository->findWithoutFail($id);
        $bnd_edit = true;
        if (empty($costOperador)) {
            Flash::error('Cost Operador not found');

            return redirect(route('costOperadors.index'));
        }

        return view('cost_operadors.edit', compact('bnd_edit'))->with('costOperador', $costOperador);
    }

    /**
     * Update the specified cost_operador in storage.
     *
     * @param  int $id
     * @param Updatecost_operadorRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_operadorRequest $request)
    {
        $costOperador = $this->costOperadorRepository->findWithoutFail($id);
        $input = $request->all();
        $input[ 'fecha_operador' ] = $request->fecha_operador_submit;
        unset($input[ 'fecha_operador_submit' ]);
        if (empty($costOperador)) {
            Flash::error('Cost Operador not found');

            return redirect(route('costOperadors.index'));
        }
        $costOperador = $this->costOperadorRepository->update($input, $id);
        Flash::success('El operador se actualizó exitosamente');
        return redirect(route('costOperadors.show', [ $costOperador->id ]));
    }

    /**
     * Remove the specified cost_operador from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costOperador = $this->costOperadorRepository->findWithoutFail($id);

        if (empty($costOperador)) {
            Flash::error('Cost Operador not found');

            return redirect(route('costOperadors.index'));
        }
        $id_proyecto =$costOperador->id_proyecto;
        $this->costOperadorRepository->delete($id);

        Flash::success('El operador fue borrado exitosamente');
        return redirect(route('costOperadors.index', ['id_proyecto'=>$id_proyecto]));

    }
}
