<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateempleoRequest;
use App\Http\Requests\UpdateempleoRequest;
use App\Repositories\empleoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class empleoController extends AppBaseController
{
    /** @var  empleoRepository */
    private $empleoRepository;

    public function __construct(empleoRepository $empleoRepo)
    {
        $this->empleoRepository = $empleoRepo;
    }

    /**
     * Display a listing of the empleo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->empleoRepository->pushCriteria(new RequestCriteria($request));
        $empleos = $this->empleoRepository->all();

        return view('empleos.index')
            ->with('empleos', $empleos);
    }

    /**
     * Show the form for creating a new empleo.
     *
     * @return Response
     */
    public function create()
    {
        return view('empleos.create');
    }

    /**
     * Store a newly created empleo in storage.
     *
     * @param CreateempleoRequest $request
     *
     * @return Response
     */
    public function store(CreateempleoRequest $request)
    {
        $input = $request->all();

        $empleo = $this->empleoRepository->create($input);

        Flash::success('Empleo saved successfully.');

        return redirect(route('empleos.index'));
    }

    /**
     * Display the specified empleo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $empleo = $this->empleoRepository->findWithoutFail($id);

        if (empty($empleo)) {
            Flash::error('Empleo not found');

            return redirect(route('empleos.index'));
        }

        return view('empleos.show')->with('empleo', $empleo);
    }

    /**
     * Show the form for editing the specified empleo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $empleo = $this->empleoRepository->findWithoutFail($id);

        if (empty($empleo)) {
            Flash::error('Empleo not found');

            return redirect(route('empleos.index'));
        }

        return view('empleos.edit')->with('empleo', $empleo);
    }

    /**
     * Update the specified empleo in storage.
     *
     * @param  int              $id
     * @param UpdateempleoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateempleoRequest $request)
    {
        $empleo = $this->empleoRepository->findWithoutFail($id);

        if (empty($empleo)) {
            Flash::error('Empleo not found');

            return redirect(route('empleos.index'));
        }

        $empleo = $this->empleoRepository->update($request->all(), $id);

        Flash::success('Empleo updated successfully.');

        return redirect(route('empleos.index'));
    }

    /**
     * Remove the specified empleo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $empleo = $this->empleoRepository->findWithoutFail($id);

        if (empty($empleo)) {
            Flash::error('Empleo not found');

            return redirect(route('empleos.index'));
        }

        $this->empleoRepository->delete($id);

        Flash::success('Empleo deleted successfully.');

        return redirect(route('empleos.index'));
    }
}
