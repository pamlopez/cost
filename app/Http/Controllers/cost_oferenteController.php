<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_oferenteRequest;
use App\Http\Requests\Updatecost_oferenteRequest;
use App\Repositories\cost_oferenteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_oferenteController extends AppBaseController {

    /** @var  cost_oferenteRepository */
    private $costOferenteRepository;

    public function __construct(cost_oferenteRepository $costOferenteRepo)
    {
        $this->costOferenteRepository = $costOferenteRepo;
    }

    /**
     * Display a listing of the cost_oferente.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        $this->costOferenteRepository->pushCriteria(new RequestCriteria($request));
        $costOferentes = $this->costOferenteRepository->findWhere([ 'id_proyecto' => $request->id_proyecto, 'id_fase' => $request->id_fase ])->all();

        return view('cost_oferentes.index', compact('id_fase', 'id_proyecto'))
            ->with('costOferentes', $costOferentes);
    }

    /**
     * Show the form for creating a new cost_oferente.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        return view('cost_oferentes.create', compact('bnd_edit', 'id_proyecto', 'id_fase'));
    }

    /**
     * Store a newly created cost_oferente in storage.
     *
     * @param Createcost_oferenteRequest $request
     *
     * @return Response
     */
    public function store(Createcost_oferenteRequest $request)
    {
        $input = $request->all();
        $costOferente = $this->costOferenteRepository->create($input);
        Flash::success('El oferente fue creado exitosamente.');
        return redirect(route('costOferentes.show', [ $costOferente->id ]));
    }

    /**
     * Display the specified cost_oferente.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costOferente = $this->costOferenteRepository->findWithoutFail($id);

        if (empty($costOferente)) {
            Flash::error('Cost Oferente not found');

            return redirect(route('costOferentes.index'));
        }

        return view('cost_oferentes.show')->with('costOferente', $costOferente);
    }

    /**
     * Show the form for editing the specified cost_oferente.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costOferente = $this->costOferenteRepository->findWithoutFail($id);
        $bnd_edit = true;
        if (empty($costOferente)) {
            Flash::error('Cost Oferente not found');

            return redirect(route('costOferentes.index'));
        }
        return view('cost_oferentes.edit', compact('bnd_edit'))->with('costOferente', $costOferente);
    }

    /**
     * Update the specified cost_oferente in storage.
     *
     * @param  int $id
     * @param Updatecost_oferenteRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_oferenteRequest $request)
    {
        $costOferente = $this->costOferenteRepository->findWithoutFail($id);
        if (empty($costOferente)) {
            Flash::error('Cost Oferente not found');

            return redirect(route('costOferentes.index'));
        }
        $costOferente = $this->costOferenteRepository->update($request->all(), $id);
        Flash::success('El oferente  ha sido actualizado exitosamente');
        return redirect(route('costOferentes.show', [ $costOferente->id ]));
    }

    /**
     * Remove the specified cost_oferente from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costOferente = $this->costOferenteRepository->findWithoutFail($id);
        if (empty($costOferente)) {
            Flash::error('Cost Oferente not found');

            return redirect(route('costOferentes.index'));
        }
        $id_proyecto =$costOferente->id_proyecto;
        $id_fase = $costOferente->id_fase;
        $this->costOferenteRepository->delete($id);
        Flash::success('El Oferente se borro exitosamente.');
        return redirect(route('costOferentes.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]));

    }
}
