<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcat_itemRequest;
use App\Http\Requests\Updatecat_itemRequest;
use App\Models\cat_item;
use App\Repositories\cat_itemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cat_itemController extends AppBaseController
{
    /** @var  cat_itemRepository */
    private $catItemRepository;

    public function __construct(cat_itemRepository $catItemRepo)
    {
        $this->catItemRepository = $catItemRepo;
    }

    /**
     * Display a listing of the cat_item.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //$this->catItemRepository->pushCriteria(new RequestCriteria($request));
        //$catItems = $this->catItemRepository->all();

        $id_cat = isset($request->id_cat) ? $request->id_cat : 1;
        $listado=cat_item::where('id_cat',$id_cat)->orderBy('descripcion','asc')
                ->paginate(20);
        $nombre_catalogo=cat_item::nombre_catalogo($id_cat);

        return view('cat_items.index')
            ->with('id_cat',$id_cat)
            ->with('nombre_catalogo',$nombre_catalogo)
            ->with('listado',$listado);

        /*return view('cat_items.index')
            ->with('catItems', $catItems);*/
    }

    /**
     * Show the form for creating a new cat_item.
     *
     * @return Response
     */
    public function create()
    {
        return view('cat_items.create');
    }

    /**
     * Store a newly created cat_item in storage.
     *
     * @param Createcat_itemRequest $request
     *
     * @return Response
     */
    public function store(Createcat_itemRequest $request)
    {
        //dd($request);
        $input = $request->all();

        $catItem = $this->catItemRepository->create($input);

        Flash::success('Item creado con éxito.');

        return redirect(route('catItems.index'));
    }

    /**
     * Display the specified cat_item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $catItem = $this->catItemRepository->findWithoutFail($id);

        if (empty($catItem)) {
            Flash::error('Cat Item not found');

            return redirect(route('catItems.index'));
        }

        return view('cat_items.show')->with('catItem', $catItem);
    }

    /**
     * Show the form for editing the specified cat_item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $catItem = $this->catItemRepository->findWithoutFail($id);

        if (empty($catItem)) {
            Flash::error('Cat Item not found');

            return redirect(route('catItems.index'));
        }

        return view('cat_items.edit')->with('catItem', $catItem);
    }

    /**
     * Update the specified cat_item in storage.
     *
     * @param  int              $id
     * @param Updatecat_itemRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecat_itemRequest $request)
    {
        $catItem = $this->catItemRepository->findWithoutFail($id);

        if (empty($catItem)) {
            Flash::error('Cat Item not found');

            return redirect(route('catItems.index'));
        }

        $catItem = $this->catItemRepository->update($request->all(), $id);

        Flash::success('Item actualizado con éxito');

        return redirect(route('catItems.index'));
    }

    /**
     * Remove the specified cat_item from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $catItem = $this->catItemRepository->findWithoutFail($id);

        if (empty($catItem)) {
            Flash::error('Cat Item not found');

            return redirect(route('catItems.index'));
        }

        $this->catItemRepository->delete($id);

        Flash::success('Item eliminado con éxito');

        return redirect(route('catItems.index'));
    }

    public function json(Request $request) {
        return cat_item::find($request->id_item);
    }

    public function itemdeshabilitado($id,Request $request){
        $model = cat_item::find($id);
        $model->id_cat = $model->id_cat;
        $model->descripcion = $model->descripcion;
        $model->orden = $model->orden;
        $model->otro = $request->otro;
        $model->predeterminado = $model->predeterminado;
        $model->save();
        return redirect(route('catItems.index'));
    }
}
