<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateparticipanteRequest;
use App\Http\Requests\UpdateparticipanteRequest;
use App\Repositories\participanteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class participanteController extends AppBaseController
{
    /** @var  participanteRepository */
    private $participanteRepository;

    public function __construct(participanteRepository $participanteRepo)
    {
        $this->participanteRepository = $participanteRepo;
    }

    /**
     * Display a listing of the participante.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->participanteRepository->pushCriteria(new RequestCriteria($request));
        $participantes = $this->participanteRepository->all();

        return view('participantes.index')
            ->with('participantes', $participantes);

    }

    /**
     * Show the form for creating a new participante.
     *
     * @return Response
     */
    public function create()
    {
        return view('participantes.create');
    }

    /**
     * Store a newly created participante in storage.
     *
     * @param CreateparticipanteRequest $request
     *
     * @return Response
     */
    public function store(CreateparticipanteRequest $request)
    {
        $input = $request->all();

        $participante = $this->participanteRepository->create($input);

        Flash::success('Participante saved successfully.');

        return redirect(route('participantes.index'));
    }

    /**
     * Display the specified participante.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $participante = $this->participanteRepository->findWithoutFail($id);

        if (empty($participante)) {
            Flash::error('Participante not found');

            return redirect(route('participantes.index'));
        }

        return view('participantes.show')->with('participante', $participante);
    }

    /**
     * Show the form for editing the specified participante.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $participante = $this->participanteRepository->findWithoutFail($id);

        if (empty($participante)) {
            Flash::error('Participante not found');

            return redirect(route('participantes.index'));
        }

        return view('participantes.edit')->with('participante', $participante);
    }

    /**
     * Update the specified participante in storage.
     *
     * @param  int              $id
     * @param UpdateparticipanteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateparticipanteRequest $request)
    {
        $participante = $this->participanteRepository->findWithoutFail($id);

        if (empty($participante)) {
            Flash::error('Participante not found');

            return redirect(route('participantes.index'));
        }

        $participante = $this->participanteRepository->update($request->all(), $id);

        Flash::success('Participante updated successfully.');

        return redirect(route('participantes.index'));
    }

    /**
     * Remove the specified participante from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $participante = $this->participanteRepository->findWithoutFail($id);

        if (empty($participante)) {
            Flash::error('Participante not found');

            return redirect(route('participantes.index'));
        }

        $this->participanteRepository->delete($id);

        Flash::success('Participante deleted successfully.');

        return redirect(route('participantes.index'));
    }
}
