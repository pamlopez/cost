<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_actaRequest;
use App\Http\Requests\Updatecost_actaRequest;
use App\Repositories\cost_actaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_actaController extends AppBaseController {

    /** @var  cost_actaRepository */
    private $costActaRepository;

    public function __construct(cost_actaRepository $costActaRepo)
    {
        $this->costActaRepository = $costActaRepo;
        
    }

    /**
     * Display a listing of the cost_acta.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        $this->costActaRepository->pushCriteria(new RequestCriteria($request));
        $costActas = $this->costActaRepository->findWhere([ 'id_proyecto' => $id_proyecto, 'id_fase' => $id_fase ])->all();
        return view('cost_actas.index', compact('id_fase', 'id_proyecto'))
            ->with('costActas', $costActas);
    }

    /**
     * Show the form for creating a new cost_acta.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $id_fase = $request->id_fase;
        $id_proyecto = $request->id_proyecto;
        $bnd_edit = false;
        return view('cost_actas.create', compact('id_fase', 'id_proyecto', 'bnd_edit'));
    }

    /**
     * Store a newly created cost_acta in storage.
     *
     * @param Createcost_actaRequest $request
     *
     * @return Response
     */
    public function store(Createcost_actaRequest $request)
    {
        $input = $request->all();
        $input[ 'fecha_acta' ] = $request->fecha_acta_submit;
        unset($input[ 'fecha_acta_submit' ]);
        $costActa = $this->costActaRepository->create($input);

        Flash::success('El acta se creó exitosamente');
        return redirect(route('costActas.show', [ $costActa->id ]));
    }

    /**
     * Display the specified cost_acta.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costActa = $this->costActaRepository->findWithoutFail($id);

        if (empty($costActa)) {
            Flash::error('Cost Acta not found');

            return redirect(route('costActas.index'));
        }

        return view('cost_actas.show')->with('costActa', $costActa);
    }

    /**
     * Show the form for editing the specified cost_acta.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bnd_edit = true;
        $costActa = $this->costActaRepository->findWithoutFail($id);

        if (empty($costActa)) {
            Flash::error('Cost Acta not found');
            return redirect(route('costActas.index'));
        }
        return view('cost_actas.edit', compact('bnd_edit'))->with('costActa', $costActa);
    }

    /**
     * Update the specified cost_acta in storage.
     *
     * @param  int $id
     * @param Updatecost_actaRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_actaRequest $request)
    {
        $costActa = $this->costActaRepository->findWithoutFail($id);
        $input = $request->all();
        $input[ 'fecha_acta' ] = $request->fecha_acta_submit;
        unset($input[ 'fecha_acta_submit' ]);


        if (empty($costActa)) {
            Flash::error('Cost Acta not found');

            return redirect(route('costActas.index'));
        }

        $costActa = $this->costActaRepository->update($input, $id);

        Flash::success('El acta ha sido actualizada exitosamente');
        return redirect(route('costActas.show', [ $costActa->id ]));

    }

    /**
     * Remove the specified cost_acta from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costActa = $this->costActaRepository->findWithoutFail($id);

        if (empty($costActa)) {
            Flash::error('Cost Acta not found');

            return redirect(route('costActas.index'));
        }
        $id_proyecto =$costActa->id_proyecto;
        $id_fase = $costActa->id_fase;
        $this->costActaRepository->delete($id);

        Flash::success('El acta fue borrada exitosamente');

        return redirect(route('costActas.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]));
    }
}
