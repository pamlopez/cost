<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_ampliacionRequest;
use App\Http\Requests\Updatecost_ampliacionRequest;
use App\Repositories\cost_ampliacionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_ampliacionController extends AppBaseController {

    /** @var  cost_ampliacionRepository */
    private $costAmpliacionRepository;

    public function __construct(cost_ampliacionRepository $costAmpliacionRepo)
    {
        $this->costAmpliacionRepository = $costAmpliacionRepo;
    }

    /**
     * Display a listing of the cost_ampliacion.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        $this->costAmpliacionRepository->pushCriteria(new RequestCriteria($request));
        $costAmpliacions = $this->costAmpliacionRepository->findWhere([ 'id_proyecto' => $request->id_proyecto, 'id_fase' => $request->id_fase ])->all();

        return view('cost_ampliacions.index', compact('id_fase', 'id_proyecto'))
            ->with('costAmpliacions', $costAmpliacions);
    }

    /**
     * Show the form for creating a new cost_ampliacion.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        return view('cost_ampliacions.create', compact('bnd_edit', 'id_proyecto', 'id_fase'));
    }

    /**
     * Store a newly created cost_ampliacion in storage.
     *
     * @param Createcost_ampliacionRequest $request
     *
     * @return Response
     */
    public function store(Createcost_ampliacionRequest $request)
    {
        $input = $request->all();
        $input[ 'fecha_ampliacion' ] = $request->fecha_ampliacion_submit;
        unset($input[ 'fecha_ampliacion_submit' ]);
        $costAmpliacion = $this->costAmpliacionRepository->create($input);
        Flash::success('La ampliación se creó exitosamente');
        return redirect(route('costAmpliacions.show', [ $costAmpliacion->id ]));
    }

    /**
     * Display the specified cost_ampliacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costAmpliacion = $this->costAmpliacionRepository->findWithoutFail($id);

        if (empty($costAmpliacion)) {
            Flash::error('Cost Ampliacion not found');

            return redirect(route('costAmpliacions.index'));
        }

        return view('cost_ampliacions.show')->with('costAmpliacion', $costAmpliacion);
    }

    /**
     * Show the form for editing the specified cost_ampliacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costAmpliacion = $this->costAmpliacionRepository->findWithoutFail($id);
        $bnd_edit = true;

        if (empty($costAmpliacion)) {
            Flash::error('Cost Ampliacion not found');

            return redirect(route('costAmpliacions.index'));
        }

        return view('cost_ampliacions.edit', compact('bnd_edit'))->with('costAmpliacion', $costAmpliacion);
    }

    /**
     * Update the specified cost_ampliacion in storage.
     *
     * @param  int $id
     * @param Updatecost_ampliacionRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_ampliacionRequest $request)
    {
        $costAmpliacion = $this->costAmpliacionRepository->findWithoutFail($id);
        $input = $request->all();
        $input[ 'fecha_ampliacion' ] = $request->fecha_ampliacion_submit;
        unset($input[ 'fecha_ampliacion_submit' ]);
        if (empty($costAmpliacion)) {
            Flash::error('Cost Ampliacion not found');

            return redirect(route('costAmpliacions.index'));
        }

        $costAmpliacion = $this->costAmpliacionRepository->update($input, $id);

        Flash::success('La ampliación ha sido actualizada exitosamente');
        return redirect(route('costAmpliacions.show', [ $costAmpliacion->id ]));
    }

    /**
     * Remove the specified cost_ampliacion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costAmpliacion = $this->costAmpliacionRepository->findWithoutFail($id);

        if (empty($costAmpliacion)) {
            Flash::error('Cost Ampliacion not found');

            return redirect(route('costAmpliacions.index'));
        }
        $id_proyecto =$costAmpliacion->id_proyecto;
        $id_fase = $costAmpliacion->id_fase;
        $this->costAmpliacionRepository->delete($id);
        Flash::success('La ampliación fue borrada exitosamente');
        return redirect(route('costAmpliacions.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]));

    }
}
