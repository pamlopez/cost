<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_pago_efectuadoRequest;
use App\Http\Requests\Updatecost_pago_efectuadoRequest;
use App\Repositories\cost_pago_efectuadoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;

use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Flash;

class cost_pago_efectuadoController extends AppBaseController {

    /** @var  cost_pago_efectuadoRepository */
    private $costPagoEfectuadoRepository;

    public function __construct(cost_pago_efectuadoRepository $costPagoEfectuadoRepo)
    {
        $this->costPagoEfectuadoRepository = $costPagoEfectuadoRepo;
    }

    /**
     * Display a listing of the cost_pago_efectuado.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        $this->costPagoEfectuadoRepository->pushCriteria(new RequestCriteria($request));
        $costPagoEfectuados = $this->costPagoEfectuadoRepository->findWhere([ 'id_proyecto' => $request->id_proyecto, 'id_fase' => $request->id_fase ])->all();

        return view('cost_pago_efectuados.index', compact('id_fase', 'id_proyecto'))
            ->with('costPagoEfectuados', $costPagoEfectuados);
    }

    /**
     * Show the form for creating a new cost_pago_efectuado.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        return view('cost_pago_efectuados.create', compact('bnd_edit', 'id_proyecto', 'id_fase'));
    }

    /**
     * Store a newly created cost_pago_efectuado in storage.
     *
     * @param Createcost_pago_efectuadoRequest $request
     *
     * @return Response
     */
    public function store(Createcost_pago_efectuadoRequest $request)
    {
        $input = $request->all();
        $input[ 'fecha_pago' ] = $request->fecha_pago_submit;
        $input[ 'fecha_aprobacion_ampliacion' ] = $request->fecha_aprobacion_ampliacion_submit;
        unset($input[ 'fecha_pago_submit' ], $input[ 'fecha_aprobacion_ampliacion_submit' ]);
        $costPagoEfectuado = $this->costPagoEfectuadoRepository->create($input);

        Flash::success('El Pago efectuado se creó exitosamente');
        return redirect(route('costPagoEfectuados.show', [ $costPagoEfectuado->id ]));
    }

    /**
     * Display the specified cost_pago_efectuado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costPagoEfectuado = $this->costPagoEfectuadoRepository->findWithoutFail($id);

        if (empty($costPagoEfectuado)) {
            Flash::error('Cost Pago Efectuado not found');

            return redirect(route('costPagoEfectuados.index'));
        }

        return view('cost_pago_efectuados.show')->with('costPagoEfectuado', $costPagoEfectuado);
    }

    /**
     * Show the form for editing the specified cost_pago_efectuado.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costPagoEfectuado = $this->costPagoEfectuadoRepository->findWithoutFail($id);
        $bnd_edit = true;
        if (empty($costPagoEfectuado)) {
            Flash::error('Cost Pago Efectuado not found');

            return redirect(route('costPagoEfectuados.index'));
        }

        return view('cost_pago_efectuados.edit', compact('bnd_edit'))->with('costPagoEfectuado', $costPagoEfectuado);
    }

    /**
     * Update the specified cost_pago_efectuado in storage.
     *
     * @param  int $id
     * @param Updatecost_pago_efectuadoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_pago_efectuadoRequest $request)
    {
        $costPagoEfectuado = $this->costPagoEfectuadoRepository->findWithoutFail($id);
        $input = $request->all();
        $input[ 'fecha_pago' ] = $request->fecha_pago_submit;
        $input[ 'fecha_aprobacion_ampliacion' ] = $request->fecha_aprobacion_ampliacion_submit;
        unset($input[ 'fecha_pago_submit' ], $input[ 'fecha_aprobacion_ampliacion_submit' ]);
        if (empty($costPagoEfectuado)) {
            Flash::error('Cost Pago Efectuado not found');

            return redirect(route('costPagoEfectuados.index'));
        }
        $costPagoEfectuado = $this->costPagoEfectuadoRepository->update($input, $id);
        Flash::success('El Pago Efectuado ha sido almacenado exitosamente');
        return redirect(route('costPagoEfectuados.show', [ $costPagoEfectuado->id ]));
    }

    /**
     * Remove the specified cost_pago_efectuado from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costPagoEfectuado = $this->costPagoEfectuadoRepository->findWithoutFail($id);

        if (empty($costPagoEfectuado)) {
            Flash::error('Cost Pago Efectuado not found');

            return redirect(route('costPagoEfectuados.index'));
        }
        $id_proyecto =$costPagoEfectuado->id_proyecto;
        $id_fase = $costPagoEfectuado->id_fase;
        $this->costPagoEfectuadoRepository->delete($id);
        Flash::success('El Pago efectuado fue borrado exitosamente');
        return redirect(route('costPagoEfectuados.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]));

    }
}
