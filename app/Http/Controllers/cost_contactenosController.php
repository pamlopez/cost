<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_contactenosRequest;
use App\Http\Requests\Updatecost_contactenosRequest;
use App\Repositories\cost_contactenosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_contactenosController extends AppBaseController
{
    /** @var  cost_contactenosRepository */
    private $costContactenosRepository;

    public function __construct(cost_contactenosRepository $costContactenosRepo)
    {
        $this->costContactenosRepository = $costContactenosRepo;
    }

    /**
     * Display a listing of the cost_contactenos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->costContactenosRepository->pushCriteria(new RequestCriteria($request));
        $costContactenos = $this->costContactenosRepository->all();

        return view('cost_contactenos.index')
            ->with('costContactenos', $costContactenos);
    }

    /**
     * Show the form for creating a new cost_contactenos.
     *
     * @return Response
     */
    public function create()
    {
        return view('cost_contactenos.create');
    }

    /**
     * Store a newly created cost_contactenos in storage.
     *
     * @param Createcost_contactenosRequest $request
     *
     * @return Response
     */
    public function store(Createcost_contactenosRequest $request)
    {
        $input = $request->all();

        $costContactenos = $this->costContactenosRepository->create($input);

        Flash::success('Se ha enviado con éxito su comentario, estaremos respondiendo lo más pronto posible, gracias.');

        return redirect(route('costProyectos.index'));
    }

    /**
     * Display the specified cost_contactenos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costContactenos = $this->costContactenosRepository->findWithoutFail($id);

        if (empty($costContactenos)) {
            Flash::error('Cost Contactenos not found');

            return redirect(route('costContactenos.index'));
        }

        return view('cost_contactenos.show')->with('costContactenos', $costContactenos);
    }

    /**
     * Show the form for editing the specified cost_contactenos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costContactenos = $this->costContactenosRepository->findWithoutFail($id);

        if (empty($costContactenos)) {
            Flash::error('Cost Contactenos not found');

            return redirect(route('costContactenos.index'));
        }

        return view('cost_contactenos.edit')->with('costContactenos', $costContactenos);
    }

    /**
     * Update the specified cost_contactenos in storage.
     *
     * @param  int              $id
     * @param Updatecost_contactenosRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_contactenosRequest $request)
    {
        $costContactenos = $this->costContactenosRepository->findWithoutFail($id);

        if (empty($costContactenos)) {
            Flash::error('Cost Contactenos not found');

            return redirect(route('costContactenos.index'));
        }

        $costContactenos = $this->costContactenosRepository->update($request->all(), $id);

        Flash::success('Cost Contactenos updated successfully.');

        return redirect(route('costContactenos.index'));
    }

    /**
     * Remove the specified cost_contactenos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costContactenos = $this->costContactenosRepository->findWithoutFail($id);

        if (empty($costContactenos)) {
            Flash::error('Cost Contactenos not found');

            return redirect(route('costContactenos.index'));
        }

        $this->costContactenosRepository->delete($id);

        Flash::success('Cost Contactenos deleted successfully.');

        return redirect(route('costContactenos.index'));
    }
}
