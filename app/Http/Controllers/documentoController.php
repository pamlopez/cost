<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatedocumentoRequest;
use App\Http\Requests\UpdatedocumentoRequest;
use App\Repositories\documentoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class documentoController extends AppBaseController
{
    /** @var  documentoRepository */
    private $documentoRepository;

    public function __construct(documentoRepository $documentoRepo)
    {
        $this->documentoRepository = $documentoRepo;
    }

    /**
     * Display a listing of the documento.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        $this->documentoRepository->pushCriteria(new RequestCriteria($request));
        $documentos = $this->documentoRepository->findWhere([ 'id_proyecto' => $id_proyecto, 'id_fase' => $id_fase ])->all();
        return view('documentos.index', compact('id_fase', 'id_proyecto'))
            ->with('documentos', $documentos);
    }

    /**
     * Show the form for creating a new documento.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $id_fase = $request->id_fase;
        $id_proyecto = $request->id_proyecto;
        $bnd_edit = false;
        return view('documentos.create', compact('id_fase', 'id_proyecto', 'bnd_edit'));
    }

    /**
     * Store a newly created documento in storage.
     *
     * @param CreatedocumentoRequest $request
     *
     * @return Response
     */
    public function store(CreatedocumentoRequest $request)
    {
        $input = $request->all();
        if(strlen($request->fh_publicacion_submit)==0){
            $request->fh_publicacion_submit = NULL;
        }
        if(strlen($request->fh_modificacion_submit)==0){
            $request->fh_modificacion_submit = NULL;
        }
        if(empty($request->fh_publicacion_submit)|| $request->fh_publicacion_submit == null){
            $input[ 'fh_publicacion' ] = null;
            unset($input[ 'fh_publicacion_submit' ]);


        }else{
            $input[ 'fh_publicacion' ] = $request->fh_publicacion_submit;
            unset($input[ 'fh_publicacion_submit' ]);

        }

        if(empty($request->fh_modificacion_submit)|| $request->fh_modificacion_submit == null){
            $input[ 'fh_modificacion' ] = null;
            unset($input[ 'fh_modificacion_submit' ]);


        }else{
            $input[ 'fh_modificacion' ] = $request->fh_modificacion_submit;
            unset($input[ 'fh_modificacion_submit' ]);
        }


        $documento = $this->documentoRepository->create($input);
        Flash::success('El documento se creó exitosamente');
        return redirect(route('documentos.show', [ $documento->id ]));
    }

    /**
     * Display the specified documento.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $documento = $this->documentoRepository->findWithoutFail($id);

        if (empty($documento)) {
            Flash::error('Documento not found');

            return redirect(route('documentos.index'));
        }

        return view('documentos.show')->with('documento', $documento);
    }

    /**
     * Show the form for editing the specified documento.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bnd_edit = true;
        $documento = $this->documentoRepository->findWithoutFail($id);

        if (empty($documento)) {
            Flash::error('Documento not found');

            return redirect(route('documentos.index'));
        }

        return view('documentos.edit', compact('bnd_edit'))->with('documento', $documento);
    }

    /**
     * Update the specified documento in storage.
     *
     * @param  int              $id
     * @param UpdatedocumentoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatedocumentoRequest $request)
    {
        //dd($request);
        $documento = $this->documentoRepository->findWithoutFail($id);
        $input = $request->all();
        if(empty($request->fh_publicacion_submit)|| $request->fh_publicacion_submit == null){
            $input[ 'fh_publicacion' ] = null;
            unset($input[ 'fh_publicacion_submit' ]);


        }else{
            $input[ 'fh_publicacion' ] = $request->fh_publicacion_submit;
            unset($input[ 'fh_publicacion_submit' ]);

        }

        if(empty($request->fh_modificacion_submit)|| $request->fh_modificacion_submit == null){
            $input[ 'fh_modificacion' ] = null;
            unset($input[ 'fh_modificacion_submit' ]);


        }else{
            $input[ 'fh_modificacion' ] = $request->fh_modificacion_submit;
            unset($input[ 'fh_modificacion_submit' ]);
        }



        if (empty($documento)) {
            Flash::error('Documento not found');

            return redirect(route('documentos.index'));
        }

        $documento = $this->documentoRepository->update($input, $id);

        Flash::success('El documento ha sido actualizado exitosamente');
        return redirect(route('documentos.show', [ $documento->id ]));

    }

    /**
     * Remove the specified documento from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $documento = $this->documentoRepository->findWithoutFail($id);

        if (empty($documento)) {
            Flash::error('Documento not found');

            return redirect(route('documentos.index'));
        }
        $id_proyecto =$documento->id_proyecto;
        $id_fase = $documento->id_fase;
        $this->documentoRepository->delete($id);

        Flash::success('Documento borrado exitosamente');

        return redirect(route('documentos.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]));
    }
}
