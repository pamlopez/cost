<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdministracionRequest;
use App\Http\Requests\CambioPasswordRequest;
use App\Models\administracion;
use App\Models\caimus;
use App\Models\sede;
use App\Models\users_asignacion;
use App\Models\users_rol;
use App\User;
use Doctrine\DBAL\Driver\IBMDB2\DB2Driver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;

class AdministracionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $usuarios = User::get_all_users();
        $listado_caimus = sede::listado_caimus('Selecciona'); /**multi sedes*/

        return view('auth.administracion.index',compact('usuarios','listado_caimus'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request);
        #usuario caimu
        $user = User::find($request->id_users);
        $user->id_sede = $request->id_sede;
        $user->save();

        #asignacion
        $asignacion = new users_asignacion();
        foreach (Input::get('roles') as $key=>$value){
            users_asignacion::create([
                'id_users'=>$request->id_users,
                'id_rol' => $value
            ]);
        }

        Flash::success('El usuario ha sido creado éxitosamente.');

        return redirect()->route('administracion.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        //$listado_roles = User::listado_roles('Seleccionar');
        $listado_roles = users_rol::all();
        $listado_caimus = sede::listado_caimus('Selecciona');
        $seleccionados_roles =0;

        return view('auth.administracion.users_asignacion',compact('user','listado_roles','listado_caimus','seleccionados_roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        //$listado_roles = User::listado_roles('Seleccionar');
        $listado_roles = users_rol::all();
        $listado_caimus = sede::listado_caimus('Selecciona');
        $seleccionados_roles = administracion::seleccionados_roles($id);
      //dd($seleccionados_roles884575);

        return view('auth.administracion.users_asignacion',compact('user','listado_roles','listado_caimus','seleccionados_roles'));
    }

    public function edit_user(Request $request){

        #update de usuario
        $user = User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        #roles
        if (isset($request->roles)){

            #quito roles existentes
            $antiguo = users_asignacion::where('id_users',$request->id);
            $antiguo->delete();

            #creo los nuevos
            foreach (Input::get('roles') as $key=>$value){
                users_asignacion::create([
                    'id_users'=>$request->id_users,
                    'id_rol' => $value
                ]);
            }
        }

        Flash::success('El usuario ha sido actualizado éxitosamente.');

        return redirect()->route('administracion.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        #usuario caimu
        $user = User::find($request->id_users);
        $user->id_caimu = $request->id_caimu;
        $user->save();

        #asignacion
        $asignacion = new users_asignacion();
        foreach (Input::get('roles') as $key=>$value){
            users_asignacion::create([
                'id_users'=>$request->id_users,
                'id_rol' => $value
            ]);
        }
        return redirect()->route('administracion.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     *
     */
    public function destroy($id)
    {
        $admin =User::find($id);
        $admin->delete();
        return redirect('administracion');
    }

    public function reiniciar_password($id){

        if (isset($id) && $id >0){
            #generando encriptacion del nuevo pasword
                $password = env('REINICIAR_PASSWORD');
                $nuevo= bcrypt($password);
            #buscar el usuario para hacer update al usuario con el nuevo pasword
                $update = User::find($id);
                $update->password = $nuevo;
                $update->save();
        }

        Flash::success('La contraseña ha sido reiniciada éxitosamente.');
        return redirect()->route('administracion.index');
    }

    public function password_deshabilitar($id){

        if (isset($id) && $id >0){
            #generando encriptacion del nuevo pasword
            $password = env('DESHABILITAR_PASSWORD');
            $nuevo= bcrypt($password);
            #buscar el usuario para hacer update al usuario con el nuevo pasword
            $update = User::find($id);
            $update->password = $nuevo;
            $update->estado = 1;
            $update->save();
        }

        Flash::success('La contraseña ha sido reiniciada éxitosamente.');
        return redirect()->route('administracion.index');
    }

    public function password_habilitar($id){

        if (isset($id) && $id >0){
            #generando encriptacion del nuevo pasword
            $password = env('REINICIAR_PASSWORD');
            $nuevo= bcrypt($password);
            #buscar el usuario para hacer update al usuario con el nuevo pasword
            $update = User::find($id);
            $update->password = $nuevo;
            $update->estado = 0;
            $update->save();
        }

        Flash::success('La contraseña ha sido reiniciada éxitosamente.');
        return redirect()->route('administracion.index');
    }



    public function reiniciar_password_perfil($id){

        if (isset($id) && $id >0){
            #datos del usuario registrado
                $user = User::find($id);
                $roles_asignados = users_asignacion::where('id_users',$id)->get();

            return view('auth.administracion.perfil',compact('user','roles_asignados'));

        }else{

            Flash::success('La contraseña ha sido reiniciada éxitosamente.');
            return redirect()->route('administracion.index');
        }
    }

    public function password_change(Request $request){

        $id = $request->id;

        if (strlen($request->password)>=6 && strlen($request->password_dos)>=6 ){
            if ($request->password === $request->password_dos){// si son iguales la cambia
                if (isset($id) && $id >0){
                    #generando encriptacion del nuevo pasword
                    $password = $request->password;
                    $nuevo= bcrypt($password);
                    #buscar el usuario para hacer update al usuario con el nuevo pasword
                    $update = User::find($id);
                    $update->password = $nuevo;
                    $update->estado = 0;
                    $update->save();
                }
                $error = false;
                Flash::success('La contraseña ha sido actualizada éxitosamente.');
                return redirect(url('reiniciar_password_perfil', [$id]));

            }else{//se regresa a pedir que ingresen de nuevo
                $error = true;
                $error_titulo = 'No coincide la contraseña!';
                $error_descripcion = 'No hay coicencia, ingrese la nueva contraseña nuevamente. ';
                return view('auth.administracion.change_password',compact('id','error_descripcion','error_titulo','error'));
            }
        }else{
            $error = true;
            $error_titulo = 'Caracteres mínimos';
            $error_descripcion = 'La contraseña debe tener como mínimo 6 caracteres.';
            return view('auth.administracion.change_password',compact('id','error_descripcion','error_titulo','error'));
        }

    }

    public function password_perfil_change($id){
        $error = null;
        return view('auth.administracion.change_password',compact('id','error'));
    }
}

