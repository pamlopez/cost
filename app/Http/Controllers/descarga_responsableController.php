<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createdescarga_responsableRequest;
use App\Http\Requests\Updatedescarga_responsableRequest;
use App\Models\cost_proyecto;
use App\Models\descarga_responsable;
use App\Repositories\descarga_responsableRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use function Sodium\compare;

class descarga_responsableController extends AppBaseController
{
    /** @var  descarga_responsableRepository */
    private $descargaResponsableRepository;

    public function __construct(descarga_responsableRepository $descargaResponsableRepo)
    {
        $this->descargaResponsableRepository = $descargaResponsableRepo;

        // arreglo que contiene la configuracion del datatable variable que podra modificarse en la vista
        $this->data_table = [
            "searching"=>   false,
            "paging"=>   true,
            "ordering"=> false,
            "info"=>     false,
            "dom"=> 'Bfrtip',
            "buttons"=> [
                ["extend"=>"copy","className"=>"btn bg-gray","footer"=> true],
                ["extend"=>"excel","className"=>"btn bg-olive","footer"=> true],
                ["extend"=>"pdf","className"=>"btn bg-red","footer"=> true],
                ["extend"=>"print","className"=>"btn bg-yellow","footer"=> true]
            ],
            "language"=> [
                "url" => url('/js/reportes/data_tables/language/Spanish.json'),
                "buttons"=> [
                    "copyTitle" =>'Copiado',
                    "copyKeys"=> 'Preciona <i>ctrl</i> ou <i>\u2318</i> + <i>C</i> para copiar los datos de la tabla a su portapapeles. <br><br>Para cancelar, haga clic en este mensaje o presione Esc.',
                    "copySuccess" => ["_"=> '%d Filas Copiadas  ',1=>'1 Fila Copiada'],
                    "copy" =>'<i class="fa fa-copy"></i> Copiar',
                    "excel"=>'<i class="fa fa-file-excel-o"></i> Excel',
                    "pdf"=>'<i class="fa fa-file-pdf-o"></i> PDF',
                    "print"=>'<i class="fa fa-print"></i> Imprimir',
                ]
            ]

        ];
    }

    /**
     * Display a listing of the descarga_responsable.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (isset($request)){
            $url =url()->full();
            $explode = explode('?',$url);
            if (isset($explode[1])){
                $id_proyecto = $explode[1];
                $descargaResponsables = descarga_responsable::where('id_proyecto',$id_proyecto)->get();
            }else{
                $this->descargaResponsableRepository->pushCriteria(new RequestCriteria($request));
                $descargaResponsables = $this->descargaResponsableRepository->all();
            }
        }else{
            $this->descargaResponsableRepository->pushCriteria(new RequestCriteria($request));
            $descargaResponsables = $this->descargaResponsableRepository->all();
        }

        return view('descarga_responsables.index')
            ->with('descargaResponsables', $descargaResponsables);
    }

    /**
     * Show the form for creating a new descarga_responsable.
     *
     * @return Respons
     */
    public function create()
    {
        return view('descarga_responsables.create');
    }

    /**
     * Store a newly created descarga_responsable in storage.
     *
     * @param Createdescarga_responsableRequest $request
     *
     * @return Response
     */
    public function store(Createdescarga_responsableRequest $request)
    {
        $input = $request->all();
        #try cat del insert sino que muestre muestre mensaje rediccion al index
        try {
            #guarda datos del que descarga
                $descargaResponsable = $this->descargaResponsableRepository->create($input);
            #buscar datos de la descarga
                if (isset($request->general)){//descarga todos proyecto
                    $tipo_descarga = 1;
                    $array_resultado = cost_proyecto::busca_datos_proyecto_descarga(0,$tipo_descarga);
                }else{//descarga proyecto especifico
                    $tipo_descarga = 2;
                    $array_resultado = cost_proyecto::busca_datos_proyecto_descarga($request->id_proyecto,$tipo_descarga);
                }

        }
        catch (\Exception $e) {
            $array_resultado=[];
            $array_resultado['proyecto']=false;
            $array_resultado['proyecto_error']=$e->getMessage();
            $array_resultado['proyecto_mensaje']=null;
        }


        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());
        return view('descarga_responsables.descarga_excel',compact('array_resultado','json_data_table','tipo_descarga'));
    }

    /**
     * Display the specified descarga_responsable.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try {
            #buscar datos de la descarga
            if ($id == 'general') {//descarga todos proyecto
                $tipo_descarga = 1;
                $array_resultado = cost_proyecto::busca_datos_proyecto_descarga(0, $tipo_descarga);
            } else {//descarga proyecto especifico
                $tipo_descarga = 2;
                $array_resultado = cost_proyecto::busca_datos_proyecto_descarga($id, $tipo_descarga);
            }

        } catch (\Exception $e) {
            $array_resultado = [];
            $array_resultado['proyecto'] = false;
            $array_resultado['proyecto_error'] = $e->getMessage();
            $array_resultado['proyecto_mensaje'] = null;
        }


        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());
        return view('descarga_responsables.descarga_excel', compact('array_resultado', 'json_data_table', 'tipo_descarga'));
    }
    /**
     * Show the form for editing the specified descarga_responsable.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $descargaResponsable = $this->descargaResponsableRepository->findWithoutFail($id);

        if (empty($descargaResponsable)) {
            Flash::error('Descarga Responsable not found');

            return redirect(route('descargaResponsables.index'));
        }

        return view('descarga_responsables.edit')->with('descargaResponsable', $descargaResponsable);
    }

    /**
     * Update the specified descarga_responsable in storage.
     *
     * @param  int              $id
     * @param Updatedescarga_responsableRequest $request
     *
     * @return Response
     */
    public function update($id, Updatedescarga_responsableRequest $request)
    {
        $descargaResponsable = $this->descargaResponsableRepository->findWithoutFail($id);

        if (empty($descargaResponsable)) {
            Flash::error('Descarga Responsable not found');

            return redirect(route('descargaResponsables.index'));
        }

        $descargaResponsable = $this->descargaResponsableRepository->update($request->all(), $id);

        Flash::success('Descarga Responsable updated successfully.');

        return redirect(route('descargaResponsables.index'));
    }

    /**
     * Remove the specified descarga_responsable from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $descargaResponsable = $this->descargaResponsableRepository->findWithoutFail($id);

        if (empty($descargaResponsable)) {
            Flash::error('Descarga Responsable not found');

            return redirect(route('descargaResponsables.index'));
        }

        $this->descargaResponsableRepository->delete($id);

        Flash::success('Descarga Responsable deleted successfully.');

        return redirect(route('descargaResponsables.index'));
    }
}
