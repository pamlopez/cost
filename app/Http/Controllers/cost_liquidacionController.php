<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_liquidacionRequest;
use App\Http\Requests\Updatecost_liquidacionRequest;
use App\Repositories\cost_liquidacionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_liquidacionController extends AppBaseController
{
    /** @var  cost_liquidacionRepository */
    private $costLiquidacionRepository;

    public function __construct(cost_liquidacionRepository $costLiquidacionRepo)
    {
        $this->costLiquidacionRepository = $costLiquidacionRepo;
    }

    /**
     * Display a listing of the cost_liquidacion.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->costLiquidacionRepository->pushCriteria(new RequestCriteria($request));
        $costLiquidacions = $this->costLiquidacionRepository->all();

        return view('cost_liquidacions.index')
            ->with('costLiquidacions', $costLiquidacions);
    }

    /**
     * Show the form for creating a new cost_liquidacion.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        return view('cost_liquidacions.create', compact('bnd_edit', 'id_proyecto'));
    }

    /**
     * Store a newly created cost_liquidacion in storage.
     *
     * @param Createcost_liquidacionRequest $request
     *
     * @return Response
     */
    public function store(Createcost_liquidacionRequest $request)
    {
        $input = $request->all();

        $costLiquidacion = $this->costLiquidacionRepository->create($input);

        Flash::success('La información de la liquidación se creó exitosamente');

        return redirect(route('costLiquidacions.show', [ $costLiquidacion->id ]));
    }

    /**
     * Display the specified cost_liquidacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costLiquidacion = $this->costLiquidacionRepository->findWithoutFail($id);

        if (empty($costLiquidacion)) {
            Flash::error('Cost Liquidacion not found');

            return redirect(route('costLiquidacions.index'));
        }

        return view('cost_liquidacions.show')->with('costLiquidacion', $costLiquidacion);
    }

    /**
     * Show the form for editing the specified cost_liquidacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costLiquidacion = $this->costLiquidacionRepository->findWithoutFail($id);
        $bnd_edit = true;
        if (empty($costLiquidacion)) {
            Flash::error('Cost Liquidacion not found');

            return redirect(route('costLiquidacions.index'));
        }

        return view('cost_liquidacions.edit', compact('bnd_edit'))->with('costLiquidacion', $costLiquidacion);
    }

    /**
     * Update the specified cost_liquidacion in storage.
     *
     * @param  int              $id
     * @param Updatecost_liquidacionRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_liquidacionRequest $request)
    {
        $costLiquidacion = $this->costLiquidacionRepository->findWithoutFail($id);

        if (empty($costLiquidacion)) {
            Flash::error('Cost Liquidacion not found');

            return redirect(route('costLiquidacions.index'));
        }

        $costLiquidacion = $this->costLiquidacionRepository->update($request->all(), $id);

        Flash::success('La información de la liquidación se actualizó exitosamente');

        return redirect(route('costLiquidacions.show',[$costLiquidacion->id]));
    }

    /**
     * Remove the specified cost_liquidacion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costLiquidacion = $this->costLiquidacionRepository->findWithoutFail($id);

        if (empty($costLiquidacion)) {
            Flash::error('Cost Liquidacion not found');

            return redirect(route('costLiquidacions.index'));
        }
        $id_proyecto =$costLiquidacion->id_proyecto;
        $this->costLiquidacionRepository->delete($id);

        Flash::success('La liquidación se borro exitosamente');

        return redirect(route('costLiquidacions.show', ['id_proyecto'=>$id_proyecto,'a'=>1] ));
    }
}
