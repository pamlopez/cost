<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_proyecto_dis_planRequest;
use App\Http\Requests\Updatecost_proyecto_dis_planRequest;
use App\Models\cost_proyecto;
use App\Models\cost_proyecto_dis_plan;
use App\Repositories\cost_proyecto_dis_planRepository;
use App\Repositories\cost_proyectoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_proyecto_dis_planController extends AppBaseController
{
    /** @var  cost_proyecto_dis_planRepository */
    private $costProyectoDisPlanRepository;

    public function __construct(cost_proyecto_dis_planRepository $costProyectoDisPlanRepo)
    {
        $this->costProyectoDisPlanRepository = $costProyectoDisPlanRepo;
    }

    /**
     * Display a listing of the cost_proyecto_dis_plan.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->costProyectoDisPlanRepository->pushCriteria(new RequestCriteria($request));
        $costProyectoDisPlans = $this->costProyectoDisPlanRepository->all();

        return view('cost_proyecto_dis_plans.index')
            ->with('costProyectoDisPlans', $costProyectoDisPlans);
    }

    /**
     * Show the form for creating a new cost_proyecto_dis_plan.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->cost;
        $filtros = cost_proyecto_dis_plan::criterios_default($request);
        $costProyecto = cost_proyecto::with("rel_dis_plan")->with("rel_planifica")->with("rel_supervision")->where("id_proyecto", "=",$id_proyecto)->first();
        return view('cost_proyecto_dis_plans.create',compact('id_proyecto','costProyecto','filtros','bnd_edit'));
    }

    /**
     * Store a newly created cost_proyecto_dis_plan in storage.
     *
     * @param Createcost_proyecto_dis_planRequest $request
     *
     * @return Response
     */
    public function store(Createcost_proyecto_dis_planRequest $request)
    {
        //dd('jpas');
        $input = $request->all();

       // dd($input);
        $costProyectoDisPlan = new cost_proyecto_dis_plan();
        $input[ 'f_resolucion_marn' ] = $request->f_resolucion_marn_submit;
        $input['presupuesto_moneda'] =  $request->presupuesto_moneda;
        ///$input['f_resolucion_marn'] = !empty($request->f_resolucion_marn)?$request->f_resolucion_marn_submit:$costProyectoDisPlan->f_resolucion_marn;
        $input['agrip_f'] = !empty($request->agrip_f)?$request->agrip_f_submit:$costProyectoDisPlan->agrip_f;
        $input['estudio_suelo_f'] =  !empty($request->estudio_suelo_f)?$request->estudio_suelo_f_submit:$costProyectoDisPlan->estudio_suelo_f;
        $input['estudio_topografia_f'] = !empty($request->estudio_topografia_f)?$request->estudio_topografia_f_submit:$costProyectoDisPlan->estudio_topografia_f;
        $input['estudio_geologico_f'] = !empty($request->estudio_geologico_f)?$request->estudio_geologico_f_submit:$costProyectoDisPlan->estudio_geologico_f;
        $input['fecha_hidrogeologico'] = !empty($request->fecha_hidrogeologico)?$request->fecha_hidrogeologico_submit:$costProyectoDisPlan->fecha_hidrogeologico;
        $input['diseno_estructural_f'] = !empty($request->diseno_estructural_f)?$request->diseno_estructural_f_submit:$costProyectoDisPlan->diseno_estructural_f;
        $input['memoria_calculo_f'] = !empty($request->memoria_calculo_f)?$request->memoria_calculo_f_submit:$costProyectoDisPlan->memoria_calculo_f;
        $input['plano_completo_f'] = !empty($request->plano_completo_f)?$request->plano_completo_f_submit:$costProyectoDisPlan->plano_completo_f;
        $input['dispo_especial_f'] = !empty($request->dispo_especial_f)?$request->dispo_especial_f_submit:$costProyectoDisPlan->dispo_especial_f;
        $input['otro_f'] =  !empty($request->otro_f)?$request->otro_f_submit:$costProyectoDisPlan->otro_f;
        $input['inst_ea_d'] =  !empty($request->inst_ea_d)?$request->inst_ea_d_submit:$costProyectoDisPlan->inst_ea_d;
        $input['presupuesto_aprobacion_f'] = !empty($request->presupuesto_aprobacion_f_submit)?$request->presupuesto_aprobacion_f_submit:$costProyectoDisPlan->resupuesto_aprobacion_f;
        $input['observaciones_topografia'] = !empty($request->observaciones_topografia)?$request->observaciones_topografia:$costProyectoDisPlan->observaciones_topografia;
        unset($input[ 'f_resolucion_marn_submit' ]);

        $costProyectoDisPlans = $this->costProyectoDisPlanRepository->create($input);

        Flash::success('Registro de preparación del proyecto creada exitosamente');
        $costProyecto= cost_proyecto::find($costProyectoDisPlan->id_proyecto);
        return redirect(route('costProyectoDisPlans.show',$costProyectoDisPlans->id_proyecto_dis_plan));

    }

    /**
     * Display the specified cost_proyecto_dis_plan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costProyectoDisPlan = $this->costProyectoDisPlanRepository->findWithoutFail($id);
        $costProyecto= cost_proyecto::find($costProyectoDisPlan->id_proyecto);

        if (empty($costProyectoDisPlan)) {
            Flash::error('Cost Proyecto Dis Plan not found');

            return redirect(route('costProyectoDisPlans.index'));
        }

        return view('cost_proyecto_dis_plans.show')->with('costProyecto', $costProyecto)->with('costProyectoDisPlan', $costProyectoDisPlan)->with('id', $id);
    }

    /**
     * Show the form for editing the specified cost_proyecto_dis_plan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
       $costProyectoDisPlan = $this->costProyectoDisPlanRepository->findWithoutFail($id);
       // dd($costProyectoDisPlan);
        $bnd_edit = true;
        if (empty($costProyectoDisPlan)) {
            Flash::error('Cost Proyecto Dis Plan not found');

            return redirect(route('costProyectoDisPlans.index'));
        }

        return view('cost_proyecto_dis_plans.edit')->with('costProyectoDisPlan', $costProyectoDisPlan)->with('bnd_edit',$bnd_edit)->with('id',$id);
    }

    /**
     * Update the specified cost_proyecto_dis_plan in storage.
     *
     * @param  int              $id
     * @param Updatecost_proyecto_dis_planRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_proyecto_dis_planRequest $request)
    {
       // dd($request);
        $costProyectoDisPlan = $this->costProyectoDisPlanRepository->findWithoutFail($id);
        $input = $request->all();
        $input['f_resolucion_marn'] = !empty($request->f_resolucion_marn)?$request->f_resolucion_marn_submit:$costProyectoDisPlan->f_resolucion_marn;
        $input['agrip_f'] = !empty($request->agrip_f)?$request->agrip_f_submit:$costProyectoDisPlan->agrip_f;
        $input['estudio_suelo_f'] =  !empty($request->estudio_suelo_f)?$request->estudio_suelo_f_submit:$costProyectoDisPlan->estudio_suelo_f;
        $input['estudio_topografia_f'] = !empty($request->estudio_topografia_f)?$request->estudio_topografia_f_submit:$costProyectoDisPlan->estudio_topografia_f;
        $input['estudio_geologico_f'] = !empty($request->estudio_geologico_f)?$request->estudio_geologico_f_submit:$costProyectoDisPlan->estudio_geologico_f;
        $input['fecha_hidrogeologico'] = !empty($request->fecha_hidrogeologico)?$request->fecha_hidrogeologico_submit:$costProyectoDisPlan->fecha_hidrogeologico;
        $input['diseno_estructural_f'] = !empty($request->diseno_estructural_f)?$request->diseno_estructural_f_submit:$costProyectoDisPlan->diseno_estructural_f;
        $input['memoria_calculo_f'] = !empty($request->memoria_calculo_f)?$request->memoria_calculo_f_submit:$costProyectoDisPlan->memoria_calculo_f;
        $input['plano_completo_f'] = !empty($request->plano_completo_f)?$request->plano_completo_f_submit:$costProyectoDisPlan->plano_completo_f;
        $input['dispo_especial_f'] = !empty($request->dispo_especial_f)?$request->dispo_especial_f_submit:$costProyectoDisPlan->dispo_especial_f;
        $input['otro_f'] =  !empty($request->otro_f)?$request->otro_f_submit:$costProyectoDisPlan->otro_f;
        $input['inst_ea_d'] =  !empty($request->inst_ea_d)?$request->inst_ea_d_submit:$costProyectoDisPlan->inst_ea_d;
        $input['presupuesto_aprobacion_f'] = !empty($request->presupuesto_aprobacion_f_submit)?$request->presupuesto_aprobacion_f_submit:$costProyectoDisPlan->presupuesto_aprobacion_f;
        $input['observaciones_topografia'] = !empty($request->observaciones_topografia)?$request->observaciones_topografia:$costProyectoDisPlan->observaciones_topografia;
        $input['presupuesto_moneda'] =  $request->presupuesto_moneda;
        unset($input['presupuesto_aprobacion_f_submit']);

        if (empty($costProyectoDisPlan)) {
            Flash::error('No se pudo encontrar el registro que busca');
            return redirect(route('costProyectoDisPlans.index'));
        }
       // dd($input);

        $costProyectoDisPlan = $this->costProyectoDisPlanRepository->update($input, $id);
        //dd($costProyectoDisPlan );
        Flash::success('Se ingresado el registro correctamente');
        return redirect(route('costProyectoDisPlans.show',$id));
    }

    /**
     * Remove the specified cost_proyecto_dis_plan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costProyectoDisPlan = $this->costProyectoDisPlanRepository->findWithoutFail($id);

        if (empty($costProyectoDisPlan)) {
            Flash::error('Cost Proyecto Dis Plan not found');

            return redirect(route('costProyectoDisPlans.index'));
        }

        $this->costProyectoDisPlanRepository->delete($id);

        Flash::success('Cost Proyecto Dis Plan deleted successfully.');

        return redirect(route('costProyectoDisPlans.index'));
    }

    public function vista_plana($id){
        dd($id);
    }
}
