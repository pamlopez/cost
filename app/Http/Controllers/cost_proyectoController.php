<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_proyectoRequest;
use App\Http\Requests\Updatecost_proyectoRequest;
use App\Models\cat_item;
use App\Models\cost_info_contrato;
use App\Models\cost_liquidacion;
use App\Models\cost_liquidacion_alcance;
use App\Models\cost_liquidacion_monto;
use App\Models\cost_liquidacion_tiempo;
use App\Models\cost_oferente;
use App\Models\cost_pago_efectuado;
use App\Models\cost_proyecto;
use App\Models\cost_proyecto_foto;
use App\Models\documento;
use App\Models\participante;
use App\Models\proyecto_view;
use App\Repositories\cost_proyectoRepository;
use App\Http\Controllers\AppBaseController;
use App\User;
use Carbon\Carbon;
use Doctrine\DBAL\Driver\IBMDB2\DB2Driver;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Monolog\Handler\IFTTTHandler;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_proyectoController extends AppBaseController
{

    /** @var  cost_proyectoRepository */
    private $costProyectoRepository;

    public function __construct(cost_proyectoRepository $costProyectoRepo)
    {
        $this->costProyectoRepository = $costProyectoRepo;
    }

    /**
     * Display a listing of the cost_proyecto.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $filtros = cost_proyecto::criterios_default($request);

        $query = cost_proyecto::filtrarOrdenar($filtros);

        $costProyectos = $query->paginate(10);

        /**Verificar si tiene permiso de digitador*/
        $you_have_permission = User::tiene_rol(2);
        $you_have_permission_publicador = User::tiene_rol(3);


        return view('cost_proyectos.index', compact('costProyectos', 'request', 'filtros', 'you_have_permission', 'you_have_permission_publicador'))
            ->with('rel_historial_descarga');
    }

    /**
     * Show the form for creating a new cost_proyecto.
     *
     * @return Response
     */
    public function create()
    {
        $edit = 0;
        $costProyecto = new cost_proyecto();
        $select_entidad_aquisicion = participante::all();
        return view('cost_proyectos.create', compact('edit', 'costProyecto'));
    }

    public function limpia_nulos($arreglo)
    {

        foreach ($arreglo as $key => $value) {

            if (($value == null) or ($value == '')) {
                unset($arreglo->$key);
            }
        }

        $tmp = (array)$arreglo;
    }

    /**
     * Store a newly created cost_proyecto in storage.
     *
     * @param Createcost_proyectoRequest $request
     *
     * @return Response
     */
    public function store(Createcost_proyectoRequest $request)
    {

        $model = new cost_proyecto();
        $model->id_proyecto = isset($request->id_proyecto) ? $request->id_proyecto : $model->id_proyecto;
        $model->entidad_adquisicion = isset($request->entidad_adquisicion) ? $request->entidad_adquisicion : $model->entidad_adquisicion;
        $model->id_entidad_adquisicion = isset($request->id_entidad_adquisicion) ? intval($request->id_entidad_adquisicion) : $model->id_entidad_adquisicion;
        $model->entidad_admin_contrato = isset($request->entidad_admin_contrato) ? $request->entidad_admin_contrato : $model->entidad_admin_contrato;
        $model->sector = isset($request->sector) ? $request->sector : $model->sector;
        $model->sub_sector = isset($request->sub_sector) ? $request->sub_sector : $model->sub_sector;
        $model->programa_multi_d = isset($request->programa_multi_d) ? $request->programa_multi_d : $model->programa_multi_d;
        $model->programa_multi = isset($request->programa_multi) ? $request->programa_multi : $model->programa_multi;
        $model->presupuesto_proyecto_multi_d = isset($request->presupuesto_proyecto_multi_d) ? $request->presupuesto_proyecto_multi_d : $model->presupuesto_proyecto_multi_d;
        $model->presupuesto_proyecto_multi = isset($request->presupuesto_proyecto_multi) ? $request->presupuesto_proyecto_multi : $model->presupuesto_proyecto_multi;
        $model->plan_adqui_d = isset($request->plan_adqui_d) ? $request->plan_adqui_d : $model->plan_adqui_d;
        $model->plan_adqui = isset($request->plan_adqui) ? $request->plan_adqui : $model->plan_adqui;
        $model->contacto_entidad = isset($request->contacto_entidad) ? $request->contacto_entidad : $model->contacto_entidad;
        $model->nombre_proyecto = isset($request->nombre_proyecto) ? $request->nombre_proyecto : $model->nombre_proyecto;
        $model->detalle_responsable = isset($request->detalle_responsable) ? $request->detalle_responsable : $model->detalle_responsable;
        $model->fh_ultima_actualizacion = isset($fh_ultima_actualizacion_submit) && $request->fh_ultima_actualizacion_submit != '' ? $request->fh_ultima_actualizacion_submit : $model->fh_ultima_actualizacion_submit;
        $model->localizacion = isset($request->localizacion) ? $request->localizacion : $model->localizacion;
        $model->localizacion_muni_d = isset($request->localizacion_muni_d) ? $request->localizacion_muni_d : $model->localizacion_muni_d;
        $model->localizacion_muni = isset($request->localizacion_muni) ? $request->localizacion_muni : $model->localizacion_muni;
        $model->localizacion_depto_d = isset($request->localizacion_depto_d) ? $request->localizacion_depto_d : $model->localizacion_depto_d;
        $model->localizacion_depto = isset($request->localizacion_muni_depto) ? $request->localizacion_muni_depto : $model->localizacion_depto;
        $model->coordenadas = isset($request->coordenadas) ? $request->coordenadas : $model->coordenadas;
        $model->coordenadas_cln = isset($request->coordenadas_cln) ? $request->coordenadas_cln : $model->coordenadas_cln;
        $model->latitud = isset($request->latitud) ? $request->latitud : $model->latitud;
        $model->longitud = isset($request->longitud) ? $request->longitud : $model->longitud;
        $model->snip = $request->snip > 0 ? $request->snip : 0;
        $model->snip_detalle = isset($request->snip_detalle) ? $request->snip_detalle : $model->snip_detalle;
        $model->proposito = isset($request->proposito) ? $request->proposito : $model->proposito;
        $model->analisis_alternativas = isset($request->analisis_alternativas) ? $request->analisis_alternativas : $model->analisis_alternativas;
        $model->descripcion = isset($request->descripcion) ? $request->descripcion : $model->descripcion;
        $model->resumen = isset($request->resumen) ? $request->resumen : $model->resumen;
        $model->beneficiario_d = isset($request->beneficiario_d) ? $request->beneficiario_d : $model->beneficiario_d;
        $model->beneficiario_total = $request->beneficiario_total > 0 ? $request->beneficiario_total : 0;
        $model->f_aprobacion = isset($request->f_aprobacion_submit) && $request->f_aprobacion_submit != '' ? $request->f_aprobacion_submit : $model->f_aprobacion_submit;
        $model->f_r_proactiva = isset($request->f_r_proactiva_submit) && $request->f_r_proactiva_submit != '' ? $request->f_r_proactiva_submit : $model->f_r_proactiva_submit;
        $model->f_r_reactiva = isset($request->f_r_reactiva_submit) && $request->f_r_reactiva_submit != '' ? $request->f_r_reactiva_submit : $model->f_r_reactiva_submit;
        $model->f_finalizacion = isset($request->f_finalizacion_submit) && $request->f_finalizacion_submit != '' ? $request->f_finalizacion_submit : $model->f_finalizacion_submit;
        $model->f_ingreso = Carbon::now()->format('Y-m-d');
        $model->foto = $request->foto > 0 ? $request->foto : 0;
        $model->pdf = $request->pdf > 0 ? $request->pdf : 0;
        $model->nog = $request->nog > 0 ? $request->nog : 0;
        $model->sector_oc = $request->sector_oc > 0 ? $request->sector_oc : 0;
        $model->estado_oc_d = $request->estado_oc > 0 ? cat_item::find($request->estado_oc)->descripcion : 0;
        $model->estado_oc = $request->estado_oc;
        $model->tipo_oc = $request->tipo_oc > 0 ? $request->tipo_oc : 0;
        $model->pais = isset($request->pais) ? $request->pais : '';
        $model->codigo_postal = isset($request->codigo_postal) ? $request->codigo_postal : '';

        $model->avance_financiero = $request->avance_financiero != '' ? $request->avance_financiero : 0;
        //dd($model);
        $model->entidad_admin_contrato = isset($request->entidad_admin_contrato) ? $request->entidad_admin_contrato : $model->entidad_admin_contrato;
        $model->estado_actual_proyecto = isset($request->estado_actual_proyecto) ? $request->estado_actual_proyecto : $model->estado_actual_proyecto;
        $model->operador_portal = isset($request->operador_portal) ? $request->operador_portal : $model->operador_portal;
        $model->nog_f = isset($request->nog_f_submit) ? $request->nog_f_submit : '';
//dd($model);
        $model->save();

        /***se crean nuevos registros actuales y reemplazan por los seleccionados**/
        foreach (Input::get('roles') as $key => $value) {
            $rol_otro = cat_item::find($value);
            /**Buscar rol para usar el campo otro */
            cost_oferente::create([
                'id_proyecto' => $model->id_proyecto,
                'rol' => intval($rol_otro->otro),
                'id_fase' => 1
            ]);
        }

        Flash::success('Registro ingresado correctamente.');

        return redirect(route('costProyectos.index'));
    }

    function ocds(Request $request)
    {
        $id = $request->id;
        $json_format = $this->ocestandar($id);
        return view('cost_proyectos.proyecto', compact('json_format'));
    }

    public function ocestandar($id,$fase=0,$parte=0,$busqueda=0)
    {
        $costProyecto = cost_proyecto::with("rel_dis_plan")
            ->where("id_proyecto", "=", $id)->first();
        // Datos  para OC4IDS
        $datos_ocds = cost_proyecto::vista_proyecto_ocds($costProyecto->id_proyecto);
        $datos_ocds = count($datos_ocds) > 0 ? $datos_ocds[0] : $datos_ocds;
        unset($datos_ocds->id_proyecto);

        //Se crea el arreglo principal del oc4ids
        $datos_ocds = (object)array_filter((array)$datos_ocds);

        //Datos generales sobre el proyecto


        // Se gestionan los datos del período
        $datos_periodo_ocds = cost_proyecto::vista_proyecto_periodo_ocds($costProyecto->id_proyecto);
        $datos_periodo_ocds = count($datos_periodo_ocds) > 0 ? $datos_periodo_ocds[0] : $datos_periodo_ocds;
        $datos_periodo_ocds = (object)array_filter((array)$datos_periodo_ocds);

        // Se agregan  los datos del período al arreglo principal
        if (count($datos_periodo_ocds) > 0) {
            $datos_ocds->period = $datos_periodo_ocds;
        }


        //Datos sobre las direcciones del proyecto del proyecto
        $datos_localidad_ocds = cost_proyecto::vista_proyecto_localidad_ocds($costProyecto->id_proyecto);
        $datos_localidad_ocds = count($datos_localidad_ocds) > 0 ? $datos_localidad_ocds[0] : $datos_localidad_ocds;
        $datos_localidad_ocds = (object)array_filter((array)$datos_localidad_ocds);
        $datos_geometria_ocds = cost_proyecto::vista_proyecto_geometria_ocds($costProyecto->id_proyecto);
        $datos_geometria_ocds = count($datos_geometria_ocds) > 0 ? $datos_geometria_ocds[0] : $datos_geometria_ocds;
        $datos_geometria_ocds = (object)array_filter((array)$datos_geometria_ocds);
        $datos_direccion_ocds = cost_proyecto::vista_proyecto_direccion_ocds($costProyecto->id_proyecto);
        $datos_direccion_ocds = count($datos_direccion_ocds) > 0 ? $datos_direccion_ocds[0] : $datos_direccion_ocds;
        $datos_direccion_ocds = (object)array_filter((array)$datos_direccion_ocds);
        $datos_direccion_ocds = (array)$datos_direccion_ocds;

        //Se agregan los datos de las direcciones al arreglo principal

        if (count($datos_localidad_ocds) > 0) {
            $datos_ocds->locations = $datos_localidad_ocds;
        }
        if (count($datos_geometria_ocds) > 0 && isset($datos_geometria_ocds->coordinates) && ($datos_geometria_ocds->coordinates[0] != 0.0 || $datos_geometria_ocds->coordinates[0] != 0)) {
            $datos_localidad_ocds->geometry = $datos_geometria_ocds;
        }
        if (count($datos_direccion_ocds) > 0) {
            $datos_localidad_ocds->address = $datos_direccion_ocds;
        }
        if (count($datos_localidad_ocds) > 0) {
            $local[0] = $datos_localidad_ocds;
            $datos_ocds->locations = $local;
        }

        //Se gestionan los datos de los montos se manda a llamar dos veces la misma vista por que se necesitan los datos como parte del arreglo
/*        $datos_presupuesto_ocds = cost_proyecto::vista_presupuesto_ocds($costProyecto->id_proyecto, 2);
        $datos_presupuesto_ocds = count($datos_presupuesto_ocds) > 0 ? $datos_presupuesto_ocds[0] : $datos_presupuesto_ocds;
        $datos_presupuesto_ocds = (object)array_filter((array)$datos_presupuesto_ocds);
        $datos_presupuesto_ocds = (array)$datos_presupuesto_ocds;*/


        $datos_presupuesto_monto_ocds = cost_proyecto::vista_presupuesto_ocds($costProyecto->id_proyecto, 1);
        $datos_presupuesto_monto_ocds = count($datos_presupuesto_monto_ocds) > 0 ? $datos_presupuesto_monto_ocds[0] : $datos_presupuesto_monto_ocds;
        $datos_presupuesto_monto_ocds = (object)array_filter((array)$datos_presupuesto_monto_ocds);
        $datos_presupuesto_monto_ocds = (array)$datos_presupuesto_monto_ocds;


        //Se agregan los datos del budget o presupuesto al arreglo principal
        if (isset($datos_presupuesto_monto_ocds['amount']) and $datos_presupuesto_monto_ocds['amount'] > 0) {
            if (!isset($datos_presupuesto_ocds) || count($datos_presupuesto_ocds) == 0) {
                $datos_presupuesto_ocds = new \stdClass();
            }

            $datos_presupuesto_ocds->amount = $datos_presupuesto_monto_ocds['amount'];
            if ($datos_presupuesto_ocds->amount > 0) {
                $datos_presupuesto_ocds->currency = $datos_presupuesto_monto_ocds['currency'];
            }

            if(isset($datos_presupuesto_monto_ocds['approvalDate'])){
                $datos_presupuesto_ocds->approvalDate = $datos_presupuesto_monto_ocds['approvalDate'];
            }
        }
        if (count($datos_presupuesto_monto_ocds) > 0) {
             $datos_ocds->budget = $datos_presupuesto_ocds;

         }

        //información general de los participantes
        $participantes = cost_proyecto::vista_oferentes($costProyecto->id_proyecto);
        $participantes = (object)array_filter((array)$participantes);

        if (count($participantes) > 0) {
            foreach ($participantes as $participante) {
                $participante_principal_ocds = cost_proyecto::vista_participantes($participante->id_participante);
                $participante_principal_ocds = $participante_principal_ocds[0];
                $participante_principal_ocds = (object)array_filter((array)$participante_principal_ocds);

                $identificador = cost_proyecto::vista_participantes_schema($participante->id_participante);
                $identificador = $identificador[0];
                $identificador = (object)array_filter((array)$identificador);

                $direccion = cost_proyecto::vista_participantes_direccion($participante->id_participante);
                $direccion = $direccion[0];
                $direccion = (object)array_filter((array)$direccion);

                $contact = cost_proyecto::vista_participantes_contact($participante->id_participante);
                $contact = (object)array_filter((array)$contact[0]);

                $roles = cost_proyecto::roles_participante($participante->id_participante);

                $roles = array_filter((array)$roles);
                $tmp = (array)$identificador;


                $tmp = (array)$direccion;
                if (!empty($tmp))
                    $participante_principal_ocds->address = $direccion;

                $tmp = (array)$contact;
                if (!empty($tmp))
                    $participante_principal_ocds->contactPoint = $contact;

                $tmp = (array)$roles;
                $r = array();
                if (!empty($tmp)) {
                    foreach ($tmp as $t) {
                        $r[] = $t->rol;
                    }
                }

                $participante_principal_ocds->roles = $r;

                if (!empty($tmp))
                    $participante_principal_ocds->identifier = $identificador;


                $clase[] = $participante_principal_ocds;
            }

            //Agrega los participantes al proyecto

            if (isset($clase) and count($clase) > 0)
                $datos_ocds->parties = $clase;
        }

        //Se buscan los documentos  del proyecto

        $documentos = cost_proyecto::vista_documentos($costProyecto->id_proyecto);
        $documentos = count($documentos) > 0 ? $documentos : null;
        $documentos = (object)array_filter((array)$documentos);

        $tmp = (array)$documentos;
        if (!empty($tmp)) {

            if (count($documentos) > 0) {
                foreach ($documentos as $documento) {
                    $tmp_tender = (object)array_filter((array)$documento);
                    $tmpo[] = (array)$tmp_tender;
                }
                //Se agregan los documentos al arreglo
                $datos_ocds->documents = $tmpo;

            }
        }



        //Se verifica si tiene datos sobre la liquidación del proyecto
        $datos_liquidacion = cost_proyecto::vista_liquidacion($costProyecto->id_proyecto);
        $datos_liquidacion = count($datos_liquidacion) > 0 ? $datos_liquidacion[0] : $datos_liquidacion;
        $datos_liquidacion = (object)array_filter((array)$datos_liquidacion);


        $tmp = (array)$datos_liquidacion;
        if (!empty($tmp)) {
            //Se agregan los datos de liquidación al arreglo general
            $datos_ocds->completion = $datos_liquidacion;
        }

        //Parte de OC4IDS procesos
        $process_id_diseno = cost_proyecto::vista_process_id($costProyecto->id_proyecto, 2);
        $process_id_contrato = cost_proyecto::vista_process_id($costProyecto->id_proyecto, 3);
        $process_id_supervision = cost_proyecto::vista_process_id($costProyecto->id_proyecto, 6);

        $procesos[2] = $process_id_diseno[0];
        $procesos[3] = $process_id_contrato[0];
        $procesos[6] = $process_id_supervision[0];
        $proceso_oc = [];

        foreach ($procesos as $key => $value) {
            $summary = cost_proyecto::vista_process_summary($costProyecto->id_proyecto, $key);

            if (count($summary) > 0) {
                $value->summary = (object)array_filter((array)$summary[0]);

                //Inicio de buscador de tenders
                $tender = cost_proyecto::vista_process_tender($costProyecto->id_proyecto, $key);
                if (count($tender) > 0) {
                    $tmp_tender = (object)array_filter((array)$tender[0]);
                    $tmp = (array)$tmp_tender;
                    if (!empty($tmp))
                        $value->summary->tender = $tmp_tender;
                }
                $amount = cost_proyecto::vista_process_tender_amount($costProyecto->id_proyecto, $key);
                if (count($amount) > 0) {
                    $tmp_tender = (object)array_filter((array)$amount[0]);
                    $tmp = (array)$tmp_tender;
                    if (!empty($tmp)) {
                        if (isset($value->summary->tender))
                            $value->summary->tender->costEstimate = $tmp;
                        else {
                            $value->summary->tender = new \stdClass();
                            $value->summary->tender->costEstimate = $tmp;
                        }
                    }
                }

                $total_oferentes = cost_proyecto::total_oferentes($costProyecto->id_proyecto, $key);

                if (count($total_oferentes) > 0) {
                    if (isset($value->summary->tender))
                        $value->summary->tender->numberOfTenderers = $total_oferentes;
                    else {
                        $value->summary->tender = new \stdClass();
                        $value->summary->tender->numberOfTenderers = $total_oferentes;
                    }
                }

                $procuring = cost_proyecto::vista_oferente_fase($costProyecto->id_proyecto, 0, 2);
                if (count($procuring) > 0) {
                    $tmp_tender = (object)array_filter((array)$procuring[0]);
                    $tmp = (array)$tmp_tender;

                    if (!empty($tmp)) {
                        if (isset($value->summary->tender)) {
                            $value->summary->tender->procuringEntity = $tmp;
                            $value->summary->tender->administrativeEntity = $tmp;
                        } else {
                            $value->summary->tender = new \stdClass();
                            $value->summary->tender->procuringEntity = $tmp;
                            $value->summary->tender->administrativeEntity = $tmp;
                        }
                        //Agrega los datos de los tenders si la parte es 1
                        if($fase==$key && $parte==1){
                            $masivo_parte= new \stdClass();
                            $masivo_parte = serialize($value);
                            $masivo_parte = unserialize($masivo_parte);
                        }
                    }
                }




                // Inicio de buscador de supliers
                $supplier = cost_proyecto::vista_oferente_fase($costProyecto->id_proyecto, $key, 3);
                if (count($supplier) > 0) {
                    $tmp_tender = (object)array_filter((array)$supplier);
                    $tmp = (array)$tmp_tender;
                    if (!empty($tmp)) {
                        $value->summary->suppliers = $supplier;
                        //Agrega los proveedores si selecciono esa parte
                        if($key==$fase && $parte==2){
                            unset( $value->summary->tender);
                            $masivo_parte= new \stdClass();
                            $masivo_parte = serialize($value);
                            $masivo_parte = unserialize($masivo_parte);
                        }
                    }
                }



                $cvalue = cost_proyecto::vista_summary_contract($costProyecto->id_proyecto, $key, 0);
                if (count($cvalue) > 0) {
                    $tmp_tender = (object)array_filter((array)$cvalue[0]);
                    $tmp = (array)$tmp_tender;
                    if (!empty($tmp)){
                        $value->summary->contractValue = $tmp;
                        //Agrega los monto del contrato si selecciono esa parte
                        if($key==$fase && $parte==3){
                            if(isset($value->summary->tender))  unset( $value->summary->tender);
                            if(isset($value->summary->suppliers))  unset( $value->summary->suppliers);
                            $masivo_parte= new \stdClass();
                            $masivo_parte = serialize($value);
                            $masivo_parte = unserialize($masivo_parte);
                        }
                    }

                }



                $cperiod = cost_proyecto::vista_summary_contract($costProyecto->id_proyecto, $key, 1);
                if (count($cperiod) > 0) {
                    $tmp_tender = (object)array_filter((array)$cperiod[0]);
                    $tmp = (array)$tmp_tender;
                    if (!empty($tmp)){
                        $value->summary->contractPeriod = $tmp;
                        //Agrega los periodos si selecciono esa parte
                        if($key==$fase && $parte==4){
                            if(isset($value->summary->tender))  unset( $value->summary->tender);
                            if(isset($value->summary->suppliers))  unset( $value->summary->suppliers);
                            if(isset($value->summary->contractValue))  unset( $value->summary->contractValue);
                            $masivo_parte= new \stdClass();
                            $masivo_parte = serialize($value);
                            $masivo_parte = unserialize($masivo_parte);
                        }
                    }

                }



                $cfinalv = cost_proyecto::vista_summary_contract($costProyecto->id_proyecto, $key, 2);

                if (count($cfinalv) > 0) {
                    $tmp_tender = (object)array_filter((array)$cfinalv[0]);
                    $tmp = (array)$tmp_tender;
                    if (!empty($tmp)) {
                       $value->summary->finalValue = $tmp;
                        //Agrega monto final si selecciono esa parte
                        if($key==$fase && $parte==5){
                            if(isset($value->summary->tender))  unset( $value->summary->tender);
                            if(isset($value->summary->suppliers))  unset( $value->summary->suppliers);
                            if(isset($value->summary->contractValue))  unset( $value->summary->contractValue);
                            if(isset( $value->summary->contractPeriod ))  unset(  $value->summary->contractPeriod );
                            $masivo_parte= new \stdClass();
                            $masivo_parte = serialize($value);
                            $masivo_parte = unserialize($masivo_parte);
                        }
                    }
                }

                //reasigno la fase de los documentos de ejecucion por la informacion previa
                if($key == 3){$key_doc = 4;}else{
                    $key_doc = $key;
                }

                $documents = cost_proyecto::vista_documentos_fase($costProyecto->id_proyecto, $key_doc);
                if (count($documents) > 0) {
                    foreach ($documents as $document) {
                        $tmp_tender = (object)array_filter((array)$document);
                        $tmp[] = (array)$tmp_tender;
                    }

                    if (!empty($tmp)) {
                        $value->summary->documents = $tmp;
                        //Agrega monto final si selecciono esa parte
                        if ($key == $fase && $parte == 6) {
                            if (isset($value->summary->tender)) unset($value->summary->tender);
                            if (isset($value->summary->suppliers)) unset($value->summary->suppliers);
                            if (isset($value->summary->contractValue)) unset($value->summary->contractValue);
                            if (isset($value->summary->contractPeriod)) unset($value->summary->contractPeriod);
                            if (isset($value->summary->finalValue)) unset($value->summary->finalValue);
                            $masivo_parte = new \stdClass();
                            $masivo_parte = serialize($value);
                            $masivo_parte = unserialize($masivo_parte);
                        }
                    }

                }

                $modificacion_alcance = cost_proyecto::vista_modifica_alcance($costProyecto->id_proyecto, $key);
                if (count($modificacion_alcance) > 0) {
                    foreach ($modificacion_alcance as $valor) {
                        $tmp_tender = (object)array_filter((array)$valor);
                        $tmp = (array)$tmp_tender;
                        $value->summary->modifications[] = $tmp;
                    }
                }

                $modificacion_monto = cost_proyecto::vista_modifica_monto($costProyecto->id_proyecto, 3);
                if (count($modificacion_monto) > 0) {
                    // dd($modificacion_monto);
                    foreach ($modificacion_monto as $valor) {
                        $a_monto['id'] = $valor->id;
                        $a_monto['description'] = $valor->description;
                        $a_monto['rationale'] = $valor->rationale;
                        $a_monto['date'] = $valor->date;
                        $a_monto['type'] = $valor->type;
                        $a_monto_a['amount'] = $valor->amountN - $valor->suma_mod;//monto antiguo
                        $a_monto_a['currency'] = cost_proyecto::busca_descripcion_catalogo($valor->currency);//
                        $tmp_tender = (object)array_filter((array)$a_monto_a);
                        $tmp = (array)$tmp_tender;
                        $a_monto['oldContractValue'] = $tmp;
                        $a_monto_n['amount'] = $valor->amountN;
                        $a_monto_n['currency'] = cost_proyecto::busca_descripcion_catalogo($valor->currencyN);//

                        if ($valor->amountN > 0) {
                            $a_monto_n['currency'] = $a_monto_n['currency'];
                        }

                        // $a_monto_n[ 'currency' ] = $valor->currencyN;
                        $tmp_tender = (object)array_filter((array)$a_monto_n);
                        $tmp = (array)$tmp_tender;
                        $a_monto['newContractValue'] = $tmp;

                        $tmp_tender = (object)array_filter((array)$a_monto);
                        $tmp = (array)$tmp_tender;
                        $value->summary->modifications[] = $tmp;
                    }
                }

                $modificacion_tiempo = cost_proyecto::vista_modifica_tiempo($costProyecto->id_proyecto, $key);
                if (count($modificacion_tiempo) > 0) {
                    foreach ($modificacion_tiempo as $valor) {
                        $a_tiempo['id'] = $valor->id;
                        $a_tiempo['description'] = $valor->description;
                        $a_tiempo['rationale'] = $valor->rationale;
                        $a_tiempo['date'] = $valor->date;
                        $a_tiempo['type'] = $valor->type;
                        $a_tiempo_a['startDate'] = $valor->startDate;
                        $a_tiempo_a['endDate'] = $valor->endDate;
                        $a_tiempo_a['durationInDays'] = intval($valor->durationInDays);
                        $tmp_tender = (object)array_filter((array)$a_tiempo_a);
                        $tmp = (array)$tmp_tender;
                        $a_tiempo['oldContractPeriod'] = $tmp;
                        $a_tiempo_n['startDate'] = $valor->startDateN;
                        $a_tiempo_n['endDate'] = $valor->endDateN;
                        $a_tiempo_n['durationInDays'] = intval($valor->durationInDaysN);
                        $tmp_tender = (object)array_filter((array)$a_tiempo_n);
                        $tmp = (array)$tmp_tender;
                        $a_tiempo['newContractPeriod'] = $tmp;
                        $tmp_tender = (object)array_filter((array)$a_tiempo);
                        $tmp = (array)$tmp_tender;
                        $value->summary->modifications[] = $tmp;
                    }
                }

                if ($key == $fase && $parte == 7 && isset($value->summary->modifications)) {
                    if (isset($value->summary->tender)) unset($value->summary->tender);
                    if (isset($value->summary->suppliers)) unset($value->summary->suppliers);
                    if (isset($value->summary->contractValue)) unset($value->summary->contractValue);
                    if (isset($value->summary->contractPeriod)) unset($value->summary->contractPeriod);
                    if (isset($value->summary->finalValue)) unset($value->summary->finalValue);
                    if (isset($value->summary->documents)) unset($value->summary->documents);
                    $masivo_parte = new \stdClass();
                    $masivo_parte = serialize($value);
                    $masivo_parte = unserialize($masivo_parte);
                }


                $proceso_oc[] = $value;


                if($fase == $key && $parte ==0){
                    $fase_arreglo = $value;
                }elseif($fase>0 && $parte>0){
                    $fase_arreglo = $masivo_parte;
                }



            } else {
                $value->summary = $summary;
            }


        }



        if($fase>0){
           if($fase_arreglo == null)  {
               $datos_ocds = null;
           }else{
               $datos_ocds_fase  = new \stdClass();;
               $datos_ocds_fase->contractingProcess =  $fase_arreglo;
               $datos_ocds = $datos_ocds_fase;
           }


        }else{
            if (count($proceso_oc) > 0) {
                $datos_ocds->contractingProcesses = $proceso_oc;
            }
        }


        $package['uri'] = url('costProyectos/'.$id);
        $package['publishedDate'] = $datos_ocds->updated;
        $package['version'] = "0.9";
        $publisher['name'] = "CoST Guatemala";
        $package['publisher'] = (object)$publisher;
        $package['license'] = "https://creativecommons.org/licenses/by/4.0/legalcode";
        //   $package['publicationPolicy'] = "https://politica";

        if($busqueda==1){
            $b_result[0]=$package;
            $b_result[1]=$datos_ocds;
            return $b_result;
        }else{
            $package['projects'][] = $datos_ocds;
            $package = (object)array_filter((array)$package);
            $json_format = json_encode($package, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return $json_format;
        }

    }

    /**
     * Display the specified cost_proyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, Request $request)
    {
        if (isset($request->a)) {
            $a = $request->a;
        } else {
            $a = 0;
        }

        $you_have_permission = User::tiene_rol(2);
        $you_have_permission_publicador = User::tiene_rol(3);

        $costProyecto = cost_proyecto::with("rel_dis_plan")
            ->with("rel_planifica")
            ->with("rel_dis_plan")
            ->with("rel_supervision")
            ->with("rel_acta")
            ->with("rel_oferente")
            ->with("rel_acuerdo_financiamiento")
            ->with("rel_ampliacion")
            ->with("rel_fianza")
            ->with("rel_info_contrato")
            ->with("rel_liquidacion")
            ->with("rel_liquidacion_monto")
            ->with("rel_liquidacion_alcance")
            ->with("rel_liquidacion_tiempo")
            ->with("rel_pago_efectuado")
            ->where("id_proyecto", "=", $id)->first();

        if ($costProyecto->latitud == null) {
            $costProyecto->latitud = '14.64072';
        }
        if ($costProyecto->longitud == null) {
            $costProyecto->longitud = '-90.51327';
        }

        $mc = cost_info_contrato::where('id_proyecto', $id)->where('id_fase', 3)->first();

        $modalidad_contrato = isset($mc->fmt_modalidad_contratacion) ? $mc->fmt_modalidad_contratacion : "Información no disponible";

        $empresa_contratada = isset($mc->empresa_constructora) ? $mc->empresa_constructora : "Información no disponible";
        $monto_contratado = isset($mc->precio_contrato) ? $mc->precio_contrato : "Información no disponible";

        $mcd = cost_info_contrato::where('id_proyecto', $id)->where('id_fase', 2)->first();
        $responsable_diseno = (isset($mcd->nombre_responsable_seleccionado) && strlen($mcd->nombre_responsable_seleccionado) > 0) ? $mcd->nombre_responsable_seleccionado : "Información no disponible";
        $responsable_estudio = (isset($mcd->nombre_firma) && strlen($mcd->nombre_firma) > 0) ? $mcd->nombre_firma : "Información no disponible";

        $fecha_inicio = isset($mc->fecha_inicio) ? Carbon::parse($mc->fecha_inicio)->format('d/m/Y') : "Información no disponible";
        $mcs = cost_info_contrato::where('id_proyecto', $id)->where('id_fase', 6)->first();
        $responsable_supervision = (isset($mcs->nombre_responsable_seleccionado) && strlen($mcs->nombre_responsable_seleccionado) > 0) ? $mcs->nombre_responsable_seleccionado : "Información no disponible";


        $costOferentes = cost_oferente::where('id_proyecto', $id)->where('id_fase', 3)->where('rol', 1)->get();
        $costLiquidacionMontos = cost_liquidacion_monto::where('id_proyecto', $id)->where('id_fase', 3)->get();
        $costLiquidacionTiempos = cost_liquidacion_tiempo::where('id_proyecto', $id)->where('id_fase', 3)->get();
        $costLiquidacionAlcances = cost_liquidacion_alcance::where('id_proyecto', $id)->where('id_fase', 3)->get();
        $costLiquidacionMontos_s = cost_liquidacion_monto::where('id_proyecto', $id)->where('id_fase', 6)->get();
        $costLiquidacionTiempos_s = cost_liquidacion_tiempo::where('id_proyecto', $id)->where('id_fase', 6)->get();
        $costLiquidacionAlcances_s = cost_liquidacion_alcance::where('id_proyecto', $id)->where('id_fase', 6)->get();
        $costPagoEfectuado = cost_pago_efectuado::where('id_proyecto', $id)->where('id_fase', 3)->get();
        $costDocumentos = documento::where('id_proyecto', $id)->get();
        count($costPagoEfectuado) > 0 ? $indice_28 = 1 : $indice_28 = 0;
        $costLiquidacion = cost_liquidacion::where('id_proyecto', $id)->get();
        (count($costLiquidacion) > 0 && strlen($costLiquidacion[0]->informe_avances) > 0) ? $indice_26 = 1 : $indice_26 = 0;
        (count($costLiquidacion) > 0 && strlen($costLiquidacion[0]->reporte_evaluacion) > 0) ? $indice_31 = 1 : $indice_31 = 0;
        $indice_23 = 0;
        if (count($costLiquidacionMontos_s) > 0 || count($costLiquidacionTiempos_s) > 0 || count($costLiquidacionAlcances_s)) {
            $indice_23 = 1;
        } else {
            $indice_23 = 0;
        }
        $indice_27 = 0;
        $indice_24 = 0;
        if (count($costLiquidacionMontos) > 0) {
            $indice_24 = 1;
            $cuantos = count($costLiquidacionMontos) - 1;
            $costLiquidacionMontos[$cuantos]->monto_actualizado > 0 ? $indice_27 = 1 : $indice_27 = 0;

        } else {
            $indice_24 = 0;
        }
        $indice_25 = 0;
        if (count($costLiquidacionTiempos)) {
            $indice_25 = 1;
        } else {
            $indice_25 = 0;
        }

        $indice_29 = 0;
        if (count($costLiquidacionAlcances)) {
            $indice_29 = 1;
        } else {
            $indice_29 = 0;
        }


        if (empty($costProyecto)) {
            Flash::error('Registro no encontrado');

            return redirect(route('costProyectos.index'));
        }

        //actas
        $actas = $costProyecto->rel_acta;
        $acta_diseno = 0;
        $acta_adj_sup = 0;
        $acta_adj_eje = 0;
        foreach ($actas as $acta) {
            if ($acta->id_fase == 2 && $acta->tipo == 8134) {
                $acta_diseno = 1;
            }
            if ($acta->id_fase == 3 && $acta->tipo == 8134) {
                $acta_adj_eje = 1;
            }
            if ($acta->id_fase == 6 && $acta->tipo == 8134) {
                $acta_adj_sup = 1;
            }
        }

        $dis_plan = $costProyecto->rel_dis_plan;
        $fuentes_financiamiento = $costProyecto->rel_acuerdo_financiamiento;
        $fuente_diseno = 0;
        $fuente_ejecucion = 0;
        $fuente_supervision = 0;
        foreach ($fuentes_financiamiento as $fuente) {
            if ($fuente->id_fase == 2) {
                $fuente_diseno = 1;
            } elseif ($fuente->id_fase == 3) {
                $fuente_ejecucion = 1;
            } elseif ($fuente->id_fase == 6) {
                $fuente_supervision = 1;
            }
        }

        $info_contratos = $costProyecto->rel_info_contrato;
        $fecha_publicacion_eje = 0;
        $bases_concurso_eje = 0;
        $fecha_ultima_mod_gc_eje = 0;
        $total_inconformidades_eje = 0;
        $total_inconformidades_r_eje = 0;
        $fecha_aprobacion_contrato = 0;
        $monto_contrato_sup = 0;
        $alcance_sup = 0;
        $programa_sup = 0;
        $nombre_contratista = 0;
        $monto_contrato = 0;
        $alcance = 0;
        $programa = 0;
        $indice_30 = 0;

        foreach ($info_contratos as $inf_contrato) {
            if ($inf_contrato->id_fase == 3) {
                if ($inf_contrato->fecha_publicacion != null &&
                    $inf_contrato->fecha_publicacion->format('Y') > 1990) {
                    $fecha_publicacion_eje = 1;
                }
                if (strlen($inf_contrato->bases_concurso) > 0) {
                    $bases_concurso_eje = 1;
                }
                if ($inf_contrato->fecha_ultima_mod_gc != null &&
                    $inf_contrato->fecha_ultima_mod_gc->format('Y') > 1990) {
                    $fecha_ultima_mod_gc_eje = 1;
                }
                if ($inf_contrato->total_inconformidades > 0) {
                    $total_inconformidades_eje = 1;
                }
                if ($inf_contrato->total_inconformidades_r > 0) {
                    $total_inconformidades_r_eje = 1;
                }
                if (strlen($inf_contrato->programa) > 0) {
                    $indice_30 = 1;
                }
            }
            if ($inf_contrato->id_fase == 2) {
                if ($inf_contrato->fecha_aprobacion != null &&
                    $inf_contrato->fecha_aprobacion->format('Y') > 1990) {
                    $fecha_aprobacion_contrato = 1;
                }
            }

            if ($inf_contrato->id_fase == 6) {
                if ($inf_contrato->precio_contrato > 0) {
                    $monto_contrato_sup = 1;
                } else {
                    $monto_contrato_sup = 0;
                }
                if (strlen($inf_contrato->alcance) > 0) {
                    $alcance_sup = 1;
                } else {
                    $alcance_sup = 0;
                }
                if (strlen($inf_contrato->programa) > 0) {
                    $programa_sup = 1;
                } else {
                    $programa_sup = 0;
                }
            }
            if ($inf_contrato->id_fase == 3) {
                if (strlen($inf_contrato->empresa_constructora) > 0) {
                    $nombre_contratista = 1;
                } else {
                    $nombre_contratista = 0;
                }
                if ($inf_contrato->precio_contrato > 0) {
                    $monto_contrato = 1;
                } else {
                    $monto_contrato = 0;
                }
                if (strlen($inf_contrato->alcance) > 0) {
                    $alcance = 1;
                } else {
                    $alcance = 0;
                }
                if (strlen($inf_contrato->programa) > 0) {
                    $programa = 1;
                } else {
                    $programa = 0;
                }
            }


        }

        $i_oferente_d = 0;
        $i_oferente_s = 0;
        $i_oferente_e = 0;
        foreach ($costProyecto->rel_oferente as $oferente) {
            if ($oferente->id_fase == 2) {
                $i_oferente_d++;
            }
            if ($oferente->id_fase == 3) {
                $i_oferente_e++;
            }
            if ($oferente->id_fase == 6) {
                $i_oferente_s++;
            }
        }

        //medición de cumplimiento de standar cost

        $cost_inter['entidad_adquisicion'] = strlen($costProyecto->entidad_adquisicion) > 0 and $costProyecto->entidad_adquisicion != 'Sin información' ? 1 : 0;
        $cost_inter['entidad_admin_contrato'] = strlen($costProyecto->entidad_admin_contrato) > 0 and $costProyecto->entidad_adquisicion != 'Sin información' ? 1 : 0;
        $cost_inter['sector'] = strlen($costProyecto->sector) > 0 and $costProyecto->sector != 'Sin información' ? 1 : 0;
        $cost_inter['sub_sector'] = strlen($costProyecto->sub_sector) > 0 and $costProyecto->sub_sector != 'Sin información' ? 1 : 0;
        $cost_inter['programa_multi_d'] = strlen($costProyecto->programa_multi_d) == 0 and $costProyecto->programa_multi_d != 'Sin información' ? 1 : 0;

        $cuantos_cost = array_sum($cost_inter);
        $de_cuantos_cost = count($cost_inter);

        // medición de cumplimiento de resolución

        //1
        if (isset($dis_plan)) {

            $espe_tecnica = $dis_plan->espe_tecnica > 0 ? 1 : 0;
        } else {
            $espe_tecnica = 0;

        }

        $res_inter['Especificaciones'] = $espe_tecnica;
        $res_inter2[0]['nombre'] = 'Especificaciones';
        $res_inter2[0]['valor'] = $espe_tecnica;
        $res_inter2[0]['link_id'] = $dis_plan->id_proyecto_dis_plan;
        $res_inter2[0]['link_view'] = 'costProyectoDisPlans.show';

        //2
        $res_inter['Propósito'] = (strlen($costProyecto->resumen) > 0 || strlen($costProyecto->descripcion) > 0) and $costProyecto->resumen != 'Sin información' ? 1 : 0;
        //3
        $res_inter['Localización'] = strlen($costProyecto->localizacion) > 0 and $costProyecto->localizacion != 'Sin información' ? 1 : 0;
        //4
        $res_inter['Beneficiarios'] = strlen($costProyecto->beneficiario_total) > 0 and $costProyecto->beneficiario_total != 'Sin información' ? 1 : 0;
        //5 Pendiente de que me digan de donde sacar este dato.
        $res_inter['Estudio de viabilidad'] = 0;
        //6
        $nog = $costProyecto->nog > 0 ? 1 : 0;
        $fuente_financiamiento_supervision = $fuente_supervision;
        $fuente_financiamiento_ejecucion = $fuente_ejecucion;
        $res_inter['Acuerdo de Financiamiento'] = $nog && $fuente_financiamiento_ejecucion && $fuente_financiamiento_supervision ? 1 : 0;
        //7
        if (isset($dis_plan)) {
            $presupuesto_proyecto = $dis_plan->presupuesto_proyecto;
            $costo_estimado_proyecto = $dis_plan->costo_estimado_proyecto > 0 ? 1 : 0;
        } else {
            $presupuesto_proyecto = 0;
            $costo_estimado_proyecto = 0;
        }

        $res_inter['Presupuesto'] = $presupuesto_proyecto && $costo_estimado_proyecto ? 1 : 0;
        //8
        $res_inter['Costo estimado del proyecto'] = $fecha_aprobacion_contrato;
        //9  Aquí se deben realizar alguna evaluaciones
        $res_inter['Proceso de oferta de diseño'] = $i_oferente_d > 0 ? 1 : 0;
        //10
        $res_inter['Nombre del consultor principal del diseño'] = $acta_diseno;
        //11
        $oferentes_diseno = $i_oferente_d > 0 ? 1 : 0;

        if (isset($dis_plan)) {
            $invitacion = strlen($dis_plan->dis_anuncio_invitacion) > 0 ? 1 : 0;
        } else {
            $invitacion = 0;
        }

        $res_inter['Proceso de ofertas de supervisión'] = $oferentes_diseno && $invitacion ? 1 : 0;
        //12
        $resp_sup = strlen($responsable_supervision) > 0 ? 1 : 0;
        $res_inter['Nombre del consultor principal de la supervisión'] = $resp_sup && $acta_adj_sup ? 1 : 0;
        //13
        $bases_consurso_eje = $bases_concurso_eje;
        $fecha_publicacion_eje = $fecha_publicacion_eje;
        $fecha_ultima_mod_gc_eje = $fecha_ultima_mod_gc_eje;
        $res_inter['Proceso de ofertas'] = $bases_consurso_eje && $fecha_publicacion_eje && $fecha_ultima_mod_gc_eje ? 1 : 0;
        //14
        $res_inter['Lista de oferentes'] = $i_oferente_e > 0 ? 1 : 0;
        //15
        $res_inter['Informes de evaluación de las ofertas'] = $acta_adj_eje;
        //16
        $res_inter['Precio del contrato de supervisión'] = $monto_contrato_sup;
        //17 Trabajo  y alcances del proyecto
        $res_inter['Trabajos y alcances de la supervisión'] = $alcance_sup;
        //18
        $res_inter['Programa de tabajo de la supervisión'] = $programa_sup;
        //19
        $res_inter[' Nombre del contratista'] = $nombre_contratista;
        //20
        $res_inter['Precio del contrato'] = $monto_contrato;
        //21 Trabajo  y alcances del proyecto
        $res_inter['Trabajos y alcances de la obra'] = $alcance;
        //22
        $res_inter['Programa de trabajo aprobado al ejecutor'] = $programa;
        //23
        $res_inter['Cambios significativos al precio del contrato, el programa, su alcance y justificación'] = $indice_23;
        //24
        $res_inter['Cambios individuales que afectan el precio y razón de los cambios'] = $indice_24;
        //25
        $res_inter['Cambios individuales que afectan el programa y razón de los cambios'] = $indice_25;
        //26
        $res_inter['Detalles de cualquier recompensa al contratista'] = $indice_26;
        //27
        $res_inter['Precio actualizado del contrato'] = $indice_27;
        //28
        $res_inter['Total de pagos realizados'] = $indice_28;
        //29
        $res_inter['Alcance real de los trabajos'] = $indice_29;
        //30
        $res_inter['Programa actualizado'] = $indice_30;
        //31
        $res_inter['Reportes de evaluaciones y auditoria'] = $indice_31;

        $cuantos_res = array_sum($res_inter);
        $de_cuantos_res = count($res_inter);
        #fotos
        $fotos = cost_proyecto_foto::where('id_proyecto', $id)->get();

        $json_format = $this->ocestandar($id);

        $md5 = md5($json_format);


        return view('cost_proyectos.show', compact('a', 'modalidad_contrato', 'oferentes', 'empresa_contratada'
            , 'monto_contratado', 'lcm', 'lct', 'lcp', 'a', 'mc',
            'responsable_diseno',
            'responsable_estudio',
            'responsable_supervision',
            'fecha_inicio',
            'de_cuantos_cost',
            'cuantos_cost',
            'de_cuantos_res',
            'cuantos_res',
            'cost_inter',
            'res_inter',
            'res_inter2',
            'json_format',
            'you_have_permission',
            'you_have_permission_publicador',
            'fotos','md5'))
            ->with('costProyecto', $costProyecto)
            ->with('costOferentes', $costOferentes)
            ->with('costLiquidacionMontos', $costLiquidacionMontos)
            ->with('costLiquidacionTiempos', $costLiquidacionTiempos)
            ->with('costLiquidacionAlcances', $costLiquidacionAlcances)
            ->with('documentos', $costDocumentos);
    }

    /**
     * Show the form for editing the specified cost_proyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costProyecto = $this->costProyectoRepository->findWithoutFail($id);
        if (empty($costProyecto)) {
            Flash::error('No se encontró el resultado que busca');

            return redirect(route('costProyectos.index'));
        }

        $edit = 1;
        $depto_descripcion = $costProyecto->localizacion_depto_d;
        $muni_descripcion = $costProyecto->localizacion_muni_d;
        $fase = 1;

        $seleccionados_roles = cost_oferente::seleccionados_roles($id, $fase);
        return view('cost_proyectos.edit')->with('costProyecto', $costProyecto)
            ->with('seleccionados_roles', $seleccionados_roles)
            ->with('depto_descripcion', $depto_descripcion)
            ->with('muni_descripcion', $muni_descripcion)
            ->with('edit', $edit);
    }

    /**
     * Update the specified cost_proyecto in storage.
     *
     * @param  int $id
     * @param Updatecost_proyectoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_proyectoRequest $request)
    {
        //dd($request);
        $model = $this->costProyectoRepository->findWithoutFail($id);
        $model->id_proyecto = isset($request->id_proyecto) ? $request->id_proyecto : $model->id_proyecto;
        $model->entidad_adquisicion = isset($request->entidad_adquisicion) ? $request->entidad_adquisicion : $model->entidad_adquisicion;
        $model->id_entidad_adquisicion = isset($request->id_entidad_adquisicion) ? intval($request->id_entidad_adquisicion) : $model->id_entidad_adquisicion;
        $model->entidad_admin_contrato = isset($request->entidad_admin_contrato) ? $request->entidad_admin_contrato : $model->entidad_admin_contrato;
        $model->sector = isset($request->sector) ? $request->sector : $model->sector;
        $model->sub_sector = isset($request->sub_sector) ? $request->sub_sector : $model->sub_sector;
        $model->programa_multi_d = isset($request->programa_multi_d) ? $request->programa_multi_d : $model->programa_multi_d;
        $model->programa_multi = isset($request->programa_multi) ? $request->programa_multi : $model->programa_multi;
        $model->presupuesto_proyecto_multi_d = isset($request->presupuesto_proyecto_multi_d) ? $request->presupuesto_proyecto_multi_d : $model->presupuesto_proyecto_multi_d;
        $model->presupuesto_proyecto_multi = isset($request->presupuesto_proyecto_multi) ? $request->presupuesto_proyecto_multi : $model->presupuesto_proyecto_multi;
        $model->plan_adqui_d = isset($request->plan_adqui_d) ? $request->plan_adqui_d : $model->plan_adqui_d;
        $model->plan_adqui = isset($request->plan_adqui) ? $request->plan_adqui : $model->plan_adqui;
        $model->contacto_entidad = isset($request->contacto_entidad) ? $request->contacto_entidad : $model->contacto_entidad;
        $model->nombre_proyecto = isset($request->nombre_proyecto) ? $request->nombre_proyecto : $model->nombre_proyecto;
        $model->detalle_responsable = isset($request->detalle_responsable) ? $request->detalle_responsable : $model->detalle_responsable;
        $model->fh_ultima_actualizacion = Carbon::now()->format('Y-m-d');
        $model->localizacion = isset($request->localizacion) ? $request->localizacion : $model->localizacion;
        $model->localizacion_muni_d = isset($request->localizacion_muni_d) ? $request->localizacion_muni_d : $model->localizacion_muni_d;
        $model->localizacion_muni = isset($request->localizacion_muni) ? $request->localizacion_muni : $model->localizacion_muni;
        $model->localizacion_depto_d = isset($request->localizacion_depto_d) ? $request->localizacion_depto_d : $model->localizacion_depto_d;
        $model->localizacion_depto = isset($request->localizacion_muni_depto) ? $request->localizacion_muni_depto : $model->localizacion_depto;
        $model->coordenadas = isset($request->coordenadas) ? $request->coordenadas : $model->coordenadas;
        $model->coordenadas_cln = isset($request->coordenadas_cln) ? $request->coordenadas_cln : $model->coordenadas_cln;
        $model->latitud = isset($request->latitud) ? $request->latitud : $model->latitud;
        $model->longitud = isset($request->longitud) ? $request->longitud : $model->longitud;
        $model->snip = isset($request->snip) ? $request->snip : $model->snip;
        $model->snip_detalle = isset($request->snip_detalle) ? $request->snip_detalle : $model->snip_detalle;
        $model->proposito = isset($request->proposito) ? $request->proposito : $model->proposito;
        $model->analisis_alternativas = isset($request->analisis_alternativas) ? $request->analisis_alternativas : $model->analisis_alternativas;
        $model->descripcion = isset($request->descripcion) ? $request->descripcion : $model->descripcion;
        $model->resumen = isset($request->resumen) ? $request->resumen : $model->resumen;
        $model->beneficiario_d = isset($request->beneficiario_d) ? $request->beneficiario_d : $model->beneficiario_d;
        $model->beneficiario_total = isset($request->beneficiario_total) ? $request->beneficiario_total : $model->beneficiario_total;
        $model->f_aprobacion = isset($request->f_aprobacion_submit) && $request->f_aprobacion_submit != '' ? $request->f_aprobacion_submit : $model->f_aprobacion_submit;;
        $model->f_ingreso = $model->f_ingreso;
        $model->foto = isset($request->foto) ? $request->foto : $model->foto;
        $model->pdf = isset($request->pdf) ? $request->pdf : $model->pdf;
        $model->nog_f = isset($request->nog_f_submit) ? $request->nog_f_submit : '';

        if ($model->avance_fisico != '') {
            $model->avance_fisico = isset($request->avance_fisico) ? $request->avance_fisico : $model->avance_fisico;
        } else {
            $model->avance_fisico = null;
        }
        $model->nog = isset($request->nog) ? $request->nog : $model->nog;

        $model->avance_financiero = isset($request->avance_financiero) ? $request->avance_financiero : $model->avance_financiero;
        $model->entidad_admin_contrato = isset($request->entidad_admin_contrato) ? $request->entidad_admin_contrato : $model->entidad_admin_contrato;
        $model->estado_actual_proyecto = isset($request->estado_actual_proyecto) ? $request->estado_actual_proyecto : $model->estado_actual_proyecto;
        $model->operador_portal = isset($request->operador_portal) ? $request->operador_portal : $model->operador_portal;
        $model->f_r_proactiva = isset($request->f_r_proactiva_submit) && $request->f_r_proactiva_submit != '' ? $request->f_r_proactiva_submit : $model->f_r_proactiva_submit;
        $model->f_r_reactiva = isset($request->f_r_reactiva_submit) && $request->f_r_reactiva_submit != '' ? $request->f_r_reactiva_submit : $model->f_r_reactiva_submit;
        $model->f_finalizacion = isset($request->f_finalizacion_submit) && $request->f_finalizacion_submit != '' ? $request->f_finalizacion_submit : $model->f_finalizacion_submit;
        $model->sector_oc = $request->sector_oc > 0 ? $request->sector_oc : 0;
        $model->estado_oc_d = $request->estado_oc > 0 ? cat_item::find($request->estado_oc)->descripcion : 0;
        $model->estado_oc = $request->estado_oc;
        $model->tipo_oc = $request->tipo_oc > 0 ? $request->tipo_oc : 0;
        $model->pais = isset($request->pais) ? $request->pais : '';
        $model->codigo_postal = isset($request->codigo_postal) ? $request->codigo_postal : '';

        $model->avance_financiero = $request->avance_financiero != '' ? $request->avance_financiero : 0;
        //dd($model);
        $model->entidad_admin_contrato = isset($request->entidad_admin_contrato) ? $request->entidad_admin_contrato : $model->entidad_admin_contrato;
        $model->estado_actual_proyecto = isset($request->estado_actual_proyecto) ? $request->estado_actual_proyecto : $model->estado_actual_proyecto;
        //dd($model);
        if (empty($model)) {
            Flash::error('Registro no encontrado');
            return redirect(route('costProyectos.index'));
        }

        $model->save();


        /***se borran los registros actuales y reemplazan por los seleccionados**/
        $antiguo = cost_oferente::where('id_proyecto', $id)->where('id_fase', 1);

        if (count($antiguo) > 0) {

            $antiguo->delete();

            /***se crean nuevos registros actuales y reemplazan por los seleccionados**/
            foreach (Input::get('roles') as $key => $value) {
                $rol_otro = cat_item::find($value);
                /**Buscar rol para usar el campo otro */
                cost_oferente::create([
                    'id_proyecto' => $model->id_proyecto,
                    'rol' => intval($rol_otro->otro),
                    'id_fase' => 1
                ]);
            }

        } else {
            /***se crean nuevos registros actuales y reemplazan por los seleccionados**/
            foreach (Input::get('roles') as $key => $value) {
                $rol_otro = cat_item::find($value);
                /**Buscar rol para usar el campo otro */
                cost_oferente::create([
                    'id_proyecto' => $model->id_proyecto,
                    'rol' => intval($rol_otro->otro),
                    'id_fase' => 1
                ]);
            }
        }

        /**fin*/
        Flash::success('Registro actualizado correctamente');
        return redirect(url('identificacionProyecto', [$id]));
    }

    /**
     * Remove the specified cost_proyecto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costProyecto = $this->costProyectoRepository->findWithoutFail($id);

        if (empty($costProyecto)) {
            Flash::error('Cost Proyecto not found');

            return redirect(route('costProyectos.index'));
        }

        $this->costProyectoRepository->delete($id);

        Flash::success('Registro eliminado correctamente.');

        return redirect(route('costProyectos.index'));
    }

    public function detalle_plano_ident_proyecto($id)
    {
        $costProyecto = cost_proyecto::with("rel_dis_plan")->with("rel_planifica")->with("rel_supervision")->where("id_proyecto", "=", $id)->first();
        if (empty($costProyecto)) {
            Flash::error('Registro no encontrado');
            return redirect(route('costProyectos.index'));
        }

        $json_format = json_encode($costProyecto, JSON_PRETTY_PRINT);

        return view('cost_proyectos.detalle_plano_ident_proyecto', compact('costProyecto', 'json_format'));
    }

    public function publicar_proyecto($id)
    {
        $publicador = cost_proyecto::publicar_proyecto($id);
        return Redirect::back();
    }

    public function no_publicar_proyecto($id)
    {
        $publicador = cost_proyecto::no_publicar_proyecto($id);
        return Redirect::back();
    }

    public function marcar_seguimiento($id)
    {
        $publicador = cost_proyecto::marcar_seguimiento($id);
        return Redirect::back();
    }

    public function no_marcar_seguimiento($id)
    {
        $publicador = cost_proyecto::no_marcar_seguimiento($id);
        return Redirect::back();
    }

    public function convert_json_csv($id)
    {
        $costProyecto = cost_proyecto::find($id);
        $json_format = $this->ocestandar($id);
        $jsonDecoded = json_decode($json_format);
        $proyecto = $jsonDecoded->projects[0];
        $title = cost_proyecto::limpia_string($proyecto->title);

        return view('cost_proyectos.dowload_csv', compact('jsonDecoded','proyecto','title','costProyecto'));
    }

    public function convert_json_excel($id)
    {
        $costProyecto = cost_proyecto::find($id);
        $json_format = $this->ocestandar($id);
        $jsonDecoded = json_decode($json_format);
        $proyecto = $jsonDecoded->projects[0];
        $title = cost_proyecto::limpia_string($proyecto->title);

        return view('cost_proyectos.dowload_excel', compact('jsonDecoded','proyecto','title','costProyecto'));
    }

    public function download_oc4ids(Request $request)
    {
       //dd($request);
        $titulo_busqueda = 'Búsqueda de proyectos OC4IDS';
        $icono ='fa-object-group';
        $icono_color='#1e88e5';
        $tipo_busqueda = 3;

        $filtros = proyecto_view::criterios_default_oc4ids($request);

        if ($request->all() != []) {
            $proyectos = proyecto_view::BuscarIdOc4ids($filtros);
            $proyectos = $proyectos->get();


            /**extraigo los id proyecto*/
            $id_proyectos = [];
            foreach ($proyectos as $k => $y) {
                $id_proyectos[$k] = $y->id_proyecto;
            }
            //dd($filtros);
            /**llamar a la funcion de oc4idsestadar*/
            $union_json_proyects = [];
            $arreglo_json_proyects_limpios = [];
            foreach ($id_proyectos as $key => $item) {
                $union_json_proyects[$key] = $this->ocestandar($item, $filtros->fase_proyecto_despliegue, $filtros->componente_proyecto_despliegue, 1);
            }

            /**evaluar solo los proyectos que traigan contenido de contractingProcess en la posicion 1*/
            foreach ($union_json_proyects as $ky => $itm) {
                foreach ($itm as $k) {
                    if (count($itm[1]) > 0) {
                        $arreglo_json_proyects_limpios[$ky] = $itm;
                    }
                }
            }

            /**unificar en un solo json*/
            $package = $arreglo_json_proyects_limpios[0][0];
            $package['projects'][] = $arreglo_json_proyects_limpios;
            $package = (object)array_filter((array)$package);
            $json_final = json_encode($package, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

        }else{
            $json_final = [];
        }
        /*echo '<pre>';
        print_r($json_final);
        echo '</pre>';*/

        return view('cost_proyectos.busqueda_proyectos_oc4ids.busqueda_proyectos_oc4ids',
        compact('json_final','filtros','union_json_proyects','titulo_busqueda','icono','icono_color','tipo_busqueda'));
    }

    public function busqueda_especifica(Request $request)
    {
        $titulo_busqueda = 'Búsqueda específica de proyectos';
        $icono ='fa-search-plus';
        $icono_color='#827717';
        $tipo_busqueda = 2;
        $you_have_permission = false;
        $you_have_permission_publicador = false;

        $filtros = proyecto_view::criterios_default_oc4ids($request);
        if ($request->all() != []){
            $proyectos = proyecto_view::BuscarIdOc4ids($filtros);
            $proyectos = $proyectos->paginate();


            /**extraigo los id proyecto*/
            $id_proyectos  = [];
            foreach ($proyectos as $k => $y) {
                $id_proyectos[$k] = $y->id_proyecto;
            }

            /**llamar a el proyecto paginado*/

            $costProyectos = cost_proyecto::whereIn('id_proyecto',$id_proyectos)->paginate(10);
        }else{
            $costProyectos = [];
        }


        return view('cost_proyectos.busqueda_proyectos_oc4ids.busqueda_proyectos_oc4ids',
            compact('request','costProyectos','filtros','union_json_proyects','titulo_busqueda','icono','icono_color','tipo_busqueda','you_have_permission_publicador','you_have_permission'));
    }



    function ocds_unificado(Request $request)
    {
        $json_format = $request->json_final;
        return view('cost_proyectos.proyecto', compact('json_format'));
    }




}
