<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_fianzaRequest;
use App\Http\Requests\Updatecost_fianzaRequest;
use App\Repositories\cost_fianzaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_fianzaController extends AppBaseController {

    /** @var  cost_fianzaRepository */
    private $costFianzaRepository;

    public function __construct(cost_fianzaRepository $costFianzaRepo)
    {
        $this->costFianzaRepository = $costFianzaRepo;
    }

    /**
     * Display a listing of the cost_fianza.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        $this->costFianzaRepository->pushCriteria(new RequestCriteria($request));
        $costFianzas = $this->costFianzaRepository->findWhere([ 'id_proyecto' => $request->id_proyecto, 'id_fase' => $request->id_fase ])->all();

        return view('cost_fianzas.index', compact('id_fase', 'id_proyecto'))
            ->with('costFianzas', $costFianzas);
    }

    /**
     * Show the form for creating a new cost_fianza.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        return view('cost_fianzas.create', compact('bnd_edit', 'id_proyecto', 'id_fase'));
    }

    /**
     * Store a newly created cost_fianza in storage.
     *
     * @param Createcost_fianzaRequest $request
     *
     * @return Response
     */
    public function store(Createcost_fianzaRequest $request)
    {
        $input = $request->all();
        $input[ 'fecha_fianza' ] = $request->fecha_fianza_submit;
        unset($input[ 'fecha_fianza_submit' ]);
        $costFianza = $this->costFianzaRepository->create($input);
        Flash::success('La fianza se creó exitosamente');
        return redirect(route('costFianzas.show', [ $costFianza->id ]));
    }

    /**
     * Display the specified cost_fianza.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costFianza = $this->costFianzaRepository->findWithoutFail($id);

        if (empty($costFianza)) {
            Flash::error('Cost Fianza not found');
            return redirect(route('costFianzas.index'));
        }

        return view('cost_fianzas.show')->with('costFianza', $costFianza);
    }

    /**
     * Show the form for editing the specified cost_fianza.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costFianza = $this->costFianzaRepository->findWithoutFail($id);
        $bnd_edit = true;
        if (empty($costFianza)) {
            Flash::error('Cost Fianza not found');
            return redirect(route('costFianzas.index'));
        }
        return view('cost_fianzas.edit', compact('bnd_edit'))->with('costFianza', $costFianza);
    }

    /**
     * Update the specified cost_fianza in storage.
     *
     * @param  int $id
     * @param Updatecost_fianzaRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_fianzaRequest $request)
    {
        $costFianza = $this->costFianzaRepository->findWithoutFail($id);
        $input = $request->all();
        $input[ 'fecha_fianza' ] = $request->fecha_fianza_submit;
        unset($input[ 'fecha_fianza_submit' ]);
        if (empty($costFianza)) {
            Flash::error('Cost Fianza not found');
            return redirect(route('costFianzas.index'));
        }
        $costFianza = $this->costFianzaRepository->update($input, $id);

        Flash::success('El acta ha sido actualizada exitosamente');
        return redirect(route('costFianzas.show', [ $costFianza->id ]));
    }

    /**
     * Remove the specified cost_fianza from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costFianza = $this->costFianzaRepository->findWithoutFail($id);

        if (empty($costFianza)) {
            Flash::error('Cost Fianza not found');

            return redirect(route('costFianzas.index'));
        }
        $id_proyecto =$costFianza->id_proyecto;
        $id_fase = $costFianza->id_fase;
        $this->costFianzaRepository->delete($id);
        Flash::success('La fianza fue borrada exitosamente');
        return redirect(route('costFianzas.index', ['id_proyecto'=>$id_proyecto,'id_fase'=>$id_fase]));
    }
}
