<?php

namespace App\Http\Controllers;

use App\Models\agenda;
use App\Models\asesoria;
use App\Models\asesoria_proceso;
use App\Models\atencion_inicial;
use App\Models\cat_item;
use App\Models\criterio_fijo;
use App\Models\proceso;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class reportesController extends Controller
{
    /**
     * @var int variable gloval que sirve para inicializar origen de reporte
     */
    var $anio_inicio = 2012;

    /**
     * @var int variable gloval que sirve para inicializar año actual
     */
    var $anio_curso;

    /**
     * @var int variable gloval que sirve para inicializar mes actual
     */
    var $mes_curso;

    /**
     * @var array arreglo contiene los meses del año y sirve para reporteria en tablas
     */
    var $mes = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

    /**
     * @var array arreglo declarado para reporterias de asesoria especificamente para los reportes de asesoria-estatus y asesoria-avance
     */
    var $estatus_proceso = ["EN CURSO","EN TRAMITE","SE ARCHIVO"];

    /**
     * @var array arreglo que se utiliza para reporteria de familia especificamente en proceso de familia por inicio de año
     */
    var $estatus_proceso_inicio = ["EN TRAMITE","ORDENADO PENDIENTE DE STATUS","REVISAR"];

    /**
     * @var array arreglo que se utiliza para reporteria de familia especificamente en proceso de familia por inicio de año
     */
    var $estatus_proceso_inicio_penal;

    /**
     * @var array  arreglo para obtener lo tipo de proceso solicitado en el reporte
     */
    var $id_tipo_proceso_persona;

    /**
     * @var array  arreglo para obtener lo tipo de proceso solicitado en el reporte penal
     */
    var $id_tipo_proceso_persona_penal;

    /**
     * @var array arreglo que se transformara en json para las vistas que utilizan tablas y se puedan hacer los exports respectivos
     */
    var $data_table;

    var $consulta_catitem;

    /**
     * Constructor para setear varables globales
     *
     * reportesController constructor.
     */
    public function __construct()
    {
        // id items que estan activados 31
        $id_item_cat_31 = cat_item::select("id_item")->where("id_cat",31)->where("otro","<>",1)->get()->pluck("id_item");

        // id items que estan activados 30
        $id_item_cat_30 = cat_item::select("id_item")->where("id_cat",30)->where("otro","<>",1)->get()->pluck("id_item");

        //arreglo para obtener lo tipo de proceso solicitado en el reporte
        $this->id_tipo_proceso_persona = $id_item_cat_31;

        //arreglo para obtener lo tipo de proceso solicitado en el reporte penal
        $this->id_tipo_proceso_persona_penal = $id_item_cat_30;

        // A REVISAR = 6749, ACTUALIZAR = 6733, EN TRAMITE = 6390
        $this->estatus_proceso_inicio_penal = [6749,6733,6390];

        // se asigna valor del año en curso con valor entero
        $this->anio_curso = (int)date("Y");

        // se asigna valor al mes actual
        $this->mes_curso = date("m");

        // arreglo que contiene la configuracion del datatable variable que podra modificarse en la vista
        $this->data_table = [
            "searching"=>   false,
            "paging"=>   true,
            "ordering"=> false,
            "info"=>     false,
            "dom"=> 'Bfrtip',
            "buttons"=> [
                ["extend"=>"copy","className"=>"btn bg-gray","footer"=> true],
                ["extend"=>"excel","className"=>"btn bg-olive","footer"=> true],
                ["extend"=>"pdf","className"=>"btn bg-red","footer"=> true],
                ["extend"=>"print","className"=>"btn bg-yellow","footer"=> true]
            ],
            "language"=> [
                "url" => url('/js/reportes/data_tables/language/Spanish.json'),
                "buttons"=> [
                    "copyTitle" =>'Copiado',
                    "copyKeys"=> 'Preciona <i>ctrl</i> ou <i>\u2318</i> + <i>C</i> para copiar los datos de la tabla a su portapapeles. <br><br>Para cancelar, haga clic en este mensaje o presione Esc.',
                    "copySuccess" => ["_"=> '%d Filas Copiadas  ',1=>'1 Fila Copiada'],
                    "copy" =>'<i class="fa fa-copy"></i> Copiar',
                    "excel"=>'<i class="fa fa-file-excel-o"></i> Excel',
                    "pdf"=>'<i class="fa fa-file-pdf-o"></i> PDF',
                    "print"=>'<i class="fa fa-print"></i> Imprimir',
                ]
            ]

        ];

        // consulta los estados de cat_item que se usaran en el controlador
        $this->consulta_catitem = cat_item::select("descripcion","id_item","id_cat")->whereIn("id_cat",[29,30,31,37])->get();
    }

    public function repoMingob(Request $request)
    {
        $request->id_caimu = Auth::user()->id_caimu;
        $filtros= atencion_inicial::criterios_repomingob($request);
        $url_submit = 'repoMingob';
        $url_reset = 'repoMingob';

        $at = new atencion_inicial();
        $atencionInicials = $at->reporte_mingob($filtros);
        $muestro = 2;// la uso para mostrar o no en el control de fechas las opciones necesarias segun el reporte -- 1 muestra y 2 no
        $collapse = 1;
        return view('reportes.repo_mingob', compact('fecha','fecha_transcurrida','url_submit','url_reset','atencionInicials','filtros','muestro','collapse'));
    }

    /**
     * Metodo que se utiliza para la reporteria general de asesoria
     * retorna la vista repo_asesoria con todas las variables declaradas en el metodo
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function repoAsesoria(Request $request)
    {
        // se inicializa el año actual
        $anio_curso = $this->anio_curso;

        // se inicializa el mes actual
        $mes_curso = $this->mes_curso;

        // se declara la variable mes con valor declarado en la variable gloval que contiene los meses del año en formato arreglo
        $mes = $this->mes;

        // se declara variable anio inicio que contiene valor numerico declarado en la variable gloval anio_inicio
        $anio_inicio =$this->anio_inicio;

        // se obtiene el año seleccionado por el usuario en la vista del formulario
        $anio = trim($request->fh_entrevista_legal);

        // se declara la variable array_rango_anio de tipo arreglo y comienza con el valor declarado en la variable anio_inicio
        $array_rango_anio[]=$this->anio_inicio;

        // se hacen las validaciones para llenar el arreglo array_rango_anio y agregar anio fin asi mismo agregar arreglo de meses
        if (is_numeric($anio) && strlen($anio)>0 && $anio_curso != $anio){ // tasmbien se valida que el anio elegido no sea el mismo en curso ejemplo 2018 para que no tome todos los meses

            // se agrega a la variable $array_rango_anio el rango de anio final si entra aqui es porque el usuario elijio un año
            $array_rango_anio[]=(int)$anio;

            // como el anio es distino al que esta en curso se genera un arreglo con un rango de 12 que conforman los 12 meses del año
            $array_meses = range(1,12);

            // se genera un arreglo de años con la variable anio inicio y la variable anio
            $rango_anios = range($anio_inicio,$anio);
        }else{ // si entra al else es porque el usuario eligio el anio en curso o es la pantalla inicial
            // como es el anio en curso se agrega en el array_rango_anio (declarado anteriormente) el anio fin
            $array_rango_anio[]=$anio_curso;

            // y se llena un arreglo solo con los meses que han pasado del anio en curso
            $array_meses = range(1,$mes_curso);
            // y se genera un arreglo con un rango de años de las variables $anio_inicio y $anio_curso
            $rango_anios = range($anio_inicio,$anio_curso);
        }

        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());

        // se declara variable id_estado proceso para su uso en la tabla cat_item
        $id_estado_proceso = collect($this->estatus_proceso);

        // variable que se hace uso en la vista _por_anio
        $consulta_asesoria = asesoria::atenciones_anio($array_rango_anio,$array_meses);

        // variable que se hace uso en la vista _por_abogada
        $consulta_asesoria_abogada = asesoria::atencion_abogada($array_rango_anio[1],$array_meses);

        // variable que se hace uso en la vista _por_proceso
        $atencion_inicial = atencion_inicial::getAsesriaProceso($array_rango_anio[1],$array_meses);

        // se realiza consulta a la tabla criterio fijo de todos para reutilizar variable en otras validacines necesarias
        $criterio_fijo = criterio_fijo::select("descripcion","id_opcion","id_criterio_fijo")->get();

        // se utiliza en la vista _por_proceso para construir la tabla de reportes
        $criterio_fijo_proceso = $criterio_fijo->where("id_criterio_fijo",4)->pluck("descripcion","id_opcion")->toArray();

        // se consulta la tabla cat_item y no se hace join con las tablas que se deberia por optimizacion de codigo con el id_cat 29 que hace referencia al Estado
        $consulta_catitem = $this->consulta_catitem;

        // se prepara el arreglo que servira para traer la informacion de la tabla proceso con los estados que se piden y declararon servira para la variable $id_estado_proceso_consultado
        $estatus_proceos = $consulta_catitem->whereIn('descripcion', $this->estatus_proceso)->pluck("descripcion","id_item");

        // se reutiliza la variable $consulta_catitem y se obteine solo lo que se necesita para su uso en la variable $id_estado_proceso_consultado_tc
        $id_estado_solo_tc = $consulta_catitem->whereIn('descripcion', $id_estado_proceso->forget(1))->pluck("descripcion","id_item");

        // se construye el arreglo que se necesita para obtener la informacion con los estados que se piden de la tabla proceso
        $id_estado_proceso_consultado =$estatus_proceos->keys();

        // se construye el arreglo que se necesita para obtener la informacion con los estados que se piden de la tabla asesoria_proceso
        $id_estado_proceso_consultado_tc =$id_estado_solo_tc->keys();

        // variable que se hace uso en la vista _por_estatus_demanda
        $consulta_proceso = proceso::atencion_inicial_count($array_rango_anio[1],$id_estado_proceso_consultado)->get();

        // variable que se hace uso en la vista _por_proyecto_mes
        $proyectos = asesoria::atenciones_clase($array_rango_anio[1],$array_meses);

        // se utiliza consulta para abastecer informacion den las variables $obj_cruso y $obj_archivo
        $asesoria_proceso = asesoria_proceso::asesoria_at($array_rango_anio[1],$array_meses,$id_estado_proceso_consultado_tc)->get();

        // variable que se hace uso en la vista _por_proyecto_mes. EN CURSO = 6379
        $obj_cruso = $asesoria_proceso->where("id_estado",6379);

        // variable que se hace uso en la vista _por_demanda_archivo. SE ARCHIVO = 6384
        $obj_archivo = $asesoria_proceso->where("id_estado",6384);

        // variable que se hace uso en la _por_demanda_iniciada
        $asesoria_proceso_iniciado = asesoria_proceso::asesoria_at($array_rango_anio[1],$array_meses,2,'procesos_iniciados')->get();

        return view('reportes.asesoria.repo_asesoria', get_defined_vars());
    }

    /**
     * Metodo que se utiliza para la reporteria general de Familia
     * retorna la vista repo_asesoria con todas las variables declaradas en el metodo
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function repoFamilia(Request $request)
    {
        // tipo de demanda famila = 2
        $id_tipo_demanda_func = 2;

        // se inicializa el año actual
        $anio_curso = $this->anio_curso;

        // se inicializa el mes actual
        $mes_curso = $this->mes_curso;

        // se declara la variable mes con valor declarado en la variable gloval que contiene los meses del año en formato arreglo
        $mes = $this->mes;

        // se declara variable anio inicio que contiene valor numerico declarado en la variable gloval anio_inicio
        $anio_inicio =$this->anio_inicio;

        // se obtiene el año seleccionado por el usuario en la vista del formulario
        $anio = trim($request->fh_entrevista_legal);

        // se declara la variable array_rango_anio de tipo arreglo y comienza con el valor declarado en la variable anio_inicio
        $array_rango_anio[]=$this->anio_inicio;

        // se hacen las validaciones para llenar el arreglo array_rango_anio y agregar anio fin asi mismo agregar arreglo de meses
        if (is_numeric($anio) && strlen($anio)>0 && $anio_curso != $anio){ // tasmbien se valida que el anio elegido no sea el mismo en curso ejemplo 2018 para que no tome todos los meses

            // se agrega a la variable $array_rango_anio el rango de anio final si entra aqui es porque el usuario elijio un año
            $array_rango_anio[]=(int)$anio;

            // como el anio es distino al que esta en curso se genera un arreglo con un rango de 12 que conforman los 12 meses del año
            $array_meses = range(1,12);

            // se genera un arreglo de años con la variable anio inicio y la variable anio
            $rango_anios = range($anio_inicio,$anio);
        }else{ // si entra al else es porque el usuario eligio el anio en curso o es la pantalla inicial
            // como es el anio en curso se agrega en el array_rango_anio (declarado anteriormente) el anio fin
            $array_rango_anio[]=$anio_curso;

            // y se llena un arreglo solo con los meses que han pasado del anio en curso
            $array_meses = range(1,$mes_curso);
            // y se genera un arreglo con un rango de años de las variables $anio_inicio y $anio_curso
            $rango_anios = range($anio_inicio,$anio_curso);
        }

        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());

        // id_tipo_proceos para la tabla proceso
        $id_tipo_proceso_persona = $this->id_tipo_proceso_persona;
        $estatus_proceso_inicio =  collect($this->estatus_proceso_inicio);
        $consulta_catitem = $this->consulta_catitem;
        $id_estados_proceso_fam_anio = $consulta_catitem->where("id_cat",29)->whereIn("descripcion",$estatus_proceso_inicio)->pluck("id_item");
        $id_estados_proceso_fam_anio_des = $consulta_catitem->where("id_cat",29)->whereIn("descripcion",$estatus_proceso_inicio)->pluck("descripcion","id_item");

        $id_estados_proceso_fam_tipo_proceso = $consulta_catitem->where("id_cat",29)->whereIn("descripcion",$estatus_proceso_inicio->first())->pluck("descripcion","id_item");

        $tipo_proceso_fam_desglosado = $consulta_catitem->where("id_cat",31)->pluck("id_item");
        $tipo_proceso_fam_desglosadodes = $consulta_catitem->where("id_cat",31)->pluck("descripcion","id_item");

        $tipo_proceso_fam_juzgado = $consulta_catitem->where("id_cat",37)->pluck("id_item");
        $tipo_proceso_fam_juzgadodes = $consulta_catitem->where("id_cat",37)->pluck("descripcion","id_item");

        $consulta_proceso = proceso::atencion_inicial_count_anual($rango_anios,$id_estados_proceso_fam_anio)->get();
        $consulta_proceso_desglosado = proceso::atencion_inicial_count_anual_desglosado($rango_anios,$tipo_proceso_fam_desglosado,$id_estados_proceso_fam_tipo_proceso->keys())->get()->each(function ($v) use ($tipo_proceso_fam_desglosadodes){
            $v = $v->nombre_tipo_proceso =isset($tipo_proceso_fam_desglosadodes[$v->id_tipo_proceso]) ? $tipo_proceso_fam_desglosadodes[$v->id_tipo_proceso] : '--';
            return $v;
        });

        $consulta_proceso_juzgado = proceso::atencion_inicial_count_juzgado([$array_rango_anio[1]],$tipo_proceso_fam_juzgado)->get();
        $consulta_proceso_tramite = $consulta_proceso_desglosado->where("anio",$array_rango_anio[1])->where("id_estado",$id_estados_proceso_fam_tipo_proceso->keys()->first())->forget("anio");
        // seobtiene la data de las personas y proceos
        $consulta_tipo_proceso_persona= proceso::proceso_persona($array_rango_anio[1],$id_tipo_proceso_persona,[1,2])->get();

        // consulta_var_cat_item
        $desc_ripo_proceso_per = $consulta_catitem->where("id_cat",31)->whereIn("id_item",$id_tipo_proceso_persona)->pluck("descripcion","id_item");
        return view('reportes.familia.repo_familia', get_defined_vars());
    }

    public function repoPenal(Request $request)
    {
        // tipo de demnda = 1
        $id_tipo_demanda_func = 1;

        // se inicializa el año actual
        $anio_curso = $this->anio_curso;

        // se inicializa el mes actual
        $mes_curso = $this->mes_curso;

        // se declara la variable mes con valor declarado en la variable gloval que contiene los meses del año en formato arreglo
        $mes = $this->mes;

        // se declara variable anio inicio que contiene valor numerico declarado en la variable gloval anio_inicio
        $anio_inicio =$this->anio_inicio;

        // se obtiene el año seleccionado por el usuario en la vista del formulario
        $anio = trim($request->fh_entrevista_legal);

        // se declara la variable array_rango_anio de tipo arreglo y comienza con el valor declarado en la variable anio_inicio
        $array_rango_anio[]=$this->anio_inicio;

        // se hacen las validaciones para llenar el arreglo array_rango_anio y agregar anio fin asi mismo agregar arreglo de meses
        if (is_numeric($anio) && strlen($anio)>0 && $anio_curso != $anio){ // tasmbien se valida que el anio elegido no sea el mismo en curso ejemplo 2018 para que no tome todos los meses

            // se agrega a la variable $array_rango_anio el rango de anio final si entra aqui es porque el usuario elijio un año
            $array_rango_anio[]=(int)$anio;

            // como el anio es distino al que esta en curso se genera un arreglo con un rango de 12 que conforman los 12 meses del año
            $array_meses = range(1,12);

            // se genera un arreglo de años con la variable anio inicio y la variable anio
            $rango_anios = range($anio_inicio,$anio);
        }else{ // si entra al else es porque el usuario eligio el anio en curso o es la pantalla inicial
            // como es el anio en curso se agrega en el array_rango_anio (declarado anteriormente) el anio fin
            $array_rango_anio[]=$anio_curso;

            // y se llena un arreglo solo con los meses que han pasado del anio en curso
            $array_meses = range(1,$mes_curso);
            // y se genera un arreglo con un rango de años de las variables $anio_inicio y $anio_curso
            $rango_anios = range($anio_inicio,$anio_curso);
        }

        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());

        // id_tipo_proceos para la tabla proceso
        $consulta_catitem = $this->consulta_catitem;

        $estado_penal_anio_id = $consulta_catitem->whereIn("id_item",$this->estatus_proceso_inicio_penal)->pluck("id_item");
        $estado_penal_anio_desc = $consulta_catitem->whereIn("id_item",$this->estatus_proceso_inicio_penal)->pluck("descripcion","id_item");
        $en_tramite ="EN TRAMITE";
        $consulta_proceso = proceso::proceso_penal_anios($rango_anios,$estado_penal_anio_id,$id_tipo_demanda_func)->get();

        $consulta_proceso_tipo_tramite = proceso::penal_tipo_proceso_tramite($array_rango_anio[1],$estado_penal_anio_desc->search($en_tramite),$id_tipo_demanda_func)->get();

        $consulta_penal_juzgado = proceso::penal_juzgado($array_rango_anio[1])->get();

        $id_tipo_proceso_persona_penal = $this->id_tipo_proceso_persona_penal;
        $consulta_tipo_proceso_persona= proceso::proceso_persona($array_rango_anio[1],$id_tipo_proceso_persona_penal,[$id_tipo_demanda_func])->get();

        // consulta_var_cat_item
        $desc_ripo_proceso_per = $consulta_catitem->where("id_cat",30)->whereIn("id_item",$id_tipo_proceso_persona_penal)->pluck("descripcion","id_item");


        return view('reportes.penal.repo_penal', get_defined_vars());
    }

    public function repoPenalArchivado(Request $request)
    {
        // tipo de demnda = 1
        $id_tipo_demanda_func = 1;

        // se inicializa el año actual
        $anio_curso = $this->anio_curso;

        // se inicializa el mes actual
        $mes_curso = $this->mes_curso;

        // se declara la variable mes con valor declarado en la variable gloval que contiene los meses del año en formato arreglo
        $mes = $this->mes;

        // se declara variable anio inicio que contiene valor numerico declarado en la variable gloval anio_inicio
        $anio_inicio =$this->anio_inicio;

        // se obtiene el año seleccionado por el usuario en la vista del formulario
        $anio = trim($request->fh_entrevista_legal);

        // se declara la variable array_rango_anio de tipo arreglo y comienza con el valor declarado en la variable anio_inicio
        $array_rango_anio[]=$this->anio_inicio;

        // se hacen las validaciones para llenar el arreglo array_rango_anio y agregar anio fin asi mismo agregar arreglo de meses
        if (is_numeric($anio) && strlen($anio)>0 && $anio_curso != $anio){ // tasmbien se valida que el anio elegido no sea el mismo en curso ejemplo 2018 para que no tome todos los meses

            // se agrega a la variable $array_rango_anio el rango de anio final si entra aqui es porque el usuario elijio un año
            $array_rango_anio[]=(int)$anio;

            // como el anio es distino al que esta en curso se genera un arreglo con un rango de 12 que conforman los 12 meses del año
            $array_meses = range(1,12);

            // se genera un arreglo de años con la variable anio inicio y la variable anio
            $rango_anios = range($anio_inicio,$anio);
        }else{ // si entra al else es porque el usuario eligio el anio en curso o es la pantalla inicial
            // como es el anio en curso se agrega en el array_rango_anio (declarado anteriormente) el anio fin
            $array_rango_anio[]=$anio_curso;

            // y se llena un arreglo solo con los meses que han pasado del anio en curso
            $array_meses = range(1,$mes_curso);
            // y se genera un arreglo con un rango de años de las variables $anio_inicio y $anio_curso
            $rango_anios = range($anio_inicio,$anio_curso);
        }

        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());

        // id_tipo_proceos para la tabla proceso
        $consulta_catitem = $this->consulta_catitem;

        // SE ARCHIVO = 6384
        $id_estado = [6384];

        $consulta_motivo_archivo= proceso::motivo_archivo($array_rango_anio[1],$id_estado,$id_tipo_demanda_func)->get();

        $consulta_archivo_anio= proceso::proceso_penal_anios($rango_anios,$id_estado,$id_tipo_demanda_func)->get();

        return view('reportes.penal_archivo.repo_penal_archivo', get_defined_vars());

    }

    public function repoFamiliaArchivado(Request $request)
    {
        // se inicializa el año actual
        $anio_curso = $this->anio_curso;

        // se inicializa el mes actual
        $mes_curso = $this->mes_curso;

        // se declara la variable mes con valor declarado en la variable gloval que contiene los meses del año en formato arreglo
        $mes = $this->mes;

        // se declara variable anio inicio que contiene valor numerico declarado en la variable gloval anio_inicio
        $anio_inicio =$this->anio_inicio;

        // se obtiene el año seleccionado por el usuario en la vista del formulario
        $anio = trim($request->fh_entrevista_legal);

        // se declara la variable array_rango_anio de tipo arreglo y comienza con el valor declarado en la variable anio_inicio
        $array_rango_anio[]=$this->anio_inicio;

        // se hacen las validaciones para llenar el arreglo array_rango_anio y agregar anio fin asi mismo agregar arreglo de meses
        if (is_numeric($anio) && strlen($anio)>0 && $anio_curso != $anio){ // tasmbien se valida que el anio elegido no sea el mismo en curso ejemplo 2018 para que no tome todos los meses

            // se agrega a la variable $array_rango_anio el rango de anio final si entra aqui es porque el usuario elijio un año
            $array_rango_anio[]=(int)$anio;

            // como el anio es distino al que esta en curso se genera un arreglo con un rango de 12 que conforman los 12 meses del año
            $array_meses = range(1,12);

            // se genera un arreglo de años con la variable anio inicio y la variable anio
            $rango_anios = range($anio_inicio,$anio);
        }else{ // si entra al else es porque el usuario eligio el anio en curso o es la pantalla inicial
            // como es el anio en curso se agrega en el array_rango_anio (declarado anteriormente) el anio fin
            $array_rango_anio[]=$anio_curso;

            // y se llena un arreglo solo con los meses que han pasado del anio en curso
            $array_meses = range(1,$mes_curso);
            // y se genera un arreglo con un rango de años de las variables $anio_inicio y $anio_curso
            $rango_anios = range($anio_inicio,$anio_curso);
        }

        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());

        // id_tipo_proceos para la tabla proceso
        $consulta_catitem = $this->consulta_catitem;

        $id_estado = [6384];
        $id_tipo_demanda = 1;
        $consulta_motivo_archivo= proceso::motivo_archivo($array_rango_anio[1],$id_estado,$id_tipo_demanda)->get();

        $consulta_archivo_anio= proceso::proceso_penal_anios($rango_anios,$id_estado,$id_tipo_demanda)->get();

        return view('reportes.familia_archivo.repo_familia_archivo', get_defined_vars());

    }

    public function analisisLegal($data = []){
        if (count($data)==0)
        {
            $data[0]["icon"] ='<i class="fa fa-flash fa-3x" aria-hidden="true"></i>';
            $data[0]["title"] ='Asesor&iacute;a';
            $data[0]["url"] ='reportes/repoAsesoria';

            $data[1]["icon"] ='<i class="fa fa-users fa-3x" aria-hidden="true"></i>';
            $data[1]["title"] ='Familia';
            $data[1]["url"] ='reportes/repoFamilia';

            $data[2]["icon"] ='<i class="fa fa-balance-scale fa-3x" aria-hidden="true"></i>';
            $data[2]["title"] ='Penal';
            $data[2]["url"] ='reportes/repoPenal';

            $data[3]["icon"] ='<i class="fa fa-balance-scale fa-3x" aria-hidden="true"></i>';
            $data[3]["title"] ='Penal Archivado';
            $data[3]["url"] ='reportes/repoPenalArchivado';

            $data[4]["icon"] ='<i class="fa fa-folder-o fa-3x" aria-hidden="true"></i>';
            $data[4]["title"] ='Familia Archivado';
            $data[4]["url"] ='reportes/repoFamiliaArchivado';
        }

        return view('reportes.panel',get_defined_vars());
    }


    public function repoAgendaLegal(Request $request)
    {
        $anio_inicio = $this->anio_inicio;
        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());
        $inpus = collect($request->input());
        if ($inpus->count()>0){
            $rango_fechas = [$request->input("fh_inicio"),$request->input("fh_final")];
        }else{
            $fecha_inicio  = "01-".date("Y");
            $fecha_fin  = date("m-Y");
            $rango_fechas = [$fecha_inicio,$fecha_fin];
        }

        $proceso_mes = proceso::agenda_proceso($rango_fechas)->get();
        return view('reportes.agenda_legal.todos', get_defined_vars());

    }

    public function repoAgendaMes(Request $request)
    {
        $fecha_inicial = Carbon::now()->subMonth(1)->format("Y-m-d");
        $fecha_final = Carbon::now()->format("Y-m-d");
        $fecha_rango = [$fecha_inicial,$fecha_final];
        $filtro_normal = true;

        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());
        $inpus = collect($request->input());
        if ($inpus->count()>0){
            $fecha_rango[0] =$inpus->get("fh_inicial_submit");
            $fecha_rango[1] =$inpus->get("fh_fin_submit");
        }
        $filtros["url"] = url("/reportes/repoAgendaMes");
        $filtros["fecha_del"] = $fecha_rango[0];
        $filtros["fecha_al"] = $fecha_rango[1];
        $filtros = (object)$filtros;

        $consulta_agenda = agenda::accion_rango_fechas($fecha_rango)->get();
        return view('reportes.agenda_mes.todos', get_defined_vars());

    }

    public function indexAccionAA(Request $request)
    {
        $fecha_inicial = Carbon::now()->subMonth(1)->format("Y-m-d");
        $fecha_final = Carbon::now()->format("Y-m-d");
        $fecha_rango = [$fecha_inicial,$fecha_final];
        $filtro_normal = true;

        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());
        $inpus = collect($request->input());
        if ($inpus->count()>0){
            $fecha_rango[0] =$inpus->get("fh_inicial_submit");
            $fecha_rango[1] =$inpus->get("fh_fin_submit");
        }
        $filtros["url"] = url("/reportes/indexAccionAA");
        $filtros["fecha_del"] = $fecha_rango[0];
        $filtros["fecha_al"] = $fecha_rango[1];
        $filtros = (object)$filtros;

        $consulta_agenda_abogada = agenda::accion_responsable($fecha_rango,"id_abogada")->get();
        $consulta_agenda_q_agenda = agenda::accion_responsable($fecha_rango,"id_quien_agenda")->get();
        return view('reportes.agenda_responsable.todos', get_defined_vars());

    }

    public function indexCantAccionesResp(Request $request)
    {
        $fecha_inicial = Carbon::now()->subMonth(1)->format("Y-m-d");
        $fecha_final = Carbon::now()->format("Y-m-d");
        $fecha_rango = [$fecha_inicial,$fecha_final];
        $filtro_normal = true;

        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());
        $inpus = collect($request->input());
        if ($inpus->count()>0){
            $fecha_rango[0] =$inpus->get("fh_inicial_submit");
            $fecha_rango[1] =$inpus->get("fh_fin_submit");
        }
        $filtros["url"] = url("/reportes/indexAccionAA");
        $filtros["fecha_del"] = $fecha_rango[0];
        $filtros["fecha_al"] = $fecha_rango[1];
        $filtros = (object)$filtros;

        $consulta_agenda_abogada_fecha = agenda::fecha_responsable($fecha_rango,"id_abogada")->get();
        $consulta_agenda_q_agenda_fecha = agenda::fecha_responsable($fecha_rango,"id_quien_agenda")->get();
        return view('reportes.agenda_responsable_fecha.todos', get_defined_vars());
    }

    public function indexProcesoMujer(Request $request)
    {
        $fecha_inicial = Carbon::now()->subMonth(1)->format("Y-m-d");
        $fecha_final = Carbon::now()->format("Y-m-d");
        $fecha_rango = [$fecha_inicial,$fecha_final];
        $filtro_normal = true;

        // se declara variable $json_data_table y se asigna valor de la variabel global data_table transformado en json pasa su uso en las vistas de reporteria
        $json_data_table = base64_encode(collect($this->data_table)->toJson());
        $inpus = collect($request->input());
        if ($inpus->count()>0){
            $fecha_rango[0] =$inpus->get("fh_inicial_submit");
            $fecha_rango[1] =$inpus->get("fh_fin_submit");
        }
        $filtros["url"] = url("/reportes/indexProcesoMujer");
        $filtros["fecha_del"] = $fecha_rango[0];
        $filtros["fecha_al"] = $fecha_rango[1];
        $filtros = (object)$filtros;

        $consulta_proceso_mujer = proceso::proceso_persona_fecha($fecha_rango)->get();

        return view('reportes.proceso_mujer_fecha.todos', get_defined_vars());
    }

    public function indexAgendaLegal()
    {
        $data[0]["icon"] ='<i class="fa fa-flash fa-3x" aria-hidden="true"></i>';
        $data[0]["title"] ='Acciones agenda Legal';
        $data[0]["url"] ='reportes/repoAgendaLegal';

        $data[1]["icon"] ='<i class="fa fa-book fa-3x" aria-hidden="true"></i>';
        $data[1]["title"] ='Accion agendada';
        $data[1]["url"] ='reportes/repoAgendaMes';

        $data[2]["icon"] ='<i class="fa fa-users fa-3x" aria-hidden="true"></i>';
        $data[2]["title"] ='Acción agenda responsables';
        $data[2]["url"] ='reportes/indexAccionAA';

        $data[3]["icon"] ='<i class="fa fa-align-justify fa-3x" aria-hidden="true"></i>';
        $data[3]["title"] ='Acción agenda responsables (cantidad)';
        $data[3]["url"] ='reportes/indexCantAccionesResp';

        $data[4]["icon"] ='<i class="fa fa-female fa-3x" aria-hidden="true"></i>';
        $data[4]["title"] ='Procesos por mujer';
        $data[4]["url"] ='reportes/indexProcesoMujer';

        return $this->analisisLegal($data);
    }

    public function repo_mensual(Request $request){
        $request->id_caimu = Auth::user()->id_caimu;
        $filtros= atencion_inicial::criterios_default($request);

        $at = new atencion_inicial();
        $total_atenciones = $at->total_atenciones($filtros);
        $total_mujeres_seguimiento = $at->total_mujeres_seguimiento($filtros);
        $rango_edades = $at->rango_edades($filtros);
        $rango_hijos = $at->rango_hijos($filtros);
        $estado_civil = $at->estado_civil($filtros);
        $escolaridad = $at->escolaridad($filtros);
        $trabajo_mujer = $at->trabajo_mujer($filtros);
        $area_residencial = $at->area_residencial($filtros);
        $procedencia = $at->procedencia($filtros);
        $muni_domicilio = $at->muni_domicilio($filtros);
        $depto_domicilio = $at->depto_domicilio($filtros);
        $nacionalidad = $at->nacionalidad($filtros);
        $etnia = $at->etnia($filtros);
        $motivo_visita = $at->motivo_visita($filtros);
        $tipo_violencia = $at->tipo_violencia($filtros);
        $quien_agrede = $at->quien_agrede($filtros);
        $quien_refiere= $at->quien_refiere($filtros);
        $area_dentro_caimu= $at->area_dentro_caimu($filtros);
        $referencia_externa= $at->referencia_externa($filtros);
        $json_data_table = base64_encode(collect($this->data_table)->toJson());
        $sede = $at->sede_caimu_login();

        return view('atencion_inicials.repo_mensual_at_caimus')
            ->with('filtros', $filtros)
            ->with('total_atenciones', $total_atenciones)
            ->with('total_mujeres_seguimiento', $total_mujeres_seguimiento)
            ->with('rango_edades', $rango_edades)
            ->with('rango_hijos', $rango_hijos)
            ->with('estado_civil', $estado_civil)
            ->with('escolaridad', $escolaridad)
            ->with('trabajo_mujer', $trabajo_mujer)
            ->with('area_residencial', $area_residencial)
            ->with('procedencia', $procedencia)
            ->with('muni_domicilio', $muni_domicilio)
            ->with('depto_domicilio', $depto_domicilio)
            ->with('nacionalidad', $nacionalidad)
            ->with('etnia', $etnia)
            ->with('motivo_visita', $motivo_visita)
            ->with('tipo_violencia', $tipo_violencia)
            ->with('quien_agrede', $quien_agrede)
            ->with('quien_agrede', $quien_agrede)
            ->with('quien_refiere', $quien_refiere)
            ->with('area_dentro_caimu', $area_dentro_caimu)
            ->with('json_data_table', $json_data_table)
            ->with('referencia_externa', $referencia_externa)
            ->with('sede', $sede)

            ;
    }
}
