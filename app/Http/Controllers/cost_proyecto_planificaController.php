<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_proyecto_planificaRequest;
use App\Http\Requests\Updatecost_proyecto_planificaRequest;
use App\Repositories\cost_proyecto_planificaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_proyecto_planificaController extends AppBaseController
{
    /** @var  cost_proyecto_planificaRepository */
    private $costProyectoPlanificaRepository;

    public function __construct(cost_proyecto_planificaRepository $costProyectoPlanificaRepo)
    {
        $this->costProyectoPlanificaRepository = $costProyectoPlanificaRepo;
    }

    /**
     * Display a listing of the cost_proyecto_planifica.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->costProyectoPlanificaRepository->pushCriteria(new RequestCriteria($request));
        $costProyectoPlanificas = $this->costProyectoPlanificaRepository->all();

        return view('cost_proyecto_planificas.index')
            ->with('costProyectoPlanificas', $costProyectoPlanificas);
    }

    /**
     * Show the form for creating a new cost_proyecto_planifica.
     *
     * @return Response
     */
    public function create()
    {
        return view('cost_proyecto_planificas.create');
    }

    /**
     * Store a newly created cost_proyecto_planifica in storage.
     *
     * @param Createcost_proyecto_planificaRequest $request
     *
     * @return Response
     */
    public function store(Createcost_proyecto_planificaRequest $request)
    {
        $input = $request->all();

        $costProyectoPlanifica = $this->costProyectoPlanificaRepository->create($input);

        Flash::success('Cost Proyecto Planifica saved successfully.');

        return redirect(route('costProyectoPlanificas.index'));
    }

    /**
     * Display the specified cost_proyecto_planifica.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costProyectoPlanifica = $this->costProyectoPlanificaRepository->findWithoutFail($id);

        if (empty($costProyectoPlanifica)) {
            Flash::error('Cost Proyecto Planifica not found');

            return redirect(route('costProyectoPlanificas.index'));
        }

        return view('cost_proyecto_planificas.show')->with('costProyectoPlanifica', $costProyectoPlanifica);
    }

    /**
     * Show the form for editing the specified cost_proyecto_planifica.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costProyectoPlanifica = $this->costProyectoPlanificaRepository->findWithoutFail($id);

        if (empty($costProyectoPlanifica)) {
            Flash::error('Cost Proyecto Planifica not found');

            return redirect(route('costProyectoPlanificas.index'));
        }

        return view('cost_proyecto_planificas.edit')->with('costProyectoPlanifica', $costProyectoPlanifica);
    }

    /**
     * Update the specified cost_proyecto_planifica in storage.
     *
     * @param  int              $id
     * @param Updatecost_proyecto_planificaRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_proyecto_planificaRequest $request)
    {
        $costProyectoPlanifica = $this->costProyectoPlanificaRepository->findWithoutFail($id);

        if (empty($costProyectoPlanifica)) {
            Flash::error('Cost Proyecto Planifica not found');

            return redirect(route('costProyectoPlanificas.index'));
        }

        $costProyectoPlanifica = $this->costProyectoPlanificaRepository->update($request->all(), $id);

        Flash::success('Cost Proyecto Planifica updated successfully.');

        return redirect(route('costProyectoPlanificas.index'));
    }

    /**
     * Remove the specified cost_proyecto_planifica from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costProyectoPlanifica = $this->costProyectoPlanificaRepository->findWithoutFail($id);

        if (empty($costProyectoPlanifica)) {
            Flash::error('Cost Proyecto Planifica not found');

            return redirect(route('costProyectoPlanificas.index'));
        }

        $this->costProyectoPlanificaRepository->delete($id);

        Flash::success('Cost Proyecto Planifica deleted successfully.');

        return redirect(route('costProyectoPlanificas.index'));
    }
}
