<?php
namespace App\Http\Controllers;


use App\Models\cost_proyecto;
use App\Models\cost_proyecto_foto;
use Illuminate\Http\Request;
use App\Http\Requests;


class ImageController extends Controller
{


    /**
     * Create view file
     *
     * @return void
     */
    public function imageUpload(Request $request)
    {
        $a = $request->a;
        $contenido = cost_proyecto_foto::where('id_proyecto',$a)->orderBy('id_proyecto_foto','desc')->paginate(20);
        $datos_proyecto = cost_proyecto::find($a);
        return view('cost_proyectos.image-upload',compact('a','contenido','datos_proyecto'));
    }


    /**
     * Manage Post Request
     *
     * @return void
     */
    public function imageUploadPost(Request $request)
    {

        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,svg,pdf|max:2048',
        ]);

        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        #guardar en bd
        $model = new cost_proyecto_foto();
        $model->id_proyecto = $request->id_proyecto;
        $model->link = 'images/upload'.'/id-'.$request->id_proyecto.'-'.$imageName;
        $model->save();

        #upload en carpeta
       // dd(public_path('images\upload'));
        $request->image->move(public_path('images/upload'), 'id-'.$request->id_proyecto.'-'.$imageName);
        $nombre_completo = 'id-'.$request->id_proyecto.'-'.$imageName;

        return back()
           // ->with('success','Image Uploaded successfully.')
            ->with('path',$nombre_completo);
    }

    public function eliminar_foto($id,Request $request){

        $imagen_borrame = cost_proyecto_foto::find($id);
        $mi_imagen = public_path().'/'.$imagen_borrame->link;

        if (@getimagesize($mi_imagen)) {
            unlink($mi_imagen); //existe
            $imagen_borrame->delete();
            return redirect(url('image_upload?a='.$request->id_proyecto))->with('success','El registro se eliminó, exitosamente.');
        }
        else
        {
            return redirect(url('image_upload?a='.$request->id_proyecto))->with('danger','El registro que desea eliminar, no existe.');
        }
    }


}
