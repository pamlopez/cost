<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_liquidacion_tiempoRequest;
use App\Http\Requests\Updatecost_liquidacion_tiempoRequest;
use App\Repositories\cost_liquidacion_tiempoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_liquidacion_tiempoController extends AppBaseController {

    /** @var  cost_liquidacion_tiempoRepository */
    private $costLiquidacionTiempoRepository;

    public function __construct(cost_liquidacion_tiempoRepository $costLiquidacionTiempoRepo)
    {
        $this->costLiquidacionTiempoRepository = $costLiquidacionTiempoRepo;
    }

    /**
     * Display a listing of the cost_liquidacion_tiempo.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $id_proyecto = $request->id_proyecto;
        $this->costLiquidacionTiempoRepository->pushCriteria(new RequestCriteria($request));
        $costLiquidacionTiempos = $this->costLiquidacionTiempoRepository->findWhere([ 'id_proyecto' => $id_proyecto ])->all();

        return view('cost_liquidacion_tiempos.index', compact('id_proyecto'))
            ->with('costLiquidacionTiempos', $costLiquidacionTiempos);
    }

    /**
     * Show the form for creating a new cost_liquidacion_tiempo.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        return view('cost_liquidacion_tiempos.create', compact('bnd_edit', 'id_proyecto'));
    }

    /**
     * Store a newly created cost_liquidacion_tiempo in storage.
     *
     * @param Createcost_liquidacion_tiempoRequest $request
     *
     * @return Response
     */
    public function store(Createcost_liquidacion_tiempoRequest $request)
    {
        $input = $request->all();
        $costLiquidacionTiempo = $this->costLiquidacionTiempoRepository->create($input);
        Flash::success('El tiempoa a liquidar se creo satisfactoriamente');
        return view('cost_liquidacion_tiempos.show')->with('costLiquidacionTiempo', $costLiquidacionTiempo);
    }

    /**
     * Display the specified cost_liquidacion_tiempo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costLiquidacionTiempo = $this->costLiquidacionTiempoRepository->findWithoutFail($id);

        if (empty($costLiquidacionTiempo)) {
            Flash::error('Cost Liquidacion Tiempo not found');

            return redirect(route('costLiquidacionTiempos.index'));
        }

        return view('cost_liquidacion_tiempos.show')->with('costLiquidacionTiempo', $costLiquidacionTiempo);
    }

    /**
     * Show the form for editing the specified cost_liquidacion_tiempo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costLiquidacionTiempo = $this->costLiquidacionTiempoRepository->findWithoutFail($id);
        $bnd_edit = true;
        if (empty($costLiquidacionTiempo)) {
            Flash::error('Cost Liquidacion Tiempo not found');

            return redirect(route('costLiquidacionTiempos.index'));
        }

        return view('cost_liquidacion_tiempos.edit', compact('bnd_edit'))->with('costLiquidacionTiempo', $costLiquidacionTiempo);
    }

    /**
     * Update the specified cost_liquidacion_tiempo in storage.
     *
     * @param  int $id
     * @param Updatecost_liquidacion_tiempoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_liquidacion_tiempoRequest $request)
    {
        $costLiquidacionTiempo = $this->costLiquidacionTiempoRepository->findWithoutFail($id);

        if (empty($costLiquidacionTiempo)) {
            Flash::error('Cost Liquidacion Tiempo not found');

            return redirect(route('costLiquidacionTiempos.index'));
        }

        $costLiquidacionTiempo = $this->costLiquidacionTiempoRepository->update($request->all(), $id);

        Flash::success('La información de liquidación del tiempo se actualizó satisfactoriamente.');
        return view('cost_liquidacion_tiempos.show')->with('costLiquidacionTiempo', $costLiquidacionTiempo);
    }

    /**
     * Remove the specified cost_liquidacion_tiempo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costLiquidacionTiempo = $this->costLiquidacionTiempoRepository->findWithoutFail($id);

        if (empty($costLiquidacionTiempo)) {
            Flash::error('Cost Liquidacion Tiempo not found');

            return redirect(route('costLiquidacionTiempos.index'));
        }
        $id_proyecto = $costLiquidacionTiempo->id_proyecto;
        $this->costLiquidacionTiempoRepository->delete($id);

        Flash::success('El tiempo ha sido borrado exitosamente');

        return redirect(route('costLiquidacionTiempos.index', [ 'id_proyecto' => $id_proyecto ]));
    }
}
