<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class geoController extends Controller
{
    //PAra el JSON del control dependiente
    public function mostrar_hijos(Request $request) {
        return  \App\Models\geo::json_select($request->depdrop_parents[0], $request->depdrop_params[0]);
    }

    public function mostrar_hijos_con_todo(Request $request) {
        return  \App\Models\geo::json_select($request->depdrop_parents[0], $request->depdrop_params[0],"(Mostrar todos)");
    }
}