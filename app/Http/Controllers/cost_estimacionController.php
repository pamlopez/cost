<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_estimacionRequest;
use App\Http\Requests\Updatecost_estimacionRequest;
use App\Repositories\cost_estimacionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_estimacionController extends AppBaseController {

    /** @var  cost_estimacionRepository */
    private $costEstimacionRepository;

    public function __construct(cost_estimacionRepository $costEstimacionRepo)
    {
        $this->costEstimacionRepository = $costEstimacionRepo;
    }

    /**
     * Display a listing of the cost_estimacion.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $id_proyecto = $request->id_proyecto;
        $tipo_estimacion = $request->tipo_estimacion;
        $this->costEstimacionRepository->pushCriteria(new RequestCriteria($request));
        $costEstimacions = $this->costEstimacionRepository->findWhere([ 'id_proyecto' => $request->id_proyecto, 'tipo_estimacion' => $request->tipo_estimacion ])->all();

        return view('cost_estimacions.index', compact('tipo_estimacion', 'id_proyecto'))
            ->with('costEstimacions', $costEstimacions);
    }

    /**
     * Show the form for creating a new cost_estimacion.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        $tipo_estimacion = $request->tipo_estimacion;
        $fmt_tipo_estimacion =  ($tipo_estimacion == 1 )? "Estimación Aprobada":"Estimación Pagada";
        return view('cost_estimacions.create', compact('bnd_edit', 'id_proyecto', 'tipo_estimacion','fmt_tipo_estimacion'));
    }

    /**
     * Store a newly created cost_estimacion in storage.
     *
     * @param Createcost_estimacionRequest $request
     *
     * @return Response
     */
    public function store(Createcost_estimacionRequest $request)
    {
        $input = $request->all();
        $costEstimacion = $this->costEstimacionRepository->create($input);
        Flash::success('La estimación se creo exitosamente');
        return redirect(route('costEstimacions.show', [ $costEstimacion->id ]));
    }

    /**
     * Display the specified cost_estimacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costEstimacion = $this->costEstimacionRepository->findWithoutFail($id);

        if (empty($costEstimacion)) {
            Flash::error('Cost Estimacion not found');

            return redirect(route('costEstimacions.index'));
        }

        return view('cost_estimacions.show')->with('costEstimacion', $costEstimacion);
    }

    /**
     * Show the form for editing the specified cost_estimacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costEstimacion = $this->costEstimacionRepository->findWithoutFail($id);
        $bnd_edit = true;
        if (empty($costEstimacion)) {
            Flash::error('Cost Estimacion not found');

            return redirect(route('costEstimacions.index'));
        }
        return view('cost_estimacions.edit', compact('bnd_edit'))->with('costEstimacion', $costEstimacion);
    }

    /**
     * Update the specified cost_estimacion in storage.
     *
     * @param  int $id
     * @param Updatecost_estimacionRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_estimacionRequest $request)
    {
        $costEstimacion = $this->costEstimacionRepository->findWithoutFail($id);
        if (empty($costEstimacion)) {
            Flash::error('Cost Estimacion not found');

            return redirect(route('costEstimacions.index'));
        }
        $costEstimacion = $this->costEstimacionRepository->update($request->all(), $id);
        Flash::success('Cost Estimacion updated successfully.');
        return redirect(route('costEstimacions.show', [ $costEstimacion->id ]));
    }

    /**
     * Remove the specified cost_estimacion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costEstimacion = $this->costEstimacionRepository->findWithoutFail($id);

        if (empty($costEstimacion)) {
            Flash::error('Cost Estimacion not found');

            return redirect(route('costEstimacions.index'));
        }
        $id_proyecto = $costEstimacion->id_proyecto;
        $tipo_estimacion = $costEstimacion->tipo_estimacion;
        $this->costEstimacionRepository->delete($id);

        Flash::success('La estimación fue borrada exitosamente');
        return redirect(route('costEstimacions.index', [ 'id_proyecto' => $id_proyecto, 'tipo_estimacion' => $tipo_estimacion ]));
    }
}
