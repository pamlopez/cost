<?php

namespace App\Http\Controllers;

use App\Models\control_asistencia;
use App\Models\cost_proyecto;
use App\Models\indicadores;
use App\Models\proceso;
use App\Models\proceso_accion;
use Illuminate\Http\Request;
use App\Models\atencion_inicial;

use App\Http\Requests;

class indicadoresController extends Controller
{
    function indicadores(Request $request) {

        $filtros= cost_proyecto::criterios_default($request);
        $indicadores = new indicadores();
        $total_historico = $indicadores->total_historico();
        $total_proyecto_anio= $indicadores->total_proyecto_anio($filtros);
        $total_sectores_afectados= $indicadores->total_sectores_afectados($filtros);

        #estados del proyecto
        $total_estado_actual_proyecto= $indicadores->total_estado_actual_proyecto($filtros,0);
        $tabla_estado_actual_proyecto= $indicadores->total_estado_actual_proyecto($filtros,1);

        #entidad de adquisicion
        $tabla_entidad_adquisicion= $indicadores->total_entidad_adquisicion($filtros,0);
        $total_entidad_adquisicion = $indicadores->total_entidad_adquisicion($filtros,1);

        //dd($tabla_entidad_adquisicion);
        #entidad de adquisicion
        $total_sectores_afectados_detalle= $indicadores->total_sectores_afectados_detalle($filtros,0);
        $tabla_sectores_afectados_detalle= $indicadores->total_sectores_afectados_detalle($filtros,1);



        $datitos = new \stdClass();
        $datitos->g_estado_actual_proyecto  =indicadores::g_pie($total_estado_actual_proyecto,"Distribución de estados de proyectos");
        $datitos->g_entidad_adquisicion  = indicadores::g_barra($tabla_entidad_adquisicion,"Entidades de adquisición", "Cantidad");
        $datitos->g_sectores = indicadores::g_barra($total_sectores_afectados_detalle,"Sectores", "Cantidad");
        //$datitos->g_entidad_adquisicion  = atencion_inicial::g_columna($total_entidad_adquisicion, $titulo="Por tipo de violencia",$nombre_serie="Hojas de vida");

//dd($datitos);

        return view("indicadores.proyectos",compact("total_sectores_afectados_detalle","tabla_sectores_afectados_detalle","total_entidad_adquisicion","datitos","filtros","muestro",'total_historico','total_proyecto_anio','total_sectores_afectados','total_estado_actual_proyecto','tabla_estado_actual_proyecto','tabla_entidad_adquisicion','tabla_sectores_afectados_detalle'));

    }
}
