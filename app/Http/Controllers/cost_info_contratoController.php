<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createcost_info_contratoRequest;
use App\Http\Requests\Updatecost_info_contratoRequest;
use App\Models\cat_item;
use App\Repositories\cost_info_contratoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash; // rewrite this line

use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class cost_info_contratoController extends AppBaseController {

    /** @var  cost_info_contratoRepository */
    private $costInfoContratoRepository;

    public function __construct(cost_info_contratoRepository $costInfoContratoRepo)
    {
        $this->costInfoContratoRepository = $costInfoContratoRepo;
    }

    /**
     * Display a listing of the cost_info_contrato.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->costInfoContratoRepository->pushCriteria(new RequestCriteria($request));
        $costInfoContratos = $this->costInfoContratoRepository->all();

        return view('cost_info_contratos.index')
            ->with('costInfoContratos', $costInfoContratos);
    }

    /**
     * Show the form for creating a new cost_info_contrato.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $bnd_edit = false;
        $id_proyecto = $request->id_proyecto;
        $id_fase = $request->id_fase;
        $tipo = $request->tipo;
        return view('cost_info_contratos.create', compact('bnd_edit', 'id_proyecto', 'id_fase', 'tipo'));
    }

    /**
     * Store a newly created cost_info_contrato in storage.
     *
     * @param Createcost_info_contratoRequest $request
     *
     * @return Response
     */
    public function store(Createcost_info_contratoRequest $request)
    {
        $input = $request->all();
        $input[ 'fecha_aprobacion_contrato' ] = $request->fecha_aprobacion_contrato_submit;
        $input[ 'fecha_nombramiento' ] = $request->fecha_nombramiento_submit;
        $input[ 'fecha_inicio' ] = $request->fecha_inicio_submit;
        $input[ 'fecha_contrato' ] = $request->fecha_contrato_submit;
        $input[ 'fecha_publicacion' ] = $request->fecha_publicacion_submit;
        $input[ 'fecha_ultima_mod_gc' ] = $request->fecha_ultima_mod_gc_submit;
        $input[ 'fecha_autoriza_bitacora' ] = $request->fecha_autoriza_bitacora_submit;
        $input[ 'fecha_inicio_ejecucion' ] = $request->fecha_inicio_ejecucion_submit;
        $input[ 'estado_contrato_oc_d' ] = isset($request->estado_contrato_oc)?cat_item::find($request->estado_contrato_oc)->descripcion:null;
        unset($input[ 'fecha_autoriza_bitacora_submit' ],$input[ 'fecha_ultima_mod_gc_submit' ], $input[ 'fecha_publicacion_submit' ],$input[ 'fecha_aprobacion_contrato_submit' ], $input[ 'fecha_nombramiento_submit' ], $input[ 'fecha_inicio_submit' ], $input[ 'fecha_contrato_submit' ]);

        $input[ 'fecha_inicio_ejecucion' ] = $request->fecha_inicio_ejecucion_submit;
        $input[ 'fecha_fin_ejecucion' ] = $request->fecha_fin_ejecucion_submit;
        $input[ 'metodo_procuracion' ] = $request->metodo_procuracion;
        $input[ 'costo_estimado_moneda' ] = $request->costo_estimado_moneda;
        $input[ 'costo_estimado' ] = $request->costo_estimado;
        $input[ 'moneda_final' ] = $request->moneda_final;
        $input[ 'precio_contrato_final' ] = $request->precio_contrato_final;

        $costInfoContrato = $this->costInfoContratoRepository->create($input);

        Flash::success('La Información del  Contrato se creó exitosamente');
        return redirect(route('costInfoContratos.show', [ $costInfoContrato->id ]));

    }

    /**
     * Display the specified cost_info_contrato.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $costInfoContrato = $this->costInfoContratoRepository->findWithoutFail($id);
        if (empty($costInfoContrato)) {
            Flash::error('Cost Info Contrato not found');
            return redirect(route('costInfoContratos.index'));
        }
        return view('cost_info_contratos.show')->with('costInfoContrato', $costInfoContrato);
    }

    /**
     * Show the form for editing the specified cost_info_contrato.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $costInfoContrato = $this->costInfoContratoRepository->findWithoutFail($id);
        $id_fase = $costInfoContrato->id_fase;
        $bnd_edit = true;
        if (empty($costInfoContrato)) {
            Flash::error('Cost Info Contrato not found');

            return redirect(route('costInfoContratos.index'));
        }

        return view('cost_info_contratos.edit', compact('bnd_edit','id_fase'))->with('costInfoContrato', $costInfoContrato);
    }

    /**
     * Update the specified cost_info_contrato in storage.
     *
     * @param  int $id
     * @param Updatecost_info_contratoRequest $request
     *
     * @return Response
     */
    public function update($id, Updatecost_info_contratoRequest $request)
    {//dd($request);
        $costInfoContrato = $this->costInfoContratoRepository->findWithoutFail($id);
        $input = $request->all();
        $input[ 'fecha_aprobacion_contrato' ] = $request->fecha_aprobacion_contrato_submit;
        $input[ 'fecha_nombramiento' ] = $request->fecha_nombramiento_submit;
        $input[ 'fecha_inicio' ] = $request->fecha_inicio_submit;
        $input[ 'fecha_contrato' ] = $request->fecha_contrato_submit;
        $input[ 'fecha_publicacion' ] = $request->fecha_publicacion_submit;
        $input[ 'fecha_ultima_mod_gc' ] = $request->fecha_ultima_mod_gc_submit;
        $input[ 'fecha_autoriza_bitacora' ] = $request->fecha_autoriza_bitacora_submit;
        $input[ 'fecha_inicio_ejecucion' ] = $request->fecha_inicio_ejecucion_submit;
        $input[ 'estado_contrato_oc_d' ] = isset($request->estado_contrato_oc)?cat_item::find($request->estado_contrato_oc)->descripcion:null;

        $input[ 'fecha_inicio_ejecucion' ] = $request->fecha_inicio_ejecucion_submit;
        $input[ 'fecha_fin_ejecucion' ] = $request->fecha_fin_ejecucion_submit;
        $input[ 'metodo_procuracion' ] = $request->metodo_procuracion;
        $input[ 'costo_estimado_moneda' ] = $request->costo_estimado_moneda;
        $input[ 'costo_estimado' ] = $request->costo_estimado;
        $input[ 'moneda_final' ] = $request->moneda_final;
        $input[ 'precio_contrato_final' ] = $request->precio_contrato_final;

        unset($input[ 'fecha_aprobacion_contrato_submit' ], $input[ 'fecha_nombramiento_submit' ], $input[ 'fecha_inicio_submit' ], $input[ 'fecha_contrato_submit' ]);

        if (empty($costInfoContrato)) {
            Flash::error('Cost Info Contrato not found                  ');

            return redirect(route('costInfoContratos.index'));
        }
        $costInfoContrato = $this->costInfoContratoRepository->update($input,$id);

        Flash::success('La Información del  Contrato se actualizó exitosamente.');
        return redirect(route('costInfoContratos.show', [ $costInfoContrato->id ]));
    }

    /**
     * Remove the specified cost_info_contrato from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $costInfoContrato = $this->costInfoContratoRepository->findWithoutFail($id);

        if (empty($costInfoContrato)) {
            Flash::error('Cost Info Contrato not found');

            return redirect(route('costInfoContratos.index'));
        }
        $id_proyecto =$costInfoContrato->id_proyecto;
        $this->costInfoContratoRepository->delete($id);

        Flash::success('Información del  Contrato eliminada exitosamente.');

        return redirect(route('costProyectos.show', ['id_proyecto'=>$id_proyecto,'a'=>1]));
    }
}
