<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\proceso;

class CreateprocesoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        //return proceso::$rules;
        return [
            'id_tipo_proceso' => 'required',
            'id_juzgado' => 'required',
            'id_calidad' => 'required',
            'id_apersonamiento_caimu' => 'required',
            'id_estado' => 'required',
            'id_motivo' => 'required',
            'id_quien_ordena' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'id_tipo_proceso.required'=> 'Seleccione una opción.',
            'id_juzgado.required'=> 'Seleccione una opción.',
            'id_calidad.required'=> 'Seleccione una opción.',
            'id_apersonamiento_caimu.required'=> 'Seleccione una opción.',
            'id_estado.required'=> 'Seleccione una opción.',
            'id_motivo.required'=> 'Seleccione una opción.',
            'id_quien_ordena.required'=> 'Seleccione una opción.',
        ];
    }
}
