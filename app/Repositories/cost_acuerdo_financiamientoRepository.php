<?php

namespace App\Repositories;

use App\Models\cost_acuerdo_financiamiento;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_acuerdo_financiamientoRepository
 * @package App\Repositories
 * @version January 17, 2019, 5:16 pm UTC
 *
 * @method cost_acuerdo_financiamiento findWithoutFail($id, $columns = ['*'])
 * @method cost_acuerdo_financiamiento find($id, $columns = ['*'])
 * @method cost_acuerdo_financiamiento first($columns = ['*'])
*/
class cost_acuerdo_financiamientoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'tipo',
        'id_fase',
        'partida_presupuestaria',
        'constitucional',
        'iva_paz',
        'circulacion_vehiculos',
        'petroleo',
        'consejo_desarrollo',
        'fondos_propios',
        'otros_descripcion',
        'otros',
        'donacion',
        'prestamo',
        'fecha_aprobacion',
        'aporte_beneficiario',
        'transferencia',
        'observaciones'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_acuerdo_financiamiento::class;
    }
}
