<?php

namespace App\Repositories;

use App\Models\descarga_responsable;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class descarga_responsableRepository
 * @package App\Repositories
 * @version March 3, 2019, 4:57 pm UTC
 *
 * @method descarga_responsable findWithoutFail($id, $columns = ['*'])
 * @method descarga_responsable find($id, $columns = ['*'])
 * @method descarga_responsable first($columns = ['*'])
*/
class descarga_responsableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_completo',
        'no_identificacion_personal',
        'descripcion',
        'fh_descarga',
        'id_proyecto'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return descarga_responsable::class;
    }
}
