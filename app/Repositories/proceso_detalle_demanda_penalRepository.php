<?php

namespace App\Repositories;

use App\Models\proceso_detalle_demanda_penal;
use InfyOm\Generator\Common\BaseRepository;

class proceso_detalle_demanda_penalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proceso',
        'fh_inicio',
        'diligencia_previa_audiencia',
        'fh_audiencia',
        'hr_audiencia',
        'diligencia_audiencia',
        'estrategia',
        'incidencia',
        'resultado'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return proceso_detalle_demanda_penal::class;
    }
}
