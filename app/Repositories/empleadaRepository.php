<?php

namespace App\Repositories;

use App\Models\empleada;
use InfyOm\Generator\Common\BaseRepository;

class empleadaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_users',
        'id_caimu',
        'n1',
        'n2',
        'p1',
        'p2',
        'id_puesto'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return empleada::class;
    }
}
