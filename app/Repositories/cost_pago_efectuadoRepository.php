<?php

namespace App\Repositories;

use App\Models\cost_pago_efectuado;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_pago_efectuadoRepository
 * @package App\Repositories
 * @version January 17, 2019, 5:19 pm UTC
 *
 * @method cost_pago_efectuado findWithoutFail($id, $columns = ['*'])
 * @method cost_pago_efectuado find($id, $columns = ['*'])
 * @method cost_pago_efectuado first($columns = ['*'])
*/
class cost_pago_efectuadoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'id_fase',
        'tipo',
        'no_pago',
        'fecha_pago',
        'monto_pagado',
        'tiene_ampliacion',
        'fecha_aprobacion_ampliacion',
        'monto_ampliacion',
        'ampliacion_aprobada_por',
        'acta_ampliacion',
        'monto_acumulado',
        'observaciones'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_pago_efectuado::class;
    }
}
