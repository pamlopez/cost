<?php

namespace App\Repositories;

use App\Models\cost_proyecto_supervision;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_proyecto_supervisionRepository
 * @package App\Repositories
 * @version October 30, 2018, 5:09 am UTC
 *
 * @method cost_proyecto_supervision findWithoutFail($id, $columns = ['*'])
 * @method cost_proyecto_supervision find($id, $columns = ['*'])
 * @method cost_proyecto_supervision first($columns = ['*'])
*/
class cost_proyecto_supervisionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'sup_invitacion',
        'sup_listado_ofe',
        'sup_acta_adj',
        'sup_responsable',
        'sup_monto_contratado',
        'sup_plazo_contrato',
        'sup_tipo_contrato',
        'sup_contrato_alcance',
        'sup_programa_trabajo',
        'sup_f_inicio',
        'sup_f_entrega',
        'sup_responsable_ea',
        'sup_f_aprobacion_contrato',
        'sup_anticipo',
        'sup_no_pagos',
        'sup_fechas',
        'sup_mod_plazo',
        'resp_mod_monto',
        'nueva_f_finaliza',
        'sup_mod_monto',
        'sup_resp_mod_monto',
        'sup_porcentaje',
        'sup_acta_recepcion',
        'sup_acta_recepcion_f',
        'sup_finiquito_f',
        'fianza_sos_oferta',
        'fianza_cumplimiento',
        'sup_afianzadora'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_proyecto_supervision::class;
    }
}
