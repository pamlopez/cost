<?php

namespace App\Repositories;

use App\Models\agenda;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class agendaRepository
 * @package App\Repositories
 * @version July 4, 2018, 7:05 am UTC
 *
 * @method agenda findWithoutFail($id, $columns = ['*'])
 * @method agenda find($id, $columns = ['*'])
 * @method agenda first($columns = ['*'])
*/
class agendaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fh_agenda',
        'id_abogada',
        'hr_inicio',
        'hr_fin',
        'id_vehiculo',
        'id_atencion_inicial',
        'id_proceso',
        'hr_salida',
        'id_lugar',
        'direccion',
        'id_accion_agenda_legal',
        'id_tipo',
        'id_realiza_suspende_audi_debate',
        'id_tipo_proceso_agenda_legal',
        'id_quien_agenda',
        'instruccion_accion',
        'resultado_hoja_evolucion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return agenda::class;
    }
}
