<?php

namespace App\Repositories;

use App\Models\proceso;
use InfyOm\Generator\Common\BaseRepository;

class procesoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_persona',
        'id_tipo_proceso',
        'juzgado',
        'anio_inicio_demanda',
        'no_proceso',
        'oficial',
        'calidad',
        'apersonamiento_caimu',
        'id_estado',
        'motivo',
        'anio_terminacion',
        'observaciones',
        'id_quien_ordena'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return proceso::class;
    }
}
