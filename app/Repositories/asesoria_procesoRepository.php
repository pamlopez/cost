<?php

namespace App\Repositories;

use App\Models\asesoria_proceso;
use InfyOm\Generator\Common\BaseRepository;

class asesoria_procesoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_atencion_inicial',
        'id_tipo_proceso_asesoria',
        'id_clase_proceso'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return asesoria_proceso::class;
    }
}
