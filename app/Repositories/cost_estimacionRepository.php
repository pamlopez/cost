<?php

namespace App\Repositories;

use App\Models\cost_estimacion;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_estimacionRepository
 * @package App\Repositories
 * @version April 21, 2019, 2:26 pm UTC
 *
 * @method cost_estimacion findWithoutFail($id, $columns = ['*'])
 * @method cost_estimacion find($id, $columns = ['*'])
 * @method cost_estimacion first($columns = ['*'])
*/
class cost_estimacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'tipo_estimacion',
        'numero_pagos',
        'monto_total_pagos'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_estimacion::class;
    }
}
