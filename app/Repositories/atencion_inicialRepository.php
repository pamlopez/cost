<?php

namespace App\Repositories;

use App\Models\atencion_inicial;
use InfyOm\Generator\Common\BaseRepository;

class atencion_inicialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_caimu',
        'id_persona',
        'fecha',
        'numero_historia_vida',
        'edad',
        'id_rango_caimu',
        'id_rango_ine',
        'id_nacionalidad',
        'telefono',
        'id_area_residencial',
        'id_estado_civil',
        'id_relacion_actual',
        'id_ocupacion',
        'id_escolaridad',
        'nombre_agresor',
        'id_ocupacion_agresor',
        'lugar_trabajo_agresor',
        'ingreso_mensual_agresor',
        'cant_hija_mayor',
        'cant_hijo_mayor',
        'cant_hija_menor',
        'cant_hijo_menor',
        'motivo_visita_sra',
        'id_motivo_visita_familia',
        'id_motivo_visita_penal',
        'id_motivo_visita_psicologica',
        'id_motivo_visita_trabajadora_social',
        'necesita_albergue',
        'id_quien_agrede',
        'id_quien_refiere_a_caimu',
        'referencia_exacta',
        'id_quien_entrevista',
        'quien_apoya'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return atencion_inicial::class;
    }
}
