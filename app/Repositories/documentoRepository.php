<?php

namespace App\Repositories;

use App\Models\documento;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class documentoRepository
 * @package App\Repositories
 * @version July 3, 2019, 4:01 am UTC
 *
 * @method documento findWithoutFail($id, $columns = ['*'])
 * @method documento find($id, $columns = ['*'])
 * @method documento first($columns = ['*'])
*/
class documentoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'id_fase',
        'tipo',
        'url',
        'documento',
        'titulo',
        'fh_publicacion',
        'fh_modificacion',
        'formato',
        'lenguaje',
        'pagina_inicio',
        'pagina_fin',
        'detalle_acceso',
        'autor'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return documento::class;
    }
}
