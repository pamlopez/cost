<?php

namespace App\Repositories;

use App\Models\cost_proyecto;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_proyectoRepository
 * @package App\Repositories
 * @version October 30, 2018, 4:38 am UTC
 *
 * @method cost_proyecto findWithoutFail($id, $columns = ['*'])
 * @method cost_proyecto find($id, $columns = ['*'])
 * @method cost_proyecto first($columns = ['*'])
*/
class cost_proyectoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'entidad_adquisicion',
        'id_entidad_adquisicion',
        'entidad_admin_contrato',
        'sector',
        'sub_sector',
        'programa_multi_d',
        'programa_multi',
        'presupuesto_proyecto_multi_d',
        'presupuesto_proyecto_multi',
        'contacto_entidad',
        'nombre_proyecto',
        'detalle_responsable',
        'fh_ultima_actualizacion',
        'localizacion',
        'localizacion_muni_d',
        'localizacion_muni',
        'localizacion_depto_d',
        'localizacion_depto',
        'coordenadas',
        'coordenadas_cln',
        'snip',
        'snip_detalle',
        'proposito',
        'resumen',
        'beneficiario_d',
        'beneficiario_total',
        'f_aprobacion',
        'f_r_proactiva',
        'f_r_reactiva',
        'estado_oc',
        'estado_oc_d',
        'sector_oc',
        'subsector_oc',
        'tipo_oc',
        'f_finalizacion',
        'vida_activo',
        'codigo_postal',
        'pais',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_proyecto::class;
    }
}
