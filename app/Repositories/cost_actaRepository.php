<?php

namespace App\Repositories;

use App\Models\cost_acta;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_actaRepository
 * @package App\Repositories
 * @version January 17, 2019, 5:16 pm UTC
 *
 * @method cost_acta findWithoutFail($id, $columns = ['*'])
 * @method cost_acta find($id, $columns = ['*'])
 * @method cost_acta first($columns = ['*'])
*/
class cost_actaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'id_fase',
        'tipo',
        'id_fase',
        'tipo_acta',
        'tipo_acta_d',
        'no_acta',
        'fecha_acta',
        'link'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_acta::class;
    }
}
