<?php

namespace App\Repositories;

use App\Models\cost_operador;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_operadorRepository
 * @package App\Repositories
 * @version April 21, 2019, 7:32 pm UTC
 *
 * @method cost_operador findWithoutFail($id, $columns = ['*'])
 * @method cost_operador find($id, $columns = ['*'])
 * @method cost_operador first($columns = ['*'])
*/
class cost_operadorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'nombre_operador',
        'usuario_operador',
        'fecha_operador'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_operador::class;
    }
}
