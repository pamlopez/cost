<?php

namespace App\Repositories;

use App\Models\cost_liquidacion_monto;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_liquidacion_montoRepository
 * @package App\Repositories
 * @version March 25, 2019, 5:04 am UTC
 *
 * @method cost_liquidacion_monto findWithoutFail($id, $columns = ['*'])
 * @method cost_liquidacion_monto find($id, $columns = ['*'])
 * @method cost_liquidacion_monto first($columns = ['*'])
*/
class cost_liquidacion_montoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'documento_cambio',
        'suma_mod',
        'razon_cambio',
        'monto_actualizado',
        'no_fianza'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_liquidacion_monto::class;
    }
}
