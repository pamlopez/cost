<?php

namespace App\Repositories;

use App\Models\cost_ampliacion;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_ampliacionRepository
 * @package App\Repositories
 * @version January 17, 2019, 5:15 pm UTC
 *
 * @method cost_ampliacion findWithoutFail($id, $columns = ['*'])
 * @method cost_ampliacion find($id, $columns = ['*'])
 * @method cost_ampliacion first($columns = ['*'])
*/
class cost_ampliacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_ampliacion',
        'id_proyecto',
        'id_fase',
        'tipo',
        'tipo_ampliacion',
        'no_ampliacion',
        'fecha_ampliacion',
        'no_registro_ampliacion',
        'avance_financiero',
        'avance_obra',
        'link',
        'observaciones'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_ampliacion::class;
    }
}
