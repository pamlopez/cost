<?php

namespace App\Repositories;

use App\Models\politica;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class politicaRepository
 * @package App\Repositories
 * @version November 10, 2021, 1:24 pm CST
 *
 * @method politica findWithoutFail($id, $columns = ['*'])
 * @method politica find($id, $columns = ['*'])
 * @method politica first($columns = ['*'])
*/
class politicaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'descripcion_html',
        'estado'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return politica::class;
    }
}
