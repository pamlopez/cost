<?php

namespace App\Repositories;

use App\Models\empleo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class empleoRepository
 * @package App\Repositories
 * @version October 16, 2018, 8:09 pm UTC
 *
 * @method empleo findWithoutFail($id, $columns = ['*'])
 * @method empleo find($id, $columns = ['*'])
 * @method empleo first($columns = ['*'])
*/
class empleoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'puesto',
        'link',
        'descripcion',
        'otros'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return empleo::class;
    }
}
