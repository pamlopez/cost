<?php

namespace App\Repositories;

use App\Models\control_asistencia;
use InfyOm\Generator\Common\BaseRepository;

class control_asistenciaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_atencion_inicial',
        'id_persona',
        'fecha_hora',
        'acompania_en_su_nombre',
        'visita_atencion_inicial',
        'id_otros_visita_atencion_inicial',
        'id_atendio_visita_atencion_inicial',
        'id_visita_area_legal',
        'id_atendio_visita_area_legal',
        'id_visita_area_psicologica',
        'id_atendio_visita_area_psicologica',
        'id_visita_area_trabajo_social',
        'id_atendio_visita_area_trabajo_social'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return control_asistencia::class;
    }
}
