<?php

namespace App\Repositories;

use App\Models\cost_proyecto_dis_plan;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_proyecto_dis_planRepository
 * @package App\Repositories
 * @version October 30, 2018, 5:07 am UTC
 *
 * @method cost_proyecto_dis_plan findWithoutFail($id, $columns = ['*'])
 * @method cost_proyecto_dis_plan find($id, $columns = ['*'])
 * @method cost_proyecto_dis_plan first($columns = ['*'])
*/
class cost_proyecto_dis_planRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'presupuesto_moneda',
        'id_proyecto',
        'dis_anuncio_invitacion',
        'dis_listado_oferentes',
        'acta_adjudica',
        'dis_responsable',
        'dis_precio_contratado',
        'dis_plazo_contrato',
        'dis_tipo_contrato',
        'dis_afianzadora',
        'dis_fianza',
        'dis_f_inicio',
        'dis_f_entrega',
        'dis_responsable_ea',
        'dis_f_aprobacion_contrato_d',
        'dis_f_aprobacion_contrato',
        'dis_anticipo',
        'dis_numero_pagos',
        'dis_fechas',
        'dis_mod_plazo',
        'dis_responsable_mod_plazo',
        'dis_f_nueva_finaliza',
        'dis_monto_modificado',
        'dis_monto_modificado_d',
        'dis_responsable_mod_monto',
        'dis_porcentaje_cambio',
        'dis_acta_recepcion',
        'dis_f_recepcion',
        'dis_f_recepcion_d',
        'dis_f_finiquito',
        'dis_fecha_finiquito_det',
        'dis_fianza_detalle',
        'dis_det_fianza',
        'instrumento_eia',
        'elaborado_por_eia',
        'f_eia',
        'resolucion_marn',
        'f_resolucion_marn',
        'medida_mitigacion_presupuesto',
        'agrip',
        'agrip_f',
        'agrip_elaborado_por',
        'agrip_medida_mitigacion',
        'responsable_estudio_tecnico',
        'estudio_suelo',
        'estudio_suelo_f',
        'estudio_suelo_por',
        'estudio_topografia',
        'estudio_topografia_f',
        'estudio_topografia_por',
        'estudio_geologico',
        'estudio_geologico_f',
        'estudio_geologico_por',
        'diseno_estructural',
        'diseno_estructural_f',
        'diseno_estructural_por',
        'memoria_calculo',
        'memoria_calculo_responsable',
        'plano_completo',
        'plano_completo_responsable',
        'espe_tecnica',
        'espe_tecnica_responsable',
        'espe_general',
        'espe_general_responsable',
        'dispo_especial',
        'dispo_especial_responsable',
        'otro_estudio',
        'otro_estudio_responsable',
        'presupuesto_proyecto',
        'costo_estimado_proyecto',
        'presupuesto_aprobacion_f',
        'estado_actual_proyecto_d',
        'estado_actual_proyecto',
        'avance_fisico',
        'avance_financiero',
        'modalidad_contratacion',
        'fuente_financiamiento_dis',
        'monto_asignado_dis',
        'nog',
        'nog_f',
        'fuente_f_sup',
        'monto_sup',
        'fuente_f_ejecucion',
        'monto_ejecucion',
        'fuente_f_ambiental',
        'monto_ambiental',
        'fuente_riesgo',
        'monto_riesgo',
        'estudio_viabilidad',
        'aprobacion_viabilidad'

    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_proyecto_dis_plan::class;
    }
}
