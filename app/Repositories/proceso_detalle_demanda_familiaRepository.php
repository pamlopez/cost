<?php

namespace App\Repositories;

use App\Models\proceso_detalle_demanda_familia;
use InfyOm\Generator\Common\BaseRepository;

class proceso_detalle_demanda_familiaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proceso',
        'fh_diligencia_previa',
        'desc_diligencia_previa',
        'fh_presentacion_demanda',
        'accion_legal',
        'produracion',
        'fh_resolucion',
        'previo/rechazo',
        'fh_accion',
        'subsanacion_previos',
        'fh_procuracion',
        'fh_primera_resolucion',
        'contenido_resolucion',
        'fh_audiencia_senialada',
        'hora_audiencia_senialada',
        'tipo_audiencia',
        'fh_notificacion_demandado',
        'resultado_audiencia'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return proceso_detalle_demanda_familia::class;
    }
}
