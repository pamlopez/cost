<?php

namespace App\Repositories;

use App\Models\sede;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class sedeRepository
 * @package App\Repositories
 * @version March 29, 2019, 11:48 pm UTC
 *
 * @method sede findWithoutFail($id, $columns = ['*'])
 * @method sede find($id, $columns = ['*'])
 * @method sede first($columns = ['*'])
*/
class sedeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return sede::class;
    }
}
