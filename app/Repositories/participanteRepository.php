<?php

namespace App\Repositories;

use App\Models\participante;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class participanteRepository
 * @package App\Repositories
 * @version April 21, 2019, 3:29 pm UTC
 *
 * @method participante findWithoutFail($id, $columns = ['*'])
 * @method participante find($id, $columns = ['*'])
 * @method participante first($columns = ['*'])
*/
class participanteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre_comun',
        'nombre_legal',
        'uri',
        'direccion',
        'direccion_referencia',
        'codigo_postal',
        'nombre_contacto',
        'email',
        'telefono',
        'localidad',
        'region',
        'nombre_pais',
        'punto_contacto',
        'numero_fax'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return participante::class;
    }
}
