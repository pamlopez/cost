<?php

namespace App\Repositories;

use App\Models\cost_info_contrato;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_info_contratoRepository
 * @package App\Repositories
 * @version January 17, 2019, 5:18 pm UTC
 *
 * @method cost_info_contrato findWithoutFail($id, $columns = ['*'])
 * @method cost_info_contrato find($id, $columns = ['*'])
 * @method cost_info_contrato first($columns = ['*'])
*/
class cost_info_contratoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'tipo',
        'id_fase',
        'oferentes',
        'nombre_responsable_seleccionado',
        'estado_contrato',
        'plazo_contrato',
        'tipo_numero_contrato',
        'fecha_inicio',
        'empresa_constructora',
        'objeto_contrato',
        'no_precalificado',
        'fecha_actualizacion',
        'precio_contrato',
        'fecha_contrato',
        'aprobacion_contrato',
        'fecha_aprobacion_contrato',
        'nombre_firma',
        'cargo_firma',
        'nombramiento',
        'fecha_nombramiento',
        'conclusion',
        'modalidad_contratacion',
        'modalidad_contratacion_d',
        'bases_concurso',
        'bases_concurso_link',
        'fecha_publicacion',
        'fecha_ultima_mod_gc',
        'total_inconformidades',
        'total_inconformidades_r',
        'alcance',
        'programa',
        'nombres_suscriben',
        'fecha_autoriza_bitacora',
        'quien_autoriza_bitacora',
        'clausula_sobrecosto',

        'costo_estimado_moneda',
        'costo_estimado',
        'fecha_fin_ejecucion',
        'fecha_inicio_ejecucion',
        'metodo_procuracion',
        'detalle_procuracion',
        'precio_contrato_final',
        'moneda_final',


    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_info_contrato::class;
    }
}
