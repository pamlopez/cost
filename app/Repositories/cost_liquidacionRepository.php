<?php

namespace App\Repositories;

use App\Models\cost_liquidacion;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_liquidacionRepository
 * @package App\Repositories
 * @version March 25, 2019, 5:03 am UTC
 *
 * @method cost_liquidacion findWithoutFail($id, $columns = ['*'])
 * @method cost_liquidacion find($id, $columns = ['*'])
 * @method cost_liquidacion first($columns = ['*'])
*/
class cost_liquidacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'informe_avances',
        'planos_finales',
        'reporte_evaluacion',
        'informe_terminacion',
        'informe_evaluacion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_liquidacion::class;
    }
}
