<?php

namespace App\Repositories;

use App\Models\cost_fianza;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_fianzaRepository
 * @package App\Repositories
 * @version January 17, 2019, 5:17 pm UTC
 *
 * @method cost_fianza findWithoutFail($id, $columns = ['*'])
 * @method cost_fianza find($id, $columns = ['*'])
 * @method cost_fianza first($columns = ['*'])
*/
class cost_fianzaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'id_fase',
        'tipo',
        'tipo_fianza',
        'tipo_fianza_d',
        'no_fianza',
        'monto',
        'fecha_fianza',
        'link',
        'clase_fianza',
        'plazo_fianza',
        'entidad_afianzadora'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_fianza::class;
    }
}
