<?php

namespace App\Repositories;

use App\Models\cost_liquidacion_tiempo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_liquidacion_tiempoRepository
 * @package App\Repositories
 * @version March 25, 2019, 5:05 am UTC
 *
 * @method cost_liquidacion_tiempo findWithoutFail($id, $columns = ['*'])
 * @method cost_liquidacion_tiempo find($id, $columns = ['*'])
 * @method cost_liquidacion_tiempo first($columns = ['*'])
*/
class cost_liquidacion_tiempoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'no_modificaciones_plazo',
        'cantidad_tiempo_modificado',
        'nueva_fecha_finaliza',
        'razones_cambio_plazo',
        'no_fianza'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_liquidacion_tiempo::class;
    }
}
