<?php

namespace App\Repositories;

use App\Models\cost_liquidacion_alcance;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_liquidacion_alcanceRepository
 * @package App\Repositories
 * @version March 25, 2019, 5:04 am UTC
 *
 * @method cost_liquidacion_alcance findWithoutFail($id, $columns = ['*'])
 * @method cost_liquidacion_alcance find($id, $columns = ['*'])
 * @method cost_liquidacion_alcance first($columns = ['*'])
*/
class cost_liquidacion_alcanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'no_modificaciones_alcance',
        'razon_cambio',
        'alcance_real',
        'programa',
        'no_fianza'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_liquidacion_alcance::class;
    }
}
