<?php

namespace App\Repositories;

use App\Models\cost_proyecto_planifica;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_proyecto_planificaRepository
 * @package App\Repositories
 * @version October 30, 2018, 5:08 am UTC
 *
 * @method cost_proyecto_planifica findWithoutFail($id, $columns = ['*'])
 * @method cost_proyecto_planifica find($id, $columns = ['*'])
 * @method cost_proyecto_planifica first($columns = ['*'])
*/
class cost_proyecto_planificaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'bases_concurso',
        'bases_f_publicacion',
        'bases_f_ultima_mod',
        'no_inconformidades',
        'no_inconformidades_r',
        'oferente',
        'acta_adjudicacion',
        'alcance_contrato',
        'programa_ejecucion',
        'estado_contrato',
        'tipo_no_contrato',
        'empresa_constructora',
        'monto_contrato',
        'f_aprobacion_contrato',
        'nombre_firmante',
        'nombramiento_f',
        'plazo_contrato',
        'f_inicio_contrato',
        'f_autoriza_bitacora',
        'monto_anticipo',
        'no_estima_pagada',
        'monto_estima_pagada',
        'no_pagos_voac',
        'pagos_voac_aprobado',
        'acta_inicio',
        'acta_inicio_f',
        'acta_recepcion',
        'acta_recepcion_f',
        'acta_liquidacion',
        'acta_aprobacion_liquidacion',
        'finiquito_fechas',
        'no_fianza_sos_obra',
        'no_fianza_sos_obra_f',
        'monto_fianza',
        'no_fianza_cumple_contrato',
        'no_fianza_cumple_contrato_f',
        'monto_fianza_cumple',
        'no_fianza_anticipo',
        'no_fianza_anticipo_f',
        'monto_fianza_anticipo',
        'no_fianza_conser_obra',
        'no_fianza_conser_obra_f',
        'monto_fianza_conser_obra',
        'no_fianza_saldo_deudor',
        'no_fianza_saldo_deudor_f',
        'monto_fianza_saldo',
        'entidad_afianzadora',
        'clase_fianza',
        'doc_cambios_contrato',
        'suma_mod_monto',
        'razon_cambio_monto',
        'monto_actualizado_contrato',
        'total_pagos',
        'no_mod_plazo_contrato',
        'cantidad_tiempo_mod',
        'nueva_f_finaliza',
        'razon_cambio_plazo',
        'no_mod_alcance_obra',
        'razon_cambio_alcance',
        'alcance_real_obra',
        'programa_trabajo_actualizado',
        'informe_avance_pago',
        'planos_completos',
        'reporte_eva_au',
        'informe_termina_proyecto',
        'informe_evalua_pro',
        'estado_actual'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_proyecto_planifica::class;
    }
}
