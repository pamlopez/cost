<?php

namespace App\Repositories;

use App\Models\cost_oferente;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class cost_oferenteRepository
 * @package App\Repositories
 * @version April 21, 2019, 3:35 pm UTC
 *
 * @method cost_oferente findWithoutFail($id, $columns = ['*'])
 * @method cost_oferente find($id, $columns = ['*'])
 * @method cost_oferente first($columns = ['*'])
*/
class cost_oferenteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'id_fase',
        'id_participante',
        'monto_ofertado',
        'oferta_presentada'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return cost_oferente::class;
    }
}
