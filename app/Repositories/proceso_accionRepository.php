<?php

namespace App\Repositories;

use App\Models\proceso_accion;
use InfyOm\Generator\Common\BaseRepository;

class proceso_accionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proceso',
        'id_fecha',
        'id_tipo_accion',
        'fh_entrevista_legal',
        'id_abogada_atendio',
        'contenido_estrategia',
        'mds',
        'procesos_ya_inciados',
        'cant_procesos_iniciados',
        'procesos_por_inciar',
        'id_estado',
        'avance',
        'id_asignada_a',
        'id_estado_papeleria',
        'accion',
        'observaciones'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return proceso_accion::class;
    }
}
