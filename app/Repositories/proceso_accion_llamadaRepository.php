<?php

namespace App\Repositories;

use App\Models\proceso_accion_llamada;
use InfyOm\Generator\Common\BaseRepository;

class proceso_accion_llamadaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proceso',
        'id_control_llamada',
        'evolucion_llamada'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return proceso_accion_llamada::class;
    }
}
