<?php

namespace App;

use App\Models\users_asignacion;
use App\Models\users_rol;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

/**
 * App\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $unreadNotifications
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','estado',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getFmtIdCaimuAttribute() {
        $caimu = caimus::find($this->id_caimu);
        if($caimu) {
            return $caimu->descripcion;
        }
        else {
            return "Sin especificar";
        }
    }

    public static function listado_roles($vacio=''){
        $listado= users_rol::orderBy('descripcion','asc')
            ->pluck('descripcion','id_users_rol');
        if(strlen($vacio)>0) {
            $listado->prepend($vacio,0);
        }
        return $listado;

    }

    public static function busca_roles(){
        //  return $query = users_asignacion::where('id_users',Auth::user()->id)->get();
        return $query = users_asignacion::where('id_users',Auth::user()->id)->get()->toArray();
    }

    public static function tiene_rol($rol){

        if ( isset(Auth::user()->id) and Auth::user()->id > 0 ){

            $query = users_asignacion::where('id_users',Auth::user()->id)
                ->where('id_rol',$rol)
                ->get();

            if (count($query)>0){
                return true;
            }else{
                return false;
            }

        } else{

            return false;
        };



    }

    public function rel_rol()
    {
        return $this->hasMany(\App\Models\users_asignacion::class,'id_users','id');
    }

    public  static function listado_usuarios($vacio=""){
        $listado=self::where('id_caimu',Auth::user()->id_caimu)->orderBy('name','asc')
            ->pluck('name','id');
        if(strlen($vacio)>0) {
            $listado->prepend($vacio,0);
        }
        return $listado;

    }




    public static function scopeBusca_coincidencia($query, $request='') {
        // dd($request);
        $texto=str_replace(" ","%",$request);
        if(!empty($request) || isset($request->n1)) {
            $query->Usuario($request->n1)
                ->Sede($request->cs)
                ->orderBy('created_at','desc');
        }else{
            $query->orderBy('created_at','desc');
        }
        return $query->paginate(15);
    }

    //public static  function  EstadoUsuario

    public static function scopeSede($query, $sede=0){
        if($sede >0){
            $query->where('id_caimu',$sede);
        }
    }

    public static function scopeUsuario($query, $usuario=0){
        if(!empty($usuario)){
            $query->where('name','like',"$usuario%")->orderBy('name','asc');//->paginate(10);
        }
    }

    public static function get_all_users(){

        $query =  User::orderBy('name','asc')->with('rel_rol')->paginate(15);
        return $query;
    }

    public function rel_id_rol()
    {
        return $this->belongsTo(\App\Models\users_rol::class, 'id_rol', 'id_users_rol');
    }


    public function getFmtIdRolAttribute()
    {
        if ($this->rel_id_rol) {
            return $this->rel_id_rol->descripcion;
        } else {
            return "-";

        }
    }


}
