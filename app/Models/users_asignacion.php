<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class users_asignacion extends Model
{

    // use SoftDeletes;

    public $table = 'users_asignacion';
    protected $primaryKey='id_users_asignacion';
    public $timestamps=false;

    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';
    //protected $dates = ['deleted_at'];

    public $fillable = [
        'id_users_asignacion',
        'id_rol',
        'id_users',
    ];

    public function rel_rol_asignado(){
        return $this->belongsTo(\App\Models\users_rol::class,'id_users','id_users');
    }

    public function getFmtIdRolAttribute()
    {
        if ($this->id_rol >0) {
            $rol = users_rol::find($this->id_rol);


            return $rol->descripcion;
        } else {
            return "-";
        }
    }





}
