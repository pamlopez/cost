<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class sede
 * @package App\Models
 * @version March 29, 2019, 11:48 pm UTC
 *
 * @property string descripcion
 */
class sede extends Model
{
    public $table = 'sede';
    protected $primaryKey='id_sede';
    public $timestamps=false;

    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';
    //protected $dates = ['deleted_at'];


    public $fillable = [
        'id_sede',
        'descripcion',
    ];

    public static function listado_caimus($vacio=''){
        $listado= sede::orderBy('id_sede','asc')
            ->pluck('descripcion','id_sede');
        if(strlen($vacio)>0) {
            $listado->prepend($vacio,0);
        }
        return $listado;

    }

}
