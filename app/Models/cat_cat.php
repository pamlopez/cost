<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cat_cat
 * @package App\Models
 * @version February 20, 2018, 9:40 pm UTC
 */

/**
 * @property int $id_cat
 * @property string $descripcion
 * @property string $usuario_ingreso
 * @property string $ip_ingreso
 * @property string $fh_ingreso
 * @property string $usuario_ultima_mod
 * @property string $ip_ultima_mod
 * @property string $fh_ultima_mod
 */


class cat_cat extends Model
{
    //use SoftDeletes;

    public $table = 'cat_cat';
    protected $primaryKey='id_cat';
    public $timestamps=false;

    
    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];


    public $fillable = [
        'descripcion',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_cat' => 'integer',
        'descripcion' => 'string',
        'usuario_ingreso' => 'string',
        'ip_ingreso' => 'string',
        'usuario_ultima_mod' => 'string',
        'ip_ultima_mod' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'usuario_ingreso'=>'required',
        //'usuario_ultima_mod'=>'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function rel_cat_item()
    {
        return $this->hasMany(\App\Models\cat_item::class,'id_cat','id_cat');
    }

    // Para babosear al generador
    public function getIdAttribute() {
        return $this->id_cat;
    }

    public static function listado_catalgos($id_item=1) {
        $listado=self::pluck('descripcion','id_cat');
        return $listado;
    }
}
