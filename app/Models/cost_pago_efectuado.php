<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_pago_efectuado
 * @package App\Models
 * @version January 17, 2019, 5:19 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property integer id_proyecto
 * @property integer tipo
 * @property integer no_pago
 * @property date fecha_pago
 * @property float monto_pagado
 * @property integer tiene_ampliacion
 * @property date fecha_aprobacion_ampliacion
 * @property float monto_ampliacion
 * @property string ampliacion_aprobada_por
 * @property string acta_ampliacion
 * @property float monto_acumulado
 * @property string observaciones
 */
class cost_pago_efectuado extends Model
{
   // use SoftDeletes;

    public $table = 'cost_pago_efectuado';
    protected $primaryKey='id_pago_efectuado';
    public $timestamps=false;
    //const CREATED_AT = 'created_at';
   // const UPDATED_AT = 'updated_at';


   // protected $dates = ['deleted_at'];


    public $fillable = [
        'id_proyecto',
        'id_fase',
        'tipo',
        'no_pago',
        'fecha_pago',
        'monto_pagado',
        'tiene_ampliacion',
        'fecha_aprobacion_ampliacion',
        'monto_ampliacion',
        'ampliacion_aprobada_por',
        'acta_ampliacion',
        'monto_acumulado',
        'observaciones'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'id_fase' => 'integer',
        'tipo' => 'integer',
        'no_pago' => 'integer',
        'fecha_pago' => 'date',
        'monto_pagado' => 'float',
        'tiene_ampliacion' => 'integer',
        'fecha_aprobacion_ampliacion' => 'date',
        'monto_ampliacion' => 'float',
        'ampliacion_aprobada_por' => 'string',
        'acta_ampliacion' => 'string',
        'monto_acumulado' => 'float',
        'observaciones' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    public function getFmtTieneAmpliacionAttribute()
    {
        if ($this->tiene_ampliacion== 1){
            $resp = 'Si';
        }else if ($this->tiene_ampliacion== 0){
            $resp = 'No';
        }else{
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtTipoAttribute() {
        if($this->rel_tipo) {
            return $this->rel_tipo->descripcion;
        }
        else {

            return "-";

        }
    }

    public function rel_tipo(){
        return $this->belongsTo(\App\Models\cat_item::class,'tipo','id_item');

    }

    public function setTipoAttribute($tipo){

        $this->attributes[ 'tipo' ] = $tipo > 0 ? $tipo : 0;
    }
    public function setNoPagoAttribute($no_pago){

        $this->attributes[ 'no_pago' ] = $no_pago > 0 ? $no_pago : 0;
    }

    public function setFechaPagoAttribute($fecha_pago){

        $this->attributes[ 'fecha_pago' ] = strlen($fecha_pago) > 0 ? $fecha_pago : Null;
    }
    public function setMontoPagadoAttribute($monto_pagado){

        $this->attributes[ 'monto_pagado' ] = $monto_pagado > 0 ? $monto_pagado : 0 ;
    }
    public function setTieneAmpliacionAttribute($tiene_ampliacion){

        $this->attributes[ 'tiene_ampliacion' ] = $tiene_ampliacion > 0 ? $tiene_ampliacion : 0;
    }

    public function setFechaAprobacionAmpliacionAttribute($fecha_aprobacion_ampliacion){

        $this->attributes[ 'fecha_aprobacion_ampliacion' ] = strlen($fecha_aprobacion_ampliacion) > 0 ? $fecha_aprobacion_ampliacion : Null;
    }

    public function setMontoAmpliacionAttribute($monto_ampliacion){

        $this->attributes[ 'monto_ampliacion' ] = $monto_ampliacion > 0 ? $monto_ampliacion : 0;
    }

    public function setAmpliacionAprobadaPorAttribute($ampliacion_aprobada_por){

        $this->attributes[ 'ampliacion_aprobada_por' ] = strlen($ampliacion_aprobada_por)> 0 ? $ampliacion_aprobada_por : Null;
    }
    public function setActaAmpliacionAttribute($acta_ampliacion){

        $this->attributes[ 'acta_ampliacion' ] = strlen($acta_ampliacion)> 0 ? $acta_ampliacion : Null;
    }
    public function setMontoAcumuladoAttribute($monto_acumulado){

        $this->attributes[ 'monto_acumulado' ] = $monto_acumulado > 0 ? $monto_acumulado : 0;
    }
    public function setObservacionesAttribute($observaciones){

        $this->attributes[ 'observaciones' ] = strlen($observaciones)> 0 ? $observaciones : Null;
    }

    public function getIdAttribute() {
        return $this->id_pago_efectuado;
    }
    public static function pago_e($id_proyecto, $id_fase){
        $id_pe= Self::where('id_proyecto',$id_proyecto)->where('id_fase',$id_fase)->get();
        return $id_pe;
    }
    
}
