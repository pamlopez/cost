<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * @property int $id_geo
 * @property int $id_padre
 * @property string $nombre
 * @property int $nivel
 * @property string $codigo
 */


class geo extends Model
{
    public $table = 'geo';
    protected $primaryKey='id_geo';
    public $timestamps=false;

    /**
     * Para llenar un control de la forma mas simple
     * @param null $id_padre
     * @return mixed
     */
    public static function listar_hijos($id_padre=null, $vacio="") {

        if($id_padre>0) {
            $opciones=self::where('id_padre',$id_padre)->where('habilitado',0)->orderby('nombre')->pluck('nombre','id_geo')->toArray();
        }
        else {
            $opciones=self::whereNull('id_padre')->where('habilitado',0)->orderby('nombre')->pluck('nombre','id_geo')->toArray();
        }

        if(strlen($vacio)>0) {
            $opciones = [0=>$vacio] + $opciones;
        }


        return $opciones;
    }

    /**
     * PAra el JSON del control dependiente
     */
    public static function json_select($id_padre=null,$elegido=null, $vacio="") {
        //$id_padre=$request->depdrop_parents[0];
        $listado = self::listar_hijos($id_padre);


        //preparar arreglo de acuerdo a http://plugins.krajee.com/dependent-dropdown/demo

        $nuevo=array();
        if(strlen($vacio)>0) {
            $nuevo[]=array('id'=>-1,'name'=>$vacio);
            //$listado[-1]=$vacio;
        }
        foreach($listado as $key=>$value) {
            $nuevo[]=array('id'=>$key,'name'=>$value);
        }

        $json['output']=$nuevo;

        //si viene un predeterminado, ver que exista y agregarlo
        if(array_key_exists($elegido,$listado)) {
            $json['selected']=$elegido;
        }
        elseif(strlen($vacio)>0) {
            //$json['selected']=0;
        }

        return $json;
    }
}
