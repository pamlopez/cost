<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class documento
 * @package App\Models
 * @version July 3, 2019, 4:01 am UTC
 *
 * @property integer id_proyecto
 * @property integer id_fase
 * @property integer tipo
 * @property string url
 * @property string documento
 * @property string titulo
 * @property string|\Carbon\Carbon fh_publicacion
 * @property string|\Carbon\Carbon fh_modificacion
 * @property string formato
 * @property string lenguaje
 * @property integer pagina_inicio
 * @property integer pagina_fin
 * @property string detalle_acceso
 * @property string autor
 */
class documento extends Model
{
    //use SoftDeletes;

    public $table = 'documento';
    protected $primaryKey='id_documento';
    public $timestamps=false;
    
  //  const CREATED_AT = 'created_at';
  //  const UPDATED_AT = 'updated_at';


 //   protected $dates = ['deleted_at'];


    public $fillable = [
        'id_proyecto',
        'id_fase',
        'tipo',
        'url',
        'documento',
        'titulo',
        'fh_publicacion',
        'fh_modificacion',
        'formato',
        'lenguaje',
        'pagina_inicio',
        'pagina_fin',
        'detalle_acceso',
        'autor'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'id_fase' => 'integer',
        'tipo' => 'integer',
        'url' => 'string',
        'documento' => 'string',
        'titulo' => 'string',
        'formato' => 'string',
        'lenguaje' => 'string',
        'pagina_inicio' => 'integer',
        'pagina_fin' => 'integer',
        'detalle_acceso' => 'string',
        'autor' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function setTipoAttribute($tipo)
    {
        $this->attributes[ 'tipo' ] = strlen($tipo) > 0 ? $tipo : null;
    }

    public function setUrlAttribute($url)
    {
        $this->attributes[ 'url' ] = strlen($url) > 0 ? $url : null;
    }

    public function setDocumentoAttribute($documento)
    {
        $this->attributes[ 'documento' ] = strlen($documento) > 0 ? $documento : null;
    }

    public function setTituloAttribute($titulo)
    {
        $this->attributes[ 'titulo' ] = strlen($titulo) > 0 ? $titulo : null;
    }

    public function setFormatoAttribute($formato)
    {
        $this->attributes[ 'formato' ] = strlen($formato) > 0 ? $formato : null;
    }

    public function setLenguajeAttribute($lenguaje)
    {
    $this->attributes[ 'lenguaje' ] = strlen($lenguaje) > 0 ? $lenguaje : null;
    }

    public function setPaginaInicioAttribute($pagina_inicio)
    {
        $this->attributes[ 'pagina_inicio' ] = strlen($pagina_inicio) > 0 ? $pagina_inicio : null;
    }

    public function setPaginaFinAttribute($pagina_fin)
    {
        $this->attributes[ 'pagina_fin' ] = strlen($pagina_fin) > 0 ? $pagina_fin : null;
    }


    public function setDetalleAccesoAttribute($detalle_acceso)
    {
        $this->attributes[ 'detalle_acceso' ] = strlen($detalle_acceso) > 0 ? $detalle_acceso : null;
    }

    public function setAutorAttribute($autor)
    {
        $this->attributes[ 'autor' ] = strlen($autor) > 0 ? $autor : null;
    }

    public function getFmtFormatoAttribute()
    {
        if ($this->rel_formato) {
            return $this->rel_formato->descripcion;
        } else {
            return "";
        }
    }

    public function rel_formato()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'formato', 'id_item');
    }


    public function getFmtLenguajeAttribute()
    {
        if ($this->rel_lenguaje) {
            return $this->rel_lenguaje->descripcion;
        } else {
            return "";
        }
    }

    public function rel_lenguaje()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'lenguaje', 'id_item');
    }

    public function getIdAttribute()
    {
        return $this->id_documento;
    }

    public function getFmtTipoAttribute()
    {
        if ($this->rel_tipo) {
            return $this->rel_tipo->descripcion;
        } else {
            return "";
        }
    }

    public function rel_tipo(){
        return $this->belongsTo(\App\Models\cat_item::class,'tipo','id_item');

    }

    public static function documento_t($id_proyecto, $id_fase)
    {
        $id_a = Self::where('id_proyecto', $id_proyecto)->where('id_fase', $id_fase)->get();
        return $id_a;
    }

    
}
