<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class descarga_responsable
 * @package App\Models
 * @version March 3, 2019, 4:57 pm UTC
 *
 * @property string nombre_completo
 * @property string no_identificacion_personal
 * @property string descripcion
 * @property string|\Carbon\Carbon fh_descarga
 * @property integer id_proyecto
 */
class descarga_responsable extends Model
{
  //  use SoftDeletes;

    public $table = 'descarga_responsable';
    protected $primaryKey='id_descarga_responsable';
    public $timestamps=false;
    
    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre_completo',
        'no_identificacion_personal',
        'descripcion',
        'fh_descarga',
        'id_proyecto'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_descarga_responsable' => 'integer',
        'nombre_completo' => 'string',
        'no_identificacion_personal' => 'string',
        'descripcion' => 'string',
        'id_proyecto' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    public function getIdAttribute() {
        return $this->id_descarga_responsable;
    }
    
}
