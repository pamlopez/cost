<?php

namespace App\Models;
use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_info_contrato
 * @package App\Models
 * @version January 17, 2019, 5:18 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property integer id_proyecto
 * @property integer tipo
 * @property string oferentes
 * @property string estado_contrado
 * @property string plazo_contrato
 * @property string tipo_numero_contrato
 * @property date fecha_inicio
 * @property string empresa_constructora
 * @property string objeto_contrato
 * @property integer no_precalificado
 * @property date fecha_actualizacion
 * @property float precio_contrato
 * @property date fecha_contrato
 * @property string aprobacion_contrato
 * @property date fecha_aprobacion_contrato
 * @property string nombre_firma
 * @property string cargo_firma
 * @property string nombramiento
 * @property date fecha_nombramiento
 * @property string conclusion
 */
class cost_info_contrato extends Model {

    // use SoftDeletes;

    public $table = 'cost_info_contrato';

    protected $primaryKey = 'id_info_contrato';

    public $timestamps = false;

    // const CREATED_AT = 'created_at';
    public $fillable = [
        'id_proyecto',
        'tipo',
        'id_fase',
        'oferentes',
        'nombre_responsable_seleccionado',
        'estado_contrato',
        'plazo_contrato',
        'tipo_numero_contrato',
        'fecha_inicio',
        'empresa_constructora',
        'objeto_contrato',
        'no_precalificado',
        'fecha_actualizacion',
        'precio_contrato',
        'fecha_contrato',
        'aprobacion_contrato',
        'fecha_aprobacion_contrato',
        'nombre_firma',
        'cargo_firma',
        'nombramiento',
        'fecha_nombramiento',
        'conclusion',
        'modalidad_contratacion',
        'modalidad_contratacion_d',
        'bases_concurso',
        'bases_concurso_link',
        'fecha_publicacion',
        'fecha_ultima_mod_gc',
        'total_inconformidades',
        'total_inconformidades_r',
        'alcance',
        'programa',
        'nombres_suscriben',
        'fecha_autoriza_bitacora',
        'quien_autoriza_bitacora',
        'clausula_sobrecosto',
        'fecha_inicio_ejecucion',
        'estado_contrato_oc',
        'estado_contrato_oc_d',

        'costo_estimado_moneda',
        'costo_estimado',
        'fecha_fin_ejecucion',
        'fecha_inicio_ejecucion',
        'metodo_procuracion',
        'detalle_procuracion',
        'precio_contrato_final',
        'moneda_final',

    ];

    // const UPDATED_AT = 'updated_at';

    // protected $dates = ['deleted_at'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto'               => 'integer',
        'tipo'                      => 'integer',
        'id_fase'                   => 'integer',
        'oferentes'                 => 'string',
        'estado_contrato'           => 'string',
        'plazo_contrato'            => 'string',
        'tipo_numero_contrato'      => 'string',
        'empresa_constructora'      => 'string',
        'objeto_contrato'           => 'string',
        'no_precalificado'          => 'integer',
        'precio_contrato'           => 'float',
        'aprobacion_contrato'       => 'string',
        'nombre_firma'              => 'string',
        'cargo_firma'               => 'string',
        'nombramiento'              => 'string',
        'fecha_nombramiento'        => 'date',
        'fecha_inicio'              => 'date',
        'fecha_actualizacion'       => 'date',
        'fecha_contrato'            => 'date',
        'fecha_aprobacion_contrato' => 'date',
        'fecha_publicacion'         => 'date',
        'fecha_autoriza_bitacora'   => 'date',
        'fecha_ultima_mod_gc'       => 'date',
        'fecha_inicio_ejecucion'    => 'date',
        'conclusion'                => 'string',
        'total_inconformidades'   => 'integer',
        'total_inconformidades_r' => 'integer',
    ];

    public static $rules = [

    ];

    public function getIdAttribute()
    {
        return $this->id_info_contrato;
    }

    public function setOferentesAttribute($oferentes)
    {
        $this->attributes[ 'oferentes' ] = strlen($oferentes) > 0 ? $oferentes : null;
    }

    public function setNombreResponsableSeleccionadoAttribute($nombre_responsable_seleccionado)
    {
        $this->attributes[ 'nombre_responsable_seleccionado' ] = strlen($nombre_responsable_seleccionado) > 0 ? $nombre_responsable_seleccionado : null;
    }

    public function setEstadoContratoAttribute($estado_contrato)
    {
        $this->attributes[ 'estado_contrato' ] = $estado_contrato > 0 ? $estado_contrato : null;
    }

    public function setPlazoContratoAttribute($plazo_contrato)
    {
        $this->attributes[ 'plazo_contrato' ] = strlen($plazo_contrato) > 0 ? $plazo_contrato : null;
    }

    public function setTipoNumeroContratoAttribute($tipo_numero_contrato)
    {
        $this->attributes[ 'tipo_numero_contrato' ] = strlen($tipo_numero_contrato) > 0 ? $tipo_numero_contrato : null;
    }

    public function setFechaInicioAttribute($fecha_inicio)
    {
        $this->attributes[ 'fecha_inicio' ] = strlen($fecha_inicio) > 0 ? $fecha_inicio : null;
    }

    public function setEmpresaConstructoraAttribute($empresa_constructora)
    {
        $this->attributes[ 'empresa_constructora' ] = strlen($empresa_constructora) > 0 ? $empresa_constructora : null;
    }

    public function setObjetoContratoAttribute($objeto_contrato)
    {
        $this->attributes[ 'objeto_contrato' ] = strlen($objeto_contrato) > 0 ? $objeto_contrato : null;
    }

    public function setNoPrecalificadoAttribute($no_precalificado)
    {
        $this->attributes[ 'no_precalificado' ] = strlen($no_precalificado) > 0 ? $no_precalificado : null;
    }

    public function setFechaActualizacionAttribute($fecha_actualizacion)
    {
        $this->attributes[ 'fecha_actualizacion' ] = strlen($fecha_actualizacion) > 0 ? $fecha_actualizacion : null;
    }

    public function setPrecioContratoAttribute($precio_contrato)
    {
        $this->attributes[ 'precio_contrato' ] = strlen($precio_contrato) > 0 ? $precio_contrato : null;
    }

    public function setFechaContratoAttribute($fecha_contrato)
    {
        $this->attributes[ 'fecha_contrato' ] = strlen($fecha_contrato) > 0 ? $fecha_contrato : null;
    }

    public function setAprobacionContratoAttribute($aprobacion_contrato)
    {
        $this->attributes[ 'aprobacion_contrato' ] = strlen($aprobacion_contrato) > 0 ? $aprobacion_contrato : null;
    }

    public function setFechaAprobacionContratoAttribute($fecha_aprobacion_contrato)
    {
        $this->attributes[ 'fecha_aprobacion_contrato' ] = strlen($fecha_aprobacion_contrato) > 0 ? $fecha_aprobacion_contrato : null;
    }

    public function setNombreFirmaAttribute($nombre_firma)
    {
        $this->attributes[ 'nombre_firma' ] = strlen($nombre_firma) > 0 ? $nombre_firma : null;
    }

    public function setCargoFirmaAttribute($cargo_firma)
    {
        $this->attributes[ 'cargo_firma' ] = strlen($cargo_firma) > 0 ? $cargo_firma : null;
    }

    public function setNombramientoAttribute($nombramiento)
    {
        $this->attributes[ 'nombramiento' ] = strlen($nombramiento) > 0 ? $nombramiento : null;
    }

    public function setFechaNombramientoAttribute($fecha_nombramiento)
    {
        $this->attributes[ 'fecha_nombramiento' ] = strlen($fecha_nombramiento) > 0 ? $fecha_nombramiento : null;
    }

    public function setConclusionAttribute($conclusion)
    {
        $this->attributes[ 'conclusion' ] = strlen($conclusion) > 0 ? $conclusion : null;
    }

    public function setModalidadContratacionAttribute($modalidad_contratacion)
    {
        $this->attributes[ 'modalidad_contratacion' ] = strlen($modalidad_contratacion) > 0 ? $modalidad_contratacion : null;
    }

    public function setModalidadContratacionDAttribute($modalidad_contratacion_d)
    {
        $this->attributes[ 'modalidad_contratacion_d' ] = strlen($modalidad_contratacion_d) > 0 ? $modalidad_contratacion_d : null;
    }

    public function setBasesConcursoAttribute($bases_concurso)
    {
        $this->attributes[ 'bases_concurso' ] = strlen($bases_concurso) > 0 ? $bases_concurso : null;
    }

    public function setBasesConcursoLinkAttribute($bases_concurso_link)
    {
        $this->attributes[ 'bases_concurso_link' ] = strlen($bases_concurso_link) > 0 ? $bases_concurso_link : null;
    }

    public function setFechaPublicacionAttribute($fecha_publicacion)
    {
        $this->attributes[ 'fecha_publicacion' ] = strlen($fecha_publicacion) > 0 ? $fecha_publicacion : null;
    }

    public function setFechaUltimaModGcAttribute($fecha_ultima_mod_gc)
    {
        $this->attributes[ 'fecha_ultima_mod_gc' ] = strlen($fecha_ultima_mod_gc) > 0 ? $fecha_ultima_mod_gc : null;
    }

    public function setTotalInconformidadesAttribute($total_inconformidades)
    {
        $this->attributes[ 'total_inconformidades' ] = strlen($total_inconformidades) > 0 ? $total_inconformidades : null;
    }

    public function setTotalInconformidadesRAttribute($total_inconformidades_r)
    {
        $this->attributes[ 'total_inconformidades_r' ] = strlen($total_inconformidades_r) > 0 ? $total_inconformidades_r : null;
    }

    public function setAlcanceAttribute($alcance)
    {
        $this->attributes[ 'alcance' ] = strlen($alcance) > 0 ? $alcance : null;
    }

    public function setProgramaAttribute($programa)
    {
        $this->attributes[ 'programa' ] = strlen($programa) > 0 ? $programa : null;
    }

    public function setNombresSuscribenAttribute($nombres_suscriben)
    {
        $this->attributes[ 'nombres_suscriben' ] = strlen($nombres_suscriben) > 0 ? $nombres_suscriben : null;
    }

    public function setFechaAutorizaBitacoraAttribute($fecha_autoriza_bitacora)
    {
        $this->attributes[ 'fecha_autoriza_bitacora' ] = strlen($fecha_autoriza_bitacora) > 0 ? $fecha_autoriza_bitacora : null;
    }

    public function setQuienAutorizaBitacoraAttribute($quien_autoriza_bitacora)
    {
        $this->attributes[ 'quien_autoriza_bitacora' ] = strlen($quien_autoriza_bitacora) > 0 ? $quien_autoriza_bitacora : null;
    }

    public function setClausulaSobrecostoAttribute($clausula_sobrecosto)
    {
        $this->attributes[ 'clausula_sobrecosto' ] = strlen($clausula_sobrecosto) > 0 ? $clausula_sobrecosto : null;
    }

    public function setFechaInicioEjecucionAttribute($fecha_inicio_ejecucion)
    {
        $this->attributes[ 'fecha_inicio_ejecucion' ] = strlen($fecha_inicio_ejecucion) > 0 ? $fecha_inicio_ejecucion : null;
    }

    public function getFmtModalidadContratacionAttribute()
    {
        if ($this->rel_modalidad_contratacion) {
            return $this->rel_modalidad_contratacion->descripcion;
        } else {

            if (strlen($this->modalidad_contratacion_d) > 0)
                return $this->modalidad_contratacion_d;
            else
                return "-";
        }
    }

    public function getFmtFechaPublicacionAttribute()
    {
        if ($this->fecha_publicacion != null && $this->fecha_publicacion->year > 0 )
            return $this->fecha_publicacion->format('d-m-Y');
        return null;
    }
    public function getFmtFechaUltimaModGcAttribute()
    {
        if ($this->fecha_ultima_mod_gc != null && $this->fecha_ultima_mod_gc->year > 0 )
            return $this->fecha_ultima_mod_gc->format('d-m-Y');
        return null;
    }
    public function getFmtFechaInicioAttribute()
    {
        if ($this->fecha_inicio != null && $this->fecha_inicio->year >0)
            return $this->fecha_inicio->format('d-m-Y');
        return null;

    }
    public function getFmtFechaAprobacionContratoAttribute()
    {
        if ($this->fecha_aprobacion_contrato != null && $this->fecha_aprobacion_contrato->year >0)
            return $this->fecha_aprobacion_contrato->format('d-m-Y');
        return null;

    }

    public function getFmtFechaNombramientoAttribute()
    {
        if ($this->fecha_nombramiento != null && $this->fecha_nombramiento->year >0)
            return $this->fecha_nombramiento->format('d-m-Y');
        return null;

    }

    public function getFmtFechaContratoAttribute()
    {
        if ($this->fecha_contrato != null && $this->fecha_contrato->year >0)
            return $this->fecha_contrato->format('d-m-Y');
        return null;

    }
    public function getFmtFechaInicioEjecucionAttribute()
    {
        if ($this->fecha_inicio_ejecucion != null && $this->fecha_inicio_ejecucion->year >0)
            return $this->fecha_inicio_ejecucion->format('d-m-Y');
        return null;

    }
    public function getFmtFechaAutorizaBitacoraAttribute()
    {
        if ($this->fecha_autoriza_bitacora != null && $this->fecha_autoriza_bitacora->year >0)
            return $this->fecha_autoriza_bitacora->format('d-m-Y');
        return null;

    }


    public function rel_modalidad_contratacion()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'modalidad_contratacion', 'id_item');
    }

    public function getFmtEstadoContratoAttribute()
    {
        if ($this->rel_estado_contrato) {
            return $this->rel_estado_contrato->descripcion;
        } else {
                return "-";
        }
    }

    public function rel_estado_contrato()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'estado_contrato', 'id_item');
    }


    public static function info_c($id_proyecto, $id_fase)
    {
        $id_ic = Self::where('id_proyecto', $id_proyecto)->where('id_fase', $id_fase)->get();
        return $id_ic;
    }

    public function rel_proyecto()
    {
        return $this->belongsTo(\App\Models\cost_proyecto::class, 'id_proyecto', 'id_proyecto');
    }
}
