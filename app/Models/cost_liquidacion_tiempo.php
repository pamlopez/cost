<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_liquidacion_tiempo
 * @package App\Models
 * @version March 25, 2019, 5:05 am UTC
 *
 * @property integer id_liquidacion
 * @property integer no_modificaciones_plazo
 * @property string cantidad_tiempo_modificado
 * @property date nueva_fecha_finaliza
 * @property string razones_cambio_plazo
 * @property string no_fianza
 */
class cost_liquidacion_tiempo extends Model {

    // use SoftDeletes;

    public $table = 'cost_liquidacion_tiempo';

    protected $primaryKey = 'id_liquidacion_tiempo';

    public $timestamps = false;

    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';

    protected $dates = [ 'deleted_at' ];

    public $fillable = [
        'id_proyecto',
        'no_modificaciones_plazo',
        'cantidad_tiempo_modificado',
        'nueva_fecha_finaliza',
        'razones_cambio_plazo',
        'no_fianza',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto'                => 'integer',
        'no_modificaciones_plazo'    => 'integer',
        'cantidad_tiempo_modificado' => 'string',
        'nueva_fecha_finaliza'       => 'date',
        'razones_cambio_plazo'       => 'string',
        'no_fianza'                  => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function setDocumentoCambioAttribute($documento_cambio)
    {
        $this->attributes[ 'documento_cambio' ] = strlen($documento_cambio) > 0 ? $documento_cambio : null;
    }

    public function setSumaModAttribute($suma_mod)
    {
        $this->attributes[ 'suma_mod' ] = strlen($suma_mod) > 0 ? $suma_mod : null;
    }

    public function setRazonCambioAttribute($razon_cambio)
    {
        $this->attributes[ 'razon_cambio' ] = strlen($razon_cambio) > 0 ? $razon_cambio : null;
    }

    public function setMontoActualizadoAttribute($monto_actualizado)
    {
        $this->attributes[ 'monto_actualizado' ] = strlen($monto_actualizado) > 0 ? $monto_actualizado : null;
    }

    public function setNoFianzaAttribute($no_fianza)
    {
        $this->attributes[ 'no_fianza' ] = strlen($no_fianza) > 0 ? $no_fianza : null;
    }

    public function getIdAttribute()
    {
        return $this->id_liquidacion_tiempo;
    }

    public function getFmtNuevaFechaFinalizaAttribute()
    {

        if ($this->nueva_fecha_finaliza != null && $this->nueva_fecha_finaliza->year > 0)
            return $this->nueva_fecha_finaliza->format('d-m-Y');
        return null;
    }

    public static function tiempo($id_proyecto)
    {
        $id_ic = Self::where('id_proyecto', $id_proyecto)->get();
        return $id_ic;
    }

}
