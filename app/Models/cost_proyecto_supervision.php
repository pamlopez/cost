<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_proyecto_supervision
 * @package App\Models
 * @version October 30, 2018, 5:09 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property integer id_proyecto
 * @property string sup_invitacion
 * @property string sup_listado_ofe
 * @property string sup_acta_adj
 * @property string sup_responsable
 * @property float sup_monto_contratado
 * @property string sup_plazo_contrato
 * @property string sup_tipo_contrato
 * @property string sup_contrato_alcance
 * @property integer sup_programa_trabajo
 * @property date sup_f_inicio
 * @property date sup_f_entrega
 * @property string sup_responsable_ea
 * @property date sup_f_aprobacion_contrato
 * @property integer sup_anticipo
 * @property integer sup_no_pagos
 * @property string sup_fechas
 * @property string sup_mod_plazo
 * @property string resp_mod_monto
 * @property date nueva_f_finaliza
 * @property float sup_mod_monto
 * @property string sup_resp_mod_monto
 * @property float sup_porcentaje
 * @property string sup_acta_recepcion
 * @property date sup_acta_recepcion_f
 * @property date sup_finiquito_f
 * @property string fianza_sos_oferta
 * @property string fianza_cumplimiento
 * @property string sup_afianzadora
 */
class cost_proyecto_supervision extends Model
{
   // use SoftDeletes;

    public $table = 'cost_proyecto_supervision';
    protected $primaryKey='id_proyecto_supervision';
    
  //  const CREATED_AT = 'created_at';
  //  const UPDATED_AT = 'updated_at';


  //  protected $dates = ['deleted_at'];


    public $fillable = [
        'id_proyecto',
        'sup_invitacion',
        'sup_listado_ofe',
        'sup_acta_adj',
        'sup_responsable',
        'sup_monto_contratado',
        'sup_plazo_contrato',
        'sup_tipo_contrato',
        'sup_contrato_alcance',
        'sup_programa_trabajo',
        'sup_f_inicio',
        'sup_f_entrega',
        'sup_responsable_ea',
        'sup_f_aprobacion_contrato',
        'sup_anticipo',
        'sup_no_pagos',
        'sup_fechas',
        'sup_mod_plazo',
        'resp_mod_monto',
        'nueva_f_finaliza',
        'sup_mod_monto',
        'sup_resp_mod_monto',
        'sup_porcentaje',
        'sup_acta_recepcion',
        'sup_acta_recepcion_f',
        'sup_finiquito_f',
        'fianza_sos_oferta',
        'fianza_cumplimiento',
        'sup_afianzadora'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'sup_invitacion' => 'string',
        'sup_listado_ofe' => 'string',
        'sup_acta_adj' => 'string',
        'sup_responsable' => 'string',
        'sup_monto_contratado' => 'float',
        'sup_plazo_contrato' => 'string',
        'sup_tipo_contrato' => 'string',
        'sup_contrato_alcance' => 'string',
        'sup_programa_trabajo' => 'integer',
        'sup_f_inicio' => 'date',
        'sup_f_entrega' => 'date',
        'sup_responsable_ea' => 'string',
        'sup_f_aprobacion_contrato' => 'date',
        'sup_anticipo' => 'integer',
        'sup_no_pagos' => 'integer',
        'sup_fechas' => 'string',
        'sup_mod_plazo' => 'string',
        'resp_mod_monto' => 'string',
        'nueva_f_finaliza' => 'date',
        'sup_mod_monto' => 'float',
        'sup_resp_mod_monto' => 'string',
        'sup_porcentaje' => 'float',
        'sup_acta_recepcion' => 'string',
        'sup_acta_recepcion_f' => 'date',
        'sup_finiquito_f' => 'date',
        'fianza_sos_oferta' => 'string',
        'fianza_cumplimiento' => 'string',
        'sup_afianzadora' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getIdAttribute() {
        return $this->id_proyecto_supervision;
    }

    public function rel_proyecto()
    {
        return $this->belongsTo(\App\Models\cost_proyecto::class,'id_proyecto','id_proyecto');
    }
}
