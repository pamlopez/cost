<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_estimacion
 * @package App\Models
 * @version April 21, 2019, 2:26 pm UTC
 *
 * @property integer id_proyecto
 * @property integer tipo_estimacion
 * @property integer numero_pagos
 * @property float monto_total_pagos
 */
class cost_estimacion extends Model
{


    public $table = 'cost_estimacion';
    protected $primaryKey='id_estimacion';
    public $timestamps=false;


    public $fillable = [
        'id_proyecto',
        'tipo_estimacion',
        'numero_pagos',
        'monto_total_pagos'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'tipo_estimacion' => 'integer',
        'numero_pagos' => 'integer',
        'monto_total_pagos' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    // Para babosear al generador
    public function getIdAttribute() {
        return $this->id_estimacion;
    }
    public function getFmtTipoEstimacionAttribute()
    {
       return ($this->attributes['tipo_estimacion']== 1 )? "Estimación Aprobada":"Estimación Pagada";
    }

        public function setNumeroPagosAttribute($numero_pagos){ $this->attributes['numero_pagos'] =  strlen($numero_pagos) >0 ? $numero_pagos : null;}
        public function setTotalPagosAttribute($total_pagos){ $this->attributes['total_pagos'] =  strlen($total_pagos) >0 ? $total_pagos : null;}


    public static function estimacion_p($id_proyecto, $tipo_estimacion){
        $id_fp= Self::where('id_proyecto',$id_proyecto)->where('tipo_estimacion',$tipo_estimacion)->get();
        return $id_fp;
    }
    
}
