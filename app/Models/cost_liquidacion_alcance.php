<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_liquidacion_alcance
 * @package App\Models
 * @version March 25, 2019, 5:04 am UTC
 *
 * @property integer id_liquidacion
 * @property integer no_modificaciones_alcance
 * @property string razon_cambio
 * @property string alcance_real
 * @property string programa
 * @property string no_fianza
 */
class cost_liquidacion_alcance extends Model
{
   // use SoftDeletes;

    public $table = 'cost_liquidacion_alcance';
    protected $primaryKey='id_liquidacion_alcance';
    public $timestamps=false;

    
   // const CREATED_AT = 'created_at';
   // const UPDATED_AT = 'updated_at';


  //  protected $dates = ['deleted_at'];


    public $fillable = [
        'id_proyecto',
        'no_modificaciones_alcance',
        'razon_cambio',
        'alcance_real',
        'programa',
        'no_fianza',
        'fecha_cambio'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'no_modificaciones_alcance' => 'integer',
        'razon_cambio' => 'string',
        'alcance_real' => 'string',
        'programa' => 'string',
        'no_fianza' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

        public function setNoModificacionesAlcanceAttribute($no_modificaciones_alcance){ $this->attributes['no_modificaciones_alcance']= strlen($no_modificaciones_alcance)>0 ? $no_modificaciones_alcance  : null;}
        public function setRazonCambioAttribute($razon_cambio){ $this->attributes['razon_cambio']= strlen($razon_cambio)>0 ? $razon_cambio  : null;}
        public function setAlcanceRealAttribute($alcance_real){ $this->attributes['alcance_real']= strlen($alcance_real)>0 ? $alcance_real  : null;}
        public function setProgramaAttribute($programa){ $this->attributes['programa']= strlen($programa)>0 ? $programa  : null;}
        public function setNoFianzaAttribute($no_fianza){ $this->attributes['no_fianza']= strlen($no_fianza)>0 ? $no_fianza  : null;}


    
    



    public function getIdAttribute() {
        return $this->id_liquidacion_alcance;
    }
    public static function alcance($id_proyecto){
        $id_ic= Self::where('id_proyecto',$id_proyecto)->get();
        return $id_ic;
    }


    public function getFmtFechaCambioAttribute()
    {
        $fecha = Carbon::parse($this->fecha_cambio)->format('d/m/Y');
        return $fecha;
    }
}
