<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Null_;

/**
 * Class cost_oferente
 * @package App\Models
 * @version April 21, 2019, 3:35 pm UTC
 *
 * @property integer id_participante
 * @property float monto_ofertado
 * @property string oferta_presentada
 */
class cost_oferente extends Model
{
  //  use SoftDeletes;


    public $table = 'cost_oferente';
    protected $primaryKey='id_oferente';
    public $timestamps=false;
    
   // const CREATED_AT = 'created_at';
   // const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];


    public $fillable = [
        'id_proyecto',
        'id_fase',
        'id_participante',
        'monto_ofertado',
        'oferta_presentada',
        'rol'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'id_fase' => 'integer',
        'id_participante' => 'integer',
        'monto_ofertado' => 'float',
        'oferta_presentada' => 'string',
        'rol' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


    public function setMontoOfertadoAttribute($monto_ofertado)
    {
        $this->attributes[ 'monto_ofertado' ] = strlen($monto_ofertado) > 0 ? $monto_ofertado : 0;
    }

    public function setOfertaPresentadaAttribute($oferta_presentada)
    {
        $this->attributes[ 'oferta_presentada' ] = strlen($oferta_presentada) > 0 ? $oferta_presentada : Null;
    }

    public function getIdAttribute() {
        return $this->id_oferente;
    }

    public function getFmtParticipanteAttribute()
   {
        if(isset($this->id_participante)){

            $fmt_participante = $this->rel_participante->nombre_comun;
            return $fmt_participante;
        }
    }

    public function rel_participante()
    {
        $test = $this->hasOne(\App\Models\participante::class,'id_participante','id_participante');
        return $test;
    }

    public static function oferente_p($id_proyecto, $id_fase){
        $id_pe= Self::where('id_proyecto',$id_proyecto)->where('id_fase',$id_fase)->get();
        return $id_pe;
    }
    public static function seleccionados_roles($id,$fase)
    {
        $listado = cost_oferente::select(DB::raw('rol'))
            ->where('id_proyecto', $id)
            ->where('id_fase',$fase)
            ->get();

        $new_listado = [];
        foreach ($listado as $v => $k) {
            $id_item = cat_item::where('otro',$k->rol)->where('id_cat',84)->first();
            $new_listado[$id_item->id_item] = $id_item->id_item;
        }

        return $new_listado;
    }
}
