<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * @property int $id_criterio_fijo
 * @property int $id_opcion
 * @property string $descripcion
 * @property int $orden
 * @property int $estado
 */


class criterio_fijo extends Model
{
    public $table = 'criterio_fijo';
    protected $primaryKey='id_criterio_fijo';
    public $timestamps=false;

    public static function listado_items($id_grupo=1,$vacio='') {
        $listado=self::where('id_criterio_fijo',$id_grupo)
            ->orderby('orden')
            ->orderby('descripcion')
            ->pluck('descripcion','id_opcion');
        if(strlen($vacio)>0) {
            $listado->prepend($vacio,0);
        }
        return $listado->toArray();
    }
}
