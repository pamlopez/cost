<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class indicadores extends Model
{
    #GENERADORES DE GRAFICAS
    public static function g_pie($datos,$titulo="",$nombre_serie="",$subtitulo="") {
        //Asignar datos
        $pie=array();
        foreach($datos as $rodaja) {

            $item['name']=$rodaja['txt'];
            $item['y']=$rodaja['valor'];
            $pie[]=$item;
        }

        if(!empty($nombre_serie)) {
            $series[0]['name']=$nombre_serie;
        }
        $series[0]['data']=$pie;



        // Configurar grafica
        $grafico=array();
        //Asignacion de datos
        $grafico['series']=$series;
        //Aspectos de forma
        $grafico['chart']['type']='pie';
        if(!empty($titulo)) {
            $grafico['title']['text']=$titulo;
        }
        if(!empty($subtitulo)) {
            $grafico['subtitle']['text']=$subtitulo;
        }
        $grafico['credits']['enabled']=false;
        $grafico['exporting']['filename']="conflicto_estado";
        $grafico['exporting']['scale']=3;
        $grafico['plotOptions']['pie']['dataLabels']['enabled']=true;
        $grafico['plotOptions']['pie']['dataLabels']['format']='<b>{point.name}</b>:<br> {point.percentage:.1f} %';

        //Generar JSON final
        $json=json_encode($grafico);
        return $json;
    }
    //Crear grafica con barras horizontales
    public static function g_barra($datos, $titulo="Por tipo de violencia",$nombre_serie="Hojas de vida") {
        // Extraer datos crudos
        $series=array();
        $series[0]=$datos;

        // Crear grafica
        $a_datos[0]=array();
        $a_categorias=array();
        $a_categorias_grafica=array();

        //Crear un listado unico de categorias
        foreach($series[0] as $id=>$dato) {
            $a_categorias[$id]=$dato['txt'];
        }

        // Crear un arreglo con todos los datos de todos los deptos
        foreach($a_categorias as $id=>$conteo) {
            $a_datos[0][] = isset($series[0][$id]['valor']) ? $series[0][$id]['valor'] : 0;
            $a_categorias_grafica[]=$a_categorias[$id];
        }

        //Generar datos
        $datos=array();
        $datos[0]['name']=$nombre_serie;
        $datos[0]['data']=$a_datos[0];



        // Configurar grafica
        $grafico=array();
        $grafico['chart']['type']='bar';
        //$grafico['colors']=['#f45b5b', '#434348'];
        //$grafico['colors']=[ '#00c0ef', '#001f3f'];

        $grafico['title']['text']=$titulo;

        $grafico['plotOptions']['bar']['stacking']='normal';

        $grafico['legend']['align']='center';
        $grafico['legend']['verticalAlign']='bottom';
        $grafico['legend']['enabled']=false;
        //$grafico['legend']['floating']='true';
        //$grafico['legend']['layout']='vertical';


        $grafico['xAxis']['categories']=$a_categorias_grafica;
        $grafico['yAxis']['title']['text']=null;
        $grafico['credits']['enabled']=false;

        // Nombre del archivo al exportar
        $grafico['exporting']['filename']="por_tipo_violencia";
        //$grafico['exporting']['scale']=3;

        //Asignacion de datos
        $grafico['series']=$datos;


        //Generar JSON final
        $json=json_encode($grafico);
        return $json;
    }


    #FIN

    public function total_historico(){
        $query = cost_proyecto::select(\DB::raw("COUNT(*) as total"))
            ->where('cost_proyecto.estado','=','1')->first();
        return $query;
    }

    public function total_proyecto_anio($filtros){
        $query = cost_proyecto::select(\DB::raw("COUNT(*) as total"))
            ->whereBetween('cost_proyecto.f_ingreso', array(date("Y"."-01"."-01"), $filtros->fecha_al))
            ->where('cost_proyecto.estado','=','1')
            ->first();
        return $query;
    }

    public function total_sectores_afectados($filtros){
        $query = \DB::select(\DB::raw("Select Count(*) as total from ( Select sector  from cost_proyecto
                  where cost_proyecto.estado = 1
                  GROUP BY sector) q1"));
        return $query;
    }

    public function total_estado_actual_proyecto($filtros,$tabla){

        $query = cost_proyecto::select(\DB::raw("COUNT(*) as identificador, cat_item.descripcion as text"))
            ->join('cat_item','cost_proyecto.estado_actual_proyecto','=','cat_item.id_item')
            ->whereBetween('cost_proyecto.f_ingreso', array($filtros->fecha_del, $filtros->fecha_al))
            ->where('cost_proyecto.estado','=','1')
            ->groupBy('cat_item.descripcion')
         //   ->toSql();
            ->get();
         //   dd($query);
        if (isset($tabla) && $tabla == 1){
            return $query;
        }else{
            $arreglo=array();
            foreach($query as $registro) {

                $arreglo[$registro->identificador]['valor']=(int)$registro->identificador;;
                $arreglo[$registro->identificador]['txt']=$registro->text;
            }
           // dd($arreglo);
            return $arreglo;
        }


    }


     public function total_entidad_adquisicion($filtros,$tabla)
    {
        $query = cost_proyecto::select(\DB::raw("COUNT(*) as valor, cost_proyecto.entidad_adquisicion as txt"))
            ->join('cost_proyecto_dis_plan', 'cost_proyecto.id_proyecto', '=', 'cost_proyecto_dis_plan.id_proyecto')
            ->whereBetween('cost_proyecto.f_ingreso', [ $filtros->fecha_del, $filtros->fecha_al ])
            ->where('cost_proyecto.estado','=','1')
            ->groupBy('cost_proyecto.entidad_adquisicion')
            ->get();

      //  dd($query);

        if (isset($tabla) && $tabla == 1){
            return $query;
        }else{
            $arreglo=array();
            foreach($query as $key => $registro) {

                $arreglo[$key]['valor']=(int)$registro->valor;
                $arreglo[$key]['txt']=$registro->txt;

            }
            return $arreglo;
        }
    }

    public function total_sectores_afectados_detalle($filtros,$tabla){
        $query = cost_proyecto::select(\DB::raw("COUNT(cost_proyecto.sector) as valor, cost_proyecto.sector as txt"))
            ->join('cost_proyecto_dis_plan', 'cost_proyecto.id_proyecto', '=', 'cost_proyecto_dis_plan.id_proyecto')
            ->whereBetween('cost_proyecto.f_ingreso', [ $filtros->fecha_del, $filtros->fecha_al ])
            ->where('cost_proyecto.estado','=','1')
            ->groupBy('cost_proyecto.sector')
            ->get();

        if (isset($tabla) && $tabla == 1) {
            return $query;
        } else {
            $arreglo = [];
            foreach($query as $key => $registro) {
                $arreglo[$key][ 'valor' ] = (int)$registro->valor;
                $arreglo[$key][ 'txt' ] = $registro->txt;
            }
            //dd($arreglo);
            return $arreglo;
        }

    }

}
