<?php

namespace App\Models;

use Eloquent as Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_acuerdo_financiamiento
 * @package App\Models
 * @version January 17, 2019, 5:16 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property integer id_proyecto
 * @property integer tipo
 * @property string partida_presupuestaria
 * @property float constitucional
 * @property float iva_paz
 * @property float circulacion_vehiculos
 * @property float petroleo
 * @property float consejo_desarrollo
 * @property float fondos_propios
 * @property string otros_descripcion
 * @property float otros
 * @property float donacion
 * @property float prestamo
 * @property date fecha_aprobacion
 * @property float aporte_beneficiario
 * @property float transferencia
 * @property string observaciones
 */
class cost_acuerdo_financiamiento extends Model {

    //  use SoftDeletes;

    public $table = 'cost_acuerdo_financiamiento';

    protected $primaryKey = 'id_acuerdo_financiamiento';

    public $timestamps = false;

    // const CREATED_AT = 'created_at';
    //  const UPDATED_AT = 'updated_at';

    //  protected $dates = ['deleted_at'];

    public $fillable = [
        'id_proyecto',
        'tipo',
        'id_fase',
        'partida_presupuestaria',
        'constitucional',
        'iva_paz',
        'circulacion_vehiculos',
        'petroleo',
        'consejo_desarrollo',
        'fondos_propios',
        'otros_descripcion',
        'otros',
        'donacion',
        'prestamo',
        'fecha_aprobacion',
        'aporte_beneficiario',
        'transferencia',
        'observaciones',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto'            => 'integer',
        'tipo'                   => 'integer',
        'id_fase'                => 'integer',
        'partida_presupuestaria' => 'string',
        'constitucional'         => 'float',
        'iva_paz'                => 'float',
        'circulacion_vehiculos'  => 'float',
        'petroleo'               => 'float',
        'consejo_desarrollo'     => 'float',
        'fondos_propios'         => 'float',
        'otros_descripcion'      => 'string',
        'otros'                  => 'float',
        'donacion'               => 'float',
        'prestamo'               => 'float',
        'fecha_aprobacion'       => 'date',
        'aporte_beneficiario'    => 'float',
        'transferencia'          => 'float',
        'monto_total_asignado'   => 'float',
        'observaciones'          => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

        public function setPartidaPresupuestariaAttribute($partida_presupuestaria){ $this->attributes['partida_presupuestaria'] = strlen($partida_presupuestaria)>0? $partida_presupuestaria : $partida_presupuestaria ;}
        public function setConstitucionalAttribute($constitucional){
            $this->attributes['constitucional'] = strlen($constitucional)>0? $constitucional : 0 ;}
        public function setIvaPazAttribute($iva_paz){ $this->attributes['iva_paz'] = strlen($iva_paz)>0? $iva_paz : 0 ;}
        public function setCirculacionVehiculosAttribute($circulacion_vehiculos){ $this->attributes['circulacion_vehiculos'] = strlen($circulacion_vehiculos)>0? $circulacion_vehiculos : 0 ;}
        public function setPetroleoAttribute($petroleo){ $this->attributes['petroleo'] = strlen($petroleo)>0? $petroleo : 0 ;}
        public function setConsejoDesarrolloAttribute($consejo_desarrollo){ $this->attributes['consejo_desarrollo'] = strlen($consejo_desarrollo)>0? $consejo_desarrollo : 0 ;}
        public function setFondosPropiosAttribute($fondos_propios){ $this->attributes['fondos_propios'] = strlen($fondos_propios)>0? $fondos_propios : 0 ;}
        public function setOtrosDescripcionAttribute($otros_descripcion){ $this->attributes['otros_descripcion'] = strlen($otros_descripcion)>0? $otros_descripcion : null ;}
        public function setOtrosAttribute($otros){ $this->attributes['otros'] = strlen($otros)>0? $otros : 0 ;}
        public function setDonacionAttribute($donacion){ $this->attributes['donacion'] = strlen($donacion)>0? $donacion : 0 ;}
        public function setPrestamoAttribute($prestamo){ $this->attributes['prestamo'] = strlen($prestamo)>0? $prestamo : 0 ;}
        public function setFechaAprobacionAttribute($fecha_aprobacion){ $this->attributes['fecha_aprobacion'] = strlen($fecha_aprobacion)>0? $fecha_aprobacion : null ;}
        public function setAporteBeneficiarioAttribute($aporte_beneficiario){ $this->attributes['aporte_beneficiario'] = strlen($aporte_beneficiario)>0? $aporte_beneficiario : 0 ;}
        public function setTransferenciaAttribute($transferencia){ $this->attributes['transferencia'] = strlen($transferencia)>0? $transferencia : null ;}
        public function setObservacionesAttribute($observaciones){ $this->attributes['observaciones'] = strlen($observaciones)>0? $observaciones : null ;}

    public function getIdAttribute()
    {
        return $this->id_acuerdo_financiamiento;
    }

   /* public function getFechaAprobacionAttribute($fecha_aprobacion)
    {
        if ($fecha_aprobacion != null)

            return Carbon::parse($fecha_aprobacion)->format('d/m/Y');
        return null;
    }*/

    public static function acuerdo_f($id_proyecto, $id_fase)
    {
        $id_af = Self::where('id_proyecto', $id_proyecto)->where('id_fase', $id_fase)->get();
        return $id_af;
    }



}
