<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cat_item
 * @package App\Models
 * @version February 20, 2018, 9:59 pm UTC
 */
/**
 * @property int $id_item
 * @property int $id_cat
 * @property string $descripcion
 * @property int $orden
 * @property string $otro
 * @property int $predeterminado
 */
class cat_item extends Model
{
    //use SoftDeletes;

    public $table = 'cat_item';
    protected $primaryKey='id_item';
    public $timestamps=false;
    
    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];


    public $fillable = [
        'id_cat',
        'descripcion',
        'orden',
        'otro',
       // 'predeterminado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_item' => 'integer',
        'id_cat' => 'integer',
        'descripcion' => 'string',
        'orden' => 'integer',
        'otro' => 'string',
      //  'predeterminado' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function rel_cat_cat()
    {
        return $this->belongsTo(\App\Models\cat_cat::class,'id_cat','id_cat');
    }

    // Para babosear al generador
    public function getIdAttribute() {
        return $this->id_item;
    }


    public function getFmtIdCatAttribute() {
        if($this->rel_cat_cat) {
            return $this->rel_cat_cat->descripcion;
        }
        else {
            return "Sin especificar ($this->rel_cat_cat)";
        }
    }

    /**trae catalogos catiem */
    public static function listado_items($id_catalogo=1, $vacio="") {
        $listado=self::where('id_cat',$id_catalogo)
           ->Desabilitado()
            ->ordenar()
            ->pluck('descripcion','id_item');
        if(strlen($vacio)>0) {
            $listado->prepend($vacio,0);
        }
        return $listado;
    }

    /**trae catalogos catiem con campo otro traduccion, ver el select de fields de costProyecto*/
    public static function listar_catalogo_traduccion($id_catalogo=0,$vacio='') {
        $datos = self::where('id_cat',$id_catalogo)
            ->Desabilitado()
            ->ordenar()
            ->get();
       // dd($datos);
        $arreglo = array();
        foreach($datos as $fila) {
            $arreglo[0] = 'Seleccionar';
            $arreglo[$fila->id_item]=$fila->descripcion_traduccion;
        }
        return $arreglo;
    }

    public static function estado_ejecusion($id_catalogo=0,$id_proyecto) {
        $estado = cost_info_contrato::where('id_proyecto',$id_proyecto)->first();
        if (isset($estado->estado_contrato_oc) && $estado->estado_contrato_oc >0 && $estado->estado_contrato_oc != 8282 ){
            $datos = self::where('id_cat',$id_catalogo)->where('id_item','!=',8210)
                ->Desabilitado()
                ->ordenar()
                ->get();

            $datos->prepend('Seleccione',0);


        }else{
            $datos = self::where('id_cat',$id_catalogo)
                ->Desabilitado()
                ->ordenar()
                ->get();
            $datos->prepend('Seleccione',0);
        }

        // dd($datos);
        $arreglo = array();
        foreach($datos as $fila) {
            $arreglo[$fila->id_item]=$fila->descripcion_traduccion;
        }
        return $arreglo;
    }



    public function getDescripcionTraduccionAttribute() {
        $txt = cat_item::describir($this->id_item);

        if(strlen(trim($this->descripcion))>0) {
            $txt.=  " ($this->otro)";
        }
       // $txt .= "  [".$this->otro."]";
        return $txt;
    }

    public static function describir($id_item=0) {
        if(empty($id_item)) return "Sin Especificar";

        $existe = self::find($id_item);
        if(empty($existe)) {
            return "Desconocido: $id_item.";
        }
        else {
            return $existe->descripcion;
        }
    }

    public static function listado_items_une_dos($id_cat_uno=1,$id_cat_dos=2, $vacio=""){
        $listado=self::where('id_cat',$id_cat_uno)
                    ->orWhere('id_cat',$id_cat_dos)
                    ->DesabilitadoMezclaCatalogos()
                    ->ordenar()
                    ->pluck('descripcion','id_item');
                    if(strlen($vacio)>0) {
                        $listado->prepend($vacio,0);
                    }

                    return $listado;
    }

    public function scopeDesabilitadoMezclaCatalogos($query){
        $query->where('otro','!=',1)
            ->orWhere('otro',NULL);
    }
    public function scopeDesabilitado($query){
        $query->where('otro','!=',1)
              ->orWhere('otro',NULL);
    }

    //Para que siempre orden por orden y descripcion
    public function scopeOrdenar($query) {
        $query->orderby('descripcion','asc');
    }

    // Obtener el nombre del catalogo
    public static function nombre_catalogo($id_catalogo=0) {
       // $esquema=self::schema;
        $listado=\DB::select("select  descripcion from cat_cat where id_cat=$id_catalogo");
        if(count($listado)>0) {
            $registro=$listado[0];
            return $registro->descripcion;
        }
        else {
            return "No existe catalogo $id_catalogo";
        }


    }

    public function setDescripcionAttribute($descripcion)
    {
        $this->attributes[ 'descripcion' ] = strlen($descripcion) > 0 ? $descripcion : '';
    }

    public function setOrdenAttribute($orden)
    {
        $this->attributes[ 'orden' ] = strlen($orden) > 0 ? $orden : 1;
    }

    public function setOtroAttribute($otro)
    {
        $this->attributes[ 'otro' ] = strlen($otro) > 0 ? $otro : '';
    }
    public static function listado_catalogos() {

        $listado=\DB::select("select id_cat, descripcion from cat_cat order by 2");
        $a_opciones=array();
        foreach($listado as $registro) {
            $a_opciones[$registro->id_cat]=$registro->descripcion;
        }
        return $a_opciones;
    }

    public static function item($item){
        $fn= cat_item::find($item);
        return $fn->descripcion;
    }

    public static function item_otro($item){
        $fn= cat_item::find($item);
        return $fn->otro;
    }

    public static function listado_items_avances($id_catalogo, $vacio="",$otro) {
        $listado=self::where('id_cat',$id_catalogo)
                      ->where('otro',$otro)
                      ->ordenar()
                      ->pluck('descripcion','id_item');
                    if(strlen($vacio)>0) {
                        $listado->prepend($vacio,0);
                    }
        return $listado;
    }

}
