<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class proyecto_view extends Model
{
    public $table = 'proyecto_view';

    protected $primaryKey = 'id_proyecto';

    public $timestamps = false;


    /**BUSQUEDAS Y CRITERIOS DEFAULT*/
    public static function criterios_default_oc4ids($request = 0)
    {
        //dd($request);
        $filtros = new \stdClass();
        $filtros->title = '';
        $filtros->status = 0;
        $filtros->sector = 0;
        $filtros->type = 0;
        $filtros->streetAddress = '';
        $filtros->startDate = '';
        $filtros->endDate = '';
        $filtros->id_participante = 0;
        $filtros->fase_proyecto = 0;
        $filtros->localizacion_muni_depto = 0;//departamento
        $filtros->localizacion_muni = 0;//municipio
        $filtros->localizacion = '';//municipio

        //documents
        $filtros->documentType = 0;
        $filtros->documentTitle = '';
        $filtros->datePublished = '';
        $filtros->dateModified = '';

        //otras partes
        $filtros->tipo_acta = 0;
        $filtros->tipo_fianza = 0;
        $filtros->tipo_pago_efectuado = 0;

        //busqueda por componentes
        $filtros->fase_proyecto_despliegue = 0;
        $filtros->componente_proyecto_despliegue = 0;

        // Procesar request (opcional)
        $consultado = '#e53935';
        if (is_object($request)) {
            $filtros->url = $request->url();

            if (isset($request->title) && strlen($request->title) > 0) {
                $filtros->title = $request->title;
                $filtros->title_color = strlen($request->title) > 0 ? $consultado : '';
            }
            if (isset($request->status)) {
                $filtros->status = $request->status;
                $filtros->status_color = $request->status > 0 ? $consultado : '';
            }
            if (isset($request->sector)) {
                $filtros->sector = $request->sector;
                $filtros->sector_color = $request->sector > 0 ? $consultado : '';
            }
            if (isset($request->type)) {
                $filtros->type = $request->type;
                $filtros->type_color = $request->type > 0 ? $consultado : '';
            }

            if (isset($request->startDate_submit) && strlen($request->startDate_submit) > 0) {
                $fh_formato = Carbon::parse($request->startDate_submit);
                $filtros->startDate_submit = $fh_formato->format('Y-m-d');
                $filtros->startDate_color = strlen($request->startDate) > 0 ? $consultado : '';
            }
            if (isset($request->endDate_submit) && strlen($request->endDate_submit) > 0) {
                $fh_formato = Carbon::parse($request->endDate_submit);
                $filtros->endDate_submit = $fh_formato->format('Y-m-d');
                $filtros->endDate_color = strlen($request->endDate) > 0 ? $consultado : '';
            }

            if (isset($request->localizacion_muni_depto) && $request->localizacion_muni_depto > 0) {
                $filtros->localizacion_muni_depto = $request->localizacion_muni_depto;
                $filtros->localizacion_muni_depto_color = $request->localizacion_muni_depto > 0 ? $consultado : '';
            }

            if (isset($request->localizacion_muni) && $request->localizacion_muni > 0) {
                $filtros->localizacion_muni = $request->localizacion_muni;
                $filtros->localizacion_muni_color = $request->localizacion_muni > 0 ? $consultado : '';
            }

            if (isset($request->localizacion) && strlen($request->localizacion) > 0) {
                $filtros->localizacion = $request->localizacion;
                $filtros->localizacion_color = $request->localizacion > 0 ? $consultado : '';
            }

            //dd($request);
            if (isset($request->documentType) && $request->documentType > 0) {
                $filtros->documentType = $request->documentType;
                $filtros->documentType_color = $request->documentType > 0 ? $consultado : '';
            }

            if (isset($request->documentTitle) && strlen($request->documentTitle) > 0) {
                $filtros->documentTitle = $request->documentTitle;

                $filtros->documentTitle_color = strlen($request->documentTitle) > 0 ? $consultado : '';
            }

            if (isset($request->datePublished_submit) && strlen($request->datePublished_submit) > 0) {
                $fh_formato = Carbon::parse($request->datePublished_submit);
                $filtros->datePublished = $fh_formato->format('Y-m-d');
                $filtros->datePublished_color = strlen($request->datePublished_submit) > 0 ? $consultado : '';
            }

            if (isset($request->dateModified_submit) && strlen($request->dateModified_submit) > 0) {
                $fh_formato = Carbon::parse($request->dateModified_submit);
                $filtros->dateModified_ = $fh_formato->format('Y-m-d');
                $filtros->dateModified_color = strlen($request->dateModified_submit) > 0 ? $consultado : '';
            }

            if (isset($request->id_participante) && $request->id_participante > 0) {
                $filtros->id_participante = $request->id_participante;
                $filtros->id_participante_color = $request->id_participante > 0 ? $consultado : '';
            }

            if (isset($request->fase_proyecto) && $request->fase_proyecto > 0) {
                $filtros->fase_proyecto = $request->fase_proyecto;
                $filtros->fase_proyecto_color = $request->fase_proyecto > 0 ? $consultado : '';
            }

            if (isset($request->tipo_acta) && $request->tipo_acta > 0) {
                $filtros->tipo_acta = $request->tipo_acta;
                $filtros->tipo_acta_color = $request->tipo_acta > 0 ? $consultado : '';
            }

            if (isset($request->tipo_fianza) && $request->tipo_fianza > 0) {
                $filtros->tipo_fianza = $request->tipo_fianza;
                $filtros->tipo_fianza_color = $request->tipo_fianza > 0 ? $consultado : '';
            }

            if (isset($request->tipo_pago_efectuado) && $request->tipo_pago_efectuado > 0) {
                $filtros->tipo_pago_efectuado = $request->tipo_pago_efectuado;
                $filtros->tipo_pago_efectuado_color = $request->tipo_pago_efectuado > 0 ? $consultado : '';
            }

            if (isset($request->tipo_ampliacion) && $request->tipo_ampliacion > 0) {
                $filtros->tipo_ampliacion = $request->tipo_ampliacion;
                $filtros->tipo_ampliacion_color = $request->tipo_ampliacion > 0 ? $consultado : '';
            }

            if (isset($request->fase_proyecto_despliegue) && $request->fase_proyecto_despliegue > 0) {
                $filtros->fase_proyecto_despliegue = $request->fase_proyecto_despliegue;
                $filtros->fase_proyecto_despliegue_color = $request->fase_proyecto_despliegue > 0 ? $consultado : '';
            }

            if (isset($request->componente_proyecto_despliegue) && $request->componente_proyecto_despliegue > 0) {
                $filtros->componente_proyecto_despliegue = $request->componente_proyecto_despliegue;
                $filtros->componente_proyecto_despliegue_color = $request->componente_proyecto_despliegue > 0 ? $consultado : '';
            }

        }
        return $filtros;
    }

    public static function scopeTitle($query, $filtros)
    {
        $texto = str_replace(" ", "%", $filtros->title);
        if (strlen($filtros->title) > 0) {
            $query->where('title', 'like', "$texto%");
        }
    }


    public static function scopeStatus($query, $filtros)
    {
        if (isset($filtros->status) && $filtros->status > 0) {
            $query->join('cost_proyecto as st', 'st.id_proyecto', '=', 'proyecto_view.id_proyecto')
                  ->where('st.estado_oc',$filtros->status);
        }
    }

    public static function scopeSector($query, $filtro)
    {
        if (isset($filtro->sector) && $filtro->sector > 0) {
            $query->join('cost_proyecto as sct', 'sct.id_proyecto', '=', 'proyecto_view.id_proyecto')
                        ->where('sct.sector_oc',$filtro->sector);
        }
    }

    public static function scopeTipoProyecto($query, $filtro)
    {
        if (isset($filtro->type) && $filtro->type > 0) {
            $query->join('cost_proyecto as tpr', 'tpr.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('tpr.tipo_oc',$filtro->type);
        }
    }

    /*scope que evalua departamento y municipio*/
    public static function scopeRegion($query, $filtros)
    {
        if (isset($filtros->localizacion_muni_depto) && $filtros->localizacion_muni_depto > 0) {
            $query->join('cost_proyecto as cpy', 'cpy.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('cpy.localizacion_depto', intval($filtros->localizacion_muni_depto))
                ->where('cpy.localizacion_muni', intval($filtros->localizacion_muni));
        }
    }

    public static function scopeLocalizacion($query, $filtros)
    {
        if (isset($filtros->localizacion) && strlen($filtros->localizacion) > 0) {
            dd($filtros->localizacion);
            $texto = str_replace(" ", "%", $filtros->localizacion);
            $query->join('cost_proyecto as cpl', 'cpl.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('cpl.localizacion', 'like', "%$texto%");
        }
    }

    public static function scopeStartDay($query, $filtros)
    {
        if (isset($filtros->startDate_submit) && strlen($filtros->startDate_submit) > 0) {
            $query->join('cost_proyecto as pvp', 'pvp.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('pvp.f_aprobacion', $filtros->startDate_submit);
        }
    }

    public static function scopeEndDay($query, $filtros)
    {
        if (isset($filtros->endDate_submit) && strlen($filtros->endDate_submit) > 0) {
            $query->join('cost_proyecto as pve', 'pve.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('pve.f_finalizacion', $filtros->endDate_submit);
        }
    }

    public static function scopeIdParticipante($query, $filtros)
    {
        //dd($filtros->fase_proyecto);
        if ($filtros->id_participante > 0) {
            $query->join('cost_oferente as p', 'p.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('p.id_participante', $filtros->id_participante)
                ->where('p.id_fase',$filtros->fase_proyecto);
        }
    }

    public static function scopeTipoActa($query, $filtros)
    {
        if (isset($filtros->tipo_acta) && $filtros->tipo_acta > 0) {
              $query->join('cost_acta as ca', 'ca.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('ca.tipo', $filtros->tipo_acta)
                ->whereIn('ca.id_fase',$filtros->fase_proyecto);
        }
    }

    public static function scopeTipoFianza($query, $filtros)
    {
        if ($filtros->tipo_fianza > 0) {
            $query->join('cost_fianza as cf', 'cf.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('cf.tipo', $filtros->tipo_fianza)
                ->where('cf.id_fase',$filtros->fase_proyecto);
        }
    }

    public static function scopeTipoPagoEfectuado($query, $filtros)
    {

        if ($filtros->tipo_pago_efectuado > 0) {
            $query->join('cost_pago_efectuado as cpe', 'cpe.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('cpe.tipo', $filtros->tipo_pago_efectuado)
                ->where('cpe.id_fase',$filtros->fase_proyecto);
        }
    }

    public static function scopeTipoAmpliacion($query, $filtros)
    {
        if ($filtros->tipo_ampliacion > 0) {
            //dd($filtros->tipo_ampliacion);
            $query->join('cost_ampliacion as cam', 'cam.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('cam.tipo', $filtros->tipo_ampliacion)
                ->where('cam.id_fase',$filtros->fase_proyecto);
        }
    }

    public static function scopeDocumentType($query, $filtros)
    {
        if (isset($filtros->documentType) && $filtros->documentType > 0) {
            $query->join('documento as doc', 'doc.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('doc.tipo', $filtros->documentType)
                ->where('doc.id_fase',$filtros->fase_proyecto);
        }
    }

    public static function scopeDocumentTitle($query, $filtros)
    {
        if (isset($filtros->documentTitle) && strlen($filtros->documentTitle) > 0) {
            $texto = str_replace(" ", "%", $filtros->documentTitle);
            $query->join('documento as dvt', 'dvt.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('dvt.titulo', 'like', "$texto%")
                ->where('dvt.id_fase',$filtros->fase_proyecto);
        }
    }

    public static function scopeDatePublished($query, $filtros)
    {
        if (strlen($filtros->datePublished) > 0) {
            $query->join('documento_view as dvp', 'dvp.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('dvp.datePublished', $filtros->datePublished)
                ->where('dvp.id_fase', $filtros->fase_proyecto);
        }
    }

    public static function scopeDateModified($query, $filtros)
    {
        if (strlen($filtros->dateModified) > 0) {
            $query->join('documento_view as dvm', 'dvm.id_proyecto', '=', 'proyecto_view.id_proyecto')
                ->where('dvm.dateModified', $filtros->dateModified)
                ->where('dvm.id_fase', $filtros->fase_proyecto);
        }
    }


    /**scope principal de la busqueda*/
    public static function scopeBuscarIdOc4ids($query, $filtros)
    {
        $query->Title($filtros)
            ->Status($filtros)
            ->Sector($filtros)
            ->TipoProyecto($filtros)
            ->Region($filtros)
            ->Localizacion($filtros)
            ->StartDay($filtros)
            ->EndDay($filtros)
            ->IdParticipante($filtros)
            ->TipoActa($filtros)
            ->TipoFianza($filtros)
            ->TipoPagoEfectuado($filtros)
            ->TipoAmpliacion($filtros)
            ->DocumentType($filtros)
            ->DocumentTitle($filtros)
        ;
        return $query;
    }

}
