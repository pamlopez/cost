<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class politica
 * @package App\Models
 * @version November 10, 2021, 1:24 pm CST
 *
 * @property string titulo
 * @property string descripcion_html
 * @property integer estado
 */
class politica extends Model
{
   // use SoftDeletes;

    public $table = 'politica';
    protected $primaryKey='id_politica';
    public $timestamps=false;
    
   // const CREATED_AT = 'created_at';
   // const UPDATED_AT = 'updated_at';


    // protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'descripcion_html',
        'estado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'titulo' => 'string',
        'descripcion_html' => 'string',
        'estado' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getIdAttribute() {
        return $this->id_politica;
    }
    
}
