<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_liquidacion
 * @package App\Models
 * @version March 25, 2019, 5:03 am UTC
 *
 * @property integer id_proyecto
 * @property string informe_avances
 * @property string planos_finales
 * @property string reporte_evaluacion
 * @property string informe_terminacion
 * @property string informe_evaluacion
 */
class cost_liquidacion extends Model
{
    //use SoftDeletes;

    public $table = 'cost_liquidacion';
    protected $primaryKey='id_liquidacion';
    public $timestamps=false;
    
   // const CREATED_AT = 'created_at';
   // const UPDATED_AT = 'updated_at';


   // protected $dates = ['deleted_at'];


    public $fillable = [
        'id_proyecto',
        'informe_avances',
        'planos_finales',
        'reporte_evaluacion',
        'informe_terminacion',
        'informe_evaluacion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'informe_avances' => 'string',
        'planos_finales' => 'string',
        'reporte_evaluacion' => 'string',
        'informe_terminacion' => 'string',
        'informe_evaluacion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];


        public function setInformeAvancesAttribute($informe_avances){ $this->attributes['informe_avances']= strlen($informe_avances)>0 ? $informe_avances: null ;}
        public function setPlanosFinalesAttribute($planos_finales){ $this->attributes['planos_finales']= strlen($planos_finales)>0 ? $planos_finales: null ;}
        public function setReporteEvaluacionAttribute($reporte_evaluacion){ $this->attributes['reporte_evaluacion']= strlen($reporte_evaluacion)>0 ? $reporte_evaluacion: null ;}
        public function setInformeTerminacionAttribute($informe_terminacion){ $this->attributes['informe_terminacion']= strlen($informe_terminacion)>0 ? $informe_terminacion: null ;}
        public function setInformeEvaluacionAttribute($informe_evaluacion){ $this->attributes['informe_evaluacion']= strlen($informe_evaluacion)>0 ? $informe_evaluacion: null ;}

    public function getIdAttribute() {
        return $this->id_liquidacion;
    }
    public static function info_c($id_proyecto){
        $id_ic= Self::where('id_proyecto',$id_proyecto)->get();
        return $id_ic;
    }
}
