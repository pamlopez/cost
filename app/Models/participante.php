<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class participante
 * @package App\Models
 * @version April 21, 2019, 3:29 pm UTC
 *
 * @property string nombre_comun
 * @property string nombre_legal
 * @property string uri
 * @property string direccion
 * @property string direccion_referencia
 * @property integer codigo_postal
 * @property string nombre_contacto
 * @property string email
 * @property string telefono
 */
class participante extends Model
{
  //  use SoftDeletes;

    public $table = 'participante';
    protected $primaryKey='id_participante';
    public $timestamps=false;
    
  //  const CREATED_AT = 'created_at';
  //  const UPDATED_AT = 'updated_at';


  //  protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre_comun',
        'nombre_legal',
        'uri',
        'direccion',
        'direccion_referencia',
        'codigo_postal',
        'nombre_contacto',
        'email',
        'telefono',
        'localidad',
        'region',
        'nombre_pais',
        'punto_contacto',
        'numero_fax',
        'schema',
        'id_schema',
        'url'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre_comun' => 'string',
        'nombre_legal' => 'string',
        'uri' => 'string',
        'direccion' => 'string',
        'direccion_referencia' => 'string',
        'codigo_postal' => 'integer',
        'nombre_contacto' => 'string',
        'email' => 'string',
        'telefono' => 'string',
        'localidad' => 'string',
        'region' => 'string',
        'nombre_pais' => 'string',
        'numero_fax' => 'string',
        'schema' => 'integer',
        'id_schema' => 'integer',
        'url' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function setLocalidadAttribute($localidad)
    {
        $this->attributes[ 'localidad' ] = strlen($localidad) > 0 ? $localidad : null;
    }

    public function setRegionAttribute($region)
    {
        $this->attributes[ 'region' ] = strlen($region) > 0 ? $region : null;
    }

    public function setNombrePaisAttribute($nombre_pais)
    {
        $this->attributes[ 'nombre_pais' ] = strlen($nombre_pais) > 0 ? $nombre_pais : null;
    }



    public function setNumeroFaxAttribute($numero_fax)
    {
        $this->attributes[ 'numero_fax' ] = strlen($numero_fax) > 0 ? $numero_fax : null;
    }

    public function setNombreComunAttribute($nombre_comun)
    {
        $this->attributes[ 'nombre_comun' ] = strlen($nombre_comun) > 0 ? $nombre_comun : null;
    }
    public function setNombreLegalAttribute($nombre_legal)
    {
        $this->attributes[ 'nombre_legal' ] = strlen($nombre_legal) > 0 ? $nombre_legal : Null;
    }

    public function setUriAttribute($uri)
    {
        $this->attributes[ 'uri' ] = strlen($uri) > 0 ? $uri : Null;
    }
    public function setDireccionAttribute($direccion)
    {
        $this->attributes[ 'direccion' ] = strlen($direccion) > 0 ? $direccion : Null;
    }
    public function setDireccionReferenciaAttribute($direccion_referencia)
    {
        $this->attributes[ 'direccion_referencia' ] = strlen($direccion_referencia) > 0 ? $direccion_referencia : Null;
    }
    public function setCodigoPostalAttribute($codigo_postal)
    {
        $this->attributes[ 'codigo_postal' ] = strlen($codigo_postal) > 0 ? $codigo_postal : Null;
    }

    public function setNombreContactoAttribute($nombre_contacto)
    {
        $this->attributes[ 'nombre_contacto' ] = strlen($nombre_contacto) > 0 ? $nombre_contacto : Null;
    }

    public function setEmailAttribute($email)
    {
        $this->attributes[ 'email' ] = strlen($email) > 0 ? $email : Null;
    }

    public function setTelefonoAttribute($telefono)
    {
        $this->attributes[ 'telefono' ] = strlen($telefono) > 0 ? $telefono : Null;
    }

    public function setUrlAttribute($url)
    {
        $this->attributes[ 'url' ] = strlen($url) > 0 ? $url : Null;
    }
    public function setSchemaAttribute($schema)
    {
        if($schema > 0){
            $this->attributes[ 'schema' ] = $schema ;
            $cat_item = cat_item::where('id_item', $schema)->first()->otro;
            $this->attributes[ 'id_schema' ] = $cat_item ;

        }else{

            $this->attributes[ 'schema' ] =  Null;
            $this->attributes[ 'id_schema' ] = Null;
        }

    }

    public function getFmtFormatoAttribute()
    {
        if ($this->rel_formato) {
            return $this->rel_formato->descripcion;
        } else {
            return "";
        }
    }

    public function getFmtSchemaAttribute()
    {
        if ($this->rel_schema) {
            return $this->rel_schema->descripcion;
        } else {
            return "";
        }
    }
    public function getFmtIdSchemaAttribute()
    {
        if ($this->rel_schema) {
            return $this->rel_schema->otro;
        } else {
            return "";
        }
    }
    public function rel_schema()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'schema', 'id_item');
    }

    public function getIdAttribute() {
        return $this->id_participante;
    }

    public function rel_oferentes()
    {
        $test = $this->hasMany(\App\Models\cost_oferente::class,'id_participante','id_participante');
        return $test;
    }

    public static function listado_items($vacio) {
        $listado=self::pluck('nombre_comun','id_participante');
        if(strlen($vacio)>0) {
            $listado->prepend($vacio,0);
        }
        return $listado;
    }

    
}
