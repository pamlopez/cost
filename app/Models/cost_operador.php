<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_operador
 * @package App\Models
 * @version April 21, 2019, 7:32 pm UTC
 *
 * @property integer id_proyecto
 * @property string nombre_operador
 * @property string usuario_operador
 * @property date fecha_operador
 */
class cost_operador extends Model
{
  //  use SoftDeletes;

    public $table = 'cost_operador';
    
  //  const CREATED_AT = 'created_at';
  //  const UPDATED_AT = 'updated_at';


   // protected $dates = ['deleted_at'];
    protected $primaryKey='id_operador';
    public $timestamps=false;


    public $fillable = [
        'id_proyecto',
        'nombre_operador',
        'usuario_operador',
        'fecha_operador'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'nombre_operador' => 'string',
        'usuario_operador' => 'string',
        'fecha_operador' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

        public function setNombreOperadorAttribute($nombre_operador){ $this->attributes['nombre_operador']=strlen($nombre_operador)>0 ? $nombre_operador : null ;}
        public function setUsuarioOperadorAttribute($usuario_operador){ $this->attributes['usuario_operador']=strlen($usuario_operador)>0 ? $usuario_operador : null ;}
        public function setFechaOperadorAttribute($fecha_operador){ $this->attributes['fecha_operador']=strlen($fecha_operador)>0 ? $fecha_operador : null ;}

    public function getIdAttribute() {
        return $this->id_operador;
    }

    public static function operador_c($id_proyecto){
        $id_ic= Self::where('id_proyecto',$id_proyecto)->get();
        return $id_ic;
    }

    
}
