<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_proyecto_dis_plan
 * @package App\Models
 * @version October 30, 2018, 5:07 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property integer id_proyecto
 * @property string dis_anuncio_invitacion
 * @property string dis_listado_oferentes
 * @property string acta_adjudica
 * @property string dis_responsable
 * @property float dis_precio_contratado
 * @property string dis_plazo_contrato
 * @property string dis_tipo_contrato
 * @property string dis_afianzadora
 * @property string dis_fianza
 * @property date dis_f_inicio
 * @property date dis_f_entrega
 * @property string dis_responsable_ea
 * @property string dis_f_aprobacion_contrato_d
 * @property date dis_f_aprobacion_contrato
 * @property float dis_anticipo
 * @property string dis_numero_pagos
 * @property string dis_fechas
 * @property string dis_mod_plazo
 * @property string dis_responsable_mod_plazo
 * @property date dis_f_nueva_finaliza
 * @property float dis_monto_modificado
 * @property string dis_monto_modificado_d
 * @property string dis_responsable_mod_monto
 * @property float dis_porcentaje_cambio
 * @property string dis_acta_recepcion
 * @property date dis_f_recepcion
 * @property string dis_f_recepcion_d
 * @property date dis_f_finiquito
 * @property string dis_fecha_finiquito_det
 * @property string dis_fianza_detalle
 * @property string dis_det_fianza
 * @property string instrumento_eia
 * @property string elaborado_por_eia
 * @property date f_eia
 * @property string resolucion_marn
 * @property date f_resolucion_marn
 * @property string medida_mitigacion_presupuesto
 * @property string agrip
 * @property date agrip_f
 * @property string agrip_elaborado_por
 * @property string agrip_medida_mitigacion
 * @property string responsable_estudio_tecnico
 * @property integer estudio_suelo
 * @property date estudio_suelo_f
 * @property string estudio_suelo_por
 * @property integer estudio_topografia
 * @property date estudio_topografia_f
 * @property string estudio_topografia_por
 * @property integer estudio_geologico
 * @property date estudio_geologico_f
 * @property string estudio_geologico_por
 * @property integer diseno_estructural
 * @property date diseno_estructural_f
 * @property string diseno_estructural_por
 * @property integer memoria_calculo
 * @property string memoria_calculo_responsable
 * @property integer plano_completo
 * @property string plano_completo_responsable
 * @property integer espe_tecnica
 * @property string espe_tecnica_responsable
 * @property integer espe_general
 * @property string espe_general_responsable
 * @property integer dispo_especial
 * @property string dispo_especial_responsable
 * @property integer otro_estudio
 * @property string otro_estudio_responsable
 * @property float presupuesto_proyecto
 * @property float costo_estimado_proyecto
 * @property date presupuesto_aprobacion_f
 * @property string estado_actual_proyecto_d
 * @property integer estado_actual_proyecto
 * @property float avance_fisico
 * @property float avance_financiero
 * @property integer modalidad_contratacion
 * @property string fuente_financiamiento_dis
 * @property float monto_asignado_dis
 * @property integer nog
 * @property date nog_f
 * @property string fuente_f_sup
 * @property float monto_sup
 * @property string fuente_f_ejecucion
 * @property float monto_ejecucion
 * @property string fuente_f_ambiental
 * @property float monto_ambiental
 * @property string fuente_riesgo
 * @property float monto_riesgo
 * @property string estudio_viabilidad
 * @property string aprobacion_viabilidad
 * @property string resultado_principal_impacto
 * @property string observaciones_marn
 * @property string links_marn
 * @property string observaciones_suelo
 * @property string links_suelo
 * @property string observaciones_agrip
 * @property string links_agrip
 * @property string observaciones_topografia
 * @property string links_topografia
 * @property string observaciones_geologico
 * @property string links_geologico
 * @property string observaciones_hidrogeologico
 * @property string links_hidrogeologico
 * @property string estudio_hidrogeologico
 * @property date fecha_hidrogeologico
 * @property string hecho_por_hidrogeologico
 * @property string observaciones_estructural
 * @property string links_estructural
 * @property string memoria_calculo_hecho_por
 * @property string observaciones_memoria_calculo
 * @property string liks_memoria_calculo
 * @property string observaciones_plano_completo
 * @property string liks_plano_completo
 * @property string liks_espe_general
 */
class cost_proyecto_dis_plan extends Model {

    // use SoftDeletes;

    public $table = 'cost_proyecto_dis_plan';

    protected $primaryKey = 'id_proyecto_dis_plan';

    public $timestamps = false;

    // const CREATED_AT = 'created_at';
    // const UPDATED_AT = 'updated_at';

    //protected $dates = ['deleted_at'];

    public $fillable = [
        'id_proyecto',
        'dis_anuncio_invitacion',
        'dis_listado_oferentes',
        'acta_adjudica',
        'dis_responsable',
        'dis_precio_contratado',
        'dis_plazo_contrato',
        'dis_tipo_contrato',
        'dis_afianzadora',
        'dis_fianza',
        'dis_f_inicio',
        'dis_f_entrega',
        'dis_responsable_ea',
        'dis_f_aprobacion_contrato_d',
        'dis_f_aprobacion_contrato',
        'dis_anticipo',
        'dis_numero_pagos',
        'dis_fechas',
        'dis_mod_plazo',
        'dis_responsable_mod_plazo',
        'dis_f_nueva_finaliza',
        'dis_monto_modificado',
        'dis_monto_modificado_d',
        'dis_responsable_mod_monto',
        'dis_porcentaje_cambio',
        'dis_acta_recepcion',
        'dis_f_recepcion',
        'dis_f_recepcion_d',
        'dis_f_finiquito',
        'dis_fecha_finiquito_det',
        'dis_fianza_detalle',
        'dis_det_fianza',
        'instrumento_eia',
        'elaborado_por_eia',
        'f_eia',
        'resolucion_marn',
        'f_resolucion_marn',
        'medida_mitigacion_presupuesto',
        'agrip',
        'agrip_f',
        'agrip_elaborado_por',
        'agrip_medida_mitigacion',
        'responsable_estudio_tecnico',
        'estudio_suelo',
        'estudio_suelo_f',
        'estudio_suelo_por',
        'estudio_topografia',
        'estudio_topografia_f',
        'estudio_topografia_por',
        'estudio_geologico',
        'estudio_geologico_f',
        'estudio_geologico_por',
        'diseno_estructural',
        'diseno_estructural_f',
        'diseno_estructural_por',
        'memoria_calculo',
        'memoria_calculo_responsable',
        'plano_completo',
        'plano_completo_responsable',
        'espe_tecnica',
        'espe_tecnica_responsable',
        'espe_general',
        'espe_general_responsable',
        'dispo_especial',
        'dispo_especial_responsable',
        'otro_estudio',
        'otro_estudio_responsable',
        'presupuesto_proyecto',
        'presupuesto_aprobacion_f',
        'estado_actual_proyecto_d',
        'estado_actual_proyecto',
        'avance_fisico',
        'avance_financiero',
        'modalidad_contratacion',
        'fuente_financiamiento_dis',
        'monto_asignado_dis',
       // 'nog',
       // 'nog_f',
        'fuente_f_sup',
        'monto_sup',
        'fuente_f_ejecucion',
        'monto_ejecucion',
        'fuente_f_ambiental',
        'monto_ambiental',
        'fuente_riesgo',
        'monto_riesgo',
        'estudio_viabilidad',
        'aprobacion_viabilidad',
        'resultado_principal_impacto',
        'observaciones_marn',
        'links_marn',
        'observaciones_suelo',
        'links_suelo',
        'observaciones_agrip',
        'links_agrip',
        'observaciones_topografia',
        'links_topografia',
        'observaciones_geologico',
        'links_geologico',
        'observaciones_hidrogeologico',
        'estudio_hidrogeologico',
        'fecha_hidrogeologico',
        'hecho_por_hidrogeologico',
        'links_hidrogeologico',
        'observaciones_estructural',
        'links_estructural',
        'memoria_calculo_hecho_por',
        'observaciones_memoria_calculo',
        'liks_memoria_calculo',
        'observaciones_memoria_calculo',
        'liks_plano_completo',
        'observaciones_plano_completo',
        'observaciones_dispo_espe',
        'links_espe_general',
        'observaciones_espe_tecnica',
        'links_espe_tecnica',
        'observaciones_dispo_espe',
        'links_dispo_especial',
        'observaciones_otro',
        'links_otro',
        'dis_terminos_referencia',
        'inst_ea',
        'links_inst_ea',
        'inst_ea_resoponsable',
        'inst_ea_d',
        'links_presupuesto',
        'costo_estimado_proyecto',
        'resp_diseno',
        'a_estudio_facti',
        'a_entidades',
        'a_derecho_paso',
        'presupuesto_moneda'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function getIdAttribute()
    {
        return $this->id_proyecto_dis_plan;
    }

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto'                   => 'integer',
        'dis_anuncio_invitacion'        => 'string',
        'dis_listado_oferentes'         => 'string',
        'acta_adjudica'                 => 'string',
        'dis_responsable'               => 'string',
        'dis_precio_contratado'         => 'float',
        'dis_plazo_contrato'            => 'string',
        'dis_tipo_contrato'             => 'string',
        'dis_afianzadora'               => 'string',
        'dis_fianza'                    => 'string',
        'dis_f_inicio'                  => 'date',
        'dis_f_entrega'                 => 'date',
        'dis_responsable_ea'            => 'string',
        'dis_f_aprobacion_contrato_d'   => 'string',
        'dis_f_aprobacion_contrato'     => 'date',
        'dis_anticipo'                  => 'float',
        'dis_numero_pagos'              => 'string',
        'dis_fechas'                    => 'string',
        'dis_mod_plazo'                 => 'string',
        'dis_responsable_mod_plazo'     => 'string',
        'dis_f_nueva_finaliza'          => 'date',
        'dis_monto_modificado'          => 'float',
        'dis_monto_modificado_d'        => 'string',
        'dis_responsable_mod_monto'     => 'string',
        'dis_porcentaje_cambio'         => 'float',
        'dis_acta_recepcion'            => 'string',
        'dis_f_recepcion'               => 'date',
        'dis_f_recepcion_d'             => 'string',
        'dis_f_finiquito'               => 'date',
        'dis_fecha_finiquito_det'       => 'string',
        'dis_fianza_detalle'            => 'string',
        'dis_det_fianza'                => 'string',
        'instrumento_eia'               => 'string',
        'elaborado_por_eia'             => 'string',
        'f_eia'                         => 'date',
        'resolucion_marn'               => 'string',
        'f_resolucion_marn'             => 'date',
        'medida_mitigacion_presupuesto' => 'string',
        'agrip'                         => 'string',
        'agrip_f'                       => 'date',
        'agrip_elaborado_por'           => 'string',
        'agrip_medida_mitigacion'       => 'string',
        'responsable_estudio_tecnico'   => 'string',
        'estudio_suelo'                 => 'integer',
        'estudio_suelo_f'               => 'date',
        'estudio_suelo_por'             => 'string',
        'estudio_topografia'            => 'integer',
        'estudio_topografia_f'          => 'date',
        'estudio_topografia_por'        => 'string',
        'estudio_geologico'             => 'integer',
        'estudio_geologico_f'           => 'date',
        'estudio_geologico_por'         => 'string',
        'diseno_estructural'            => 'integer',
        'diseno_estructural_f'          => 'date',
        'diseno_estructural_por'        => 'string',
        'memoria_calculo'               => 'integer',
        'memoria_calculo_responsable'   => 'string',
        'plano_completo'                => 'integer',
        'plano_completo_responsable'    => 'string',
        'espe_tecnica'                  => 'integer',
        'espe_tecnica_responsable'      => 'string',
        'espe_general'                  => 'integer',
        'espe_general_responsable'      => 'string',
        'dispo_especial'                => 'integer',
        'dispo_especial_responsable'    => 'string',
        'otro_estudio'                  => 'integer',
        'otro_estudio_responsable'      => 'string',
        'presupuesto_proyecto'          => 'float',
        'costo_estimado_proyecto'       => 'float',
        'presupuesto_aprobacion_f'      => 'date',
        'estado_actual_proyecto_d'      => 'string',
        'estado_actual_proyecto'        => 'integer',
        'avance_fisico'                 => 'float',
        'avance_financiero'             => 'float',
        'modalidad_contratacion'        => 'integer',
        'fuente_financiamiento_dis'     => 'string',
        'monto_asignado_dis'            => 'float',
      //  'nog'                           => 'integer',
      //  'nog_f'                         => 'date',
        'fuente_f_sup'                  => 'string',
        'monto_sup'                     => 'float',
        'fuente_f_ejecucion'            => 'string',
        'monto_ejecucion'               => 'float',
        'fuente_f_ambiental'            => 'string',
        'monto_ambiental'               => 'float',
        'fuente_riesgo'                 => 'string',
        'monto_riesgo'                  => 'float',
        'estudio_viabilidad'            => 'string',
        'aprobacion_viabilidad'         => 'string',
        'resultado_principal_impacto'   => 'string',
        'observaciones_marn'            => 'string',
        'links_marn'                    => 'string',
        'observaciones_agrip'           => 'string',
        'links_agrip'                   => 'string',
        'observaciones_topografia'      => 'string',
        'links_topografia'              => 'string',
        'observaciones_hidrogeologico'  => 'string',
        'estudio_hidrogeologico'        => 'string',
        'fecha_hidrogeologico'          => 'date',
        'hecho_por_hidrogeologico'      => 'string',
        'links_hidrogeologico'          => 'string',
        'links_hidrogeologico'          => 'string',
        'memoria_calculo_hecho_por'     => 'string',
        'observaciones_memoria_calculo' => 'string',
        'liks_memoria_calculo'          => 'string',
        'observaciones_memoria_calculo' => 'string',
        'observaciones_espe_general'    => 'string',
        'links_espe_general'            => 'string',
        'observaciones_espe_tecnica'    => 'string',
        'links_espe_tecnica'            => 'string',
        'observaciones_dispo_espe'     => 'string',
        'links_dispo_especial'          => 'string',
        'observaciones_otro'            => 'string',
        'links_otro'                    => 'string',
        'a_estudio_facti'               => 'string',
        'dis_terminos_referencia'       => 'string',
        'inst_ea'                       => 'string',
        'links_inst_ea'                 => 'string',
        'inst_ea_d'                     => 'date',
        'inst_ea_resoponsable'          => 'string',
        'links_presupuesto'             => 'string',
        'resp_diseno'                   => 'string',
        'a_estudio_facti'               => 'string',
        'a_entidades'                   => 'string',
        'a_derecho_paso'                => 'string',
        //'presupuesto_moneda'            => 'integer'

    ];

    public function setResolucionMarnAttribute($resolucion_marn)
    {
        $this->resolucion_marn = strlen($resolucion_marn) > 0 ? $resolucion_marn : null;
    }

    public function setEstudioSueloAttribute($estudio_suelo)
    {
        $this->estudio_suelo = $estudio_suelo > 0 ? $estudio_suelo : 0;
    }
   /* public function setPresupuestoMonedaAttribute($presupuesto_moneda)
    {
        $this->presupuesto_moneda = strlen($presupuesto_moneda )> 0 ? $presupuesto_moneda : '';
    }*/
    public function setEstudioTopografiaAttribute($estudio_topografia)
    {
        $this->estudio_topografia = $estudio_topografia > 0 ? $estudio_topografia : 0;
    }

    public function setDisAnuncioInvitacionAttribute($dis_anuncio_invitacion)
    {
        $this->attributes[ 'dis_anuncio_invitacion' ] = strlen($dis_anuncio_invitacion) > 0 ? $dis_anuncio_invitacion : null;
    }

    public function setDisListado_oferentesAttribute($dis_listado_oferentes)
    {
        $this->attributes[ 'dis_listado_oferentes' ] = strlen($dis_listado_oferentes) > 0 ? $dis_listado_oferentes : null;
    }

    public function setactaAdjudicaAttribute($acta_adjudica)
    {
        $this->attributes[ 'acta_adjudica' ] = strlen($acta_adjudica) > 0 ? $acta_adjudica : null;
    }

    public function setDisResponsableAttribute($dis_responsable)
    {
        $this->attributes[ 'dis_responsable' ] = strlen($dis_responsable) > 0 ? $dis_responsable : null;
    }

    public function setDisPrecioContratadoAttribute($dis_precio_contratado)
    {
        $this->attributes[ 'dis_precio_contratado' ] = strlen($dis_precio_contratado) > 0 ? $dis_precio_contratado : null;
    }

    public function setDisPlazoContratoAttribute($dis_plazo_contrato)
    {
        $this->attributes[ 'dis_plazo_contrato' ] = strlen($dis_plazo_contrato) > 0 ? $dis_plazo_contrato : null;
    }

    public function setDisTipoContratoAttribute($dis_tipo_contrato)
    {
        $this->attributes[ 'dis_tipo_contrato' ] = strlen($dis_tipo_contrato) > 0 ? $dis_tipo_contrato : null;
    }

    public function setDisAfianzadoraAttribute($dis_afianzadora)
    {
        $this->attributes[ 'dis_afianzadora' ] = strlen($dis_afianzadora) > 0 ? $dis_afianzadora : null;
    }

    public function setDisFianzaAttribute($dis_fianza)
    {
        $this->attributes[ 'dis_fianza' ] = strlen($dis_fianza) > 0 ? $dis_fianza : null;
    }

    public function setDisFInicioAttribute($dis_f_inicio)
    {
        $this->attributes[ 'dis_f_inicio' ] = strlen($dis_f_inicio) > 0 ? $dis_f_inicio : null;
    }

    public function setDisFEntregaAttribute($dis_f_entrega)
    {
        $this->attributes[ 'dis_f_entrega' ] = strlen($dis_f_entrega) > 0 ? $dis_f_entrega : null;
    }

    public function setDisResponsableEaAttribute($dis_responsable_ea)
    {
        $this->attributes[ 'dis_responsable_ea' ] = strlen($dis_responsable_ea) > 0 ? $dis_responsable_ea : null;
    }

    public function setDisFAprobacionContratoDAttribute($dis_f_aprobacion_contrato_d)
    {
        $this->attributes[ 'dis_f_aprobacion_contrato_d' ] = strlen($dis_f_aprobacion_contrato_d) > 0 ? $dis_f_aprobacion_contrato_d : null;
    }

    public function setDisFAprobacionContratoAttribute($dis_f_aprobacion_contrato)
    {
        $this->attributes[ 'dis_f_aprobacion_contrato' ] = strlen($dis_f_aprobacion_contrato) > 0 ? $dis_f_aprobacion_contrato : null;
    }

    public function setDisAnticipoAttribute($dis_anticipo)
    {
        $this->attributes[ 'dis_anticipo' ] = strlen($dis_anticipo) > 0 ? $dis_anticipo : null;
    }

    public function setDisNumeroPagosAttribute($dis_numero_pagos)
    {
        $this->attributes[ 'dis_numero_pagos' ] = strlen($dis_numero_pagos) > 0 ? $dis_numero_pagos : null;
    }

    public function setDisFechasAttribute($dis_fechas)
    {
        $this->attributes[ 'dis_fechas' ] = strlen($dis_fechas) > 0 ? $dis_fechas : null;
    }

    public function setDisModPlazoAttribute($dis_mod_plazo)
    {
        $this->attributes[ 'dis_mod_plazo' ] = strlen($dis_mod_plazo) > 0 ? $dis_mod_plazo : null;
    }

    public function setDisResponsableModPlazoAttribute($dis_responsable_mod_plazo)
    {
        $this->attributes[ 'dis_responsable_mod_plazo' ] = strlen($dis_responsable_mod_plazo) > 0 ? $dis_responsable_mod_plazo : null;
    }

    public function setDisFNuevaFinalizaAttribute($dis_f_nueva_finaliza)
    {
        $this->attributes[ 'dis_f_nueva_finaliza' ] = strlen($dis_f_nueva_finaliza) > 0 ? $dis_f_nueva_finaliza : null;
    }

    public function setDisMontoModificadoAttribute($dis_monto_modificado)
    {
        $this->attributes[ 'dis_monto_modificado' ] = strlen($dis_monto_modificado) > 0 ? $dis_monto_modificado : null;
    }

    public function setDisMontoModificadoDAttribute($dis_monto_modificado_d)
    {
        $this->attributes[ 'dis_monto_modificado_d' ] = strlen($dis_monto_modificado_d) > 0 ? $dis_monto_modificado_d : null;
    }

    public function setDisResponsableModMontoAttribute($dis_responsable_mod_monto)
    {
        $this->attributes[ 'dis_responsable_mod_monto' ] = strlen($dis_responsable_mod_monto) > 0 ? $dis_responsable_mod_monto : null;
    }

    public function setDisPorcentajeCambioAttribute($dis_porcentaje_cambio)
    {
        $this->attributes[ 'dis_porcentaje_cambio' ] = strlen($dis_porcentaje_cambio) > 0 ? $dis_porcentaje_cambio : null;
    }

    public function setDisActaRecepcionAttribute($dis_acta_recepcion)
    {
        $this->attributes[ 'dis_acta_recepcion' ] = strlen($dis_acta_recepcion) > 0 ? $dis_acta_recepcion : null;
    }

    public function setDisFRecepcionAttribute($dis_f_recepcion)
    {
        $this->attributes[ 'dis_f_recepcion' ] = strlen($dis_f_recepcion) > 0 ? $dis_f_recepcion : null;
    }

    public function setDisFRecepcionDAttribute($dis_f_recepcion_d)
    {
        $this->attributes[ 'dis_f_recepcion_d' ] = strlen($dis_f_recepcion_d) > 0 ? $dis_f_recepcion_d : null;
    }

    public function setDisFFiniquitoAttribute($dis_f_finiquito)
    {
        $this->attributes[ 'dis_f_finiquito' ] = strlen($dis_f_finiquito) > 0 ? $dis_f_finiquito : null;
    }

    public function setDisFechaFiniquitoDetAttribute($dis_fecha_finiquito_det)
    {
        $this->attributes[ 'dis_fecha_finiquito_det' ] = strlen($dis_fecha_finiquito_det) > 0 ? $dis_fecha_finiquito_det : null;
    }

    public function setDisFianzaDetalleAttribute($dis_fianza_detalle)
    {
        $this->attributes[ 'dis_fianza_detalle' ] = strlen($dis_fianza_detalle) > 0 ? $dis_fianza_detalle : null;
    }

    public function setDisDetFianzaAttribute($dis_det_fianza)
    {
        $this->attributes[ 'dis_det_fianza' ] = strlen($dis_det_fianza) > 0 ? $dis_det_fianza : null;
    }

    public function setinstrumentoEiaAttribute($instrumento_eia)
    {
        $this->attributes[ 'instrumento_eia' ] = strlen($instrumento_eia) > 0 ? $instrumento_eia : null;
    }

    public function setelaboradoPorEiaAttribute($elaborado_por_eia)
    {
        $this->attributes[ 'elaborado_por_eia' ] = strlen($elaborado_por_eia) > 0 ? $elaborado_por_eia : null;
    }

    public function setFEiaAttribute($f_eia)
    {
        $this->attributes[ 'f_eia' ] = strlen($f_eia) > 0 ? $f_eia : null;
    }



    public function setFResolucionMarnAttribute($f_resolucion_marn)
    {
        $this->attributes[ 'f_resolucion_marn' ] = strlen($f_resolucion_marn) > 0 ? $f_resolucion_marn : null;
    }

    public function setMedidaMitigacionPresupuestoAttribute($medida_mitigacion_presupuesto)
    {
        $this->attributes[ 'medida_mitigacion_presupuesto' ] = strlen($medida_mitigacion_presupuesto) > 0 ? $medida_mitigacion_presupuesto : null;
    }

    public function setAgripAttribute($agrip)
    {
        $this->attributes[ 'agrip' ] = strlen($agrip) > 0 ? $agrip : null;
    }

    public function setagripFAttribute($agrip_f)
    {
        $this->attributes[ 'agrip_f' ] = strlen($agrip_f) > 0 ? $agrip_f : null;
    }

    public function setAgripElaboradoPorAttribute($agrip_elaborado_por)
    {
        $this->attributes[ 'agrip_elaborado_por' ] = strlen($agrip_elaborado_por) > 0 ? $agrip_elaborado_por : null;
    }

    public function setagripMedidaMitigacionAttribute($agrip_medida_mitigacion)
    {
        $this->attributes[ 'agrip_medida_mitigacion' ] = strlen($agrip_medida_mitigacion) > 0 ? $agrip_medida_mitigacion : null;
    }

    public function setResponsableEstudioTecnicoAttribute($responsable_estudio_tecnico)
    {
        $this->attributes[ 'responsable_estudio_tecnico' ] = strlen($responsable_estudio_tecnico) > 0 ? $responsable_estudio_tecnico : null;
    }



    public function setEstudioSueloFAttribute($estudio_suelo_f)
    {
        $this->attributes[ 'estudio_suelo_f' ] = strlen($estudio_suelo_f) > 0 ? $estudio_suelo_f : null;
    }

    public function setEstudioSueloPorAttribute($estudio_suelo_por)
    {
        $this->attributes[ 'estudio_suelo_por' ] = strlen($estudio_suelo_por) > 0 ? $estudio_suelo_por : null;
    }



    public function setEstudioTopografiaFAttribute($estudio_topografia_f)
    {
        $this->attributes[ 'estudio_topografia_f' ] = strlen($estudio_topografia_f) > 0 ? $estudio_topografia_f : null;
    }

    public function setEstudioTopografiaPorAttribute($estudio_topografia_por)
    {
        $this->attributes[ 'estudio_topografia_por' ] = strlen($estudio_topografia_por) > 0 ? $estudio_topografia_por : null;
    }

    public function setEstudioGeologicoAttribute($estudio_geologico)
    {
        $this->attributes[ 'estudio_geologico' ] = strlen($estudio_geologico) > 0 ? $estudio_geologico : null;
    }

    public function setEstudioGeologicoFAttribute($estudio_geologico_f)
    {
        $this->attributes[ 'estudio_geologico_f' ] = strlen($estudio_geologico_f) > 0 ? $estudio_geologico_f : null;
    }

    public function setEstudioGeologicoPorAttribute($estudio_geologico_por)
    {
        $this->attributes[ 'estudio_geologico_por' ] = strlen($estudio_geologico_por) > 0 ? $estudio_geologico_por : null;
    }

    public function setDisenoEstructuralAttribute($diseno_estructural)
    {
        $this->attributes[ 'diseno_estructural' ] = strlen($diseno_estructural) > 0 ? $diseno_estructural : null;
    }

    public function setDisenoEstructuralFAttribute($diseno_estructural_f)
    {
        $this->attributes[ 'diseno_estructural_f' ] = strlen($diseno_estructural_f) > 0 ? $diseno_estructural_f : null;
    }

    public function setDisenoEstructuralPorAttribute($diseno_estructural_por)
    {
        $this->attributes[ 'diseno_estructural_por' ] = strlen($diseno_estructural_por) > 0 ? $diseno_estructural_por : null;
    }

    public function setMemoriaCalculoAttribute($memoria_calculo)
    {
        $this->attributes[ 'memoria_calculo' ] = strlen($memoria_calculo) > 0 ? $memoria_calculo : null;
    }

    public function setMemoriaCalculoResponsableAttribute($memoria_calculo_responsable)
    {
        $this->attributes[ 'memoria_calculo_responsable' ] = strlen($memoria_calculo_responsable) > 0 ? $memoria_calculo_responsable : null;
    }

    public function setPlanoCompletoAttribute($plano_completo)
    {
        $this->attributes[ 'plano_completo' ] = strlen($plano_completo) > 0 ? $plano_completo : null;
    }

    public function setPlanoCompletoResponsableAttribute($plano_completo_responsable)
    {
        $this->attributes[ 'plano_completo_responsable' ] = strlen($plano_completo_responsable) > 0 ? $plano_completo_responsable : null;
    }

    public function setEspeTecnicaAttribute($espe_tecnica)
    {
        $this->attributes[ 'espe_tecnica' ] = strlen($espe_tecnica) > 0 ? $espe_tecnica : null;
    }

    public function setEspeTecnicaResponsableAttribute($espe_tecnica_responsable)
    {
        $this->attributes[ 'espe_tecnica_responsable' ] = strlen($espe_tecnica_responsable) > 0 ? $espe_tecnica_responsable : null;
    }

    public function setEspeGeneralAttribute($espe_general)
    {
        $this->attributes[ 'espe_general' ] = strlen($espe_general) > 0 ? $espe_general : null;
    }

    public function setEspeGeneralResponsableAttribute($espe_general_responsable)
    {
        $this->attributes[ 'espe_general_responsable' ] = strlen($espe_general_responsable) > 0 ? $espe_general_responsable : null;
    }

    public function setDispoEspecialAttribute($dispo_especial)
    {
        $this->attributes[ 'dispo_especial' ] = strlen($dispo_especial) > 0 ? $dispo_especial : null;
    }

    public function setDispoEspecialResponsableAttribute($dispo_especial_responsable)
    {
        $this->attributes[ 'dispo_especial_responsable' ] = strlen($dispo_especial_responsable) > 0 ? $dispo_especial_responsable : null;
    }

    public function setOtroEstudioAttribute($otro_estudio)
    {
        $this->attributes[ 'otro_estudio' ] = strlen($otro_estudio) > 0 ? $otro_estudio : null;
    }

    public function setOtroEstudioResponsableAttribute($otro_estudio_responsable)
    {
        $this->attributes[ 'otro_estudio_responsable' ] = strlen($otro_estudio_responsable) > 0 ? $otro_estudio_responsable : null;
    }

    public function setPresupuestoProyectoAttribute($presupuesto_proyecto)
    {
        $this->attributes[ 'presupuesto_proyecto' ] = strlen($presupuesto_proyecto) > 0 ? $presupuesto_proyecto : null;
    }

    public function setPresupuestoAprobacionFAttribute($presupuesto_aprobacion_f)
    {
        $this->attributes[ 'presupuesto_aprobacion_f' ] = strlen($presupuesto_aprobacion_f) > 0 ? $presupuesto_aprobacion_f : null;
    }

    public function setEstadoActualProyectoDAttribute($estado_actual_proyecto_d)
    {
        $this->attributes[ 'estado_actual_proyecto_d' ] = strlen($estado_actual_proyecto_d) > 0 ? $estado_actual_proyecto_d : null;
    }

    public function setEstadoActualProyectoAttribute($estado_actual_proyecto)
    {
        $this->attributes[ 'estado_actual_proyecto' ] = strlen($estado_actual_proyecto) > 0 ? $estado_actual_proyecto : null;
    }

    public function setavanceFisicoAttribute($avance_fisico)
    {
        $this->attributes[ 'avance_fisico' ] = strlen($avance_fisico) > 0 ? $avance_fisico : null;
    }

    public function setavanceFinancieroAttribute($avance_financiero)
    {
        $this->attributes[ 'avance_financiero' ] = strlen($avance_financiero) > 0 ? $avance_financiero : null;
    }

    public function setModalidadContratacionAttribute($modalidad_contratacion)
    {
        $this->attributes[ 'modalidad_contratacion                                    
            ' ] = strlen($modalidad_contratacion) > 0 ? $modalidad_contratacion : null;
    }

    public function setFuenteFinanciamientoDisAttribute($fuente_financiamiento_dis)
    {
        $this->attributes[ 'fuente_financiamiento_dis' ] = strlen($fuente_financiamiento_dis) > 0 ? $fuente_financiamiento_dis : null;
    }

    public function setMontoAsignadoDisAttribute($monto_asignado_dis)
    {
        $this->attributes[ 'monto_asignado_dis' ] = strlen($monto_asignado_dis) > 0 ? $monto_asignado_dis : null;
    }

/*    public function setNogAttribute($nog)
    {
        $this->attributes[ 'nog                                    
            ' ] = strlen($nog) > 0 ? $nog : null;
    }*/

  /*  public function setNogFAttribute($nog_f)
    {
        $this->attributes[ 'nog_f' ] = strlen($nog_f) > 0 ? $nog_f : null;
    }*/

    public function setFuenteFSupAttribute($fuente_f_sup)
    {
        $this->attributes[ 'fuente_f_sup                                    
            ' ] = strlen($fuente_f_sup) > 0 ? $fuente_f_sup : null;
    }

    public function setMontoSupAttribute($monto_sup)
    {
        $this->attributes[ 'monto_sup                                    
            ' ] = strlen($monto_sup) > 0 ? $monto_sup : null;
    }

    public function setFuenteFEjecucionAttribute($fuente_f_ejecucion)
    {
        $this->attributes[ 'fuente_f_ejecucion                                    
            ' ] = strlen($fuente_f_ejecucion) > 0 ? $fuente_f_ejecucion : null;
    }

    public function setMontoEjecucionAttribute($monto_ejecucion)
    {
        $this->attributes[ 'monto_ejecucion                                    
            ' ] = strlen($monto_ejecucion) > 0 ? $monto_ejecucion : null;
    }

    public function setFuenteFAmbientalAttribute($fuente_f_ambiental)
    {
        $this->attributes[ 'fuente_f_ambiental' ] = strlen($fuente_f_ambiental) > 0 ? $fuente_f_ambiental : null;
    }

    public function setMontoAmbientalAttribute($monto_ambiental)
    {
        $this->attributes[ 'monto_ambiental' ] = strlen($monto_ambiental) > 0 ? $monto_ambiental : null;
    }

    public function setFuenteRiesgoAttribute($fuente_riesgo)
    {
        $this->attributes[ 'fuente_riesgo' ] = strlen($fuente_riesgo) > 0 ? $fuente_riesgo : null;
    }

    public function setMontoRiesgoAttribute($monto_riesgo)
    {
        $this->attributes[ 'monto_riesgo' ] = strlen($monto_riesgo) > 0 ? $monto_riesgo : null;
    }

    public function setEstudioViabilidadAttribute($estudio_viabilidad)
    {
        $this->attributes[ 'estudio_viabilidad' ] = strlen($estudio_viabilidad) > 0 ? $estudio_viabilidad : null;
    }

    public function setaprobacionViabilidadAttribute($aprobacion_viabilidad)
    {
        $this->attributes[ 'aprobacion_viabilidad' ] = strlen($aprobacion_viabilidad) > 0 ? $aprobacion_viabilidad : null;
    }

    public function setResultadoPrincipalImpactoAttribute($resultado_principal_impacto)
    {
        $this->attributes[ 'resultado_principal_impacto' ] = strlen($resultado_principal_impacto) > 0 ? $resultado_principal_impacto : null;
    }

    public function setObservacionesMarnAttribute($observaciones_marn)
    {
        $this->attributes[ 'observaciones_marn' ] = strlen($observaciones_marn) > 0 ? $observaciones_marn : null;
    }

    public function setLinksMarnAttribute($links_marn)
    {
        $this->attributes[ 'links_marn' ] = strlen($links_marn) > 0 ? $links_marn : null;
    }

    public function setObservacionesSueloAttribute($observaciones_suelo)
    {
        $this->attributes[ 'observaciones_suelo' ] = strlen($observaciones_suelo) > 0 ? $observaciones_suelo : null;
    }

    public function setLinksSueloAttribute($links_suelo)
    {
        $this->attributes[ 'links_suelo' ] = strlen($links_suelo) > 0 ? $links_suelo : null;
    }

    public function setObservacionesAgripAttribute($observaciones_agrip)
    {
        $this->attributes[ 'observaciones_agrip' ] = strlen($observaciones_agrip) > 0 ? $observaciones_agrip : null;
    }

    public function setLinksAgripAttribute($links_agrip)
    {
        $this->attributes[ 'links_agrip' ] = strlen($links_agrip) > 0 ? $links_agrip : null;
    }

    public function setObservacionesTopografiaAttribute($observaciones_topografia)
    {
        $this->attributes[ 'observaciones_topografia' ] = strlen($observaciones_topografia) > 0 ? $observaciones_topografia : null;
    }

    public function setLinksTopografiaAttribute($links_topografia)
    {
        $this->attributes[ 'links_topografia' ] = strlen($links_topografia) > 0 ? $links_topografia : null;
    }

    public function setObservacionesGeologicoAttribute($observaciones_geologico)
    {
        $this->attributes[ 'observaciones_geologico' ] = strlen($observaciones_geologico) > 0 ? $observaciones_geologico : null;
    }

    public function setLinksGeologicoAttribute($links_geologico)
    {
        $this->attributes[ 'links_geologico' ] = strlen($links_geologico) > 0 ? $links_geologico : null;
    }

    public function setObservacionesHidrogeologicoAttribute($observaciones_hidrogeologico)
    {
        $this->attributes[ 'observaciones_hidrogeologico' ] = strlen($observaciones_hidrogeologico) > 0 ? $observaciones_hidrogeologico : null;
    }

    public function setEstudioHidrogeologicoAttribute($estudio_hidrogeologico)
    {
        $this->attributes[ 'estudio_hidrogeologico' ] = strlen($estudio_hidrogeologico) > 0 ? $estudio_hidrogeologico : null;
    }

    public function setFechaHidrogeologicoAttribute($fecha_hidrogeologico)
    {
        $this->attributes[ 'fecha_hidrogeologico' ] = strlen($fecha_hidrogeologico) > 0 ? $fecha_hidrogeologico : null;
    }

    public function setHechoPorHidrogeologicoAttribute($hecho_por_hidrogeologico)
    {
        $this->attributes[ 'hecho_por_hidrogeologico' ] = strlen($hecho_por_hidrogeologico) > 0 ? $hecho_por_hidrogeologico : null;
    }

    public function setLinksHidrogeologicoAttribute($links_hidrogeologico)
    {
        $this->attributes[ 'links_hidrogeologico' ] = strlen($links_hidrogeologico) > 0 ? $links_hidrogeologico : null;
    }

    public function setObservacionesEstructuralAttribute($observaciones_estructural)
    {
        $this->attributes[ 'observaciones_estructural' ] = strlen($observaciones_estructural) > 0 ? $observaciones_estructural : null;
    }

    public function setLinksEstructuralAttribute($links_estructural)
    {
        $this->attributes[ 'links_estructural' ] = strlen($links_estructural) > 0 ? $links_estructural : null;
    }

    public function setMemoriaCalculoHechoPorAttribute($memoria_calculo_hecho_por)
    {
        $this->attributes[ 'memoria_calculo_hecho_por' ] = strlen($memoria_calculo_hecho_por) > 0 ? $memoria_calculo_hecho_por : null;
    }

    public function setObservacionesMemoriaCalculoAttribute($observaciones_memoria_calculo)
    {
        $this->attributes[ 'observaciones_memoria_calculo' ] = strlen($observaciones_memoria_calculo) > 0 ? $observaciones_memoria_calculo : null;
    }

    public function setLiksMemoriaCalculoAttribute($liks_memoria_calculo)
    {
        $this->attributes[ 'liks_memoria_calculo' ] = strlen($liks_memoria_calculo) > 0 ? $liks_memoria_calculo : null;
    }


    public function setLiksPlanoCompletoAttribute($liks_plano_completo)
    {
        $this->attributes[ 'liks_plano_completo' ] = strlen($liks_plano_completo) > 0 ? $liks_plano_completo : null;
    }

    public function setObservacionesPlanoCompletoAttribute($observaciones_plano_completo)
    {
        $this->attributes[ 'observaciones_plano_completo' ] = strlen($observaciones_plano_completo) > 0 ? $observaciones_plano_completo : null;
    }

    public function setObservacionesDispoEspeAttribute($observaciones_dispo_espe)
    {
        $this->attributes[ 'observaciones_dispo_espe' ] = strlen($observaciones_dispo_espe) > 0 ? $observaciones_dispo_espe : null;
    }

    public function setLinksEspeGeneralAttribute($links_espe_general)
    {
        $this->attributes[ 'links_espe_general' ] = strlen($links_espe_general) > 0 ? $links_espe_general : null;
    }

    public function setObservacionesEspeTecnicaAttribute($observaciones_espe_tecnica)
    {
        $this->attributes[ 'observaciones_espe_tecnica' ] = strlen($observaciones_espe_tecnica) > 0 ? $observaciones_espe_tecnica : null;
    }

    public function setLinksEspeTecnicaAttribute($links_espe_tecnica)
    {
        $this->attributes[ 'links_espe_tecnica' ] = strlen($links_espe_tecnica) > 0 ? $links_espe_tecnica : null;
    }


    public function setLinksDispoEspecialAttribute($links_dispo_especial)
    {
        $this->attributes[ 'links_dispo_especial' ] = strlen($links_dispo_especial) > 0 ? $links_dispo_especial : null;
    }

    public function setObservaciones_otroAttribute($observaciones_otro)
    {
        $this->attributes[ 'observaciones_otro' ] = strlen($observaciones_otro) > 0 ? $observaciones_otro : null;
    }

    public function setLinks_otroAttribute($links_otro)
    {
        $this->attributes[ 'links_otro' ] = strlen($links_otro) > 0 ? $links_otro : null;
    }

    public function setAEstudioFactiAttribute($a_estudio_facti)
    {
        $this->attributes[ 'a_estudio_facti' ] = strlen($a_estudio_facti) > 0 ? $a_estudio_facti : null;
    }

    public function setDisTerminosReferenciaAttribute($dis_terminos_referencia)
    {
        $this->attributes[ 'dis_terminos_referencia' ] = strlen($dis_terminos_referencia) > 0 ? $dis_terminos_referencia : null;
    }

    public function setInstEaAttribute($inst_ea)
    {
        $this->attributes[ 'inst_ea' ] = strlen($inst_ea) > 0 ? $inst_ea : null;
    }

    public function setLinksInstEaAttribute($links_inst_ea)
    {
        $this->attributes[ 'links_inst_ea' ] = strlen($links_inst_ea) > 0 ? $links_inst_ea : null;
    }

    public function setInstEaResoponsableAttribute($inst_ea_resoponsable)
    {
        $this->attributes[ 'inst_ea_resoponsable' ] = strlen($inst_ea_resoponsable) > 0 ? $inst_ea_resoponsable : null;
    }

    public function setInstEaDAttribute($inst_ea_d)
    {
        $this->attributes[ 'inst_ea_d' ] = strlen($inst_ea_d) > 0 ? $inst_ea_d : null;
    }

    public function setLinksPresupuestoAttribute($links_presupuesto)
    {
        $this->attributes[ 'links_presupuesto' ] = strlen($links_presupuesto) > 0 ? $links_presupuesto : null;
    }

    public function setCostoEstimadoProyectoAttribute($costo_estimado_proyecto)
    {
        $this->attributes[ 'costo_estimado_proyecto' ] = strlen($costo_estimado_proyecto) > 0 ? $costo_estimado_proyecto : null;
    }

    public function setRespDisenoAttribute($resp_diseno)
    {
        $this->attributes[ 'resp_diseno' ] = strlen($resp_diseno) > 0 ? $resp_diseno : null;
    }


    public function setAEntidadesAttribute($a_entidades)
    {
        $this->attributes[ 'a_entidades' ] = strlen($a_entidades) > 0 ? $a_entidades : null;
    }

    public function setADerechoPasoAttribute($a_derecho_paso)
    {
        $this->attributes[ 'a_derecho_paso' ] = strlen($a_derecho_paso) > 0 ? $a_derecho_paso : null;
    }

    /**
     * @return date
     */
    public function getFResolucionMarnAttribute($f_resolucion_marn)
    {
        if ($f_resolucion_marn != null)
            return Carbon::createFromFormat('Y-m-d', $f_resolucion_marn);
        return null;
    }

    /**
     * @return string
     */
    public function getResolucionMarnAttribute($resolucion_marn)
    {
        return strlen($resolucion_marn) > 0 ? $resolucion_marn : '';
    }

    /**
     * @return string
     */
    public function getMedidaMitigacionPresupuestoAttribute($medida_mitigacion_presupuesto)
    {
        return strlen($medida_mitigacion_presupuesto) > 0 ? $medida_mitigacion_presupuesto : '';
    }

    /**
     * @param string $resolucion_marn
     */

    public function getEstudioSueloInfoAttribute()
    {
        $dato = 'SI, elaborado por:' . $this->attributes[ 'estudio_suelo_por' ];
        $texto = $this->attributes[ 'estudio_suelo' ] > 0 ? $dato : 'NO';
        return $texto;
    }

    public function getEstudioTopografiaInfoAttribute()
    {
        $dato = 'SI, elaborado por: ' . $this->attributes[ 'estudio_topografia_por' ];
        $texto = $this->attributes[ 'estudio_topografia' ] > 0 ? $dato : 'NO';
        return $texto;
    }

    public function getEstudioGeologicoInfoAttribute()
    {
        $dato = 'SI, elaborado por: ' . $this->attributes[ 'estudio_geologico_por' ];
        $texto = $this->attributes[ 'estudio_geologico' ] > 0 ? $dato : 'NO';
        return $texto;
    }

    public function getEspeTecnicaInfoAttribute()
    {
        $dato = 'SI, elaborado por: ' . $this->attributes[ 'espe_tecnica_responsable' ];
        $texto = $this->attributes[ 'espe_tecnica' ] > 0 ? $dato : 'NO';
        return $texto;
    }

    public function getPlanoCompletoInfoAttribute()
    {
        $dato = 'SI, elaborado por: ' . $this->attributes[ 'plano_completo_responsable' ];
        $texto = $this->attributes[ 'plano_completo' ] > 0 ? $dato : 'NO';
        return $texto;
    }

    public function getDispoEspecialInfoAttribute()
    {
        $dato = 'SI, elaborado por: ' . $this->attributes[ 'dispo_especial_responsable' ];
        $texto = $this->attributes[ 'dispo_especial' ] > 0 ? $dato : 'NO';
        return $texto;
    }

    public function getDisenoEstructuralInfoAttribute()
    {
        $dato = 'SI, elaborado por: ' . $this->attributes[ 'diseno_estructural_por' ];
        $texto = $this->attributes[ 'diseno_estructural' ] > 0 ? $dato : 'NO';
        return $texto;
    }

    public function getEspeGeneralInfoAttribute()
    {
        $dato = 'SI, elaborado por: ' . $this->attributes[ 'espe_general_responsable' ];
        $texto = $this->attributes[ 'espe_general' ] > 0 ? $dato : 'NO';
        return $texto;
    }

    public function getFMTAgripAttribute()
    {
        if ($this->agrip == 1) {
            $resp = 'Si';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtestudioSueloAttribute()
    {
        if ($this->estudio_suelo == 1) {
            $resp = 'Si';
        } else if ($this->estudio_suelo == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtestudioGeologicoAttribute()
    {
        if ($this->estudio_geologico == 1) {
            $resp = 'Si';
        } else if ($this->estudio_suelo == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtEstudioHidrogeologicoAttribute()
    {
        if ($this->estudio_hidrogeologico == 1) {
            $resp = 'Si';
        } else if ($this->estudio_hidrogeologico == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtDisenoEstructuralAttribute()
    {
        if ($this->diseno_estructural == 1) {
            $resp = 'Si';
        } else if ($this->diseno_estructural == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtmemoriaCalculoAttribute()
    {
        if ($this->memoria_calculo == 1) {
            $resp = 'Si';
        } else if ($this->diseno_estmemoria_calculoructural == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtplanoCompletoAttribute()
    {
        if ($this->plano_completo == 1) {
            $resp = 'Si';
        } else if ($this->plano_completo == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtEspeGeneralAttribute()
    {
        if ($this->espe_general == 1) {
            $resp = 'Si';
        } else if ($this->espe_general == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtEspeTecnicaAttribute()
    {
        if ($this->espe_tecnica == 1) {
            $resp = 'Si';
        } else if ($this->espe_tecnica == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtDispoEspecialAttribute()
    {
        if ($this->dispo_especial == 1) {
            $resp = 'Si';
        } else if ($this->dispo_especial == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtOtroEstudioAttribute()
    {
        if ($this->otro_estudio == 1) {
            $resp = 'Si';
        } else if ($this->otro_estudio == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtInstEaAttribute()
    {
        if ($this->inst_ea == 1) {
            $resp = 'Si';
        } else if ($this->inst_ea == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    public function getFmtPresupuestoProyectoAttribute()
    {
        if ($this->presupuesto_proyecto == 1) {
            $resp = 'Si';
        } else if ($this->presupuesto_proyecto == 0) {
            $resp = 'No';
        } else {
            $resp = 'No';
        }
        return $resp;
    }

    /**
     * @return string
     */
    public function rel_proyecto()
    {
        return $this->belongsTo(\App\Models\cost_proyecto::class, 'id_proyecto', 'id_proyecto');
    }



    public static function dis_plan_t($id_proyecto)
    {
        $id_dp = Self::where('id_proyecto', $id_proyecto)->get();
        return $id_dp;
    }

    public static function criterios_default($request = 0)
    {
        $filtros = new \stdClass();
        $filtros->f_resolucion_marn = date("Y-m-d");
        $filtros->agrip_f = date("Y-m-d");
        $filtros->url = "";

        //Para presentacion

        $filtros->fmt_f_resolucion_marn = \Carbon\Carbon::createFromFormat("Y-m-d", $filtros->f_resolucion_marn)->format("d-m-Y");
        $filtros->agrip_f = $request->agrip_f != '-0001-11-30 00:00:00.000000' ? \Carbon\Carbon::createFromFormat("Y-m-d", $filtros->agrip_f)->format("d-m-Y") : '';

        //Filtro de caimu según el usuario
        if (\Auth::check()) {
            if (\Auth::user()->id_caimu > 0) {
                $filtros->id_caimu = \Auth::user()->id_caimu;
            }
        }

        // Procesar request (opcional)
        if (is_object($request)) {
            $filtros->url = $request->url();

            if (isset($request->f_resolucion_marn)) {
                $filtros->f_resolucion_marn = $request->f_resolucion_marn_submit;
            }

            if (isset($request->agrip_f)) {
                if ($request->agrip_f != '-0001-11-30 00:00:00.000000') {
                    $filtros->f_resolucion_marn = $request->f_resolucion_marn_submit;
                } else {
                    $filtros->f_resolucion_marn = '';
                }
            }
        }

        return $filtros;
    }

    public function rel_presupuesto_moneda()
    {
        return $this->belongsTo(\App\Models\cat_item::class,'presupuesto_moneda','id_item');
    }

    public function getFmtPresupuestoMonedaAttribute()
    {
        if ($this->rel_presupuesto_moneda) {
            return $this->rel_presupuesto_moneda->descripcion;
        } else {
            return "-";
        }
    }

}
