<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_liquidacion_monto
 * @package App\Models
 * @version March 25, 2019, 5:04 am UTC
 *
 * @property integer id_liquidacion
 * @property string documento_cambio
 * @property float suma_mod
 * @property string razon_cambio
 * @property float monto_actualizado
 * @property string no_fianza
 */
class cost_liquidacion_monto extends Model
{
   // use SoftDeletes;

    public $table = 'cost_liquidacion_monto';
    protected $primaryKey='id_liquidacion_monto';
    public $timestamps=false;
    
  //  const CREATED_AT = 'created_at';
  //  const UPDATED_AT = 'updated_at';


 //   protected $dates = ['deleted_at'];


    public $fillable = [
        'id_proyecto',
        'documento_cambio',
        'suma_mod',
        'razon_cambio',
        'monto_actualizado',
        'no_fianza',

        'monto_antiguo',
        'moneda_antigua',
        'monto_actualizado',
        'moneda',
    ];



    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'documento_cambio' => 'string',
        'suma_mod' => 'float',
        'razon_cambio' => 'string',
        'monto_actualizado' => 'float',
        'no_fianza' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function setNoModificacionesPlazoAttribute($no_modificaciones_plazo){ $this->attributes['no_modificaciones_plazo']= strlen($no_modificaciones_plazo)>0 ? $no_modificaciones_plazo  : null;}
    public function setCantidadTiempoModificadoAttribute($cantidad_tiempo_modificado){ $this->attributes['cantidad_tiempo_modificado']= strlen($cantidad_tiempo_modificado)>0 ? $cantidad_tiempo_modificado  : null;}
    public function setNuevaFechaFinalizaAttribute($nueva_fecha_finaliza){ $this->attributes['nueva_fecha_finaliza']= strlen($nueva_fecha_finaliza)>0 ? $nueva_fecha_finaliza  : null;}
    public function setRazonesCambioPlazoAttribute($razones_cambio_plazo){ $this->attributes['razones_cambio_plazo']= strlen($razones_cambio_plazo)>0 ? $razones_cambio_plazo  : null;}
    public function setNoFianzaAttribute($no_fianza){ $this->attributes['no_fianza']= strlen($no_fianza)>0 ? $no_fianza  : null;}


    public function getIdAttribute() {
        return $this->id_liquidacion_monto;
    }
    public static function monto($id_proyecto){
        $id_ic= Self::where('id_proyecto',$id_proyecto)->get();
        return $id_ic;
    }

    public function rel_moneda_actual_oc()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'moneda', 'id_item');
    }

    public function rel_moneda_antigua_oc()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'moneda_antigua', 'id_item');
    }

    public function getFmtMonedaAntiguaAttribute()
    {
        if ($this->rel_moneda_antigua_oc) {
            return $this->rel_moneda_antigua_oc->descripcion;
        } else {
            return "";
        }
    }

    public function getFmtMonedaActualAttribute()
    {
        if ($this->rel_moneda_actual_oc) {
            return $this->rel_moneda_actual_oc->descripcion;
        } else {
            return "";
        }
    }
}
