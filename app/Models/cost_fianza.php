<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_fianza
 * @package App\Models
 * @version January 17, 2019, 5:17 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property integer id_proyecto
 * @property integer tipo
 * @property integer tipo_fianza
 * @property string tipo_fianza_d
 * @property string no_fianza
 * @property float monto
 * @property date fecha_fianza
 * @property string link
 */
class cost_fianza extends Model
{
 //   use SoftDeletes;

    public $table = 'cost_fianza';
    protected $primaryKey='id_fianza';
    public $timestamps=false;
    
 //   const CREATED_AT = 'created_at';
 //   const UPDATED_AT = 'updated_at';


  //  protected $dates = ['deleted_at'];


    public $fillable = [
        'id_proyecto',
        'id_fase',
        'tipo',
        'tipo_fianza',
        'tipo_fianza_d',
        'no_fianza',
        'monto',
        'fecha_fianza',
        'link',
        'clase_fianza',
        'plazo_fianza',
        'entidad_afianzadora'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'id_fase' => 'integer',
        'tipo' => 'integer',
        'tipo_fianza' => 'integer',
        'tipo_fianza_d' => 'string',
        'no_fianza' => 'string',
        'monto' => 'float',
        'fecha_fianza' => 'date',
        'link' => 'string',
        'clase_fianza' => 'string',
        'plazo_fianza' => 'string',
        'entidad_afianzadora' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function setTipoAttribute($tipo)
    {
        $this->attributes[ 'tipo' ] = $tipo > 0 ? $tipo : 0;
    }

    public function setTipoFianzaAttribute($tipo_fianza)
    {
        $this->attributes[ 'tipo_fianza' ] = $tipo_fianza > 0 ? $tipo_fianza : 0;
    }

    public function setTipoFianzaDAttribute($tipo_fianza_d)
    {
        $this->attributes[ 'tipo_fianza_d' ] = strlen($tipo_fianza_d) > 0 ? $tipo_fianza_d : Null;
    }

    public function setNoFianzaAttribute($no_fianza)
    {
        $this->attributes[ 'no_fianza' ] = strlen($no_fianza) > 0 ? $no_fianza : 0;
    }

    public function setMontoAttribute($monto)
    {
        $this->attributes[ 'monto' ] = strlen($monto) > 0 ? $monto : 0;
    }

    public function setFechaFianzaAttribute($fecha_fianza)
    {
        $this->attributes[ 'fecha_fianza' ] = strlen($fecha_fianza) > 0 ? $fecha_fianza : Null;
    }

    public function setLinkAttribute($link)
    {
        $this->attributes[ 'link' ] = strlen($link) > 0 ? $link : Null;
    }

    public function setClaseFianzaAttribute($clase_fianza)
    {
        $this->attributes[ 'clase_fianza' ] = strlen($clase_fianza) > 0 ? $clase_fianza : Null;
    }

    public function setPlazoFianzaAttribute($plazo_fianza){

        $this->attributes[ 'plazo_fianza' ] = strlen($plazo_fianza) > 0 ? $plazo_fianza : Null;
    }


    public function setEntidadAfianzadoraAttribute($entidad_afianzadora){

        $this->attributes[ 'entidad_afianzadora' ] = strlen($entidad_afianzadora) > 0 ? $entidad_afianzadora : 0;
    }


    public function getFmtEntidadAfianzadoraAttribute() {
        if($this->rel_entidad_afianzadora) {
            return $this->rel_entidad_afianzadora->descripcion;
        }
        else {

                return "-";

        }
    }

    public function rel_entidad_afianzadora(){
        return $this->belongsTo(\App\Models\cat_item::class,'entidad_afianzadora','id_item');

    }

    public function getFmtTipoAttribute() {
        if($this->rel_tipo) {
            return $this->rel_tipo->descripcion;
        }
        else {

            return "-";

        }
    }

    public function rel_tipo(){
        return $this->belongsTo(\App\Models\cat_item::class,'tipo','id_item');

    }

    public function getIdAttribute() {
        return $this->id_fianza;
    }

    public static function fianza_p($id_proyecto, $id_fase){
        $id_fp= Self::where('id_proyecto',$id_proyecto)->where('id_fase',$id_fase)->get();
        return $id_fp;
    }

}
