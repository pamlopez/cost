<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_proyecto_planifica
 * @package App\Models
 * @version October 30, 2018, 5:08 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property integer id_proyecto
 * @property date bases_concurso
 * @property date bases_f_publicacion
 * @property date bases_f_ultima_mod
 * @property integer no_inconformidades
 * @property integer no_inconformidades_r
 * @property string oferente
 * @property string acta_adjudicacion
 * @property string alcance_contrato
 * @property integer programa_ejecucion
 * @property integer estado_contrato
 * @property string tipo_no_contrato
 * @property string empresa_constructora
 * @property float monto_contrato
 * @property date f_aprobacion_contrato
 * @property string nombre_firmante
 * @property date nombramiento_f
 * @property string plazo_contrato
 * @property date f_inicio_contrato
 * @property date f_autoriza_bitacora
 * @property float monto_anticipo
 * @property integer no_estima_pagada
 * @property float monto_estima_pagada
 * @property integer no_pagos_voac
 * @property string pagos_voac_aprobado
 * @property string acta_inicio
 * @property date acta_inicio_f
 * @property string acta_recepcion
 * @property date acta_recepcion_f
 * @property string acta_liquidacion
 * @property string acta_aprobacion_liquidacion
 * @property string finiquito_fechas
 * @property string no_fianza_sos_obra
 * @property date no_fianza_sos_obra_f
 * @property float monto_fianza
 * @property string no_fianza_cumple_contrato
 * @property string no_fianza_cumple_contrato_f
 * @property float monto_fianza_cumple
 * @property string no_fianza_anticipo
 * @property date no_fianza_anticipo_f
 * @property float monto_fianza_anticipo
 * @property string no_fianza_conser_obra
 * @property date no_fianza_conser_obra_f
 * @property float monto_fianza_conser_obra
 * @property string no_fianza_saldo_deudor
 * @property date no_fianza_saldo_deudor_f
 * @property float monto_fianza_saldo
 * @property string entidad_afianzadora
 * @property string clase_fianza
 * @property string doc_cambios_contrato
 * @property float suma_mod_monto
 * @property string razon_cambio_monto
 * @property float monto_actualizado_contrato
 * @property string total_pagos
 * @property integer no_mod_plazo_contrato
 * @property integer cantidad_tiempo_mod
 * @property date nueva_f_finaliza
 * @property string razon_cambio_plazo
 * @property integer no_mod_alcance_obra
 * @property string razon_cambio_alcance
 * @property string alcance_real_obra
 * @property string programa_trabajo_actualizado
 * @property string informe_avance_pago
 * @property integer planos_completos
 * @property string reporte_eva_au
 * @property string informe_termina_proyecto
 * @property string informe_evalua_pro
 * @property integer estado_actual
 */
class cost_proyecto_planifica extends Model
{
    //use SoftDeletes;

    public $table = 'cost_proyecto_planifica';
    protected $primaryKey='id_proyecto_planifica';
    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];


    public $fillable = [
        'id_proyecto',
        'bases_concurso',
        'bases_f_publicacion',
        'bases_f_ultima_mod',
        'no_inconformidades',
        'no_inconformidades_r',
        'oferente',
        'acta_adjudicacion',
        'alcance_contrato',
        'programa_ejecucion',
        'estado_contrato',
        'tipo_no_contrato',
        'empresa_constructora',
        'monto_contrato',
        'f_aprobacion_contrato',
        'nombre_firmante',
        'nombramiento_f',
        'plazo_contrato',
        'f_inicio_contrato',
        'f_autoriza_bitacora',
        'monto_anticipo',
        'no_estima_pagada',
        'monto_estima_pagada',
        'no_pagos_voac',
        'pagos_voac_aprobado',
        'acta_inicio',
        'acta_inicio_f',
        'acta_recepcion',
        'acta_recepcion_f',
        'acta_liquidacion',
        'acta_aprobacion_liquidacion',
        'finiquito_fechas',
        'no_fianza_sos_obra',
        'no_fianza_sos_obra_f',
        'monto_fianza',
        'no_fianza_cumple_contrato',
        'no_fianza_cumple_contrato_f',
        'monto_fianza_cumple',
        'no_fianza_anticipo',
        'no_fianza_anticipo_f',
        'monto_fianza_anticipo',
        'no_fianza_conser_obra',
        'no_fianza_conser_obra_f',
        'monto_fianza_conser_obra',
        'no_fianza_saldo_deudor',
        'no_fianza_saldo_deudor_f',
        'monto_fianza_saldo',
        'entidad_afianzadora',
        'clase_fianza',
        'doc_cambios_contrato',
        'suma_mod_monto',
        'razon_cambio_monto',
        'monto_actualizado_contrato',
        'total_pagos',
        'no_mod_plazo_contrato',
        'cantidad_tiempo_mod',
        'nueva_f_finaliza',
        'razon_cambio_plazo',
        'no_mod_alcance_obra',
        'razon_cambio_alcance',
        'alcance_real_obra',
        'programa_trabajo_actualizado',
        'informe_avance_pago',
        'planos_completos',
        'reporte_eva_au',
        'informe_termina_proyecto',
        'informe_evalua_pro',
        'estado_actual'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'bases_concurso' => 'date',
        'bases_f_publicacion' => 'date',
        'bases_f_ultima_mod' => 'date',
        'no_inconformidades' => 'integer',
        'no_inconformidades_r' => 'integer',
        'oferente' => 'string',
        'acta_adjudicacion' => 'string',
        'alcance_contrato' => 'string',
        'programa_ejecucion' => 'integer',
        'estado_contrato' => 'integer',
        'tipo_no_contrato' => 'string',
        'empresa_constructora' => 'string',
        'monto_contrato' => 'float',
        'f_aprobacion_contrato' => 'date',
        'nombre_firmante' => 'string',
        'nombramiento_f' => 'date',
        'plazo_contrato' => 'string',
        'f_inicio_contrato' => 'date',
        'f_autoriza_bitacora' => 'date',
        'monto_anticipo' => 'float',
        'no_estima_pagada' => 'integer',
        'monto_estima_pagada' => 'float',
        'no_pagos_voac' => 'integer',
        'pagos_voac_aprobado' => 'string',
        'acta_inicio' => 'string',
        'acta_inicio_f' => 'date',
        'acta_recepcion' => 'string',
        'acta_recepcion_f' => 'date',
        'acta_liquidacion' => 'string',
        'acta_aprobacion_liquidacion' => 'string',
        'finiquito_fechas' => 'string',
        'no_fianza_sos_obra' => 'string',
        'no_fianza_sos_obra_f' => 'date',
        'monto_fianza' => 'float',
        'no_fianza_cumple_contrato' => 'string',
        'no_fianza_cumple_contrato_f' => 'string',
        'monto_fianza_cumple' => 'float',
        'no_fianza_anticipo' => 'string',
        'no_fianza_anticipo_f' => 'date',
        'monto_fianza_anticipo' => 'float',
        'no_fianza_conser_obra' => 'string',
        'no_fianza_conser_obra_f' => 'date',
        'monto_fianza_conser_obra' => 'float',
        'no_fianza_saldo_deudor' => 'string',
        'no_fianza_saldo_deudor_f' => 'date',
        'monto_fianza_saldo' => 'float',
        'entidad_afianzadora' => 'string',
        'clase_fianza' => 'string',
        'doc_cambios_contrato' => 'string',
        'suma_mod_monto' => 'float',
        'razon_cambio_monto' => 'string',
        'monto_actualizado_contrato' => 'float',
        'total_pagos' => 'string',
        'no_mod_plazo_contrato' => 'integer',
        'cantidad_tiempo_mod' => 'integer',
        'nueva_f_finaliza' => 'date',
        'razon_cambio_plazo' => 'string',
        'no_mod_alcance_obra' => 'integer',
        'razon_cambio_alcance' => 'string',
        'alcance_real_obra' => 'string',
        'programa_trabajo_actualizado' => 'string',
        'informe_avance_pago' => 'string',
        'planos_completos' => 'integer',
        'reporte_eva_au' => 'string',
        'informe_termina_proyecto' => 'string',
        'informe_evalua_pro' => 'string',
        'estado_actual' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getIdAttribute() {
        return $this->id_proyecto_planifica;
    }

    public function rel_proyecto()
    {
        return $this->belongsTo(\App\Models\cost_proyecto::class,'id_proyecto','id_proyecto');
    }

    /**
     * @return string
     */
    public function getMontoDolaresAttribute()
    {
        return ($this->monto_contrato / 7.75);
    }
}
