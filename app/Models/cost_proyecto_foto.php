<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class cost_proyecto_foto extends Model
{
    public $table = 'cost_proyecto_foto';
    protected $primaryKey='id_proyecto_foto';
    public $timestamps=false;
}
