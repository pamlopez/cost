<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

/**
 * Class cost_proyecto
 * @package App\Models
 * @version October 30, 2018, 4:38 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property string entidad_adquisicion
 * @property integer id_entidad_adquisicion
 * @property string entidad_admin_contrato
 * @property string sector
 * @property string sub_sector
 * @property string programa_multi_d
 * @property integer programa_multi
 * @property string presupuesto_proyecto_multi_d
 * @property integer presupuesto_proyecto_multi
 * @property string contacto_entidad
 * @property string nombre_proyecto
 * @property string detalle_responsable
 * @property date fh_ultima_actualizacion
 * @property string localizacion
 * @property string localizacion_muni_d
 * @property integer localizacion_muni
 * @property string localizacion_depto_d
 * @property integer localizacion_depto
 * @property string coordenadas
 * @property string coordenadas_cln
 * @property integer snip
 * @property string snip_detalle
 * @property string proposito
 * @property string resumen
 * @property string beneficiario_d
 * @property integer beneficiario_total
 * @property date f_aprobacion
 */
class cost_proyecto extends Model {

    //  use SoftDeletes;

    public $table = 'cost_proyecto';

    protected $primaryKey = 'id_proyecto';

    public $timestamps = false;

    //   const CREATED_AT = 'created_at';criterios_default
    //   const UPDATED_AT = 'updated_at';

    //  protected $dates = ['deleted_at'];

    public $fillable = [
        'entidad_adquisicion',
        'id_entidad_adquisicion',
        'entidad_admin_contrato',
        'sector',
        'sub_sector',
        'programa_multi_d',
        'programa_multi',
        'presupuesto_proyecto_multi_d',
        'presupuesto_proyecto_multi',
        'contacto_entidad',
        'nombre_proyecto',
        'detalle_responsable',
        'fh_ultima_actualizacion',
        'localizacion',
        'localizacion_muni_d',
        'localizacion_muni',
        'localizacion_depto_d',
        'localizacion_depto',
        'coordenadas',
        'coordenadas_cln',
        'snip',
        'snip_detalle',
        'nog',
        'proposito',
        'resumen',
        'beneficiario_d',
        'beneficiario_total',
        'f_aprobacion',
        'f_r_proactiva',
        'f_r_reactiva',
        'no_cost',
        'estado_oc',
        'estado_oc_d',
        'sector_oc',
        'subsector_oc',
        'tipo_oc',
        'f_finalizacion',
        'vida_activo',
        'localidad',
        'codigo_postal',
        'pais',
        'estado',
        'seguimiento'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'entidad_adquisicion'          => 'string',
        //'id_entidad_adquisicion'       => 'integer',
        'entidad_admin_contrato'       => 'string',
        'sector'                       => 'string',
        'sub_sector'                   => 'string',
        'programa_multi_d'             => 'string',
        'programa_multi'               => 'integer',
        'presupuesto_proyecto_multi_d' => 'string',
        'presupuesto_proyecto_multi'   => 'integer',
        'contacto_entidad'             => 'string',
        'nombre_proyecto'              => 'string',
        'detalle_responsable'          => 'string',
        'fh_ultima_actualizacion'      => 'date',
        'localizacion'                 => 'string',
        'localizacion_muni_d'          => 'string',
        'localizacion_muni'            => 'integer',
        'localizacion_depto_d'         => 'string',
        'localizacion_depto'           => 'integer',
        'coordenadas'                  => 'string',
        'coordenadas_cln'              => 'string',
        'snip'                         => 'integer',
        'snip_detalle'                 => 'string',
        'proposito'                    => 'string',
        'resumen'                      => 'string',
        'beneficiario_d'               => 'string',
        'beneficiario_total'           => 'integer',
        'f_aprobacion'                 => 'date',
        'f_r_proactiva'                => 'date',
        'f_r_reactiva'                 => 'date',
        'f_ingreso'                    => 'date',
        'estado_oc'                    => 'integer',
        'estado_oc_d'                  => 'string',
        'sector_oc'                    => 'string',
        'subsector_oc'                 => 'string',
        'tipo_oc'                      => 'string',
        'f_finalizacion'               => 'date',
        'vida_activo'                  => 'integer',
        'codigo_postal'                => 'string',
        'pais'                         => 'string',
        'estado'                       => 'integer',
        'nog'                       => 'integer',
        'seguimiento'                       => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function setEstadoOcAttributre($estado_oc)
    {
        if($estado_oc > 0){
            $this->attributes[ 'estado_oc' ] = $estado_oc ;
            $cat_item = cat_item::where('id_item', $estado_oc)->first()->otro;
            $this->attributes[ 'estado_oc_d' ] = $cat_item ;

        }else{

            $this->attributes[ 'estado_oc' ] =  Null;
            $this->attributes[ 'estado_oc_d' ] = Null;
        }
    }


    public function setSectorOcAttributre($sector_oc)
    {
        if($sector_oc > 0){
            $this->attributes[ 'sector_oc' ] = $sector_oc ;
            $cat_item = cat_item::where('id_item', $sector_oc)->first()->otro;
            $this->attributes[ 'sector_oc_d' ] = $cat_item ;

        }else{

            $this->attributes[ 'sector_oc' ] =  Null;
            $this->attributes[ 'sector_oc_d' ] = Null;
        }
    }

    public function setSubsectorOcAttributre($subsector_oc)
    {
        $this->subsector_oc = sterlen($subsector_oc) ? $subsector_oc : null;
    }

    public function setEstadoAttributre($estado)
    {
        $this->estado = $estado > 0 ? $estado : 0;
    }

    public function setSeguimientoAttributre($seguimiento)
    {
        $this->seguimiento = $seguimiento > 0 ? $seguimiento : 0;
    }


    public function setTipoOcAttributre($tipo_oc)
    {
        if($tipo_oc > 0){
            $this->attributes[ 'tipo_oc' ] = $tipo_oc ;
          //  $cat_item = cat_item::where('id_item', $tipo_oc)->first()->otro;
          //  $this->attributes[ 'tipo_oc_d' ] = $cat_item ;

        }else{
            $this->attributes[ 'tipo_oc' ] =  Null;
          //  $this->attributes[ 'tipo_oc_d' ] = Null;
        }
        //$this->tipo_oc = sterlen($tipo_oc) ? $tipo_oc : null;
    }

    public function setFFinalizacionAttributre($f_finalizacion)
    {
        $this->f_finalizacion = sterlen($f_finalizacion) ? $f_finalizacion : null;
    }

    public function setVidaActivoAttributre($vida_activo)
    {
        $this->vida_activo = sterlen($vida_activo) ? $vida_activo : null;
    }

    public function setCodigoPostalAttributre($codigo_postal)
    {
        $this->codigo_postal = sterlen($codigo_postal) ? $codigo_postal : null;
    }

    public function setPaisAttributre($pais)
    {
        $this->pais = sterlen($pais) ? $pais : null;
    }

    public function setEntidadAdquisicionAttributre($entidad_adquisicion)
    {
        $this->entidad_adquisicion = sterlen($entidad_adquisicion) ? $entidad_adquisicion : null;
    }


    public function setEntidadAdminContratoAttributre($entidad_admin_contrato)
    {
        $this->entidad_admin_contrato = sterlen($entidad_admin_contrato) ? $entidad_admin_contrato : null;
    }

    public function setSectorAttributre($sector)
    {
        $this->sector = sterlen($sector) ? $sector : null;
    }

    public function setSubSectorAttributre($sub_sector)
    {
        $this->sub_sector = sterlen($sub_sector) ? $sub_sector : null;
    }

    public function setProgramaMultiDAttributre($programa_multi_d)
    {
        $this->programa_multi_d = sterlen($programa_multi_d) ? $programa_multi_d : 0;
    }

    public function setProgramaMultiAttributre($programa_multi)
    {
        $this->programa_multi = sterlen($programa_multi) ? $programa_multi : 0;
    }

    public function setPresupuestoProyectoMultiDAttributre($presupuesto_proyecto_multi_d)
    {
        $this->presupuesto_proyecto_multi_d = sterlen($presupuesto_proyecto_multi_d) ? $presupuesto_proyecto_multi_d : 0;
    }

    public function setPresupuestoProyectoMultiAttributre($presupuesto_proyecto_multi)
    {
        $this->presupuesto_proyecto_multi = sterlen($presupuesto_proyecto_multi) ? $presupuesto_proyecto_multi : 0;
    }

    public function setContactoEntidadAttributre($contacto_entidad)
    {
        $this->contacto_entidad = sterlen($contacto_entidad) ? $contacto_entidad : null;
    }

    public function setNombreProyectoAttributre($nombre_proyecto)
    {
        $this->nombre_proyecto = sterlen($nombre_proyecto) ? $nombre_proyecto : null;
    }

    public function setDetalleResponsableAttributre($detalle_responsable)
    {
        $this->detalle_responsable = sterlen($detalle_responsable) ? $detalle_responsable : null;
    }

    public function setFhUltimaActualizacionAttributre($fh_ultima_actualizacion)
    {
        $this->fh_ultima_actualizacion = sterlen($fh_ultima_actualizacion) ? $fh_ultima_actualizacion : 0;
    }

    public function setLocalizacionAttributre($localizacion)
    {
        $this->localizacion = sterlen($localizacion) ? $localizacion : 0;
    }

    public function setLocalizacionMuniDAttributre($localizacion_muni_d)
    {
        $this->localizacion_muni_d = sterlen($localizacion_muni_d) ? $localizacion_muni_d : 0;
    }

    public function setLocalizacionMuniAttributre($localizacion_muni)
    {
        $this->localizacion_muni = sterlen($localizacion_muni) ? $localizacion_muni : 0;
    }

    public function setLocalizacionDeptoDAttributre($localizacion_depto_d)
    {
        $this->localizacion_depto_d = sterlen($localizacion_depto_d) ? $localizacion_depto_d : 0;
    }

    public function setLocalizacionDeptoAttributre($localizacion_depto)
    {
        $this->localizacion_depto = sterlen($localizacion_depto) ? $localizacion_depto : 0;
    }

    public function setCoordenadasAttributre($coordenadas)
    {
        $this->coordenadas = sterlen($coordenadas) ? $coordenadas : 0;
    }

    public function setCoordenadasClnAttributre($coordenadas_cln)
    {
        $this->coordenadas_cln = sterlen($coordenadas_cln) ? $coordenadas_cln : 0;
    }

    public function setSnipAttributre($snip)
    {
        $this->snip = sterlen($snip) ? $snip : 0;
    }

    public function setNogFAttributre($nog_f)
    {
        $this->attributes[ 'nog_f' ] = sterlen($nog_f) ? $nog_f : null;
    }

    public function setAvanceFinancieroAttributre($avance_financiero)
    {
        $this->attributes[ 'avance_financiero' ] = sterlen($avance_financiero) ? $avance_financiero : 0;
    }

    public function setAvanceFisicoAttributre($avance_fisico)
    {
        $this->attributes[ 'avance_fisico' ] = sterlen($avance_fisico) ? $avance_fisico : 0;
    }

    public function setSnipDetalleAttributre($snip_detalle)
    {
        $this->snip_detalle = sterlen($snip_detalle) ? $snip_detalle : null;
    }

    public function setPropositoAttributre($proposito)
    {
        $this->proposito = sterlen($proposito) ? $proposito : null;
    }

    public function setResumenAttributre($resumen)
    {
        $this->resumen = sterlen($resumen) ? $resumen : null;
    }

    public function setBeneficiarioDAttributre($beneficiario_d)
    {
        $this->beneficiario_d = sterlen($beneficiario_d) ? $beneficiario_d : 0;
    }

    public function setBeneficiarioTotalAttributre($beneficiario_total)
    {
        $this->beneficiario_total = sterlen($beneficiario_total) ? $beneficiario_total : 0;
    }

    public function setAprobacionAttributre($f_aprobacion)
    {
        $this->f_aprobacion = sterlen($f_aprobacion) ? $f_aprobacion : null;
    }

    public function setFRProactivaAttributre($f_r_proactiva)
    {
        $this->f_r_proactiva = sterlen($f_r_proactiva) ? $f_r_proactiva : null;
    }

    public function setFRReactivaAttributre($f_r_reactiva)
    {
        $this->f_r_reactiva = sterlen($f_r_reactiva) ? $f_r_reactiva : null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNoCostAttribute()
    {
        $anio = $this->f_ingreso->format('Y');
        $no = $this->id_proyecto . '-' . $anio;
        return $no;
    }

    public function getIdAttribute()
    {
        return $this->id_proyecto;
    }
    public function getFmtEstadoOcAttribute()
    {
        if ($this->rel_estado_oc) {
            return $this->rel_estado_oc->descripcion;
        } else {
            return "";
        }
    }
    public function getFmtSectorOcAttribute()
    {
        if ($this->rel_sector_oc) {
            return $this->rel_sector_oc->descripcion;
        } else {
            return "";
        }
    }
    public function getFmtTipoOcAttribute()
    {
        if ($this->rel_tipo_oc) {
            return $this->rel_tipo_oc->descripcion;
        } else {
            return "";
        }
    }

    public function rel_estado_oc()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'estado_oc', 'id_item');
    }

    public function rel_sector_oc()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'sector_oc', 'id_item');
    }

    public function rel_tipo_oc()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'tipo_oc', 'id_item');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function rel_dis_plan()
    {
        $test = $this->hasOne(\App\Models\cost_proyecto_dis_plan::class, 'id_proyecto', 'id_proyecto');
        return $test;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function rel_planifica()
    {
        return $this->hasOne(\App\Models\cost_proyecto_planifica::class, 'id_proyecto', 'id_proyecto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function rel_supervision()
    {
        return $this->hasOne(\App\Models\cost_proyecto_supervision::class, 'id_proyecto', 'id_proyecto');
    }

    public function rel_historial_descarga()
    {
        return $this->belongsTo(\App\Models\descarga_responsable::class, 'id_proyecto', 'id_proyecto');
    }

    public function rel_oferente()
    {
        return $this->hasMany(\App\Models\cost_oferente::class, 'id_proyecto', 'id_proyecto');
    }

    public function rel_acta()
    {
        return $this->hasMany(\App\Models\cost_acta::class, 'id_proyecto', 'id_proyecto');
    }

    public function rel_acuerdo_financiamiento()
    {
        return $this->hasMany(\App\Models\cost_acuerdo_financiamiento::class, 'id_proyecto', 'id_proyecto');
    }

    public function rel_ampliacion()
    {
        return $this->hasMany(\App\Models\cost_ampliacion::class, 'id_proyecto', 'id_proyecto');
    }

    public function rel_fianza()
    {
        return $this->hasMany(\App\Models\cost_fianza::class, 'id_proyecto', 'id_proyecto');
    }

    public function rel_info_contrato()
    {
        return $this->hasMany(\App\Models\cost_info_contrato::class, 'id_proyecto', 'id_proyecto');
    }

    public function rel_liquidacion()
    {
        return $this->hasMany(\App\Models\cost_liquidacion::class, 'id_proyecto', 'id_proyecto');
    }
    public function rel_liquidacion_monto()
    {
        return $this->hasMany(\App\Models\cost_liquidacion_monto::class, 'id_proyecto', 'id_proyecto');
    }
    public function rel_liquidacion_alcance()
    {
        return $this->hasMany(\App\Models\cost_liquidacion_alcance::class, 'id_proyecto', 'id_proyecto');
    }
    public function rel_liquidacion_tiempo()
    {
        return $this->hasMany(\App\Models\cost_liquidacion_tiempo::class, 'id_proyecto', 'id_proyecto');
    }


    public function rel_pago_efectuado()
    {
        return $this->hasMany(\App\Models\cost_pago_efectuado::class, 'id_proyecto', 'id_proyecto');
    }

    public function rel_id_depto_domicilio()
    {
        return $this->belongsTo(\App\Models\geo::class, 'localizacion_depto', 'id_geo');
    }

    public function rel_id_muni_domicilio()
    {
        return $this->belongsTo(\App\Models\geo::class, 'localizacion_muni', 'id_geo');
    }

    public function getFmtIdMuniDomicilioAttribute()
    {
        if ($this->rel_id_muni_domicilio) {
            return $this->rel_id_muni_domicilio->nombre;
        } else {
            return "-";
        }
    }

    public function getFmtIdDeptoDomicilioAttribute()
    {
        if ($this->rel_id_depto_domicilio) {
            return $this->rel_id_depto_domicilio->nombre;
        } else {
            return "-";
        }
    }

    /**BUSQUEDAS Y CRITERIOS DEFAULT*/
    public static function criterios_default($request = 0)
    {
        $filtros = new \stdClass();
        $filtros->fecha_del = date("2010" . "-m" . "-05");
        $filtros->fecha_al = date("Y-m-d");
        $filtros->brapida = "";
        //Para presentacion
        $filtros->fmt_fecha_del = \Carbon\Carbon::createFromFormat("Y-m-d", $filtros->fecha_del)->format("d-m-Y");
        $filtros->fmt_fecha_al = \Carbon\Carbon::createFromFormat("Y-m-d", $filtros->fecha_al)->format("d-m-Y");

        $filtros->nombre_proyecto = '';
        $filtros->snip = 0;
        $filtros->nog = 0;
        $filtros->localizacion = '';
        $filtros->sector = '';
        $filtros->sub_sector = '';
        $filtros->entidad_adquisicion = '';
        $filtros->fh_ultima_actualizacion = 0;
        $filtros->f_aprobacion = 0;

        // Procesar request (opcional)
        if (is_object($request)) {
            $filtros->url = $request->url();
            if (isset($request->q)) {
                $filtros->brapida = $request->q;
            }
            if (isset($request->fh_inicial_submit)) {
                $filtros->fecha_del = $request->fh_inicial_submit;
            }
            if (isset($request->fh_fin_submit)) {
                $filtros->fecha_al = $request->fh_fin_submit;
            }
            if (strlen($request->nombre_proyecto) > 0) {
                $filtros->nombre_proyecto = $request->nombre_proyecto;
            }
            if (strlen($request->localizacion) > 0) {
                $filtros->localizacion = $request->localizacion;
            }
            if (strlen($request->sector) > 0) {
                $filtros->sector = $request->sector;
            }
            if (strlen($request->sub_sector) > 0) {
                $filtros->sub_sector = $request->sub_sector;
            }
            if ($request->snip > 0) {
                $filtros->snip = $request->snip;
            }

            if ($request->entidad_adquisicion != '') {
                $filtros->entidad_adquisicion = $request->entidad_adquisicion;
            }
            if ($request->nog > 0) {
                $filtros->nog = $request->nog;
            }
            if ($request->f_aprobacion > 0) {
                $filtros->f_aprobacion = $request->f_aprobacion;
            }

            if ($request->fh_ultima_actualizacion > 0) {
                $filtros->fh_ultima_actualizacion = $request->fh_ultima_actualizacion;
            }
        }
        return $filtros;
    }

    public static function scopeFiltrarOrdenar($query, $criterios)
    {
        if (!is_object($criterios)) {  //Para evitar que truene por mala programacion
            $criterios = self::criterios_default();
        }
        if (strlen($criterios->brapida) > 0) {
            $query->brapida($criterios->brapida);
        } else {
            $query->bavanzada($criterios);
        }

        if(!Auth::check()){
            $query ->where('estado', 1);
        }
        return $query;
    }

    public static function scopeBrapida($query, $texto)
    {
        $texto = str_replace(" ", "%", $texto);
        if (strlen($texto) > 0) {
            $query->where('snip', 'like', "$texto%")
            //    ->join('cost_proyecto_dis_plan as p', 'cost_proyecto.id_proyecto', '=', 'p.id_proyecto')
                ->orwhere('nombre_proyecto', 'like', "%$texto%")
                ->orwhere('sector', 'like', "%$texto%")
                ->orwhere('sub_sector', 'like', "%$texto%")
                ->orwhere('localizacion', 'like', "%$texto%")
                ->orwhere('entidad_adquisicion', 'like', "%$texto%")
                ->orwhere('cost_proyecto.nog', 'like', "%$texto%")
                ->orderBy('cost_proyecto.id_proyecto','desc');
        }
    }

    public static function scopeBavanzada($query, $criterios)
    {
        $nombre_proyecto = str_replace(" ", "%", $criterios->nombre_proyecto);
        $snip = str_replace(" ", "%", $criterios->snip);
        $nog = str_replace(" ", "%", $criterios->nog);
        $localizacion = str_replace(" ", "%", $criterios->localizacion);
        $sector = str_replace(" ", "%", $criterios->sector);
        $sub_sector = str_replace(" ", "%", $criterios->sub_sector);
        $entidad = str_replace(" ", "%", $criterios->entidad_adquisicion);
        if (strlen($criterios->fecha_del) > 0) {
            $query
            ->Fecha($criterios)
                ->NombreProyecto($nombre_proyecto)
                ->Sector($sector)
                ->SubSector($sub_sector)
                ->Localizacion($localizacion)
                ->Snip($snip)
                ->EntidadAdquisicion($entidad)
                ->Nog($nog)
                ->orderBy('cost_proyecto.id_proyecto','desc');
        }
    }

    public static function scopeFecha($query, $criterios)
    {
        if ($criterios->fh_ultima_actualizacion > 0) {
            $query->whereBetween('fh_ultima_actualizacion', [ $criterios->fecha_del, $criterios->fecha_al ]);
        } elseif ($criterios->f_aprobacion > 0) {
            $query->whereBetween('f_aprobacion', [ $criterios->fecha_del, $criterios->fecha_al ]);
        } else {
            $query->whereBetween('f_ingreso', [ $criterios->fecha_del, $criterios->fecha_al ]);
        }
    }

    public static function scopeNombreProyecto($query, $nombre_proyecto)
    {
        if (strlen($nombre_proyecto) > 0) {
            $query->where('nombre_proyecto', 'like', "%$nombre_proyecto%");
        }
    }

    public static function scopeEntidadAdquisicion($query, $entidad)
    {
        if (strlen($entidad) > 0) {
            $query->where('entidad_adquisicion', 'like', "%$entidad%");
        }
    }

    public static function scopeSector($query, $sector)
    {
        if (strlen($sector) > 0) {
            $query->where('sector', 'like', "%$sector%");
        }
    }

    public static function scopeSubSector($query, $sub_sector)
    {
        if (strlen($sub_sector) > 0) {
            $query->where('sub_sector', 'like', "%$sub_sector%");
        }
    }

    public static function scopeSnip($query, $snip)
    {
        if ($snip > 0) {
            $query->where('snip', 'like', "%$snip%");
        }
    }

    public static function scopeNog($query, $nog)
    {
        if ($nog > 0) {
            $query->where('nog', 'like', "%$nog%");
        }
    }

    public static function scopeLocalizacion($query, $localizacion)
    {
        if (strlen($localizacion) > 0) {
            $query->where('localizacion', 'like', "%$localizacion%");
        }
    }

    public function rel_estado_actual_proyecto()
    {
        return $this->belongsTo(\App\Models\cat_item::class, 'estado_actual_proyecto', 'id_item');
    }

    public function getFmtEstadoActualProyectoAttribute()
    {
        if ($this->rel_estado_actual_proyecto) {
            return $this->rel_estado_actual_proyecto->descripcion;
        } else {

            if (strlen($this->rel_estado_actual_proyecto_d) > 0)
                return $this->rel_estado_actual_proyecto_d;
            else
                return "-";
        }
    }

    public static function datos_entrada_index()
    {
        $query = self::join('cost_proyecto_dis_plan as p', 'cost_proyecto.id_proyecto', '=', 'p.id_proyecto')
            ->paginate(10);
        return $query;
    }

    #revisa si tiene descargas
    public static function historial_descargas($id_proyecto)
    {
        $query = descarga_responsable::where('id_proyecto', $id_proyecto)->get();
        if (count($query) > 0) {
            $resp = true;
        } else {
            $resp = false;
        }
        return $resp;
    }

    public static function format_oc4ids_sector($sector){
        if ($sector == 8190){//transport.air
            $descripcion_otro = cat_item::item_otro($sector);
            $format_json[] = "transport";
            $format_json[]= $descripcion_otro;
            $formato_item_oc4ids = $format_json;

        }else{//otro item
            $descripcion_otro = cat_item::item_otro($sector);
            $formato_item_oc4ids[] = $descripcion_otro;
        }
        return $formato_item_oc4ids;
    }

    public static function vista_proyecto_ocds($id_proyecto)
    {
        $where = " WHERE  id_proyecto = '$id_proyecto'";
        $results = DB::select(DB::raw("SELECT *  FROM proyecto_view $where"));

        foreach($results as $result){
            $result->updated = date('c', strtotime($result->updated) );
            $result->id = "oc4ids-kw6tu4-".$result->id  ;
            $result->type = ($result->type==null || !isset($result->type) || $result->type==0 )?  null : cat_item::item_otro($result->type);
            $result->sector = ($result->sector==null || !isset($result->sector) || $result->sector==0)?  null : cost_proyecto::format_oc4ids_sector($result->sector);
            $result->purpose =  ($result->purpose==null || !isset($result->purpose) || $result->purpose==0)?  null : str_replace('"', "", $result->purpose );
        }
        return $results;
    }

    public static function vista_proyecto_periodo_ocds($id_proyecto)
    {
        $where = " WHERE  id = '$id_proyecto' LIMIT 1";
        $results = DB::select(DB::raw("SELECT startDate, endDate, durationInDays  FROM proyecto_view_periodo $where"));
        foreach($results as $result){
            $result->startDate = date('c', strtotime($result->startDate) );
            $result->endDate = date('c', strtotime($result->endDate) );
        }
        return $results;
    }

    public static function vista_proyecto_direccion_ocds($id_proyecto)
    {
        $where = " WHERE  id = '$id_proyecto' LIMIT 1";
        $results = DB::select(DB::raw("SELECT streetAddress, locality, region, postalCode,
                  countryName  FROM proyecto_view_direccion $where"));
        return $results;

    }

    public static function vista_proyecto_geometria_ocds($id_proyecto)
    {
        $where = " WHERE  id = '$id_proyecto' LIMIT 1";
        $results = DB::select(DB::raw("SELECT type, coordinates FROM proyecto_view_direccion $where"));
        $valores =[];

        /**transformando coordinates*/
        foreach ($results as $k => $v){
            if(isset($v->coordinates)){
                $coordenadas = explode(',',$v->coordinates);
                $v->coordinates = [floatval($coordenadas[0]),floatval($coordenadas[1])];

            }
            $valores[$k] = $v;
        }

        return $valores;
    }

    public static function vista_proyecto_localidad_ocds($id_proyecto)
    {
        $where = " WHERE  id = '$id_proyecto' LIMIT 1";
        $results = DB::select(DB::raw("SELECT id, description FROM proyecto_view_direccion $where"));

        return $results;
    }

    public static function vista_presupuesto_ocds($id_proyecto, $tipo)
    {
        $where = " WHERE  id = '$id_proyecto' and amount > 0 LIMIT 1";
        if ($tipo == 1) {

            $results = DB::select(DB::raw("SELECT amount,currency,approvalDate FROM presupuesto_view $where"));

            foreach($results as $result){
                $result->amount = (int)$result->amount;
                if($result->currency != null){
                    $int_currency = intval($result->currency);
                    $result->currency = cost_proyecto::busca_descripcion_catalogo($int_currency);
                }
            }
        } else {
            $results = DB::select(DB::raw("SELECT approvalDate FROM presupuesto_view $where"));
        }

        return $results;
    }

    public static function vista_oferentes($id_proyecto)
    {
        $where = " WHERE  id_proyecto = '$id_proyecto'";
        $results = DB::select(DB::raw("SELECT DISTINCT id_participante FROM cost_oferente  $where"));
        return $results;
    }

    public static function vista_documentos($id_proyecto)
    {
        $where = " WHERE  id_proyecto = '$id_proyecto'";
        $results = DB::select(DB::raw("SELECT id,
                                        documentType,
                                        title,
                                        description,
                                        url,
                                        datePublished,
                                        dateModified,
                                        format,
                                        language,
                                        pageStart,
                                        pageEnd,
                                        accessDetails,
                                        author FROM documento_view  $where"));

        foreach($results as $key => $result){
            if (count($result->accessDetails)== 0){
                unset($result->accessDetails);
            }

            if ($result->language == 'Español'){
                $result->language ='es';
            }else{
                $result->language = 'en';
            }

            if ($result->format == 8166){
                $result->format ='application/pdf';
            }elseif($result->format == 8168){
                $result->format ='text/html';
            }elseif($result->format == 8167){
                $result->format ='text/css';
            }else{
                $result->format = $result->format;
            }

            $results[$key]->datePublished = date('c', strtotime($result->datePublished) );
            $results[$key]->dateModified = date('c', strtotime($result->dateModified) );

            if(isset($result->documentType) && $result->documentType>0 ) {
                $cat_item = cat_item::where('id_item', $result->documentType)->first()->descripcion;
                $results[ $key ]->documentType = $cat_item;
            }

            if(isset($result->format) && $result->format>0){
                $format = cat_item::where('id_item', $result->format)->first()->descripcion;
                $results[$key]->format = $format;
            }

            if(isset($result->language) && $result->language>0 ){

                $len = cat_item::where('id_item', $result->language)->first()->descripcion;
                $results[$key]->language = $len;
            }


        }

        return (object)$results;
    }

    public static function vista_liquidacion($id_proyecto)
    {
        $where = " WHERE  id_proyecto = '$id_proyecto'";
        $results = DB::select(DB::raw("SELECT amount, 
                                      currency, 
                                      finalValueDetails, 
                                      finalScope, 
                                       finalScopeDetails 
                                       FROM liquidacion_view  $where"));

        foreach($results as $result){
            $result->amount = (int)$result->amount;
            if($result->amount != null) {
                $int_currency = intval($result->currency);
                $result->currency = cost_proyecto::busca_descripcion_catalogo($int_currency);
            }

        }

        return $results;
    }

    public static function vista_participantes($id_participante)
    {
        $where = " WHERE  id_participante = '$id_participante'";
        $results = DB::select(DB::raw("SELECT name, id FROM participante_view  $where"));
        return $results;
    }

    public static function vista_participantes_direccion($id_participante)
    {
        $where = " WHERE  id_participante = '$id_participante'";
        $results = DB::select(DB::raw("SELECT streetAddress, locality, region, postalCode, countryName FROM participante_view  $where"));
        return $results;
    }

    public static function vista_participantes_schema($id_participante)
    {
        $where = " WHERE  id_participante = '$id_participante'";
        $results = DB::select(DB::raw("SELECT scheme,id,legalName,uri FROM participante_schema_contact_view  $where"));
        return $results;
    }

    public static function roles_participante($id_participante)
    {
        $where = " WHERE  id_participante = '$id_participante' GROUP BY rol";
        $results = DB::select(DB::raw("SELECT 
      CASE 
            WHEN rol = 1 THEN 'tenderer'
            WHEN rol = 2 THEN 'procuringEntity'
            WHEN rol = 3 THEN 'supplier'
            WHEN rol = 4 THEN 'publicAuthority'
            WHEN rol = 5 THEN 'buyer'
    
        END as   rol  FROM cost_oferente  $where"));
        return $results;
    }

    public static function vista_participantes_contact($id_participante)
    {
        $where = " WHERE  id_participante = '$id_participante'";
        $results = DB::select(DB::raw("SELECT name, email, telephone, faxNumber, url FROM participante_schema_contact_view  $where"));
        return $results;
    }

    public static function vista_process_id($id_proyecto, $numero_proceso)
    {
        $where = " WHERE  id_proyecto = '$id_proyecto'";
        $results = DB::select(DB::raw("SELECT concat('ocds-xqjsxa-',$numero_proceso,'-',id_proyecto,'-', year(f_ingreso),'Gt') as id FROM cost_proyecto p 
                                                                                        $where"));
        return $results;
    }

    public static function vista_process_summary($id_proyecto, $numero_proceso)
    {
        $where = " WHERE  id_proyecto = $id_proyecto and id_fase = $numero_proceso";
        if ($numero_proceso == 2) {
            $nature = 'design';
            $title = 'Preparación del Proyecto';
        } elseif ($numero_proceso == 3) {
            $nature = 'construction';
            $title = 'Información del Contrato';
        } else {
            $nature = 'supervision';
            $title = 'Información del Contrato de la supervisión';
        }

        $results = DB::select(DB::raw("SELECT '$nature' as nature, '$title' as title, alcance as description, estado_contrato_oc_d as status  FROM cost_info_contrato  $where"));

        foreach($results as $result){
            $arreglo[0]=$result->nature;

            $result->nature = $arreglo;
        }
        return $results;
    }

    public static function vista_process_tender($id_proyecto, $numero_proceso)
    {
        $where = " WHERE  id_proyecto = $id_proyecto and id_fase = $numero_proceso";
        $results = DB::select(DB::raw("SELECT metodo_procuracion as procurementMethod, detalle_procuracion as procurementMethodDetails  FROM cost_info_contrato  $where"));
        return $results;
    }

    public static function vista_process_tender_amount($id_proyecto, $numero_proceso)
    {
        $where = " WHERE  id_proyecto = $id_proyecto and id_fase = $numero_proceso";
        $results = DB::select(DB::raw("SELECT costo_estimado as amount, costo_estimado_moneda as currency  FROM cost_info_contrato  $where"));

        foreach($results as $result){

            $result->amount = (int)$result->amount;
            if($result->currency != null){
                $int_currency = intval($result->currency);
                $result->currency = cost_proyecto::busca_descripcion_catalogo($int_currency);
            }
        }
        return $results;
    }

    public static function total_oferentes($id_proyecto, $numero_proceso)
    {
        $where = " WHERE  id_proyecto = $id_proyecto and id_fase = $numero_proceso and rol = 1";
        $results = DB::select(DB::raw("SELECT count(1) as numberOfTenderers FROM cost_oferente  $where"));

        /**enviar solamente la cantidad de oferentes*/
        $total_oferentes = intval($results[0]->numberOfTenderers);
        return $total_oferentes ;
    }

    public static function vista_oferente_fase($id_proyecto, $id_fase, $id_rol)
    {
        $where = " WHERE  c.id_proyecto = '$id_proyecto' and c.id_fase = $id_fase and c.rol = $id_rol";
        $results = DB::select(DB::raw("SELECT p.id_participante as id, p.nombre_comun as name FROM cost_oferente as c LEFT JOIN participante as p ON c.id_participante = p.id_participante    $where"));
        return $results;
    }

    public static function busca_descripcion_catalogo ($id_item){
        if (count($id_item)>0){
            $q = cat_item::find($id_item);
            return $q->descripcion;
        }else{
            return 'GTQ';
        }


    }
    public static function vista_summary_contract($id_proyecto, $numero_proceso, $tipo)
    {
        $where = " WHERE  id_proyecto = $id_proyecto and id_fase = $numero_proceso";
        if ($tipo == 0) {
            $results = DB::select(DB::raw("SELECT precio_contrato as amount, moneda, moneda_final as currency  FROM cost_info_contrato  $where"));

            foreach($results as $result){
                $result->amount = (int)$result->amount;

                if ( $result->amount>0 and $result->currency != null){

                    if(strlen($result->currency)==0 && strlen($result->moneda)>1 && $result->moneda == 'QGT'){
                        $result->currency = 8199;
                    }
                    $result->currency = cost_proyecto::busca_descripcion_catalogo($result->currency);
                }
            }
        } elseif ($tipo == 1) {
            $results = DB::select(DB::raw("SELECT fecha_inicio_ejecucion as startDate, fecha_fin_ejecucion as endDate, (to_days(fecha_fin_ejecucion) - to_days(fecha_inicio_ejecucion)) as durationInDays  FROM cost_info_contrato  $where"));

            foreach($results as $result){
                if(!is_null($result->startDate)){
                    $result->startDate = date('c', strtotime($result->startDate) );
                }
                if(!is_null($result->endDate)){
                    $result->endDate = date('c', strtotime($result->endDate) );
                }
                $result->durationInDays = isset($result->endDate) ? intval($result->durationInDays): 0;
            }

        } elseif ($tipo == 2) {
            $results = DB::select(DB::raw("SELECT precio_contrato_final as amount, moneda_final as currency  FROM cost_info_contrato  $where"));
            foreach($results as $result){
                $result->amount = (int)$result->amount;
                if($result->currency != null){
                    $int_currency = intval($result->currency);
                    $result->currency = cost_proyecto::busca_descripcion_catalogo($int_currency);
                }

            }

        }
        return $results;
    }

    public static function vista_documentos_fase($id_proyecto, $fase)
    {
        $where = "WHERE  id_proyecto = '$id_proyecto' and id_fase = $fase";
        $results = DB::select(DB::raw("SELECT id,
                                        documentType,
                                        title,
                                        description,
                                        url,
                                        datePublished,
                                        dateModified,
                                        format,
                                        language,
                                        pageStart,
                                        pageEnd,
                                        accessDetails,
                                        author FROM documento_view  $where"));

        foreach($results as $result){

            if (count($result->accessDetails)== 0){
                unset($result->accessDetails);
            }

            if ($result->language == 'Español'){
                $result->language ='es';
            }else{
                $result->language = 'en';
            }

            if ($result->format == 8166){
                $result->format ='application/pdf';
            }elseif($result->format == 8168){
                $result->format ='text/html';
            }elseif($result->format == 8167){
                $result->format ='text/css';
            }else{
                $result->format = $result->format;
            }


            if(!is_null($result->datePublished)){
                $result->datePublished = date('c', strtotime($result->datePublished) );
            }
           if(!is_null($result->dateModified)){
                $result->dateModified = date('c', strtotime($result->dateModified) );
            }
            if(isset($result->documentType) && $result->documentType>0 ) {
                $cat_item = cat_item::where('id_item', $result->documentType)->first()->descripcion;
                $result->documentType = $cat_item;
            }

            if(isset($result->format) && $result->format>0){
                $format = cat_item::where('id_item', $result->format)->first()->descripcion;
                $result->format = $format;
            }

            if(isset($result->language) && $result->language>0 ){

                $len = cat_item::where('id_item', $result->language)->first()->descripcion;
                $result->language = $len;
            }
        }



        return $results;
    }

    public static function vista_modifica_alcance($id_proyecto, $fase)
    {
        $where = " WHERE  id_proyecto = '$id_proyecto' and id_fase = $fase";
        $results = DB::select(DB::raw("SELECT 
                                        concat('1',
                                        id_liquidacion_alcance) as id,
                                        razon_cambio as description,
                                        alcance_real as rationale,
                                        fecha_cambio as date,
                                        'scope' as type
                                        FROM cost_liquidacion_alcance  $where"));
        return $results;
    }

    public static function vista_modifica_tiempo($id_proyecto, $fase)
    {
        $where = " WHERE  id_proyecto = '$id_proyecto' and id_fase = $fase";
        $results = DB::select(DB::raw("SELECT 
                                        concat('3',
                                        ct.id_liquidacion_tiempo) as id,
                                        ct.razones_cambio_plazo as description,
                                        ct.razones_cambio_plazo as rationale,
                                        ct.fecha_cambio as date,
                                        'duration' as type,
                                        antigua_fecha_inicio as startDate,
                                        nueva_fecha_inicio as startDateN,
                                        cantidad_tiempo_modifica_inicio as durationInDays,
                                        antigua_fecha_finaliza as endDate,
                                        nueva_fecha_finaliza as endDateN,
                                        cantidad_tiempo_modificado as durationInDaysN
                                        FROM cost_liquidacion_tiempo as ct
                                          $where"));
        foreach($results as $result){

            if(!is_null($result->startDate)){
                $result->startDate = date('c', strtotime($result->startDate) );
            }
            if(!is_null($result->startDateN)){
                $result->startDateN = date('c', strtotime($result->startDateN) );
            }
            if(!is_null($result->endDate)){
                $result->endDate = date('c', strtotime($result->endDate) );
            }
            if(!is_null($result->endDateN)){
                $result->endDateN = date('c', strtotime($result->endDateN) );
            }
        }
        return $results;
    }

    public static function vista_modifica_monto($id_proyecto, $fase)
    {
        $where = " WHERE  id_proyecto = '$id_proyecto' and id_fase = $fase";
        $results = DB::select(DB::raw("SELECT                                         concat('3',

                                        ct.id_liquidacion_monto) as id,
                                        ct.razon_cambio as description,
                                        ct.razon_cambio as rationale,
                                        ct.fecha_cambio as date,
                                        'value' as type,
                                        monto_antiguo as amount, 
                                        moneda_antigua as currency,
                                        monto_actualizado as amountN,
                                        moneda as currencyN,
                                        suma_mod as suma_mod
                                        FROM cost_liquidacion_monto as ct
                                          $where"));
       //dd($results);
        foreach($results as $result){
            $result->amount = (int)$result->amount;
            $result->amountN = (int)$result->amountN;
        }
        return $results;
    }

    //Busquedas para

    public static function busca_datos_proyecto_descarga($id_proyecto, $tipo_descarga)
    {

        $where = $tipo_descarga == 1 ? " WHERE 1" : " WHERE  c.id_proyecto = '$id_proyecto'";
        $results = DB::select(DB::raw("SELECT 
                                                c.nombre_proyecto,    
                                                c.resumen as descripcion,
                                                c.sector,
                                                c.sub_sector,
                                                c.proposito,
                                                c.localizacion,
                                                c.entidad_adquisicion,
                                                c.snip,
                                                c.snip_detalle,
                                                c.f_aprobacion as fecha_aprobacion,
                                                c.nog,
                                                c.detalle_responsable as operador_portal,
                                                c.nog_f as fecha_nog,
                                                c.contacto_entidad, 
                                                dp.resolucion_marn,
                                                dp.f_resolucion_marn,
                                                dp.agrip,
                                                dp.agrip_medida_mitigacion,
                                                dp.agrip_elaborado_por,
                                                dp.agrip_f,
                                                dp.estudio_suelo,
                                                dp.estudio_suelo_f,
                                                dp.estudio_suelo_por,observaciones_topografia
                                                dp.estudio_topografia,
                                                dp.estudio_topografia_f,
                                                dp.estudio_topografia_por,
                                                dp.estudio_geologico,
                                                dp.estudio_geologico_f,
                                                dp.estudio_geologico_por,
                                                dp.espe_tecnica,
                                                dp.espe_tecnica_f,
                                                dp.espe_tecnica_responsable,
                                                dp.memoria_calculo,
                                                dp.memoria_calculo_f,
                                                dp.memoria_calculo_responsable,
                                                dp.plano_completo,
                                                dp.plano_completo_responsable,
                                                dp.plano_completo_f,
                                                dp.dispo_especial,
                                                dp.dispo_especial_responsable,
                                                dp.dispo_especial_f,
                                                dp.diseno_estructural,
                                                dp.diseno_estructural_por,
                                                dp.diseno_estructural_f,
                                                dp.espe_general,
                                                dp.espe_general_responsable,
                                                dp.espe_general_f,
                                                dp.fuente_financiamiento_dis as partida_presupuestaria_diseno,
                                                dp.dis_precio_contratado as monto_contratado_financiamiento,
                                                dp.fuente_f_sup as partida_presupuestaria_supervision,
                                                s.sup_monto_contratado as monto_contratado_supervision,
                                                dp.fuente_f_ejecucion as partida_presupuestaria_ejecucion,
                                                p.monto_contrato as monto_asignado,
                                                dp.fuente_f_ambiental as partida_presupuestaria_mitiga_ambiental,
                                                dp.monto_ambiental as monto_mitiga_ambiental,
                                                p.oferente,
                                                p.empresa_constructora,
                                                p.plazo_contrato,
                                                p.f_inicio_contrato as fecha_inicio_contrato,
                                                p.tipo_no_contrato,
                                                p.alcance_contrato,
                                                p.monto_contrato as precio_contrato,
                                                p.f_inicio_contrato as fecha_inicio_contrato,
                                                p.f_inicio_contrato as fecha_aprobacion_contrato,
                                                s.observaciones,
                                                s.conclusiones,
                                                s.recomendaciones
                                                FROM cost_proyecto as c
                                                LEFT JOIN cost_proyecto_dis_plan as dp on c.id_proyecto = dp.id_proyecto
                                                LEFT JOIN cost_proyecto_supervision as s on c.id_proyecto = s.id_proyecto
                                                LEFT JOIN cost_proyecto_planifica as p on c.id_proyecto = p.id_proyecto
                                                $where
                                               "));

        return $results;
    }

    public static function listado_roles_participantes($vacio='') {
        $listado=cat_item::
        orderby('descripcion','asc')
            ->where('id_cat',84)
            ->pluck('descripcion','id_item');
        if(strlen($vacio)>0) {
            $listado->prepend($vacio,0);
        }
        return $listado;
    }

    public static function listado_entidad_adquisicion($vacio='') {
        $listado=participante::
        orderby('nombre_comun','asc')
            ->pluck('nombre_comun','id_participante');
        if(strlen($vacio)>0) {
            $listado->prepend($vacio,0);
        }
        return $listado;
    }

    public function rel_id_entidad_adquisicion()
    {
        return $this->belongsTo(\App\Models\participante::class,'id_entidad_adquisicion','id_participante');
    }

    public function getFmtIdEntidadAdquisicionAttribute()
    {
        if ($this->rel_id_entidad_adquisicion) {
            return $this->rel_id_entidad_adquisicion->nombre_comun;
        } else {
            return "-";
        }
    }

   public static function  publicar_proyecto($id){
        $publicar = cost_proyecto::find($id);
        $publicar->estado = 1;
        $publicar->save();
        return $publicar;
   }

   public static function  no_publicar_proyecto($id){
       $publicar = cost_proyecto::find($id);
       $publicar->estado = 0;
       $publicar->save();
       return $publicar;
   }


   public static function  marcar_seguimiento($id){
        $publicar = cost_proyecto::find($id);
        $publicar->seguimiento = 1;
        $publicar->save();
        return $publicar;
   }

   public static function  no_marcar_seguimiento($id){
       $publicar = cost_proyecto::find($id);
       $publicar->seguimiento = 0;
       $publicar->save();
       return $publicar;
   }

    public static function limpia_string($string = ''){
        $replace = str_replace('.',' ',trim($string));
        $replace = str_replace('+',' ',trim($replace));
        $replace = str_replace('-',' ',trim($replace));
        $replace = str_replace('(',' ',trim($replace));
        $replace = str_replace(')',' ',trim($replace));
        $replace = str_replace(':',' ',trim($replace));
        $replace = str_replace(',',' ',trim($replace));
        $replace = str_replace('/',' ',trim($replace));
        return $replace;
    }



}
