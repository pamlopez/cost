<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_acta
 * @package App\Models
 * @version January 17, 2019, 5:16 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property integer id_proyecto
 * @property integer tipo
 * @property integer tipo_acta
 * @property string tipo_acta_d
 * @property string no_acta
 * @property date fecha_acta
 * @property string link
 */
class cost_acta extends Model {

    //  use SoftDeletes;

    public $table = 'cost_acta';

    protected $primaryKey = 'id_acta';

    public $timestamps = false;

    // const CREATED_AT = 'created_at';
    // const UPDATED_AT = 'updated_at';

    //   protected $dates = ['deleted_at'];

    public $fillable = [
        'id_proyecto',
        'id_fase',
        'tipo',
        'tipo_acta',
        'tipo_acta_d',
        'no_acta',
        'fecha_acta',
        'link',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'id_fase',
        'tipo'        => 'integer',
        'tipo_acta'   => 'integer',
        'tipo_acta_d' => 'string',
        'no_acta'     => 'string',
        'fecha_acta'  => 'date',
        'link'        => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function setTipoAttribute($tipo)
    {
        $this->attributes[ 'tipo' ] = strlen($tipo) > 0 ? $tipo : null;
    }

    public function setTipoActaAttribute($tipo_acta)
    {
        $this->attributes[ 'tipo_acta' ] = strlen($tipo_acta) > 0 ? $tipo_acta : null;
    }

    public function setTipoActaDAttribute($tipo_acta_d)
    {
        $this->attributes[ 'tipo_acta_d' ] = strlen($tipo_acta_d) > 0 ? $tipo_acta_d : null;
    }

    public function setNoActaAttribute($no_acta)
    {
        $this->attributes[ 'no_acta' ] = strlen($no_acta) > 0 ? $no_acta : null;
    }

    public function setFechaActaAttribute($fecha_acta)
    {
        $this->attributes[ 'fecha_acta' ] = strlen($fecha_acta) > 0 ? $fecha_acta : null;
    }

    public function setLinkAttribute($link)
    {
        $this->attributes[ 'link' ] = strlen($link) > 0 ? $link : null;
    }

    public function getIdAttribute()
    {
        return $this->id_acta;
    }
    public function getFmtTipoAttribute() {
        if($this->rel_tipo) {
            return $this->rel_tipo->descripcion;
        }
        else {

            return "-";

        }
    }

    public function rel_tipo(){
        return $this->belongsTo(\App\Models\cat_item::class,'tipo','id_item');

    }

    public static function acta_t($id_proyecto, $id_fase)
    {
        $id_a = Self::where('id_proyecto', $id_proyecto)->where('id_fase', $id_fase)->get();
        return $id_a;
    }
}
