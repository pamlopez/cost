<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class cost_ampliacion
 * @package App\Models
 * @version January 17, 2019, 5:15 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection asesoriaLlamada
 * @property bigInteger id_ampliacion
 * @property integer id_proyecto
 * @property integer tipo
 * @property integer tipo_ampliacion
 * @property integer no_ampliacion
 * @property date fecha_ampliacion
 * @property string no_registro_ampliacion
 * @property integer avance_financiero
 * @property integer avance_obra
 * @property string link
 * @property string observaciones
 */
class cost_ampliacion extends Model
{
   // use SoftDeletes;

    public $table = 'cost_ampliacion';
    protected $primaryKey='id_ampliacion';
    public $timestamps=false;

  //  const CREATED_AT = 'created_at';
  //  const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id_proyecto',
        'id_fase',
        'tipo',
        'tipo_ampliacion',
        'no_ampliacion',
        'fecha_ampliacion',
        'no_registro_ampliacion',
        'avance_financiero',
        'avance_obra',
        'link',
        'observaciones'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_proyecto' => 'integer',
        'id_fase' => 'integer',
        'tipo' => 'integer',
        'tipo_ampliacion' => 'integer',
        'no_ampliacion' => 'integer',
        'fecha_ampliacion' => 'date',
        'no_registro_ampliacion' => 'string',
        'avance_financiero' => 'integer',
        'avance_obra' => 'integer',
        'link' => 'string',
        'observaciones' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

        public function setTipoAmpliacionAttribute($tipo_ampliacion){$this->attributes['tipo_ampliacion']= strlen($tipo_ampliacion)>0 ? $tipo_ampliacion : null ; }
        public function setNoAmpliacionAttribute($no_ampliacion){$this->attributes['no_ampliacion']= strlen($no_ampliacion)>0 ? $no_ampliacion : null ; }
        public function setFechaAmpliacionAttribute($fecha_ampliacion){$this->attributes['fecha_ampliacion']= strlen($fecha_ampliacion)>0 ? $fecha_ampliacion : null ; }
        public function setNoRegistroAmpliacionAttribute($no_registro_ampliacion){$this->attributes['no_registro_ampliacion']= strlen($no_registro_ampliacion)>0 ? $no_registro_ampliacion : null ; }
        public function setAvanceFinancieroAttribute($avance_financiero){$this->attributes['avance_financiero']= strlen($avance_financiero)>0 ? $avance_financiero : null ; }
        public function setAvanceObraAttribute($avance_obra){$this->attributes['avance_obra']= strlen($avance_obra)>0 ? $avance_obra : null ; }
        public function setLinkAttribute($link){$this->attributes['link']= strlen($link)>0 ? $link : null ; }
        public function setObservacionesAttribute($observaciones){$this->attributes['observaciones']= strlen($observaciones)>0 ? $observaciones : null ; }

    public function getIdAttribute() {
        return $this->id_ampliacion;
    }
    public static function ampliacion_p($id_proyecto, $id_fase){
        $id_amp= Self::where('id_proyecto',$id_proyecto)->where('id_fase',$id_fase)->get();
        return $id_amp;
    }
}
