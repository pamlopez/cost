<?php

namespace App\Importar;

use App\Models\asesoria_proceso;
use App\Models\proceso;
use App\Models\atencion_inicial;
use App\Models\persona;
use App\Models\proceso_accion;
use App\Models\proceso_accion_llamada;
use Carbon\Carbon;
use App\Importar\importar_tabla;
use App\Importar\importar_traza;
use App\Importar\importar_bitacora;
use App\Models\atencion_inicial_tipo_violencia;
use App\Models\atencion_inicial_referencia_caimu;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_imp_asesoria
 * @property int $id_importar_bitacora
 * @property string $hoja_vida
 * @property string $nombre
 * @property string $etnia
 * @property string $municipio
 * @property string $departamneto
 * @property string $zona
 * @property string $mes
 * @property string $anio
 * @property string $f_atencion_inicial
 * @property string $f_entrevista_legal
 * @property string $atencion_emergencia
 * @property string $abogada_atendio
 * @property string $contenido_estrategia
 * @property string $mds
 * @property string $procesos_iniciados
 * @property string $cantidad_procesos_iniciados
 * @property string $procesos_por_iniciar
 * @property string $cantidad_proyectos_demandas
 * @property string $status
 * @property string $avance
 * @property string $asiganada_a
 * @property string $papeleria
 * @property string $documentos_faltan
 * @property string $accion
 * @property string $control_ultima_llamada
 * @property string $evolucion_llamadas
 * @property string $obsevaciones
 * @property int $importado
 * @property string $control
 */

class imp_asesoria extends Model
{
    //
    protected $table = 'imp_asesoria';
    protected $primaryKey = "id_imp_asesoria";
    public $timestamps = FALSE;
    protected $guarded=['id_imp_asesoria'];


    public static function conteos() {
        $respuesta = new \stdClass();
        $respuesta->filas_total=self::count();;
        $respuesta->filas_exito=self::where('importado',1)->count();
        $respuesta->filas_fallo=self::where('importado',99)->count();
        $respuesta->filas_pendiente=self::where('importado',0)->count();
        if($respuesta->filas_total > 0) {
            $respuesta->avance =  ($respuesta->filas_total - $respuesta->filas_pendiente) / $respuesta->filas_total;
            $respuesta->avance = intval($respuesta->avance * 100);
        }
        else {
            $respuesta->avance=0;
        }

        return $respuesta;
    }




    //////////
    /*
     * Procesa imp_ai para importar atencion_inicial y persona
     */
    public static function importar($cantidad=10) {
        $listado=self::where('importado',0)->orderby('id_imp_asesoria')->limit($cantidad)->get();
        $conteo=$listado->count();

        if($conteo == 0) {
            //Fin de la importación
            $log="<br>Nada que importar";
            $resultado=false;
        }
        else {

            $log="<br>Importando bloque de $conteo filas en 9.1 Asesoría";
            $log_error="";

            foreach($listado as $fila) {
                $log.="<br>Fila $fila->id_imp_asesoria, correlativo $fila->numero ";
                //No aceptar sin número de hoja de vida
                if(strlen($fila->hoja_vida)<1) {
                    $error="Fila $fila->id_imp_asesoria  ignorada, sin numero de hoja de vida.  $fila->nombre<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }

                else {
                    //Al final, actualizar la fila importada
                    $fila->importado=1;


                    //Primer bloque: Atención inicial
                    $hoja_vida = atencion_inicial::where('numero_historia_vida', $fila->hoja_vida)->first();
                    if ($hoja_vida) { //Ya está, no hacer nada
                        $log.="<br>H.V.  $fila->hoja_vida existente";
                    }
                    else {  //No existe, crear una nueva
                        $hoja_vida = new atencion_inicial();
                        $hoja_vida->numero_historia_vida = $fila->hoja_vida;
                        $hoja_vida->fecha = importar_tabla::procesar_fecha_auto($fila->f_atencion_inicial);
                        $hoja_vida->edad = null;
                        $hoja_vida->id_rango_caimu = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_caimu");
                        $hoja_vida->id_rango_mingob = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_mingob");
                        $hoja_vida->id_rango_ine = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_ine");

                        //Hard code
                        $hoja_vida->id_caimu = 177;
                        //Determinar persona
                        $creacion = self::crear_persona($fila);
                        if ($creacion['resultado']) {
                            $hoja_vida->id_persona = $creacion['objeto']->id_persona;
                            $log .= " Creada persona " . $creacion['objeto']->id_persona;
                            importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_persona' => $creacion['objeto']->id_persona]);
                        } else {
                            $hoja_vida->id_persona = null;
                            $error = "<Br>Fila $fila->id_imp_ai.  No se pudo crear la persona: " . $creacion['mensaje'] . ". ";
                            $log .= $error;
                            $log_error .= $error;
                        }
                        //Grabar
                        $hoja_vida->save();
                        $log .= " Atención inicial creada. ";
                        //Traza de la A.I.
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_atencion_inicial' => $hoja_vida->id_atencion_inicial]);

                    }
                    //Fin del primer bloque

                    $log.="<br> Asesoria. ";

                    //Segundo bloque: asesoría
                    $asesoria=new proceso_accion();  //El modelo apunta a "asesoria"
                    $asesoria->id_atencion_inicial = $hoja_vida->id_atencion_inicial;
                    $asesoria->fh_entrevista_legal = importar_tabla::procesar_fecha_auto($fila->f_entrevista_legal);
                    $asesoria->id_atencion_emergencia = importar_tabla::procesar_si_no($fila->atencion_emergencia);
                    $asesoria->id_abogada_atendio = importar_tabla::determinar_empleada($fila->abogada_atendio);
                    $asesoria->contenido_estrategia = importar_tabla::procesar_texto($fila->contenido_estrategia);
                    $asesoria->mds = importar_tabla::procesar_texto($fila->mds);
                    $asesoria->save();
                    importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_asesoria' => $asesoria->id_asesoria]);

                    //Bloque: asesoria_proceso
                    if($fila->procesos_iniciados) {
                        if(mb_strtolower($fila->procesos_iniciados) <> "ninguno" ) {
                            $asesoria_proceso = new asesoria_proceso();
                            $asesoria_proceso->id_atencion_inicial = $hoja_vida->id_atencion_inicial;
                            $asesoria_proceso->id_tipo_proceso_asesoria = 1;
                            $asesoria_proceso->id_clase_proceso = importar_tabla::determinar_cat_item($fila->procesos_iniciados,31);
                            $asesoria_proceso->save();
                            importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_asesoria_proceso' => $asesoria_proceso->id_asesoria_proceso]);

                            $log.=" (proceso iniciado)";
                        }
                    }
                    if($fila->procesos_por_iniciar) {
                        if(mb_strtolower($fila->procesos_por_iniciar) <> "ninguno" ) {
                            $asesoria_proceso = new asesoria_proceso();
                            $asesoria_proceso->id_atencion_inicial = $hoja_vida->id_atencion_inicial;
                            $asesoria_proceso->id_tipo_proceso_asesoria = 2;
                            $asesoria_proceso->id_clase_proceso = importar_tabla::determinar_cat_item($fila->procesos_por_iniciar,31);
                            $asesoria_proceso->save();
                            importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_asesoria_proceso' => $asesoria_proceso->id_asesoria_proceso]);

                            $log.=" (proceso por iniciar)";
                        }
                    }

                    //Bloque: Asesoría (continuacion)
                    $asesoria->id_estado = importar_tabla::determinar_cat_item($fila->status,29);
                    $asesoria->id_avance = importar_tabla::determinar_cat_item($fila->avance,43);
                    $asesoria->id_asignada_a =  importar_tabla::determinar_empleada($fila->asignada_a);
                    $asesoria->id_estado_papeleria = importar_tabla::determinar_cat_item($fila->papeleria,40);
                    $asesoria->docs_que_faltan = importar_tabla::procesar_texto($fila->documentos_faltan);
                    $asesoria->id_accion = importar_tabla::determinar_cat_item($fila->accion,41);
                    $asesoria->observaciones = importar_tabla::procesar_texto($fila->observaciones);
                    $asesoria->save();


                    //Bloque: evolucion de llamadas
                    if($fila->control_ultima_llamada) {
                        $a_llamada = new proceso_accion_llamada();
                        $a_llamada->id_atencion_inicial = $hoja_vida->id_atencion_inicial;
                        $a_llamada->id_control_ultima_llamada = importar_tabla::determinar_cat_item($fila->control_ultima_llamada,42);
                        $a_llamada->evolucion_llamada = importar_tabla::procesar_texto($fila->evolucion_llamadas);
                        $a_llamada->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_asesoria_llamada' => $a_llamada->id_asesoria_llamada]);
                        $log.=" (llamada) ";
                    }

                    $log .= " Fila procesada. <br> ";


                }

                $fila->save();
                //$log.=" (Fila temporal actualizada).";
            }
            $log.="<br>Fin del bloque<br>";

            //Actualizar el log de errores
            if(strlen($log_error) > 0) {
                $cualquiera=self::first();
                $encabezado=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $encabezado->log_procesa.=$log_error;
                $encabezado->save();
            }


            //Determinar si faltan mas
            $conteo=self::where('importado',0)->count();


            if($conteo==0) {  //Llegamos al final, hay que actualizar encabezados
                //Fin de la importación
                $log.="<br><h3>Fin de la importacion</h3>";
                //Actualizar control de versiones y bitacora
                $cualquiera=self::first();
                $bitacora=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $bitacora->id_estado=3;
                $bitacora->save();
            }
            $resultado = true;
        }

        //Conteos
        $cierre = new \stdClass();
        $cierre->filas_total    =self::count();
        $cierre->filas_exito    =self::where('importado',1)->count();
        $cierre->filas_fallo    =self::where('importado',99)->count();
        $cierre->filas_pendiente  =self::where('importado',0)->count();
        $cierre->log=$log;
        $cierre->log_error=$log_error;
        $cierre->resultado=$resultado;
        return $cierre;
    }

    // Recibe los datos de una fila de imp_asesoria y crea la persona
    public static function crear_persona($fila_datos) {
        //Limpiar de guiones
        $nombre_apellido = importar_tabla::procesar_texto($fila_datos->nombre);
        if(strlen($nombre_apellido)<1) {
            return array("resultado"=>false, "objeto"=>null, "mensaje"=>"Nombre en blanco");
        }
        else {
            $existe = persona::where('nombre_completo','like',$nombre_apellido)->first();
            if(isset($existe->id_persona)) {
                return array("resultado"=>true, "objeto"=>$existe);
            }
            else {
                $persona=new persona();
                $persona->nombre_completo=$nombre_apellido;
                //De plano son mujeres
                $persona->genero=1;


                $muni=importar_tabla::procesar_municipio($fila_datos->municipio,$fila_datos->departamneto);
                if($muni<>null) {
                    $persona->id_depto_domicilio=$muni->id_padre;
                    $persona->id_muni_domicilio=$muni->id_geo;
                }
                $persona->zona_domicilio=$fila_datos->zona;
                $persona->id_etnia = importar_tabla::determinar_cat_item($fila_datos->etnia,6);


                try {
                    $persona->save();
                    return array("resultado"=>true, "objeto"=>$persona);
                }
                catch (\Exception $e) {
                    return array("resultado"=>false, "objeto"=>null, "mensaje"=>$e->getMessage());
                }
            }
        }

    }





}
