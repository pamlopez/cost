<?php

namespace App\Importar;

use App\Models\agenda;
use App\Models\asesoria_proceso;
use App\Models\atencion_inicial;
use App\Models\control_asistencia;
use App\Models\persona;
use App\Models\proceso_accion;
use App\Models\proceso_accion_llamada;
use App\Models\proceso_detalle_demanda_familia;
use App\Models\proceso_detalle_demanda_penal;
use Illuminate\Database\Eloquent\Model;

class importar_bitacora extends Model
{
    //
    protected $table = 'importar_bitacora';
    protected $primaryKey = "id_importar_bitacora";
    protected $fillable = ['ubicacion', 'id_tipo', 'id_estado', ];
    public $timestamps = FALSE;
    protected $dates = [
        'fecha_hora',
        'control'
    ];
    protected $carpeta="uploads/";

    //Traza de la importacion
    public function rel_traza() {
        return $this->hasMany(importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->orderby('id_importar_traza');
    }
    //trazas especificas
    public function rel_atencion_inicial() {
        return $this->hasMany(\App\Importar\importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->wherenotnull('id_atencion_inicial')
            ->orderby('id_importar_traza');
    }
    public function rel_persona() {
        return $this->hasMany(\App\Importar\importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->wherenotnull('id_persona')
            ->orderby('id_importar_traza');
    }

    public function rel_asesoria() {
        return $this->hasMany(importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->wherenotnull('id_asesoria')
            ->orderby('id_importar_traza');
    }
    public function rel_asesoria_proceso() {
        return $this->hasMany(importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->wherenotnull('id_asesoria_proceso')
            ->orderby('id_importar_traza');
    }
    public function rel_asesoria_llamada() {
        return $this->hasMany(importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->wherenotnull('id_asesoria_llamada')
            ->orderby('id_importar_traza');
    }
    public function rel_proceso() {
        return $this->hasMany(importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->wherenotnull('id_proceso')
            ->orderby('id_importar_traza');
    }
    public function rel_proceso_familia() {
        return $this->hasMany(importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->wherenotnull('id_proceso_detale_demanda_familia')
            ->orderby('id_importar_traza');
    }
    public function rel_proceso_penal() {
        return $this->hasMany(importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->wherenotnull('id_proceso_detalle_demanda_penal')
            ->orderby('id_importar_traza');
    }
    public function rel_agenda() {
        return $this->hasMany(importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->wherenotnull('id_agenda')
            ->orderby('id_importar_traza');
    }
    public function rel_asistencia() {
        return $this->hasMany(importar_traza::class,'id_importar_bitacora','id_importar_bitacora')
            ->wherenotnull('id_control_asistencia')
            ->orderby('id_importar_traza');
    }









    public static function a_tipos() {
        $a_tipos=array(1=>"Atencion Inicial", 2=>"9.1 Asesoría", 3=>"9.2 Familia", 4=>"9.3 Penalees VIF", 5=>"9.4 Familia Archivado",6=>"9.5 Penal Archivado",7=>"Agenda legal",8=>"Control de asistencia", 10=>"Agenda Psicología",11=>"Agenda T.S.",12=>"Agenda Albergue");

        return $a_tipos;
    }

    public function getFmtIdTipoAttribute() {
        $a_tipos=self::a_tipos();
        if(isset($a_tipos[$this->id_tipo])) {
            return $a_tipos[$this->id_tipo];
        }
        else {
            return "Desconocido";
        }
    }


    public function getFmtFechaHoraAttribute() {
        return $this->fecha_hora->formatlocalized("%a %d/%b/%Y %H:%M");
    }
    public function getFmtIdEstadoAttribute() {
        $a_estado=array(0=>"Sin especificar",1=>"(1/3) Cargado al servidor",2=>"(2/3) Importado a tabla temporal",3=>"(3/3) Procesado y finalizado",99=>"Anulado");
        return $a_estado[$this->id_estado];
    }
    public function getLinkArchivoAttribute() {
        $url=url("uploads/$this->ubicacion");
        $str="<a href='$url'>".substr($this->ubicacion,16)."</a>";
        return $str;
    }

    public function getLinkFechaAttribute() {
        $url=url("uploads/$this->ubicacion");
        $str="<a href='$url'>".$this->fmt_fecha_hora."</a>";
        return $str;
    }

    //Wrapper para determinar conteos
    public function getConteosAttribute() {

        $respuesta = new \stdClass();
        $respuesta->filas_total=0;
        $respuesta->filas_exito=0;
        $respuesta->filas_fallo=0;
        $respuesta->filas_pendiente=0;
        $respuesta->avance=0;

        if($this->id_estado==2 ) {
            if($this->id_tipo==1) {
                $respuesta=\App\Importar\imp_ai::conteos();
            }
        }

        return $respuesta;

    }


    public  function getConteosFinalesAttribute() {
        $conteos = new \stdClass();
        $conteos->filas_procesadas=0;
        $conteos->total_registros_creados=$this->rel_traza->count();
        $conteos->atencion_inicial=$this->rel_atencion_inicial->count();
        $conteos->persona=$this->rel_persona->count();

        //$conteos->proceso=$this->rel_proceso->count();
        $conteos->asesoria=$this->rel_asesoria->count();
        $conteos->asesoria_proceso=$this->rel_asesoria_proceso->count();
        $conteos->asesoria_llamada=$this->rel_asesoria_llamada->count();

        $conteos->proceso = $this->rel_proceso->count();
        $conteos->proceso_detalle_familia = $this->rel_proceso_familia->count();
        $conteos->proceso_detalle_penal   = $this->rel_proceso_penal->count();

        $conteos->agenda = $this->rel_agenda->count();
        $conteos->asistencia   = $this->rel_asistencia->count();



        $modelo=importar_tabla::find($this->id_tipo);
        $conteos->filas_procesadas = \DB::table($modelo->tabla)->count();

        return $conteos;
    }




    //Segun el estado, determina lo siguiente a realizar
    public function procesar() {

        set_time_limit(0);


        $res=new \stdClass();
        $res->resultado=false;
        $res->filas_total=0;
        $res->filas_exito=0;
        $res->filas_fallo=0;
        $res->filas_pendiente=0;
        $res->avance=0;
        $res->log="Procesar archivo";
        $res->log_error="Registro de errores";


        if($this->id_estado==1) {  //Recien cargado, importar XLS

            $res->resultado = $this->importar_excel();

            $res->log=$this->log_importa;
            $res->log_error=$this->log_error;
            //Estos valores se establecen en la importacion
            $res->filas_total=$this->filas_total;
            $res->filas_exito=$this->filas_exito;
            $res->filas_fallo=$this->filas_fallo;
            //Nunca hay filas pendientes porque el archivo se procesa de un solo
        }
        elseif($this->id_estado == 2) {  // Importado
            switch ($this->id_tipo) {
                case 1:
                    $res=\App\Importar\imp_ai::importar();
                    break;
                case 2:
                    $res=imp_asesoria::importar();
                    break;
                case 3:
                    $res=imp_familia::importar();
                    break;
                case 4:
                    $res=imp_penal::importar();
                    break;
                case 5:
                    $res=imp_familia_a::importar();
                    break;
                case 6:
                    $res=imp_penal_a::importar();
                    break;
                case 7:
                    $res=imp_agenda::importar();
                    break;
                case 8:
                    $res=imp_asistencia::importar();
                    break;
                case 10:
                    $res=imp_agenda_p::importar();
                    break;
                case 11:
                    $res=imp_agenda_ts::importar();
                    break;
                case 12:

                    $res=imp_agenda_a::importar();
                    break;
            }
        }
        elseif($this->id_estado == 3) {  // Terminado
            switch ($this->id_tipo) {
                case 1:
                    $res=\App\Importar\imp_ai::conteos();
                    break;
                case 2:
                    $res=imp_asesoria::conteos();
                    break;
                case 3:
                    $res=imp_familia::conteos();
                    break;
                case 4:
                    $res=imp_penal::conteos();
                    break;
                case 5:
                    $res=imp_familia_a::conteos();
                    break;
                case 6:
                    $res=imp_penal_a::conteos();
                    break;
            }
        }

        return $res;

    }



    //Proceso de importación
    public function importar_excel() {
        $id=$this->id_importar_bitacora;
        //dd("Importar $id");
        $texto="<h3>Bitacora de importacion</h3>";
        $this->hora_inicio=microtime(true);
        $texto.="<br>Inicio: ".date("H:i:s",$this->hora_inicio)."<br>";

        // Archivo a importar
        $this->info_archivo=\App\Importar\importar_bitacora::find($id);
        if(!$this->info_archivo) {
            $this->texto.="<h2 class='text-error'>Problema</h2>";
            $this->texto.="Identificador de archivo ($id) no existe";
            return false;
        }

        if($this->info_archivo->id_estado<>1) {
            $this->texto.="<h2 class='text-danger'>Problema</h2>";
            $this->texto.="El archivo ($id) ya ha sido procesado";
            return false;
        }



        // Reglas de ese tipo de archivo
        $this->info_reglas=\App\Importar\importar_tabla::find($this->info_archivo->id_tipo);
        if(!$this->info_reglas) {
            $this->texto.="<h2 class='text-error'>Problema</h2>";
            $this->texto.="El archivo con el identificador ($id) corresponde a un tipo no válido";
            return false;
        }






        //Archivito de excel
        $archivo=$this->carpeta.$this->info_archivo->ubicacion;

        // Para retroalimentar al usuario en la pantalla por ajax

        $texto.="Importar archivo de ".$this->info_reglas->descripcion.", cargado el ". $this->info_archivo->fecha_hora;
        // Paso 1: truncate a la tabla
        \DB::table($this->info_reglas->tabla)->truncate();
        $nombre_tabla=$this->info_reglas->tabla;
        $texto.="<br>Tabla temporal '$nombre_tabla' borrada";
        $hora=microtime(true);
        $texto.="<br>Antes de abrir el excel: ".date("H:i:s",$hora);



        $this->error=false;

        //Abrir el archivo de excel
        \Excel::load($archivo, function($reader)  use (&$texto) {

            $hora=microtime(true);
            $texto.="<br>Luego de abrir el excel: ".date("H:i:s",$hora);
            $tiempo = $hora - $this->hora_inicio;

            $texto.="<br>Tiempo en abrir el excel: $tiempo ";

            //Para usar menos memoria
            $reader->setReadDataOnly(true);
            $reader->calculate(false); //mas rapido
            //Mas rapidin
            if($this->info_archivo->id_tipo == 1 ) {  //Atencion inicial
                //Manejo de fechas
                $reader->formatDates(false);
            }
            else {
                //Prueba
                //$reader->formatDates(true, 'Y-m-d');
            }


            $hojas = $reader->noHeading()->get();




            // Ojo: la configuracion del importador, tiene que tener 'force_sheets_collection' => true
            $tabla=$hojas->first();

            $linea = 0;
            $encabezados=array();

            //dd("encabezados en fila # ".$this->info_reglas->fila_encabezados);

            // Buscar fila de encabezados
            foreach ($tabla as $registro) {
                if ($linea == $this->info_reglas->fila_encabezados) {
                    $encabezados = $registro;
                    break;
                }
                $linea++;
            }

            $valido = $this->revisar_encabezados($encabezados);

            //dd($encabezados);


            if(!$valido) {
                $texto.="<br><span class='text-danger'>Error: el archivo no contiene la estructura esperada</span>";
                $texto.=$this->texto;

                //Anular el archivo
                $this->info_archivo->id_estado =99 ;
                $this->info_archivo->log_importa =$texto ;
                $this->info_archivo->save() ;
                //Devolver error
                $this->error=true;
                return false;
            }
            else {
                $texto.="<br>Estructura del archivo verificada exitosamente.";
                // Proceder con la importada
                $linea = 0;
                $lineas_importadas=0;
                foreach ($tabla as $registro) {
                    if ($linea > $this->info_reglas->fila_encabezados) {  //A partir de la linea de encabezados
                        $texto.="<br>Fila ".$linea;
                        $exito=$this->importar_linea($registro->toarray());
                        if($exito) {
                            $lineas_importadas++;
                            //$texto.=" OK.";
                        }
                        else {
                            $texto.=" <span class='text-danger'>Problema al importar. Revisar archivo</span>";
                        }
                    }
                    else {
                        $texto.="<br>Ignorando fila ".$linea;
                    }
                    $linea++;
                }
                $texto.="<br><br>FIN de la importacion. $lineas_importadas filas procesadas";
                return true;
            }
        });

        //PAra ver si hubo error en el procesamiento
        if($this->error) {
            $exito=false;
        }
        else {
            $exito=true;
            //Actualizar archivo importado
            unset($this->error);  //Para que no busque este campo en la tabla
            $this->info_archivo->id_estado =2 ;
            $this->info_archivo->log_importa =$texto ;
            $this->info_archivo->save() ;
            $texto.="<br><br>Bitácora del archivo actualizada ";

            $hora=microtime(true);
            $tiempo = $hora - $this->hora_inicio;
            $texto.="<br><br>Tiempo en procesar el excel: $tiempo ";
            $this->texto=$texto;
        }


        return $exito;



    }

    //Esto me sirve para importar archivos de texto
    public  function importar_csv() {
        ini_set("auto_detect_line_endings", true);


        $this->log_importa="<h3>Bitacora de importacion CSV</h3>";
        $this->hora_inicio=microtime(true);
        $this->log_importa.="<br>Inicio: ".date("H:i:s",$this->hora_inicio)."<br>";


        // Reglas de ese tipo de archivo
        $this->info_reglas=\App\importar_tabla::find($this->id_tipo);
        if(!$this->info_reglas) {
            $this->log_importa.="<h2 class='text-error'>Problema</h2>";
            $this->log_importa.="Identificador de tipo de archivo ($this->id_tipo) no existe";
            return false;
        }

        //dd($this->fmt_fecha_hora);


        //Ubicar archivo CSV
        $archivo="public/uploads/".$this->ubicacion;
        $archivo=base_path($archivo);
        if(!file_exists($archivo)) {
            $this->log_importa.="<h2 class='text-error'>Problema</h2>";
            $this->log_importa.="No existe el archivo: $archivo";
            return false;
        }






        $this->log_importa.="Importar datos para:".$this->fmt_id_tipo." Archivo: $this->ubicacion , cargado el ". $this->fmt_fecha_hora."<br><br>";
        // Paso 1: truncate a la tabla
        $nombre_tabla=$this->info_reglas->tabla;
        \DB::table($nombre_tabla)->truncate();
        //$this->log_importa.="<br>Tabla temporal '$nombre_tabla' borrada";
        $hora=microtime(true);
        //$this->log_importa.="<br>Antes de abrir el csv: ".date("H:i:s",$hora);




        // Abrir el archivo a bajo nivel y procesarlo linea por linea

        $handle = fopen($archivo, 'r');

        if ($handle) {
            $fila=0; //Contador de filas
            $fila_exito=0; // Filas procesadas exitosamente
            $fila_fallo=0; //Filas no procesadas (con error)
            while ($line = fgetcsv($handle)) {
                $line = array_map("utf8_encode", $line);  //convertir a UTF-8
                if($fila==0) { //Revisar Encabezados
                    $valido=$this->revisar_encabezados($line);
                    if(!$valido) {
                        $this->log_importa.="<br>Fin de la importación por errores al validar estructura.";
                        return false;
                    }
                }
                else {
                    //$this->log_importa.="<br> Fila $fila: ";
                    $exito = $this->importar_linea($line);

                    if($exito) {
                        $fila_exito++;
                        //$this->log_importa.=" importada";
                    }
                    else {
                        $fila_fallo++;
                        $this->log_error.="<br> Fila $fila: ";
                        $this->log_importa.="<br> Fila $fila: ";
                        $this->log_importa.=" <span class='text-danger'>No importada</span>";
                    }
                }
                $fila++;
            }
        }
        $hora=microtime(true);
        $fila--;
        $this->log_importa.="<br>Fin del procesamiento.  $fila filas procesadas. ".date("H:i:s",$hora);

        //Cierre
        $hora=microtime(true);
        $tiempo = $hora - $this->hora_inicio;
        $tiempo=number_format($tiempo,2);
        $this->log_importa.="<br><br>Tiempo en segundos para procesar el csv: $tiempo ";
        if(strlen($this->log_error)>0) {
            $this->log_importa = "Advertencia, hubo errores en la importación.  Se recomienda revisar lo siguiente: <br> $this->log_error <br><br>".$this->log_importa;
        }
        //Actualizar archivo importado
        $this->id_estado =2 ;
        //Quito estas propiedades para poder actualizar la tabla
        unset($this->info_reglas);
        unset($this->encabezados);
        unset($this->importar_campos);
        unset($this->hora_inicio);
        $log_error=$this->log_error;  //temporal
        unset($this->log_error);
        $this->save() ;
        $this->log_importa.="<br>Bitácora del archivo actualizada ";
        $this->filas_total=$fila;
        $this->filas_exito=$fila_exito;
        $this->filas_fallo=$fila_fallo;
        $this->log_error=$log_error;


        return true;
    }







    public function revisar_encabezados( $encabezados) {
        $this->encabezados = new \stdClass();
        $this->encabezados->excel=$encabezados;  //Para mostrar diferencia
        //dd($encabezados);

        $esperado=\App\Importar\importar_campo::a_estructura($this->id_tipo);
        $this->encabezados->esperado=$esperado; //Para mostrar diferencia

        // La cantidad de columnas recibidas no puede ser menor a las esperadas
        if(count($encabezados) < count($esperado)) {
            $error="<br>ERROR en estructura del archivo. ";
            $error.="Se esperan ".count($esperado)." columnas, pero se detectaron ".count($encabezados);

            $this->log_importa.=$error;
            $this->log_error  .=$error;
            return false;
        }

        foreach($encabezados as $id=>$value) {

            if(isset($esperado[$id])) {
                if($value <> null) {
                    if(trim(mb_strtolower($esperado[$id]))<> trim(mb_strtolower($value))) {
                        $error="<br>ERROR en estructura del archivo: ";
                        $error.="se esperaba la columna $id: '".$esperado[$id]."', pero se detectó la columna '$value''";

                        $this->log_importa .=$error;
                        $this->log_error   .=$error;

                        return false;
                    }
                }
            }
        }
        return true;

    }

    //Recibe el arreglo recién leido del CSV
    public function importar_linea($linea) {
        //convierte arreglo asociativo a arreglo numerico
        //$linea=array_values($linea->toArray());



        // Obtener la estructura del registro y el numero de columna de cada dato
        if(count($this->importar_campos) <1) {
            $this->importar_campos = \App\Importar\importar_campo::a_estructura_campos($this->info_reglas->id_importar_tabla);
        }

        // Crear una instancia para el registro
        $tabla=$this->info_reglas->tabla;
        $clase= "\App\Importar\\".$tabla;
        $registro = new $clase();

        // Ciclo para explorar c/columna
        foreach($this->importar_campos as $columna=>$campo) {
            $valor=$linea[$columna];
            // Este chapulin es para eliminar los decimales de los enteros
            if(is_numeric($valor)) {
                if($valor==intval($valor)) {
                    $valor=intval($valor);
                }
            }
            //Eliminar espacios en blanco
            if(is_string($valor)) {
                $valor=trim($valor);
            }
            $registro->$campo=$valor;
        }
        //Agregarle llave foranea
        $registro->id_importar_bitacora =  $this->id_importar_bitacora;

        //Grabar a la tabla
        try {
            $exito=$registro->save();
        }
        catch (\Exception $e) {
            $error=" ERROR: datos inconsistentes. <br><pre> ".$e->getMessage()."</pre><br>";
            $this->log_error.=$error;
            $this->log_importa.=$error;
            $exito=false;
        }

        return $exito;
    }

    //Deshace lo importado p
    function anular() {
        $this->id_estado=1;
        $this->log_importa=null;
        $this->log_procesa=null;
        $this->save();


        proceso_detalle_demanda_familia::join('importar_traza','importar_traza.id_proceso_detale_demanda_familia','=','proceso_detalle_demanda_familia.id_proceso_detale_demanda_familia')
            ->where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->delete();

        proceso_detalle_demanda_penal::join('importar_traza','importar_traza.id_proceso_detalle_demanda_penal','=','proceso_detalle_demanda_penal.id_proceso_detalle_demanda_penal')
            ->where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->delete();

        //Borrar asesoria_llamada
        proceso_accion_llamada::join('importar_traza','importar_traza.id_asesoria_llamada','=','asesoria_llamada.id_asesoria_llamada')
            ->where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->delete();


        //Borrar asesoria_proceso
        asesoria_proceso::join('importar_traza','importar_traza.id_asesoria_proceso','=','asesoria_proceso.id_asesoria_proceso')
            ->where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->delete();

        //Borrar asesoria
        proceso_accion::join('importar_traza','importar_traza.id_asesoria','=','asesoria.id_asesoria')
            ->where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->delete();





        // Borrar personas
        persona::join('importar_traza','importar_traza.id_persona','=','persona.id_persona')
            ->where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->delete();

        //Borrar atencion inicial
        atencion_inicial::join('importar_traza','importar_traza.id_atencion_inicial','=','atencion_inicial.id_atencion_inicial')
            ->where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->delete();

        //Borrar agenda
        agenda::join('importar_traza','importar_traza.id_agenda','=','agenda.id_agenda')
            ->where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->delete();

        //Borrar control_asistencia
        control_asistencia::join('importar_traza','importar_traza.id_control_asistencia','=','control_asistencia.id_control_asistencia')
            ->where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->delete();





        \App\Importar\importar_traza::where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->delete();

        imp_ai::where('id_importar_bitacora','=',$this->id_importar_bitacora)
                    ->update(['importado'=>0]);
        imp_asesoria::where('id_importar_bitacora','=',$this->id_importar_bitacora)
            ->update(['importado'=>0]);




    }
    //Deshace lo importado p
    public static function resetear($id) {
        $cual=self::find($id);
        if($cual) {
            $cual->anular();
            $cual->id_estado=1;
            $cual->log_procesa=null;
            $cual->save();
            return true;
        }
        else {
            return false;
        }


    }
}
