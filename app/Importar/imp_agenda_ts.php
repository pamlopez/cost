<?php

// php artisan krlov:generate:model importar/imp_asistencia --table-name=imp_asistencia

namespace App\Importar;

use App\Models\agenda;
use Illuminate\Database\Eloquent\Model;
use App\Models\atencion_inicial;
use App\Models\persona;
use Carbon\Carbon;
use App\Importar\importar_tabla;
use App\Importar\importar_traza;
use App\Importar\importar_bitacora;
use App\Models\atencion_inicial_tipo_violencia;
use App\Models\atencion_inicial_referencia_caimu;
use App\Importar\imp_agenda;

/**
 * @property int $id_imp_agenda
 * @property int $id_importar_bitacora
 * @property string $dia_semana
 * @property string $dia_mes
 * @property string $mes
 * @property string $anio
 * @property string $abogada_atendio
 * @property string $hora_inicio
 * @property string $hora_fin
 * @property string $vehiculo
 * @property string $hora_salida
 * @property string $lugar
 * @property string $direccion
 * @property string $accion
 * @property string $tipo
 * @property string $realizada
 * @property string $tipo_proceso
 * @property string $hoja_vida
 * @property string $nombre
 * @property string $quien_agenda
 * @property string $instruccion
 * @property string $resultado
 * @property int $importado
 * @property string $control
 */
class imp_agenda_ts extends imp_agenda
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'imp_agenda_ts';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_imp_agenda';


    //Agenda psicologia

    public static function importar($cantidad=10) {
        $listado=self::where('importado',0)
            ->limit($cantidad)
            ->get();
        $conteo=$listado->count();

        if($conteo == 0) {
            //Fin de la importación
            $log="<br>Nada que importar";
            $resultado=false;
        }
        else {

            $log="<br>Importando bloque de $conteo filas en Agenda (trabajo social)";
            $log_error="";

            foreach($listado as $fila) {
                $log.="<br>Fila $fila->id_imp_agenda ";
                //El mes viene como "Enero"
                $mes = importar_tabla::procesar_mes($fila->mes);
                //No aceptar sin fecha
                if(strlen($fila->dia_mes)<1) {
                    $error="Fila $fila->id_imp_agenda  ignorada, sin dia del mes<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }
                elseif(!$mes) {
                    $error="Fila $fila->id_imp_agenda  ignorada, mes inválido ($mes).<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }
                elseif(strlen($fila->anio)<1) {
                    $error="Fila $fila->id_imp_agenda  ignorada, sin anio<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }
                elseif(strlen($fila->hoja_vida) < 4 || strlen($fila->hoja_vida) > 15 ) {
                    $error="Fila $fila->id_imp_agenda  ignorada, sin número de hoja de vida<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }

                else {

                    //Fecha: usar el mes convertido para las validaciones

                    $fecha=Carbon::create($fila->anio,$mes,$fila->dia_mes);
                    $nuevo =new agenda();
                    $nuevo->id_area = 3; //Criterio fijo para trabajo social
                    $nuevo->fh_agenda   = $fecha->format("Y-m-d");
                    $nuevo->id_abogada  = importar_tabla::determinar_empleada($fila->abogada_atendio);
                    $nuevo->hr_inicio   = importar_tabla::procesar_hora($fila->hora_inicio);
                    $nuevo->hr_fin      = importar_tabla::procesar_hora($fila->hora_fin);
                    $nuevo->vehiculo    = importar_tabla::procesar_texto($fila->vehiculo);
                    $nuevo->hr_salida   = importar_tabla::procesar_hora($fila->hora_salida);
                    $nuevo->lugar       = importar_tabla::procesar_texto($fila->lugar);
                    $nuevo->id_accion   = importar_tabla::determinar_cat_item($fila->accion,55);
                    $nuevo->tipo        = importar_tabla::procesar_texto($fila->tipo);
                    $hoja_vida=self::determinar_atencion_inicial($fila);
                    $nuevo->id_atencion_inicial = $hoja_vida->id_atencion_inicial;
                    $nuevo->id_quien_agenda  = importar_tabla::determinar_empleada($fila->quien_agenda);
                    $nuevo->resultado_hoja_evolucion = importar_tabla::procesar_texto($fila->resultado);
                    // Campos nuevos
                    $nuevo->a_nivel    = importar_tabla::determinar_cat_item($fila->a_nivel,66);
                    $nuevo->instrumentos = importar_tabla::procesar_texto($fila->instrumentos);
                    //Grabar
                    $nuevo->save();

                    //Traza de la importacion
                    importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_agenda' => $nuevo->id_agenda]);

                    //Actualizar la fila importada
                    $fila->importado=1;

                }

                $fila->save();
                $log.=" (Fila temporal actualizada).";
            }
            $log.="<br>Fin del bloque<br>";

            //Actualizar el log de errores
            if(strlen($log_error) > 0) {
                $cualquiera=self::first();
                $encabezado=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $encabezado->log_procesa.=$log_error;
                $encabezado->save();
            }


            //Determinar si faltan mas
            $conteo=self::where('importado',0)->count();


            if($conteo==0) {  //Llegamos al final, hay que actualizar encabezados
                //Fin de la importación
                $log.="<br><h3>Fin de la importacion</h3>";
                //Actualizar control de versiones y bitacora
                $cualquiera=self::first();
                $bitacora=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $bitacora->id_estado=3;
                $bitacora->save();
            }
            $resultado = true;
        }

        //Conteos
        $cierre = new \stdClass();
        $cierre->filas_total    =self::count();
        $cierre->filas_exito    =self::where('importado',1)->count();
        $cierre->filas_fallo    =self::where('importado',99)->count();
        $cierre->filas_pendiente  =self::where('importado',0)->count();
        $cierre->log=$log;
        $cierre->log_error=$log_error;
        $cierre->resultado=$resultado;
        return $cierre;
    }
}
