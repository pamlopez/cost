<?php

namespace App\Importar;

use Illuminate\Database\Eloquent\Model;
use App\Models\atencion_inicial;
use App\Models\persona;
use Carbon\Carbon;
use App\Importar\importar_tabla;
use App\Importar\importar_traza;
use App\Importar\importar_bitacora;
use App\Models\atencion_inicial_tipo_violencia;
use App\Models\atencion_inicial_referencia_caimu;



class imp_ai extends Model
{
    //
    protected $table = 'imp_ai';
    protected $primaryKey = "id_imp_ai";
    public $timestamps = FALSE;
    protected $guarded=['id_imp_ai'];


    public static function conteos() {
        $respuesta = new \stdClass();
        $respuesta->filas_total=self::count();;
        $respuesta->filas_exito=self::where('importado',1)->count();
        $respuesta->filas_fallo=self::where('importado',99)->count();
        $respuesta->filas_pendiente=self::where('importado',0)->count();
        if($respuesta->filas_total > 0) {
            $respuesta->avance =  ($respuesta->filas_total - $respuesta->filas_pendiente) / $respuesta->filas_total;
            $respuesta->avance = intval($respuesta->avance * 100);
        }
        else {
            $respuesta->avance=0;
        }

        return $respuesta;
    }




    //////////
    /*
     * Procesa imp_ai para importar atencion_inicial y persona
     */
    public static function importar($cantidad=10) {
        $listado=self::where('importado',0)->limit($cantidad)->get();
        $conteo=$listado->count();

        if($conteo == 0) {
            //Fin de la importación
            $log="<br>Nada que importar";
            $resultado=false;
        }
        else {

            $log="<br>Importando bloque de $conteo filas en Atencion Inicial";
            $log_error="";

            foreach($listado as $fila) {
                $log.="<br>Fila $fila->id_imp_ai, correlativo $fila->numero ";
                //No aceptar sin número de hoja de vida
                if(strlen($fila->h_v)<1) {
                    $error="Fila $fila->id_imp_ai  ignorada, sin numero de hoja de vida<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }
                //Detectar duplicados
                else {
                    $existe = atencion_inicial::where('numero_historia_vida', $fila->h_v)->count();
                    if ($existe > 0) {
                        $error = "<br>Fila $fila->id_imp_ai  ignorada, numero de hoja de vida duplicado:$fila->h_v <br>";
                        $log .= $error;
                        $fila->importado = 99;
                        $log_error .= $error;
                    }
                    else {
                        $nuevo = new atencion_inicial();
                        $nuevo->numero_historia_vida = $fila->h_v;
                        $nuevo->fecha = importar_tabla::procesar_fecha($fila->fecha, "d/m/Y");
                        $nuevo->edad = importar_tabla::procesar_numero($fila->edad);
                        $nuevo->id_rango_caimu = atencion_inicial::calcula_rango($nuevo->edad, "rango_caimu");
                        $nuevo->id_rango_mingob = atencion_inicial::calcula_rango($nuevo->edad, "rango_mingob");
                        $nuevo->id_rango_ine = atencion_inicial::calcula_rango($nuevo->edad, "rango_ine");
                        $nuevo->id_nacionalidad = importar_tabla::determinar_cat_item($fila->nacionalidad, 11);
                        $nuevo->telefono = $fila->telefonos;
                        $nuevo->id_area_residencial = importar_tabla::determinar_cat_item($fila->area_residencia, 12);
                        $nuevo->id_estado_civil = importar_tabla::determinar_cat_item($fila->estado_civil, 13);
                        $nuevo->id_relacion_actual = importar_tabla::determinar_cat_item($fila->relacion_actual, 14);
                        $nuevo->id_ocupacion = importar_tabla::determinar_cat_item($fila->ocupacion, 15);
                        $nuevo->id_escolaridad = importar_tabla::determinar_cat_item($fila->escolaridad, 16);
                        $nuevo->nombre_agresor = importar_tabla::procesar_texto($fila->agresor_nombre);
                        $nuevo->id_ocupacion_agresor = importar_tabla::determinar_cat_item($fila->agresor_ocupacion, 15);
                        $nuevo->lugar_trabajo_agresor = importar_tabla::procesar_texto($fila->agresor_lugar_trabajo);
                        $nuevo->ingreso_mensual_agresor = importar_tabla::procesar_texto($fila->agresor_ingresos_mensuales);
                        $nuevo->cant_hija_mayor = importar_tabla::procesar_numero($fila->hijas_mayores);
                        $nuevo->cant_hija_menor = importar_tabla::procesar_numero($fila->hijas_menores);
                        $nuevo->cant_hijo_mayor = importar_tabla::procesar_numero($fila->hijos_mayores);
                        $nuevo->cant_hijo_menor = importar_tabla::procesar_numero($fila->hijos_menores);
                        $nuevo->motivo_visita_sra = $fila->motivo_visita;
                        $nuevo->id_motivo_visita_familia = $fila->motivo_visita_familia;
                        $nuevo->id_motivo_visita_penal = importar_tabla::determinar_cat_item($fila->motivo_visita_penal, 18);
                        $nuevo->id_motivo_visita_psicologica = importar_tabla::determinar_cat_item($fila->motivo_visita_psicologia, 19);
                        $nuevo->id_motivo_visita_trabajadora_social = importar_tabla::determinar_cat_item($fila->motivo_visita_ts, 20);
                        $nuevo->necesita_albergue = importar_tabla::procesar_si_no($fila->necesita_albergue);
                        $nuevo->id_quien_agrede = importar_tabla::determinar_cat_item($fila->quien_agrede, 21);
                        $nuevo->id_quien_refiere_a_caimu = importar_tabla::determinar_cat_item($fila->referida_caimu, 22);
                        $nuevo->referencia_exacta = importar_tabla::determinar_cat_item($fila->referida_externa, 22);
                        $nuevo->id_quien_entrevista = importar_tabla::determinar_empleada($fila->entrevistador, 22);
                        $nuevo->quien_apoya = importar_tabla::procesar_texto($fila->nombre_apoya);
                        //Hard code
                        $nuevo->id_caimu = 177;
                        //Determinar persona
                        $creacion = self::crear_persona($fila);
                        if ($creacion['resultado']) {
                            $nuevo->id_persona = $creacion['objeto']->id_persona;
                            $log .= " Creada persona " . $creacion['objeto']->id_persona;
                            importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_persona' => $creacion['objeto']->id_persona]);
                        } else {
                            $nuevo->id_persona = null;
                            $error = "<Br>Fila $fila->id_imp_ai.  No se pudo crear la persona: " . $creacion['mensaje'] . ". ";
                            $log .= $error;
                            $log_error .= $error;
                        }
                        //Grabar
                        $nuevo->save();
                        $log .= " Historia creada. ";
                        if ($nuevo->id_atencion_inicial > 0) {
                            self::determinar_violencia($fila, $nuevo->id_atencion_inicial);
                            $log .= " tipologia determinada.";
                            self::determinar_areas($fila, $nuevo->id_atencion_inicial);
                            $log .= " areas caimus determinadas.";
                        }


                        //Traza de la A.I.
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_atencion_inicial' => $nuevo->id_atencion_inicial]);

                        //Actualizar la fila importada
                        $fila->importado=1;

                    }
                }



                $fila->save();
                $log.=" (Fila temporal actualizada).";
            }
            $log.="<br>Fin del bloque<br>";

            //Actualizar el log de errores
            if(strlen($log_error) > 0) {
                $cualquiera=self::first();
                $encabezado=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $encabezado->log_procesa.=$log_error;
                $encabezado->save();
            }


            //Determinar si faltan mas
            $conteo=self::where('importado',0)->count();


            if($conteo==0) {  //Llegamos al final, hay que actualizar encabezados
                //Fin de la importación
                $log.="<br><h3>Fin de la importacion</h3>";
                //Actualizar control de versiones y bitacora
                $cualquiera=self::first();
                $bitacora=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $bitacora->id_estado=3;
                $bitacora->save();
            }
            $resultado = true;
        }

        //Conteos
        $cierre = new \stdClass();
        $cierre->filas_total    =self::count();
        $cierre->filas_exito    =self::where('importado',1)->count();
        $cierre->filas_fallo    =self::where('importado',99)->count();
        $cierre->filas_pendiente  =self::where('importado',0)->count();
        $cierre->log=$log;
        $cierre->log_error=$log_error;
        $cierre->resultado=$resultado;
        return $cierre;
    }

    // Recibe los datos de una fila de imp_am y crea el caso
    public static function crear_persona($fila_datos) {
        //Limpiar de guiones
        $fila_datos->nombre_apellido = importar_tabla::procesar_texto($fila_datos->nombre_apellido);
        if(strlen($fila_datos->nombre_apellido)<1) {
            return array("resultado"=>false, "objeto"=>null, "mensaje"=>"Nombre en blanco");
        }
        else {
            $existe = persona::where('nombre_completo','like',$fila_datos->nombre_apellido)->first();
            if(isset($existe->id_persona)) {
                return array("resultado"=>true, "objeto"=>$existe);

            }
            else {
                $persona=new persona();
                $persona->nombre_completo=$fila_datos->nombre_apellido;
                $persona->no_identificacion=importar_tabla::procesar_dpi($fila_datos->dpi);


                if(strlen($persona->no_identificacion)==13) {
                    $persona->id_tipo_documento_identificacion=1;
                }
                elseif($persona->no_identificacion=="N/A" || $persona->no_identificacion=="N/I" || $persona->no_identificacion=="N/I") {
                    $persona->id_tipo_documento_identificacion = null;
                }
                else {
                    $persona->id_tipo_documento_identificacion = importar_tabla::determinar_cat_item("Cédula",1);
                }

                if(strlen($fila_datos->m)>0){
                    $persona->genero=1;
                }
                else {
                    $persona->genero=2;
                }
                $muni=importar_tabla::procesar_municipio($fila_datos->municipio,$fila_datos->depto);
                if($muni<>null) {
                    $persona->id_depto_domicilio=$muni->id_padre;
                    $persona->id_muni_domicilio=$muni->id_geo;
                }
                $persona->zona_domicilio=$fila_datos->zona;
                $persona->direccion_domicilio=$fila_datos->direccion;

                $persona->id_etnia = importar_tabla::determinar_cat_item($fila_datos->etnia,6);


                try {
                    $persona->save();
                    return array("resultado"=>true, "objeto"=>$persona);
                }
                catch (\Exception $e) {
                    return array("resultado"=>false, "objeto"=>null, "mensaje"=>$e->getMessage());
                }
            }
        }

    }

    // Determina tipos de violecia segun las columnas marcadas
    public static function determinar_violencia($fila_datos, $id_atencion_inicial) {
        $registro['id_atencion_inicial']=$id_atencion_inicial;
        if(strlen($fila_datos->fisica) > 0) {
            $registro['id_tipo_violencia']=74;
            atencion_inicial_tipo_violencia::create($registro);
        }

        if(strlen($fila_datos->psicologica) > 0) {
            $registro['id_tipo_violencia']=75;
            atencion_inicial_tipo_violencia::create($registro);
        }

        if(strlen($fila_datos->verbal) > 0) {
            $registro['id_tipo_violencia']=76;
            atencion_inicial_tipo_violencia::create($registro);
        }

        if(strlen($fila_datos->sexual) > 0) {
            $registro['id_tipo_violencia']=77;
            atencion_inicial_tipo_violencia::create($registro);
        }
        if(strlen($fila_datos->patrimonial) > 0) {
            $registro['id_tipo_violencia']=78;
            atencion_inicial_tipo_violencia::create($registro);
        }

        if(strlen($fila_datos->economica) > 0) {
            $registro['id_tipo_violencia']=79;
            atencion_inicial_tipo_violencia::create($registro);
        }

        if(strlen($fila_datos->trata) > 0) {
            $registro['id_tipo_violencia']=80;
            atencion_inicial_tipo_violencia::create($registro);
        }

        if(strlen($fila_datos->violacion) > 0) {
            $registro['id_tipo_violencia']=81;
            atencion_inicial_tipo_violencia::create($registro);
        }
        if(strlen($fila_datos->amenazas_muerte) > 0) {
            $registro['id_tipo_violencia']=82;
            atencion_inicial_tipo_violencia::create($registro);
        }

        if(strlen($fila_datos->misoginia) > 0) {
            $registro['id_tipo_violencia']=83;
            atencion_inicial_tipo_violencia::create($registro);
        }

        if(strlen($fila_datos->femicidio) > 0) {
            $registro['id_tipo_violencia']=84;
            atencion_inicial_tipo_violencia::create($registro);
        }
        if(strlen($fila_datos->no_aplica) > 0) {  //Cambio el 1-Ago-18
            $registro['id_tipo_violencia']=7118;
            atencion_inicial_tipo_violencia::create($registro);
        }
    }

    // Determina las areas de caimu
    public static function determinar_areas($fila_datos, $id_atencion_inicial) {
        $registro['id_atencion_inicial']=$id_atencion_inicial;

        $fila_datos->referida_caimu=mb_strtolower($fila_datos->referida_caimu);

        if(strpos($fila_datos->referida_caimu,"egal") > 0) {
            $registro['id_area_caimu']=153;
            atencion_inicial_referencia_caimu::create($registro);
        }

        if(strpos($fila_datos->referida_caimu,"psico") > 0) {
            $registro['id_area_caimu']=154;
            atencion_inicial_referencia_caimu::create($registro);
        }

        if(strpos($fila_datos->referida_caimu,"lbergue") > 0) {
            $registro['id_area_caimu']=155;
            atencion_inicial_referencia_caimu::create($registro);
        }
        if(strpos($fila_datos->referida_caimu,"ocial") > 0) {
            $registro['id_area_caimu']=156;
            atencion_inicial_referencia_caimu::create($registro);
        }
        if(strpos($fila_datos->referida_caimu,"medica") > 0) {
            $registro['id_area_caimu']=157;
            atencion_inicial_referencia_caimu::create($registro);
        }
        if(strpos($fila_datos->referida_caimu,"médica") > 0) {
            $registro['id_area_caimu']=157;
            atencion_inicial_referencia_caimu::create($registro);
        }
        if(strpos($fila_datos->referida_caimu,"poyo") > 0) {
            $registro['id_area_caimu']=158;
            atencion_inicial_referencia_caimu::create($registro);
        }
        if(strpos($fila_datos->referida_caimu,"plica") > 0) {  //No aplica
            $registro['id_area_caimu']=159;
            atencion_inicial_referencia_caimu::create($registro);
        }
    }

}
