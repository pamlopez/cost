<?php

namespace App\Importar;

use Illuminate\Database\Eloquent\Model;

class importar_traza extends Model
{
    //
    //Detalle de registros importados en cada importación
    protected $table = 'importar_traza';
    protected $primaryKey = "id_importar_traza";
    protected $guarded = ['id_importar_traza' ];
    public $timestamps = FALSE;
    protected $dates = [
        'control_i',
        'control_u'
    ];



    public static function listadoAtencionInicial($id) {
        return \App\Models\atencion_inicial::join('importar_traza','importar_traza.id_atencion_inicial','=','atencion_inicial.id_atencion_inicial')
            ->where('id_importar_bitacora','=',$id)
            ->orderby('importar_traza.id_importar_traza')
            ->paginate();

    }

    public static function listadoPersona($id) {
        return \App\Models\persona::join('importar_traza','importar_traza.id_persona','=','persona.id_persona')
            ->where('id_importar_bitacora','=',$id)
            ->orderby('importar_traza.id_importar_traza')
            ->paginate();
    }


}
