<?php

namespace App\Importar;

use App\Models\proceso;
use App\Models\atencion_inicial;
use App\Models\persona;
use App\Models\proceso_detalle_demanda_familia;
use Carbon\Carbon;
use App\Importar\importar_tabla;
use App\Importar\importar_traza;
use App\Importar\importar_bitacora;
use App\Models\atencion_inicial_tipo_violencia;
use App\Models\atencion_inicial_referencia_caimu;


use Illuminate\Database\Eloquent\Model;

// php artisan krlov:generate:model Importar/imp_familia --table-name=imp_familia

/**
 * @property int $id_imp_familia
 * @property int $id_importar_bitacora
 * @property string $anio
 * @property string $hoja_vida
 * @property string $nombre_completo
 * @property string $tipo_proceso
 * @property string $juzgado
 * @property string $anio_inicio
 * @property string $numero_proceso
 * @property string $oficial
 * @property string $status
 * @property string $motivo
 * @property string $anio_fin
 * @property string $observaciones
 * @property string $quien_ordena
 * @property string $fecha_diligencia_previa
 * @property string $diligencia_previa
 * @property string $fecha_presentacion
 * @property string $accion_legal
 * @property string $procuracion
 * @property string $fecha_resolucion
 * @property string $previos_rechazo
 * @property string $fecha_accion
 * @property string $subsanacion_previos
 * @property string $fecha_procuracion
 * @property string $procuracion2
 * @property string $fecha_1_resolucion
 * @property string $contenido_resolucion
 * @property string $fecha_audiencia
 * @property string $hora
 * @property string $tipo_audiencia
 * @property string $fecha_notificacion_demandado
 * @property string $fecha_audiencia_a_cabo
 * @property string $resultado_audiencia
 * @property string $a1_fecha
 * @property string $a1_accion
 * @property string $a1_fecha_resolucion
 * @property string $a1_contenido_resolucion
 * @property string $a1_fecha_accion
 * @property string $a1_tipo_accion
 * @property string $a1_fecha2
 * @property string $a1_acciones_demandado
 * @property string $a1_fecha_resolucion2
 * @property string $a1_contenido_resolucion2
 * @property string $a1_fecha_audiencia_senalada
 * @property string $a1_hora
 * @property string $a1_tipo_audiencia
 * @property string $a1_fecha_audiencia_realizada
 * @property string $a1_resultado_audiencia
 * @property string $a1_fecha_procuracion
 * @property string $a1_otros
 * @property string $a2_fecha_accion
 * @property string $a2_accion
 * @property string $a2_fecha_procuracion
 * @property string $a2_contenido
 * @property string $a2_fecha_resolucion
 * @property string $a2_contenido_resolucion
 * @property string $a2_fecha_audiencia_senalada
 * @property string $a2_hora
 * @property string $a2_tipo_audiencia
 * @property string $a2_fecha_audiencia_realizada
 * @property string $a2_resultado_audiencia
 * @property string $a2_fecha_procuracion2
 * @property string $a2_contenido_otros
 * @property string $a2_fecha_accion2
 * @property string $a2_accion_legal
 * @property string $a2_fecha_procuracion3
 * @property string $a2_contenido_otros2
 * @property string $a2_fecha_resolucion2
 * @property string $a2_contenido_resolucion2
 * @property string $ignorar
 * @property string $sentencia_fecha
 * @property string $sentencia
 * @property string $sentencia_seguimiento
 * @property string $aclaracion_fecha
 * @property string $aclaracion
 * @property string $recurso_fecha
 * @property string $recurso_interpuesto
 * @property string $notificacion_fecha
 * @property string $notificacion
 * @property string $accion_fecha
 * @property string $accion
 * @property string $procuracion_fecha
 * @property string $procuracion3
 * @property string $resolucion_fecha
 * @property string $resolucion_contenido
 * @property string $sentencia_2_instancia_fecha
 * @property string $sentencia_2_instancia
 * @property string $seguimiento
 * @property int $importado
 * @property string $control
 */
class imp_familia extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'imp_familia';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_imp_familia';
    public $timestamps = FALSE;

    /**
     * @var array
     */
    protected $fillable = ['id_importar_bitacora', 'anio', 'hoja_vida', 'nombre_completo', 'tipo_proceso', 'juzgado', 'anio_inicio', 'numero_proceso', 'oficial', 'status', 'motivo', 'anio_fin', 'observaciones', 'quien_ordena', 'fecha_diligencia_previa', 'diligencia_previa', 'fecha_presentacion', 'accion_legal', 'procuracion', 'fecha_resolucion', 'previos_rechazo', 'fecha_accion', 'subsanacion_previos', 'fecha_procuracion', 'procuracion2', 'fecha_1_resolucion', 'contenido_resolucion', 'fecha_audiencia', 'hora', 'tipo_audiencia', 'fecha_notificacion_demandado', 'fecha_audiencia_a_cabo', 'resultado_audiencia', 'a1_fecha', 'a1_accion', 'a1_fecha_resolucion', 'a1_contenido_resolucion', 'a1_fecha_accion', 'a1_tipo_accion', 'a1_fecha2', 'a1_acciones_demandado', 'a1_fecha_resolucion2', 'a1_contenido_resolucion2', 'a1_fecha_audiencia_senalada', 'a1_hora', 'a1_tipo_audiencia', 'a1_fecha_audiencia_realizada', 'a1_resultado_audiencia', 'a1_fecha_procuracion', 'a1_otros', 'a2_fecha_accion', 'a2_accion', 'a2_fecha_procuracion', 'a2_contenido', 'a2_fecha_resolucion', 'a2_contenido_resolucion', 'a2_fecha_audiencia_senalada', 'a2_hora', 'a2_tipo_audiencia', 'a2_fecha_audiencia_realizada', 'a2_resultado_audiencia', 'a2_fecha_procuracion2', 'a2_contenido_otros', 'a2_fecha_accion2', 'a2_accion_legal', 'a2_fecha_procuracion3', 'a2_contenido_otros2', 'a2_fecha_resolucion2', 'a2_contenido_resolucion2', 'ignorar', 'sentencia_fecha', 'sentencia', 'sentencia_seguimiento', 'aclaracion_fecha', 'aclaracion', 'recurso_fecha', 'recurso_interpuesto', 'notificacion_fecha', 'notificacion', 'accion_fecha', 'accion', 'procuracion_fecha', 'procuracion3', 'resolucion_fecha', 'resolucion_contenido', 'sentencia_2_instancia_fecha', 'sentencia_2_instancia', 'seguimiento', 'importado', 'control'];

    public static function conteos() {
        $respuesta = new \stdClass();
        $respuesta->filas_total=self::count();;
        $respuesta->filas_exito=self::where('importado',1)->count();
        $respuesta->filas_fallo=self::where('importado',99)->count();
        $respuesta->filas_pendiente=self::where('importado',0)->count();
        if($respuesta->filas_total > 0) {
            $respuesta->avance =  ($respuesta->filas_total - $respuesta->filas_pendiente) / $respuesta->filas_total;
            $respuesta->avance = intval($respuesta->avance * 100);
        }
        else {
            $respuesta->avance=0;
        }

        return $respuesta;
    }


    //////////
    /*
     * Procesa imp_familia para importar datos en proceso
     */
    public static function importar($cantidad=10) {
        $listado=self::where('importado',0)->orderby('id_imp_familia')->limit($cantidad)->get();
        $conteo=$listado->count();

        if($conteo == 0) {
            //Fin de la importación
            $log="<br>Nada que importar";
            $resultado=false;
        }
        else {

            $log="<br>Importando bloque de $conteo filas en 9.2 Familia";
            $log_error="";

            foreach($listado as $fila) {
                $log.="<br>Fila $fila->id_imp_familia, HV: $fila->hoja_vida ";
                //No aceptar sin número de hoja de vida
                if(strlen($fila->hoja_vida)<1) {
                    $error="Fila $fila->id_imp_familia  ignorada, sin numero de hoja de vida.  $fila->nombre_completo<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }

                else {
                    //Al final, actualizar la fila importada
                    $fila->importado=1;


                    //Primer bloque: Atención inicial
                    $hoja_vida = atencion_inicial::where('numero_historia_vida', $fila->hoja_vida)->first();
                    if ($hoja_vida) { //Ya está, no hacer nada
                        $log.="<br>H.V.  $fila->hoja_vida existente";
                    }
                    else {  //No existe, crear una nueva
                        $hoja_vida = new atencion_inicial();
                        $hoja_vida->numero_historia_vida = $fila->hoja_vida;
                        //$hoja_vida->fecha = importar_tabla::procesar_fecha($fila->f_atencion_inicial, "Y-m-d");
                        $hoja_vida->edad = null;
                        $hoja_vida->id_rango_caimu = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_caimu");
                        $hoja_vida->id_rango_mingob = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_mingob");
                        $hoja_vida->id_rango_ine = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_ine");

                        //Hard code
                        $hoja_vida->id_caimu = 177;
                        //Determinar persona
                        $creacion = self::crear_persona($fila);
                        if ($creacion['resultado']) {
                            $hoja_vida->id_persona = $creacion['objeto']->id_persona;
                            $log .= " Creada persona " . $creacion['objeto']->id_persona;
                            importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_persona' => $creacion['objeto']->id_persona]);
                        } else {
                            $hoja_vida->id_persona = null;
                            $error = "<Br>Fila $fila->id_imp_ai.  No se pudo crear la persona: " . $creacion['mensaje'] . ". ";
                            $log .= $error;
                            $log_error .= $error;
                        }
                        //Grabar
                        $hoja_vida->save();
                        $log .= " Atención inicial creada. ";
                        //Traza de la A.I.
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_atencion_inicial' => $hoja_vida->id_atencion_inicial]);

                    }
                    //Fin del  bloque

                    $log.="<br> Proceso. ";

                    //Bloque de proceso
                    $proceso = new proceso();
                    $proceso->id_atencion_inicial = $hoja_vida->id_atencion_inicial;
                    $proceso->id_tipo_demanda = 2;  //Familiar
                    $proceso->id_tipo_proceso   = importar_tabla::determinar_cat_item($fila->tipo_proceso,31);
                    $proceso->id_juzgado        = importar_tabla::determinar_cat_item($fila->juzgado,37);
                    $proceso->anio_inicio_demanda = importar_tabla::procesar_numero($fila->anio_inicio);
                    $proceso->no_proceso        = importar_tabla::procesar_texto($fila->numero_proceso);
                    $proceso->oficial           = importar_tabla::procesar_texto($fila->oficial);
                    $proceso->id_estado           = importar_tabla::determinar_cat_item($fila->status,29);
                    $proceso->id_calidad         = importar_tabla::determinar_cat_item($fila->calidad,34);
                    $proceso->id_motivo         = importar_tabla::determinar_cat_item($fila->motivo,36);
                    $proceso->anio_terminacion  = importar_tabla::procesar_numero($fila->anio_fin);
                    $proceso->observaciones     = importar_tabla::procesar_texto($fila->observaciones);
                    $proceso->id_quien_ordena   = importar_tabla::determinar_cat_item($fila->quien_ordena,21);
                    $proceso->save();
                    importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso' => $proceso->id_proceso]);
                    $log.=" Proceso creado. ";

                    // Primer detalle
                    $detalle = new proceso_detalle_demanda_familia();
                    $detalle->id_proceso            = $proceso->id_proceso;
                    $detalle->fh_diligencia_previa  = importar_tabla::procesar_fecha_auto($fila->fecha_diligencia_previa);
                    $detalle->desc_diligencia_previa= importar_tabla::procesar_texto($fila->diligencia_previa);
                    $detalle->fh_presentacion_demanda=importar_tabla::procesar_fecha_auto($fila->fecha_presentacion);
                    $detalle->accion_legal          = importar_tabla::procesar_texto($fila->accion_legal);
                    $detalle->procuracion           = importar_tabla::procesar_texto($fila->procuracion);
                    $detalle->fh_resolucion         = importar_tabla::procesar_fecha_auto($fila->fecha_resolucion);
                    $detalle->previo_rechazo        = importar_tabla::procesar_texto($fila->previos_rechazo);
                    $detalle->fh_accion             = importar_tabla::procesar_fecha_auto($fila->fecha_accion);
                    $detalle->subsanacion_previos   = importar_tabla::procesar_texto($fila->subsanacion_previos);
                    $detalle->fh_procuracion        = importar_tabla::procesar_fecha_auto($fila->procuracion);
                    $detalle->procuracion           = importar_tabla::procesar_texto($fila->procuracion2);
                    $detalle->fh_primera_resolucion = importar_tabla::procesar_fecha_auto($fila->fecha_1_resolucion);
                    $detalle->contenido_resolucion  = importar_tabla::procesar_texto($fila->contenido_resolucion);
                    $detalle->fh_audiencia_senialada = importar_tabla::procesar_fecha_auto($fila->fecha_audiencia);
                    $detalle->hora_audiencia_senialada = importar_tabla::procesar_texto($fila->hora);
                    $detalle->id_tipo_audiencia     = importar_tabla::determinar_cat_item($fila->tipo_audiencia,39);
                    $detalle->fh_notificacion_demandado = importar_tabla::procesar_fecha_auto($fila->fecha_notificacion_demandado);
                    $detalle->fh_audiencia_que_llevo_acabo = importar_tabla::procesar_fecha_auto($fila->fecha_audiencia_a_cabo);
                    $detalle->resultado_audiencia  = importar_tabla::procesar_texto($fila->resultado_audiencia);
                    $detalle->save();
                    importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                    $log.=" Detalle creado. ";

                    //Iteracion 1
                    if(strlen(trim($fila->a1_fecha))>4 || strlen(trim($fila->a1_accion))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso            = $proceso->id_proceso;
                        $detalle->fh_accion             = importar_tabla::procesar_fecha_auto($fila->a1_fecha);
                        $detalle->accion_legal          = importar_tabla::procesar_texto($fila->a1_accion);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 1. ";
                    }

                    //Iteracion 2
                    if(strlen(trim($fila->a1_fecha_resolucion))>4 || strlen(trim($fila->a1_contenido_resolucion))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso            = $proceso->id_proceso;
                        $detalle->fh_resolucion          = importar_tabla::procesar_fecha_auto($fila->a1_fecha_resolucion);
                        $detalle->contenido_resolucion   = importar_tabla::procesar_texto($fila->a1_contenido_resolucion);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 2. ";
                    }

                    //Iteracion 3
                    if(strlen(trim($fila->a1_fecha_accion))>4 || strlen(trim($fila->a1_tipo_accion))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_accion      = importar_tabla::procesar_fecha_auto($fila->a1_fecha_accion);
                        $detalle->accion_legal   = importar_tabla::procesar_texto($fila->a1_tipo_accion);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 3. ";
                    }

                    //Iteracion 4
                    if(strlen(trim($fila->a1_fecha2))>4 || strlen(trim($fila->a1_acciones_demandado))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_accion      = importar_tabla::procesar_fecha_auto($fila->a1_fecha2);
                        $detalle->accion_legal   = importar_tabla::procesar_texto($fila->a1_acciones_demandado);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 4. ";
                    }

                    //Iteracion 5
                    if(strlen(trim($fila->a1_fecha_resolucion2))>4 || strlen(trim($fila->a1_contenido_resolucion2))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_resolucion      = importar_tabla::procesar_fecha_auto($fila->a1_fecha_resolucion2);
                        $detalle->accion_legal   = importar_tabla::procesar_texto($fila->a1_contenido_resolucion2);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 5. ";
                    }

                    //Iteracion 6
                    if(strlen(trim($fila->a1_fecha_audiencia_senalada))>4 || strlen(trim($fila->a1_tipo_audiencia))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_audiencia_senialada      = importar_tabla::procesar_fecha_auto($fila->a1_fecha_audiencia_senalada);
                        $detalle->hora_audiencia_senialada = importar_tabla::procesar_texto($fila->hora);
                        $detalle->id_tipo_audiencia   = importar_tabla::determinar_cat_item($fila->a1_tipo_audiencia,39);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 6. ";
                    }

                    //Iteracion 7
                    if(strlen(trim($fila->a1_fecha_audiencia_realizada))>4 || strlen(trim($fila->a1_resultado_audiencia))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_audiencia_senialada      = importar_tabla::procesar_fecha_auto($fila->a1_fecha_audiencia_realizada);
                        $detalle->resultado_audiencia   = importar_tabla::procesar_texto($fila->a1_resultado_audiencia);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 7. ";
                    }
                    //Iteracion 8
                    if(strlen(trim($fila->a1_fecha_procuracion))>4 || strlen(trim($fila->a1_otros))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_procuracion      = importar_tabla::procesar_fecha_auto($fila->a1_fecha_procuracion);
                        $detalle->procuracion   = importar_tabla::procesar_texto($fila->a1_otros);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 8. ";
                    }

                    //Iteracion 10
                    if(strlen(trim($fila->a2_fecha_procuracion))>4 || strlen(trim($fila->a2_contenido))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_procuracion = importar_tabla::procesar_fecha_auto($fila->a2_fecha_procuracion);
                        $detalle->procuracion    = importar_tabla::procesar_texto($fila->a2_contenido);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 10. ";
                    }

                    //Iteracion 11
                    if(strlen(trim($fila->a2_fecha_resolucion))>4 || strlen(trim($fila->a2_contenido_resolucion))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_resolucion = importar_tabla::procesar_fecha_auto($fila->a2_fecha_resolucion);
                        $detalle->contenido_resolucion    = importar_tabla::procesar_texto($fila->a2_contenido_resolucion);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 11. ";
                    }

                    //Iteracion 12
                    if(strlen(trim($fila->a2_fecha_audiencia_senalada))>4 || strlen(trim($fila->a2_tipo_audiencia))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_audiencia_senialada      = importar_tabla::procesar_fecha_auto($fila->a2_fecha_audiencia_senalada);

                        $detalle->hora_audiencia_senialada = importar_tabla::procesar_texto($fila->a2_hora);
                        $detalle->id_tipo_audiencia   = importar_tabla::determinar_cat_item($fila->a2_tipo_audiencia,39);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 12. ";
                    }

                    //Iteracion 13
                    if(strlen(trim($fila->a2_fecha_audiencia_realizada))>4 || strlen(trim($fila->a2_resultado_audiencia))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_audiencia_que_llevo_acabo      = importar_tabla::procesar_fecha_auto($fila->a2_fecha_audiencia_realizada);

                        $detalle->resultado_audiencia   = importar_tabla::procesar_texto($fila->a2_resultado_audiencia);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 13. ";
                    }

                    //Iteracion 14
                    if(strlen(trim($fila->a2_fecha_procuracion2))>4 || strlen(trim($fila->a2_contenido_otros))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_procuracion = importar_tabla::procesar_fecha_auto($fila->a2_fecha_procuracion2);
                        $detalle->procuracion    = importar_tabla::procesar_texto($fila->a2_contenido_otros);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 14. ";
                    }

                    //Iteracion 15
                    if(strlen(trim($fila->a2_fecha_accion2))>4 || strlen(trim($fila->a2_accion_legal))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_accion      = importar_tabla::procesar_fecha_auto($fila->a2_fecha_accion2);
                        $detalle->accion_legal   = importar_tabla::procesar_texto($fila->a2_accion_legal);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 15. ";
                    }

                    //Iteracion 16
                    if(strlen(trim($fila->a2_fecha_procuracion3))>4 || strlen(trim($fila->a2_contenido_otros2))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_procuracion = importar_tabla::procesar_fecha_auto($fila->a2_fecha_procuracion3);
                        $detalle->procuracion    = importar_tabla::procesar_texto($fila->a2_contenido_otros2);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 16. ";
                    }

                    //Iteracion 17
                    if(strlen(trim($fila->a2_fecha_resolucion2))>4 || strlen(trim($fila->a2_contenido_resolucion2))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_resolucion = importar_tabla::procesar_fecha_auto($fila->a2_fecha_resolucion2);
                        $detalle->contenido_resolucion    = importar_tabla::procesar_texto($fila->a2_contenido_resolucion2);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 17. ";
                    }

                    //Iteracion 18 y 19
                    if(strlen(trim($fila->sentencia_fecha))>4 || strlen(trim($fila->sentencia))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_sentencia = importar_tabla::procesar_fecha_auto($fila->sentencia_fecha);
                        $detalle->sentencia    = importar_tabla::procesar_texto($fila->sentencia);
                        $detalle->seguimiento    = importar_tabla::procesar_texto($fila->sentencia_seguimiento);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 18 y 19. ";
                    }

                    //Iteracion 20
                    if(strlen(trim($fila->aclaracion_fecha))>4 || strlen(trim($fila->aclaracion))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_aclaracion_ampliacion = importar_tabla::procesar_fecha_auto($fila->aclaracion_fecha);
                        $detalle->aclaracion_ampliacion    = importar_tabla::procesar_texto($fila->aclaracion);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 20. ";
                    }

                    //Iteracion 21
                    if(strlen(trim($fila->recurso_fecha))>4 || strlen(trim($fila->recurso_interpuesto))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_recurso_interpuesto = importar_tabla::procesar_fecha_auto($fila->recurso_fecha);
                        $detalle->recurso_interpuesto    = importar_tabla::procesar_texto($fila->recurso_interpuesto);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 21. ";
                    }

                    //Iteracion 22
                    if(strlen(trim($fila->notificacion_fecha))>4 || strlen(trim($fila->notificacion))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_notificacion = importar_tabla::procesar_fecha_auto($fila->notificacion_fecha);
                        $detalle->notificacion    = importar_tabla::procesar_texto($fila->notificacion);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 22. ";

                    }

                    //Iteracion 23
                    if(strlen(trim($fila->accion_fecha))>4 || strlen(trim($fila->accion))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_accion      = importar_tabla::procesar_fecha_auto($fila->accion_fecha);
                        $detalle->accion_legal   = importar_tabla::procesar_texto($fila->accion);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 23. ";
                    }

                    //Iteracion 24
                    if(strlen(trim($fila->procuracion_fecha))>4 || strlen(trim($fila->procuracion3))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_procuracion      = importar_tabla::procesar_fecha_auto($fila->procuracion_fecha);
                        $detalle->procuracion   = importar_tabla::procesar_texto($fila->procuracion3);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 24. ";
                    }

                    //Iteracion 25
                    if(strlen(trim($fila->resolucion_fecha))>4 || strlen(trim($fila->resolucion_contenido))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_resolucion      = importar_tabla::procesar_fecha_auto($fila->resolucion_fecha);
                        $detalle->contenido_resolucion   = importar_tabla::procesar_texto($fila->resolucion_contenido);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 25. ";
                    }

                    //Iteracion 26
                    if(strlen(trim($fila->sentencia_2_instancia_fecha))>4 || strlen(trim($fila->sentencia_2_instancia))>1 ){
                        $detalle = new proceso_detalle_demanda_familia();
                        $detalle->id_proceso     = $proceso->id_proceso;
                        $detalle->fh_sentencia      = importar_tabla::procesar_fecha_auto($fila->sentencia_2_instancia_fecha);
                        $detalle->sentencia   = importar_tabla::procesar_texto($fila->sentencia_2_instancia);
                        $detalle->seguimiento   = importar_tabla::procesar_texto($fila->seguimiento);
                        $detalle->save();
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detale_demanda_familia' => $detalle->id_proceso_detale_demanda_familia]);
                        $log.=" Iteracion 26. ";
                    }

                    $log .= " Fila procesada. <br> ";
                }

                $fila->save();  //Cambiar el estado a "importada" o "anulada"
            }
            $log.="<br>Fin del bloque<br>";

            //Actualizar el log de errores
            if(strlen($log_error) > 0) {
                $cualquiera=self::first();
                $encabezado=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $encabezado->log_procesa.=$log_error;
                $encabezado->save();
            }


            //Determinar si faltan mas
            $conteo=self::where('importado',0)->count();


            if($conteo==0) {  //Llegamos al final, hay que actualizar encabezados
                //Fin de la importación
                $log.="<br><h3>Fin de la importacion</h3>";
                //Actualizar control de versiones y bitacora
                $cualquiera=self::first();
                $bitacora=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $bitacora->id_estado=3;
                $bitacora->save();
            }
            $resultado = true;
        }

        //Conteos
        $cierre = new \stdClass();
        $cierre->filas_total    =self::count();
        $cierre->filas_exito    =self::where('importado',1)->count();
        $cierre->filas_fallo    =self::where('importado',99)->count();
        $cierre->filas_pendiente  =self::where('importado',0)->count();
        $cierre->log=$log;
        $cierre->log_error=$log_error;
        $cierre->resultado=$resultado;
        return $cierre;
    }

    // Recibe los datos de una fila de imp_familia y crea la persona
    public static function crear_persona($fila_datos) {
        //Limpiar de guiones
        $nombre_apellido = importar_tabla::procesar_texto($fila_datos->nombre_completo);
        if(strlen($nombre_apellido)<1) {
            return array("resultado"=>false, "objeto"=>null, "mensaje"=>"Nombre en blanco");
        }
        else {
            $existe = persona::where('nombre_completo','like',$nombre_apellido)->first();
            if(isset($existe->id_persona)) {
                return array("resultado"=>true, "objeto"=>$existe);
            }
            else {
                $persona=new persona();
                $persona->nombre_completo=$nombre_apellido;
                //De plano son mujeres
                $persona->genero=1;


                try {
                    $persona->save();
                    return array("resultado"=>true, "objeto"=>$persona);
                }
                catch (\Exception $e) {
                    return array("resultado"=>false, "objeto"=>null, "mensaje"=>$e->getMessage());
                }
            }
        }

    }







}
