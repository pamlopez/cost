<?php

namespace App\Importar;

use App\Models\proceso;
use App\Models\atencion_inicial;
use App\Models\persona;
use App\Models\proceso_detalle_demanda_penal;
use Carbon\Carbon;
use App\Importar\importar_tabla;
use App\Importar\importar_traza;
use App\Importar\importar_bitacora;
use App\Models\atencion_inicial_tipo_violencia;
use App\Models\atencion_inicial_referencia_caimu;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_imp_penal
 * @property int $id_importar_bitacora
 * @property string $anio
 * @property string $hoja_vida
 * @property string $nombre_completo
 * @property string $tipo_proceso
 * @property string $juzgado
 * @property string $ignorame1
 * @property string $anio_inicio
 * @property string $numero_proceso
 * @property string $notificador
 * @property string $calidad
 * @property string $apersonamiento
 * @property string $status
 * @property string $motivo
 * @property string $anio_fin
 * @property string $observaciones
 * @property string $fecha_inicio
 * @property string $diligencias_previas
 * @property string $d1_fecha
 * @property string $d1_hora
 * @property string $d1_diligencia
 * @property string $d1_estrategia
 * @property string $d1_incidencia
 * @property string $d1_resultado
 * @property string $d2_fecha
 * @property string $d2_hora
 * @property string $d2_diligencia
 * @property string $d2_estrategia
 * @property string $d2_incidnecia
 * @property string $d2_resultado
 * @property string $d3_fecha
 * @property string $d3_hora
 * @property string $d3_diligencia
 * @property string $d3_estrategia
 * @property string $d3_incidencia
 * @property string $d3_resultado
 * @property string $d4_fecha
 * @property string $d4_hora
 * @property string $d4_diligencia
 * @property string $d4_estrategia
 * @property string $d4_incidencia
 * @property string $d4_resultado
 * @property string $d5_fecha
 * @property string $d5_hora
 * @property string $d5_diligencia
 * @property string $d5_estrategia
 * @property string $d5_resultado
 * @property string $d6_fecha
 * @property string $d6_hora
 * @property string $d6_diligencia
 * @property string $d6_estrategia
 * @property string $d6_resultado
 * @property string $d7_fecha
 * @property string $d7_hora
 * @property string $d7_diligencia
 * @property string $d7_estrategia
 * @property string $d7_resultado
 * @property string $d8_fecha
 * @property string $d8_hora
 * @property string $d8_diligencia
 * @property string $d8_incidencia
 * @property string $d8_resultado
 * @property string $d9_fecha
 * @property string $d9_hora
 * @property string $d9_diligencia
 * @property string $d9_estrategia
 * @property string $d9_resultado
 * @property string $d10_fecha
 * @property string $d10_hora
 * @property string $d10_diligencia
 * @property string $d10_incidencia
 * @property string $d10_resultado
 * @property string $d11_fecha
 * @property string $d11_hora
 * @property string $d11_diligencia
 * @property string $d11_estrategia
 * @property string $d11_resultado
 * @property string $d12_fecha
 * @property string $d12_hora
 * @property string $d12_diligencia
 * @property string $d12_incidencia
 * @property string $d12_resultado
 * @property string $d13_fecha
 * @property string $d13_hora
 * @property string $d13_diligencia
 * @property string $d13_estrategia
 * @property string $d13_resultado
 * @property string $seguimiento
 * @property int $importado
 * @property string $control
 */
class imp_penal extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'imp_penal';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_imp_penal';
    public $timestamps = FALSE;

    /**
     * @var array
     */
    protected $fillable = ['id_importar_bitacora', 'anio', 'hoja_vida', 'nombre_completo', 'tipo_proceso', 'juzgado', 'ignorame1', 'anio_inicio', 'numero_proceso', 'notificador', 'calidad', 'apersonamiento', 'status', 'motivo', 'anio_fin', 'observaciones', 'fecha_inicio', 'diligencias_previas', 'd1_fecha', 'd1_hora', 'd1_diligencia', 'd1_estrategia', 'd1_incidencia', 'd1_resultado', 'd2_fecha', 'd2_hora', 'd2_diligencia', 'd2_estrategia', 'd2_incidnecia', 'd2_resultado', 'd3_fecha', 'd3_hora', 'd3_diligencia', 'd3_estrategia', 'd3_incidencia', 'd3_resultado', 'd4_fecha', 'd4_hora', 'd4_diligencia', 'd4_estrategia', 'd4_incidencia', 'd4_resultado', 'd5_fecha', 'd5_hora', 'd5_diligencia', 'd5_estrategia', 'd5_resultado', 'd6_fecha', 'd6_hora', 'd6_diligencia', 'd6_estrategia', 'd6_resultado', 'd7_fecha', 'd7_hora', 'd7_diligencia', 'd7_estrategia', 'd7_resultado', 'd8_fecha', 'd8_hora', 'd8_diligencia', 'd8_incidencia', 'd8_resultado', 'd9_fecha', 'd9_hora', 'd9_diligencia', 'd9_estrategia', 'd9_resultado', 'd10_fecha', 'd10_hora', 'd10_diligencia', 'd10_incidencia', 'd10_resultado', 'd11_fecha', 'd11_hora', 'd11_diligencia', 'd11_estrategia', 'd11_resultado', 'd12_fecha', 'd12_hora', 'd12_diligencia', 'd12_incidencia', 'd12_resultado', 'd13_fecha', 'd13_hora', 'd13_diligencia', 'd13_estrategia', 'd13_resultado', 'seguimiento', 'importado', 'control'];

    public static function conteos() {
        $respuesta = new \stdClass();
        $respuesta->filas_total=self::count();;
        $respuesta->filas_exito=self::where('importado',1)->count();
        $respuesta->filas_fallo=self::where('importado',99)->count();
        $respuesta->filas_pendiente=self::where('importado',0)->count();
        if($respuesta->filas_total > 0) {
            $respuesta->avance =  ($respuesta->filas_total - $respuesta->filas_pendiente) / $respuesta->filas_total;
            $respuesta->avance = intval($respuesta->avance * 100);
        }
        else {
            $respuesta->avance=0;
        }

        return $respuesta;
    }


    //////////
    /*
     * Procesa imp_penal para importar datos en proceso
     */
    public static function importar($cantidad=10) {
        $listado=self::where('importado',0)->orderby('id_imp_penal')->limit($cantidad)->get();
        $conteo=$listado->count();

        if($conteo == 0) {
            //Fin de la importación
            $log="<br>Nada que importar";
            $resultado=false;
        }
        else {

            $log="<br>Importando bloque de $conteo filas en 9.4 Penal";
            $log_error="";

            foreach($listado as $fila) {
                $log.="<br>Fila $fila->id_imp_penal, HV: $fila->hoja_vida ";
                //No aceptar sin número de hoja de vida
                if(strlen($fila->hoja_vida)<1) {
                    $error="Fila $fila->id_imp_penal  ignorada, sin numero de hoja de vida.  $fila->nombre_completo<br>";
                    $log.=$error;
                    $fila->importado=99;

                    $log_error.=$error;
                }

                else {
                    //Al final, actualizar la fila importada
                    $fila->importado=1;


                    //Primer bloque: Atención inicial
                    $hoja_vida = atencion_inicial::where('numero_historia_vida', $fila->hoja_vida)->first();
                    if ($hoja_vida) { //Ya está, no hacer nada
                        $log.="<br>H.V.  $fila->hoja_vida existente";
                    }
                    else {  //No existe, crear una nueva
                        $hoja_vida = new atencion_inicial();
                        $hoja_vida->numero_historia_vida = $fila->hoja_vida;
                        //$hoja_vida->fecha = importar_tabla::procesar_fecha($fila->f_atencion_inicial, "Y-m-d");
                        $hoja_vida->edad = null;
                        $hoja_vida->id_rango_caimu = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_caimu");
                        $hoja_vida->id_rango_mingob = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_mingob");
                        $hoja_vida->id_rango_ine = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_ine");

                        //Hard code
                        $hoja_vida->id_caimu = 177;
                        //Determinar persona
                        $creacion = self::crear_persona($fila);
                        if ($creacion['resultado']) {
                            $hoja_vida->id_persona = $creacion['objeto']->id_persona;
                            $log .= " Creada persona " . $creacion['objeto']->id_persona;
                            importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_persona' => $creacion['objeto']->id_persona]);
                        } else {
                            $hoja_vida->id_persona = null;
                            $error = "<Br>Fila $fila->id_imp_ai.  No se pudo crear la persona: " . $creacion['mensaje'] . ". ";
                            $log .= $error;
                            $log_error .= $error;
                        }
                        //Grabar
                        $hoja_vida->save();
                        $log .= " Atención inicial creada. ";
                        //Traza de la A.I.
                        importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_atencion_inicial' => $hoja_vida->id_atencion_inicial]);

                    }
                    //Fin del  bloque

                    $log.="<br> Proceso. ";

                    //Bloque de proceso
                    $proceso = new proceso();
                    $proceso->id_atencion_inicial = $hoja_vida->id_atencion_inicial;
                    $proceso->id_tipo_demanda = 1;  //Penal
                    $proceso->id_tipo_proceso   = importar_tabla::determinar_cat_item($fila->tipo_proceso,31);
                    $proceso->id_juzgado        = importar_tabla::determinar_cat_item($fila->juzgado,37);
                    $proceso->id_juzgado_auxiliar = importar_tabla::determinar_cat_item($fila->ignorame1,37);
                    $proceso->anio_inicio_demanda = importar_tabla::procesar_numero($fila->anio_inicio);
                    $proceso->no_proceso        = importar_tabla::procesar_texto($fila->numero_proceso);
                    $proceso->oficial           = importar_tabla::procesar_texto($fila->notificador);
                    $proceso->id_calidad         = importar_tabla::determinar_cat_item($fila->calidad,34);
                    $proceso->id_apersonamiento_caimu         = importar_tabla::determinar_cat_item($fila->apersonamiento,35);
                    $proceso->id_estado         = importar_tabla::determinar_cat_item($fila->status,29);
                    $proceso->id_motivo         = importar_tabla::determinar_cat_item($fila->motivo,36);
                    $proceso->anio_terminacion  = importar_tabla::procesar_numero($fila->anio_fin);
                    $proceso->observaciones     = importar_tabla::procesar_texto($fila->observaciones);
                    $proceso->save();
                    importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso' => $proceso->id_proceso]);
                    $log.=" Proceso creado. ";

                    // Primer detalle
                    $detalle = new proceso_detalle_demanda_penal();
                    $detalle->id_proceso            = $proceso->id_proceso;
                    $detalle->fh_inicio             = importar_tabla::procesar_fecha_auto($fila->fecha_inicio);
                    $detalle->diligencia_previa_audiencia= importar_tabla::procesar_texto($fila->diligencias_previas);
                    $detalle->save();
                    importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detalle_demanda_penal' => $detalle->id_proceso_detalle_demanda_penal]);
                    $log.=" Detalle creado. ";


                    //Reiteraciones

                    for($i=1; $i<=13 ;$i++) {
                        $fh_audiencia   = "d".$i."_fecha";
                        $hr_audiencia   = "d".$i."_hora";
                        $diligencia_audiencia = "d".$i."_diligencia";
                        $estrategia =  "d".$i."_estrategia";
                        $incidencia =  "d".$i."_incidencia";
                        $resultado  =  "d".$i."_resultado";

                        if(strlen(trim($fila->$fh_audiencia))>4 || strlen(trim($fila->$diligencia_audiencia))>1 ){
                            $detalle = new proceso_detalle_demanda_penal();
                            $detalle->id_proceso  = $proceso->id_proceso;
                            $detalle->fh_audiencia = importar_tabla::procesar_fecha_auto($fila->$fh_audiencia);
                            $detalle->hr_audiencia = importar_tabla::procesar_texto($fila->$hr_audiencia);
                            $detalle->diligencia_audiencia = importar_tabla::procesar_texto($fila->$diligencia_audiencia);
                            $detalle->estrategia = importar_tabla::procesar_texto($fila->$estrategia);
                            $detalle->incidencia = importar_tabla::procesar_texto($fila->$incidencia);
                            $detalle->resultado  = importar_tabla::procesar_texto($fila->$resultado);
                            $detalle->save();
                            importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_proceso_detalle_demanda_penal' => $detalle->id_proceso_detalle_demanda_penal]);
                            $log.="( reiteracion $i )";
                        }
                    }

                    $log .= " Fila procesada. <br> ";
                }
                $fila->save();  //Cambiar el estado a "importada" o "anulada"
            }
            $log.="<br>Fin del bloque<br>";

            //Actualizar el log de errores
            if(strlen($log_error) > 0) {
                $cualquiera=self::first();
                $encabezado=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $encabezado->log_procesa.=$log_error;
                $encabezado->save();
            }

            //Determinar si faltan mas
            $conteo=self::where('importado',0)->count();

            if($conteo==0) {  //Llegamos al final, hay que actualizar encabezados
                //Fin de la importación
                $log.="<br><h3>Fin de la importacion</h3>";
                //Actualizar control de versiones y bitacora
                $cualquiera=self::first();
                $bitacora=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $bitacora->id_estado=3;
                $bitacora->save();
            }
            $resultado = true;
        }

        //Conteos
        $cierre = new \stdClass();
        $cierre->filas_total    =self::count();
        $cierre->filas_exito    =self::where('importado',1)->count();
        $cierre->filas_fallo    =self::where('importado',99)->count();
        $cierre->filas_pendiente  =self::where('importado',0)->count();
        $cierre->log=$log;
        $cierre->log_error=$log_error;
        $cierre->resultado=$resultado;
        return $cierre;
    }

    // Recibe los datos de una fila de imp_penal y crea la persona
    public static function crear_persona($fila_datos) {
        //Limpiar de guiones
        $nombre_apellido = importar_tabla::procesar_texto($fila_datos->nombre_completo);
        if(strlen($nombre_apellido)<1) {
            return array("resultado"=>false, "objeto"=>null, "mensaje"=>"Nombre en blanco");
        }
        else {
            $existe = persona::where('nombre_completo','like',$nombre_apellido)->first();
            if(isset($existe->id_persona)) {
                return array("resultado"=>true, "objeto"=>$existe);
            }
            else {
                $persona=new persona();
                $persona->nombre_completo=$nombre_apellido;
                //De plano son mujeres
                $persona->genero=1;


                try {
                    $persona->save();
                    return array("resultado"=>true, "objeto"=>$persona);
                }
                catch (\Exception $e) {
                    return array("resultado"=>false, "objeto"=>null, "mensaje"=>$e->getMessage());
                }
            }
        }

    }






}
