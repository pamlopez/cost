<?php

namespace App\Importar;

use Illuminate\Database\Eloquent\Model;

class importar_campo extends Model
{
    //
    protected $table = 'importar_campo';
    protected $primaryKey = "id_importar_campo";
    public $timestamps = FALSE;

    public static function estructura($id_tabla=1) {
        return self::where('id_importar_tabla',$id_tabla)
            ->select('campo','columna')
            ->orderby('columna')
            ->get();
    }


    public static function estructura_campos($id_tabla=1) {
        return self::where('id_importar_tabla',$id_tabla)
            ->orderby('columna')
            ->lists('nombre')->toArray();
    }

    // Usado para verificar los encabezados en proyectos
    public static function a_estructura($id_tabla=1) {
        return self::where('id_importar_tabla',$id_tabla)
            ->orderby('columna')
            ->pluck('nombre','columna');
    }

    // Usado para la importacion dinamica en proyectos
    public static function a_estructura_campos($id_tabla=1) {
        return self::where('id_importar_tabla',$id_tabla)
            ->orderby('columna')
            ->pluck('campo','columna');
    }
}
