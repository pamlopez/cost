<?php

namespace App\Importar;

use App\Models\control_asistencia;
use Illuminate\Database\Eloquent\Model;
use App\Models\atencion_inicial;
use App\Models\persona;
use Carbon\Carbon;
use App\Importar\importar_tabla;
use App\Importar\importar_traza;
use App\Importar\importar_bitacora;
use App\Models\atencion_inicial_tipo_violencia;
use App\Models\atencion_inicial_referencia_caimu;

/**
 * @property int $id_imp_asistencia
 * @property int $id_importar_bitacora
 * @property string $dia_semana
 * @property string $dia_mes
 * @property string $mes
 * @property string $anio
 * @property string $hoja_vida
 * @property string $nombres
 * @property string $acompania
 * @property string $causa_a_i
 * @property string $causa_otros
 * @property string $atendio
 * @property string $legal_visita
 * @property string $legal_atiende
 * @property string $psico_visita
 * @property string $psico_atiende
 * @property string $social_visita
 * @property string $social_atiende
 * @property int $importado
 * @property string $control
 */
class imp_asistencia extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'imp_asistencia';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_imp_asistencia';

    /**
     * @var array
     */
    protected $fillable = ['id_importar_bitacora', 'dia_semana', 'dia_mes', 'mes', 'anio', 'hoja_vida', 'nombres', 'acompania', 'causa_a_i', 'causa_otros', 'atendio', 'legal_visita', 'legal_atiende', 'psico_visita', 'psico_atiende', 'social_visita', 'social_atiende', 'importado', 'control'];
    public $timestamps = FALSE;



    public static function conteos() {
        $respuesta = new \stdClass();
        $respuesta->filas_total=self::count();;
        $respuesta->filas_exito=self::where('importado',1)->count();
        $respuesta->filas_fallo=self::where('importado',99)->count();
        $respuesta->filas_pendiente=self::where('importado',0)->count();
        if($respuesta->filas_total > 0) {
            $respuesta->avance =  ($respuesta->filas_total - $respuesta->filas_pendiente) / $respuesta->filas_total;
            $respuesta->avance = intval($respuesta->avance * 100);
        }
        else {
            $respuesta->avance=0;
        }

        return $respuesta;
    }




    //////////
    /*
     * Procesa imp_asistencia para importar control_asistencia
     */

    public static function importar($cantidad=10) {
        $listado=self::where('importado',0)->limit($cantidad)->get();
        $conteo=$listado->count();

        if($conteo == 0) {
            //Fin de la importación
            $log="<br>Nada que importar";
            $resultado=false;
        }
        else {

            $log="<br>Importando bloque de $conteo filas en Control de Asistencia";
            $log_error="";

            foreach($listado as $fila) {
                $log.="<br>Fila $fila->id_imp_asistencia ";
                //No aceptar sin fecha
                if(strlen($fila->dia_mes)<1) {
                    $error="Fila $fila->id_imp_asistencia  ignorada, sin dia del mes<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }
                elseif(strlen($fila->mes)<1) {
                    $error="Fila $fila->id_imp_asistencia  ignorada, sin mes<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }
                elseif(strlen($fila->anio)<1) {
                    $error="Fila $fila->id_imp_asistencia  ignorada, sin anio<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }


                else {



                    //Fecha
                    $fecha=Carbon::create($fila->anio,$fila->mes,$fila->dia_mes);

                    $nuevo =new control_asistencia();
                    //Hard code
                    $nuevo->id_caimu=177;
                    
                    $nuevo->fecha_hora   = $fecha->format("Y-m-d");
                    //Verificar si hay hoja de vida
                    if(strlen($fila->hoja_vida)<1) {  //No hay hoja de vida, crear persona
                        $nuevo->id_atencion_inicial = null;
                        //Crear persona
                        $creacion = self::crear_persona($fila);
                        if ($creacion['resultado']) {
                            $nuevo->id_persona = $creacion['objeto']->id_persona;
                            $log .= " Creada persona " . $creacion['objeto']->id_persona;
                        }
                        else {
                            $hoja_vida->id_persona = null;
                            $error = "<Br>Fila $fila_datos->id_imp_agenda.  No se pudo crear la persona: " . $creacion['mensaje'] . ". ";
                            $log .= $error;
                            $log_error .= $error;
                        }
                    }
                    else {  //Hay hoja de vida
                        $hoja_vida=self::determinar_atencion_inicial($fila); //La hoja de vida crea la persona
                        $nuevo->id_atencion_inicial = $hoja_vida->id_atencion_inicial;
                        $nuevo->id_persona = null;
                    }

                    $nuevo->acompania_en_su_nombre = importar_tabla::procesar_texto($fila->acompania);

                    $nuevo->visita_atencion_inicial   = importar_tabla::determinar_cat_item($fila->causa_a_i,56);
                    $nuevo->id_otros_visita_atencion_inicial   = importar_tabla::determinar_cat_item($fila->causa_otros,57);
                    $nuevo->id_atendio_visita_atencion_inicial  = importar_tabla::determinar_empleada($fila->atendio);

                    $nuevo->id_visita_area_legal   = importar_tabla::determinar_cat_item($fila->legal_visita,58);
                    $nuevo->id_atendio_visita_area_legal  = importar_tabla::determinar_empleada($fila->legal_atiende);

                    $nuevo->id_visita_area_psicologica   = importar_tabla::determinar_cat_item($fila->psico_visita,59);
                    $nuevo->id_atendio_visita_area_psicologica  = importar_tabla::determinar_empleada($fila->psico_atiende);

                    $nuevo->id_visita_area_trabajo_social   = importar_tabla::determinar_cat_item($fila->social_visita,60);
                    $nuevo->id_atendio_visita_area_trabajo_social  = importar_tabla::determinar_empleada($fila->social_atiende);

                    //Grabar
                    $nuevo->save();

                    //Traza de la importacion
                    importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_agenda' => $nuevo->id_agenda]);

                    //Actualizar la fila importada
                    $fila->importado=1;

                }

                $fila->save();
                $log.=" (Fila temporal actualizada).";
            }
            $log.="<br>Fin del bloque<br>";

            //Actualizar el log de errores
            if(strlen($log_error) > 0) {
                $cualquiera=self::first();
                $encabezado=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $encabezado->log_procesa.=$log_error;
                $encabezado->save();
            }


            //Determinar si faltan mas
            $conteo=self::where('importado',0)->count();


            if($conteo==0) {  //Llegamos al final, hay que actualizar encabezados
                //Fin de la importación
                $log.="<br><h3>Fin de la importacion</h3>";
                //Actualizar control de versiones y bitacora
                $cualquiera=self::first();
                $bitacora=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $bitacora->id_estado=3;
                $bitacora->save();
            }
            $resultado = true;
        }

        //Conteos
        $cierre = new \stdClass();
        $cierre->filas_total    =self::count();
        $cierre->filas_exito    =self::where('importado',1)->count();
        $cierre->filas_fallo    =self::where('importado',99)->count();
        $cierre->filas_pendiente  =self::where('importado',0)->count();
        $cierre->log=$log;
        $cierre->log_error=$log_error;
        $cierre->resultado=$resultado;
        return $cierre;
    }

    //Ver si existe la atencion inicial
    public static function determinar_atencion_inicial($fila_datos){
        $log="";
        $hoja_vida = atencion_inicial::where('numero_historia_vida', $fila_datos->hoja_vida)->first();
        if ($hoja_vida) { //Ya está, no hacer nada
            $log="<br>H.V.  $fila_datos->hoja_vida existente";
        }
        else {  //No existe, crear una nueva

            $hoja_vida = new atencion_inicial();
            $hoja_vida->numero_historia_vida = $fila_datos->hoja_vida;
            //$hoja_vida->fecha = importar_tabla::procesar_fecha($fila->f_atencion_inicial, "Y-m-d");
            $hoja_vida->edad = null;
            $hoja_vida->id_rango_caimu = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_caimu");
            $hoja_vida->id_rango_mingob = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_mingob");
            $hoja_vida->id_rango_ine = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_ine");

            //Hard code
            $hoja_vida->id_caimu = 177;
            //Determinar persona
            $creacion = self::crear_persona($fila_datos);
            if ($creacion['resultado']) {
                $hoja_vida->id_persona = $creacion['objeto']->id_persona;
                $log .= " Creada persona " . $creacion['objeto']->id_persona;
                importar_traza::create(['id_importar_bitacora' => $fila_datos->id_importar_bitacora, 'id_persona' => $creacion['objeto']->id_persona]);
            } else {
                $hoja_vida->id_persona = null;
                $error = "<Br>Fila $fila_datos->id_imp_agenda.  No se pudo crear la persona: " . $creacion['mensaje'] . ". ";
                $log .= $error;
                $log_error .= $error;
            }
            //Grabar
            $hoja_vida->save();
            $log .= " Atención inicial creada. ";
            //Traza de la A.I.
            importar_traza::create(['id_importar_bitacora' => $fila_datos->id_importar_bitacora, 'id_atencion_inicial' => $hoja_vida->id_atencion_inicial]);
        }
        return $hoja_vida;

    }

    // Recibe los datos de una fila de imp_am y crea el caso
    public static function crear_persona($fila_datos) {
        //Limpiar de guiones
        $fila_datos->nombres = importar_tabla::procesar_texto($fila_datos->nombres);
        if(strlen($fila_datos->nombres)<1) {
            return array("resultado"=>false, "objeto"=>null, "mensaje"=>"Nombre en blanco");
        }
        else {
            $existe = persona::where('nombre_completo','like',$fila_datos->nombres)->first();
            if(isset($existe->id_persona)) {
                return array("resultado"=>true, "objeto"=>$existe);
            }
            else {
                $persona=new persona();
                $persona->nombre_completo=$fila_datos->nombres;
                $persona->no_identificacion=null;
                $persona->genero=1;
                try {
                    $persona->save();
                    importar_traza::create(['id_importar_bitacora' => $fila_datos->id_importar_bitacora, 'id_persona' => $persona->id_persona]);
                    return array("resultado"=>true, "objeto"=>$persona);
                }
                catch (\Exception $e) {
                    return array("resultado"=>false, "objeto"=>null, "mensaje"=>$e->getMessage());
                }
            }
        }

    }



}
