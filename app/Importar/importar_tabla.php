<?php

namespace App\Importar;

use Illuminate\Database\Eloquent\Model;
use App\Models\cat_item;
use Carbon\Carbon;
use App\Models\empleada;
use App\Models\geo;

class importar_tabla extends Model
{
    //
    protected $table = 'importar_tabla';
    protected $primaryKey = "id_importar_tabla";
    public $timestamps = FALSE;



    // Funciones de uso general
    public static function determinar_cat_item($texto="",$id_cat=0) {
        $texto=trim($texto);
        if(strlen($texto)==0 || $id_cat==0 || is_null($texto)) {
            $valor=null;
        }
        else {
            // Quitar los que tienen "_", "--" "___" o algo así
            $test=$texto;
            if(strlen($texto)>200) {
                $texto=substr($texto,0,190)." ...";
            }
            //$test=str_replace("_","",$test);
            //$test=str_replace("-","",$test);
            if(strlen($test)==0) {
                $valor=null;
            }
            else {
                $desconocidos=collect(["maiz","sin informacion","sin información","sin info", "sin identificar","sin especificar",""]);
                if($desconocidos->search(mb_strtolower($texto)) > 0) {  //Encontrado
                    $valor=null;
                }
                else {
                    // algunas transformaciones
                    if($texto == "No Determinado" || $texto == "No Determinada") $texto="NDT";
                    if($texto == "NA" || $texto == "N/A" || $texto == "N.A." ) $texto="No Aplica";

                    $id=cat_item::where('id_cat',$id_cat)
                        ->where('descripcion','like',"$texto")
                        ->value('id_item');
                    if($id>0) {
                        $valor=$id;
                    }
                    else {
                        $nuevo = cat_item::create(['id_cat'=>$id_cat,'descripcion'=>$texto]);
                        $valor=$nuevo->id_item;
                    }
                }
            }
        }
        return $valor;
    }

    //Criterio fijo: sexo
    public static function sexo_id($cual) {
        $cual=trim($cual);
        $cual=mb_strtolower($cual);
        $a_masculino=array("masculino", "masc","hombre");
        $a_femenino=array("femenino","fem","mujer");
        if(array_search($cual,$a_masculino)===false) {
            if(array_search($cual,$a_femenino)===false) {
                return 3; //No determinado
            }
            else {
                return 1;
            }
        }
        else {
            return 2;
        }

    }

    public static function procesar_fecha($txt_fecha,$formato="d/m/Y") {
        try {
            $fecha=Carbon::createFromFormat($formato,$txt_fecha);
            return $fecha->format("Y-m-d");
        }
        catch (\Exception $e) {
            //$fecha=Carbon::createFromFormat("Ymd","19000101");
            $fecha=null;
            return $fecha;
        }

    }
    public static function procesar_fecha_auto($txt_fecha) {
        $txt_fecha=trim($txt_fecha);

        $largo=strlen($txt_fecha);
        if($largo<6) {
            return null;
        }
        if($largo>10) {  //Agarrar la primer fecha
            $txt_fecha=substr($txt_fecha,0,10);
        }

        if(strpos($txt_fecha,"/")>0) {
            $b="/";
        }
        else {
            $b="-";
        }

        $pedazos=explode($b,$txt_fecha);
        if(count($pedazos)<>3) {
            return null;
        }

        if(strlen($pedazos[0])==4) {  //Y-m-d
            $formato ="Y".$b."m".$b."d";
        }
        elseif(strlen($pedazos[2])==4) {   //d-m-Y
            $formato ="d".$b."m".$b."Y";
        }
        else {  // d-m-y
            $formato = "d".$b."m".$b."y";
        }


        try {
            $fecha=Carbon::createFromFormat($formato,$txt_fecha);
            return $fecha->format("Y-m-d");
        }
        catch (\Exception $e) {
            return null;
        }

    }

    //Para eliminar espacios en blanco, guiones, espacios, etc.
    public static function procesar_texto($texto) {
        $texto=str_replace("-"," ",$texto);
        $texto=str_replace("_"," ",$texto);
        $texto=trim($texto);
        return $texto;
    }

    //Para eliminar caracteres
    public static function procesar_numero($numero) {
        //return $numero*1;
        $numero= filter_var ( $numero, FILTER_SANITIZE_NUMBER_FLOAT);
        if(strlen(trim($numero)==0)) {
            return 0;
        }
        else {
            return $numero*1;
        }

    }

    //Para eliminar caracteres
    public static function procesar_si_no($texto) {
        $texto=mb_strtolower($texto);
        $inicio=substr($texto,0,1);
        if($inicio=="s") {
            return 1;
        }
        elseif($inicio=="n") {
            return 2;
        }
        else {
            return 0;
        }
    }
    //Para eliminar caracteres
    public static function procesar_hora($texto) {
        $texto=trim($texto);
        $texto = str_replace('":"',":",$texto);
        return $texto;
        /*
        if(strlen($texto)<3) {
            return null;
        }
        else {
            $texto=substr($texto,0,5);
        }
        try {
            $hora=Carbon::createFromFormat("H:i",$texto);
            return $hora->format("H:i");
        }
        catch(\Exception $e) {
            return null;
        }
        */
    }
    // Cambiar "Enero" a 1
    public static function procesar_mes($texto) {
        $texto=mb_strtolower($texto);
        $a_meses[1]='enero';
        $a_meses[2]='febrero';
        $a_meses[3]='marzo';
        $a_meses[4]='abril';
        $a_meses[5]='mayo';
        $a_meses[6]='junio';
        $a_meses[7]='julio';
        $a_meses[8]='agosto';
        $a_meses[9]='septiembre';
        $a_meses[10]='octubre';
        $a_meses[11]='noviembre';
        $a_meses[12]='diciembre';
        $meses = collect($a_meses);
        $pos=$meses->search($texto);
        return $pos;
    }

    //Data cleaning
    public static function procesar_dpi($texto) {

        $texto=trim($texto);
        $texto=mb_strtolower($texto);

        // No tiene, no trajo, etc.
        if(strpos($texto,"no")>0) {
            $texto="N/I";
        }
        // En trámite
        if(strpos($texto,"tramite")>0) {
            $texto="N/I";
        }
        // En trámite
        if(strpos($texto,"trámite")>0) {
            $texto="N/I";
        }
        // En trámite
        if(strlen($texto)==0 || is_null($texto)) {
            $texto="N/I";
        }

        // Menor
        if(strpos($texto,"enor")>0) {
            $texto="N/A";
        }

        $texto=str_replace(" ","",$texto);
        $texto=str_replace("-","",$texto);
        $texto=str_replace(".","",$texto);
        return $texto;
    }

    // Funciones de uso general
    public static function determinar_empleada($texto="") {
        $texto=trim($texto);
        if(strlen($texto)==0 || is_null($texto)) {
            $valor=null;
        }
        else {
            // Quitar los que tienen "_", "--" "___" o algo así
            $test=$texto;
            $test=str_replace("_","",$test);
            $test=str_replace("-","",$test);
            if(strlen($test)==0) {
                $valor=null;
            }
            else {
                $desconocidos=collect(["maiz","sin informacion","sin información","sin info", "sin identificar","sin especificar",""]);
                if($desconocidos->search(mb_strtolower($texto)) > 0) {  //Encontrado
                    $valor=null;
                }
                else {
                    // algunas transformaciones
                    if($texto == "No Determinado" || $texto == "No Determinada") $texto="NDT";
                    if($texto == "NA" || $texto == "N/A" || $texto == "N.A." ) $texto="No Aplica";

                    $id=empleada::where('n1','like',"$texto")
                        ->value('id_empleada');
                    if($id>0) {
                        $valor=$id;
                    }
                    else {
                        $nuevo = empleada::create(['n1'=>$texto , 'id_caimu'=>177]);
                        $valor=$nuevo->id_empleada;
                    }
                }
            }
        }
        return $valor;
    }

    public static function procesar_municipio($txt_municipio="",$txt_departamento="") {
        $txt_municipio=self::procesar_texto($txt_municipio);
        $txt_departamento=self::procesar_texto($txt_departamento);
        if(strlen($txt_municipio)<1) {
            return null;
        }
        elseif(strlen($txt_departamento)<1) {
            return null;
        }
        else {
            $depto=geo::where('nivel',1)
                        ->where('nombre','like',$txt_departamento)
                        ->first();
            if(!isset($depto->id_geo)) {
                $depto =new geo();
                $depto->nombre = $txt_departamento;
                $depto->nivel=1;
                $depto->id_padre=0;
                $depto->save();
            }
            $muni=geo::where('id_padre',$depto->id_geo)
                        ->where('nombre','like',$txt_municipio)
                        ->where('nivel',2)
                        ->first();
            if(!isset($muni->id_geo)) {
                $muni =new geo();
                $muni->nombre = $txt_municipio;
                $muni->nivel=2;
                $muni->id_padre=$depto->id_geo;
                $muni->save();
            }
            return $muni;
        }
    }
}
