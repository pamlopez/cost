<?php

// php artisan krlov:generate:model importar/imp_asistencia --table-name=imp_asistencia

namespace App\Importar;

use App\Models\agenda;
use Illuminate\Database\Eloquent\Model;
use App\Models\atencion_inicial;
use App\Models\persona;
use Carbon\Carbon;
use App\Importar\importar_tabla;
use App\Importar\importar_traza;
use App\Importar\importar_bitacora;
use App\Models\atencion_inicial_tipo_violencia;
use App\Models\atencion_inicial_referencia_caimu;

/**
 * @property int $id_imp_agenda
 * @property int $id_importar_bitacora
 * @property string $dia_semana
 * @property string $dia_mes
 * @property string $mes
 * @property string $anio
 * @property string $abogada_atendio
 * @property string $hora_inicio
 * @property string $hora_fin
 * @property string $vehiculo
 * @property string $hora_salida
 * @property string $lugar
 * @property string $direccion
 * @property string $accion
 * @property string $tipo
 * @property string $realizada
 * @property string $tipo_proceso
 * @property string $hoja_vida
 * @property string $nombre
 * @property string $quien_agenda
 * @property string $instruccion
 * @property string $resultado
 * @property int $importado
 * @property string $control
 */
class imp_agenda extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'imp_agenda';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_imp_agenda';

    /**
     * @var array
     */
    protected $guarded = ['id_imp_agenda'];

    public $timestamps = FALSE;




    public static function conteos() {
        $respuesta = new \stdClass();
        $respuesta->filas_total=self::count();;
        $respuesta->filas_exito=self::where('importado',1)->count();
        $respuesta->filas_fallo=self::where('importado',99)->count();
        $respuesta->filas_pendiente=self::where('importado',0)->count();
        if($respuesta->filas_total > 0) {
            $respuesta->avance =  ($respuesta->filas_total - $respuesta->filas_pendiente) / $respuesta->filas_total;
            $respuesta->avance = intval($respuesta->avance * 100);
        }
        else {
            $respuesta->avance=0;
        }

        return $respuesta;
    }




    //////////
    /*
     * Procesa imp_agenda para importar la tabla agenda
     */
    public static function importar($cantidad=10) {
        $listado=self::where('importado',0)->limit($cantidad)->get();
        $conteo=$listado->count();

        if($conteo == 0) {
            //Fin de la importación
            $log="<br>Nada que importar";
            $resultado=false;
        }
        else {

            $log="<br>Importando bloque de $conteo filas en Agenda";
            $log_error="";

            foreach($listado as $fila) {
                $log.="<br>Fila $fila->id_imp_agenda ";
                //No aceptar sin fecha
                if(strlen($fila->dia_mes)<1) {
                    $error="Fila $fila->id_imp_agenda  ignorada, sin dia del mes<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }
                elseif(strlen($fila->mes)<1) {
                    $error="Fila $fila->id_imp_agenda  ignorada, sin mes<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }
                elseif(strlen($fila->anio)<1) {
                    $error="Fila $fila->id_imp_agenda  ignorada, sin anio<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }
                elseif(strlen($fila->hoja_vida) < 4 || strlen($fila->hoja_vida) > 15 ) {
                    $error="Fila $fila->id_imp_agenda  ignorada, sin número de hoja de vida<br>";
                    $log.=$error;
                    $fila->importado=99;
                    $log_error.=$error;
                }

                else {

                    //Fecha
                    $fecha=Carbon::create($fila->anio,$fila->mes,$fila->dia_mes);

                    $nuevo =new agenda();
                    $nuevo->fh_agenda   = $fecha->format("Y-m-d");
                    $nuevo->id_abogada  = importar_tabla::determinar_empleada($fila->abogada_atendio);
                    $nuevo->hr_inicio   = importar_tabla::procesar_hora($fila->hora_inicio);
                    //$nuevo->hr_inicio   = importar_tabla::procesar_texto($fila->hora_inicio);
                    $nuevo->hr_fin      = importar_tabla::procesar_hora($fila->hora_fin);
                    //$nuevo->hr_fin      = importar_tabla::procesar_texto($fila->hora_fin);
                    //$nuevo->id_vehiculo = importar_tabla::procesar_si_no($fila->vehiculo);
                    $nuevo->vehiculo = importar_tabla::procesar_texto($fila->vehiculo);
                    $nuevo->hr_salida   = importar_tabla::procesar_hora($fila->hora_salida);
                    //$nuevo->hr_salida   = importar_tabla::procesar_texto($fila->hora_salida);
                    $nuevo->lugar       = importar_tabla::procesar_texto($fila->lugar);
                    $nuevo->direccion   = importar_tabla::procesar_texto($fila->direccion);
                    $nuevo->id_accion   = importar_tabla::determinar_cat_item($fila->accion,55);
                    //$nuevo->id_tipo     = importar_tabla::determinar_cat_item($fila->tipo,52);
                    $nuevo->tipo     = importar_tabla::procesar_texto($fila->tipo);
                    $nuevo->id_realiza_suspende_audi_debate = importar_tabla::determinar_cat_item($fila->realizada,51);
                    $nuevo->id_tipo_proceso     = importar_tabla::determinar_cat_item($fila->tipo_proceso,31);
                    $hoja_vida=self::determinar_atencion_inicial($fila);
                    $nuevo->id_atencion_inicial = $hoja_vida->id_atencion_inicial;
                    $nuevo->id_quien_agenda  = importar_tabla::determinar_empleada($fila->quien_agenda);
                    $nuevo->instruccion_accion = importar_tabla::procesar_texto($fila->instruccion);
                    $nuevo->resultado_hoja_evolucion = importar_tabla::procesar_texto($fila->resultado);
                    //Grabar
                    $nuevo->save();

                    //Traza de la importacion
                    importar_traza::create(['id_importar_bitacora' => $fila->id_importar_bitacora, 'id_agenda' => $nuevo->id_agenda]);

                    //Actualizar la fila importada
                    $fila->importado=1;

                }

                $fila->save();
                $log.=" (Fila temporal actualizada).";
            }
            $log.="<br>Fin del bloque<br>";

            //Actualizar el log de errores
            if(strlen($log_error) > 0) {
                $cualquiera=self::first();
                $encabezado=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $encabezado->log_procesa.=$log_error;
                $encabezado->save();
            }


            //Determinar si faltan mas
            $conteo=self::where('importado',0)->count();


            if($conteo==0) {  //Llegamos al final, hay que actualizar encabezados
                //Fin de la importación
                $log.="<br><h3>Fin de la importacion</h3>";
                //Actualizar control de versiones y bitacora
                $cualquiera=self::first();
                $bitacora=importar_bitacora::find($cualquiera->id_importar_bitacora);
                $bitacora->id_estado=3;
                $bitacora->save();
            }
            $resultado = true;
        }

        //Conteos
        $cierre = new \stdClass();
        $cierre->filas_total    =self::count();
        $cierre->filas_exito    =self::where('importado',1)->count();
        $cierre->filas_fallo    =self::where('importado',99)->count();
        $cierre->filas_pendiente  =self::where('importado',0)->count();
        $cierre->log=$log;
        $cierre->log_error=$log_error;
        $cierre->resultado=$resultado;
        return $cierre;
    }



    //Ver si existe la atencion inicial
    public static function determinar_atencion_inicial($fila_datos){
        $log="";
        $log_error="";
        if(strlen($fila_datos->hoja_vida) < 4 || strlen($fila_datos->hoja_vida) > 15) {
            $log="<br>H.V.  sin especificar";
            $hoja_vida=null;
        }
        else{
            $hoja_vida = atencion_inicial::where('numero_historia_vida', $fila_datos->hoja_vida)->first();
            if ($hoja_vida) { //Ya está, no hacer nada
                $log="<br>H.V.  $fila_datos->hoja_vida existente";
            }
            else {  //No existe, crear una nueva

                $hoja_vida = new atencion_inicial();
                $hoja_vida->numero_historia_vida = $fila_datos->hoja_vida;
                //$hoja_vida->fecha = importar_tabla::procesar_fecha($fila->f_atencion_inicial, "Y-m-d");
                $hoja_vida->edad = null;
                $hoja_vida->id_rango_caimu = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_caimu");
                $hoja_vida->id_rango_mingob = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_mingob");
                $hoja_vida->id_rango_ine = atencion_inicial::calcula_rango($hoja_vida->edad, "rango_ine");

                //Hard code
                $hoja_vida->id_caimu = 177;
                //Determinar persona
                $creacion = self::crear_persona($fila_datos);
                if ($creacion['resultado']) {
                    $hoja_vida->id_persona = $creacion['objeto']->id_persona;
                    $log .= " Creada persona " . $creacion['objeto']->id_persona;
                    importar_traza::create(['id_importar_bitacora' => $fila_datos->id_importar_bitacora, 'id_persona' => $creacion['objeto']->id_persona]);
                } else {
                    $hoja_vida->id_persona = null;
                    $error = "<Br>Fila $fila_datos->id_imp_agenda.  No se pudo crear la persona: " . $creacion['mensaje'] . ". ";
                    $log .= $error;
                    $log_error .= $error;
                }
                //Grabar
                $hoja_vida->save();
                $log .= " Atención inicial creada. ";
                //Traza de la A.I.
                importar_traza::create(['id_importar_bitacora' => $fila_datos->id_importar_bitacora, 'id_atencion_inicial' => $hoja_vida->id_atencion_inicial]);
            }
        }

        return $hoja_vida;

    }

    // Recibe los datos de una fila de imp_am y crea el caso
    public static function crear_persona($fila_datos) {
        //Limpiar de guiones
        $fila_datos->nombre = importar_tabla::procesar_texto($fila_datos->nombre);
        if(strlen($fila_datos->nombre)<1) {
            return array("resultado"=>false, "objeto"=>null, "mensaje"=>"Nombre en blanco");
        }
        else {
            $existe = persona::where('nombre_completo','like',$fila_datos->nombre)->first();
            if(isset($existe->id_persona)) {
                return array("resultado"=>true, "objeto"=>$existe);
            }
            else {
                $persona=new persona();
                $persona->nombre_completo=$fila_datos->nombre;
                $persona->no_identificacion=null;
                $persona->genero=1;
                try {
                    $persona->save();
                    return array("resultado"=>true, "objeto"=>$persona);
                }
                catch (\Exception $e) {
                    return array("resultado"=>false, "objeto"=>null, "mensaje"=>$e->getMessage());
                }
            }
        }

    }





}
